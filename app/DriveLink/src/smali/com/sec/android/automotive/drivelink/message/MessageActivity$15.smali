.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$15;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getSuggestsListItemOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 1592
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v12, 0x7f0a014a

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1596
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->hideInputMethod()V
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$61(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    .line 1598
    const/4 v9, 0x0

    .line 1599
    .local v9, "currentItem":I
    const/4 v0, 0x0

    .line 1600
    .local v0, "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    const/4 v8, 0x0

    .line 1601
    .local v8, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)I

    move-result v1

    const/16 v2, 0x10

    if-ne v1, v2, :cond_1

    .line 1603
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$55(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    .line 1604
    mul-int/lit8 v2, v2, 0x4

    .line 1603
    add-int/2addr v1, v2

    add-int/lit8 v9, v1, -0x1

    .line 1605
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$62(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 1606
    .restart local v0    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-static {v0}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v8

    .line 1619
    :cond_0
    :goto_0
    new-instance v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v7}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 1621
    .local v7, "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getMainPhoneNumber(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)I
    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$63(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)I

    move-result v1

    iput v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    .line 1623
    iput-object v8, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 1624
    iput v9, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserIndex:I

    .line 1625
    if-eqz v8, :cond_3

    .line 1626
    iget-object v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v1, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1628
    iget-object v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v1, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isJapaneseString(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1629
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1630
    new-array v2, v11, [Ljava/lang/Object;

    .line 1631
    iget-object v3, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v3, v3, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    aput-object v3, v2, v10

    .line 1630
    invoke-virtual {v1, v12, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1629
    iput-object v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 1641
    :goto_1
    const-string/jumbo v1, "DM_SMS_COMPOSE"

    invoke-static {v1, v7}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 1644
    return-void

    .line 1607
    .end local v7    # "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)I

    move-result v1

    const/16 v2, 0x42

    if-ne v1, v2, :cond_0

    .line 1608
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$55(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    .line 1609
    mul-int/lit8 v2, v2, 0x4

    .line 1608
    add-int/2addr v1, v2

    add-int/lit8 v9, v1, -0x1

    .line 1610
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "getSuggestsListItemOnClickListener"

    .line 1611
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "currentItem : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1610
    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$39(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1612
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactChoices:Ljava/util/List;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$20(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    check-cast v8, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 1613
    .restart local v8    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .end local v0    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    iget-wide v1, v8, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    .line 1614
    iget-object v3, v8, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 1615
    invoke-virtual {v8}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v4

    .line 1616
    invoke-virtual {v8}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    .line 1613
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .restart local v0    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    goto/16 :goto_0

    .line 1633
    .restart local v7    # "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1634
    new-array v2, v11, [Ljava/lang/Object;

    .line 1635
    iget-object v3, v8, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    aput-object v3, v2, v10

    .line 1634
    invoke-virtual {v1, v12, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1633
    iput-object v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    goto :goto_1

    .line 1638
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1639
    new-array v2, v11, [Ljava/lang/Object;

    const-string/jumbo v3, ""

    aput-object v3, v2, v10

    invoke-virtual {v1, v12, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1638
    iput-object v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    goto/16 :goto_1
.end method
