.class Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$1;
.super Ljava/lang/Object;
.source "OnDriveLinkMusicPlayerListenerImp.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->onMusicPlayStarted(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;

.field private final synthetic val$_bPlayNow:Z

.field private final synthetic val$_music:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Z)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$1;->val$_music:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    iput-boolean p3, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$1;->val$_bPlayNow:Z

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$1;->val$_music:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 32
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$1;->val$_bPlayNow:Z

    .line 31
    invoke-interface {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;->onMusicPlayStarted(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Z)V

    .line 34
    :cond_0
    return-void
.end method
