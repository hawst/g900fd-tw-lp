.class public Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/PrivateModeBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PrivateModeBroadcastReceiver.java"


# instance fields
.field private mNotificationHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/PrivateModeBroadcastReceiver;->mNotificationHandler:Landroid/os/Handler;

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/PrivateModeBroadcastReceiver;->mNotificationHandler:Landroid/os/Handler;

    .line 21
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/PrivateModeBroadcastReceiver;->mNotificationHandler:Landroid/os/Handler;

    .line 22
    return-void
.end method

.method private sendMessage(IILjava/lang/Object;)V
    .locals 2
    .param p1, "notiType"    # I
    .param p2, "arg1"    # I
    .param p3, "data"    # Ljava/lang/Object;

    .prologue
    .line 44
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/PrivateModeBroadcastReceiver;->mNotificationHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 45
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 46
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 47
    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 49
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/PrivateModeBroadcastReceiver;->mNotificationHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 50
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 26
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/PrivateModeBroadcastReceiver;->mNotificationHandler:Landroid/os/Handler;

    if-nez v1, :cond_1

    .line 41
    :cond_0
    :goto_0
    return-void

    .line 30
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 32
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v1, "MY_TAG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[YANG] onReceive() :: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    const-string/jumbo v1, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 35
    const/16 v1, 0x13

    invoke-direct {p0, v1, v4, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/PrivateModeBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    .line 38
    :cond_2
    const-string/jumbo v1, "com.samsung.android.intent.action.PRIVATE_MODE_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 39
    const/16 v1, 0x12

    invoke-direct {p0, v1, v4, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/PrivateModeBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto :goto_0
.end method
