.class Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;
.super Ljava/lang/Object;
.source "PhoneActivity.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->initSearchTextField()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    .line 709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    return-object v0
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 10
    .param p1, "arg0"    # Landroid/widget/TextView;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 713
    const-string/jumbo v7, "PhoneActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "search : actionId : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    if-eqz p3, :cond_0

    .line 715
    const-string/jumbo v7, "PhoneActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "search : Keyevent : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 717
    :cond_0
    const-string/jumbo v7, "PhoneActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "search : text "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v9}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 719
    if-eqz p3, :cond_6

    .line 720
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v7

    const/16 v8, 0x42

    if-eq v7, v8, :cond_1

    .line 721
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v7

    const/16 v8, 0x54

    if-ne v7, v8, :cond_6

    .line 723
    :cond_1
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-interface {v7}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 883
    :goto_0
    return v5

    .line 727
    :cond_2
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$2(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 728
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 730
    const-string/jumbo v7, "DEBUG"

    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 731
    const-string/jumbo v7, "HIDDEN_FEATURE_DISP_USERTURN"

    .line 732
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v8}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 733
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    .line 734
    const-string/jumbo v8, "HiddenFeature : Disp UserTuen"

    .line 733
    invoke-static {v7, v8, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    .line 735
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 737
    const-string/jumbo v5, "HIDDEN_FEATURE_DISP_USERTURN"

    .line 738
    const-string/jumbo v7, "TRUE"

    .line 736
    invoke-static {v5, v7}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v6

    .line 739
    goto :goto_0

    .line 741
    :cond_3
    const-string/jumbo v7, "HIDDEN_FEATURE_DISP_WAKEUP_NOTI"

    .line 742
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v8}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 743
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    .line 744
    const-string/jumbo v8, "HiddenFeature : Wakeup Notfication"

    .line 743
    invoke-static {v7, v8, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    .line 745
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 747
    const-string/jumbo v5, "HIDDEN_FEATURE_DISP_WAKEUP_NOTI"

    .line 748
    const-string/jumbo v7, "TRUE"

    .line 746
    invoke-static {v5, v7}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v6

    .line 749
    goto/16 :goto_0

    .line 751
    :cond_4
    const-string/jumbo v7, "HIDDEN_FEATURE_DEBUG_SETTING"

    .line 752
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v8}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 753
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    .line 754
    const-string/jumbo v8, "HiddenFeature : Debug setting"

    .line 753
    invoke-static {v7, v8, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    .line 755
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 757
    const-string/jumbo v5, "HIDDEN_FEATURE_DEBUG_SETTING"

    .line 758
    const-string/jumbo v7, "TRUE"

    .line 756
    invoke-static {v5, v7}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v6

    .line 759
    goto/16 :goto_0

    .line 797
    :cond_5
    const-string/jumbo v5, "PhoneActivity"

    const-string/jumbo v7, "search : request! "

    invoke-static {v5, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 799
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 802
    .local v0, "dlServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    new-instance v5, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11$1;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11$1;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;)V

    invoke-interface {v0, v5}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkContactListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;)V

    .line 871
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 873
    .local v3, "startTime":J
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    .line 874
    invoke-interface {v7}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v7

    .line 872
    invoke-interface {v0, v5, v7}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestSearchedContactList(Landroid/content/Context;Ljava/lang/String;)V

    .line 876
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 878
    .local v1, "endTime":J
    const-string/jumbo v5, "PhoneActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "searched  requestSearchedContactList: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 879
    sub-long v8, v1, v3

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 878
    invoke-static {v5, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .end local v0    # "dlServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    .end local v1    # "endTime":J
    .end local v3    # "startTime":J
    :cond_6
    move v5, v6

    .line 883
    goto/16 :goto_0
.end method
