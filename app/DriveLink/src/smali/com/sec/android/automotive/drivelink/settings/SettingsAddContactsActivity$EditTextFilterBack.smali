.class public Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$EditTextFilterBack;
.super Landroid/widget/EditText;
.source "SettingsAddContactsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EditTextFilterBack"
.end annotation


# instance fields
.field private mOnImeBack:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$EditTextImeBackListener;

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 772
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$EditTextFilterBack;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    .line 773
    invoke-direct {p0, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 774
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 776
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$EditTextFilterBack;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    .line 777
    invoke-direct {p0, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 778
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "attrs"    # Landroid/util/AttributeSet;
    .param p4, "defStyle"    # I

    .prologue
    .line 781
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$EditTextFilterBack;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    .line 782
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 783
    return-void
.end method


# virtual methods
.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 787
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 788
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 789
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$EditTextFilterBack;->mOnImeBack:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$EditTextImeBackListener;

    if-eqz v0, :cond_0

    .line 790
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$EditTextFilterBack;->mOnImeBack:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$EditTextImeBackListener;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$EditTextFilterBack;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$EditTextImeBackListener;->onImeBack(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$EditTextFilterBack;Ljava/lang/String;)V

    .line 792
    :cond_0
    invoke-super {p0, p2}, Landroid/widget/EditText;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public setOnEditTextImeBackListener(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$EditTextImeBackListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$EditTextImeBackListener;

    .prologue
    .line 797
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$EditTextFilterBack;->mOnImeBack:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$EditTextImeBackListener;

    .line 798
    return-void
.end method
