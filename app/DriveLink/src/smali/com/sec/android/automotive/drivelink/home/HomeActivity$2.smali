.class Lcom/sec/android/automotive/drivelink/home/HomeActivity$2;
.super Ljava/lang/Object;
.source "HomeActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/home/HomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    .line 1428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseDownloadProgress(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLDownloadData;)V
    .locals 0
    .param p1, "progress"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLDownloadData;

    .prologue
    .line 1479
    return-void
.end method

.method public onResponseInstallStarted()V
    .locals 0

    .prologue
    .line 1485
    return-void
.end method

.method public onResponseRequestCheckUpdateVersion(Z)V
    .locals 3
    .param p1, "result"    # Z

    .prologue
    .line 1433
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$1()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onResponseRequestCheckUpdateVersion : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1435
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isUpdate()Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$5(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1436
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isForeground:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$6(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1437
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->goUpdateActivity()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$7(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)V

    .line 1460
    :cond_0
    :goto_0
    return-void

    .line 1439
    :cond_1
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$1()Ljava/lang/String;

    move-result-object v0

    .line 1440
    const-string/jumbo v1, "HOME ACTIVITY IS IN BACKGROUND!! WAIT UNTIL FOREGROUND!!"

    .line 1439
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1441
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$8(Z)V

    goto :goto_0
.end method

.method public onResponseRequestUpdateApplicationFail(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;)V
    .locals 0
    .param p1, "result"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .prologue
    .line 1473
    return-void
.end method

.method public onResponseRequestUpdateApplicationSuccess()V
    .locals 0

    .prologue
    .line 1466
    return-void
.end method
