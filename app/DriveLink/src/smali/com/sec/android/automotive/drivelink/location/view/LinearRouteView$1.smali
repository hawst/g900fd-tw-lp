.class Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$1;
.super Ljava/lang/Object;
.source "LinearRouteView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->buildLayout(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;

.field private final synthetic val$participant:Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$1;->this$0:Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$1;->val$participant:Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;

    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$1;->this$0:Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;

    # getter for: Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->clickListener:Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$OnParticipantClickListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->access$0(Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;)Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$OnParticipantClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$1;->this$0:Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;

    # getter for: Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->clickListener:Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$OnParticipantClickListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->access$0(Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;)Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$OnParticipantClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$1;->val$participant:Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;

    .line 142
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$1;->this$0:Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;

    # getter for: Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->data:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->access$1(Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$1;->val$participant:Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;

    invoke-interface {v2, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 141
    invoke-interface {v0, p1, v1, v2}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$OnParticipantClickListener;->onParticipantClick(Landroid/view/View;Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;I)V

    .line 144
    :cond_0
    return-void
.end method
