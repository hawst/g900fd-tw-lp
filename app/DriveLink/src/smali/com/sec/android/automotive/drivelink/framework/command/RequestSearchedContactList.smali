.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestSearchedContactList.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mItemCount:I

.field private mKeyword:Ljava/lang/String;

.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/lang/String;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "keyword"    # Ljava/lang/String;
    .param p4, "itemCount"    # I

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 12
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;->mContext:Landroid/content/Context;

    .line 13
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;->mList:Ljava/util/ArrayList;

    .line 14
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;->mItemCount:I

    .line 15
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;->mKeyword:Ljava/lang/String;

    .line 22
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;->mContext:Landroid/content/Context;

    .line 23
    iput p4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;->mItemCount:I

    .line 24
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;->mKeyword:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;->mContext:Landroid/content/Context;

    .line 45
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;->mList:Ljava/util/ArrayList;

    .line 46
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;->mKeyword:Ljava/lang/String;

    .line 47
    return-void
.end method

.method protected process()V
    .locals 4

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;->mKeyword:Ljava/lang/String;

    iget v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;->mItemCount:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getSearchedContactList(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;->mList:Ljava/util/ArrayList;

    .line 31
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 36
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkContactListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;

    move-result-object v0

    .line 37
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;
    if-eqz v0, :cond_0

    .line 38
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;->mList:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;->onResponseRequestSearchedContactList(Ljava/util/ArrayList;)V

    .line 40
    :cond_0
    return-void
.end method
