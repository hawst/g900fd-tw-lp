.class public Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "ArrivedParticipantsExpandableAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private arrivedParticipantsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;"
        }
    .end annotation
.end field

.field private final context:Landroid/content/Context;

.field private final dlinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p2, "arrivedParticipantsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;->arrivedParticipantsList:Ljava/util/List;

    .line 44
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;->context:Landroid/content/Context;

    .line 45
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;->dlinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 46
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;->arrivedParticipantsList:Ljava/util/List;

    .line 47
    return-void
.end method


# virtual methods
.method public getChild(II)Ljava/lang/Object;
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 281
    const/4 v0, 0x0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 2
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 259
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 55
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;->arrivedParticipantsList:Ljava/util/List;

    .line 56
    invoke-interface {v7, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 57
    .local v5, "participant":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;->arrivedParticipantsList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v4

    .line 59
    .local v4, "listSize":I
    if-nez p4, :cond_1

    .line 60
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;->context:Landroid/content/Context;

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 61
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f030046

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 63
    .local v6, "view":Landroid/view/View;
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;

    const/4 v7, 0x0

    invoke-direct {v1, v7}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;-><init>(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)V

    .line 64
    .local v1, "holder":Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;
    const v7, 0x7f0901a9

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->setImgUser(Landroid/widget/ImageView;)V

    .line 65
    const v7, 0x7f090123

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->setTvName(Landroid/widget/TextView;)V

    .line 67
    const v7, 0x7f0901aa

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 66
    invoke-virtual {v1, v7}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->setTvLetterUser(Landroid/widget/TextView;)V

    .line 68
    const v7, 0x7f0901ac

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->setChildArea(Landroid/view/View;)V

    .line 70
    invoke-virtual {v6, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 77
    .end local v3    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;->dlinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;->context:Landroid/content/Context;

    .line 78
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v9

    .line 77
    invoke-interface {v7, v8, v9}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 81
    .local v2, "image":Landroid/graphics/Bitmap;
    if-lez v4, :cond_0

    .line 83
    if-nez v2, :cond_2

    .line 84
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_2

    .line 85
    # getter for: Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->imgUser:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->access$1(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 86
    # getter for: Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->tvLetterUser:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->access$2(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v7

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 88
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 89
    const v8, 0x7f0201a3

    .line 90
    const v9, 0x7f0201a2

    .line 88
    invoke-static {v7, v8, v9}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 91
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->getImgUser()Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 106
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :goto_1
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 107
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->getTvName()Landroid/widget/TextView;

    move-result-object v7

    .line 108
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v8

    .line 107
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    :goto_2
    if-eqz p3, :cond_0

    .line 114
    # getter for: Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->childArea:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->access$3(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;->context:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 115
    const v9, 0x7f020267

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .line 114
    invoke-virtual {v7, v8}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 120
    :cond_0
    return-object v6

    .line 73
    .end local v1    # "holder":Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;
    .end local v2    # "image":Landroid/graphics/Bitmap;
    .end local v6    # "view":Landroid/view/View;
    :cond_1
    move-object v6, p4

    .line 74
    .restart local v6    # "view":Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;

    .restart local v1    # "holder":Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;
    goto :goto_0

    .line 92
    .restart local v2    # "image":Landroid/graphics/Bitmap;
    :cond_2
    if-nez v2, :cond_3

    .line 93
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 94
    # getter for: Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->imgUser:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->access$1(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v7

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 95
    # getter for: Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->tvLetterUser:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->access$2(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 96
    # getter for: Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->tvLetterUser:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->access$2(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v8

    .line 97
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 96
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 99
    :cond_3
    # getter for: Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->imgUser:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->access$1(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 100
    # getter for: Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->tvLetterUser:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->access$2(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v7

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 101
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 102
    const v8, 0x7f0201a2

    .line 101
    invoke-static {v7, v2, v8}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 103
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->getImgUser()Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_1

    .line 110
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_4
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->getTvName()Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2
.end method

.method public getChildrenCount(I)I
    .locals 1
    .param p1, "groupPosition"    # I

    .prologue
    .line 241
    const/4 v0, 0x0

    return v0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 1
    .param p1, "groupPosition"    # I

    .prologue
    .line 276
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGroupCount()I
    .locals 1

    .prologue
    .line 264
    const/4 v0, 0x1

    return v0
.end method

.method public getGroupId(I)J
    .locals 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 270
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v8, 0x0

    .line 129
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;->arrivedParticipantsList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    .line 131
    .local v3, "listSize":I
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 132
    const v7, 0x7f0201dc

    .line 131
    invoke-static {v6, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 133
    .local v0, "close":Landroid/graphics/Bitmap;
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 134
    const v7, 0x7f0201dd

    .line 133
    invoke-static {v6, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 136
    .local v4, "open":Landroid/graphics/Bitmap;
    if-nez p3, :cond_1

    .line 137
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;->context:Landroid/content/Context;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 138
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f030047

    invoke-virtual {v2, v6, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 140
    .local v5, "view":Landroid/view/View;
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;

    invoke-direct {v1, v8}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;-><init>(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)V

    .line 142
    .local v1, "holder":Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;
    const v6, 0x7f0901ae

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 141
    invoke-virtual {v1, v6}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->setIvListStatus(Landroid/widget/ImageView;)V

    .line 143
    const v6, 0x7f0901ad

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->setTvArrived(Landroid/widget/TextView;)V

    .line 144
    const v6, 0x7f0901ac

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->setParentArea(Landroid/view/View;)V

    .line 145
    invoke-virtual {v5, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 152
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    if-lez v3, :cond_0

    .line 153
    # getter for: Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->tvArrived:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->access$4(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 154
    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 155
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;->context:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 156
    const v9, 0x7f0a0312

    .line 155
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 153
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    # getter for: Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->parentArea:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->access$5(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 158
    const v8, 0x7f02026a

    .line 157
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 159
    # getter for: Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->ivListStatus:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->access$6(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 162
    :cond_0
    if-eqz p2, :cond_2

    .line 163
    # getter for: Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->ivListStatus:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->access$6(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 168
    :goto_1
    return-object v5

    .line 148
    .end local v1    # "holder":Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;
    .end local v5    # "view":Landroid/view/View;
    :cond_1
    move-object v5, p3

    .line 149
    .restart local v5    # "view":Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;

    .restart local v1    # "holder":Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;
    goto :goto_0

    .line 165
    :cond_2
    # getter for: Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->ivListStatus:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->access$6(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 247
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 253
    const/4 v0, 0x0

    return v0
.end method

.method public onGroupCollapsed(I)V
    .locals 0
    .param p1, "groupPosition"    # I

    .prologue
    .line 294
    invoke-super {p0, p1}, Landroid/widget/BaseExpandableListAdapter;->onGroupCollapsed(I)V

    .line 295
    return-void
.end method

.method public onGroupExpanded(I)V
    .locals 0
    .param p1, "groupPosition"    # I

    .prologue
    .line 287
    invoke-super {p0, p1}, Landroid/widget/BaseExpandableListAdapter;->onGroupExpanded(I)V

    .line 289
    return-void
.end method
