.class public Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;
.source "LocationShareMapActivity.java"


# static fields
.field public static final FLAG_FROM_SHARE:Ljava/lang/String; = "fromShare"

.field public static final FLAG_STATUS:Ljava/lang/String; = "status"

.field public static final PARAM_PHONE:Ljava/lang/String; = "phone"

.field private static final TAG:Ljava/lang/String; = "LocationShareMapActivity"


# instance fields
.field private mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getLayoutId()I
    .locals 1

    .prologue
    .line 182
    const v0, 0x7f030036

    return v0
.end method

.method protected getLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    const v0, 0x7f090098

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 51
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;)V

    .line 50
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    const v0, 0x7f0900a2

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 61
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;)V

    .line 60
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    const v0, 0x7f090180

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 70
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 139
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onDestroy()V

    .line 140
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 141
    return-void
.end method

.method public onFlowCommandExcute(Ljava/lang/String;)V
    .locals 4
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 120
    const-string/jumbo v2, "DM_LOCATION_SHARE_CONFIRM"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 121
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->checkSimState()I

    move-result v1

    .line 123
    .local v1, "state":I
    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    .line 124
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->sendLocationSharedMessage()V

    .line 125
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 126
    const-class v3, Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    .line 125
    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 127
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x4000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 128
    const-string/jumbo v2, "fromShare"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 129
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->startActivity(Landroid/content/Intent;)V

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->finish()V

    .line 135
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "state":I
    :cond_0
    :goto_0
    return-void

    .line 132
    .restart local v1    # "state":I
    :cond_1
    invoke-static {p0, v1}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->showToast(Landroid/app/Activity;I)V

    goto :goto_0
.end method

.method public onFlowCommandRoute(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->sendLocation()V

    .line 116
    return-void
.end method

.method public onFlowCommandSend(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->sendLocation()V

    .line 111
    return-void
.end method

.method public onFlowCommandYes(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->sendLocation()V

    .line 106
    return-void
.end method

.method protected sendLocation()V
    .locals 4

    .prologue
    .line 88
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->checkSimState()I

    move-result v1

    .line 90
    .local v1, "state":I
    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->sendLocationSharedMessage()V

    .line 92
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 93
    const-class v3, Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    .line 92
    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 94
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x4000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 95
    const-string/jumbo v2, "fromShare"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 96
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->startActivity(Landroid/content/Intent;)V

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->finish()V

    .line 101
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 99
    :cond_0
    invoke-static {p0, v1}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->showToast(Landroid/app/Activity;I)V

    goto :goto_0
.end method

.method sendLocationSharedMessage()V
    .locals 4

    .prologue
    .line 144
    const-string/jumbo v2, "VAC_CLIENT_DM"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->isFlowManagerIntent(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 147
    const-string/jumbo v2, "DM_LOCATION_SHARE_CONFIRM"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v1

    .line 149
    .local v1, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-nez v1, :cond_1

    .line 172
    .end local v1    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :goto_0
    return-void

    .line 153
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "phone"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 154
    const-string/jumbo v2, "LocationShareMapActivity"

    const-string/jumbo v3, "Friend\'s phone number not found."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 158
    :cond_1
    const/4 v0, 0x0

    .line 160
    .local v0, "buddyPhoneNumber":Ljava/lang/String;
    const-string/jumbo v2, "VAC_CLIENT_DM"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->isFlowManagerIntent(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 163
    const-string/jumbo v2, "DM_LOCATION_SHARE_CONFIRM"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v1

    .line 164
    .restart local v1    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v1, :cond_2

    .line 165
    iget-object v2, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    iget-object v0, v2, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    .line 170
    .end local v1    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_2
    :goto_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->getInstance()Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->shareMyLocation(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareMapActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "phone"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
