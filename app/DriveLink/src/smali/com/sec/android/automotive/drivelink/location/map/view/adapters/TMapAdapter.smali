.class public Lcom/sec/android/automotive/drivelink/location/map/view/adapters/TMapAdapter;
.super Lcom/sec/android/automotive/drivelink/location/map/LocationMap;
.source "TMapAdapter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;-><init>()V

    .line 23
    return-void
.end method


# virtual methods
.method public createUpdateableRoute(I)Lcom/sec/android/automotive/drivelink/location/model/IGraphicRoute;
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 39
    const/4 v0, 0x0

    return-object v0
.end method

.method public drawRoute(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, "points":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    return-void
.end method

.method public setCompassEnabled(Z)V
    .locals 0
    .param p1, "option"    # Z

    .prologue
    .line 49
    return-void
.end method

.method public setZoomControlsEnabled(Z)V
    .locals 0
    .param p1, "option"    # Z

    .prologue
    .line 44
    return-void
.end method

.method public zoomToArea(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;I)V
    .locals 0
    .param p1, "loc1"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p2, "loc2"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p3, "padding"    # I

    .prologue
    .line 34
    return-void
.end method
