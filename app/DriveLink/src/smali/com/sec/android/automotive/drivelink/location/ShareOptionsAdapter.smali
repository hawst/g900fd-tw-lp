.class public Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "ShareOptionsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static final ITEM_NUM_PER_PAGE:I = 0x3

.field private static final MAX_PAGE_TO_LOOP:I = 0x4e20


# instance fields
.field private mBitmapList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final mClickListener:Landroid/view/View$OnClickListener;

.field private final mContext:Landroid/content/Context;

.field private final mInitialPosition:I

.field private final mOptionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;",
            ">;"
        }
    .end annotation
.end field

.field private mPageCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View$OnClickListener;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "clickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v4, 0x0

    .line 49
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 47
    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mBitmapList:Ljava/util/ArrayList;

    .line 51
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mOptionList:Ljava/util/ArrayList;

    .line 52
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mOptionList:Ljava/util/ArrayList;

    sget-object v3, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;->SEND_LOCATION:Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mOptionList:Ljava/util/ArrayList;

    sget-object v3, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;->SHARE_REQUEST:Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mOptionList:Ljava/util/ArrayList;

    sget-object v3, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;->SHARE_GROUP:Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mContext:Landroid/content/Context;

    .line 57
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->getPageCount()I

    move-result v1

    .line 59
    .local v1, "pageCount":I
    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 61
    const/16 v2, 0x2710

    rem-int/2addr v2, v1

    rsub-int v2, v2, 0x2710

    .line 60
    iput v2, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mInitialPosition:I

    .line 62
    const/16 v2, 0x4e20

    iput v2, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mPageCount:I

    .line 68
    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mBitmapList:Ljava/util/ArrayList;

    .line 69
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mOptionList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 71
    return-void

    .line 64
    .end local v0    # "i":I
    :cond_0
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mInitialPosition:I

    .line 65
    iput v1, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mPageCount:I

    goto :goto_0

    .line 70
    .restart local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private getOptionBitmap(Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "option"    # Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    .prologue
    .line 125
    const/4 v0, 0x0

    .line 129
    .local v0, "maskedBitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 130
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;->getIconId()I

    move-result v2

    const v3, 0x7f020204

    .line 129
    invoke-static {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 132
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 133
    const v2, 0x7f020205

    .line 132
    invoke-static {v1, v0, v2}, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->imageOverlay(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method public static imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;
    .locals 11
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "origId"    # I
    .param p2, "maskId"    # I

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 151
    invoke-static {p0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 152
    .local v2, "origBitmap":Landroid/graphics/Bitmap;
    invoke-static {p0, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 155
    .local v1, "maskBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    add-int/lit8 v5, v5, -0x28

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    add-int/lit8 v6, v6, -0x28

    .line 154
    invoke-static {v2, v5, v6, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 157
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    .line 158
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 159
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 158
    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 160
    .local v4, "resultBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v0, v4}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 161
    invoke-virtual {v0, v2, v8, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 163
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 164
    .local v3, "paint":Landroid/graphics/Paint;
    invoke-virtual {v3, v10}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 165
    new-instance v5, Landroid/graphics/PorterDuffXfermode;

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v5, v6}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 167
    invoke-virtual {v0, v1, v8, v8, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 168
    invoke-virtual {v3, v9}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 169
    invoke-virtual {v0, v4, v8, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 171
    return-object v4
.end method

.method public static imageOverlay(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "origBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "overlayId"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 138
    invoke-static {p0, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 139
    .local v1, "overlayBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 140
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v5

    .line 139
    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 142
    .local v2, "resultBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 143
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v0, p1, v6, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 144
    invoke-virtual {v0, v1, v6, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 146
    return-object v2
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 177
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 178
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 182
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mPageCount:I

    return v0
.end method

.method public getIndexBitmap(I)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 196
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 197
    :cond_0
    const/4 v0, 0x0

    .line 198
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public getInitialPosition()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mInitialPosition:I

    return v0
.end method

.method public getPageCount()I
    .locals 3

    .prologue
    .line 186
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mOptionList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    div-int/lit8 v0, v2, 0x3

    .line 187
    .local v0, "page":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mOptionList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    rem-int/lit8 v1, v2, 0x3

    .line 188
    .local v1, "remainder":I
    if-lez v1, :cond_0

    .line 189
    add-int/lit8 v0, v0, 0x1

    .line 192
    :cond_0
    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 13
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    const/4 v12, 0x0

    .line 75
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mContext:Landroid/content/Context;

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 77
    .local v4, "layoutInflater":Landroid/view/LayoutInflater;
    const v8, 0x7f030067

    .line 76
    invoke-virtual {v4, v8, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 79
    .local v7, "view":Landroid/view/View;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 80
    .local v5, "listItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/widget/RelativeLayout;>;"
    const v8, 0x7f090207

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    const v8, 0x7f090208

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    const v8, 0x7f090209

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->getPageCount()I

    move-result v8

    rem-int v8, p2, v8

    mul-int/lit8 v2, v8, 0x3

    .line 88
    .local v2, "index":I
    const/4 v1, 0x1

    .line 89
    .local v1, "i":I
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_0

    .line 119
    invoke-virtual {v7, p2}, Landroid/view/View;->setId(I)V

    .line 120
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    invoke-virtual {p1, v7}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    .line 121
    return-object v7

    .line 89
    .restart local p1    # "container":Landroid/view/ViewGroup;
    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 90
    .local v3, "layout":Landroid/widget/RelativeLayout;
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter$ViewHolder;

    invoke-direct {v0, v12}, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter$ViewHolder;-><init>(Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter$ViewHolder;)V

    .line 92
    .local v0, "holder":Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter$ViewHolder;
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mOptionList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-le v8, v2, :cond_1

    .line 95
    const v8, 0x7f0901f7

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 94
    iput-object v8, v0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    .line 97
    const v8, 0x7f0901f8

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 96
    iput-object v8, v0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    .line 99
    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 100
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mOptionList:Ljava/util/ArrayList;

    .line 103
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    .line 102
    invoke-direct {p0, v8}, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->getOptionBitmap(Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 104
    .local v6, "overlayedBitmap":Landroid/graphics/Bitmap;
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v8, v2, v6}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 106
    iget-object v8, v0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    invoke-virtual {v8, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 107
    iget-object v8, v0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    .line 108
    const v10, 0x7f02023c

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 109
    iget-object v10, v0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mOptionList:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    .line 110
    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionsAdapter;->mContext:Landroid/content/Context;

    .line 109
    invoke-virtual {v8, v11}, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;->getOptionText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v10, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    .end local v6    # "overlayedBitmap":Landroid/graphics/Bitmap;
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 116
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 112
    :cond_1
    const/4 v8, 0x4

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 203
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
