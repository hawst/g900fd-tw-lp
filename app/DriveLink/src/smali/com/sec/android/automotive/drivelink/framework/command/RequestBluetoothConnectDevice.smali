.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothConnectDevice;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestBluetoothConnectDevice.java"


# instance fields
.field private mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

.field private mContext:Landroid/content/Context;

.field private mIsDeviceConnected:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 21
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 23
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothConnectDevice;->mContext:Landroid/content/Context;

    .line 24
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothConnectDevice;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 25
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 55
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothConnectDevice;->mContext:Landroid/content/Context;

    .line 56
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothConnectDevice;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 57
    return-void
.end method

.method protected process()V
    .locals 4

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothConnectDevice;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v2

    .line 31
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getConnectivityManager()Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v0

    .line 34
    .local v0, "connectivityManager":Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothConnectDevice;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothConnectDevice;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 33
    invoke-virtual {v0, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->connectBluetoothDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothConnectDevice;->mIsDeviceConnected:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :goto_0
    return-void

    .line 35
    :catch_0
    move-exception v1

    .line 37
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothConnectDevice;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 46
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkConnectivityListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;

    move-result-object v0

    .line 47
    .local v0, "connectivityListener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;
    if-eqz v0, :cond_0

    .line 49
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothConnectDevice;->mIsDeviceConnected:Z

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;->onResponseRequestBluetoothConnectDevice(Z)V

    .line 51
    :cond_0
    return-void
.end method
