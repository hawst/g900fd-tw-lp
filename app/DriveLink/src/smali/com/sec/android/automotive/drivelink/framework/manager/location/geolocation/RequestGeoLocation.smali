.class public Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;
.super Ljava/lang/Object;
.source "RequestGeoLocation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;,
        Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;,
        Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$TimeOut;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "RequestGeoLocation"

.field private static final TIME_OUT:I = 0xc350


# instance fields
.field private currentLocation:Landroid/location/Location;

.field private mBaseLocationListenerGPS:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

.field private mBaseLocationListenerNetwork:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

.field private mLocationManager:Landroid/location/LocationManager;

.field private mOnResponseRequestGeolocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;

.field private mTimer:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$TimeOut;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mLocationManager:Landroid/location/LocationManager;

    .line 25
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerGPS:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    .line 26
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerNetwork:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    .line 27
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mOnResponseRequestGeolocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;

    .line 29
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mTimer:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$TimeOut;

    .line 30
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->currentLocation:Landroid/location/Location;

    .line 33
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "constructor"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mLocationManager:Landroid/location/LocationManager;

    .line 35
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerGPS:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    .line 36
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerNetwork:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    .line 37
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mOnResponseRequestGeolocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;

    .line 38
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mLocationManager:Landroid/location/LocationManager;

    .line 25
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerGPS:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    .line 26
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerNetwork:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    .line 27
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mOnResponseRequestGeolocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;

    .line 29
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mTimer:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$TimeOut;

    .line 30
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->currentLocation:Landroid/location/Location;

    .line 41
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "constructor"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mLocationManager:Landroid/location/LocationManager;

    .line 43
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerGPS:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    .line 44
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerNetwork:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    .line 45
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mOnResponseRequestGeolocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;

    .line 46
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->currentLocation:Landroid/location/Location;

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;Landroid/location/Location;Z)V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->doResponseRequestGeoLocation(Landroid/location/Location;Z)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;)Landroid/location/Location;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->currentLocation:Landroid/location/Location;

    return-object v0
.end method

.method private doResponseRequestGeoLocation(Landroid/location/Location;Z)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;
    .param p2, "timeout"    # Z

    .prologue
    .line 145
    const-string/jumbo v0, "RequestGeoLocation"

    .line 146
    const-string/jumbo v1, "doResponseRequestGeoLocation(Location location, boolean timeout)"

    .line 145
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->stopTimer()V

    .line 149
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->stopListeners()V

    .line 151
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->onResponseRequestGeoLocation(Landroid/location/Location;Z)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mOnResponseRequestGeolocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mOnResponseRequestGeolocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;->reponse(Landroid/location/Location;Z)V

    .line 155
    :cond_0
    return-void
.end method

.method private getCurrentProviderFromGPS()Ljava/lang/String;
    .locals 2

    .prologue
    .line 213
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->isGpsProviderEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "GPS is enable."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    const-string/jumbo v0, "gps"

    .line 218
    :goto_0
    return-object v0

    .line 217
    :cond_0
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "GPS is disabled."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getCurrentProviderFromNetwork()Ljava/lang/String;
    .locals 2

    .prologue
    .line 201
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "getCurrentProviderFromNetwork()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->isNetworkProviderEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "NetworkProvider is enable."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    const-string/jumbo v0, "network"

    .line 208
    :goto_0
    return-object v0

    .line 207
    :cond_0
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "NetworkProvider is disable."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isGpsProviderEnabled()Z
    .locals 2

    .prologue
    .line 231
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "isGpsProviderEnabled()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mLocationManager:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    .line 233
    const/4 v0, 0x0

    .line 235
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mLocationManager:Landroid/location/LocationManager;

    const-string/jumbo v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private isNetworkProviderEnabled()Z
    .locals 2

    .prologue
    .line 239
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "isNetworkProviderEnabled()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mLocationManager:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    .line 241
    const/4 v0, 0x0

    .line 243
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mLocationManager:Landroid/location/LocationManager;

    .line 244
    const-string/jumbo v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private isRunningGPS()Z
    .locals 2

    .prologue
    .line 191
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "isRunningGPS()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerGPS:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRunningNetwork()Z
    .locals 2

    .prologue
    .line 196
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "isRunningNetwork()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerNetwork:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private requestLocationUpdate(J)Z
    .locals 11
    .param p1, "timeout"    # J

    .prologue
    const-wide/16 v2, 0x1

    const/4 v7, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 115
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v5, "requestLocationUpdate(long timeout)"

    invoke-static {v0, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->getCurrentProviderFromNetwork()Ljava/lang/String;

    move-result-object v1

    .line 117
    .local v1, "currentProviderNetwork":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->getCurrentProviderFromGPS()Ljava/lang/String;

    move-result-object v6

    .line 119
    .local v6, "currentProviderGPS":Ljava/lang/String;
    if-nez v1, :cond_0

    if-nez v6, :cond_0

    .line 120
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v2, "All location providers are disabled."

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    const/4 v0, 0x0

    .line 141
    :goto_0
    return v0

    .line 124
    :cond_0
    if-eqz v1, :cond_1

    .line 125
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    invoke-direct {v0, p0, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerNetwork:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    .line 126
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v5, "Requesting location from Network provider."

    invoke-static {v0, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mLocationManager:Landroid/location/LocationManager;

    .line 128
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerNetwork:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    .line 127
    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 131
    :cond_1
    if-eqz v6, :cond_2

    .line 132
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    invoke-direct {v0, p0, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerGPS:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    .line 133
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v5, "Requesting location from GPS provider."

    invoke-static {v0, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mLocationManager:Landroid/location/LocationManager;

    .line 135
    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerGPS:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    move-wide v7, v2

    move v9, v4

    .line 134
    invoke-virtual/range {v5 .. v10}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 138
    :cond_2
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v2, "starting timer"

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->startTimer(J)V

    .line 141
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private startTimer(J)V
    .locals 5
    .param p1, "timeout"    # J

    .prologue
    .line 158
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "startTimer(long timeout)"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$TimeOut;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$TimeOut;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mTimer:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$TimeOut;

    .line 161
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mTimer:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$TimeOut;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Long;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$TimeOut;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 162
    return-void
.end method

.method private stopListeners()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 173
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "stopListeners()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mLocationManager:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    .line 188
    :goto_0
    return-void

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerGPS:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    if-eqz v0, :cond_1

    .line 179
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerGPS:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 182
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerNetwork:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    if-eqz v0, :cond_2

    .line 183
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerNetwork:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 186
    :cond_2
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerGPS:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    .line 187
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mBaseLocationListenerNetwork:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;

    goto :goto_0
.end method

.method private stopTimer()V
    .locals 2

    .prologue
    .line 165
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "stopTimer()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mTimer:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$TimeOut;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mTimer:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$TimeOut;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$TimeOut;->cancel(Z)Z

    .line 168
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mTimer:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$TimeOut;

    .line 170
    :cond_0
    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 101
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "execute(Context context)"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const-wide/32 v0, 0xc350

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->execute(Landroid/content/Context;J)Z

    move-result v0

    return v0
.end method

.method public execute(Landroid/content/Context;J)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "timeout"    # J

    .prologue
    const/4 v1, 0x0

    .line 70
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v2, "execute"

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->isRunningGPS()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->isRunningNetwork()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    :cond_0
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "gps or network provider is running"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    const/4 v0, 0x1

    .line 97
    :goto_0
    return v0

    .line 77
    :cond_1
    if-nez p1, :cond_2

    .line 78
    const-string/jumbo v0, "RequestGeoLocation"

    .line 79
    const-string/jumbo v2, "Null Context to get GeoLocation from location providers."

    .line 78
    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 80
    goto :goto_0

    .line 84
    :cond_2
    const-string/jumbo v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 83
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mLocationManager:Landroid/location/LocationManager;

    .line 86
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mLocationManager:Landroid/location/LocationManager;

    if-nez v0, :cond_3

    .line 87
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v2, "Failed to get location manager instance."

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 88
    goto :goto_0

    .line 91
    :cond_3
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->isProviderEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 92
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v2, "All location providers are disabled."

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 93
    goto :goto_0

    .line 96
    :cond_4
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "requesting location update."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    invoke-direct {p0, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->requestLocationUpdate(J)Z

    move-result v0

    goto :goto_0
.end method

.method public isProviderEnabled(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 222
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "isProviderEnabled(Context context)"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mLocationManager:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    .line 225
    const-string/jumbo v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 224
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mLocationManager:Landroid/location/LocationManager;

    .line 227
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->isGpsProviderEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->isNetworkProviderEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onResponseRequestGeoLocation(Landroid/location/Location;Z)V
    .locals 0
    .param p1, "location"    # Landroid/location/Location;
    .param p2, "timeout"    # Z

    .prologue
    .line 58
    return-void
.end method

.method public setResponseListener(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;

    .prologue
    .line 49
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "setResponseListener"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->mOnResponseRequestGeolocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;

    .line 51
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 106
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "stop()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->stopTimer()V

    .line 109
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->stopListeners()V

    .line 111
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->setResponseListener(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;)V

    .line 112
    return-void
.end method
