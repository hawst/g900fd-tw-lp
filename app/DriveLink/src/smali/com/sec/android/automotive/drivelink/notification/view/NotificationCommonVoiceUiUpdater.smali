.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;
.super Ljava/lang/Object;
.source "NotificationCommonVoiceUiUpdater.java"

# interfaces
.implements Lcom/nuance/drivelink/DLUiUpdater;


# static fields
.field private static volatile _instance:Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;


# instance fields
.field private TAG:Ljava/lang/String;

.field private mCurrentMicState:Lcom/nuance/sample/MicState;

.field private mIsPhraseSotting:Z

.field private mNotificationMicStateListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const-string/jumbo v0, "NotificationCommonVoiceUiUpdater"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->TAG:Ljava/lang/String;

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->mNotificationMicStateListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->mIsPhraseSotting:Z

    .line 19
    return-void
.end method

.method public static getInstance()Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;
    .locals 2

    .prologue
    .line 23
    const-class v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;

    monitor-enter v1

    .line 24
    :try_start_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->_instance:Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->_instance:Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;

    .line 23
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->_instance:Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;

    return-object v0

    .line 23
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public displayError(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "displayError = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    return-void
.end method

.method public displaySystemTurn(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "displaySystemTurn = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    return-void
.end method

.method public displayUserTurn(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "displayUserTurn = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    return-void
.end method

.method public displayWidgetContent(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "displayWidgetContent = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    return-void
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMicState()Lcom/nuance/sample/MicState;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->mCurrentMicState:Lcom/nuance/sample/MicState;

    return-object v0
.end method

.method public handleUserCancel()V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public onClickable(Z)V
    .locals 0
    .param p1, "isClickable"    # Z

    .prologue
    .line 100
    return-void
.end method

.method public onDisplayMic(Z)V
    .locals 0
    .param p1, "isMicDisplayed"    # Z

    .prologue
    .line 121
    return-void
.end method

.method public onPhraseSpotterStateChanged(Z)V
    .locals 3
    .param p1, "isSpotting"    # Z

    .prologue
    .line 104
    const-string/jumbo v0, "UiUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " - PhraseSpotting : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 105
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 104
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->mIsPhraseSotting:Z

    .line 115
    return-void
.end method

.method public setOnMicStateChangeListener(Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->mNotificationMicStateListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .line 76
    return-void
.end method

.method public updateMicRMSChange(I)V
    .locals 0
    .param p1, "rmsValue"    # I

    .prologue
    .line 82
    return-void
.end method

.method public updateMicState(Lcom/nuance/sample/MicState;)V
    .locals 3
    .param p1, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->mCurrentMicState:Lcom/nuance/sample/MicState;

    .line 59
    const-string/jumbo v0, "UiUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->mNotificationMicStateListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->mNotificationMicStateListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;->onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V

    .line 64
    :cond_0
    return-void
.end method
