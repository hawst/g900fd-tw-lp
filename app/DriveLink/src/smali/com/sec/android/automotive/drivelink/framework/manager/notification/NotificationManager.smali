.class public Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;
.super Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;
.source "NotificationManager.java"


# static fields
.field static final NATIVE_MSG_APP:Ljava/lang/String; = "com.android.mms"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDLBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;

.field private mDLMMSBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;

.field private mIsRegisted:Z

.field private mMediaBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/MediaBroadcastReceiver;

.field private final mNotificationHandler:Landroid/os/Handler;

.field private mPrivateBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/PrivateModeBroadcastReceiver;

.field private mRegisterAlarm:Z

.field private mRegisterBTConnect:Z

.field private mRegisterCall:Z

.field private mRegisterLocation:Z

.field private mRegisterMsg:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 50
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;-><init>()V

    .line 35
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mDLBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;

    .line 36
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mDLMMSBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;

    .line 37
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mMediaBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/MediaBroadcastReceiver;

    .line 38
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mPrivateBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/PrivateModeBroadcastReceiver;

    .line 40
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mContext:Landroid/content/Context;

    .line 42
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterCall:Z

    .line 43
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterMsg:Z

    .line 44
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterLocation:Z

    .line 45
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterBTConnect:Z

    .line 46
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterAlarm:Z

    .line 48
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mIsRegisted:Z

    .line 398
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mNotificationHandler:Landroid/os/Handler;

    .line 51
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mDLBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;

    .line 52
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mDLMMSBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;

    .line 53
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mIsRegisted:Z

    .line 54
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    return-object v0
.end method

.method private addCallLogForRecommand(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "logType"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 330
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;

    .line 331
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;->INCOMING_TYPE:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    .line 332
    const/4 v5, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    .line 330
    invoke-direct/range {v0 .. v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;-><init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 333
    .local v0, "calllog":Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->addDLCallMessageLogData(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)V

    .line 335
    return-void
.end method


# virtual methods
.method public NotifyAlarm(Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;)V
    .locals 4
    .param p1, "alarmInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;

    .prologue
    .line 155
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterAlarm:Z

    if-nez v0, :cond_0

    .line 156
    const-string/jumbo v0, ""

    const-string/jumbo v1, "[YANG] NotifyAlarm : mRegisterAlarm == false"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :goto_0
    return-void

    .line 160
    :cond_0
    const-string/jumbo v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[YANG] NotifyAlarm] : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->getAlertTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 161
    const-string/jumbo v2, " active : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->getSNZActive()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 160
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyAlarm(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;)V

    goto :goto_0
.end method

.method public NotifyBTConnectionRequest(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;)V
    .locals 1
    .param p1, "btInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;

    .prologue
    .line 211
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterBTConnect:Z

    if-nez v0, :cond_0

    .line 216
    :goto_0
    return-void

    .line 214
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 215
    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyBTPairingRequest(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;)V

    goto :goto_0
.end method

.method public NotifyBattery(I)V
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyLowBattery(I)V

    .line 152
    return-void
.end method

.method public NotifyInCallState(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;)V
    .locals 3
    .param p1, "state"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;
    .param p2, "callInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterCall:Z

    if-nez v0, :cond_0

    .line 87
    const-string/jumbo v0, ""

    const-string/jumbo v1, "[YANG] NotifyInCallState : mRegisterCall == false"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :goto_0
    return-void

    .line 91
    :cond_0
    invoke-virtual {p2, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->setCallState(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;)V

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    .line 92
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getDisplayName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->setName(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyCallState(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo;)V

    goto :goto_0
.end method

.method public NotifyLocation(Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;)V
    .locals 3
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 183
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    .line 182
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getDisplayName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->setName(Ljava/lang/String;)V

    .line 184
    const-string/jumbo v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[YANG] NotifyLocation : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->notifyLocationMSG(Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;)V

    .line 186
    return-void
.end method

.method public NotifyLocationGroupDestinatiON_CHANGEdSent()V
    .locals 2

    .prologue
    .line 364
    const-string/jumbo v0, ""

    const-string/jumbo v1, "[YANG] NotifyLocationGroupDestinatiON_CHANGEdSent : "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 366
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyLocationGroupDestinatiON_CHANGEdSent()V

    .line 367
    return-void
.end method

.method public NotifyLocationGroupDestinationChanged(Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 2
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;
    .param p2, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 346
    const-string/jumbo v0, ""

    const-string/jumbo v1, "[YANG] NotifyLocationGroupDestinationChanged : "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 348
    invoke-interface {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyLocationGroupDestinationChanged(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 349
    return-void
.end method

.method public NotifyLocationGroupRequestIgnored()V
    .locals 2

    .prologue
    .line 492
    const-string/jumbo v0, ""

    const-string/jumbo v1, "[YANG] NotifyLocationGroupRequestIgnored : "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 494
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyLocationGroupRequestIgnored()V

    .line 495
    return-void
.end method

.method public NotifyLocationGroupShareRequest(Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 2
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;
    .param p2, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 339
    const-string/jumbo v0, ""

    const-string/jumbo v1, "[YANG] NotifyLocationGroupShareRequest : "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 341
    invoke-interface {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyLocationGroupShareRequest(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 342
    return-void
.end method

.method public NotifyLocationGroupShareRequestIgnored()V
    .locals 2

    .prologue
    .line 358
    const-string/jumbo v0, ""

    const-string/jumbo v1, "[YANG] NotifyLocationGroupShareRequestIgnored : "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 360
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyLocationGroupShareRequestIgnored()V

    .line 361
    return-void
.end method

.method public NotifyLocationGroupSharingExpired()V
    .locals 2

    .prologue
    .line 370
    const-string/jumbo v0, ""

    const-string/jumbo v1, "[YANG] NotifyLocationGroupSharingExpired : "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 372
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyLocationGroupSharingExpired()V

    .line 373
    return-void
.end method

.method public NotifyLocationGroupSharingRequestSent()V
    .locals 2

    .prologue
    .line 352
    const-string/jumbo v0, ""

    const-string/jumbo v1, "[YANG] NotifyLocationGroupSharingRequestSent : "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 354
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyLocationGroupSharingRequestSent()V

    .line 355
    return-void
.end method

.method public NotifyLocationRequest(Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;Ljava/lang/String;)V
    .locals 3
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 200
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterLocation:Z

    if-nez v0, :cond_0

    .line 208
    :goto_0
    return-void

    .line 203
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 204
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mContext:Landroid/content/Context;

    .line 205
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    .line 204
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getContactImageFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 203
    invoke-virtual {p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->setImage(Landroid/graphics/Bitmap;)V

    .line 206
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 207
    invoke-interface {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyLocationRequest(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public NotifyLocationRequestIgnored()V
    .locals 2

    .prologue
    .line 387
    const-string/jumbo v0, ""

    const-string/jumbo v1, "[YANG] NotifyLocationRequestIgnored : "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 389
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyLocationRequestIgnored()V

    .line 390
    return-void
.end method

.method public NotifyLocationRequestSent(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
    .locals 2
    .param p1, "dlContact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 376
    const-string/jumbo v0, ""

    const-string/jumbo v1, "[YANG] NotifyLocationRequestSent : "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 378
    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyLocationRequestSent(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    .line 379
    return-void
.end method

.method public NotifyLocationShare(Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;Ljava/lang/String;)V
    .locals 3
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 189
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterLocation:Z

    if-nez v0, :cond_0

    .line 197
    :goto_0
    return-void

    .line 192
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 193
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mContext:Landroid/content/Context;

    .line 194
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    .line 193
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getContactImageFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 192
    invoke-virtual {p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->setImage(Landroid/graphics/Bitmap;)V

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 196
    invoke-interface {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyLocationShare(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public NotifyLocationShareSent(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 393
    const-string/jumbo v0, ""

    const-string/jumbo v1, "[YANG] NotifyLocationShareSent : "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 395
    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyLocationShareSent(Ljava/lang/String;)V

    .line 396
    return-void
.end method

.method public NotifyLocationStopGroupSharing(Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;)V
    .locals 3
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    .prologue
    .line 483
    const-string/jumbo v0, ""

    const-string/jumbo v1, "[YANG] NotifyLocationStopGroupSharing : "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v1

    .line 486
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 487
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 488
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    .line 485
    :goto_0
    invoke-interface {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyLocationGroupSharingStopped(Ljava/lang/String;)V

    .line 489
    return-void

    .line 488
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public NotifyMSGMMS(Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;)V
    .locals 4
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    .prologue
    .line 130
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterMsg:Z

    if-nez v1, :cond_0

    .line 131
    const-string/jumbo v1, ""

    const-string/jumbo v2, "[YANG] NotifyMSGMMS : mRegisterMsg == false"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :goto_0
    return-void

    .line 134
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v1

    .line 135
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    .line 134
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getDisplayName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 136
    .local v0, "name":Ljava/lang/String;
    invoke-virtual {p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->setName(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v1

    .line 138
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mContext:Landroid/content/Context;

    .line 139
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    .line 138
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getContactImageFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 137
    invoke-virtual {p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->setImage(Landroid/graphics/Bitmap;)V

    .line 140
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->MMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    invoke-virtual {p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->setMSGType(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;)V

    .line 141
    const-string/jumbo v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[YANG] NotifyMSGMMS : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 142
    const-string/jumbo v3, " contents : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 141
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyReceivedMSG(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;)V

    .line 145
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->LOG_TYPE_MMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    .line 146
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    .line 145
    invoke-direct {p0, v1, v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->addCallLogForRecommand(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public NotifyMSGSMS(Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;)V
    .locals 4
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    .prologue
    .line 109
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterMsg:Z

    if-nez v1, :cond_0

    .line 110
    const-string/jumbo v1, ""

    const-string/jumbo v2, "[YANG] NotifyMSGSMS : mRegisterMsg == false"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    :goto_0
    return-void

    .line 113
    :cond_0
    const-string/jumbo v1, ""

    const-string/jumbo v2, "[YANG] NotifyMSGSMS setting start  "

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v1

    .line 115
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    .line 114
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getDisplayName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 116
    .local v0, "name":Ljava/lang/String;
    invoke-virtual {p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->setName(Ljava/lang/String;)V

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v1

    .line 118
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mContext:Landroid/content/Context;

    .line 119
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    .line 118
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getContactImageFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 117
    invoke-virtual {p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->setImage(Landroid/graphics/Bitmap;)V

    .line 120
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->SMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    invoke-virtual {p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->setMSGType(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;)V

    .line 121
    const-string/jumbo v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[YANG] NotifyMSGSMS : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 122
    const-string/jumbo v3, " contents : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 121
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyReceivedMSG(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;)V

    .line 125
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->LOG_TYPE_SMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    .line 126
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    .line 125
    invoke-direct {p0, v1, v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->addCallLogForRecommand(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public NotifyMirrorLinkSetup()V
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 220
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyMirrorLinkSetup()V

    .line 221
    return-void
.end method

.method public NotifyMirrorLinkShutDown()V
    .locals 1

    .prologue
    .line 224
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 225
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyMirrorLinkShutDown()V

    .line 226
    return-void
.end method

.method public NotifyOutCallState()V
    .locals 2

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterCall:Z

    if-nez v0, :cond_0

    .line 100
    const-string/jumbo v0, ""

    const-string/jumbo v1, "[YANG] NotifyOutCallState : mRegisterCall == false"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    :goto_0
    return-void

    .line 104
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 105
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyOutgoingCall()V

    goto :goto_0
.end method

.method public NotifySchedule(I)V
    .locals 4
    .param p1, "eventId"    # I

    .prologue
    .line 167
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterAlarm:Z

    if-nez v1, :cond_1

    .line 168
    const-string/jumbo v1, ""

    const-string/jumbo v2, "[YANG] NotifySchedule : mRegisterAlarm == false"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v1

    .line 173
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mContext:Landroid/content/Context;

    .line 172
    invoke-virtual {v1, v2, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getSchedule(Landroid/content/Context;I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;

    move-result-object v0

    .line 174
    .local v0, "schedule":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    if-eqz v0, :cond_0

    .line 175
    const-string/jumbo v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[YANG] NotifySchedule] : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v1

    .line 177
    invoke-interface {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifySchedule(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;)V

    goto :goto_0
.end method

.method public initialize(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->initialize(Landroid/content/Context;)Z

    .line 60
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mNotificationHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mDLBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;

    .line 61
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;

    .line 62
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mNotificationHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;-><init>(Landroid/os/Handler;)V

    .line 61
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mDLMMSBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;

    .line 63
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/MediaBroadcastReceiver;

    .line 64
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mNotificationHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/MediaBroadcastReceiver;-><init>(Landroid/os/Handler;)V

    .line 63
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mMediaBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/MediaBroadcastReceiver;

    .line 65
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/PrivateModeBroadcastReceiver;

    .line 66
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mNotificationHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/PrivateModeBroadcastReceiver;-><init>(Landroid/os/Handler;)V

    .line 65
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mPrivateBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/PrivateModeBroadcastReceiver;

    .line 68
    const/4 v0, 0x1

    return v0
.end method

.method public onNotifyLocationHasNoSamsungAccount(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;)V
    .locals 1
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    .prologue
    .line 382
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 383
    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyLocationHasNoSamsungAccount(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;)V

    .line 384
    return-void
.end method

.method public declared-synchronized registerNotification(Landroid/content/Context;ZZZZZ)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "call"    # Z
    .param p3, "msg"    # Z
    .param p4, "btConnect"    # Z
    .param p5, "locationShare"    # Z
    .param p6, "alarm"    # Z

    .prologue
    .line 231
    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mDLBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mDLMMSBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;

    if-eqz v6, :cond_0

    .line 232
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mMediaBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/MediaBroadcastReceiver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v6, :cond_1

    .line 304
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 235
    :cond_1
    :try_start_1
    const-string/jumbo v6, ""

    const-string/jumbo v7, "[YANG] registerNotification] : "

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    iput-boolean p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterCall:Z

    .line 237
    iput-boolean p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterMsg:Z

    .line 238
    iput-boolean p5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterLocation:Z

    .line 239
    iput-boolean p4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterBTConnect:Z

    .line 240
    iput-boolean p6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterAlarm:Z

    .line 241
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mContext:Landroid/content/Context;

    .line 243
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 245
    .local v1, "filter":Landroid/content/IntentFilter;
    iget-boolean v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterMsg:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v6, :cond_2

    .line 248
    :try_start_2
    invoke-static {p1}, Landroid/provider/Telephony$Sms;->getDefaultSmsPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 250
    .local v4, "nDefaultSmsApplication":Ljava/lang/String;
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x13

    if-lt v6, v7, :cond_4

    .line 251
    const-string/jumbo v6, "com.android.mms"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 252
    const-string/jumbo v6, "com.android.mms.RECEIVED_MSG"

    invoke-virtual {v1, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 270
    .end local v4    # "nDefaultSmsApplication":Ljava/lang/String;
    :cond_2
    :goto_1
    :try_start_3
    iget-boolean v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterAlarm:Z

    if-eqz v6, :cond_3

    .line 271
    const-string/jumbo v6, "com.samsung.sec.android.clockpackage.alarm.ALARM_STARTED_IN_DRIVELINK"

    invoke-virtual {v1, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 272
    const-string/jumbo v6, "com.android.calendar.SEND_ALERTINFO_ACTION"

    invoke-virtual {v1, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 276
    :cond_3
    const-string/jumbo v6, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v1, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 279
    const-string/jumbo v6, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v1, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 281
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mDLBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;

    invoke-virtual {p1, v6, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 282
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mDLBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->setContext(Landroid/content/Context;)V

    .line 284
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 287
    .local v2, "mediaFilter":Landroid/content/IntentFilter;
    const-string/jumbo v6, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v2, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 288
    const-string/jumbo v6, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v2, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 289
    const-string/jumbo v6, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    invoke-virtual {v2, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 290
    const-string/jumbo v6, "com.samsung.android.intent.action.PRIVATE_MODE_ON"

    invoke-virtual {v2, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 291
    const-string/jumbo v6, "file"

    invoke-virtual {v2, v6}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 293
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mMediaBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/MediaBroadcastReceiver;

    invoke-virtual {p1, v6, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 295
    new-instance v5, Landroid/content/IntentFilter;

    invoke-direct {v5}, Landroid/content/IntentFilter;-><init>()V

    .line 297
    .local v5, "privateModeFilter":Landroid/content/IntentFilter;
    const-string/jumbo v6, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    invoke-virtual {v5, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 299
    const-string/jumbo v6, "com.samsung.android.intent.action.PRIVATE_MODE_ON"

    invoke-virtual {v5, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 301
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mPrivateBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/PrivateModeBroadcastReceiver;

    invoke-virtual {p1, v6, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 303
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mIsRegisted:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 231
    .end local v1    # "filter":Landroid/content/IntentFilter;
    .end local v2    # "mediaFilter":Landroid/content/IntentFilter;
    .end local v5    # "privateModeFilter":Landroid/content/IntentFilter;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 254
    .restart local v1    # "filter":Landroid/content/IntentFilter;
    .restart local v4    # "nDefaultSmsApplication":Ljava/lang/String;
    :cond_4
    :try_start_4
    const-string/jumbo v6, "android.provider.Telephony.SMS_RECEIVED"

    invoke-virtual {v1, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 256
    const-string/jumbo v6, "android.provider.Telephony.WAP_PUSH_RECEIVED"

    .line 257
    const-string/jumbo v7, "application/vnd.wap.mms-message"

    .line 255
    invoke-static {v6, v7}, Landroid/content/IntentFilter;->create(Ljava/lang/String;Ljava/lang/String;)Landroid/content/IntentFilter;

    move-result-object v3

    .line 258
    .local v3, "mmsfilter":Landroid/content/IntentFilter;
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mDLMMSBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;

    invoke-virtual {p1, v6, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 259
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mDLMMSBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;->setContext(Landroid/content/Context;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 261
    .end local v3    # "mmsfilter":Landroid/content/IntentFilter;
    .end local v4    # "nDefaultSmsApplication":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 262
    .local v0, "e":Ljava/lang/Exception;
    :try_start_5
    const-string/jumbo v6, "registerNotification"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, " [YANG] Exception E :: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->unregisterNotification(Landroid/content/Context;)V

    .line 75
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mDLBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;

    .line 76
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mDLMMSBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;

    .line 77
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mMediaBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/MediaBroadcastReceiver;

    .line 78
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mIsRegisted:Z

    .line 80
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mContext:Landroid/content/Context;

    .line 82
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->terminate(Landroid/content/Context;)V

    .line 83
    return-void
.end method

.method public declared-synchronized unregisterNotification(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 307
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mIsRegisted:Z

    if-nez v0, :cond_0

    .line 308
    const-string/jumbo v0, ""

    const-string/jumbo v1, "[YANG] unregisterNotification] : mIsRegisted == false"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 326
    :goto_0
    monitor-exit p0

    return-void

    .line 312
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mIsRegisted:Z

    .line 314
    const-string/jumbo v0, ""

    const-string/jumbo v1, "[YANG] unregisterNotification] : "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mDLBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 317
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mRegisterMsg:Z

    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_1

    .line 318
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mDLMMSBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 321
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mMediaBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/MediaBroadcastReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 323
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mPrivateBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/PrivateModeBroadcastReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 325
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->mContext:Landroid/content/Context;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 307
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
