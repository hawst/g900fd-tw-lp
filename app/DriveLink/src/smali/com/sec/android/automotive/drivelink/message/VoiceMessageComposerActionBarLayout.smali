.class public Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;
.super Landroid/widget/RelativeLayout;
.source "VoiceMessageComposerActionBarLayout.java"

# interfaces
.implements Lcom/nuance/drivelink/DLUiUpdater;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$SineEaseOut;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I

.field private static final SINE_OUT:Landroid/view/animation/Interpolator;

.field private static TAG:Ljava/lang/String;

.field private static isDebug:Z


# instance fields
.field private isRestore:Z

.field private isRunning:Z

.field private mAnimator:Landroid/view/ViewPropertyAnimator;

.field private mBackBtn:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private mCurrentMicState:Lcom/nuance/sample/MicState;

.field private mDirection:Z

.field private mErrorCharSequence:Ljava/lang/CharSequence;

.field private mHiText:Landroid/widget/TextView;

.field private mIsErrorState:Z

.field private mIsMicDisplayed:Z

.field private mIsPhraseSotting:Z

.field private mIsStartQEnded:Z

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mListeningText:Landroid/widget/TextView;

.field private mMicBtn:Landroid/widget/ImageView;

.field private mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

.field private mMicLayout:Landroid/widget/RelativeLayout;

.field private mMicProcessBg:Landroid/widget/ImageView;

.field private mMicStartQBg:Landroid/widget/ImageView;

.field private mOn2DepthBtnClickListener:Landroid/view/View$OnClickListener;

.field private mOnComposerMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

.field private mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

.field private mPhraseSpotterHandler:Landroid/os/Handler;

.field private mProcessText:Landroid/widget/TextView;

.field private mRatio:F

.field private mSearchBtn:Landroid/widget/ImageButton;

.field private mSendBtn:Landroid/widget/Button;

.field private mStandByLayout:Landroid/widget/LinearLayout;

.field private mStartQAni:Landroid/view/animation/Animation;

.field private mTTSBar:Landroid/widget/ImageView;

.field private mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

.field private mTTSBarLayout:Landroid/widget/RelativeLayout;

.field private mTTSText:Landroid/widget/TextView;

.field private mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

.field private mVoiceLayout:Landroid/widget/RelativeLayout;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-string/jumbo v0, "VoiceMessageComposerActionBarLayout"

    sput-object v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->TAG:Ljava/lang/String;

    .line 43
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->isDebug:Z

    .line 437
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$SineEaseOut;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$SineEaseOut;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 96
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 49
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsPhraseSotting:Z

    .line 439
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mRatio:F

    .line 441
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->isRestore:Z

    .line 504
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mDirection:Z

    .line 505
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->isRunning:Z

    .line 623
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    .line 97
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->init(Landroid/content/Context;)V

    .line 98
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 102
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsPhraseSotting:Z

    .line 439
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mRatio:F

    .line 441
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->isRestore:Z

    .line 504
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mDirection:Z

    .line 505
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->isRunning:Z

    .line 623
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    .line 103
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->init(Landroid/content/Context;)V

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 108
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsPhraseSotting:Z

    .line 439
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mRatio:F

    .line 441
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->isRestore:Z

    .line 504
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mDirection:Z

    .line 505
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->isRunning:Z

    .line 623
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    .line 109
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->init(Landroid/content/Context;)V

    .line 110
    return-void
.end method

.method private static Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "level"    # Ljava/lang/String;
    .param p1, "sub"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 91
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->isDebug:Z

    if-eqz v0, :cond_0

    .line 92
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->TAG:Ljava/lang/String;

    invoke-static {p0, v0, p1, p2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsMicDisplayed:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;FJLandroid/view/animation/Interpolator;)V
    .locals 0

    .prologue
    .line 443
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->startScaleAnimation(FJLandroid/view/animation/Interpolator;)V

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)Z
    .locals 1

    .prologue
    .line 505
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->isRunning:Z

    return v0
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)Z
    .locals 1

    .prologue
    .line 504
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mDirection:Z

    return v0
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 504
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mDirection:Z

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)V
    .locals 0

    .prologue
    .line 508
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->runTTSBarAnimation()V

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 505
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->isRunning:Z

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSBar:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)Lcom/sec/android/automotive/drivelink/common/view/ListeningView;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsStartQEnded:Z

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)Z
    .locals 1

    .prologue
    .line 441
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->isRestore:Z

    return v0
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 441
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->isRestore:Z

    return-void
.end method

.method static synthetic access$9()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 437
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mContext:Landroid/content/Context;

    .line 114
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;->isListening()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsPhraseSotting:Z

    .line 116
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 118
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f0300d8

    .line 117
    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 120
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 121
    const v2, 0x7f090362

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 120
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    .line 122
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 123
    const v2, 0x7f090365

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    .line 122
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    .line 126
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090366

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mSendBtn:Landroid/widget/Button;

    .line 128
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 129
    const v2, 0x7f09034e

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 128
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    .line 131
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 132
    const v2, 0x7f090350

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 131
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    .line 133
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090338

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mHiText:Landroid/widget/TextView;

    .line 134
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090351

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    .line 135
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 136
    const v2, 0x7f090352

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 135
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    .line 137
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 138
    const v2, 0x7f09035a

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 137
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    .line 139
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 140
    const v2, 0x7f09035b

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 139
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    .line 141
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 142
    const v2, 0x7f09035c

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 141
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    .line 144
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090356

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    .line 145
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090353

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    .line 146
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 147
    const v2, 0x7f090354

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 146
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    .line 148
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 149
    const v2, 0x7f090355

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 148
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    .line 151
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 152
    const v2, 0x7f090358

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 151
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    .line 153
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 154
    const v2, 0x7f090359

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 153
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSBar:Landroid/widget/ImageView;

    .line 155
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mContext:Landroid/content/Context;

    .line 156
    const v2, 0x7f04001e

    .line 155
    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    .line 157
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    new-instance v2, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$2;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$2;-><init>(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 175
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mHiText:Landroid/widget/TextView;

    .line 176
    const-string/jumbo v2, "/system/fonts/Cooljazz.ttf"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->createTypefaceFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    .line 175
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 178
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/nuance/sample/OnClickMicListenerImpl;

    .line 179
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-direct {v2, v3, v4}, Lcom/nuance/sample/OnClickMicListenerImpl;-><init>(Lcom/nuance/sample/MicStateMaster;Landroid/view/View;)V

    .line 178
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->isMicDisplayed()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsMicDisplayed:Z

    .line 183
    return-void
.end method

.method private runTTSBarAnimation()V
    .locals 5

    .prologue
    const-wide/16 v3, 0x2bc

    .line 509
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    if-nez v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSBar:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    .line 511
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$4;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$4;-><init>(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 539
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$5;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$5;-><init>(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)Landroid/view/ViewPropertyAnimator;

    .line 549
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mDirection:Z

    if-eqz v0, :cond_1

    .line 550
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mContext:Landroid/content/Context;

    const/high16 v2, 0x428c0000    # 70.0f

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->dipToPixels(Landroid/content/Context;F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 551
    invoke-virtual {v0, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 555
    :goto_0
    return-void

    .line 553
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method private startScaleAnimation(FJLandroid/view/animation/Interpolator;)V
    .locals 2
    .param p1, "ratio"    # F
    .param p2, "duration"    # J
    .param p4, "interpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 446
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    if-nez v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setScaleX(F)V

    .line 448
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setScaleY(F)V

    .line 450
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    .line 451
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$3;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$3;-><init>(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 479
    :cond_0
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mRatio:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 487
    :goto_0
    return-void

    .line 482
    :cond_1
    iput p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mRatio:F

    .line 484
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 485
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 486
    sget-object v1, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method private startTTSBarAnimation()V
    .locals 1

    .prologue
    .line 558
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->isRunning:Z

    .line 559
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->runTTSBarAnimation()V

    .line 560
    return-void
.end method


# virtual methods
.method public displayError(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 216
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 218
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mErrorCharSequence:Ljava/lang/CharSequence;

    .line 221
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsErrorState:Z

    .line 222
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsStartQEnded:Z

    .line 223
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->stopTTSBarAnimation()V

    .line 226
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 227
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 228
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 232
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 233
    return-void
.end method

.method public displaySystemTurn(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 243
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 245
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 246
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02038c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 262
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->startTTSBarAnimation()V

    .line 264
    return-void
.end method

.method public displayUserTurn(Ljava/lang/CharSequence;)V
    .locals 4
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 238
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "displayUserTurn "

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "cs ="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    return-void
.end method

.method public displayWidgetContent(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 270
    return-void
.end method

.method protected getErrorCharSequence()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mErrorCharSequence:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 1

    .prologue
    .line 581
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMicState()Lcom/nuance/sample/MicState;
    .locals 4

    .prologue
    .line 371
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "getMicState "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    return-object v0
.end method

.method public handleUserCancel()V
    .locals 0

    .prologue
    .line 576
    return-void
.end method

.method public hideVoiceLayout()V
    .locals 2

    .prologue
    .line 598
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 600
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mSendBtn:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 601
    return-void
.end method

.method protected isErrorState()Z
    .locals 1

    .prologue
    .line 411
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsErrorState:Z

    return v0
.end method

.method protected isTTSState()Z
    .locals 1

    .prologue
    .line 419
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->isRunning:Z

    return v0
.end method

.method public onClickable(Z)V
    .locals 1
    .param p1, "isClickable"    # Z

    .prologue
    .line 383
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 384
    return-void
.end method

.method public onDisplayMic(Z)V
    .locals 4
    .param p1, "isMicDisplayed"    # Z

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 639
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsMicDisplayed:Z

    .line 645
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    if-ne v0, v1, :cond_0

    .line 646
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsMicDisplayed:Z

    if-eqz v0, :cond_1

    .line 647
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsErrorState:Z

    .line 648
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsStartQEnded:Z

    .line 649
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 650
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 652
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 653
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 654
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 655
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 656
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 657
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 659
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 660
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 661
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 662
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 663
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 664
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02038c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 679
    :cond_0
    :goto_0
    return-void

    .line 666
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsErrorState:Z

    .line 667
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsStartQEnded:Z

    .line 668
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 669
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 671
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 672
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 673
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 674
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 675
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 676
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onPhraseSpotterStateChanged(Z)V
    .locals 3
    .param p1, "isSpotting"    # Z

    .prologue
    .line 610
    const-string/jumbo v0, "UiUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " - PhraseSpotting : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 611
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 610
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsPhraseSotting:Z

    .line 621
    return-void
.end method

.method public setCurrentMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 212
    return-void
.end method

.method protected setMicDisplayed(Z)V
    .locals 0
    .param p1, "isMicDisplayed"    # Z

    .prologue
    .line 682
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsMicDisplayed:Z

    .line 683
    return-void
.end method

.method public setOn2DepthBtnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 387
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mOn2DepthBtnClickListener:Landroid/view/View$OnClickListener;

    .line 388
    return-void
.end method

.method public setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 3
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 391
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "setOnBackBtnClickListener "

    const-string/jumbo v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 394
    return-void
.end method

.method public setOnComposerMicStateChangeListener(Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;)V
    .locals 3
    .param p1, "l"    # Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .prologue
    .line 406
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "setOnMicStateChangeListener "

    const-string/jumbo v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mOnComposerMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .line 408
    return-void
.end method

.method public setOnMicStateChangeListener(Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;)V
    .locals 3
    .param p1, "l"    # Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .prologue
    .line 377
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "setOnMicStateChangeListener "

    const-string/jumbo v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .line 379
    return-void
.end method

.method public setOnSendBtnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 397
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mSendBtn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 399
    return-void
.end method

.method public setOnSendBtnEnabled(Z)V
    .locals 1
    .param p1, "isClick"    # Z

    .prologue
    .line 402
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mSendBtn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 403
    return-void
.end method

.method protected setTTSState()V
    .locals 1

    .prologue
    .line 423
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->displaySystemTurn(Ljava/lang/CharSequence;)V

    .line 424
    return-void
.end method

.method public setTTSText(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 604
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 605
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 606
    return-void
.end method

.method public showVoiceLayout()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 585
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 586
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 587
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 588
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 589
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 590
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 591
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 594
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mSendBtn:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 595
    return-void
.end method

.method protected stopTTSBarAnimation()V
    .locals 1

    .prologue
    .line 563
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_0

    .line 564
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 566
    :cond_0
    return-void
.end method

.method public updateMicRMSChange(I)V
    .locals 1
    .param p1, "rmsValue"    # I

    .prologue
    .line 431
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsStartQEnded:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->update(I)V

    .line 435
    :cond_0
    return-void
.end method

.method public updateMicState(Lcom/nuance/sample/MicState;)V
    .locals 5
    .param p1, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 274
    const-string/jumbo v0, "UiUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    .line 277
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    .line 279
    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 365
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mOnComposerMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    if-eqz v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mOnComposerMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    invoke-interface {v0, p0, p1}, Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;->onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V

    .line 367
    :cond_0
    return-void

    .line 281
    :pswitch_0
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsErrorState:Z

    .line 282
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsStartQEnded:Z

    .line 283
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 285
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->stopTTSBarAnimation()V

    .line 286
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 294
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsMicDisplayed:Z

    if-eqz v0, :cond_1

    .line 295
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 297
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 298
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 299
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 301
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 302
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02038c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 307
    :goto_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 310
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 311
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    goto :goto_0

    .line 304
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 305
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x2bc

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 316
    :pswitch_1
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsErrorState:Z

    .line 317
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->stopTTSBarAnimation()V

    .line 320
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 321
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 322
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 323
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 324
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 326
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 330
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 331
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 332
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 334
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020385

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020388

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 336
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 340
    :pswitch_2
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsErrorState:Z

    .line 341
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsStartQEnded:Z

    .line 343
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->stopTTSBarAnimation()V

    .line 344
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 345
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 347
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 348
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 349
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 351
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 353
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 354
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 355
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 356
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 357
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 358
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->play(I)V

    goto/16 :goto_0

    .line 362
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    invoke-interface {v0, p0, p1}, Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;->onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V

    goto/16 :goto_0

    .line 279
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
