.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationLocationShareIgnoreDialogView.java"


# static fields
.field private static mName:Ljava/lang/String;

.field private static mNumber:Ljava/lang/String;


# instance fields
.field private mImageView:Landroid/widget/ImageView;

.field private mNameView:Landroid/widget/TextView;

.field private mViewLocal:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mName:Ljava/lang/String;

    .line 22
    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mNumber:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 19
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mViewLocal:Landroid/view/View;

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mNameView:Landroid/widget/TextView;

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 19
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mViewLocal:Landroid/view/View;

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mNameView:Landroid/widget/TextView;

    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 55
    return-void
.end method

.method private getOptionBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # I

    .prologue
    .line 114
    const/4 v0, 0x0

    .line 116
    .local v0, "maskedBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 117
    const v2, 0x7f020204

    .line 116
    invoke-static {v1, p2, v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 119
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 120
    const v2, 0x7f020205

    .line 119
    invoke-static {v1, v0, v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageOverlay(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 68
    const-string/jumbo v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 67
    check-cast v1, Landroid/view/LayoutInflater;

    .line 70
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mViewLocal:Landroid/view/View;

    if-nez v7, :cond_0

    .line 73
    const v7, 0x7f0300ac

    .line 72
    invoke-virtual {v1, v7, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 71
    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mViewLocal:Landroid/view/View;

    .line 75
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mViewLocal:Landroid/view/View;

    const v8, 0x7f0902e6

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mImageView:Landroid/widget/ImageView;

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 77
    const v8, 0x7f0a0398

    .line 76
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 78
    .local v2, "msg":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mViewLocal:Landroid/view/View;

    .line 79
    const v8, 0x7f0902e7

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 78
    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mNameView:Landroid/widget/TextView;

    .line 81
    sget-object v7, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mName:Ljava/lang/String;

    if-eqz v7, :cond_1

    sget-object v7, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mName:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_1

    .line 82
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mName:Ljava/lang/String;

    .line 87
    .local v0, "displayName":Ljava/lang/String;
    :goto_0
    new-array v7, v10, [Ljava/lang/Object;

    aput-object v0, v7, v9

    invoke-static {v2, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 88
    .local v3, "notifcationMessage":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    const v7, 0x7f020206

    .line 89
    invoke-direct {p0, p1, v7}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->getOptionBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 91
    .local v4, "overlayedBitmap":Landroid/graphics/Bitmap;
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 94
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 95
    const v8, 0x7f0a061e

    .line 94
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 98
    .local v5, "prompt":Ljava/lang/String;
    new-array v7, v10, [Ljava/lang/Object;

    aput-object v0, v7, v9

    invoke-static {v5, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 99
    .local v6, "promptDefault":Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    .line 102
    .end local v0    # "displayName":Ljava/lang/String;
    .end local v2    # "msg":Ljava/lang/String;
    .end local v3    # "notifcationMessage":Ljava/lang/String;
    .end local v4    # "overlayedBitmap":Landroid/graphics/Bitmap;
    .end local v5    # "prompt":Ljava/lang/String;
    .end local v6    # "promptDefault":Ljava/lang/String;
    :cond_0
    new-instance v7, Landroid/os/Handler;

    invoke-direct {v7}, Landroid/os/Handler;-><init>()V

    new-instance v8, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView$1;

    invoke-direct {v8, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;)V

    .line 108
    const-wide/16 v9, 0x1388

    .line 102
    invoke-virtual {v7, v8, v9, v10}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 110
    return-void

    .line 84
    .restart local v2    # "msg":Ljava/lang/String;
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mNumber:Ljava/lang/String;

    .restart local v0    # "displayName":Ljava/lang/String;
    goto :goto_0
.end method

.method public static setContent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "phone"    # Ljava/lang/String;

    .prologue
    .line 124
    sput-object p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mName:Ljava/lang/String;

    .line 125
    sput-object p1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->mNumber:Ljava/lang/String;

    .line 126
    return-void
.end method
