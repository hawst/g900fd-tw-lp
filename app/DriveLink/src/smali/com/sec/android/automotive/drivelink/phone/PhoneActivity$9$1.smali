.class Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9$1;
.super Ljava/lang/Object;
.source "PhoneActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->onTextChanged(Ljava/lang/CharSequence;III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;

    .line 640
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseRequestContactList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 679
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    return-void
.end method

.method public onResponseRequestSearchedContactList(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 645
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    const-string/jumbo v2, "PhoneActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "search : searched! "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 646
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 645
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 648
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 649
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v2

    const/16 v3, 0x43

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setCurrentMode(I)V
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$14(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V

    .line 674
    :goto_0
    return-void

    .line 655
    :cond_0
    invoke-static {p1}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContactMatchList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 657
    .local v0, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$30(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Ljava/util/List;)V

    .line 658
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 659
    .local v1, "size":I
    const/16 v2, 0x28

    if-le v1, v2, :cond_1

    .line 660
    const/16 v1, 0x28

    .line 662
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v3

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v3

    .line 663
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 662
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$21(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Ljava/lang/String;)V

    .line 664
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v3

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v3

    .line 665
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 664
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$22(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Ljava/lang/String;)V

    .line 666
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v2

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$2(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/TextView;

    move-result-object v2

    .line 667
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 669
    const v4, 0x7f0a0244

    .line 668
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 670
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v6

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v6

    .line 671
    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    aput-object v6, v4, v5

    .line 667
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 666
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 672
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v2

    const/16 v3, 0x42

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setCurrentMode(I)V
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$14(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V

    goto/16 :goto_0
.end method
