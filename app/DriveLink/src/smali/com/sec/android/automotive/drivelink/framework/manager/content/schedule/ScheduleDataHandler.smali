.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/ScheduleDataHandler;
.super Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;
.source "ScheduleDataHandler.java"


# static fields
.field static final ATTENDEES_URI:Ljava/lang/String; = "content://com.android.calendar/attendees"

.field static final CALENDAR_URI:Ljava/lang/String; = "content://com.android.canlendar/calendars"

.field static final EVENT_URI:Ljava/lang/String; = "content://com.android.calendar/events"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;-><init>()V

    .line 23
    return-void
.end method


# virtual methods
.method public getSchedule(Landroid/content/Context;J)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    .locals 46
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "eventId"    # J

    .prologue
    .line 151
    const/16 v44, 0x0

    .line 152
    .local v44, "managedCursor":Landroid/database/Cursor;
    const/16 v45, 0x0

    .line 153
    .local v45, "schedule":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    const/16 v39, 0x0

    .line 156
    .local v39, "event":Ljava/lang/String;
    :try_start_0
    const-string/jumbo v3, "content://com.android.calendar/events"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 157
    .local v4, "eventUri":Landroid/net/Uri;
    const-string/jumbo v3, "content://com.android.calendar/attendees"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v32

    .line 159
    .local v32, "attendess":Landroid/net/Uri;
    const/16 v3, 0xd

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v7, "title"

    aput-object v7, v5, v3

    const/4 v3, 0x1

    const-string/jumbo v7, "dtstart"

    aput-object v7, v5, v3

    const/4 v3, 0x2

    const-string/jumbo v7, "dtend"

    aput-object v7, v5, v3

    const/4 v3, 0x3

    .line 160
    const-string/jumbo v7, "description"

    aput-object v7, v5, v3

    const/4 v3, 0x4

    const-string/jumbo v7, "duration"

    aput-object v7, v5, v3

    const/4 v3, 0x5

    const-string/jumbo v7, "lastDate"

    aput-object v7, v5, v3

    const/4 v3, 0x6

    const-string/jumbo v7, "eventLocation"

    aput-object v7, v5, v3

    const/4 v3, 0x7

    .line 161
    const-string/jumbo v7, "latitude"

    aput-object v7, v5, v3

    const/16 v3, 0x8

    const-string/jumbo v7, "longitude"

    aput-object v7, v5, v3

    const/16 v3, 0x9

    const-string/jumbo v7, "eventTimezone"

    aput-object v7, v5, v3

    const/16 v3, 0xa

    const-string/jumbo v7, "deleted"

    aput-object v7, v5, v3

    const/16 v3, 0xb

    const-string/jumbo v7, "_id"

    aput-object v7, v5, v3

    const/16 v3, 0xc

    .line 162
    const-string/jumbo v7, "allDay"

    aput-object v7, v5, v3

    .line 163
    .local v5, "projection":[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v7, " _id = "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v7, " AND "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 164
    const-string/jumbo v7, "visible"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v7, " = 1"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 163
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 166
    .local v6, "selection":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 167
    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 166
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v44

    .line 168
    if-eqz v44, :cond_4

    .line 169
    invoke-interface/range {v44 .. v44}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 171
    const/4 v3, 0x0

    aget-object v3, v5, v3

    move-object/from16 v0, v44

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 170
    move-object/from16 v0, v44

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 174
    .local v21, "title":Ljava/lang/String;
    const/4 v3, 0x1

    aget-object v3, v5, v3

    move-object/from16 v0, v44

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 173
    move-object/from16 v0, v44

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 172
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v33

    .line 177
    .local v33, "begineTime":J
    const/4 v3, 0x2

    aget-object v3, v5, v3

    move-object/from16 v0, v44

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 176
    move-object/from16 v0, v44

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 175
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v37

    .line 179
    .local v37, "endTime":J
    const/4 v3, 0x3

    aget-object v3, v5, v3

    move-object/from16 v0, v44

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 178
    move-object/from16 v0, v44

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 181
    .local v20, "descript":Ljava/lang/String;
    const/4 v3, 0x6

    aget-object v3, v5, v3

    move-object/from16 v0, v44

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 180
    move-object/from16 v0, v44

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 184
    .local v22, "location":Ljava/lang/String;
    const/16 v3, 0xb

    aget-object v3, v5, v3

    move-object/from16 v0, v44

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 183
    move-object/from16 v0, v44

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 182
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v35

    .line 186
    .local v35, "calendarId":I
    const-string/jumbo v3, "latitude"

    move-object/from16 v0, v44

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 185
    move-object/from16 v0, v44

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v40

    .line 188
    .local v40, "latitude":J
    const-string/jumbo v3, "longitude"

    move-object/from16 v0, v44

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 187
    move-object/from16 v0, v44

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v42

    .line 191
    .local v42, "longitude":J
    const/16 v3, 0xc

    aget-object v3, v5, v3

    move-object/from16 v0, v44

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 190
    move-object/from16 v0, v44

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 189
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v28

    .line 194
    .local v28, "allDay":I
    const/4 v3, 0x2

    new-array v9, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 195
    const-string/jumbo v7, "attendee_contact_id"

    aput-object v7, v9, v3

    const/4 v3, 0x1

    const-string/jumbo v7, "attendeeName"

    aput-object v7, v9, v3

    .line 197
    .local v9, "attendeesProjection":[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v7, " event_id = "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 199
    .local v10, "attendeesSelection":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 201
    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v8, v32

    .line 200
    invoke-virtual/range {v7 .. v12}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v31

    .line 202
    .local v31, "attendeesCursor":Landroid/database/Cursor;
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 203
    .local v23, "attendeeContactIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 205
    .local v29, "attendeeInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    invoke-interface/range {v31 .. v31}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 209
    :cond_0
    const-string/jumbo v3, "attendee_contact_id"

    move-object/from16 v0, v31

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 208
    move-object/from16 v0, v31

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v30

    .line 212
    .local v30, "attendeesContactId":I
    const-string/jumbo v3, "attendeeName"

    move-object/from16 v0, v31

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 211
    move-object/from16 v0, v31

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 214
    .local v14, "attendeeName":Ljava/lang/String;
    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 215
    new-instance v11, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 216
    move/from16 v0, v30

    int-to-long v12, v0

    const/4 v15, 0x0

    .line 217
    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-direct/range {v11 .. v17}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 215
    move-object/from16 v0, v29

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    invoke-interface/range {v31 .. v31}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 222
    .end local v14    # "attendeeName":Ljava/lang/String;
    .end local v30    # "attendeesContactId":I
    :cond_1
    invoke-interface/range {v31 .. v31}, Landroid/database/Cursor;->close()V

    .line 226
    new-instance v15, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;

    move-wide/from16 v0, p2

    long-to-int v0, v0

    move/from16 v16, v0

    .line 227
    new-instance v18, Ljava/util/Date;

    move-object/from16 v0, v18

    move-wide/from16 v1, v33

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    new-instance v19, Ljava/util/Date;

    move-object/from16 v0, v19

    move-wide/from16 v1, v37

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 228
    move-wide/from16 v0, v40

    long-to-double v0, v0

    move-wide/from16 v24, v0

    .line 229
    move-wide/from16 v0, v42

    long-to-double v0, v0

    move-wide/from16 v26, v0

    move/from16 v17, v35

    .line 226
    invoke-direct/range {v15 .. v28}, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;-><init>(IILjava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;DDI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    .end local v45    # "schedule":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    .local v15, "schedule":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    :try_start_1
    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->setAttendeesContactList(Ljava/util/ArrayList;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 237
    .end local v9    # "attendeesProjection":[Ljava/lang/String;
    .end local v10    # "attendeesSelection":Ljava/lang/String;
    .end local v20    # "descript":Ljava/lang/String;
    .end local v21    # "title":Ljava/lang/String;
    .end local v22    # "location":Ljava/lang/String;
    .end local v23    # "attendeeContactIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v28    # "allDay":I
    .end local v29    # "attendeeInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    .end local v31    # "attendeesCursor":Landroid/database/Cursor;
    .end local v33    # "begineTime":J
    .end local v35    # "calendarId":I
    .end local v37    # "endTime":J
    .end local v40    # "latitude":J
    .end local v42    # "longitude":J
    :goto_0
    if-eqz v44, :cond_2

    .line 238
    invoke-interface/range {v44 .. v44}, Landroid/database/Cursor;->close()V

    .line 239
    const/16 v44, 0x0

    .line 243
    .end local v4    # "eventUri":Landroid/net/Uri;
    .end local v5    # "projection":[Ljava/lang/String;
    .end local v6    # "selection":Ljava/lang/String;
    .end local v32    # "attendess":Landroid/net/Uri;
    :cond_2
    :goto_1
    return-object v15

    .line 234
    .end local v15    # "schedule":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    .restart local v45    # "schedule":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    :catch_0
    move-exception v36

    move-object/from16 v15, v45

    .line 235
    .end local v45    # "schedule":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    .restart local v15    # "schedule":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    .local v36, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_2
    invoke-virtual/range {v36 .. v36}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 237
    if-eqz v44, :cond_2

    .line 238
    invoke-interface/range {v44 .. v44}, Landroid/database/Cursor;->close()V

    .line 239
    const/16 v44, 0x0

    goto :goto_1

    .line 236
    .end local v15    # "schedule":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    .end local v36    # "e":Ljava/lang/Exception;
    .restart local v45    # "schedule":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    :catchall_0
    move-exception v3

    move-object/from16 v15, v45

    .line 237
    .end local v45    # "schedule":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    .restart local v15    # "schedule":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    :goto_3
    if-eqz v44, :cond_3

    .line 238
    invoke-interface/range {v44 .. v44}, Landroid/database/Cursor;->close()V

    .line 239
    const/16 v44, 0x0

    .line 241
    :cond_3
    throw v3

    .line 236
    :catchall_1
    move-exception v3

    goto :goto_3

    .line 234
    .restart local v4    # "eventUri":Landroid/net/Uri;
    .restart local v5    # "projection":[Ljava/lang/String;
    .restart local v6    # "selection":Ljava/lang/String;
    .restart local v9    # "attendeesProjection":[Ljava/lang/String;
    .restart local v10    # "attendeesSelection":Ljava/lang/String;
    .restart local v20    # "descript":Ljava/lang/String;
    .restart local v21    # "title":Ljava/lang/String;
    .restart local v22    # "location":Ljava/lang/String;
    .restart local v23    # "attendeeContactIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v28    # "allDay":I
    .restart local v29    # "attendeeInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    .restart local v31    # "attendeesCursor":Landroid/database/Cursor;
    .restart local v32    # "attendess":Landroid/net/Uri;
    .restart local v33    # "begineTime":J
    .restart local v35    # "calendarId":I
    .restart local v37    # "endTime":J
    .restart local v40    # "latitude":J
    .restart local v42    # "longitude":J
    :catch_1
    move-exception v36

    goto :goto_2

    .end local v9    # "attendeesProjection":[Ljava/lang/String;
    .end local v10    # "attendeesSelection":Ljava/lang/String;
    .end local v15    # "schedule":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    .end local v20    # "descript":Ljava/lang/String;
    .end local v21    # "title":Ljava/lang/String;
    .end local v22    # "location":Ljava/lang/String;
    .end local v23    # "attendeeContactIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v28    # "allDay":I
    .end local v29    # "attendeeInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    .end local v31    # "attendeesCursor":Landroid/database/Cursor;
    .end local v33    # "begineTime":J
    .end local v35    # "calendarId":I
    .end local v37    # "endTime":J
    .end local v40    # "latitude":J
    .end local v42    # "longitude":J
    .restart local v45    # "schedule":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    :cond_4
    move-object/from16 v15, v45

    .end local v45    # "schedule":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    .restart local v15    # "schedule":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    goto :goto_0
.end method

.method public getScheduleList(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;)Ljava/util/ArrayList;
    .locals 46
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fromDate"    # Ljava/util/Date;
    .param p3, "toDate"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    new-instance v45, Ljava/util/ArrayList;

    invoke-direct/range {v45 .. v45}, Ljava/util/ArrayList;-><init>()V

    .line 40
    .local v45, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;>;"
    const-string/jumbo v3, "content://com.android.calendar/events"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 41
    .local v4, "event":Landroid/net/Uri;
    const-string/jumbo v3, "content://com.android.calendar/attendees"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v32

    .line 43
    .local v32, "attendess":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 45
    .local v6, "eventSelection":Ljava/lang/String;
    const/16 v3, 0xb

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v7, "_id"

    aput-object v7, v5, v3

    const/4 v3, 0x1

    .line 46
    const-string/jumbo v7, "calendar_id"

    aput-object v7, v5, v3

    const/4 v3, 0x2

    .line 47
    const-string/jumbo v7, "dtstart"

    aput-object v7, v5, v3

    const/4 v3, 0x3

    const-string/jumbo v7, "dtend"

    aput-object v7, v5, v3

    const/4 v3, 0x4

    .line 48
    const-string/jumbo v7, "duration"

    aput-object v7, v5, v3

    const/4 v3, 0x5

    .line 49
    const-string/jumbo v7, "title"

    aput-object v7, v5, v3

    const/4 v3, 0x6

    .line 50
    const-string/jumbo v7, "description"

    aput-object v7, v5, v3

    const/4 v3, 0x7

    .line 51
    const-string/jumbo v7, "eventLocation"

    aput-object v7, v5, v3

    const/16 v3, 0x8

    const-string/jumbo v7, "latitude"

    aput-object v7, v5, v3

    const/16 v3, 0x9

    .line 52
    const-string/jumbo v7, "longitude"

    aput-object v7, v5, v3

    const/16 v3, 0xa

    const-string/jumbo v7, "allDay"

    aput-object v7, v5, v3

    .line 54
    .local v5, "eventProjection":[Ljava/lang/String;
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 55
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "(dtstart <= "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 56
    invoke-virtual/range {p2 .. p2}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    invoke-virtual {v3, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v7, " AND "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 57
    const-string/jumbo v7, "dtend"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v7, " >= "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 58
    invoke-virtual/range {p2 .. p2}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    invoke-virtual {v3, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v7, ")"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v7, " OR "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v7, "("

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 59
    const-string/jumbo v7, "dtstart"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v7, " <= "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 60
    invoke-virtual/range {p3 .. p3}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    invoke-virtual {v3, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v7, " AND "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 61
    const-string/jumbo v7, "dtend"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v7, " >= "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    invoke-virtual {v3, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 62
    const-string/jumbo v7, ")"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v7, " OR "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v7, "("

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v7, "dtstart"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 63
    const-string/jumbo v7, " >= "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    invoke-virtual {v3, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v7, " AND "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 64
    const-string/jumbo v7, "dtend"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v7, " <= "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    invoke-virtual {v3, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 65
    const-string/jumbo v7, ")"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 55
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 69
    :cond_0
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 70
    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 69
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v39

    .line 72
    .local v39, "eventCursor":Landroid/database/Cursor;
    invoke-interface/range {v39 .. v39}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 75
    :cond_1
    const-string/jumbo v3, "_id"

    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 74
    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v40

    .line 78
    .local v40, "eventId":I
    const-string/jumbo v3, "calendar_id"

    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 77
    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v33

    .line 80
    .local v33, "calendarId":I
    const-string/jumbo v3, "dtstart"

    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 79
    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v36

    .line 82
    .local v36, "dtstart":J
    const-string/jumbo v3, "dtend"

    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 81
    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v34

    .line 85
    .local v34, "dtend":J
    const-string/jumbo v3, "description"

    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 84
    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 87
    .local v20, "desc":Ljava/lang/String;
    const-string/jumbo v3, "title"

    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 86
    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 90
    .local v21, "title":Ljava/lang/String;
    const-string/jumbo v3, "eventLocation"

    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 89
    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 92
    .local v22, "location":Ljava/lang/String;
    const-string/jumbo v3, "latitude"

    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 91
    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v41

    .line 94
    .local v41, "latitude":J
    const-string/jumbo v3, "longitude"

    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 93
    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v43

    .line 96
    .local v43, "longitude":J
    const-string/jumbo v3, "allDay"

    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 95
    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    .line 98
    .local v28, "allDay":I
    const/4 v3, 0x2

    new-array v9, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 99
    const-string/jumbo v7, "attendee_contact_id"

    aput-object v7, v9, v3

    const/4 v3, 0x1

    const-string/jumbo v7, "attendeeName"

    aput-object v7, v9, v3

    .line 101
    .local v9, "attendeesProjection":[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v7, " event_id = "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v40

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 103
    .local v10, "attendeesSelection":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 105
    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v8, v32

    .line 104
    invoke-virtual/range {v7 .. v12}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v31

    .line 106
    .local v31, "attendeesCursor":Landroid/database/Cursor;
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 107
    .local v23, "attendeeContactIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 109
    .local v29, "attendeeInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    invoke-interface/range {v31 .. v31}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 113
    :cond_2
    const-string/jumbo v3, "attendee_contact_id"

    move-object/from16 v0, v31

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 112
    move-object/from16 v0, v31

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v30

    .line 116
    .local v30, "attendeesContactId":I
    const-string/jumbo v3, "attendeeName"

    move-object/from16 v0, v31

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 115
    move-object/from16 v0, v31

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 117
    .local v14, "attendeeName":Ljava/lang/String;
    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    new-instance v11, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 119
    move/from16 v0, v30

    int-to-long v12, v0

    const/4 v15, 0x0

    .line 120
    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-direct/range {v11 .. v17}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 118
    move-object/from16 v0, v29

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    invoke-interface/range {v31 .. v31}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 126
    .end local v14    # "attendeeName":Ljava/lang/String;
    .end local v30    # "attendeesContactId":I
    :cond_3
    invoke-interface/range {v31 .. v31}, Landroid/database/Cursor;->close()V

    .line 129
    new-instance v15, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;

    const/16 v16, 0x0

    .line 130
    new-instance v18, Ljava/util/Date;

    move-object/from16 v0, v18

    move-wide/from16 v1, v36

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    new-instance v19, Ljava/util/Date;

    move-object/from16 v0, v19

    move-wide/from16 v1, v34

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 131
    move-wide/from16 v0, v41

    long-to-double v0, v0

    move-wide/from16 v24, v0

    .line 132
    move-wide/from16 v0, v43

    long-to-double v0, v0

    move-wide/from16 v26, v0

    move/from16 v17, v33

    .line 129
    invoke-direct/range {v15 .. v28}, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;-><init>(IILjava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;DDI)V

    .line 133
    .local v15, "schedule":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->setAttendeesContactList(Ljava/util/ArrayList;)V

    .line 134
    move-object/from16 v0, v45

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    invoke-interface/range {v39 .. v39}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 139
    .end local v9    # "attendeesProjection":[Ljava/lang/String;
    .end local v10    # "attendeesSelection":Ljava/lang/String;
    .end local v15    # "schedule":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    .end local v20    # "desc":Ljava/lang/String;
    .end local v21    # "title":Ljava/lang/String;
    .end local v22    # "location":Ljava/lang/String;
    .end local v23    # "attendeeContactIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v28    # "allDay":I
    .end local v29    # "attendeeInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    .end local v31    # "attendeesCursor":Landroid/database/Cursor;
    .end local v33    # "calendarId":I
    .end local v34    # "dtend":J
    .end local v36    # "dtstart":J
    .end local v40    # "eventId":I
    .end local v41    # "latitude":J
    .end local v43    # "longitude":J
    :cond_4
    invoke-interface/range {v39 .. v39}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    .end local v39    # "eventCursor":Landroid/database/Cursor;
    :goto_0
    return-object v45

    .line 142
    :catch_0
    move-exception v38

    .line 143
    .local v38, "e":Ljava/lang/Exception;
    invoke-virtual/range {v38 .. v38}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public initialize(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    const/4 v0, 0x0

    return v0
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    return-void
.end method
