.class public Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "UpdateProgressActivity.java"


# static fields
.field public static final MSG_FINISH:I = 0x64

.field public static bRequest:Z


# instance fields
.field private DLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private TAG:Ljava/lang/String;

.field UpdateListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mProgress:I

.field private mRemain:I

.field private mTotal:I

.field private progressBar:Landroid/widget/ProgressBar;

.field private txtStatus:Landroid/widget/TextView;

.field private txtprogress:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->bRequest:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    .line 36
    const-class v0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->TAG:Ljava/lang/String;

    .line 44
    iput v1, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->mRemain:I

    .line 45
    iput v1, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->mTotal:I

    .line 46
    iput v1, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->mProgress:I

    .line 143
    new-instance v0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->UpdateListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

    .line 208
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->mHandler:Landroid/os/Handler;

    .line 35
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->progressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;I)V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->setProgressData(I)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;II)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->setRemain(II)V

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->txtStatus:Landroid/widget/TextView;

    return-object v0
.end method

.method private setProgressData(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 136
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 137
    .local v0, "mask":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->txtprogress:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iput p1, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->mProgress:I

    .line 141
    return-void
.end method

.method private setRemain(II)V
    .locals 7
    .param p1, "remain"    # I
    .param p2, "total"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 119
    const v3, 0x7f090191

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 120
    .local v1, "txtremain":Landroid/widget/TextView;
    const v3, 0x7f090192

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 122
    .local v2, "txttotal":Landroid/widget/TextView;
    const v3, 0x7f0a0499

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 123
    .local v0, "mask":Ljava/lang/String;
    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    const v3, 0x7f0a049a

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 126
    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    iput p1, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->mRemain:I

    .line 129
    iput p2, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->mTotal:I

    .line 131
    return-void
.end method


# virtual methods
.method protected CarAppFinishFunc()V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "CarAppFinishFunc event "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->DLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    invoke-interface {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestCancelUpdateApplication(Landroid/content/Context;)V

    .line 112
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->CarAppFinishFunc()V

    .line 114
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onBackPressed... "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->DLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    invoke-interface {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestCancelUpdateApplication(Landroid/content/Context;)V

    .line 101
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->bRequest:Z

    .line 102
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "bRequest : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->bRequest:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 57
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onCreate"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    const v0, 0x7f03003d

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->setContentView(I)V

    .line 60
    iput-object p0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->mContext:Landroid/content/Context;

    .line 61
    const v0, 0x7f090194

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->progressBar:Landroid/widget/ProgressBar;

    .line 62
    const v0, 0x7f090193

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->txtprogress:Landroid/widget/TextView;

    .line 63
    const v0, 0x7f090195

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->txtStatus:Landroid/widget/TextView;

    .line 66
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 65
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->DLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 67
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->bRequest:Z

    if-nez v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "requestUpdateApplication : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->bRequest:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->DLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 70
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 69
    invoke-interface {v0, v1, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestUpdateApplication(Landroid/content/Context;Z)V

    .line 71
    invoke-direct {p0, v3, v3}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->setRemain(II)V

    .line 72
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->setProgressData(I)V

    .line 73
    sput-boolean v4, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->bRequest:Z

    .line 78
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->DLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->UpdateListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkUpdateListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;)V

    .line 80
    return-void

    .line 75
    :cond_0
    iget v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->mRemain:I

    iget v1, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->mTotal:I

    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->setRemain(II)V

    .line 76
    iget v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->mProgress:I

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->setProgressData(I)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->DLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkUpdateListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;)V

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->bRequest:Z

    .line 89
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "isFinishing bRequest : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->bRequest:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    :cond_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onDestroy()V

    .line 93
    return-void
.end method
