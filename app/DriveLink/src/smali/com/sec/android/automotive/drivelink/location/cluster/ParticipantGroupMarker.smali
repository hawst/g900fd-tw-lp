.class public final Lcom/sec/android/automotive/drivelink/location/cluster/ParticipantGroupMarker;
.super Ljava/lang/Object;
.source "ParticipantGroupMarker.java"


# static fields
.field private static OFFSET:I

.field private static RADIUS:D

.field private static pi:D


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    const/high16 v0, 0x10000000

    sput v0, Lcom/sec/android/automotive/drivelink/location/cluster/ParticipantGroupMarker;->OFFSET:I

    .line 14
    const-wide v0, 0x41945f306dc9d495L    # 8.54456594471E7

    sput-wide v0, Lcom/sec/android/automotive/drivelink/location/cluster/ParticipantGroupMarker;->RADIUS:D

    .line 15
    const-wide v0, 0x400927bb2fec56d6L    # 3.1444

    sput-wide v0, Lcom/sec/android/automotive/drivelink/location/cluster/ParticipantGroupMarker;->pi:D

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cluster(Ljava/util/List;I)Ljava/util/ArrayList;
    .locals 21
    .param p1, "zoom"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/cluster/Cluster",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 74
    .local p0, "markers":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .local v11, "clusterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/cluster/Cluster<Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;>;>;"
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 78
    .local v19, "originalListCopy":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;>;"
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 83
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v12, v3, :cond_1

    .line 131
    return-object v11

    .line 78
    .end local v12    # "i":I
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;

    .line 79
    .local v17, "marker":Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 86
    .end local v17    # "marker":Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;
    .restart local v12    # "i":I
    :cond_1
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 87
    .local v18, "markerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;>;"
    add-int/lit8 v13, v12, 0x1

    .local v13, "j":I
    :goto_2
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v3

    if-lt v13, v3, :cond_2

    .line 111
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_4

    .line 112
    move-object/from16 v0, p0

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;

    .line 114
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v14

    .line 115
    .local v14, "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    new-instance v2, Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;

    .line 116
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v5, v4, 0x1

    .line 117
    invoke-virtual {v14}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v14}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v8

    move-object/from16 v4, v18

    .line 115
    invoke-direct/range {v2 .. v9}, Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;-><init>(ILjava/util/ArrayList;IDD)V

    .line 118
    .local v2, "cluster":Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;, "Lcom/sec/android/automotive/drivelink/location/cluster/Cluster<Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;>;"
    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 120
    move-object/from16 v0, p0

    invoke-interface {v0, v12}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 121
    const/4 v12, 0x0

    .line 123
    goto :goto_1

    .line 88
    .end local v2    # "cluster":Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;, "Lcom/sec/android/automotive/drivelink/location/cluster/Cluster<Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;>;"
    .end local v14    # "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    :cond_2
    move-object/from16 v0, p0

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v15

    .line 89
    .local v15, "locationI":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    move-object/from16 v0, p0

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v16

    .line 90
    .local v16, "locationJ":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    invoke-virtual {v15}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v2

    .line 91
    invoke-virtual {v15}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v4

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v6

    .line 92
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v8

    move/from16 v10, p1

    .line 90
    invoke-static/range {v2 .. v10}, Lcom/sec/android/automotive/drivelink/location/cluster/ParticipantGroupMarker;->pixelDistance(DDDDI)I

    move-result v20

    .line 94
    .local v20, "pixelDistance":I
    const/16 v3, 0x28

    move/from16 v0, v20

    if-ge v0, v3, :cond_3

    .line 96
    move-object/from16 v0, p0

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    move-object/from16 v0, p0

    invoke-interface {v0, v13}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 100
    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 101
    add-int/lit8 v13, v12, 0x1

    .line 102
    goto/16 :goto_2

    .line 103
    :cond_3
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_2

    .line 124
    .end local v15    # "locationI":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .end local v16    # "locationJ":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .end local v20    # "pixelDistance":I
    :cond_4
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_1
.end method

.method public static clusterByCoordinate(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;Ljava/util/List;I)Ljava/util/List;
    .locals 13
    .param p0, "locActual"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    .param p2, "zoom"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "markers":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 57
    .local v9, "clusterList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 68
    return-object v9

    .line 57
    :cond_1
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;

    .line 59
    .local v11, "point":Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;
    invoke-virtual {v11}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v0

    invoke-virtual {v11}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v2

    .line 60
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLatitude()D

    move-result-wide v4

    .line 61
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLongitude()D

    move-result-wide v6

    move v8, p2

    .line 58
    invoke-static/range {v0 .. v8}, Lcom/sec/android/automotive/drivelink/location/cluster/ParticipantGroupMarker;->pixelDistance(DDDDI)I

    move-result v10

    .line 63
    .local v10, "pixelDistance":I
    const/16 v0, 0x28

    if-gt v10, v0, :cond_0

    .line 64
    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static latToY(D)D
    .locals 12
    .param p0, "lat"    # D

    .prologue
    const-wide v10, 0x4066800000000000L    # 180.0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 27
    sget v0, Lcom/sec/android/automotive/drivelink/location/cluster/ParticipantGroupMarker;->OFFSET:I

    int-to-double v0, v0

    .line 28
    sget-wide v2, Lcom/sec/android/automotive/drivelink/location/cluster/ParticipantGroupMarker;->RADIUS:D

    .line 29
    sget-wide v4, Lcom/sec/android/automotive/drivelink/location/cluster/ParticipantGroupMarker;->pi:D

    mul-double/2addr v4, p0

    div-double/2addr v4, v10

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    add-double/2addr v4, v8

    .line 30
    sget-wide v6, Lcom/sec/android/automotive/drivelink/location/cluster/ParticipantGroupMarker;->pi:D

    mul-double/2addr v6, p0

    div-double/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    sub-double v6, v8, v6

    .line 29
    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    .line 28
    mul-double/2addr v2, v4

    .line 30
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 28
    div-double/2addr v2, v4

    .line 27
    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-double v0, v0

    return-wide v0
.end method

.method private static lonToX(D)D
    .locals 6
    .param p0, "lon"    # D

    .prologue
    .line 23
    sget v0, Lcom/sec/android/automotive/drivelink/location/cluster/ParticipantGroupMarker;->OFFSET:I

    int-to-double v0, v0

    sget-wide v2, Lcom/sec/android/automotive/drivelink/location/cluster/ParticipantGroupMarker;->RADIUS:D

    mul-double/2addr v2, p0

    sget-wide v4, Lcom/sec/android/automotive/drivelink/location/cluster/ParticipantGroupMarker;->pi:D

    mul-double/2addr v2, v4

    const-wide v4, 0x4066800000000000L    # 180.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-double v0, v0

    return-wide v0
.end method

.method private static pixelDistance(DDDDI)I
    .locals 14
    .param p0, "lat1"    # D
    .param p2, "lon1"    # D
    .param p4, "lat2"    # D
    .param p6, "lon2"    # D
    .param p8, "zoom"    # I

    .prologue
    .line 35
    invoke-static/range {p2 .. p3}, Lcom/sec/android/automotive/drivelink/location/cluster/ParticipantGroupMarker;->lonToX(D)D

    move-result-wide v0

    .line 36
    .local v0, "x1":D
    invoke-static {p0, p1}, Lcom/sec/android/automotive/drivelink/location/cluster/ParticipantGroupMarker;->latToY(D)D

    move-result-wide v4

    .line 38
    .local v4, "y1":D
    invoke-static/range {p6 .. p7}, Lcom/sec/android/automotive/drivelink/location/cluster/ParticipantGroupMarker;->lonToX(D)D

    move-result-wide v2

    .line 39
    .local v2, "x2":D
    invoke-static/range {p4 .. p5}, Lcom/sec/android/automotive/drivelink/location/cluster/ParticipantGroupMarker;->latToY(D)D

    move-result-wide v6

    .line 42
    .local v6, "y2":D
    sub-double v8, v0, v2

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    sub-double v10, v4, v6

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    add-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-int v8, v8

    rsub-int/lit8 v9, p8, 0x15

    .line 41
    shr-int/2addr v8, v9

    return v8
.end method
