.class Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver$2;
.super Ljava/lang/Object;
.source "SoundSettingReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver$2;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 40
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 41
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 43
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver$2;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->mContext:Landroid/content/Context;

    .line 44
    const-string/jumbo v4, "audio"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 43
    check-cast v1, Landroid/media/AudioManager;

    .line 45
    .local v1, "mAm":Landroid/media/AudioManager;
    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v2

    .line 49
    .local v2, "mCurrentSoundMode":I
    if-ne v2, v6, :cond_1

    .line 50
    # getter for: Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->access$3()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "no change in sound setting"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    .end local v1    # "mAm":Landroid/media/AudioManager;
    .end local v2    # "mCurrentSoundMode":I
    :cond_0
    :goto_0
    return-void

    .line 51
    .restart local v1    # "mAm":Landroid/media/AudioManager;
    .restart local v2    # "mCurrentSoundMode":I
    :cond_1
    if-eqz v2, :cond_2

    .line 52
    if-ne v2, v5, :cond_0

    .line 55
    :cond_2
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver$2;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 56
    const-string/jumbo v4, "car_mode_on"

    .line 54
    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 57
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 58
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver$2;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;->SoundChangeHandler:Landroid/os/Handler;

    .line 59
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 61
    :catch_0
    move-exception v0

    .line 63
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
