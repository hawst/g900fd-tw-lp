.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestDLCallMessageLogList;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestDLCallMessageLogList.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDLCallMessageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;",
            ">;"
        }
    .end annotation
.end field

.field private mFromDate:Ljava/util/Date;

.field private mType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/util/Date;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "fromDate"    # Ljava/util/Date;
    .param p4, "type"    # I

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLCallMessageLogList;->mContext:Landroid/content/Context;

    .line 14
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLCallMessageLogList;->mFromDate:Ljava/util/Date;

    .line 16
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLCallMessageLogList;->mDLCallMessageList:Ljava/util/ArrayList;

    .line 23
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLCallMessageLogList;->mContext:Landroid/content/Context;

    .line 24
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLCallMessageLogList;->mFromDate:Ljava/util/Date;

    .line 25
    iput p4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLCallMessageLogList;->mType:I

    .line 26
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 47
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLCallMessageLogList;->mContext:Landroid/content/Context;

    .line 48
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLCallMessageLogList;->mFromDate:Ljava/util/Date;

    .line 49
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLCallMessageLogList;->mDLCallMessageList:Ljava/util/ArrayList;

    .line 50
    return-void
.end method

.method protected process()V
    .locals 4

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLCallMessageLogList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 31
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLCallMessageLogList;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLCallMessageLogList;->mFromDate:Ljava/util/Date;

    iget v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLCallMessageLogList;->mType:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getDLCallMessageLogList(Landroid/content/Context;Ljava/util/Date;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 30
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLCallMessageLogList;->mDLCallMessageList:Ljava/util/ArrayList;

    .line 32
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLCallMessageLogList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 37
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkDLCallMessageLogListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDLCallMessageLogListener;

    move-result-object v0

    .line 39
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDLCallMessageLogListener;
    if-eqz v0, :cond_0

    .line 40
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLCallMessageLogList;->mDLCallMessageList:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDLCallMessageLogListener;->onResponseRequestDLCallMessageLogList(Ljava/util/ArrayList;)V

    .line 43
    :cond_0
    return-void
.end method
