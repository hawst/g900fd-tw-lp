.class public Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;
.super Ljava/lang/Object;
.source "CueRoundRect.java"


# instance fields
.field isActivate:Z

.field mAnchorPoints1:[[F

.field mAnchorPoints2:[[F

.field mColor:I

.field mCurrentScale:F

.field private mDensityRatio:F

.field mFrames:[I

.field mFramesIdx:I

.field mFrictionFrom:F

.field mFrictionTo:F

.field mHeight:F

.field mInterpolator0:Lcom/sec/android/automotive/drivelink/common/view/easing/SineEaseIn;

.field mInterpolator1:Lcom/sec/android/automotive/drivelink/common/view/easing/SineEaseOut;

.field mInterpolator2:Lcom/sec/android/automotive/drivelink/common/view/easing/SineEaseInOut;

.field mInterpolators:[Landroid/view/animation/Interpolator;

.field mLeft:F

.field mLoopCnt:I

.field mMatrix:Landroid/graphics/Matrix;

.field mMatrixArray:[F

.field mPaint:Landroid/graphics/Paint;

.field mPath:Landroid/graphics/Path;

.field mPoints:[[F

.field mScaleIdx:I

.field mStartDelayCnt:I

.field private mTag:Ljava/lang/Object;

.field mTargerScale:F

.field private mTargetLoopCnt:I

.field mTop:F

.field mWidth:F

.field mXScales:[F

.field mYScales:[F

.field updateCnt:I


# direct methods
.method public constructor <init>([[F[[F[[FFF[F[FILandroid/graphics/Path;Landroid/graphics/Paint;[IIFF)V
    .locals 6
    .param p1, "points"    # [[F
    .param p2, "anchors1"    # [[F
    .param p3, "anchors2"    # [[F
    .param p4, "x"    # F
    .param p5, "y"    # F
    .param p6, "xScales"    # [F
    .param p7, "yScales"    # [F
    .param p8, "color"    # I
    .param p9, "path"    # Landroid/graphics/Path;
    .param p10, "paint"    # Landroid/graphics/Paint;
    .param p11, "frames"    # [I
    .param p12, "startDelayCnt"    # I
    .param p13, "rotation"    # F
    .param p14, "densityRatio"    # F

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mScaleIdx:I

    .line 29
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFramesIdx:I

    .line 30
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mStartDelayCnt:I

    .line 31
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mLoopCnt:I

    .line 50
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->isActivate:Z

    .line 132
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->updateCnt:I

    .line 133
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFrictionFrom:F

    .line 134
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFrictionTo:F

    .line 59
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mPoints:[[F

    .line 60
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mAnchorPoints1:[[F

    .line 61
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mAnchorPoints2:[[F

    .line 62
    iput p4, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mLeft:F

    .line 63
    iput p5, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mTop:F

    .line 64
    iput-object p6, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mXScales:[F

    .line 65
    iput-object p7, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mYScales:[F

    .line 67
    move/from16 v0, p14

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mDensityRatio:F

    .line 69
    iput p8, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mColor:I

    .line 70
    iput-object p9, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mPath:Landroid/graphics/Path;

    .line 71
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mPaint:Landroid/graphics/Paint;

    .line 73
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFrames:[I

    .line 74
    move/from16 v0, p12

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mStartDelayCnt:I

    .line 76
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mMatrix:Landroid/graphics/Matrix;

    .line 77
    const/16 v2, 0x9

    new-array v2, v2, [F

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mMatrixArray:[F

    .line 79
    new-instance v2, Lcom/sec/android/automotive/drivelink/common/view/easing/SineEaseIn;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/common/view/easing/SineEaseIn;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mInterpolator0:Lcom/sec/android/automotive/drivelink/common/view/easing/SineEaseIn;

    .line 80
    new-instance v2, Lcom/sec/android/automotive/drivelink/common/view/easing/SineEaseOut;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/common/view/easing/SineEaseOut;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mInterpolator1:Lcom/sec/android/automotive/drivelink/common/view/easing/SineEaseOut;

    .line 81
    new-instance v2, Lcom/sec/android/automotive/drivelink/common/view/easing/SineEaseInOut;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/common/view/easing/SineEaseInOut;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mInterpolator2:Lcom/sec/android/automotive/drivelink/common/view/easing/SineEaseInOut;

    .line 82
    const/4 v2, 0x4

    new-array v2, v2, [Landroid/view/animation/Interpolator;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mInterpolators:[Landroid/view/animation/Interpolator;

    .line 83
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mInterpolators:[Landroid/view/animation/Interpolator;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mInterpolator0:Lcom/sec/android/automotive/drivelink/common/view/easing/SineEaseIn;

    aput-object v4, v2, v3

    .line 84
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mInterpolators:[Landroid/view/animation/Interpolator;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mInterpolator2:Lcom/sec/android/automotive/drivelink/common/view/easing/SineEaseInOut;

    aput-object v4, v2, v3

    .line 85
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mInterpolators:[Landroid/view/animation/Interpolator;

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mInterpolator2:Lcom/sec/android/automotive/drivelink/common/view/easing/SineEaseInOut;

    aput-object v4, v2, v3

    .line 86
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mInterpolators:[Landroid/view/animation/Interpolator;

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mInterpolator2:Lcom/sec/android/automotive/drivelink/common/view/easing/SineEaseInOut;

    aput-object v4, v2, v3

    .line 88
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p1

    if-lt v1, v2, :cond_0

    .line 103
    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mLeft:F

    iget v3, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mDensityRatio:F

    mul-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mLeft:F

    .line 104
    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mTop:F

    iget v3, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mDensityRatio:F

    mul-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mTop:F

    .line 106
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mPaint:Landroid/graphics/Paint;

    iget v3, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mColor:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 108
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->setDimenstion([[F)V

    .line 110
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->isActivate:Z

    .line 111
    return-void

    .line 89
    :cond_0
    aget-object v2, p1, v1

    const/4 v3, 0x0

    aget-object v4, p1, v1

    const/4 v5, 0x0

    aget v4, v4, v5

    iget v5, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mLeft:F

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mDensityRatio:F

    mul-float/2addr v4, v5

    aput v4, v2, v3

    .line 90
    aget-object v2, p1, v1

    const/4 v3, 0x1

    aget-object v4, p1, v1

    const/4 v5, 0x1

    aget v4, v4, v5

    iget v5, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mTop:F

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mDensityRatio:F

    mul-float/2addr v4, v5

    aput v4, v2, v3

    .line 91
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    .line 92
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mAnchorPoints1:[[F

    aget-object v2, v2, v1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mAnchorPoints1:[[F

    aget-object v4, v4, v1

    const/4 v5, 0x0

    aget v4, v4, v5

    iget v5, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mLeft:F

    sub-float/2addr v4, v5

    .line 93
    iget v5, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mDensityRatio:F

    mul-float/2addr v4, v5

    .line 92
    aput v4, v2, v3

    .line 94
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mAnchorPoints1:[[F

    aget-object v2, v2, v1

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mAnchorPoints1:[[F

    aget-object v4, v4, v1

    const/4 v5, 0x1

    aget v4, v4, v5

    iget v5, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mTop:F

    sub-float/2addr v4, v5

    .line 95
    iget v5, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mDensityRatio:F

    mul-float/2addr v4, v5

    .line 94
    aput v4, v2, v3

    .line 96
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mAnchorPoints2:[[F

    aget-object v2, v2, v1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mAnchorPoints2:[[F

    aget-object v4, v4, v1

    const/4 v5, 0x0

    aget v4, v4, v5

    iget v5, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mLeft:F

    sub-float/2addr v4, v5

    .line 97
    iget v5, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mDensityRatio:F

    mul-float/2addr v4, v5

    .line 96
    aput v4, v2, v3

    .line 98
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mAnchorPoints2:[[F

    aget-object v2, v2, v1

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mAnchorPoints2:[[F

    aget-object v4, v4, v1

    const/4 v5, 0x1

    aget v4, v4, v5

    iget v5, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mTop:F

    sub-float/2addr v4, v5

    .line 99
    iget v5, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mDensityRatio:F

    mul-float/2addr v4, v5

    .line 98
    aput v4, v2, v3

    .line 88
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0
.end method

.method private setDimenstion([[F)V
    .locals 5
    .param p1, "points"    # [[F

    .prologue
    .line 229
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-lt v0, v3, :cond_0

    .line 240
    return-void

    .line 230
    :cond_0
    aget-object v3, p1, v0

    const/4 v4, 0x0

    aget v2, v3, v4

    .line 231
    .local v2, "maxW":F
    aget-object v3, p1, v0

    const/4 v4, 0x1

    aget v1, v3, v4

    .line 233
    .local v1, "maxH":F
    iget v3, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mWidth:F

    cmpl-float v3, v2, v3

    if-lez v3, :cond_1

    .line 234
    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mWidth:F

    .line 236
    :cond_1
    iget v3, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mHeight:F

    cmpl-float v3, v1, v3

    if-lez v3, :cond_2

    .line 237
    iput v1, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mHeight:F

    .line 229
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 19
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 170
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFrictionFrom:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFrictionTo:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFrictionFrom:F

    sub-float/2addr v2, v3

    const v3, 0x3e4ccccd    # 0.2f

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFrictionFrom:F

    .line 171
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mLoopCnt:I

    add-int/lit8 v1, v1, 0x2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mTargetLoopCnt:I

    if-ge v1, v2, :cond_3

    .line 172
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFrictionFrom:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mLoopCnt:I

    .line 179
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mLoopCnt:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mStartDelayCnt:I

    if-lt v1, v2, :cond_2

    .line 180
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mLoopCnt:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFrames:[I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFramesIdx:I

    aget v2, v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mStartDelayCnt:I

    add-int/2addr v2, v3

    if-gt v1, v2, :cond_2

    .line 181
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mLoopCnt:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mStartDelayCnt:I

    sub-int v12, v1, v2

    .line 182
    .local v12, "loopCnt":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mInterpolators:[Landroid/view/animation/Interpolator;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFramesIdx:I

    aget-object v1, v1, v2

    .line 183
    int-to-float v2, v12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFrames:[I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFramesIdx:I

    aget v3, v3, v4

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-interface {v1, v2}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v10

    .line 185
    .local v10, "factor":F
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mScaleIdx:I

    add-int/lit8 v13, v1, 0x1

    .line 186
    .local v13, "nextScaleIdx":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mXScales:[F

    array-length v1, v1

    if-lt v13, v1, :cond_1

    .line 187
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mXScales:[F

    array-length v1, v1

    add-int/lit8 v13, v1, -0x1

    .line 189
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mXScales:[F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mScaleIdx:I

    aget v1, v1, v2

    .line 190
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mXScales:[F

    aget v2, v2, v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mXScales:[F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mScaleIdx:I

    aget v3, v3, v4

    sub-float/2addr v2, v3

    mul-float/2addr v2, v10

    .line 189
    add-float v15, v1, v2

    .line 191
    .local v15, "scaleXFactor":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mYScales:[F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mScaleIdx:I

    aget v1, v1, v2

    .line 192
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mYScales:[F

    aget v2, v2, v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mYScales:[F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mScaleIdx:I

    aget v3, v3, v4

    sub-float/2addr v2, v3

    mul-float/2addr v2, v10

    .line 191
    add-float v17, v1, v2

    .line 194
    .local v17, "scaleYFactor":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 196
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mWidth:F

    neg-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mHeight:F

    neg-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    .line 197
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mMatrix:Landroid/graphics/Matrix;

    move/from16 v0, v17

    invoke-virtual {v1, v15, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 198
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mWidth:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mLeft:F

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mHeight:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mTop:F

    add-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 200
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mMatrixArray:[F

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->getValues([F)V

    .line 201
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mMatrixArray:[F

    const/4 v2, 0x0

    aget v14, v1, v2

    .line 202
    .local v14, "scaleX":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mMatrixArray:[F

    const/4 v2, 0x4

    aget v16, v1, v2

    .line 203
    .local v16, "scaleY":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mMatrixArray:[F

    const/4 v2, 0x2

    aget v8, v1, v2

    .line 204
    .local v8, "dx":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mMatrixArray:[F

    const/4 v2, 0x5

    aget v9, v1, v2

    .line 206
    .local v9, "dy":F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 207
    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 209
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mPath:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 211
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mPoints:[[F

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x0

    aget v2, v2, v3

    mul-float/2addr v2, v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mPoints:[[F

    const/4 v4, 0x0

    aget-object v3, v3, v4

    const/4 v4, 0x1

    aget v3, v3, v4

    mul-float v3, v3, v16

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 212
    const/4 v11, 0x1

    .local v11, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mPoints:[[F

    array-length v1, v1

    if-lt v11, v1, :cond_4

    .line 220
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mPath:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 222
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 223
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 225
    .end local v8    # "dx":F
    .end local v9    # "dy":F
    .end local v10    # "factor":F
    .end local v11    # "i":I
    .end local v12    # "loopCnt":I
    .end local v13    # "nextScaleIdx":I
    .end local v14    # "scaleX":F
    .end local v15    # "scaleXFactor":F
    .end local v16    # "scaleY":F
    .end local v17    # "scaleYFactor":F
    :cond_2
    return-void

    .line 175
    :cond_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mLoopCnt:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mTargetLoopCnt:I

    if-le v1, v2, :cond_0

    .line 176
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mLoopCnt:I

    add-int/lit8 v1, v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mLoopCnt:I

    goto/16 :goto_0

    .line 213
    .restart local v8    # "dx":F
    .restart local v9    # "dy":F
    .restart local v10    # "factor":F
    .restart local v11    # "i":I
    .restart local v12    # "loopCnt":I
    .restart local v13    # "nextScaleIdx":I
    .restart local v14    # "scaleX":F
    .restart local v15    # "scaleXFactor":F
    .restart local v16    # "scaleY":F
    .restart local v17    # "scaleYFactor":F
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mAnchorPoints1:[[F

    add-int/lit8 v3, v11, -0x1

    aget-object v2, v2, v3

    const/4 v3, 0x0

    aget v2, v2, v3

    mul-float/2addr v2, v14

    .line 214
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mAnchorPoints1:[[F

    add-int/lit8 v4, v11, -0x1

    aget-object v3, v3, v4

    const/4 v4, 0x1

    aget v3, v3, v4

    mul-float v3, v3, v16

    .line 215
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mAnchorPoints2:[[F

    add-int/lit8 v5, v11, -0x1

    aget-object v4, v4, v5

    const/4 v5, 0x0

    aget v4, v4, v5

    mul-float/2addr v4, v14

    .line 216
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mAnchorPoints2:[[F

    add-int/lit8 v6, v11, -0x1

    aget-object v5, v5, v6

    const/4 v6, 0x1

    aget v5, v5, v6

    mul-float v5, v5, v16

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mPoints:[[F

    aget-object v6, v6, v11

    const/4 v7, 0x0

    aget v6, v6, v7

    .line 217
    mul-float/2addr v6, v14

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mPoints:[[F

    aget-object v7, v7, v11

    const/16 v18, 0x1

    aget v7, v7, v18

    mul-float v7, v7, v16

    .line 213
    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 212
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1
.end method

.method public getActivate()Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->isActivate:Z

    return v0
.end method

.method public getHeight()F
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mHeight:F

    return v0
.end method

.method public getWidth()F
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mWidth:F

    return v0
.end method

.method public restart()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 114
    iput v1, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mLoopCnt:I

    .line 115
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->isActivate:Z

    .line 116
    iput v1, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFramesIdx:I

    .line 117
    iput v1, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mScaleIdx:I

    .line 118
    return-void
.end method

.method public setTag(Ljava/lang/Object;)V
    .locals 0
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mTag:Ljava/lang/Object;

    .line 166
    return-void
.end method

.method public update(I)V
    .locals 3
    .param p1, "value"    # I

    .prologue
    .line 151
    add-int/lit8 p1, p1, -0x1e

    .line 152
    const/16 v0, 0x11

    if-ge p1, v0, :cond_0

    .line 153
    const/16 p1, 0x11

    .line 154
    :cond_0
    add-int/lit8 v0, p1, -0x11

    int-to-float v0, v0

    const/high16 v1, 0x428e0000    # 71.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFrames:[I

    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFramesIdx:I

    aget v1, v1, v2

    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mStartDelayCnt:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int p1, v0

    .line 155
    mul-int/lit8 p1, p1, 0x2

    .line 156
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFrames:[I

    iget v1, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFramesIdx:I

    aget v0, v0, v1

    iget v1, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mStartDelayCnt:I

    add-int/2addr v0, v1

    if-le p1, v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFrames:[I

    iget v1, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFramesIdx:I

    aget v0, v0, v1

    iget v1, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mStartDelayCnt:I

    add-int p1, v0, v1

    .line 159
    :cond_1
    iput p1, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mTargetLoopCnt:I

    .line 160
    int-to-float v0, p1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->mFrictionTo:F

    .line 162
    return-void
.end method
