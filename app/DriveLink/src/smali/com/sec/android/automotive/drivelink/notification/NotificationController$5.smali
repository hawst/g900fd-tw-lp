.class Lcom/sec/android/automotive/drivelink/notification/NotificationController$5;
.super Ljava/lang/Object;
.source "NotificationController.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/NotificationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/NotificationController;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$5;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    .line 276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v2, 0x0

    .line 280
    const-string/jumbo v0, "[NotificationController]"

    const-string/jumbo v1, "Disappear Animation End."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    # getter for: Lcom/sec/android/automotive/drivelink/notification/NotificationController;->nextCommand:Lcom/sec/android/automotive/drivelink/notification/INotiCommand;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->access$7()Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$5;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    # invokes: Lcom/sec/android/automotive/drivelink/notification/NotificationController;->removeNotification(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->access$3(Lcom/sec/android/automotive/drivelink/notification/NotificationController;Z)V

    .line 283
    # getter for: Lcom/sec/android/automotive/drivelink/notification/NotificationController;->nextCommand:Lcom/sec/android/automotive/drivelink/notification/INotiCommand;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->access$7()Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$5;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->access$4(Lcom/sec/android/automotive/drivelink/notification/NotificationController;)Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/INotiCommand;->action(Landroid/content/Context;)V

    .line 284
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->access$1(Lcom/sec/android/automotive/drivelink/notification/INotiCommand;)V

    .line 288
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$5;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    iput-boolean v2, v0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->bEndAnimationStarted:Z

    .line 289
    const-string/jumbo v0, "[NotificationController]"

    const-string/jumbo v1, "Animation End..."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    return-void

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$5;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/notification/NotificationController;->removeNotification(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->access$3(Lcom/sec/android/automotive/drivelink/notification/NotificationController;Z)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 296
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 301
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$5;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->bEndAnimationStarted:Z

    .line 302
    return-void
.end method
