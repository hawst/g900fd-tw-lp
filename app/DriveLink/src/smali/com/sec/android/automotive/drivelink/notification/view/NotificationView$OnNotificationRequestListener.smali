.class public interface abstract Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;
.super Ljava/lang/Object;
.source "NotificationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnNotificationRequestListener"
.end annotation


# virtual methods
.method public abstract onRemoveDirect(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V
.end method

.method public abstract onRemoveRequest(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
.end method

.method public abstract onRemoveRequest(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Lcom/sec/android/automotive/drivelink/notification/INotiCommand;)V
.end method

.method public abstract onSetVoiceFlowIdRequest(Ljava/lang/String;)V
.end method
