.class public Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;
.super Landroid/os/Handler;
.source "HomeMultiCheckHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static isDebug:Z


# instance fields
.field MyNaviClassName:Ljava/lang/String;

.field MyNaviPackageName:Ljava/lang/String;

.field mComtext:Landroid/content/Context;

.field private mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;

    .line 20
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 19
    sput-object v0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->TAG:Ljava/lang/String;

    .line 122
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->isDebug:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->MyNaviPackageName:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->MyNaviClassName:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->mComtext:Landroid/content/Context;

    .line 25
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 29
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->mComtext:Landroid/content/Context;

    .line 30
    return-void
.end method

.method private static Debug(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "sub"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 125
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->isDebug:Z

    if-eqz v0, :cond_0

    .line 126
    const-string/jumbo v0, "i"

    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->TAG:Ljava/lang/String;

    invoke-static {v0, v1, p0, p1}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :cond_0
    return-void
.end method

.method private startMultiWindowNavi(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 113
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 114
    .local v0, "itMulti":Landroid/content/Intent;
    const/high16 v1, 0x100000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 115
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->MyNaviPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->MyNaviClassName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    sget v1, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->ZONE_B:I

    const/4 v2, 0x0

    .line 116
    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->makeMultiWindowIntent(Landroid/content/Intent;ILandroid/graphics/Rect;)Landroid/content/Intent;

    .line 118
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startMultiWindowNavi"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 120
    return-void
.end method

.method private startMultiWindowWithIndicator(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 104
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 105
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 108
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startMultiWindowWithIndicator"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 110
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 35
    const/4 v0, 0x0

    .line 38
    .local v0, "isMultiMode":Z
    new-instance v2, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 39
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->mComtext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    invoke-direct {v2, v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    .line 38
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 42
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v1, :cond_0

    .line 43
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v0

    .line 44
    const-string/jumbo v1, "handleMessage"

    .line 45
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "isMultiwindow ="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 44
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->Debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    :cond_0
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->isNavigation()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 49
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->mComtext:Landroid/content/Context;

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->startMultiWindowWithIndicator(Landroid/content/Context;)V

    .line 50
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->mComtext:Landroid/content/Context;

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->startMultiWindowNavi(Landroid/content/Context;)V

    .line 53
    :cond_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 54
    return-void
.end method

.method isNavigation()Z
    .locals 8

    .prologue
    .line 57
    const/4 v2, 0x0

    .line 59
    .local v2, "rtn":Z
    const/4 v1, 0x0

    .line 69
    .local v1, "i":I
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->mComtext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 70
    const-string/jumbo v6, "activity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 69
    check-cast v0, Landroid/app/ActivityManager;

    .line 71
    .local v0, "am":Landroid/app/ActivityManager;
    if-eqz v0, :cond_0

    .line 72
    const/16 v5, 0x64

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v4

    .line 74
    .local v4, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v4, :cond_0

    .line 75
    const-string/jumbo v5, "isNavigation"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "task size = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->Debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const/4 v1, 0x0

    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-lt v1, v5, :cond_1

    .line 99
    .end local v4    # "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_0
    :goto_1
    return v2

    .line 78
    .restart local v4    # "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_1
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v5, v5, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 80
    .local v3, "strTopPackage":Ljava/lang/String;
    const-string/jumbo v5, "isNavigation"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "task["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "]= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->Debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/map/MapFactory;->newNavigationMap()Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    move-result-object v5

    .line 83
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->getNaviPackageName()Ljava/lang/String;

    move-result-object v5

    .line 82
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->MyNaviPackageName:Ljava/lang/String;

    .line 84
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->MyNaviPackageName:Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 85
    const-string/jumbo v5, "isNavigation"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "MyNavi ="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->MyNaviPackageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->Debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    if-eqz v3, :cond_2

    .line 87
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->MyNaviPackageName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_2

    .line 88
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v5, v5, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 89
    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v5

    .line 88
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/home/HomeMultiCheckHandler;->MyNaviClassName:Ljava/lang/String;

    .line 90
    const/4 v2, 0x1

    .line 92
    goto :goto_1

    .line 77
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
