.class Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$2;
.super Ljava/lang/Object;
.source "MyPlaceCarSimpleCursorAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 131
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .line 133
    .local v4, "parentView":Landroid/view/View;
    const v8, 0x7f09024d

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 132
    check-cast v7, Landroid/widget/TextView;

    .line 135
    .local v7, "tvSettingsCarName":Landroid/widget/TextView;
    const v8, 0x7f09024f

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 134
    check-cast v5, Landroid/widget/TextView;

    .line 137
    .local v5, "tvSettingsCarId":Landroid/widget/TextView;
    const v8, 0x7f09024e

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 136
    check-cast v6, Landroid/widget/TextView;

    .line 138
    .local v6, "tvSettingsCarMacAddress":Landroid/widget/TextView;
    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 139
    .local v1, "btName":Ljava/lang/String;
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    .line 140
    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    .line 139
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 141
    .local v2, "carId":I
    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    .line 142
    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 143
    .local v0, "btAddress":Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    .line 144
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;
    invoke-static {v8}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->access$0(Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    move-result-object v8

    .line 145
    const-class v9, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    .line 143
    invoke-direct {v3, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 147
    .local v3, "intent":Landroid/content/Intent;
    const-string/jumbo v8, "EXTRA_NAME_DEVICE_ADDRESS"

    .line 146
    invoke-virtual {v3, v8, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 150
    const-string/jumbo v8, "EXTRA_NAME_DEVICE_NAME"

    .line 149
    invoke-virtual {v3, v8, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    const-string/jumbo v8, "EXTRA_NAME_DEVICE_ID"

    .line 152
    invoke-virtual {v3, v8, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 155
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;
    invoke-static {v8}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->access$0(Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    move-result-object v8

    .line 156
    invoke-virtual {v8, v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->startActivity(Landroid/content/Intent;)V

    .line 157
    return-void
.end method
