.class Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$8;
.super Ljava/lang/Object;
.source "LocationRequestActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$OnLocationSipStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->getLocationSipStateListener()Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$OnLocationSipStateListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$location$EditTextForLocationSIP$LocationSIPState:[I


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$location$EditTextForLocationSIP$LocationSIPState()[I
    .locals 3

    .prologue
    .line 362
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$8;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$location$EditTextForLocationSIP$LocationSIPState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;->values()[Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;->SIP_CLOSE:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;->SIP_SHOW:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$8;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$location$EditTextForLocationSIP$LocationSIPState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    .line 362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnStateChanged(Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;)V
    .locals 3
    .param p1, "state"    # Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    .prologue
    .line 366
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$17(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;)V

    .line 368
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$8;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$location$EditTextForLocationSIP$LocationSIPState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 393
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 376
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mTextfieldLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$18(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 379
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    .line 380
    const-wide/16 v1, 0x258

    invoke-virtual {v0, v1, v2}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    .line 381
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mTextfieldLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$18(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 382
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->showVoiceLayout()V

    .line 383
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mSearchMode:Z

    .line 384
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mSearchedText:Ljava/lang/String;

    goto :goto_0

    .line 368
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
