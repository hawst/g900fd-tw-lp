.class Lcom/sec/android/automotive/drivelink/location/LocationActivity$8;
.super Ljava/lang/Object;
.source "LocationActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationActivity;->initSearchTextField()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    .line 549
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x2

    .line 552
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$1(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->setCursorVisible(Z)V

    .line 553
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 554
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    const-string/jumbo v1, "JINSEIL"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "isInputMethodShown()="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-nez v1, :cond_0

    .line 557
    invoke-virtual {v0, v4, v4}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    .line 561
    :cond_0
    const/4 v1, 0x0

    return v1
.end method
