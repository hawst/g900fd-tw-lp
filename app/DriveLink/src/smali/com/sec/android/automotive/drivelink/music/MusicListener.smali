.class public interface abstract Lcom/sec/android/automotive/drivelink/music/MusicListener;
.super Ljava/lang/Object;
.source "MusicListener.java"


# virtual methods
.method public abstract OnBTBtnCallingState()V
.end method

.method public abstract OnBTBtnClicked()V
.end method

.method public abstract OnBTBtnPlay()V
.end method

.method public abstract OnBTBtnSeek()V
.end method

.method public abstract OnError(I)V
.end method

.method public abstract OnFinished()V
.end method

.method public abstract OnSeekComplete()V
.end method

.method public abstract OnSetVolumeControl(Z)V
.end method

.method public abstract OnShuffle(Z)V
.end method

.method public abstract OnVolumeControlCommand(I)V
.end method

.method public abstract onMusicChanged(Z)V
.end method
