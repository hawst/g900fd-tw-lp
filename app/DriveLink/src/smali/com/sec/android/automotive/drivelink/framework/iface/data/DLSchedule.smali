.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLSchedule.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getAllDay()I
.end method

.method public abstract getAttendeesContactList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAttendeesList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCalendarId()I
.end method

.method public abstract getDescription()Ljava/lang/String;
.end method

.method public abstract getDuration()I
.end method

.method public abstract getEndDate()Ljava/util/Date;
.end method

.method public abstract getId()I
.end method

.method public abstract getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
.end method

.method public abstract getStartDate()Ljava/util/Date;
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract setAttendeesContactList(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation
.end method
