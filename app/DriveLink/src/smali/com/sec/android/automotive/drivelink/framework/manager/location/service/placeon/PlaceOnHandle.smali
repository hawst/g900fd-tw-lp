.class public Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;
.super Ljava/lang/Object;
.source "PlaceOnHandle.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "PlaceOnHandle"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static addFriendsToGroup(ILjava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 12
    .param p0, "userId"    # I
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 576
    .local p1, "friends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    const/4 v2, 0x0

    .line 577
    .local v2, "jsonParam":Lorg/json/JSONObject;
    const/4 v4, 0x0

    .line 579
    .local v4, "jsonResponse":Lorg/json/JSONObject;
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    const/4 v9, 0x1

    if-ge v8, v9, :cond_1

    .line 580
    :cond_0
    const-string/jumbo v8, "PlaceOnHandle"

    const-string/jumbo v9, "Invalid data to add participant to shared group."

    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    const/4 v8, 0x0

    .line 634
    :goto_0
    return v8

    .line 585
    :cond_1
    :try_start_0
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 588
    .local v6, "participants":Lorg/json/JSONArray;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_2

    .line 605
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 607
    .end local v2    # "jsonParam":Lorg/json/JSONObject;
    .local v3, "jsonParam":Lorg/json/JSONObject;
    :try_start_1
    const-string/jumbo v8, "participants"

    invoke-virtual {v3, v8, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 609
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    .line 610
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 609
    invoke-static {v8, v9, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;->addFriendsToGroup(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v4

    .line 612
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isSuccessResponse(Lorg/json/JSONObject;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 613
    const-string/jumbo v9, "PlaceOnHandle"

    .line 614
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Fail to add new participant to group. Return value = "

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 615
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 614
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 616
    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    .line 613
    :goto_2
    invoke-static {v9, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 617
    const/4 v8, 0x0

    move-object v2, v3

    .end local v3    # "jsonParam":Lorg/json/JSONObject;
    .restart local v2    # "jsonParam":Lorg/json/JSONObject;
    goto :goto_0

    .line 588
    :cond_2
    :try_start_2
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 589
    .local v0, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 590
    .local v5, "participant":Lorg/json/JSONObject;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v7

    .line 592
    .local v7, "phone":Ljava/lang/String;
    if-eqz v7, :cond_3

    const-string/jumbo v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 593
    :cond_3
    const-string/jumbo v8, "PlaceOnHandle"

    .line 594
    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Invalid phone number to contact "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 595
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getFriendName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 594
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 593
    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 599
    :cond_4
    const-string/jumbo v8, "phoneNumber"

    const-string/jumbo v10, "-"

    const-string/jumbo v11, ""

    invoke-virtual {v7, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v8, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 600
    const-string/jumbo v10, "share"

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getShared()Z

    move-result v8

    if-eqz v8, :cond_5

    const-string/jumbo v8, "Y"

    :goto_3
    invoke-virtual {v5, v10, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 602
    invoke-virtual {v6, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 630
    .end local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .end local v5    # "participant":Lorg/json/JSONObject;
    .end local v6    # "participants":Lorg/json/JSONArray;
    .end local v7    # "phone":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 631
    .local v1, "je":Lorg/json/JSONException;
    :goto_4
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 634
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 600
    .end local v1    # "je":Lorg/json/JSONException;
    .restart local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .restart local v5    # "participant":Lorg/json/JSONObject;
    .restart local v6    # "participants":Lorg/json/JSONArray;
    .restart local v7    # "phone":Ljava/lang/String;
    :cond_5
    :try_start_3
    const-string/jumbo v8, "N"
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_3

    .line 616
    .end local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .end local v2    # "jsonParam":Lorg/json/JSONObject;
    .end local v5    # "participant":Lorg/json/JSONObject;
    .end local v7    # "phone":Ljava/lang/String;
    .restart local v3    # "jsonParam":Lorg/json/JSONObject;
    :cond_6
    :try_start_4
    const-string/jumbo v8, "Invalid result"

    goto :goto_2

    .line 620
    :cond_7
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_8

    .line 624
    invoke-static {p2, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->setGroupParticipantsFromJson(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lorg/json/JSONObject;)Z

    move-result v8

    if-nez v8, :cond_9

    .line 625
    const-string/jumbo v8, "PlaceOnHandle"

    const-string/jumbo v9, "Error to add Participant in group from Json"

    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 626
    const/4 v8, 0x0

    move-object v2, v3

    .end local v3    # "jsonParam":Lorg/json/JSONObject;
    .restart local v2    # "jsonParam":Lorg/json/JSONObject;
    goto/16 :goto_0

    .line 620
    .end local v2    # "jsonParam":Lorg/json/JSONObject;
    .restart local v3    # "jsonParam":Lorg/json/JSONObject;
    :cond_8
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 621
    .restart local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-virtual {p2, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->addGroupParticipant(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_5

    .line 630
    .end local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    :catch_1
    move-exception v1

    move-object v2, v3

    .end local v3    # "jsonParam":Lorg/json/JSONObject;
    .restart local v2    # "jsonParam":Lorg/json/JSONObject;
    goto :goto_4

    .line 629
    .end local v2    # "jsonParam":Lorg/json/JSONObject;
    .restart local v3    # "jsonParam":Lorg/json/JSONObject;
    :cond_9
    const/4 v8, 0x1

    move-object v2, v3

    .end local v3    # "jsonParam":Lorg/json/JSONObject;
    .restart local v2    # "jsonParam":Lorg/json/JSONObject;
    goto/16 :goto_0
.end method

.method public static createShareGroup(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 14
    .param p0, "profile"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .param p1, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 145
    const/4 v3, 0x0

    .line 146
    .local v3, "jsonParam":Lorg/json/JSONObject;
    const/4 v5, 0x0

    .line 148
    .local v5, "jsonResponse":Lorg/json/JSONObject;
    if-nez p0, :cond_0

    .line 149
    const-string/jumbo v9, "PlaceOnHandle"

    const-string/jumbo v10, "Invalid profile user id."

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    const/4 v9, 0x0

    .line 218
    :goto_0
    return v9

    .line 153
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 154
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v10, 0x2

    if-ge v9, v10, :cond_2

    .line 155
    :cond_1
    const-string/jumbo v9, "PlaceOnHandle"

    const-string/jumbo v10, "Invalid data to create shared group."

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    const/4 v9, 0x0

    goto :goto_0

    .line 160
    :cond_2
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 161
    .end local v3    # "jsonParam":Lorg/json/JSONObject;
    .local v4, "jsonParam":Lorg/json/JSONObject;
    :try_start_1
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v9

    if-eqz v9, :cond_3

    .line 162
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 163
    .local v1, "destination":Lorg/json/JSONObject;
    const-string/jumbo v9, "name"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 164
    const-string/jumbo v9, "address"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v10

    .line 165
    invoke-virtual {v10}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v10

    .line 164
    invoke-virtual {v1, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 166
    const-string/jumbo v9, "latitude"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v10

    invoke-virtual {v1, v9, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 168
    const-string/jumbo v9, "longitude"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v10

    invoke-virtual {v1, v9, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 169
    const-string/jumbo v9, "destination"

    invoke-virtual {v4, v9, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 172
    .end local v1    # "destination":Lorg/json/JSONObject;
    :cond_3
    new-instance v7, Lorg/json/JSONArray;

    invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V

    .line 175
    .local v7, "participants":Lorg/json/JSONArray;
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_4

    .line 193
    const-string/jumbo v9, "participants"

    invoke-virtual {v4, v9, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 194
    const-string/jumbo v9, "shareType"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getShareType()I

    move-result v10

    invoke-virtual {v4, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 195
    const-string/jumbo v9, "groupType"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;->getValue()I

    move-result v10

    invoke-virtual {v4, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 197
    const-string/jumbo v9, "duration"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getDuration()J

    move-result-wide v10

    const-wide/32 v12, 0xea60

    div-long/2addr v10, v12

    invoke-virtual {v4, v9, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 199
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v9

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getSspGuid()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getCountryCode()Ljava/lang/String;

    move-result-object v11

    .line 199
    invoke-static {v9, v10, v11, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;->createShareGroup(ILjava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v5

    .line 202
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isSuccessResponse(Lorg/json/JSONObject;)Z

    move-result v9

    if-nez v9, :cond_9

    .line 203
    if-eqz v5, :cond_8

    .line 204
    const-string/jumbo v9, "PlaceOnHandle"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Fail to create new group. Return value = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 205
    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 204
    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    :goto_2
    const/4 v9, 0x0

    move-object v3, v4

    .end local v4    # "jsonParam":Lorg/json/JSONObject;
    .restart local v3    # "jsonParam":Lorg/json/JSONObject;
    goto/16 :goto_0

    .line 175
    .end local v3    # "jsonParam":Lorg/json/JSONObject;
    .restart local v4    # "jsonParam":Lorg/json/JSONObject;
    :cond_4
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 176
    .local v0, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 177
    .local v6, "participant":Lorg/json/JSONObject;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v8

    .line 179
    .local v8, "phone":Ljava/lang/String;
    if-eqz v8, :cond_5

    const-string/jumbo v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 180
    :cond_5
    const-string/jumbo v9, "PlaceOnHandle"

    .line 181
    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Invalid phone number to contact "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 182
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getFriendName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 181
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 180
    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    const/4 v9, 0x0

    move-object v3, v4

    .end local v4    # "jsonParam":Lorg/json/JSONObject;
    .restart local v3    # "jsonParam":Lorg/json/JSONObject;
    goto/16 :goto_0

    .line 186
    .end local v3    # "jsonParam":Lorg/json/JSONObject;
    .restart local v4    # "jsonParam":Lorg/json/JSONObject;
    :cond_6
    const-string/jumbo v9, "phoneNumber"

    .line 187
    invoke-static {v8}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->removeCountryCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 186
    invoke-virtual {v6, v9, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 188
    const-string/jumbo v11, "share"

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getShared()Z

    move-result v9

    if-eqz v9, :cond_7

    const-string/jumbo v9, "Y"

    :goto_3
    invoke-virtual {v6, v11, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 190
    invoke-virtual {v7, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 214
    .end local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .end local v6    # "participant":Lorg/json/JSONObject;
    .end local v7    # "participants":Lorg/json/JSONArray;
    .end local v8    # "phone":Ljava/lang/String;
    :catch_0
    move-exception v2

    move-object v3, v4

    .line 215
    .end local v4    # "jsonParam":Lorg/json/JSONObject;
    .local v2, "je":Lorg/json/JSONException;
    .restart local v3    # "jsonParam":Lorg/json/JSONObject;
    :goto_4
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 218
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 188
    .end local v2    # "je":Lorg/json/JSONException;
    .end local v3    # "jsonParam":Lorg/json/JSONObject;
    .restart local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .restart local v4    # "jsonParam":Lorg/json/JSONObject;
    .restart local v6    # "participant":Lorg/json/JSONObject;
    .restart local v7    # "participants":Lorg/json/JSONArray;
    .restart local v8    # "phone":Ljava/lang/String;
    :cond_7
    :try_start_2
    const-string/jumbo v9, "N"

    goto :goto_3

    .line 207
    .end local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .end local v6    # "participant":Lorg/json/JSONObject;
    .end local v8    # "phone":Ljava/lang/String;
    :cond_8
    const-string/jumbo v9, "PlaceOnHandle"

    const-string/jumbo v10, "Fail to create new group. Return value = Invalid result"

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 213
    :cond_9
    invoke-static {p1, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->parseGroupFromJSONReponse(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lorg/json/JSONObject;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v9

    move-object v3, v4

    .end local v4    # "jsonParam":Lorg/json/JSONObject;
    .restart local v3    # "jsonParam":Lorg/json/JSONObject;
    goto/16 :goto_0

    .line 214
    .end local v7    # "participants":Lorg/json/JSONArray;
    :catch_1
    move-exception v2

    goto :goto_4
.end method

.method private static findParticipantInGroupResponse(Ljava/lang/String;Lorg/json/JSONArray;)Lorg/json/JSONObject;
    .locals 5
    .param p0, "phoneNumber"    # Ljava/lang/String;
    .param p1, "participants"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 511
    if-nez p1, :cond_0

    move-object v1, v3

    .line 531
    :goto_0
    return-object v1

    .line 514
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-lt v0, v4, :cond_1

    move-object v1, v3

    .line 531
    goto :goto_0

    .line 515
    :cond_1
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 516
    .local v1, "jsonObj":Lorg/json/JSONObject;
    if-nez v1, :cond_3

    .line 514
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 519
    :cond_3
    const-string/jumbo v4, "phoneNumber"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 522
    const-string/jumbo v4, "phoneNumber"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 523
    .local v2, "phone":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 526
    invoke-static {v2, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->areEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_0
.end method

.method private static getDateTime(Ljava/lang/String;Ljava/lang/String;)J
    .locals 5
    .param p0, "date"    # Ljava/lang/String;
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 298
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 299
    const/4 v0, 0x0

    .line 301
    .local v0, "dateTime":Ljava/util/Date;
    :try_start_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "yyyyMMdd HHmmss"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 302
    .local v1, "dtf":Ljava/text/DateFormat;
    const-string/jumbo v3, "Etc/UTC"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 303
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 305
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v3

    .line 311
    .end local v0    # "dateTime":Ljava/util/Date;
    .end local v1    # "dtf":Ljava/text/DateFormat;
    :goto_0
    return-wide v3

    .line 306
    .restart local v0    # "dateTime":Ljava/util/Date;
    :catch_0
    move-exception v2

    .line 307
    .local v2, "e":Ljava/text/ParseException;
    const-string/jumbo v3, "PlaceOnHandle"

    invoke-virtual {v2}, Ljava/text/ParseException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    .end local v0    # "dateTime":Ljava/util/Date;
    .end local v2    # "e":Ljava/text/ParseException;
    :cond_0
    const-wide/16 v3, 0x0

    goto :goto_0
.end method

.method public static getGroupById(II)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .locals 8
    .param p0, "userId"    # I
    .param p1, "groupId"    # I

    .prologue
    const/4 v4, 0x0

    .line 107
    const/4 v3, 0x0

    .line 108
    .local v3, "jsonResponse":Lorg/json/JSONObject;
    const/4 v0, 0x0

    .line 110
    .local v0, "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    if-gez p0, :cond_0

    .line 111
    const-string/jumbo v5, "PlaceOnHandle"

    const-string/jumbo v6, "Invalid profile user id."

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v4

    .line 139
    :goto_0
    return-object v1

    .line 115
    :cond_0
    if-gez p1, :cond_1

    .line 116
    const-string/jumbo v5, "PlaceOnHandle"

    const-string/jumbo v6, "Invalid data to get shared group."

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v4

    .line 117
    goto :goto_0

    .line 121
    :cond_1
    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 122
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 121
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;->getGroup(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 123
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isSuccessResponse(Lorg/json/JSONObject;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 124
    const-string/jumbo v6, "PlaceOnHandle"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Fail to get group info. Return value = "

    invoke-direct {v7, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 125
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_1
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 124
    invoke-static {v6, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v4

    .line 127
    goto :goto_0

    .line 126
    :cond_2
    const-string/jumbo v5, "Invalid result"

    goto :goto_1

    .line 130
    :cond_3
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    .end local v0    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .local v1, "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    :try_start_1
    invoke-virtual {v1, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setGroupId(I)V

    .line 133
    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->parseGroupFromJSONReponse(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lorg/json/JSONObject;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    if-nez v5, :cond_4

    move-object v0, v1

    .end local v1    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .restart local v0    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    move-object v1, v4

    .line 134
    goto :goto_0

    .end local v0    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .restart local v1    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    :cond_4
    move-object v0, v1

    .line 135
    .end local v1    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .restart local v0    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    goto :goto_0

    .line 136
    :catch_0
    move-exception v2

    .line 137
    .local v2, "je":Lorg/json/JSONException;
    :goto_2
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    move-object v1, v4

    .line 139
    goto :goto_0

    .line 136
    .end local v0    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .end local v2    # "je":Lorg/json/JSONException;
    .restart local v1    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    :catch_1
    move-exception v2

    move-object v0, v1

    .end local v1    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .restart local v0    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    goto :goto_2
.end method

.method public static getGroupByUrl(ILjava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .locals 8
    .param p0, "userId"    # I
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 769
    const/4 v0, 0x0

    .line 770
    .local v0, "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    const/4 v3, 0x0

    .line 773
    .local v3, "jsonResponse":Lorg/json/JSONObject;
    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;->getGroupByUrl(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 775
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isSuccessResponse(Lorg/json/JSONObject;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 776
    const-string/jumbo v6, "PlaceOnHandle"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Fail to get group info. Return value = "

    invoke-direct {v7, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 777
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_0
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 776
    invoke-static {v6, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v4

    .line 790
    :goto_1
    return-object v1

    .line 778
    :cond_0
    const-string/jumbo v5, "Invalid result"

    goto :goto_0

    .line 782
    :cond_1
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 784
    .end local v0    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .local v1, "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    :try_start_1
    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->parseGroupFromJSONReponse(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lorg/json/JSONObject;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v1

    .line 785
    .end local v1    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .restart local v0    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    goto :goto_1

    .line 786
    :catch_0
    move-exception v2

    .line 787
    .local v2, "je":Lorg/json/JSONException;
    :goto_2
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .end local v2    # "je":Lorg/json/JSONException;
    :goto_3
    move-object v1, v4

    .line 790
    goto :goto_1

    .line 786
    .end local v0    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .restart local v1    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    :catch_1
    move-exception v2

    move-object v0, v1

    .end local v1    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .restart local v0    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    goto :goto_2

    .end local v0    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .restart local v1    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    :cond_2
    move-object v0, v1

    .end local v1    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .restart local v0    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    goto :goto_3
.end method

.method public static getGroupShared(ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 6
    .param p0, "userId"    # I
    .param p1, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    const/4 v2, 0x0

    .line 77
    const/4 v1, 0x0

    .line 79
    .local v1, "jsonResponse":Lorg/json/JSONObject;
    if-gez p0, :cond_0

    .line 80
    const-string/jumbo v3, "PlaceOnHandle"

    const-string/jumbo v4, "Invalid profile user id."

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :goto_0
    return v2

    .line 84
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v3

    if-gez v3, :cond_2

    .line 85
    :cond_1
    const-string/jumbo v3, "PlaceOnHandle"

    const-string/jumbo v4, "Invalid data to get shared group."

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 90
    :cond_2
    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 91
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 90
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;->getGroup(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 92
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isSuccessResponse(Lorg/json/JSONObject;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 93
    const-string/jumbo v4, "PlaceOnHandle"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Fail to get group info. Return value = "

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 94
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 93
    invoke-static {v4, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 100
    :catch_0
    move-exception v0

    .line 101
    .local v0, "je":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 95
    .end local v0    # "je":Lorg/json/JSONException;
    :cond_3
    :try_start_1
    const-string/jumbo v3, "Invalid result"

    goto :goto_1

    .line 99
    :cond_4
    invoke-static {p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->parseGroupFromJSONReponse(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lorg/json/JSONObject;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v2

    goto :goto_0
.end method

.method public static getMyProfile(I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .locals 2
    .param p0, "userId"    # I

    .prologue
    .line 69
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;->getMyProfile(I)Lorg/json/JSONObject;

    move-result-object v0

    .line 70
    .local v0, "result":Lorg/json/JSONObject;
    if-nez v0, :cond_0

    .line 71
    const/4 v1, 0x0

    .line 73
    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnJson;->fromJson(Lorg/json/JSONObject;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v1

    goto :goto_0
.end method

.method public static getProfileByPhoneNumber(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;
    .locals 4
    .param p0, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 552
    const/4 v0, 0x0

    .line 553
    .local v0, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;->getProfileByPhoneNumber(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 554
    .local v2, "result":Lorg/json/JSONObject;
    if-eqz v2, :cond_0

    .line 555
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;

    .end local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;
    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;-><init>()V

    .line 557
    .restart local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;
    :try_start_0
    const-string/jumbo v3, "userId"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->setUserId(I)V

    .line 558
    const-string/jumbo v3, "userName"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->setUserName(Ljava/lang/String;)V

    .line 559
    const-string/jumbo v3, "model"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->setModel(Ljava/lang/String;)V

    .line 560
    const-string/jumbo v3, "phoneNumber"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->setPhoneNumber(Ljava/lang/String;)V

    .line 561
    const-string/jumbo v3, "sspGuid"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->setSspGuid(Ljava/lang/String;)V

    .line 562
    const-string/jumbo v3, "expiredTime"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->setExpiredTime(Ljava/lang/String;)V

    .line 563
    const-string/jumbo v3, "gender"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->setGender(Ljava/lang/String;)V

    .line 564
    const-string/jumbo v3, "birthday"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->setBirthday(Ljava/lang/String;)V

    .line 565
    const-string/jumbo v3, "countryCode"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->setCountryCode(Ljava/lang/String;)V

    .line 566
    const-string/jumbo v3, "email"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->setEmail(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 571
    :cond_0
    :goto_0
    return-object v0

    .line 567
    :catch_0
    move-exception v1

    .line 568
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getSharedLocationFromGroupId(ILjava/lang/String;I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .locals 9
    .param p0, "userId"    # I
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .param p2, "groupId"    # I

    .prologue
    const/4 v5, 0x0

    .line 467
    const/4 v0, 0x0

    .line 468
    .local v0, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    const/4 v3, 0x0

    .line 469
    .local v3, "jsonResponse":Lorg/json/JSONObject;
    const/4 v4, 0x0

    .line 471
    .local v4, "participants":Lorg/json/JSONArray;
    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 472
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    .line 471
    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;->getGroup(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 473
    if-nez v3, :cond_0

    move-object v1, v5

    .line 506
    :goto_0
    return-object v1

    .line 476
    :cond_0
    const-string/jumbo v6, "participants"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    move-object v1, v5

    .line 477
    goto :goto_0

    .line 479
    :cond_1
    const-string/jumbo v6, "participants"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 480
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-nez v6, :cond_3

    :cond_2
    move-object v1, v5

    .line 481
    goto :goto_0

    .line 483
    :cond_3
    invoke-static {p1, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->findParticipantInGroupResponse(Ljava/lang/String;Lorg/json/JSONArray;)Lorg/json/JSONObject;

    move-result-object v3

    .line 485
    if-nez v3, :cond_4

    .line 486
    const-string/jumbo v6, "PlaceOnHandle"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v8, " not found in shared group."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v5

    .line 487
    goto :goto_0

    .line 490
    :cond_4
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 491
    .end local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .local v1, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    :try_start_1
    invoke-virtual {v1, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setPhoneNumber(Ljava/lang/String;)V

    .line 492
    new-instance v6, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    invoke-direct {v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;-><init>()V

    invoke-virtual {v1, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 493
    invoke-virtual {v1, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setFriendName(Ljava/lang/String;)V

    .line 495
    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->setBuddyFromJSON(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;Lorg/json/JSONObject;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 496
    const-string/jumbo v6, "PlaceOnHandle"

    const-string/jumbo v7, "buddy null"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .end local v1    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .restart local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    move-object v1, v5

    .line 497
    goto :goto_0

    .end local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .restart local v1    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    :cond_5
    move-object v0, v1

    .line 500
    .end local v1    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .restart local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    goto :goto_0

    .line 502
    :catch_0
    move-exception v2

    .line 503
    .local v2, "e":Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    move-object v1, v5

    .line 506
    goto :goto_0

    .line 502
    .end local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .end local v2    # "e":Lorg/json/JSONException;
    .restart local v1    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    :catch_1
    move-exception v2

    move-object v0, v1

    .end local v1    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .restart local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    goto :goto_1
.end method

.method public static getSharedLocationFromUrl(ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .locals 9
    .param p0, "userId"    # I
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 424
    const/4 v0, 0x0

    .line 425
    .local v0, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    const/4 v3, 0x0

    .line 426
    .local v3, "jsonResponse":Lorg/json/JSONObject;
    const/4 v4, 0x0

    .line 428
    .local v4, "participants":Lorg/json/JSONArray;
    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;->getGroupByUrl(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 429
    if-nez v3, :cond_0

    move-object v1, v5

    .line 462
    :goto_0
    return-object v1

    .line 432
    :cond_0
    const-string/jumbo v6, "participants"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    move-object v1, v5

    .line 433
    goto :goto_0

    .line 435
    :cond_1
    const-string/jumbo v6, "participants"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 436
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-nez v6, :cond_3

    :cond_2
    move-object v1, v5

    .line 437
    goto :goto_0

    .line 439
    :cond_3
    invoke-static {p1, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->findParticipantInGroupResponse(Ljava/lang/String;Lorg/json/JSONArray;)Lorg/json/JSONObject;

    move-result-object v3

    .line 441
    if-nez v3, :cond_4

    .line 442
    const-string/jumbo v6, "PlaceOnHandle"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v8, " not found in shared group."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v5

    .line 443
    goto :goto_0

    .line 446
    :cond_4
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 447
    .end local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .local v1, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    :try_start_1
    invoke-virtual {v1, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setPhoneNumber(Ljava/lang/String;)V

    .line 448
    new-instance v6, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    invoke-direct {v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;-><init>()V

    invoke-virtual {v1, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 449
    invoke-virtual {v1, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setFriendName(Ljava/lang/String;)V

    .line 451
    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->setBuddyFromJSON(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;Lorg/json/JSONObject;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 452
    const-string/jumbo v6, "PlaceOnHandle"

    const-string/jumbo v7, "buddy null"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .end local v1    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .restart local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    move-object v1, v5

    .line 453
    goto :goto_0

    .end local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .restart local v1    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    :cond_5
    move-object v0, v1

    .line 456
    .end local v1    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .restart local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    goto :goto_0

    .line 458
    :catch_0
    move-exception v2

    .line 459
    .local v2, "e":Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    move-object v1, v5

    .line 462
    goto :goto_0

    .line 458
    .end local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .end local v2    # "e":Lorg/json/JSONException;
    .restart local v1    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    :catch_1
    move-exception v2

    move-object v0, v1

    .end local v1    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .restart local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    goto :goto_1
.end method

.method private static makeMsg(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "profileOf"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .param p1, "body"    # Ljava/lang/String;

    .prologue
    .line 899
    const-string/jumbo v0, "{ \"type\":\"LR\", \"status\":\"03\", \"push_type\":\"02\", \"data\":{\"userNo\":\"%s\", \"phone\":\"%s\", \"body\":\"%s\",\"User Name\":\"%s\"}}"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 900
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 901
    aput-object p1, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 899
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 898
    return-object v0
.end method

.method private static parseGroupFromJSONReponse(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lorg/json/JSONObject;)Z
    .locals 3
    .param p0, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .param p1, "jsonResponse"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 224
    if-nez p0, :cond_0

    .line 225
    const-string/jumbo v1, "PlaceOnHandle"

    const-string/jumbo v2, "Null group to parse json for!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    :goto_0
    return v0

    .line 229
    :cond_0
    if-nez p1, :cond_1

    .line 230
    const-string/jumbo v1, "PlaceOnHandle"

    const-string/jumbo v2, "Null response to create group."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 234
    :cond_1
    invoke-static {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->setGroupFromJSON(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lorg/json/JSONObject;)Z

    move-result v0

    goto :goto_0
.end method

.method public static quitFromGroup(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;I)Z
    .locals 8
    .param p0, "profile"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .param p1, "groupId"    # I

    .prologue
    const/4 v4, 0x0

    .line 794
    const/4 v3, 0x0

    .line 795
    .local v3, "jsonResponse":Lorg/json/JSONObject;
    const/4 v1, 0x0

    .line 798
    .local v1, "jsonParameters":Lorg/json/JSONObject;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 800
    .end local v1    # "jsonParameters":Lorg/json/JSONObject;
    .local v2, "jsonParameters":Lorg/json/JSONObject;
    :try_start_1
    const-string/jumbo v5, "share"

    const-string/jumbo v6, "N"

    invoke-virtual {v2, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 801
    const-string/jumbo v5, "statusCode"

    const/4 v6, 0x3

    invoke-virtual {v2, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 803
    const-string/jumbo v5, "duration"

    const/16 v6, 0x64

    invoke-virtual {v2, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 806
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 807
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 805
    invoke-static {v5, v6, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;->updateGroupParticipantStatus(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v3

    .line 809
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isSuccessResponse(Lorg/json/JSONObject;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 810
    const-string/jumbo v5, "PlaceOnHandle"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Fail to left group: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    if-eqz v3, :cond_0

    .line 812
    const-string/jumbo v5, "PlaceOnHandle"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Result: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    move-object v1, v2

    .line 823
    .end local v2    # "jsonParameters":Lorg/json/JSONObject;
    .restart local v1    # "jsonParameters":Lorg/json/JSONObject;
    :goto_1
    return v4

    .line 814
    .end local v1    # "jsonParameters":Lorg/json/JSONObject;
    .restart local v2    # "jsonParameters":Lorg/json/JSONObject;
    :cond_0
    const-string/jumbo v5, "PlaceOnHandle"

    const-string/jumbo v6, "Result is null"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 819
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 820
    .end local v2    # "jsonParameters":Lorg/json/JSONObject;
    .local v0, "e":Lorg/json/JSONException;
    .restart local v1    # "jsonParameters":Lorg/json/JSONObject;
    :goto_2
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 818
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v1    # "jsonParameters":Lorg/json/JSONObject;
    .restart local v2    # "jsonParameters":Lorg/json/JSONObject;
    :cond_1
    const/4 v4, 0x1

    move-object v1, v2

    .end local v2    # "jsonParameters":Lorg/json/JSONObject;
    .restart local v1    # "jsonParameters":Lorg/json/JSONObject;
    goto :goto_1

    .line 819
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static removeGroupShared(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 8
    .param p0, "profile"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .param p1, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    const/4 v5, 0x0

    .line 745
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v4

    if-ltz v4, :cond_0

    .line 746
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getSspGuid()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 747
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getSspGuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz p1, :cond_0

    .line 748
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v4

    const/4 v6, -0x1

    if-ne v4, v6, :cond_1

    .line 749
    :cond_0
    const-string/jumbo v4, "PlaceOnHandle"

    const-string/jumbo v6, "Invalid data to remove group."

    invoke-static {v4, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    .line 765
    :goto_0
    return v4

    .line 753
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 754
    .local v3, "userId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getSspGuid()Ljava/lang/String;

    move-result-object v2

    .line 755
    .local v2, "sspGuid":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 756
    .local v0, "groupId":Ljava/lang/String;
    invoke-static {v3, v2, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;->deleteGroup(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 758
    .local v1, "jsonResponse":Lorg/json/JSONObject;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isSuccessResponse(Lorg/json/JSONObject;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 759
    const-string/jumbo v6, "PlaceOnHandle"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Fail to remove group. Return value = "

    invoke-direct {v7, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 760
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 759
    invoke-static {v6, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    .line 762
    goto :goto_0

    .line 761
    :cond_2
    const-string/jumbo v4, "Invalid result"

    goto :goto_1

    .line 765
    :cond_3
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public static sendMessage(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;Ljava/lang/String;)Z
    .locals 5
    .param p0, "profileOf"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .param p1, "profileTo"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 885
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 886
    .local v1, "jsonParameters":Lorg/json/JSONObject;
    const-string/jumbo v3, "pushId"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getPushId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 887
    const-string/jumbo v3, "message"

    invoke-static {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->makeMsg(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 888
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;->sendMessage(Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    .line 892
    .end local v1    # "jsonParameters":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return v2

    .line 890
    :catch_0
    move-exception v0

    .line 891
    .local v0, "e":Lorg/json/JSONException;
    const-class v3, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "sendMessage"

    invoke-static {v3, v4, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static setBuddyFromJSON(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;Lorg/json/JSONObject;)Z
    .locals 8
    .param p0, "buddy"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .param p1, "jsonObj"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 316
    if-eqz p1, :cond_0

    if-nez p0, :cond_1

    .line 385
    :cond_0
    :goto_0
    return v3

    .line 320
    :cond_1
    const-string/jumbo v5, "phoneNumber"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 321
    const-string/jumbo v4, "PlaceOnHandle"

    const-string/jumbo v5, "Phone number not found in group participant."

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 325
    :cond_2
    const-string/jumbo v5, "phoneNumber"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 326
    .local v1, "phone":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->areEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 327
    const-string/jumbo v4, "PlaceOnHandle"

    const-string/jumbo v5, "Inconsistent phone number between buddy obj and json."

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 331
    :cond_3
    const-string/jumbo v5, "requestUrl"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 332
    const-string/jumbo v5, "requestUrl"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setUrl(Ljava/lang/String;)V

    .line 334
    :cond_4
    const-string/jumbo v5, "name"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 335
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getFriendName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getFriendName()Ljava/lang/String;

    move-result-object v5

    .line 336
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 337
    :cond_5
    const-string/jumbo v5, "name"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setFriendName(Ljava/lang/String;)V

    .line 339
    :cond_6
    const-string/jumbo v5, "userName"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 340
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getFriendName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_7

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getFriendName()Ljava/lang/String;

    move-result-object v5

    .line 341
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 342
    :cond_7
    const-string/jumbo v5, "userName"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setFriendName(Ljava/lang/String;)V

    .line 344
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v5

    if-nez v5, :cond_9

    .line 345
    new-instance v5, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    invoke-direct {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;-><init>()V

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 348
    :cond_9
    const-string/jumbo v5, "latitude"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 349
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v5

    const-string/jumbo v6, "latitude"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->setLatitude(D)V

    .line 352
    :cond_a
    const-string/jumbo v5, "longitude"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 353
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v5

    const-string/jumbo v6, "longitude"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->setLongitude(D)V

    .line 356
    :cond_b
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setIsOwner(Z)V

    .line 357
    const-string/jumbo v5, "isOwner"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 358
    const-string/jumbo v5, "Y"

    const-string/jumbo v6, "isOwner"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 359
    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setIsOwner(Z)V

    .line 362
    :cond_c
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setShared(Z)V

    .line 363
    const-string/jumbo v3, "isShare"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 364
    const-string/jumbo v3, "Y"

    const-string/jumbo v5, "isShare"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 365
    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setShared(Z)V

    .line 368
    :cond_d
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->NOT_RESPONSED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setStatus(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;)V

    .line 369
    const-string/jumbo v3, "statusCode"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 371
    const-string/jumbo v3, "statusCode"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 370
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->getTypeFromInt(I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setStatus(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;)V

    .line 374
    :cond_e
    const/4 v0, 0x0

    .line 375
    .local v0, "date":Ljava/lang/String;
    const/4 v2, 0x0

    .line 377
    .local v2, "time":Ljava/lang/String;
    const-string/jumbo v3, "expiredDate"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 378
    const-string/jumbo v3, "expiredDate"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 380
    :cond_f
    const-string/jumbo v3, "expiredTime"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 381
    const-string/jumbo v3, "expiredTime"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 383
    :cond_10
    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->getDateTime(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v5

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setExpireTime(J)V

    move v3, v4

    .line 385
    goto/16 :goto_0
.end method

.method private static setGroupFromJSON(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lorg/json/JSONObject;)Z
    .locals 7
    .param p0, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .param p1, "jsonResponse"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 241
    const/4 v2, 0x0

    .line 243
    .local v2, "destination":Lorg/json/JSONObject;
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 294
    :cond_0
    :goto_0
    return v4

    .line 248
    :cond_1
    const-string/jumbo v5, "Group"

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setGroupName(Ljava/lang/String;)V

    .line 250
    const-string/jumbo v5, "shareGroupId"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 251
    const-string/jumbo v5, "shareGroupId"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setGroupId(I)V

    .line 260
    :goto_1
    const-string/jumbo v5, "groupType"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 262
    const-string/jumbo v5, "groupType"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 261
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;->getTypeFromInt(I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setGroupType(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;)V

    .line 265
    :cond_2
    const/4 v0, 0x0

    .line 266
    .local v0, "date":Ljava/lang/String;
    const/4 v3, 0x0

    .line 268
    .local v3, "time":Ljava/lang/String;
    const-string/jumbo v5, "expiredDate"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 269
    const-string/jumbo v5, "expiredDate"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 271
    :cond_3
    const-string/jumbo v5, "expiredTime"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 272
    const-string/jumbo v5, "expiredTime"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 274
    :cond_4
    invoke-static {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->getDateTime(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v5

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setExpireTime(J)V

    .line 276
    invoke-static {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->setGroupParticipantsFromJson(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lorg/json/JSONObject;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 277
    const-string/jumbo v5, "PlaceOnHandle"

    const-string/jumbo v6, "Error to add Participant in Group from Json"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 253
    .end local v0    # "date":Ljava/lang/String;
    .end local v3    # "time":Ljava/lang/String;
    :cond_5
    const-string/jumbo v5, "groupId"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 254
    const-string/jumbo v5, "groupId"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setGroupId(I)V

    goto :goto_1

    .line 256
    :cond_6
    const-string/jumbo v5, "PlaceOnHandle"

    const-string/jumbo v6, "Group id not found."

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 281
    .restart local v0    # "date":Ljava/lang/String;
    .restart local v3    # "time":Ljava/lang/String;
    :cond_7
    const-string/jumbo v4, "destination"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 282
    const-string/jumbo v4, "destination"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 283
    if-eqz v2, :cond_8

    .line 284
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;-><init>()V

    .line 285
    .local v1, "dest":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;
    const-string/jumbo v4, "latitude"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLatitude(D)V

    .line 286
    const-string/jumbo v4, "longitude"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLongitude(D)V

    .line 287
    const-string/jumbo v4, "address"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLocationAddress(Ljava/lang/String;)V

    .line 288
    const-string/jumbo v4, "name"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLocationName(Ljava/lang/String;)V

    .line 289
    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setDestination(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 294
    .end local v1    # "dest":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;
    :cond_8
    :goto_2
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 292
    :cond_9
    const-string/jumbo v4, "PlaceOnHandle"

    const-string/jumbo v5, "Destination not found in get shared group response. "

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private static setGroupParticipantsFromJson(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lorg/json/JSONObject;)Z
    .locals 9
    .param p0, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .param p1, "jsonResponse"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 828
    const/4 v3, 0x0

    .line 830
    .local v3, "participants":Lorg/json/JSONArray;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 831
    const-string/jumbo v6, "participants"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 832
    :cond_0
    const-string/jumbo v6, "PlaceOnHandle"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Participants not found in create group result. "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 833
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 832
    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 879
    :goto_0
    return v5

    .line 837
    :cond_1
    const-string/jumbo v6, "participants"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 839
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-nez v6, :cond_3

    .line 840
    :cond_2
    const-string/jumbo v6, "PlaceOnHandle"

    const-string/jumbo v7, "Null participants in create group result."

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 844
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->resetStatusCount()V

    .line 846
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-lt v1, v6, :cond_4

    .line 879
    const/4 v5, 0x1

    goto :goto_0

    .line 847
    :cond_4
    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 849
    .local v2, "jsonObj":Lorg/json/JSONObject;
    const-string/jumbo v6, "phoneNumber"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 850
    const-string/jumbo v6, "PlaceOnHandle"

    .line 851
    const-string/jumbo v7, "Participant from created group is missing the phone number!"

    .line 850
    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 855
    :cond_5
    const-string/jumbo v6, "phoneNumber"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 857
    .local v4, "phone":Ljava/lang/String;
    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipantFromPhoneNumber(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    move-result-object v0

    .line 860
    .local v0, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    if-nez v0, :cond_6

    .line 861
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .end local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;-><init>()V

    .line 862
    .restart local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setPhoneNumber(Ljava/lang/String;)V

    .line 863
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->addGroupParticipant(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V

    .line 866
    :cond_6
    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->setBuddyFromJSON(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;Lorg/json/JSONObject;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 867
    const-string/jumbo v6, "PlaceOnHandle"

    .line 868
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Could not set buddy from json ... "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 869
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 868
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 867
    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 874
    :cond_7
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getUrl()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_8

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_8

    .line 875
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setGroupUrl(Ljava/lang/String;)V

    .line 877
    :cond_8
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getStatus()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->countStatus(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;)V

    .line 846
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static setMyLocation(ILcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 7
    .param p0, "userId"    # I
    .param p1, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    const/4 v3, 0x0

    .line 389
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 392
    .local v2, "object":Lorg/json/JSONObject;
    if-ltz p0, :cond_0

    if-nez p1, :cond_1

    .line 393
    :cond_0
    :try_start_0
    const-string/jumbo v4, "PlaceOnHandle"

    const-string/jumbo v5, "Invalid data to set my location."

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    :goto_0
    return v3

    .line 397
    :cond_1
    const-string/jumbo v4, "latitude"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v5

    invoke-virtual {v2, v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 398
    const-string/jumbo v4, "longitude"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v5

    invoke-virtual {v2, v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 401
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 400
    invoke-static {v4, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;->setMyLocation(Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v1

    .line 403
    .local v1, "jsonResponse":Lorg/json/JSONObject;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isSuccessResponse(Lorg/json/JSONObject;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 404
    if-eqz v1, :cond_2

    .line 405
    const-string/jumbo v4, "PlaceOnHandle"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Fail to set my location. Return value = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 406
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 405
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 415
    .end local v1    # "jsonResponse":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 416
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 408
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v1    # "jsonResponse":Lorg/json/JSONObject;
    :cond_2
    :try_start_1
    const-string/jumbo v4, "PlaceOnHandle"

    const-string/jumbo v5, "Fail to set my location. Return value = Invalid result"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 414
    :cond_3
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public static updateParticipantStatus(IILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;J)Z
    .locals 9
    .param p0, "userId"    # I
    .param p1, "groupId"    # I
    .param p2, "status"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;
    .param p3, "duration"    # J

    .prologue
    const/4 v5, 0x0

    .line 639
    const/4 v3, 0x0

    .line 640
    .local v3, "jsonResponse":Lorg/json/JSONObject;
    const/4 v1, 0x0

    .line 642
    .local v1, "jsonParameters":Lorg/json/JSONObject;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 644
    .end local v1    # "jsonParameters":Lorg/json/JSONObject;
    .local v2, "jsonParameters":Lorg/json/JSONObject;
    :try_start_1
    sget-object v6, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->ACCEPTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    if-ne p2, v6, :cond_2

    const-string/jumbo v4, "Y"

    .line 647
    .local v4, "share":Ljava/lang/String;
    :goto_0
    const-string/jumbo v6, "share"

    invoke-virtual {v2, v6, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 648
    const-string/jumbo v6, "statusCode"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->getValue()I

    move-result v7

    invoke-virtual {v2, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 649
    const-wide/16 v6, 0x0

    cmp-long v6, p3, v6

    if-lez v6, :cond_0

    .line 650
    const-string/jumbo v6, "duration"

    const-wide/32 v7, 0xea60

    div-long v7, p3, v7

    invoke-virtual {v2, v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 653
    :cond_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    .line 652
    invoke-static {v6, v7, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;->updateGroupParticipantStatus(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v3

    .line 656
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isSuccessResponse(Lorg/json/JSONObject;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 657
    const-string/jumbo v6, "PlaceOnHandle"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Fail to update participant status: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 658
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 657
    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    if-eqz v3, :cond_1

    .line 660
    const-string/jumbo v6, "PlaceOnHandle"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Result = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    move-object v1, v2

    .line 669
    .end local v2    # "jsonParameters":Lorg/json/JSONObject;
    .end local v4    # "share":Ljava/lang/String;
    .restart local v1    # "jsonParameters":Lorg/json/JSONObject;
    :goto_1
    return v5

    .line 645
    .end local v1    # "jsonParameters":Lorg/json/JSONObject;
    .restart local v2    # "jsonParameters":Lorg/json/JSONObject;
    :cond_2
    const-string/jumbo v4, "N"
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 664
    .restart local v4    # "share":Ljava/lang/String;
    :cond_3
    const/4 v5, 0x1

    move-object v1, v2

    .end local v2    # "jsonParameters":Lorg/json/JSONObject;
    .restart local v1    # "jsonParameters":Lorg/json/JSONObject;
    goto :goto_1

    .line 665
    .end local v4    # "share":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 666
    .local v0, "e":Lorg/json/JSONException;
    :goto_2
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 665
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v1    # "jsonParameters":Lorg/json/JSONObject;
    .restart local v2    # "jsonParameters":Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "jsonParameters":Lorg/json/JSONObject;
    .restart local v1    # "jsonParameters":Lorg/json/JSONObject;
    goto :goto_2
.end method

.method public static updateParticipantStatusByUrl(ILjava/lang/String;ZI)Z
    .locals 9
    .param p0, "userId"    # I
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "share"    # Z
    .param p3, "status"    # I

    .prologue
    const/4 v5, 0x0

    .line 674
    const/4 v4, 0x0

    .line 675
    .local v4, "jsonResponse":Lorg/json/JSONObject;
    const/4 v2, 0x0

    .line 679
    .local v2, "jsonParameters":Lorg/json/JSONObject;
    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;->getGroupByUrl(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 681
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isSuccessResponse(Lorg/json/JSONObject;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 682
    const-string/jumbo v6, "PlaceOnHandle"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Fail to get group from url: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 717
    :goto_0
    return v5

    .line 686
    :cond_0
    const-string/jumbo v6, "groupId"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 687
    const-string/jumbo v6, "PlaceOnHandle"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Group id not found."

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 713
    :catch_0
    move-exception v0

    .line 714
    .local v0, "e":Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 691
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_1
    :try_start_1
    const-string/jumbo v6, "groupId"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 693
    .local v1, "groupId":I
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 695
    .end local v2    # "jsonParameters":Lorg/json/JSONObject;
    .local v3, "jsonParameters":Lorg/json/JSONObject;
    :try_start_2
    const-string/jumbo v7, "share"

    if-eqz p2, :cond_3

    const-string/jumbo v6, "Y"

    :goto_2
    invoke-virtual {v3, v7, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 696
    const-string/jumbo v6, "statusCode"

    invoke-virtual {v3, v6, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 698
    const-string/jumbo v6, "duration"

    const/16 v7, 0x64

    invoke-virtual {v3, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 701
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    .line 700
    invoke-static {v6, v7, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;->updateGroupParticipantStatus(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v4

    .line 704
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isSuccessResponse(Lorg/json/JSONObject;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 705
    const-string/jumbo v6, "PlaceOnHandle"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Fail to update participant status: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 706
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 705
    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 707
    if-eqz v4, :cond_2

    .line 708
    const-string/jumbo v6, "PlaceOnHandle"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Result = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move-object v2, v3

    .line 709
    .end local v3    # "jsonParameters":Lorg/json/JSONObject;
    .restart local v2    # "jsonParameters":Lorg/json/JSONObject;
    goto/16 :goto_0

    .line 695
    .end local v2    # "jsonParameters":Lorg/json/JSONObject;
    .restart local v3    # "jsonParameters":Lorg/json/JSONObject;
    :cond_3
    const-string/jumbo v6, "N"
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 712
    :cond_4
    const/4 v5, 0x1

    move-object v2, v3

    .end local v3    # "jsonParameters":Lorg/json/JSONObject;
    .restart local v2    # "jsonParameters":Lorg/json/JSONObject;
    goto/16 :goto_0

    .line 713
    .end local v2    # "jsonParameters":Lorg/json/JSONObject;
    .restart local v3    # "jsonParameters":Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    move-object v2, v3

    .end local v3    # "jsonParameters":Lorg/json/JSONObject;
    .restart local v2    # "jsonParameters":Lorg/json/JSONObject;
    goto :goto_1
.end method

.method public static updateProfile(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)Z
    .locals 7
    .param p0, "profile"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    .prologue
    const/4 v3, 0x0

    .line 38
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnJson;->toJson(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)Lorg/json/JSONObject;

    move-result-object v1

    .line 40
    .local v1, "jsonObject":Lorg/json/JSONObject;
    if-nez v1, :cond_0

    .line 41
    const-string/jumbo v4, "PlaceOnHandle"

    const-string/jumbo v5, "Fail to create json param to create user profile."

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    :goto_0
    return v3

    .line 45
    .restart local v1    # "jsonObject":Lorg/json/JSONObject;
    :cond_0
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;->updateProfile(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v2

    .line 47
    .local v2, "result":Lorg/json/JSONObject;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isSuccessResponse(Lorg/json/JSONObject;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 48
    const-string/jumbo v5, "PlaceOnHandle"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Fail to create user profile = "

    invoke-direct {v6, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 49
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 48
    invoke-static {v5, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 62
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    .end local v2    # "result":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 63
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 65
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_2
    const/4 v3, 0x1

    goto :goto_0

    .line 50
    .restart local v1    # "jsonObject":Lorg/json/JSONObject;
    .restart local v2    # "result":Lorg/json/JSONObject;
    :cond_1
    :try_start_1
    const-string/jumbo v4, "Invalid result"

    goto :goto_1

    .line 54
    :cond_2
    const-string/jumbo v4, "userId"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 55
    const-string/jumbo v4, "PlaceOnHandle"

    .line 56
    const-string/jumbo v5, "Fail to create user profile not user id found on response!"

    .line 55
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 60
    :cond_3
    const-string/jumbo v3, "userId"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->setUserId(I)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public static updateShareGroup(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILorg/json/JSONObject;)Z
    .locals 5
    .param p0, "sspGuid"    # Ljava/lang/String;
    .param p1, "accessToken"    # Ljava/lang/String;
    .param p2, "userId"    # Ljava/lang/String;
    .param p3, "groupId"    # I
    .param p4, "params"    # Lorg/json/JSONObject;

    .prologue
    const/4 v1, 0x0

    .line 722
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 723
    if-eqz p4, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 724
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 740
    :cond_0
    :goto_0
    return v1

    .line 727
    :cond_1
    invoke-static {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;->updateShareGroup(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0

    .line 729
    .local v0, "result":Lorg/json/JSONObject;
    if-eqz v0, :cond_0

    .line 733
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isSuccessResponse(Lorg/json/JSONObject;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 734
    const-string/jumbo v2, "PlaceOnHandle"

    .line 735
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Fail to update participant status: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 734
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 736
    if-eqz v0, :cond_0

    .line 737
    const-string/jumbo v2, "PlaceOnHandle"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Result = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 740
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method createLocationFromJSON(Lorg/json/JSONObject;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;
    .locals 3
    .param p1, "json"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 535
    if-nez p1, :cond_1

    .line 536
    const/4 v0, 0x0

    .line 548
    :cond_0
    :goto_0
    return-object v0

    .line 538
    :cond_1
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;-><init>()V

    .line 542
    .local v0, "location":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;
    const-string/jumbo v1, "latitude"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 543
    const-string/jumbo v1, "latitude"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLatitude(D)V

    .line 545
    :cond_2
    const-string/jumbo v1, "longitude"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 546
    const-string/jumbo v1, "longitude"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLongitude(D)V

    goto :goto_0
.end method
