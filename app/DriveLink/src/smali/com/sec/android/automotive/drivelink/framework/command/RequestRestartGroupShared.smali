.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestRestartGroupShared.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

.field private mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

.field private mResult:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 2
    .param p1, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .param p4, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    const/4 v1, 0x0

    .line 19
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;->mResult:Z

    .line 12
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;->mContext:Landroid/content/Context;

    .line 13
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .line 14
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 20
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;->mContext:Landroid/content/Context;

    .line 21
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .line 22
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 23
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;->mContext:Landroid/content/Context;

    .line 43
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .line 44
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 45
    return-void
.end method

.method protected process()V
    .locals 4

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v0

    .line 28
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->restartGroupShared(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v0

    .line 27
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;->mResult:Z

    .line 29
    return-void
.end method

.method protected response()V
    .locals 3

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 34
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    move-result-object v0

    .line 35
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    if-eqz v0, :cond_0

    .line 36
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;->mResult:Z

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;->onResponseRequestRestartGroupShared(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V

    .line 38
    :cond_0
    return-void
.end method
