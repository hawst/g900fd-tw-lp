.class public Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;
.source "MultiWindowActivityOverlayTest.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$multiwindow$MultiWindowListener$OnMultiHomeListener$MultiHomeState:[I = null

.field public static final ACTION_EXTRA_STARTNAVI:Ljava/lang/String; = "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_STARTNAVI"

.field public static final ACTION_EXTRA_TTS:Ljava/lang/String; = "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_TTS"

.field public static final ACTION_START:Ljava/lang/String; = "com.sec.android.automotive.drivelink.multiwindow.ACTION_START"

.field private static final INDICATOR_FRAGMENT_TAG:Ljava/lang/String; = "multi_indicator"

.field private static final TAG:Ljava/lang/String; = "[MultiWindowActivity]"

.field public static final bSupportFixedSplite:Z = true

.field private static isFinished:Z

.field private static mContext:Landroid/content/Context;

.field private static mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

.field private static mWindowMgr:Landroid/view/WindowManager;

.field private static sFlowId:Ljava/lang/String;


# instance fields
.field private indicatorFragment:Landroid/support/v4/app/Fragment;

.field private mActivityStateChangeListener:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;

.field mMultiHomeListener:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

.field private mNotiKeyListener:Landroid/view/View$OnKeyListener;

.field private mNotiReqListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

.field private mNotiTouchListener:Landroid/view/View$OnTouchListener;

.field private mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

.field private mNotificationListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$multiwindow$MultiWindowListener$OnMultiHomeListener$MultiHomeState()[I
    .locals 3

    .prologue
    .line 66
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$multiwindow$MultiWindowListener$OnMultiHomeListener$MultiHomeState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->values()[Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_LOCATION:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_MESSAGE:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_MUSIC:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_PHONE:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_MIC_OFF:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_MIC_ON:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_6
    :try_start_6
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_NONE:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_7
    sput-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$multiwindow$MultiWindowListener$OnMultiHomeListener$MultiHomeState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_7

    :catch_1
    move-exception v1

    goto :goto_6

    :catch_2
    move-exception v1

    goto :goto_5

    :catch_3
    move-exception v1

    goto :goto_4

    :catch_4
    move-exception v1

    goto :goto_3

    :catch_5
    move-exception v1

    goto :goto_2

    :catch_6
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 80
    sput-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mWindowMgr:Landroid/view/WindowManager;

    .line 81
    sput-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mContext:Landroid/content/Context;

    .line 82
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->isFinished:Z

    .line 83
    sput-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    .line 85
    sput-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->sFlowId:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;-><init>()V

    .line 77
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->indicatorFragment:Landroid/support/v4/app/Fragment;

    .line 78
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .line 374
    new-instance v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$1;-><init>(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMultiHomeListener:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    .line 832
    new-instance v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$2;-><init>(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotificationListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    .line 844
    new-instance v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$3;-><init>(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiReqListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    .line 931
    new-instance v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$4;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$4;-><init>(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiKeyListener:Landroid/view/View$OnKeyListener;

    .line 946
    new-instance v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$5;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$5;-><init>(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiTouchListener:Landroid/view/View$OnTouchListener;

    .line 979
    new-instance v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$6;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$6;-><init>(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mActivityStateChangeListener:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;

    .line 66
    return-void
.end method

.method static synthetic access$0()Landroid/content/Context;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)V
    .locals 0

    .prologue
    .line 287
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->dismissNotiHomeMenu()V

    return-void
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;)V
    .locals 0

    .prologue
    .line 83
    sput-object p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 507
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->setNotiFlowId(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;)V
    .locals 0

    .prologue
    .line 353
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->moveToMenu(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;)V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;Z)Z
    .locals 1

    .prologue
    .line 733
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->checkNotification(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)V
    .locals 0

    .prologue
    .line 781
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->dismissNotification()V

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    .locals 0

    .prologue
    .line 804
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)V
    .locals 0

    .prologue
    .line 514
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->resetNotiFlowId()V

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    .locals 0

    .prologue
    .line 662
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->removeOverlayNotiView(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    return-void
.end method

.method static synthetic access$9()Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    return-object v0
.end method

.method private addIndicatorView()V
    .locals 5

    .prologue
    .line 251
    const-string/jumbo v2, "[MultiWindowActivity]"

    const-string/jumbo v3, "addIndicatorView is called."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->indicatorFragment:Landroid/support/v4/app/Fragment;

    if-nez v2, :cond_1

    .line 253
    new-instance v2, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->indicatorFragment:Landroid/support/v4/app/Fragment;

    .line 254
    const-string/jumbo v2, "[MultiWindowActivity]"

    const-string/jumbo v3, "indicator is null. create new indicator"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 264
    .local v1, "manager":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 265
    .local v0, "ft":Landroid/support/v4/app/FragmentTransaction;
    const-string/jumbo v2, "multi_indicator"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_2

    .line 266
    const v2, 0x7f0900c8

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->indicatorFragment:Landroid/support/v4/app/Fragment;

    const-string/jumbo v4, "multi_indicator"

    invoke-virtual {v0, v2, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 270
    :goto_1
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 271
    const-string/jumbo v2, "[MultiWindowActivity]"

    const-string/jumbo v3, "IndicatorView is added."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    return-void

    .line 256
    .end local v0    # "ft":Landroid/support/v4/app/FragmentTransaction;
    .end local v1    # "manager":Landroid/support/v4/app/FragmentManager;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->indicatorFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 257
    const-string/jumbo v2, "[MultiWindowActivity]"

    const-string/jumbo v3, "indicator is not null and has parent."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->removeIndicatorView()V

    .line 259
    new-instance v2, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->indicatorFragment:Landroid/support/v4/app/Fragment;

    goto :goto_0

    .line 268
    .restart local v0    # "ft":Landroid/support/v4/app/FragmentTransaction;
    .restart local v1    # "manager":Landroid/support/v4/app/FragmentManager;
    :cond_2
    const-string/jumbo v2, "[MultiWindowActivity]"

    const-string/jumbo v3, "indicator view is alread added!!"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private addNotiVoiceUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V
    .locals 2
    .param p1, "viv"    # Lcom/nuance/drivelink/DLUiUpdater;

    .prologue
    .line 901
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    if-nez v0, :cond_1

    .line 909
    :cond_0
    :goto_0
    return-void

    .line 905
    :cond_1
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->hasNotiVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 906
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->addNotiVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 907
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "Add Noti. Voice Listener"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private addOverlayNotiView()V
    .locals 3

    .prologue
    .line 622
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-nez v0, :cond_0

    .line 623
    const-string/jumbo v0, "[MultiWindowActivity]"

    .line 624
    const-string/jumbo v1, "Noti View is null. need to create noti view before call it."

    .line 623
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 638
    :goto_0
    return-void

    .line 628
    :cond_0
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "addOverlayNotiView"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->hasNotiItem()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 630
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v0

    const/16 v1, 0xe

    if-eq v0, v1, :cond_1

    .line 631
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->addNotiVoiceUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 634
    :cond_1
    const-string/jumbo v0, "window"

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 633
    check-cast v0, Landroid/view/WindowManager;

    sput-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mWindowMgr:Landroid/view/WindowManager;

    .line 635
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mWindowMgr:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    sget-object v2, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mWindowMgr:Landroid/view/WindowManager;

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->getOverlayLayoutParams(Landroid/view/WindowManager;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private checkNotification(Z)Z
    .locals 5
    .param p1, "bAnimation"    # Z

    .prologue
    .line 734
    const/4 v0, 0x0

    .line 736
    .local v0, "bNotified":Z
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v2, :cond_0

    .line 737
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->removeOverlayNotiView(Z)V

    .line 740
    :cond_0
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v2

    .line 741
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getTopNotificationView()Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    move-result-object v2

    .line 740
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .line 743
    const-string/jumbo v2, "[MultiWindowActivity]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "NotiView:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v2, :cond_3

    .line 746
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    .line 747
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 750
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiReqListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->setOnNotificationRequestListener(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;)V

    .line 752
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->hasNotiItem()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 753
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->isAppeared()Z

    move-result v2

    if-nez v2, :cond_2

    .line 754
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v2

    const/16 v3, 0xf

    if-eq v2, v3, :cond_2

    .line 755
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 756
    const-string/jumbo v2, "[MultiWindowActivity]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "MUSIC is Playing : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 757
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 756
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 759
    :try_start_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    sput-object v2, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    .line 760
    const-string/jumbo v2, "[MultiWindowActivity]"

    const-string/jumbo v3, "onPlaybackRequest PLAYBACK_REQUEST_PAUSE"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 761
    sget-object v2, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    .line 762
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;->onPlaybackRequest(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 769
    :cond_2
    :goto_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_APPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-direct {p0, v2, v3}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 770
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->addOverlayNotiView()V

    .line 771
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;

    move-result-object v2

    .line 772
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->setOnMicStateChangeListener(Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;)V

    .line 773
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->APPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-direct {p0, v2, v3}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 775
    const/4 v0, 0x1

    .line 778
    :cond_3
    return v0

    .line 763
    :catch_0
    move-exception v1

    .line 764
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private clearNotificationInMultiWindow()V
    .locals 2

    .prologue
    .line 728
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 729
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotificationListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->unregisterOnNotificationChangeListener(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;)V

    .line 730
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->setMultiMode(Z)V

    .line 731
    return-void
.end method

.method private clickLocation()V
    .locals 2

    .prologue
    .line 325
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->setMultiModeFullSize()Z

    .line 326
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 327
    .local v0, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v1, "DM_LOCATION"

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlowForMulti(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 329
    return-void
.end method

.method private clickMessage()V
    .locals 2

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->setMultiModeFullSize()Z

    .line 319
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 320
    .local v0, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v1, "DM_SMS_CONTACT"

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlowForMulti(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 322
    return-void
.end method

.method private clickMusic()V
    .locals 5

    .prologue
    .line 332
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    if-nez v2, :cond_1

    .line 351
    :cond_0
    :goto_0
    return-void

    .line 336
    :cond_1
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->endpointReco()V

    .line 337
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 338
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setCheckListSettingMusicList()Z

    move-result v1

    .line 339
    .local v1, "value":Z
    const-string/jumbo v2, "[MultiWindowActivity]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "setCheckMusicList value + "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    if-eqz v1, :cond_0

    .line 343
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->setMultiModeFullSize()Z

    .line 344
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setCallMultiwindow(Z)V

    .line 346
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 347
    .local v0, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 348
    const v3, 0x7f0a0032

    .line 347
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 349
    const-string/jumbo v2, "DM_MUSIC_PLAYER"

    invoke-static {v2, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlowForMulti(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0
.end method

.method private clickPhone()V
    .locals 2

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->setMultiModeFullSize()Z

    .line 312
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 313
    .local v0, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v1, "DM_DIAL_CONTACT"

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlowForMulti(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 315
    return-void
.end method

.method private dismissNotiHomeMenu()V
    .locals 2

    .prologue
    .line 288
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "dismissNotiHomeMenu"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->hasNotiItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v0

    const/16 v1, 0xe

    if-ne v0, v1, :cond_0

    .line 292
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->dismissNotification()V

    .line 294
    :cond_0
    return-void
.end method

.method private dismissNotiMusic()V
    .locals 2

    .prologue
    .line 297
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "dismissNotiMusic"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->hasNotiItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v0

    const/16 v1, 0xf

    if-ne v0, v1, :cond_0

    .line 301
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->dismissNotification()V

    .line 303
    :cond_0
    return-void
.end method

.method private dismissNotification()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 782
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_1

    .line 783
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "dismissNotification"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 784
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_DISAPPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 786
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 787
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v1

    .line 786
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->removeNotification(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    .line 788
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->DISAPPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 789
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->removeOverlayNotiView(Z)V

    .line 790
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->resetNotiFlowId()V

    .line 792
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    if-eqz v0, :cond_0

    .line 793
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    .line 794
    invoke-interface {v0, v2}, Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;->onPlaybackRequest(I)V

    .line 795
    sput-object v3, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    .line 798
    :cond_0
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .line 800
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->checkNotification(Z)Z

    .line 802
    :cond_1
    return-void
.end method

.method private static getOverlayLayoutParams(Landroid/view/WindowManager;)Landroid/view/WindowManager$LayoutParams;
    .locals 4
    .param p0, "windowManager"    # Landroid/view/WindowManager;

    .prologue
    const/4 v3, -0x2

    .line 686
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 687
    .local v0, "outSize":Landroid/graphics/Point;
    invoke-interface {p0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 689
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    .line 691
    const/4 v2, -0x3

    .line 689
    invoke-direct {v1, v3, v3, v2}, Landroid/view/WindowManager$LayoutParams;-><init>(III)V

    .line 701
    .local v1, "params":Landroid/view/WindowManager$LayoutParams;
    const v2, 0x40320

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 707
    const/16 v2, 0x7da

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 709
    const/16 v2, 0x32

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 711
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 712
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 714
    return-object v1
.end method

.method private handleIntent(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 118
    if-nez p1, :cond_1

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    const-string/jumbo v4, "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_STARTNAVI"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 126
    const-string/jumbo v4, "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_STARTNAVI"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 128
    .local v1, "nextNaviData":Landroid/os/Parcelable;
    if-eqz v1, :cond_2

    .line 129
    const-string/jumbo v4, "[MultiWindowActivity]"

    const-string/jumbo v5, "nextNaviData != null"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/map/MapFactory;->newNavigationMap()Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    move-result-object v0

    .line 132
    .local v0, "navigation":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    if-eqz v0, :cond_2

    .line 133
    const-string/jumbo v4, "[MultiWindowActivity]"

    const-string/jumbo v5, "navigation != null"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    const/4 v2, 0x1

    .line 136
    .local v2, "ret":Z
    sget-object v4, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mContext:Landroid/content/Context;

    .line 137
    check-cast v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    .line 136
    .end local v1    # "nextNaviData":Landroid/os/Parcelable;
    invoke-virtual {v0, v4, v1}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->startMultiNavi(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;)Z

    move-result v2

    .line 139
    const-string/jumbo v4, "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_STARTNAVI"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 141
    if-nez v2, :cond_2

    .line 142
    const-string/jumbo v4, "[MultiWindowActivity]"

    const-string/jumbo v5, "ret == null"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->finish()V

    .line 149
    .end local v0    # "navigation":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    .end local v2    # "ret":Z
    :cond_2
    const-string/jumbo v4, "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_TTS"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 150
    const-string/jumbo v4, "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_TTS"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 151
    .local v3, "tts":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 152
    const-string/jumbo v4, "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_TTS"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 153
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->playTTS(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private initNotificationInMultiWindow()V
    .locals 2

    .prologue
    .line 722
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 723
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotificationListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->registerOnNotificationChangeListener(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;)V

    .line 724
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->setMultiMode(Z)V

    .line 725
    return-void
.end method

.method private moveToMenu(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;)V
    .locals 3
    .param p1, "state"    # Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    .prologue
    .line 354
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$multiwindow$MultiWindowListener$OnMultiHomeListener$MultiHomeState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 368
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Error! unhandled case:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    :goto_0
    return-void

    .line 356
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->clickPhone()V

    goto :goto_0

    .line 359
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->clickMessage()V

    goto :goto_0

    .line 362
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->clickLocation()V

    goto :goto_0

    .line 365
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->clickMusic()V

    goto :goto_0

    .line 354
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    .locals 5
    .param p1, "state"    # Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;
    .param p2, "notiView"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .prologue
    .line 808
    if-nez p2, :cond_1

    .line 830
    :cond_0
    :goto_0
    return-void

    .line 813
    :cond_1
    move-object v0, p2

    .line 814
    .local v0, "changeListener":Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;
    :try_start_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_APPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    if-ne p1, v2, :cond_2

    .line 815
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;->onNotificationWillAppear()V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 824
    :catch_0
    move-exception v1

    .line 825
    .local v1, "e":Ljava/lang/UnsupportedOperationException;
    const-string/jumbo v2, "[MultiWindowActivity]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "This NotificationView("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 826
    const-string/jumbo v4, ") does not support Change Listener"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 825
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 827
    invoke-virtual {v1}, Ljava/lang/UnsupportedOperationException;->printStackTrace()V

    goto :goto_0

    .line 816
    .end local v1    # "e":Ljava/lang/UnsupportedOperationException;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->APPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    if-ne p1, v2, :cond_3

    .line 817
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;->onNotificationAppeared()V

    goto :goto_0

    .line 818
    :cond_3
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_DISAPPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    if-ne p1, v2, :cond_4

    .line 819
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;->onNotificationWillDisappear()V

    goto :goto_0

    .line 820
    :cond_4
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->DISAPPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    if-ne p1, v2, :cond_0

    .line 821
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;->onNotificationDisappeared()V
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private playTTS(Ljava/lang/String;)V
    .locals 4
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    .line 159
    const-string/jumbo v1, "[MultiWindowActivity]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "TTS :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    .line 161
    .local v0, "dialogFlow":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    .line 162
    return-void
.end method

.method private removeIndicatorView()V
    .locals 4

    .prologue
    .line 275
    const-string/jumbo v2, "[MultiWindowActivity]"

    const-string/jumbo v3, "removeIndicatorView is called."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->indicatorFragment:Landroid/support/v4/app/Fragment;

    if-eqz v2, :cond_0

    .line 277
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 279
    .local v1, "manager":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 280
    .local v0, "ft":Landroid/support/v4/app/FragmentTransaction;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->indicatorFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 281
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 282
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->indicatorFragment:Landroid/support/v4/app/Fragment;

    .line 283
    const-string/jumbo v2, "[MultiWindowActivity]"

    const-string/jumbo v3, "IndicatorView is removed."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    .end local v0    # "ft":Landroid/support/v4/app/FragmentTransaction;
    .end local v1    # "manager":Landroid/support/v4/app/FragmentManager;
    :cond_0
    return-void
.end method

.method private removeNotiVoiceUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V
    .locals 2
    .param p1, "viv"    # Lcom/nuance/drivelink/DLUiUpdater;

    .prologue
    .line 912
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    if-nez v0, :cond_1

    .line 920
    :cond_0
    :goto_0
    return-void

    .line 916
    :cond_1
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->hasNotiVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 917
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeNotiVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 918
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "Remove Voice Listener"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private removeOverlayNotiView(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    .locals 4
    .param p1, "view"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .prologue
    .line 663
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mWindowMgr:Landroid/view/WindowManager;

    if-nez v1, :cond_0

    .line 664
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 665
    const-string/jumbo v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 664
    check-cast v1, Landroid/view/WindowManager;

    sput-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mWindowMgr:Landroid/view/WindowManager;

    .line 668
    :cond_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mWindowMgr:Landroid/view/WindowManager;

    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 670
    :try_start_0
    const-string/jumbo v1, "[MultiWindowActivity]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "removeOverlayNotiView: view"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mWindowMgr:Landroid/view/WindowManager;

    invoke-interface {v1, p1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 675
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->hasNotiItem()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 676
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v1

    const/16 v2, 0xe

    if-eq v1, v2, :cond_1

    .line 678
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;

    move-result-object v1

    .line 677
    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->removeNotiVoiceUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 681
    :cond_1
    return-void

    .line 672
    :catch_0
    move-exception v0

    .line 673
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private removeOverlayNotiView(Z)V
    .locals 3
    .param p1, "bRemoveVoiceUpdater"    # Z

    .prologue
    .line 642
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mWindowMgr:Landroid/view/WindowManager;

    if-nez v0, :cond_0

    .line 643
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 644
    const-string/jumbo v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 643
    check-cast v0, Landroid/view/WindowManager;

    sput-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mWindowMgr:Landroid/view/WindowManager;

    .line 647
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mWindowMgr:Landroid/view/WindowManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_2

    .line 648
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "removeOverlayNotiView: bRemoveVoiceUpdater:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 649
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 648
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 650
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mWindowMgr:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 651
    if-eqz p1, :cond_1

    .line 652
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->hasNotiItem()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 653
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v0

    const/16 v1, 0xe

    if-eq v0, v1, :cond_1

    .line 655
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;

    move-result-object v0

    .line 654
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->removeNotiVoiceUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 658
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .line 660
    :cond_2
    return-void
.end method

.method private resetNotiFlowId()V
    .locals 1

    .prologue
    .line 515
    const-string/jumbo v0, "HOME_MODE_MULTIWINDOW"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->resetMainFlow(Ljava/lang/String;)V

    .line 516
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->sFlowId:Ljava/lang/String;

    .line 517
    return-void
.end method

.method private setNotiFlowId(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 508
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 509
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->setVoiceFlowId(Ljava/lang/String;)V

    .line 512
    :cond_0
    return-void
.end method

.method private setVoiceFlowId(Ljava/lang/String;)V
    .locals 3
    .param p1, "flowId"    # Ljava/lang/String;

    .prologue
    .line 502
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "flowId :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    sput-object p1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->sFlowId:Ljava/lang/String;

    .line 504
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->setActivityFlowID(Ljava/lang/String;)V

    .line 505
    return-void
.end method


# virtual methods
.method public isGoogleMapsInstalled()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 450
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 451
    const-string/jumbo v4, "com.google.android.apps.maps"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 452
    .local v1, "info":Landroid/content/pm/ApplicationInfo;
    if-nez v1, :cond_0

    .line 453
    const-string/jumbo v3, "[MultiWindowActivity]"

    const-string/jumbo v4, "App info for google maps is null"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 455
    :cond_0
    const/4 v2, 0x1

    .line 458
    .end local v1    # "info":Landroid/content/pm/ApplicationInfo;
    :goto_0
    return v2

    .line 456
    :catch_0
    move-exception v0

    .line 457
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string/jumbo v3, "[MultiWindowActivity]"

    const-string/jumbo v4, "google maps is not installed."

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 246
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onBackPressed()V

    .line 248
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 90
    const-string/jumbo v2, "[MultiWindowActivity]"

    const-string/jumbo v3, "onCreate"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 92
    const v2, 0x7f03001d

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->setContentView(I)V

    .line 93
    sput-object p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mContext:Landroid/content/Context;

    .line 95
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 96
    .local v0, "carApp":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 98
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v2, :cond_0

    .line 99
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 100
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mActivityStateChangeListener:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setStateChangeListener(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;)Z

    .line 103
    :cond_0
    sget-boolean v2, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->isFinished:Z

    if-eqz v2, :cond_1

    .line 104
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->initNotificationInMultiWindow()V

    .line 105
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->isFinished:Z

    .line 107
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->addIndicatorView()V

    .line 109
    const-string/jumbo v2, "HOME_MODE_MULTIWINDOW"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->resetMainFlow(Ljava/lang/String;)V

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 111
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_2

    .line 112
    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->handleIntent(Landroid/content/Intent;)V

    .line 115
    :cond_2
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 225
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 227
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->clearNotificationInMultiWindow()V

    .line 229
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->normalWindow()V

    .line 232
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->isFinished:Z

    .line 234
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 235
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setCallMultiwindow(Z)V

    .line 238
    :cond_1
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->sFlowId:Ljava/lang/String;

    .line 240
    :cond_2
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onDestroy()V

    .line 241
    return-void
.end method

.method public onFlowCommandCall(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 597
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 598
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandCall(Ljava/lang/String;)V

    .line 600
    :cond_0
    return-void
.end method

.method public onFlowCommandCancel(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 525
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 526
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandCancel(Ljava/lang/String;)V

    .line 530
    :goto_0
    return-void

    .line 528
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->finish()V

    goto :goto_0
.end method

.method public onFlowCommandExcute(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 555
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 556
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandExcute(Ljava/lang/String;)V

    .line 558
    :cond_0
    return-void
.end method

.method public onFlowCommandIgnore(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 562
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 563
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandIgnore(Ljava/lang/String;)V

    .line 565
    :cond_0
    return-void
.end method

.method public onFlowCommandLookup(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 569
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 570
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandLookup(Ljava/lang/String;)V

    .line 572
    :cond_0
    return-void
.end method

.method public onFlowCommandNext(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 612
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandNext(Ljava/lang/String;)V

    .line 615
    :cond_0
    return-void
.end method

.method public onFlowCommandNo(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 541
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandNo(Ljava/lang/String;)V

    .line 544
    :cond_0
    return-void
.end method

.method public onFlowCommandRead(Ljava/lang/String;)V
    .locals 3
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 604
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onFlowCommandRead:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 605
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 606
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandRead(Ljava/lang/String;)V

    .line 608
    :cond_0
    return-void
.end method

.method public onFlowCommandReply(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 590
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 591
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandReply(Ljava/lang/String;)V

    .line 593
    :cond_0
    return-void
.end method

.method public onFlowCommandReset(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 576
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 577
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandReset(Ljava/lang/String;)V

    .line 579
    :cond_0
    return-void
.end method

.method public onFlowCommandRoute(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 583
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 584
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandRoute(Ljava/lang/String;)V

    .line 586
    :cond_0
    return-void
.end method

.method public onFlowCommandSend(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 548
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 549
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandSend(Ljava/lang/String;)V

    .line 551
    :cond_0
    return-void
.end method

.method public onFlowCommandYes(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 534
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 535
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandYes(Ljava/lang/String;)V

    .line 537
    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 465
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 466
    const-string/jumbo v1, "[MultiWindowActivity]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onNewIntent action = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " flags = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 467
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 466
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->isFlowManagerIntent(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 471
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 472
    const-string/jumbo v2, "EXTRA_FLOW_ID"

    .line 471
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 473
    .local v0, "flowID":Ljava/lang/String;
    const-string/jumbo v1, "[MultiWindowActivity]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "flow ID :: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    const-string/jumbo v1, "DM_MAIN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 477
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->onNotifyMusic()V

    .line 482
    .end local v0    # "flowID":Ljava/lang/String;
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->handleIntent(Landroid/content/Intent;)V

    .line 484
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 202
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "onPause"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getInstance()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->unregisterListener()V

    .line 206
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "Finishing"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    :goto_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onPause()V

    .line 220
    return-void

    .line 209
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->isChangingConfigurations()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 210
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "isChangingConfigurations true"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->removeOverlayNotiView(Z)V

    goto :goto_0

    .line 213
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->dismissNotiHomeMenu()V

    .line 214
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->dismissNotiMusic()V

    .line 215
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->removeOverlayNotiView(Z)V

    .line 216
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->clearNotificationInMultiWindow()V

    goto :goto_0
.end method

.method public onPrepareFlowChange(Ljava/lang/String;)V
    .locals 3
    .param p1, "nextFlowID"    # Ljava/lang/String;

    .prologue
    .line 490
    const-string/jumbo v0, "DM_MAIN"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 491
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onPrepareFlowChange NextFlowID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->dismissNotiHomeMenu()V

    .line 493
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->setMultiModeFullSize()Z

    .line 499
    :goto_0
    return-void

    .line 496
    :cond_0
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onPrepareFlowChange = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 167
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onResume()V

    .line 169
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    const-string/jumbo v0, "[MultiWindowActivity]"

    .line 172
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Error!!! MulitWindow Mode:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 173
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 174
    const-string/jumbo v2, " Multi Window will be finished."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 172
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 171
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->finish()V

    .line 198
    :goto_0
    return-void

    .line 179
    :cond_0
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasNotification()Z

    move-result v0

    if-nez v0, :cond_1

    .line 180
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->resume(Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;)Z

    .line 183
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getInstance()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMultiHomeListener:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->registerListener(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;)V

    .line 185
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->initNotificationInMultiWindow()V

    .line 187
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasPendingReq()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 188
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->resetPendingReq()Z

    .line 193
    :goto_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->sFlowId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 194
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->sFlowId:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->setVoiceFlowId(Ljava/lang/String;)V

    .line 197
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->setMultiModeSize()Z

    goto :goto_0

    .line 190
    :cond_3
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->checkNotification(Z)Z

    goto :goto_1
.end method

.method public removeAllNotiVoiceUpdater()V
    .locals 2

    .prologue
    .line 923
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    if-nez v0, :cond_0

    .line 929
    :goto_0
    return-void

    .line 927
    :cond_0
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeNotiAllVoiceUiUpdater()V

    .line 928
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "Remove All Voice Listener"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setMultiModeFullSize()Z
    .locals 6

    .prologue
    .line 1030
    const/4 v2, 0x1

    .line 1031
    .local v2, "rtn":Z
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v3, :cond_0

    .line 1032
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1033
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 1034
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    const-string/jumbo v3, "[MultiWindowActivity]"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "setMultiModeFullSize:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1035
    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1034
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1036
    new-instance v1, Landroid/graphics/Point;

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-direct {v1, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    .line 1037
    .local v1, "point":Landroid/graphics/Point;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setIsolatedCenterPoint(Landroid/graphics/Point;)V

    .line 1042
    .end local v0    # "metrics":Landroid/util/DisplayMetrics;
    .end local v1    # "point":Landroid/graphics/Point;
    :goto_0
    return v2

    .line 1039
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setMultiModeSize()Z
    .locals 7

    .prologue
    const v6, 0x7f0d0020

    const v5, 0x7f0d001f

    .line 1007
    const/4 v1, 0x1

    .line 1009
    .local v1, "rtn":Z
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v2, :cond_0

    .line 1010
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1011
    const-string/jumbo v2, "[MultiWindowActivity]"

    .line 1012
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "setMultiModeSize:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1013
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1015
    const-string/jumbo v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1016
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1012
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1011
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1018
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1019
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1020
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 1018
    invoke-direct {v0, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 1022
    .local v0, "point":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setIsolatedCenterPoint(Landroid/graphics/Point;)V

    .line 1026
    .end local v0    # "point":Landroid/graphics/Point;
    :goto_0
    return v1

    .line 1024
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
