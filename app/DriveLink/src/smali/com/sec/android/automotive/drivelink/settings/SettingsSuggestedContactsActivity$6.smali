.class Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$6;
.super Ljava/lang/Object;
.source "SettingsSuggestedContactsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 148
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->rbSetFavorite:Landroid/widget/RadioButton;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)Landroid/widget/RadioButton;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 149
    new-instance v0, Landroid/content/Intent;

    .line 150
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 151
    const-class v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    .line 149
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 152
    .local v0, "intentAddContacts":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->startActivity(Landroid/content/Intent;)V

    .line 154
    .end local v0    # "intentAddContacts":Landroid/content/Intent;
    :cond_0
    return-void
.end method
