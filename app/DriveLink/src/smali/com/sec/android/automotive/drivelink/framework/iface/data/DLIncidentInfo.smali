.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLIncidentInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x20ac026ddb31d823L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getDelayImpactDistance()D
.end method

.method public abstract getDelayImpactFreeFlowMinutes()D
.end method

.method public abstract getDelayImpactTypicalMinutes()D
.end method

.method public abstract getDistance()D
.end method

.method public abstract getEventCode()Ljava/lang/Integer;
.end method

.method public abstract getFullDescription()Ljava/lang/String;
.end method

.method public abstract getLatitude()D
.end method

.method public abstract getLongitude()D
.end method

.method public abstract getSeverity()I
.end method

.method public abstract getType()I
.end method
