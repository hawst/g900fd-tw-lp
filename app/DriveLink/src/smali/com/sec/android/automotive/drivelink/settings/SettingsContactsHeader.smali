.class public Lcom/sec/android/automotive/drivelink/settings/SettingsContactsHeader;
.super Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;
.source "SettingsContactsHeader.java"


# instance fields
.field private header:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "header"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsHeader;->header:Ljava/lang/String;

    .line 12
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsHeader;->header:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;->HEADER:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;

    return-object v0
.end method
