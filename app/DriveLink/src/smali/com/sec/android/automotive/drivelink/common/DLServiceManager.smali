.class public Lcom/sec/android/automotive/drivelink/common/DLServiceManager;
.super Ljava/lang/Object;
.source "DLServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;,
        Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;,
        Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;
    }
.end annotation


# static fields
.field public static final MAX_INBOX:I = 0xc

.field public static final MAX_INCOM_MESSAGE:I = 0x14

.field public static final MAX_LOGS:I = 0xf

.field public static final MAX_MESSAGE_SUGGESTIONS:I = 0xc

.field public static final MAX_PHONE_SUGGESTIONS:I = 0xc

.field public static final MAX_RECOMMENDATIONS:I = 0x64

.field public static final TAG:Ljava/lang/String; = "DLServiceManager"

.field private static isDebug:Z

.field private static mInstance:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;


# instance fields
.field private mAlbumList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ">;"
        }
    .end annotation
.end field

.field private mArtistList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ">;"
        }
    .end annotation
.end field

.field private mCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;

.field private mContext:Landroid/content/Context;

.field private mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private mFolderList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;",
            ">;"
        }
    .end annotation
.end field

.field private mInboxList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;"
        }
    .end annotation
.end field

.field private mIncomingMessageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;",
            ">;"
        }
    .end annotation
.end field

.field private mIsAlbumListUpdated:Z

.field private mIsArtistListUpdated:Z

.field private mIsDriving:Z

.field private mIsFolderListUpdated:Z

.field private mIsInboxListUpdated:Z

.field private mIsIncomingMessageListUpdated:Z

.field private mIsLogListUpdated:Z

.field private mIsMessageSuggestionListUpdated:Z

.field private mIsMusicListByAlbumUpdated:Z

.field private mIsMusicListByArtistUpdated:Z

.field private mIsMusicListByFolderUpdated:Z

.field private mIsMusicListByMusicUpdated:Z

.field private mIsMusicListByPlayUpdated:Z

.field private mIsMusicListUpdated:Z

.field private mIsPhoneSuggestionListUpdated:Z

.field private mIsPlayListUpdated:Z

.field private mIsUnreadMessageCountByInboxUpdated:Z

.field private mIsUnreadMessageCountUpdated:Z

.field private mLocationReqManager:Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;

.field private mLocationSuggestionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation
.end field

.field private mLogList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;",
            ">;"
        }
    .end annotation
.end field

.field private mMessageSuggestionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation
.end field

.field private mMusicList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation
.end field

.field private mMusicListByAlbum:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;>;"
        }
    .end annotation
.end field

.field private mMusicListByArtist:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;>;"
        }
    .end annotation
.end field

.field private mMusicListByFolder:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;>;"
        }
    .end annotation
.end field

.field private mMusicListByPlay:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;>;"
        }
    .end annotation
.end field

.field private mNewMissedCallCount:I

.field private mOnUpdateLocationSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;

.field private mOnUpdateLogListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;

.field private mOnUpdateSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;

.field private mPhoneSuggestionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation
.end field

.field private mPhoneSuggestionThumbnailList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mPlayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;",
            ">;"
        }
    .end annotation
.end field

.field private mSearchedInbox:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;"
        }
    .end annotation
.end field

.field private mUnreadMessageCount:I

.field private mUnreadMessageCountByInbox:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1021
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->isDebug:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    .line 128
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 127
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 132
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 133
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getOnDLPhoneSuggestionListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkRecommendedContactForCallListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getOnDLLogListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkCallLogListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;)V

    .line 135
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mPhoneSuggestionThumbnailList:Ljava/util/ArrayList;

    .line 138
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 139
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getOnDLMessageSuggestionListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkRecommendedContactForMessageListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 141
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getOnDLMessageListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkMessageListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;)V

    .line 144
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mLocationReqManager:Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;

    .line 145
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getOnDLLocationSuggestionListener()Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->addLocationReqListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V

    .line 146
    const-string/jumbo v0, "DLServiceManager"

    const-string/jumbo v1, "getOnDLLocationSuggestionListener.........."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 151
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getOnDriveLinkMusicListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkMusicListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 155
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getOnDLDrivingListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkDrivingStatusListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;)V

    .line 156
    return-void
.end method

.method private static Debug(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "sub"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 1024
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->isDebug:Z

    if-eqz v0, :cond_0

    .line 1025
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "DLServiceManager"

    invoke-static {v0, v1, p0, p1}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1027
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mPhoneSuggestionList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mPhoneSuggestionThumbnailList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMessageSuggestionList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V
    .locals 0

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMessageSuggestionListUpdated:Z

    return-void
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;I)V
    .locals 0

    .prologue
    .line 82
    iput p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mUnreadMessageCount:I

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V
    .locals 0

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsUnreadMessageCountUpdated:Z

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIncomingMessageList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V
    .locals 0

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsIncomingMessageListUpdated:Z

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mInboxList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$17(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V
    .locals 0

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsInboxListUpdated:Z

    return-void
.end method

.method static synthetic access$18(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1023
    invoke-static {p0, p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->Debug(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$19(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;I)V
    .locals 0

    .prologue
    .line 83
    iput p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mUnreadMessageCountByInbox:I

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mPhoneSuggestionList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsUnreadMessageCountByInboxUpdated:Z

    return-void
.end method

.method static synthetic access$21(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mLocationSuggestionList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$22(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mOnUpdateLocationSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;

    return-object v0
.end method

.method static synthetic access$23(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mLocationSuggestionList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$24(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mPlayList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$25(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V
    .locals 0

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsPlayListUpdated:Z

    return-void
.end method

.method static synthetic access$26(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByPlay:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$27(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByPlay:Ljava/util/Map;

    return-void
.end method

.method static synthetic access$28(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mPlayList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$29(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V
    .locals 0

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMusicListByPlayUpdated:Z

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    return-object v0
.end method

.method static synthetic access$30(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByFolder:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$31(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V
    .locals 0

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMusicListByFolderUpdated:Z

    return-void
.end method

.method static synthetic access$32(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByArtist:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$33(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V
    .locals 0

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMusicListByArtistUpdated:Z

    return-void
.end method

.method static synthetic access$34(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByAlbum:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$35(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V
    .locals 0

    .prologue
    .line 102
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMusicListByAlbumUpdated:Z

    return-void
.end method

.method static synthetic access$36(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$37(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V
    .locals 0

    .prologue
    .line 106
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMusicListUpdated:Z

    return-void
.end method

.method static synthetic access$38(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mFolderList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$39(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V
    .locals 0

    .prologue
    .line 109
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsFolderListUpdated:Z

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$40(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByFolder:Ljava/util/Map;

    return-void
.end method

.method static synthetic access$41(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mFolderList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$42(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mArtistList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$43(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V
    .locals 0

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsArtistListUpdated:Z

    return-void
.end method

.method static synthetic access$44(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByArtist:Ljava/util/Map;

    return-void
.end method

.method static synthetic access$45(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mArtistList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$46(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mAlbumList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$47(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V
    .locals 0

    .prologue
    .line 107
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsAlbumListUpdated:Z

    return-void
.end method

.method static synthetic access$48(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByAlbum:Ljava/util/Map;

    return-void
.end method

.method static synthetic access$49(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mAlbumList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V
    .locals 0

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsPhoneSuggestionListUpdated:Z

    return-void
.end method

.method static synthetic access$50(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V
    .locals 0

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsDriving:Z

    return-void
.end method

.method static synthetic access$51(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;

    return-void
.end method

.method static synthetic access$52(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mOnUpdateSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mLogList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V
    .locals 0

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsLogListUpdated:Z

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mOnUpdateLogListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mInstance:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    if-nez v0, :cond_0

    .line 119
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mInstance:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    .line 122
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mInstance:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    return-object v0
.end method

.method private getOnDLDrivingListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;
    .locals 1

    .prologue
    .line 925
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$7;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$7;-><init>(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)V

    return-object v0
.end method

.method private getOnDLLocationSuggestionListener()Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;
    .locals 1

    .prologue
    .line 590
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$5;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$5;-><init>(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)V

    return-object v0
.end method

.method private getOnDLLogListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;
    .locals 1

    .prologue
    .line 300
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$2;-><init>(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)V

    return-object v0
.end method

.method private getOnDLMessageListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;
    .locals 1

    .prologue
    .line 429
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$4;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$4;-><init>(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)V

    return-object v0
.end method

.method private getOnDLMessageSuggestionListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;
    .locals 1

    .prologue
    .line 415
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$3;-><init>(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)V

    return-object v0
.end method

.method private getOnDLPhoneSuggestionListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;
    .locals 1

    .prologue
    .line 270
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$1;-><init>(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)V

    return-object v0
.end method

.method private getOnDriveLinkMusicListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;
    .locals 1

    .prologue
    .line 780
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;-><init>(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)V

    return-object v0
.end method


# virtual methods
.method public addLocationReqListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    .prologue
    .line 555
    if-nez p1, :cond_0

    .line 559
    :goto_0
    return-void

    .line 558
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mLocationReqManager:Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->addListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V

    goto :goto_0
.end method

.method public clearMissdeCallCount()V
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->clearNewMissedCallFromCallLog(Landroid/content/Context;)V

    .line 258
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mNewMissedCallCount:I

    .line 259
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mInstance:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    .line 160
    return-void
.end method

.method public getAlbumList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ">;"
        }
    .end annotation

    .prologue
    .line 708
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mAlbumList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getArtistList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ">;"
        }
    .end annotation

    .prologue
    .line 704
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mArtistList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    .locals 2

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getCarSpeedStatus(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;

    move-result-object v0

    return-object v0
.end method

.method public getDrivingStatus()Z
    .locals 1

    .prologue
    .line 984
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsDriving:Z

    return v0
.end method

.method public getFolderList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 700
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mFolderList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getInboxList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 366
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mInboxList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getIncommingMessageList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 379
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIncomingMessageList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLocation()Landroid/location/Location;
    .locals 2

    .prologue
    .line 1007
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getLocation(Landroid/content/Context;)Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public getLocationSuggestionList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 586
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mLocationSuggestionList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLogList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;",
            ">;"
        }
    .end annotation

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mLogList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMessageSuggestionList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMessageSuggestionList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMissedCallCount()I
    .locals 1

    .prologue
    .line 253
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mNewMissedCallCount:I

    return v0
.end method

.method public getMusicAlbumArt(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "album"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    .prologue
    .line 716
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getMusicAlbumArt(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "artist"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .prologue
    .line 712
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getMusicAlbumArt(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "folder"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;

    .prologue
    .line 720
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getMusicAlbumArt(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 724
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getMusicCount()I
    .locals 2

    .prologue
    .line 728
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getMusicCount(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public getMusicList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 696
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMusicListByAlbum(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "album"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 692
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByAlbum:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMusicListByArtist(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "artist"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 688
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByArtist:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMusicListByFolder(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "folder"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 684
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByFolder:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMusicListByPlay(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "playlist"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 676
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByPlay:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMusicPlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
    .locals 1

    .prologue
    .line 732
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getMusicPlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v0

    return-object v0
.end method

.method public getMusicRemotePlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;
    .locals 1

    .prologue
    .line 736
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getMusicRemotePlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    move-result-object v0

    return-object v0
.end method

.method public getPhoneSuggestionList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mPhoneSuggestionList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPhoneSuggestionThumbnail(I)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mPhoneSuggestionThumbnailList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 242
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mPhoneSuggestionThumbnailList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 245
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPhoneSuggestionThumbnailList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 249
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mPhoneSuggestionThumbnailList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPlayList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;",
            ">;"
        }
    .end annotation

    .prologue
    .line 672
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mPlayList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSearchedInboxList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 371
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mSearchedInbox:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getUnreadMessageCount()I
    .locals 1

    .prologue
    .line 383
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mUnreadMessageCount:I

    return v0
.end method

.method public getUnreadMessageCountByInbox()I
    .locals 1

    .prologue
    .line 387
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mUnreadMessageCountByInbox:I

    return v0
.end method

.method public isAlbumListUpdated()Z
    .locals 1

    .prologue
    .line 776
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsAlbumListUpdated:Z

    return v0
.end method

.method public isArtistListUpdated()Z
    .locals 1

    .prologue
    .line 772
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsArtistListUpdated:Z

    return v0
.end method

.method public isFolderListUpdated()Z
    .locals 1

    .prologue
    .line 768
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsFolderListUpdated:Z

    return v0
.end method

.method public isInboxListUpdated()Z
    .locals 1

    .prologue
    .line 395
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsInboxListUpdated:Z

    return v0
.end method

.method public isIncomingMessageListUpdated()Z
    .locals 1

    .prologue
    .line 403
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsIncomingMessageListUpdated:Z

    return v0
.end method

.method public isLogListUpdated()Z
    .locals 1

    .prologue
    .line 266
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsLogListUpdated:Z

    return v0
.end method

.method public isMessageSuggestionListUpdated()Z
    .locals 1

    .prologue
    .line 391
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMessageSuggestionListUpdated:Z

    return v0
.end method

.method public isMusicListByAlbumUpdated()Z
    .locals 1

    .prologue
    .line 760
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMusicListByAlbumUpdated:Z

    return v0
.end method

.method public isMusicListByArtistUpdated()Z
    .locals 1

    .prologue
    .line 756
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMusicListByArtistUpdated:Z

    return v0
.end method

.method public isMusicListByFolderUpdated()Z
    .locals 1

    .prologue
    .line 752
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMusicListByFolderUpdated:Z

    return v0
.end method

.method public isMusicListByMusicUpdated()Z
    .locals 1

    .prologue
    .line 748
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMusicListByMusicUpdated:Z

    return v0
.end method

.method public isMusicListByPlayUpdated()Z
    .locals 1

    .prologue
    .line 744
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMusicListByPlayUpdated:Z

    return v0
.end method

.method public isMusicListUpdated()Z
    .locals 1

    .prologue
    .line 764
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMusicListUpdated:Z

    return v0
.end method

.method public isPhoneSuggestionListUpdated()Z
    .locals 1

    .prologue
    .line 262
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsPhoneSuggestionListUpdated:Z

    return v0
.end method

.method public isPlayListUpdated()Z
    .locals 1

    .prologue
    .line 740
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsPlayListUpdated:Z

    return v0
.end method

.method public isUnreadMessageCountByInboxUpdated()Z
    .locals 1

    .prologue
    .line 407
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsUnreadMessageCountByInboxUpdated:Z

    return v0
.end method

.method public isUnreadMessageCountUpdated()Z
    .locals 1

    .prologue
    .line 399
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsUnreadMessageCountUpdated:Z

    return v0
.end method

.method public removeLocationReqListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    .prologue
    .line 563
    if-nez p1, :cond_0

    .line 566
    :goto_0
    return-void

    .line 565
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mLocationReqManager:Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->removeListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V

    goto :goto_0
.end method

.method public requestUnreadMessageCountBySync()I
    .locals 2

    .prologue
    .line 1017
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestUnreadMessageCountBySync(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public responseCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    .locals 1

    .prologue
    .line 988
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;

    return-object v0
.end method

.method public setCarSpeedThreshold(III)V
    .locals 2
    .param p1, "minSpeed"    # I
    .param p2, "maxSpeed"    # I
    .param p3, "speedCheckDurationInSec"    # I

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setCarSpeedThreshold(Landroid/content/Context;III)V

    .line 1014
    return-void
.end method

.method public setMessageMessageUpdateListener()V
    .locals 2

    .prologue
    .line 334
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 335
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getOnDLMessageListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkMessageListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;)V

    .line 336
    return-void
.end method

.method public setMessageStateToRead(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)V
    .locals 2
    .param p1, "message"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .prologue
    .line 411
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestChangeMessageStatusToRead(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)V

    .line 412
    return-void
.end method

.method public setMessageSuggestionListUpdateListener()V
    .locals 2

    .prologue
    .line 329
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 330
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getOnDLMessageSuggestionListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkRecommendedContactForMessageListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;)V

    .line 331
    return-void
.end method

.method public setOnUpdateLocationSugessionListListener(Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;

    .prologue
    .line 223
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mOnUpdateLocationSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;

    .line 224
    return-void
.end method

.method public setOnUpdateLogListListener(Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mOnUpdateLogListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;

    .line 214
    return-void
.end method

.method public setOnUpdateSugessionListListener(Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;

    .prologue
    .line 218
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mOnUpdateSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;

    .line 219
    return-void
.end method

.method public setPhoneSuggestionListUpdateListener()V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 191
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getOnDLPhoneSuggestionListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkRecommendedContactForCallListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;)V

    .line 192
    return-void
.end method

.method public setPlacesSuggestionListUpdateListener()V
    .locals 2

    .prologue
    .line 581
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    .line 582
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getOnDLLocationSuggestionListener()Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    move-result-object v1

    .line 581
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->addLocationReqListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V

    .line 583
    return-void
.end method

.method public setSearchedInboxList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 375
    .local p1, "searchedInbox":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mSearchedInbox:Ljava/util/ArrayList;

    .line 376
    return-void
.end method

.method public updateAlbumList()V
    .locals 2

    .prologue
    .line 667
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsAlbumListUpdated:Z

    .line 668
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestAlbumList(Landroid/content/Context;)V

    .line 669
    return-void
.end method

.method public updateAll()V
    .locals 0

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updatePhoneSuggestionList()V

    .line 165
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateLogList()V

    .line 168
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateMessageSuggestionList()V

    .line 169
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateInboxList()V

    .line 170
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateUnreadMessageCount()V

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateLocationList()V

    .line 174
    return-void
.end method

.method public updateAllMusic()V
    .locals 2

    .prologue
    .line 614
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestAlbumList(Landroid/content/Context;)V

    .line 615
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestArtistList(Landroid/content/Context;)V

    .line 616
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestMusicFolderList(Landroid/content/Context;)V

    .line 617
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestPlaylistList(Landroid/content/Context;)V

    .line 618
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestMusicList(Landroid/content/Context;)V

    .line 619
    return-void
.end method

.method public updateArtistList()V
    .locals 2

    .prologue
    .line 662
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsArtistListUpdated:Z

    .line 663
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestArtistList(Landroid/content/Context;)V

    .line 664
    return-void
.end method

.method public updateFolderList()V
    .locals 2

    .prologue
    .line 657
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsFolderListUpdated:Z

    .line 658
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestMusicFolderList(Landroid/content/Context;)V

    .line 659
    return-void
.end method

.method public updateInboxList()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 339
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsInboxListUpdated:Z

    .line 342
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    const/16 v2, 0xc

    invoke-interface {v0, v1, v2, v3, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestInboxList(Landroid/content/Context;IZI)V

    .line 343
    return-void
.end method

.method public updateIncomingMessageList(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;)V
    .locals 3
    .param p1, "inbox"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .prologue
    .line 346
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsIncomingMessageListUpdated:Z

    .line 347
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    .line 348
    const/16 v2, 0x14

    .line 347
    invoke-interface {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestIncommingMessageList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;I)V

    .line 349
    return-void
.end method

.method public updateLocationList()V
    .locals 3

    .prologue
    .line 569
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    if-nez v0, :cond_0

    .line 570
    const-string/jumbo v0, "DLServiceManager"

    const-string/jumbo v1, "service interface is null"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 578
    :goto_0
    return-void

    .line 574
    :cond_0
    const-string/jumbo v0, "DLServiceManager"

    const-string/jumbo v1, "requestRecommendedLocationList"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 576
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    .line 577
    const/16 v2, 0x64

    .line 576
    invoke-interface {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestRecommendedLocationList(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public updateLogList()V
    .locals 3

    .prologue
    .line 195
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsLogListUpdated:Z

    .line 196
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    const/16 v2, 0xf

    invoke-interface {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestCallLogList(Landroid/content/Context;I)V

    .line 197
    return-void
.end method

.method public updateMessageSuggestionList()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 319
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMessageSuggestionListUpdated:Z

    .line 321
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 323
    .local v0, "preference":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v4, "PREF_SETTINGS_SUGGEST_CONTACTS"

    .line 322
    invoke-virtual {v0, v4, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v1

    .line 324
    .local v1, "useFavoriteList":Z
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    .line 325
    const/16 v6, 0xc

    if-eqz v1, :cond_0

    .line 324
    :goto_0
    invoke-interface {v4, v5, v6, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestRecommendedContactListForMessage(Landroid/content/Context;IZ)V

    .line 326
    return-void

    :cond_0
    move v2, v3

    .line 325
    goto :goto_0
.end method

.method public updateMissedCallCount()I
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 228
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getNewMissedCallCount(Landroid/content/Context;)I

    move-result v0

    .line 227
    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mNewMissedCallCount:I

    .line 229
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mNewMissedCallCount:I

    return v0
.end method

.method public updateMusicList()V
    .locals 2

    .prologue
    .line 652
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMusicListUpdated:Z

    .line 653
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestMusicList(Landroid/content/Context;)V

    .line 654
    return-void
.end method

.method public updateMusicListByAlbum(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)V
    .locals 2
    .param p1, "album"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    .prologue
    .line 647
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMusicListByAlbumUpdated:Z

    .line 648
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)V

    .line 649
    return-void
.end method

.method public updateMusicListByArtist(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)V
    .locals 2
    .param p1, "artist"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .prologue
    .line 642
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMusicListByArtistUpdated:Z

    .line 643
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)V

    .line 644
    return-void
.end method

.method public updateMusicListByFolder(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;)V
    .locals 2
    .param p1, "folder"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;

    .prologue
    .line 637
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMusicListByFolderUpdated:Z

    .line 638
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;)V

    .line 639
    return-void
.end method

.method public updateMusicListByMusic(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 2
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 632
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMusicListByMusicUpdated:Z

    .line 633
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    .line 634
    return-void
.end method

.method public updateMusicListByPlay(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;)V
    .locals 2
    .param p1, "playlist"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;

    .prologue
    .line 627
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsMusicListByPlayUpdated:Z

    .line 628
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;)V

    .line 629
    return-void
.end method

.method public updatePhoneSuggestionList()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 180
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsPhoneSuggestionListUpdated:Z

    .line 182
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 184
    .local v0, "preference":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v4, "PREF_SETTINGS_SUGGEST_CONTACTS"

    .line 183
    invoke-virtual {v0, v4, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v1

    .line 185
    .local v1, "useFavoriteList":Z
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    .line 186
    const/16 v6, 0xc

    if-eqz v1, :cond_0

    .line 185
    :goto_0
    invoke-interface {v4, v5, v6, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestRecommendedContactListForCall(Landroid/content/Context;IZ)V

    .line 187
    return-void

    :cond_0
    move v2, v3

    .line 186
    goto :goto_0
.end method

.method public updatePlayList()V
    .locals 2

    .prologue
    .line 622
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsPlayListUpdated:Z

    .line 623
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestPlaylistList(Landroid/content/Context;)V

    .line 624
    return-void
.end method

.method public updateStopDrivingStatus()V
    .locals 2

    .prologue
    .line 999
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestStopDrivingStatusMonitoring(Landroid/content/Context;)V

    .line 1000
    return-void
.end method

.method public updateUnreadMessageCount()V
    .locals 2

    .prologue
    .line 352
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsUnreadMessageCountUpdated:Z

    .line 353
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestUnreadMessageCount(Landroid/content/Context;)V

    .line 354
    return-void
.end method

.method public updateUnreadMessageCountByInbox(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;)V
    .locals 2
    .param p1, "inbox"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .prologue
    .line 357
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mIsUnreadMessageCountByInboxUpdated:Z

    .line 358
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestUnreadMessageCountByInbox(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;)V

    .line 359
    return-void
.end method
