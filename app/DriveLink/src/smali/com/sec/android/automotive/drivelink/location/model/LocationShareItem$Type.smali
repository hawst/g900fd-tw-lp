.class public final enum Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;
.super Ljava/lang/Enum;
.source "LocationShareItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CONTACT:Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

.field public static final enum HEADER:Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    const-string/jumbo v1, "HEADER"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;->HEADER:Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    new-instance v0, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    const-string/jumbo v1, "CONTACT"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;->CONTACT:Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    .line 27
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    sget-object v1, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;->HEADER:Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;->CONTACT:Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
