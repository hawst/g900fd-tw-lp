.class Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity$2;
.super Ljava/lang/Object;
.source "SettingsRegisterCarActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->cbAutoLaunch:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    .line 141
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->cbAutoLaunch:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->preference:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 144
    const-string/jumbo v1, "PREF_SETTINGS_MY_CAR_AUTO_LAUNCH"

    .line 145
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->cbAutoLaunch:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;)Landroid/widget/CheckBox;

    move-result-object v2

    .line 146
    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    .line 144
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 147
    return-void

    .line 141
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
