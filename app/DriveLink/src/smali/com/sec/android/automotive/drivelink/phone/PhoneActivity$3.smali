.class Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;
.super Ljava/lang/Object;
.source "PhoneActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    .line 1268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    .locals 1

    .prologue
    .line 1268
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    return-object v0
.end method


# virtual methods
.method public onUpdateLogList(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1272
    .local p1, "calllogList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;>;"
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-static {v1, p1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$6(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Ljava/util/ArrayList;)V

    .line 1274
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->isLogClicked:Z
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$7(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1275
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mLogsList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$8(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 1284
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mLogTTS:Z
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$9(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1285
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    .line 1287
    .local v0, "dialogFlow":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1288
    const v2, 0x7f0a013a

    .line 1287
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1289
    new-instance v2, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3$1;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3$1;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;)V

    .line 1286
    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->ttsAnyway(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 1317
    .end local v0    # "dialogFlow":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$4(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$11(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V

    .line 1318
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mPreviousMode:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$12(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$13(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V

    .line 1319
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$4(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I

    move-result v2

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setCurrentMode(I)V
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$14(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V

    .line 1320
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$15(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    move-result-object v1

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->setCurrentMode(I)V

    .line 1321
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$16(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V

    .line 1328
    :cond_1
    :goto_0
    return-void

    .line 1324
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->showSuggestionList()V
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$3(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    goto :goto_0
.end method
