.class public Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ConnectivityManagerBroadcastReceiver.java"


# instance fields
.field private BT_FORCE_RECOVERY:Ljava/lang/String;

.field private intention:Landroid/content/Intent;

.field private mContext:Landroid/content/Context;

.field private mDLBTHandler:Landroid/os/Handler;

.field public mDLBTPairingInfoImpl:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->mDLBTPairingInfoImpl:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;

    .line 18
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->mDLBTHandler:Landroid/os/Handler;

    .line 21
    const-string/jumbo v0, "android.bluetooth.device.action.BT_FORCE_RECOVERY"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->BT_FORCE_RECOVERY:Ljava/lang/String;

    .line 24
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->mDLBTHandler:Landroid/os/Handler;

    .line 25
    return-void
.end method

.method private captureBTPairingType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;
    .locals 4

    .prologue
    .line 247
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->intention:Landroid/content/Intent;

    .line 248
    const-string/jumbo v2, "android.bluetooth.device.extra.PAIRING_VARIANT"

    const/high16 v3, -0x80000000

    .line 247
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 250
    .local v0, "mType":I
    packed-switch v0, :pswitch_data_0

    .line 256
    :pswitch_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    :goto_0
    return-object v1

    .line 252
    :pswitch_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;->NOT_SIMPLE_SECURE_PAIRING:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    goto :goto_0

    .line 254
    :pswitch_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;->SIMPLE_SECURE_PAIRING:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    goto :goto_0

    .line 250
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private captureDeviceName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 261
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->intention:Landroid/content/Intent;

    .line 262
    const-string/jumbo v3, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 263
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    .line 265
    .local v1, "deviceName":Ljava/lang/String;
    return-object v1
.end method

.method private capturePassKey()I
    .locals 3

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->intention:Landroid/content/Intent;

    const-string/jumbo v1, "android.bluetooth.device.extra.PAIRING_KEY"

    .line 270
    const/high16 v2, -0x80000000

    .line 269
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private sendMessage(IILjava/lang/Object;)V
    .locals 2
    .param p1, "notiType"    # I
    .param p2, "arg1"    # I
    .param p3, "data"    # Ljava/lang/Object;

    .prologue
    .line 274
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->mDLBTHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 275
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 276
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 277
    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 279
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->mDLBTHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 280
    return-void
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 38
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, "action":Ljava/lang/String;
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->intention:Landroid/content/Intent;

    .line 41
    const-string/jumbo v10, "android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 43
    const-string/jumbo v10, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 44
    const-string/jumbo v10, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 45
    :cond_0
    const-string/jumbo v10, "android.bluetooth.profile.extra.STATE"

    .line 46
    const/high16 v11, -0x80000000

    .line 45
    invoke-virtual {p2, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    .line 48
    .local v9, "state":I
    packed-switch v9, :pswitch_data_0

    .line 120
    .end local v9    # "state":I
    :cond_1
    :goto_0
    :pswitch_0
    const-string/jumbo v10, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 122
    const-string/jumbo v10, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/bluetooth/BluetoothDevice;

    .line 123
    .local v4, "device":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    .line 126
    .local v6, "deviceName":Ljava/lang/String;
    const/4 v10, 0x5

    .line 127
    const/4 v11, 0x0

    .line 125
    invoke-direct {p0, v10, v11, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    .line 128
    const/16 v10, 0x6c

    .line 129
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 128
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    .line 244
    .end local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v6    # "deviceName":Ljava/lang/String;
    :cond_2
    :goto_1
    return-void

    .line 51
    .restart local v9    # "state":I
    :pswitch_1
    const-string/jumbo v10, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/bluetooth/BluetoothDevice;

    .line 52
    .restart local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    .line 54
    .restart local v6    # "deviceName":Ljava/lang/String;
    const/4 v10, 0x5

    .line 55
    const/4 v11, 0x0

    .line 53
    invoke-direct {p0, v10, v11, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto :goto_0

    .line 59
    .end local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v6    # "deviceName":Ljava/lang/String;
    :pswitch_2
    const/16 v10, 0x6a

    .line 60
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 58
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto :goto_0

    .line 64
    :pswitch_3
    const-string/jumbo v10, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    .line 65
    .local v2, "dcDevice":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    .line 67
    .local v3, "dcDeviceName":Ljava/lang/String;
    const/4 v10, 0x6

    .line 68
    const/4 v11, 0x0

    .line 66
    invoke-direct {p0, v10, v11, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto :goto_0

    .line 72
    .end local v2    # "dcDevice":Landroid/bluetooth/BluetoothDevice;
    .end local v3    # "dcDeviceName":Ljava/lang/String;
    :pswitch_4
    const/16 v10, 0x6b

    .line 73
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 71
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto :goto_0

    .line 76
    .end local v9    # "state":I
    :cond_3
    const-string/jumbo v10, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 78
    const-string/jumbo v10, "android.bluetooth.adapter.action.DISCOVERY_STARTED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 79
    const/16 v10, 0x67

    const/4 v11, 0x0

    .line 80
    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 79
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_0

    .line 81
    :cond_4
    const-string/jumbo v10, "android.bluetooth.adapter.action.LOCAL_NAME_CHANGED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 83
    const-string/jumbo v10, "android.bluetooth.adapter.action.REQUEST_DISCOVERABLE"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 85
    const-string/jumbo v10, "android.bluetooth.adapter.action.REQUEST_ENABLE"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 87
    const-string/jumbo v10, "android.bluetooth.adapter.action.SCAN_MODE_CHANGED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 89
    const-string/jumbo v10, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 90
    const-string/jumbo v10, "android.bluetooth.adapter.extra.STATE"

    .line 91
    const/high16 v11, -0x80000000

    .line 90
    invoke-virtual {p2, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    .line 93
    .restart local v9    # "state":I
    packed-switch v9, :pswitch_data_1

    :pswitch_5
    goto/16 :goto_0

    .line 114
    :pswitch_6
    const/16 v10, 0x8

    .line 115
    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 113
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_0

    .line 96
    :pswitch_7
    const/16 v10, 0x65

    const/4 v11, 0x0

    .line 97
    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 95
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_0

    .line 102
    :pswitch_8
    const/16 v10, 0x66

    .line 103
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 102
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_0

    .line 109
    :pswitch_9
    const/4 v10, 0x7

    .line 110
    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 108
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_0

    .line 131
    .end local v9    # "state":I
    :cond_5
    const-string/jumbo v10, "android.bluetooth.device.action.ACL_DISCONNECT_REQUESTED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 133
    const-string/jumbo v10, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 135
    const/4 v10, 0x6

    .line 136
    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 134
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_1

    .line 137
    :cond_6
    const-string/jumbo v10, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 139
    const-string/jumbo v10, "android.bluetooth.device.extra.BOND_STATE"

    const/high16 v11, -0x80000000

    .line 138
    invoke-virtual {p2, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    .line 141
    .restart local v9    # "state":I
    packed-switch v9, :pswitch_data_2

    .line 200
    const/4 v10, 0x4

    .line 201
    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 199
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_1

    .line 146
    :pswitch_a
    const-string/jumbo v10, "android.bluetooth.device.extra.REASON"

    .line 147
    const/high16 v11, -0x80000000

    .line 146
    invoke-virtual {p2, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 149
    .local v8, "reason":I
    packed-switch v8, :pswitch_data_3

    :pswitch_b
    goto/16 :goto_1

    .line 152
    :pswitch_c
    const/4 v10, -0x1

    .line 153
    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 151
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_1

    .line 157
    :pswitch_d
    const/4 v10, -0x2

    .line 158
    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 156
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_1

    .line 162
    :pswitch_e
    const/4 v10, -0x3

    .line 163
    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 161
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_1

    .line 167
    :pswitch_f
    const/4 v10, -0x4

    .line 168
    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 166
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_1

    .line 172
    :pswitch_10
    const/4 v10, -0x5

    .line 173
    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 171
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_1

    .line 177
    :pswitch_11
    const/4 v10, -0x6

    .line 178
    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 176
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_1

    .line 182
    :pswitch_12
    const/4 v10, -0x7

    .line 183
    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 181
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_1

    .line 190
    .end local v8    # "reason":I
    :pswitch_13
    const/16 v10, 0x69

    .line 191
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 190
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_1

    .line 195
    :pswitch_14
    const/4 v10, 0x3

    .line 196
    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 194
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_1

    .line 204
    .end local v9    # "state":I
    :cond_7
    const-string/jumbo v10, "android.bluetooth.device.action.FOUND"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 206
    const-string/jumbo v10, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/bluetooth/BluetoothDevice;

    .line 209
    .restart local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    const/16 v10, 0x9

    const/4 v11, 0x0

    .line 208
    invoke-direct {p0, v10, v11, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_1

    .line 211
    .end local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_8
    const-string/jumbo v10, "android.bluetooth.device.action.NAME_CHANGED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 213
    const-string/jumbo v10, "android.bluetooth.device.action.PAIRING_REQUEST"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 214
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->captureBTPairingType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    move-result-object v1

    .line 215
    .local v1, "btPairingType":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->captureDeviceName()Ljava/lang/String;

    move-result-object v6

    .line 216
    .restart local v6    # "deviceName":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->capturePassKey()I

    move-result v7

    .line 218
    .local v7, "passKey":I
    new-instance v10, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;

    .line 219
    invoke-direct {v10, v1, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;-><init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;Ljava/lang/String;I)V

    .line 218
    iput-object v10, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->mDLBTPairingInfoImpl:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;

    .line 221
    const/4 v10, 0x1

    .line 222
    const/4 v11, 0x0

    iget-object v12, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->mDLBTPairingInfoImpl:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;

    .line 220
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    .line 225
    const-string/jumbo v10, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/bluetooth/BluetoothDevice;

    .line 226
    .restart local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    .line 227
    .local v5, "deviceAddress":Ljava/lang/String;
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {p0, v10, v11, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    .line 229
    const/16 v10, 0x68

    .line 230
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 229
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_1

    .line 236
    .end local v1    # "btPairingType":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;
    .end local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v5    # "deviceAddress":Ljava/lang/String;
    .end local v6    # "deviceName":Ljava/lang/String;
    .end local v7    # "passKey":I
    :cond_9
    const-string/jumbo v10, "android.bluetooth.device.action.UUID"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 238
    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->BT_FORCE_RECOVERY:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 240
    const/16 v10, 0xa

    .line 241
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 239
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_1

    .line 48
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_4
    .end packed-switch

    .line 93
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_9
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_0
    .end packed-switch

    .line 141
    :pswitch_data_2
    .packed-switch 0xa
        :pswitch_a
        :pswitch_13
        :pswitch_14
    .end packed-switch

    .line 149
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_c
        :pswitch_d
        :pswitch_b
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->mContext:Landroid/content/Context;

    .line 29
    return-void
.end method
