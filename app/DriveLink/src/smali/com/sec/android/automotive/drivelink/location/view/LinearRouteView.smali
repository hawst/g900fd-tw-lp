.class public Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;
.super Landroid/widget/RelativeLayout;
.source "LinearRouteView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$OnParticipantClickListener;
    }
.end annotation


# static fields
.field private static final CIRCLE_SIZE_DP:I = 0x22

.field public static final ITEM_SIZE_DP:I = 0x36


# instance fields
.field private btnReinvite:Landroid/widget/Button;

.field private buttonsArea:Landroid/view/View;

.field private final cachedViews:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private clickListener:Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$OnParticipantClickListener;

.field private context:Landroid/content/Context;

.field private data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;",
            ">;"
        }
    .end annotation
.end field

.field private dlinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private imgHost:Landroid/widget/ImageView;

.field private imgUser:Landroid/widget/ImageView;

.field private minArea:Landroid/view/View;

.field private tvLetterUser:Landroid/widget/TextView;

.field private tvName:Landroid/widget/TextView;

.field private tvNotMoving:Landroid/widget/TextView;

.field private tvTimeLeft:Landroid/widget/TextView;

.field private userArea:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 62
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->cachedViews:Ljava/util/Map;

    .line 68
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->initView(Landroid/content/Context;)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->cachedViews:Ljava/util/Map;

    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->initView(Landroid/content/Context;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->cachedViews:Ljava/util/Map;

    .line 80
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->initView(Landroid/content/Context;)V

    .line 81
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;)Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$OnParticipantClickListener;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->clickListener:Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$OnParticipantClickListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;)Ljava/util/List;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->data:Ljava/util/List;

    return-object v0
.end method

.method private adjustChildPosition(Landroid/view/View;ID)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "barHeight"    # I
    .param p3, "progress"    # D

    .prologue
    const/16 v7, 0x36

    .line 296
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/16 v4, 0x22

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->dpToPx(Landroid/content/Context;I)I

    move-result v0

    .line 297
    .local v0, "base":I
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v7}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->dpToPx(Landroid/content/Context;I)I

    move-result v3

    div-int/lit8 v1, v3, 0x2

    .line 298
    .local v1, "itemOffset":I
    int-to-double v3, p2

    mul-double/2addr v3, p3

    int-to-double v5, v0

    add-double/2addr v3, v5

    int-to-double v5, v1

    sub-double/2addr v3, v5

    double-to-float v2, v3

    .line 300
    .local v2, "position":F
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->getBottom()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v7}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->dpToPx(Landroid/content/Context;I)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v3, v2, v3

    if-ltz v3, :cond_0

    .line 301
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->getBottom()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v7}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->dpToPx(Landroid/content/Context;I)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    int-to-float v2, v3

    .line 304
    :cond_0
    invoke-virtual {p1, v2}, Landroid/view/View;->setY(F)V

    .line 305
    return-void
.end method

.method private buildLayout(II)V
    .locals 6
    .param p1, "t"    # I
    .param p2, "b"    # I

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->isInEditMode()Z

    move-result v3

    if-nez v3, :cond_0

    .line 128
    sub-int v3, p2, p1

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->getContext()Landroid/content/Context;

    move-result-object v4

    const/16 v5, 0x22

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->dpToPx(Landroid/content/Context;I)I

    move-result v4

    sub-int v0, v3, v4

    .line 130
    .local v0, "barHeight":I
    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->removeViewsInLayout(II)V

    .line 132
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->data:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 149
    .end local v0    # "barHeight":I
    :cond_0
    return-void

    .line 132
    .restart local v0    # "barHeight":I
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;

    .line 133
    .local v1, "participant":Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;
    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->getParticipantView(Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;)Landroid/view/View;

    move-result-object v2

    .line 134
    .local v2, "view":Landroid/view/View;
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->addView(Landroid/view/View;)V

    .line 136
    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->getParticipantProgress(Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;)D

    move-result-wide v4

    .line 135
    invoke-direct {p0, v2, v0, v4, v5}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->adjustChildPosition(Landroid/view/View;ID)V

    .line 137
    new-instance v4, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$1;

    invoke-direct {v4, p0, v1}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$1;-><init>(Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public static dpToPx(Landroid/content/Context;I)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dp"    # I

    .prologue
    .line 314
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 315
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 317
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    int-to-float v2, p1

    .line 318
    iget v3, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v3, v3

    const/high16 v4, 0x43200000    # 160.0f

    div-float/2addr v3, v4

    .line 317
    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 319
    .local v1, "px":I
    return v1
.end method

.method private getParticipantProgress(Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;)D
    .locals 12
    .param p1, "targetPerson"    # Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;

    .prologue
    const-wide/16 v7, 0x0

    .line 159
    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 160
    .local v2, "nearestPoint":D
    const-wide/16 v0, 0x1

    .line 162
    .local v0, "furthestPoint":D
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->data:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_1

    .line 173
    sub-double v5, v0, v2

    .line 175
    .local v5, "range":D
    cmpl-double v9, v5, v7

    if-nez v9, :cond_3

    :goto_1
    return-wide v7

    .line 162
    .end local v5    # "range":D
    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;

    .line 164
    .local v4, "participant":Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getDistance()D

    move-result-wide v10

    cmpg-double v10, v10, v2

    if-gez v10, :cond_2

    .line 165
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getDistance()D

    move-result-wide v2

    .line 168
    :cond_2
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getDistance()D

    move-result-wide v10

    cmpl-double v10, v10, v0

    if-lez v10, :cond_0

    .line 169
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getDistance()D

    move-result-wide v0

    goto :goto_0

    .line 175
    .end local v4    # "participant":Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;
    .restart local v5    # "range":D
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getDistance()D

    move-result-wide v7

    div-double/2addr v7, v5

    goto :goto_1
.end method

.method private getParticipantView(Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;)Landroid/view/View;
    .locals 12
    .param p1, "participant"    # Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;

    .prologue
    .line 187
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->cachedViews:Ljava/util/Map;

    invoke-interface {v7, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 189
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->cachedViews:Ljava/util/Map;

    invoke-interface {v7, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    .line 263
    .local v6, "view":Landroid/view/View;
    :goto_0
    const-wide/16 v4, 0x0

    .line 265
    .local v4, "timeLeft":D
    const-wide/16 v7, 0x0

    cmpl-double v7, v4, v7

    if-lez v7, :cond_9

    .line 266
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->tvTimeLeft:Landroid/widget/TextView;

    const-string/jumbo v8, "%.2f min"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    :goto_1
    const/4 v3, 0x0

    .line 274
    .local v3, "isMoving":Z
    if-eqz v3, :cond_a

    .line 275
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->tvNotMoving:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 280
    :goto_2
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->isMe()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 281
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->tvTimeLeft:Landroid/widget/TextView;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setActivated(Z)V

    .line 284
    :cond_0
    return-object v6

    .line 193
    .end local v3    # "isMoving":Z
    .end local v4    # "timeLeft":D
    .end local v6    # "view":Landroid/view/View;
    :cond_1
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->context:Landroid/content/Context;

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 194
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f030054

    const/4 v8, 0x0

    invoke-virtual {v2, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 196
    .restart local v6    # "view":Landroid/view/View;
    const v7, 0x7f0901e0

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->userArea:Landroid/view/View;

    .line 197
    const v7, 0x7f0901e1

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->buttonsArea:Landroid/view/View;

    .line 198
    const v7, 0x7f0901e4

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->minArea:Landroid/view/View;

    .line 199
    const v7, 0x7f0901a9

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->imgUser:Landroid/widget/ImageView;

    .line 200
    const v7, 0x7f090123

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->tvName:Landroid/widget/TextView;

    .line 201
    const v7, 0x7f0901aa

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->tvLetterUser:Landroid/widget/TextView;

    .line 202
    const v7, 0x7f0901e5

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->tvTimeLeft:Landroid/widget/TextView;

    .line 203
    const v7, 0x7f0901e3

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->imgHost:Landroid/widget/ImageView;

    .line 204
    const v7, 0x7f0901e7

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->btnReinvite:Landroid/widget/Button;

    .line 205
    const v7, 0x7f0901e6

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->tvNotMoving:Landroid/widget/TextView;

    .line 207
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->userArea:Landroid/view/View;

    if-eqz v7, :cond_2

    .line 208
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->userArea:Landroid/view/View;

    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 209
    const/4 v9, -0x1

    const/4 v10, -0x2

    invoke-direct {v8, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 208
    invoke-virtual {v7, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 212
    :cond_2
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->dlinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->getContext()Landroid/content/Context;

    move-result-object v8

    .line 213
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getDlContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v9

    .line 212
    invoke-interface {v7, v8, v9}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 216
    .local v1, "image":Landroid/graphics/Bitmap;
    if-nez v1, :cond_7

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getName()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_7

    .line 217
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->imgUser:Landroid/widget/ImageView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 218
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->tvLetterUser:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 220
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 221
    const v8, 0x7f0201a3

    .line 222
    const v9, 0x7f0201a2

    .line 220
    invoke-static {v7, v8, v9}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 223
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->imgUser:Landroid/widget/ImageView;

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 236
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :goto_3
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->tvName:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 237
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->tvName:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->isMe()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 240
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->tvName:Landroid/widget/TextView;

    const v8, 0x7f0a0318

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 241
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->userArea:Landroid/view/View;

    const v8, 0x7f0200f5

    invoke-virtual {v7, v8}, Landroid/view/View;->setBackgroundResource(I)V

    .line 244
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->isHost()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 245
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->minArea:Landroid/view/View;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 246
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->buttonsArea:Landroid/view/View;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 247
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->btnReinvite:Landroid/widget/Button;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 248
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->imgHost:Landroid/widget/ImageView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 251
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->hasLeft()Z

    move-result v7

    if-nez v7, :cond_5

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->hasLeft()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 252
    :cond_5
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->userArea:Landroid/view/View;

    const v8, 0x7f02010b

    invoke-virtual {v7, v8}, Landroid/view/View;->setBackgroundResource(I)V

    .line 253
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->minArea:Landroid/view/View;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 254
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->buttonsArea:Landroid/view/View;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 255
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->btnReinvite:Landroid/widget/Button;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 258
    :cond_6
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->cachedViews:Ljava/util/Map;

    invoke-interface {v7, p1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 224
    :cond_7
    if-nez v1, :cond_8

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getName()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_8

    .line 225
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->imgUser:Landroid/widget/ImageView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 226
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->tvLetterUser:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 227
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->tvLetterUser:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 229
    :cond_8
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->imgUser:Landroid/widget/ImageView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 230
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->tvLetterUser:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 231
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 232
    const v8, 0x7f0201a2

    .line 231
    invoke-static {v7, v1, v8}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 233
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->imgUser:Landroid/widget/ImageView;

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_3

    .line 268
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "image":Landroid/graphics/Bitmap;
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    .restart local v4    # "timeLeft":D
    :cond_9
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->tvTimeLeft:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 269
    const v9, 0x7f0a0337

    .line 268
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 277
    .restart local v3    # "isMoving":Z
    :cond_a
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->tvNotMoving:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method private initView(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->context:Landroid/content/Context;

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->isInEditMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    const v1, -0xff01

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->setBackgroundColor(I)V

    .line 100
    :goto_0
    return-void

    .line 95
    :cond_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 96
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030053

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 98
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 97
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->dlinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    goto :goto_0
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 117
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 119
    invoke-direct {p0, p3, p5}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->buildLayout(II)V

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->invalidate()V

    .line 123
    return-void
.end method

.method public setParticipantClickListener(Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$OnParticipantClickListener;)V
    .locals 0
    .param p1, "clickListener"    # Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$OnParticipantClickListener;

    .prologue
    .line 324
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->clickListener:Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView$OnParticipantClickListener;

    .line 325
    return-void
.end method

.method public setParticipantData(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 110
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->data:Ljava/util/List;

    .line 111
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->cachedViews:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 112
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->getTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->getBottom()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/location/view/LinearRouteView;->buildLayout(II)V

    .line 113
    return-void
.end method
