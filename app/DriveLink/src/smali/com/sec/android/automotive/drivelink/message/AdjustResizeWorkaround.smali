.class public Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;
.super Ljava/lang/Object;
.source "AdjustResizeWorkaround.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;
    }
.end annotation


# static fields
.field private static mAdjustStateListener:Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;


# instance fields
.field private frameLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

.field private mChildOfContent:Landroid/view/View;

.field private usableHeightPrevious:I


# direct methods
.method private constructor <init>(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const v1, 0x1020002

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 28
    check-cast v0, Landroid/widget/FrameLayout;

    .line 30
    .local v0, "content":Landroid/widget/FrameLayout;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->mChildOfContent:Landroid/view/View;

    .line 31
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->mChildOfContent:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    .line 32
    new-instance v2, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$1;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$1;-><init>(Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;)V

    .line 31
    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 37
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->mChildOfContent:Landroid/view/View;

    .line 38
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 37
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->frameLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    .line 39
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->possiblyResizeChildOfContent()V

    return-void
.end method

.method public static assistActivity(Landroid/app/Activity;)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 20
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;-><init>(Landroid/app/Activity;)V

    .line 21
    return-void
.end method

.method private computeUsableHeight()I
    .locals 3

    .prologue
    .line 77
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 78
    .local v0, "r":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->mChildOfContent:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 79
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    return v1
.end method

.method private possiblyResizeChildOfContent()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 42
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->computeUsableHeight()I

    move-result v1

    .line 43
    .local v1, "usableHeightNow":I
    iget v3, p0, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->usableHeightPrevious:I

    if-eq v1, v3, :cond_3

    .line 44
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->mChildOfContent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v3

    .line 45
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 46
    .local v2, "usableHeightSansKeyboard":I
    sub-int v0, v2, v1

    .line 47
    .local v0, "heightDifference":I
    div-int/lit8 v3, v2, 0x4

    if-le v0, v3, :cond_2

    .line 49
    const-string/jumbo v3, "MessageComposerActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "usableHeightNow :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 50
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 49
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->frameLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    .line 52
    sub-int v4, v2, v0

    .line 51
    iput v4, v3, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 54
    sget-object v3, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->mAdjustStateListener:Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;

    if-eqz v3, :cond_0

    .line 55
    sget-object v3, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->mAdjustStateListener:Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;

    invoke-interface {v3, v6}, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;->OnDisplayKeyboard(Z)V

    .line 65
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->mChildOfContent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->requestLayout()V

    .line 66
    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->usableHeightPrevious:I

    .line 74
    .end local v0    # "heightDifference":I
    .end local v2    # "usableHeightSansKeyboard":I
    :cond_1
    :goto_1
    return-void

    .line 58
    .restart local v0    # "heightDifference":I
    .restart local v2    # "usableHeightSansKeyboard":I
    :cond_2
    const-string/jumbo v3, "MessageComposerActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "usableHeightSansKeyboard :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 59
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 58
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->frameLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iput v2, v3, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 62
    sget-object v3, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->mAdjustStateListener:Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;

    if-eqz v3, :cond_0

    .line 63
    sget-object v3, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->mAdjustStateListener:Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;->OnDisplayKeyboard(Z)V

    goto :goto_0

    .line 68
    .end local v0    # "heightDifference":I
    .end local v2    # "usableHeightSansKeyboard":I
    :cond_3
    sget-object v3, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->mAdjustStateListener:Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;

    if-eqz v3, :cond_1

    .line 69
    sget-object v3, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->mAdjustStateListener:Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;

    invoke-interface {v3}, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;->OnCheckKeyboard()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 70
    sget-object v3, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->mAdjustStateListener:Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;

    invoke-interface {v3, v6}, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;->OnDisplayKeyboard(Z)V

    goto :goto_1
.end method

.method public static setOnAdjustStateListener(Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;)V
    .locals 0
    .param p0, "listener"    # Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;

    .prologue
    .line 83
    sput-object p0, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->mAdjustStateListener:Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;

    .line 84
    return-void
.end method
