.class Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$7;
.super Ljava/lang/Object;
.source "SetupWizardMyCarActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

.field private final synthetic val$savedInstanceState:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$7;->val$savedInstanceState:Landroid/os/Bundle;

    .line 264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 267
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mBTState:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->access$0(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    move-result-object v1

    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->FIRST_ACCESS_REGISTERED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    if-ne v1, v2, :cond_0

    .line 268
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->access$2(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;Z)V

    .line 269
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->initNewBTValues()V
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->access$9(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)V

    .line 270
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->initLayout()V
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->access$10(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)V

    .line 271
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    .line 272
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$7;->val$savedInstanceState:Landroid/os/Bundle;

    # invokes: Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->makeDiscoverable(Landroid/os/Bundle;)V
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->access$11(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;Landroid/os/Bundle;)V

    .line 284
    :goto_0
    return-void

    .line 274
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->canRefresh:Z

    .line 275
    new-instance v0, Landroid/content/Intent;

    .line 276
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    .line 277
    const-class v2, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    .line 275
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 278
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "NAME_BT_STATE_UNREGISTERED"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 280
    const-string/jumbo v1, "EXTRA_NAME_FIRST_ACCESS"

    .line 279
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 282
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
