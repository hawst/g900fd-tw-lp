.class Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1$1;
.super Ljava/lang/Object;
.source "LocationRequestActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->onResponseRequestSearchedContactList(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "arg0"    # I

    .prologue
    .line 184
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 3
    .param p1, "arg0"    # I
    .param p2, "arg1"    # F
    .param p3, "arg2"    # I

    .prologue
    const/4 v2, 0x0

    .line 166
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;)Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->isLoaded:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$12(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;)Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$13(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;Z)V

    .line 168
    if-nez p1, :cond_1

    .line 169
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;)Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$2(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    .line 170
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;)Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    move-result-object v1

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$8(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    move-result-object v1

    .line 171
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->getInitialPosition()I

    move-result v1

    .line 170
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;)Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$10(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 174
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;)Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$10(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 175
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;)Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    move-result-object v1

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$8(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    move-result-object v1

    .line 176
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 174
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    goto :goto_0
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;)Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$10(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 159
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;)Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    move-result-object v1

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$8(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    move-result-object v1

    .line 160
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 158
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 161
    return-void
.end method
