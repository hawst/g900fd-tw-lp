.class public Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;
.super Ljava/lang/Object;
.source "NotiLocInfo.java"


# instance fields
.field private name:Ljava/lang/String;

.field private number:Ljava/lang/String;

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "number"    # Ljava/lang/String;

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->name:Ljava/lang/String;

    .line 10
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->number:Ljava/lang/String;

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->url:Ljava/lang/String;

    .line 12
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "number"    # Ljava/lang/String;
    .param p3, "url"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->name:Ljava/lang/String;

    .line 16
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->number:Ljava/lang/String;

    .line 17
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->url:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->number:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->url:Ljava/lang/String;

    return-object v0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->name:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public setNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->number:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->url:Ljava/lang/String;

    .line 65
    return-void
.end method
