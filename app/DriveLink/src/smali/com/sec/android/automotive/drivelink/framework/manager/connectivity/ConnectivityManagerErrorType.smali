.class public Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerErrorType;
.super Ljava/lang/Object;
.source "ConnectivityManagerErrorType.java"


# static fields
.field public static final UNBOND_REASON_AUTH_FAILED:I = -0x1

.field public static final UNBOND_REASON_AUTH_REJECTED:I = -0x2

.field public static final UNBOND_REASON_AUTH_TIMEOUT:I = -0x5

.field public static final UNBOND_REASON_DISCOVERY_IN_PROGRESS:I = -0x4

.field public static final UNBOND_REASON_REMOTE_AUTH_CANCELED:I = -0x7

.field public static final UNBOND_REASON_REMOTE_DEVICE_DOWN:I = -0x3

.field public static final UNBOND_REASON_REPEATED_ATTEMPTS:I = -0x6

.field public static final UNBOND_REASON_UNKNOWN:I = -0x8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
