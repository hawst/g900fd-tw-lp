.class public Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;
.super Landroid/support/v4/view/PagerAdapter;
.source "MessageReaderPageAdaper.java"


# static fields
.field private static mTTSon:Z


# instance fields
.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mIsDriving:Z

.field private mMessageBody:Ljava/lang/String;

.field private mMessageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;",
            ">;"
        }
    .end annotation
.end field

.field private mMessageName:Ljava/lang/String;

.field private mMsgBody:Landroid/widget/TextView;

.field private mMsgCount:Landroid/widget/TextView;

.field private mMsgScrollBody:Landroid/widget/TextView;

.field private mMsgScrollView:Landroid/widget/ScrollView;

.field private mNoMsgBody:Landroid/widget/LinearLayout;

.field private mNoText:Landroid/widget/TextView;

.field private mPageCount:I

.field private mTtsBtn:Landroid/widget/ImageButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;ZLjava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "applicationContext"    # Landroid/content/Context;
    .param p3, "isDriving"    # Z
    .param p4, "mMessageTime"    # Ljava/lang/String;
    .param p5, "mMessageName"    # Ljava/lang/String;
    .param p6, "clickListener"    # Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;",
            ">;Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40
    .local p2, "mMsgInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;>;"
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 38
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mHandler:Landroid/os/Handler;

    .line 44
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mContext:Landroid/content/Context;

    .line 45
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMessageList:Ljava/util/ArrayList;

    .line 46
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mPageCount:I

    .line 47
    iput-boolean p3, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mIsDriving:Z

    .line 48
    iput-object p5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMessageName:Ljava/lang/String;

    .line 50
    iput-object p6, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mClickListener:Landroid/view/View$OnClickListener;

    .line 51
    return-void
.end method

.method private setMessageCountState(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 160
    const-string/jumbo v0, ""

    .line 162
    .local v0, "countstate":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 163
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 164
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mPageCount:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 166
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMsgCount:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    return-void
.end method

.method public static setTTSChange(Z)V
    .locals 0
    .param p0, "status"    # Z

    .prologue
    .line 156
    sput-boolean p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mTTSon:Z

    .line 157
    return-void
.end method

.method private stopTTS()V
    .locals 1

    .prologue
    .line 172
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    .line 173
    .local v0, "dialogFlow":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelDialog()V

    .line 174
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 178
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 179
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 183
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mPageCount:I

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 6
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 55
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 56
    .local v0, "layoutInflater":Landroid/view/LayoutInflater;
    const v2, 0x7f03006d

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 58
    .local v1, "v":Landroid/view/View;
    const v2, 0x7f090224

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mTtsBtn:Landroid/widget/ImageButton;

    .line 59
    const v2, 0x7f09022a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMsgCount:Landroid/widget/TextView;

    .line 60
    const v2, 0x7f090225

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMsgBody:Landroid/widget/TextView;

    .line 61
    const v2, 0x7f090228

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMsgScrollView:Landroid/widget/ScrollView;

    .line 63
    const v2, 0x7f090229

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 62
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMsgScrollBody:Landroid/widget/TextView;

    .line 64
    const v2, 0x7f090226

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mNoMsgBody:Landroid/widget/LinearLayout;

    .line 65
    const v2, 0x7f090227

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mNoText:Landroid/widget/TextView;

    .line 66
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mTtsBtn:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMessageList:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;->getMsgType()I

    move-result v2

    if-nez v2, :cond_1

    .line 70
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMessageList:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;->getMsgBody()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMessageBody:Ljava/lang/String;

    .line 83
    :goto_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMessageBody:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMessageBody:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 84
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMessageList:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;->getMsgType()I

    move-result v2

    if-nez v2, :cond_2

    .line 85
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 86
    const v3, 0x7f0a03b0

    .line 85
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMessageBody:Ljava/lang/String;

    .line 116
    :goto_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMsgBody:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 117
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMsgScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v2, v4}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 118
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mNoMsgBody:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 119
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mNoText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMessageBody:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mTtsBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 136
    :goto_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMsgBody:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMessageBody:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    add-int/lit8 v2, p2, 0x1

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->setMessageCountState(I)V

    .line 146
    invoke-virtual {v1, p2}, Landroid/view/View;->setId(I)V

    .line 147
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    invoke-virtual {p1, v1}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    .line 148
    return-object v1

    .line 73
    .restart local p1    # "container":Landroid/view/ViewGroup;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMessageList:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;->getAttachmentInfo()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessageAttachment;

    move-result-object v2

    .line 74
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessageAttachment;->getText()Ljava/lang/String;

    move-result-object v2

    .line 73
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMessageBody:Ljava/lang/String;

    goto :goto_0

    .line 113
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mContext:Landroid/content/Context;

    const v3, 0x7f0a018c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMessageBody:Ljava/lang/String;

    goto :goto_1

    .line 122
    :cond_3
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mNoMsgBody:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 123
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMsgBody:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMessageBody:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMsgScrollBody:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMessageBody:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mTtsBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 127
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mIsDriving:Z

    if-eqz v2, :cond_4

    .line 128
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMsgScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v2, v4}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 129
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMsgBody:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 131
    :cond_4
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMsgScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v2, v5}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 132
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;->mMsgBody:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 188
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
