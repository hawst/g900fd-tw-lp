.class Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$2;
.super Ljava/lang/Object;
.source "SettingsContactsImageLoader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->requestContactImage(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;Landroid/widget/ImageView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;

.field private final synthetic val$contact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

.field private final synthetic val$imageView:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;Landroid/widget/ImageView;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$2;->val$imageView:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$2;->val$contact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 66
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$2;->val$imageView:Landroid/widget/ImageView;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->mDLInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v2

    .line 68
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$2;->val$contact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 67
    invoke-interface {v2, v4, v5}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 71
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 73
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 74
    const v4, 0x7f02024e

    .line 75
    const v5, 0x7f020249

    .line 72
    invoke-static {v2, v4, v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 82
    .local v1, "maskedIcon":Landroid/graphics/Bitmap;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$2;->val$contact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$2;->val$imageView:Landroid/widget/ImageView;

    # invokes: Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->sendEventToListener(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    invoke-static {v2, v4, v5, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    .line 66
    monitor-exit v3

    .line 87
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "maskedIcon":Landroid/graphics/Bitmap;
    :goto_1
    return-void

    .line 78
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 79
    const v4, 0x7f020249

    .line 77
    invoke-static {v2, v0, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .restart local v1    # "maskedIcon":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 66
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "maskedIcon":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 84
    :catch_0
    move-exception v2

    goto :goto_1
.end method
