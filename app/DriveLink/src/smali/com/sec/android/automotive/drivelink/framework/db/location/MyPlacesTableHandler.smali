.class public Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;
.super Ljava/lang/Object;
.source "MyPlacesTableHandler.java"


# static fields
.field private static final TABLE_LOCATION_MYPLACE:Ljava/lang/String; = "tb_location_myplaces"

.field private static final TAG:Ljava/lang/String; = "MyPlacesTableHandler"


# instance fields
.field mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;)V
    .locals 1
    .param p1, "driveLinkLocationTableHandler"    # Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 23
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 24
    return-void
.end method

.method private addMyPlace(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;)Z
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "place"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;

    .prologue
    const/4 v4, 0x0

    .line 128
    const-wide/16 v1, -0x1

    .line 130
    .local v1, "result":J
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v5

    if-nez v5, :cond_1

    .line 144
    :cond_0
    :goto_0
    return v4

    .line 134
    :cond_1
    :try_start_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 136
    .local v3, "values":Landroid/content/ContentValues;
    const-string/jumbo v5, "name"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->getPlaceName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string/jumbo v5, "location_id"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string/jumbo v5, "tb_location_myplaces"

    const/4 v6, 0x0

    invoke-virtual {p1, v5, v6, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    .line 144
    .end local v3    # "values":Landroid/content/ContentValues;
    :goto_1
    const-wide/16 v5, -0x1

    cmp-long v5, v1, v5

    if-eqz v5, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    .line 140
    :catch_0
    move-exception v0

    .line 141
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method private getMyPlaceFromCursor(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 189
    new-instance v4, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;

    invoke-direct {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;-><init>()V

    .line 190
    .local v4, "myPlace":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;
    const/4 v2, 0x0

    .line 191
    .local v2, "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    const/4 v3, 0x0

    .line 193
    .local v3, "locationId":Ljava/lang/String;
    const/4 v0, 0x0

    .line 194
    .local v0, "i":I
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 195
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->setPlaceName(Ljava/lang/String;)V

    .line 198
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->getLocationsTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;

    move-result-object v5

    .line 199
    invoke-virtual {v5, p1, v3}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->getLocation(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v2

    .line 200
    if-nez v2, :cond_0

    .line 203
    const/4 v4, 0x0

    .line 209
    .end local v4    # "myPlace":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;
    :goto_0
    return-object v4

    .line 206
    .restart local v4    # "myPlace":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;
    :cond_0
    invoke-virtual {v4, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->setLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 207
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->setPlaceAddress(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private insertMyFirstPlaces(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 60
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;-><init>()V

    .line 62
    .local v0, "myPlace":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;
    const-string/jumbo v1, "google_map"

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->setPlaceName(Ljava/lang/String;)V

    .line 63
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;->saveMyPlace(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;)Z

    .line 81
    return-void
.end method

.method private myPlaceExits(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;)Z
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "place"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;

    .prologue
    .line 168
    if-nez p2, :cond_1

    .line 169
    const/4 v0, 0x0

    .line 184
    :cond_0
    :goto_0
    return v0

    .line 170
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "SELECT * FROM tb_location_myplaces WHERE name =\'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 171
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->getPlaceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\';"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 170
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 174
    .local v2, "sql":Ljava/lang/String;
    const/4 v0, 0x0

    .line 175
    .local v0, "bExist":Z
    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 176
    .local v1, "cursor":Landroid/database/Cursor;
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 177
    const/4 v0, 0x1

    .line 180
    :cond_2
    if-eqz v1, :cond_0

    .line 181
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private updateMyPlace(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;)Z
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "place"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;

    .prologue
    const/4 v4, 0x0

    .line 148
    const-wide/16 v1, 0x0

    .line 150
    .local v1, "result":J
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v5

    if-nez v5, :cond_1

    .line 164
    :cond_0
    :goto_0
    return v4

    .line 154
    :cond_1
    :try_start_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 156
    .local v3, "values":Landroid/content/ContentValues;
    const-string/jumbo v5, "location_id"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    const-string/jumbo v5, "tb_location_myplaces"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "name = \'"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 159
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->getPlaceName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    .line 158
    invoke-virtual {p1, v5, v3, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    int-to-long v1, v5

    .line 164
    .end local v3    # "values":Landroid/content/ContentValues;
    :goto_1
    const-wide/16 v5, 0x0

    cmp-long v5, v1, v5

    if-eqz v5, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    .line 160
    :catch_0
    move-exception v0

    .line 161
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public createMyPlacesTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 27
    const-string/jumbo v2, "MyPlacesTableHandler"

    const-string/jumbo v3, "createMyPlacesTable"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    :try_start_0
    const-string/jumbo v1, "CREATE TABLE IF NOT EXISTS tb_location_myplaces (name VARCHAR PRIMARY KEY, location_id VARCHAR, FOREIGN KEY(location_id) REFERENCES tb_locations(location_id));"

    .line 37
    .local v1, "sql":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 39
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;->insertMyFirstPlaces(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    .end local v1    # "sql":Ljava/lang/String;
    :goto_0
    return-void

    .line 41
    :catch_0
    move-exception v0

    .line 42
    .local v0, "e":Landroid/database/SQLException;
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    goto :goto_0
.end method

.method public dropMyPlacesTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 47
    const-string/jumbo v2, "MyPlacesTableHandler"

    const-string/jumbo v3, "dropMyPlacesTable"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    :try_start_0
    const-string/jumbo v1, "DROP TABLE IF EXISTS tb_location_myplaces;"

    .line 53
    .local v1, "sql":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    .end local v1    # "sql":Ljava/lang/String;
    :goto_0
    return-void

    .line 54
    :catch_0
    move-exception v0

    .line 55
    .local v0, "e":Landroid/database/SQLException;
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    goto :goto_0
.end method

.method public loadLocationMyPlaceList(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/ArrayList;
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 87
    .local v2, "myPlacesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    const-string/jumbo v3, "select location_id, name from tb_location_myplaces"

    .line 89
    .local v3, "sql":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 91
    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 109
    :goto_0
    return-object v2

    .line 94
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_1

    .line 95
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 100
    :cond_1
    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;->getMyPlaceFromCursor(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;

    move-result-object v1

    .line 102
    .local v1, "myPlace":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    if-eqz v1, :cond_2

    .line 103
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 107
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public saveMyPlace(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;)Z
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "place"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;

    .prologue
    const/4 v0, 0x0

    .line 113
    if-nez p2, :cond_1

    .line 114
    const-string/jumbo v1, "MyPlacesTableHandler"

    const-string/jumbo v2, "Invalid place object to save."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    :cond_0
    :goto_0
    return v0

    .line 118
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->getLocationsTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;

    move-result-object v1

    .line 119
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->saveLocation(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 122
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;->myPlaceExits(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 123
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;->updateMyPlace(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;)Z

    move-result v0

    goto :goto_0

    .line 124
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;->addMyPlace(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;)Z

    move-result v0

    goto :goto_0
.end method
