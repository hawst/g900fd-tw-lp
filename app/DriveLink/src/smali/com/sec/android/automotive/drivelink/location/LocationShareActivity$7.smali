.class Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$7;
.super Ljava/lang/Object;
.source "LocationShareActivity.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->setupListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    .line 279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setBtnSearchInvisible()V

    .line 284
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 285
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 286
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getTextFromEditTextSearch()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 287
    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getTextFromEditTextSearch()Ljava/lang/String;

    move-result-object v1

    .line 291
    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 290
    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->searchContacts(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$3(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;Ljava/lang/String;)V

    .line 294
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
