.class public Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;
.super Ljava/lang/Object;
.source "DLLightSensorManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;
    }
.end annotation


# static fields
.field private static final BRIGHTENING_LIGHT_DEBOUNCE:J = 0x5dcL

.field private static final DARKENING_LIGHT_DEBOUNCE:J = 0x5dcL

.field private static final LIGHT_SENSOR_RATE_MILLIS:I = 0xc8

.field private static final LUX_STATE_BRIGHT:I = 0x1

.field private static final LUX_STATE_DARK:I = 0x0

.field private static final LUX_STATE_UNKNOWN:I = -0x1

.field private static final MSG_LIGHT_SENSOR_DEBOUNCED:I = 0x3

.field public static final TAG:Ljava/lang/String; = "DLLightSensorManager"

.field private static mInstance:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;


# instance fields
.field final BRIGHT_LUX_VALUE:I

.field final DARK_LUX_VALUE:I

.field private final TAG_DAB:Ljava/lang/String;

.field private final TAG_SENSOR:Ljava/lang/String;

.field private mAmbientLux:F

.field mAmbientLuxValid:Z

.field private mContext:Landroid/content/Context;

.field private mDebounceLuxDirection:I

.field private mDebounceLuxTime:J

.field mHandler:Landroid/os/Handler;

.field private mHighHysteresis:F

.field private mLastObservedLux:F

.field private mLastObservedLuxTime:J

.field private mLightSensor:Landroid/hardware/Sensor;

.field private mLightSensorEnableTime:J

.field mLightSensorEnabled:Z

.field private final mLightSensorListener:Landroid/hardware/SensorEventListener;

.field private final mLightSensorWarmUpTimeConfig:I

.field private mListener:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;

.field private mLowHysteresis:F

.field private mLuxState:I

.field private mRecentLightSamples:I

.field private mRecentLongTermAverageLux:F

.field private mRecentShortTermAverageLux:F

.field private mSECAverageLux:F

.field private mSensorManager:Landroid/hardware/SensorManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string/jumbo v0, "[DAB] "

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->TAG_DAB:Ljava/lang/String;

    .line 23
    const-string/jumbo v0, "[sensor] "

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->TAG_SENSOR:Ljava/lang/String;

    .line 25
    const/16 v0, 0x16

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->DARK_LUX_VALUE:I

    .line 26
    const/16 v0, 0x2d

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->BRIGHT_LUX_VALUE:I

    .line 41
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLightSensorEnabled:Z

    .line 45
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mAmbientLuxValid:Z

    .line 76
    iput v1, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLightSensorWarmUpTimeConfig:I

    .line 82
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLuxState:I

    .line 143
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$1;-><init>(Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLightSensorListener:Landroid/hardware/SensorEventListener;

    .line 188
    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLowHysteresis:F

    .line 189
    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mHighHysteresis:F

    .line 93
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mContext:Landroid/content/Context;

    .line 94
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->initLightSensorManager()V

    .line 95
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;)I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mRecentLightSamples:I

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;JF)V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->handleLightSensorEvent(JF)V

    return-void
.end method

.method private applyLightSensorMeasurementSEC(JF)V
    .locals 2
    .param p1, "time"    # J
    .param p3, "lux"    # F

    .prologue
    .line 169
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mRecentLightSamples:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mRecentLightSamples:I

    .line 170
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mRecentLightSamples:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 171
    iput p3, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mSECAverageLux:F

    .line 181
    :goto_0
    iput p3, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLastObservedLux:F

    .line 182
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLastObservedLuxTime:J

    .line 183
    return-void

    .line 175
    :cond_0
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLastObservedLux:F

    add-float/2addr v0, p3

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mSECAverageLux:F

    goto :goto_0
.end method

.method private doBright()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 282
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLuxState:I

    if-eq v0, v1, :cond_0

    .line 283
    iput v1, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLuxState:I

    .line 285
    const-string/jumbo v0, "DLLightSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "doBright ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mAmbientLux:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mListener:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mListener:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;

    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;->setBrightMode()V

    .line 291
    :cond_0
    return-void
.end method

.method private doDark()V
    .locals 3

    .prologue
    .line 270
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLuxState:I

    if-eqz v0, :cond_0

    .line 271
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLuxState:I

    .line 273
    const-string/jumbo v0, "DLLightSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "doDark ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mAmbientLux:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mListener:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mListener:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;

    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;->setDarkMode()V

    .line 279
    :cond_0
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 85
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mInstance:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mInstance:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;

    .line 89
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mInstance:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;

    return-object v0
.end method

.method private handleLightSensorEvent(JF)V
    .locals 2
    .param p1, "time"    # J
    .param p3, "lux"    # F

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 139
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->applyLightSensorMeasurementSEC(JF)V

    .line 140
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->updateAmbientLuxSEC(J)V

    .line 141
    return-void
.end method

.method private initLightSensorManager()V
    .locals 2

    .prologue
    .line 98
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mHandler:Landroid/os/Handler;

    .line 99
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mContext:Landroid/content/Context;

    .line 100
    const-string/jumbo v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    .line 99
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 101
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLightSensor:Landroid/hardware/Sensor;

    .line 103
    const/high16 v0, 0x41b00000    # 22.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLowHysteresis:F

    .line 104
    const/high16 v0, 0x42340000    # 45.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mHighHysteresis:F

    .line 105
    return-void
.end method

.method private setLightSensorEnabled(ZZ)V
    .locals 5
    .param p1, "enable"    # Z
    .param p2, "updateAutoBrightness"    # Z

    .prologue
    const/4 v1, 0x0

    .line 109
    if-eqz p1, :cond_1

    .line 110
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLightSensorEnabled:Z

    if-nez v0, :cond_0

    .line 111
    const/4 p2, 0x1

    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLightSensorEnabled:Z

    .line 113
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLightSensorEnableTime:J

    .line 115
    const-string/jumbo v0, "DLLightSensorManager"

    .line 116
    const-string/jumbo v1, "[DAB] setLightSensorEnabled : registerListener mLightSensor"

    .line 115
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 119
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLightSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLightSensor:Landroid/hardware/Sensor;

    .line 120
    const v3, 0x30d40

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mHandler:Landroid/os/Handler;

    .line 119
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLightSensorEnabled:Z

    if-eqz v0, :cond_0

    .line 124
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLightSensorEnabled:Z

    .line 125
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mAmbientLuxValid:Z

    .line 126
    iput v1, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mRecentLightSamples:I

    .line 127
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 129
    const-string/jumbo v0, "DLLightSensorManager"

    .line 130
    const-string/jumbo v1, "[DAB] setLightSensorEnabled : unregisterListener mLightSensor"

    .line 129
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLightSensorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    goto :goto_0
.end method

.method private updateAmbientLuxSEC(J)V
    .locals 11
    .param p1, "time"    # J

    .prologue
    const-wide/16 v9, 0x5dc

    const/4 v8, 0x3

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 195
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mAmbientLuxValid:Z

    if-eqz v2, :cond_0

    .line 196
    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLightSensorEnableTime:J

    sub-long v2, p1, v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    .line 197
    :cond_0
    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mSECAverageLux:F

    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mAmbientLux:F

    .line 198
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mAmbientLuxValid:Z

    .line 199
    iput v7, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mDebounceLuxDirection:I

    .line 200
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mDebounceLuxTime:J

    .line 201
    invoke-direct {p0, v6}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->updateAutoBrightnessSEC(Z)V

    .line 247
    :cond_1
    :goto_0
    return-void

    .line 206
    :cond_2
    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mSECAverageLux:F

    iget v3, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mHighHysteresis:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_5

    .line 207
    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mDebounceLuxDirection:I

    if-gtz v2, :cond_3

    .line 208
    iput v6, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mDebounceLuxDirection:I

    .line 209
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mDebounceLuxTime:J

    .line 211
    :cond_3
    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mDebounceLuxTime:J

    add-long v0, v2, v9

    .line 212
    .local v0, "debounceTime":J
    cmp-long v2, p1, v0

    if-ltz v2, :cond_4

    .line 213
    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mSECAverageLux:F

    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mAmbientLux:F

    .line 214
    invoke-direct {p0, v6}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->updateAutoBrightnessSEC(Z)V

    goto :goto_0

    .line 216
    :cond_4
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v8, v0, v1}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    goto :goto_0

    .line 223
    .end local v0    # "debounceTime":J
    :cond_5
    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mSECAverageLux:F

    iget v3, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLowHysteresis:F

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_8

    .line 224
    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mDebounceLuxDirection:I

    if-ltz v2, :cond_6

    .line 225
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mDebounceLuxDirection:I

    .line 226
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mDebounceLuxTime:J

    .line 228
    :cond_6
    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mDebounceLuxTime:J

    add-long v0, v2, v9

    .line 229
    .restart local v0    # "debounceTime":J
    cmp-long v2, p1, v0

    if-ltz v2, :cond_7

    .line 233
    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mSECAverageLux:F

    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mAmbientLux:F

    .line 234
    invoke-direct {p0, v6}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->updateAutoBrightnessSEC(Z)V

    goto :goto_0

    .line 236
    :cond_7
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v8, v0, v1}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    goto :goto_0

    .line 243
    .end local v0    # "debounceTime":J
    :cond_8
    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mDebounceLuxDirection:I

    if-eqz v2, :cond_1

    .line 244
    iput v7, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mDebounceLuxDirection:I

    .line 245
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mDebounceLuxTime:J

    goto :goto_0
.end method

.method private updateAutoBrightnessSEC(Z)V
    .locals 2
    .param p1, "sendUpdate"    # Z

    .prologue
    .line 252
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mAmbientLuxValid:Z

    if-nez v0, :cond_0

    .line 254
    const-string/jumbo v0, "DLLightSensorManager"

    const-string/jumbo v1, "[DAB] no lux value from sensor manager"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    :goto_0
    return-void

    .line 259
    :cond_0
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mAmbientLux:F

    const/high16 v1, 0x41b00000    # 22.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 261
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->doDark()V

    goto :goto_0

    .line 263
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->doBright()V

    goto :goto_0
.end method


# virtual methods
.method public setListener(Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;

    .prologue
    const/4 v1, 0x0

    .line 294
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mListener:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;

    .line 295
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mListener:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;

    if-nez v0, :cond_0

    .line 296
    invoke-direct {p0, v1, v1}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->setLightSensorEnabled(ZZ)V

    .line 306
    :goto_0
    return-void

    .line 298
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->setLightSensorEnabled(ZZ)V

    .line 300
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLuxState:I

    if-nez v0, :cond_1

    .line 301
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mListener:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;

    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;->setDarkMode()V

    goto :goto_0

    .line 303
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mListener:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;

    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;->setBrightMode()V

    goto :goto_0
.end method

.method public updateLightState()V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mListener:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;

    if-eqz v0, :cond_0

    .line 310
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLuxState:I

    if-nez v0, :cond_1

    .line 311
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mListener:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;

    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;->setDarkMode()V

    .line 316
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mListener:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;

    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;->setBrightMode()V

    goto :goto_0
.end method
