.class Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$5;
.super Ljava/lang/Object;
.source "MyPlaceCarSimpleCursorAdapter.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->showDisconnectDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    .line 335
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x0

    .line 339
    const/4 v1, 0x4

    if-ne p2, v1, :cond_0

    .line 340
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    .line 341
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->disconnectBtAddress:Ljava/lang/String;

    .line 342
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    iput-boolean v0, v1, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->isDialogOpen:Z

    .line 343
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->access$0(Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->refreshList()V

    .line 344
    const/4 v0, 0x1

    .line 346
    :cond_0
    return v0
.end method
