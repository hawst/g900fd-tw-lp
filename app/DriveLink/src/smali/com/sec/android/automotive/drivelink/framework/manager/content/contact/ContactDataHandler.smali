.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;
.super Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;
.source "ContactDataHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;,
        Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;
    }
.end annotation


# static fields
.field public static final MAX_PHONE_SUGGESTIONS:I = 0xc

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private initThread:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;

.field private volatile mContactCache:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;",
            ">;"
        }
    .end annotation
.end field

.field private mContactCacheUpdate:Ljava/lang/Object;

.field private mContactIdHashmapWithPhoneNumber:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mContactImplCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;>;"
        }
    .end annotation
.end field

.field private mContactNameCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;",
            ">;"
        }
    .end annotation
.end field

.field private mDbHelper:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;

.field private mPhoneNumberContactCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->TAG:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;-><init>()V

    .line 37
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCacheUpdate:Ljava/lang/Object;

    .line 39
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactIdHashmapWithPhoneNumber:Ljava/util/HashMap;

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mDbHelper:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;

    .line 81
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCacheUpdate:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->initializeCaches(Landroid/content/Context;)V

    return-void
.end method

.method private buildContactCacheWireFrame(Landroid/content/Context;)V
    .locals 15
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 152
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 154
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v1, 0x3

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 155
    const-string/jumbo v4, "_id"

    aput-object v4, v2, v1

    const/4 v1, 0x1

    const-string/jumbo v4, "display_name"

    aput-object v4, v2, v1

    const/4 v1, 0x2

    .line 156
    const-string/jumbo v4, "phonetic_name"

    aput-object v4, v2, v1

    .line 163
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v3, "has_phone_number > 0 "

    .line 165
    .local v3, "selection":Ljava/lang/String;
    const-string/jumbo v5, "display_name ASC "

    .line 166
    .local v5, "sortOrder":Ljava/lang/String;
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    .line 167
    const/4 v4, 0x0

    .line 166
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 169
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_2

    .line 170
    const-string/jumbo v1, "CDH"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "buildContactCacheWireFrame : cur len ="

    invoke-direct {v4, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v13

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 172
    const-string/jumbo v1, "_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 173
    .local v6, "colId":I
    const-string/jumbo v1, "display_name"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 176
    .local v7, "colName":I
    const-string/jumbo v1, "phonetic_name"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 179
    .local v8, "colPhoneticName":I
    :cond_0
    invoke-interface {v9, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 180
    .local v11, "id":I
    invoke-interface {v9, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 181
    .local v10, "displayName":Ljava/lang/String;
    invoke-interface {v9, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 188
    .local v12, "phoneticName":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    new-instance v4, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    int-to-long v13, v11

    invoke-direct {v4, v13, v14, v10, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v11, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 190
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 193
    .end local v6    # "colId":I
    .end local v7    # "colName":I
    .end local v8    # "colPhoneticName":I
    .end local v10    # "displayName":Ljava/lang/String;
    .end local v11    # "id":I
    .end local v12    # "phoneticName":Ljava/lang/String;
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 195
    :cond_2
    return-void
.end method

.method private fillFirstGivenNameIntoContactCache(Landroid/content/Context;)V
    .locals 24
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 333
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 335
    .local v2, "contentResolver":Landroid/content/ContentResolver;
    const/16 v21, 0x0

    .line 336
    .local v21, "startOffset":I
    const/16 v13, 0x1f4

    .line 337
    .local v13, "diffValue":I
    add-int v14, v21, v13

    .line 340
    .local v14, "endOffset":I
    :cond_0
    add-int v14, v21, v13

    .line 341
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v3, v14, :cond_1

    .line 342
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v14

    .line 345
    :cond_1
    sub-int v3, v14, v21

    new-array v6, v3, [Ljava/lang/String;

    .line 347
    .local v6, "contactIds":[Ljava/lang/String;
    move/from16 v17, v21

    .local v17, "i":I
    :goto_0
    move/from16 v0, v17

    if-lt v0, v14, :cond_6

    .line 352
    array-length v3, v6

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getSQLWhereInSubQuery(I)Ljava/lang/String;

    move-result-object v19

    .line 354
    .local v19, "inQuery":Ljava/lang/String;
    const/4 v3, 0x3

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 355
    const-string/jumbo v7, "contact_id"

    aput-object v7, v4, v3

    const/4 v3, 0x1

    .line 356
    const-string/jumbo v7, "data3"

    aput-object v7, v4, v3

    const/4 v3, 0x2

    .line 357
    const-string/jumbo v7, "data2"

    aput-object v7, v4, v3

    .line 360
    .local v4, "projection":[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "mimetype = \'vnd.android.cursor.item/name\' AND contact_id IN ("

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 363
    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v7, ") "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 360
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 366
    .local v5, "where":Ljava/lang/String;
    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    .line 367
    const-string/jumbo v7, "contact_id"

    .line 365
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 368
    .local v12, "cursor":Landroid/database/Cursor;
    if-eqz v12, :cond_7

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_7

    .line 370
    const-string/jumbo v3, "CDH"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v22, "fillFirstGivenNameIntoContactCache : cur len = "

    move-object/from16 v0, v22

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 373
    const-string/jumbo v3, "contact_id"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 375
    .local v10, "colId":I
    const-string/jumbo v3, "data3"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 377
    .local v8, "colFamilyName":I
    const-string/jumbo v3, "data2"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 380
    .local v9, "colGivenName":I
    :cond_2
    invoke-interface {v12, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 381
    .local v18, "id":I
    invoke-interface {v12, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 382
    .local v15, "familyName":Ljava/lang/String;
    invoke-interface {v12, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 384
    .local v16, "givenName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 385
    .local v11, "contact":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    if-eqz v11, :cond_3

    .line 386
    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->setFirstName(Ljava/lang/String;)V

    .line 387
    invoke-virtual {v11, v15}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->setLastName(Ljava/lang/String;)V

    .line 389
    :cond_3
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 392
    .end local v8    # "colFamilyName":I
    .end local v9    # "colGivenName":I
    .end local v10    # "colId":I
    .end local v11    # "contact":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    .end local v15    # "familyName":Ljava/lang/String;
    .end local v16    # "givenName":Ljava/lang/String;
    .end local v18    # "id":I
    :cond_4
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 400
    :cond_5
    :goto_1
    add-int v21, v21, v13

    .line 401
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 339
    if-lt v14, v3, :cond_0

    .line 402
    return-void

    .line 348
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v5    # "where":Ljava/lang/String;
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v19    # "inQuery":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 349
    .local v20, "item":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    sub-int v3, v17, v21

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->getContactId()J

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    .line 347
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_0

    .line 396
    .end local v20    # "item":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    .restart local v4    # "projection":[Ljava/lang/String;
    .restart local v5    # "where":Ljava/lang/String;
    .restart local v12    # "cursor":Landroid/database/Cursor;
    .restart local v19    # "inQuery":Ljava/lang/String;
    :cond_7
    if-eqz v12, :cond_5

    .line 397
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method private fillFirstGivenNameIntoContactCache(Landroid/content/Context;I)V
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contactId"    # I

    .prologue
    .line 759
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 761
    .local v1, "contentResolver":Landroid/content/ContentResolver;
    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 762
    const-string/jumbo v5, "contact_id"

    aput-object v5, v3, v2

    const/4 v2, 0x1

    .line 763
    const-string/jumbo v5, "data3"

    aput-object v5, v3, v2

    const/4 v2, 0x2

    .line 764
    const-string/jumbo v5, "data2"

    aput-object v5, v3, v2

    .line 767
    .local v3, "projection":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "mimetype = \'vnd.android.cursor.item/name\' AND contact_id IN ("

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 769
    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 770
    const-string/jumbo v5, ") "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 767
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 772
    .local v4, "where":Ljava/lang/String;
    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    .line 773
    const/4 v5, 0x0

    const-string/jumbo v6, "contact_id"

    .line 772
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 775
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v11, :cond_4

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_4

    .line 777
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 779
    const-string/jumbo v2, "contact_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 781
    .local v9, "colId":I
    const-string/jumbo v2, "data3"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 783
    .local v7, "colFamilyName":I
    const-string/jumbo v2, "data2"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 786
    .local v8, "colGivenName":I
    :cond_0
    invoke-interface {v11, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 787
    .local v14, "id":I
    invoke-interface {v11, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 788
    .local v12, "familyName":Ljava/lang/String;
    invoke-interface {v11, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 790
    .local v13, "givenName":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v2, v14}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 791
    .local v10, "contact":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    if-eqz v10, :cond_1

    .line 792
    invoke-virtual {v10, v13}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->setFirstName(Ljava/lang/String;)V

    .line 793
    invoke-virtual {v10, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->setLastName(Ljava/lang/String;)V

    .line 795
    :cond_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 798
    .end local v7    # "colFamilyName":I
    .end local v8    # "colGivenName":I
    .end local v9    # "colId":I
    .end local v10    # "contact":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    .end local v12    # "familyName":Ljava/lang/String;
    .end local v13    # "givenName":Ljava/lang/String;
    .end local v14    # "id":I
    :cond_2
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 806
    :cond_3
    :goto_0
    return-void

    .line 802
    :cond_4
    if-eqz v11, :cond_3

    .line 803
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private fillPhoneNumberIntoContactCache(Landroid/content/Context;)V
    .locals 24
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 271
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 273
    .local v2, "contentResolver":Landroid/content/ContentResolver;
    const/16 v20, 0x0

    .line 274
    .local v20, "startOffset":I
    const/16 v13, 0x1f4

    .line 275
    .local v13, "diffValue":I
    add-int v14, v20, v13

    .line 278
    .local v14, "endOffset":I
    :cond_0
    add-int v14, v20, v13

    .line 279
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v3, v14, :cond_1

    .line 280
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v14

    .line 283
    :cond_1
    sub-int v3, v14, v20

    new-array v6, v3, [Ljava/lang/String;

    .line 284
    .local v6, "contactIds":[Ljava/lang/String;
    move/from16 v15, v20

    .local v15, "i":I
    :goto_0
    if-lt v15, v14, :cond_6

    .line 290
    array-length v3, v6

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getSQLWhereInSubQuery(I)Ljava/lang/String;

    move-result-object v17

    .line 292
    .local v17, "inQuery":Ljava/lang/String;
    const/4 v3, 0x3

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 293
    const-string/jumbo v7, "contact_id"

    aput-object v7, v4, v3

    const/4 v3, 0x1

    .line 294
    const-string/jumbo v7, "data2"

    aput-object v7, v4, v3

    const/4 v3, 0x2

    .line 295
    const-string/jumbo v7, "data1"

    aput-object v7, v4, v3

    .line 298
    .local v4, "projection":[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "contact_id IN ("

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 299
    const-string/jumbo v7, ") "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 298
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 302
    .local v5, "where":Ljava/lang/String;
    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    .line 303
    const-string/jumbo v7, "contact_id"

    .line 301
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 304
    .local v12, "cursor":Landroid/database/Cursor;
    if-eqz v12, :cond_5

    .line 305
    const-string/jumbo v3, "CDH"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v22, "fillPhoneNumberIntoContactCache : cur len = "

    move-object/from16 v0, v22

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 308
    const-string/jumbo v3, "contact_id"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 309
    .local v8, "colId":I
    const-string/jumbo v3, "data2"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 311
    .local v10, "colType":I
    const-string/jumbo v3, "data1"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 314
    .local v9, "colPhoneNumber":I
    :cond_2
    invoke-interface {v12, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 315
    .local v16, "id":I
    invoke-interface {v12, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    .line 316
    .local v21, "type":I
    invoke-interface {v12, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 318
    .local v19, "phoneNumber":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 319
    .local v11, "contact":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    if-eqz v11, :cond_3

    .line 320
    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    move/from16 v0, v21

    move-object/from16 v1, v19

    invoke-direct {v3, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;-><init>(ILjava/lang/String;)V

    invoke-virtual {v11, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->addPhoneNumber(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;)V

    .line 322
    :cond_3
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 325
    .end local v8    # "colId":I
    .end local v9    # "colPhoneNumber":I
    .end local v10    # "colType":I
    .end local v11    # "contact":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    .end local v16    # "id":I
    .end local v19    # "phoneNumber":Ljava/lang/String;
    .end local v21    # "type":I
    :cond_4
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 327
    :cond_5
    add-int v20, v20, v13

    .line 328
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 277
    if-lt v14, v3, :cond_0

    .line 330
    return-void

    .line 285
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v5    # "where":Ljava/lang/String;
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v17    # "inQuery":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v3, v15}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 286
    .local v18, "item":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    sub-int v3, v15, v20

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->getContactId()J

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    .line 284
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_0
.end method

.method private fillPhoneNumberIntoContactCache(Landroid/content/Context;I)V
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contactId"    # I

    .prologue
    .line 809
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 811
    .local v1, "contentResolver":Landroid/content/ContentResolver;
    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 812
    const-string/jumbo v5, "contact_id"

    aput-object v5, v3, v2

    const/4 v2, 0x1

    .line 813
    const-string/jumbo v5, "data2"

    aput-object v5, v3, v2

    const/4 v2, 0x2

    .line 814
    const-string/jumbo v5, "data1"

    aput-object v5, v3, v2

    .line 817
    .local v3, "projection":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "contact_id IN ("

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 818
    const-string/jumbo v5, ") "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 817
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 820
    .local v4, "where":Ljava/lang/String;
    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    .line 821
    const/4 v5, 0x0

    const-string/jumbo v6, "contact_id"

    .line 820
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 823
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v11, :cond_4

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_4

    .line 825
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 826
    const-string/jumbo v2, "contact_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 827
    .local v7, "colId":I
    const-string/jumbo v2, "data2"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 829
    .local v9, "colType":I
    const-string/jumbo v2, "data1"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 832
    .local v8, "colPhoneNumber":I
    :cond_0
    invoke-interface {v11, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 833
    .local v12, "id":I
    invoke-interface {v11, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 834
    .local v14, "type":I
    invoke-interface {v11, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 836
    .local v13, "phoneNumber":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v2, v12}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 837
    .local v10, "contact":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    if-eqz v10, :cond_1

    .line 838
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    invoke-direct {v2, v14, v13}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;-><init>(ILjava/lang/String;)V

    invoke-virtual {v10, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->addPhoneNumber(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;)V

    .line 840
    :cond_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 843
    .end local v7    # "colId":I
    .end local v8    # "colPhoneNumber":I
    .end local v9    # "colType":I
    .end local v10    # "contact":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    .end local v12    # "id":I
    .end local v13    # "phoneNumber":Ljava/lang/String;
    .end local v14    # "type":I
    :cond_2
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 850
    :cond_3
    :goto_0
    return-void

    .line 847
    :cond_4
    if-eqz v11, :cond_3

    .line 848
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private declared-synchronized finalizeCaches()V
    .locals 1

    .prologue
    .line 144
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactIdHashmapWithPhoneNumber:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 145
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactIdHashmapWithPhoneNumber:Ljava/util/HashMap;

    .line 147
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 148
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    monitor-exit p0

    return-void

    .line 144
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private getContactIdFromDisplayName(Landroid/content/Context;Ljava/lang/String;)I
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "displayName"    # Ljava/lang/String;

    .prologue
    .line 1404
    const/4 v7, 0x0

    .line 1405
    .local v7, "contactId":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1407
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "display_name LIKE \"%"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1408
    const-string/jumbo v2, "%\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1407
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1409
    .local v3, "selection":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1410
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 1412
    .local v5, "sortOrder":Ljava/lang/String;
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1414
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 1415
    const-string/jumbo v1, "_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 1417
    .local v6, "colId":I
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1418
    invoke-interface {v8, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1421
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1423
    .end local v6    # "colId":I
    :cond_1
    return v7
.end method

.method private getContactInfo(Landroid/content/Context;I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .locals 18
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contactId"    # I

    .prologue
    .line 1148
    const/4 v11, 0x0

    .line 1150
    .local v11, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1152
    .local v2, "contentResolver":Landroid/content/ContentResolver;
    const/4 v3, 0x3

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 1153
    const-string/jumbo v6, "_id"

    aput-object v6, v4, v3

    const/4 v3, 0x1

    const-string/jumbo v6, "display_name"

    aput-object v6, v4, v3

    const/4 v3, 0x2

    .line 1154
    const-string/jumbo v6, "phonetic_name"

    aput-object v6, v4, v3

    .line 1156
    .local v4, "projection":[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "(has_phone_number > 0) AND _id = "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1157
    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1156
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1159
    .local v5, "selection":Ljava/lang/String;
    const-string/jumbo v7, "display_name ASC "

    .line 1161
    .local v7, "sortOrder":Ljava/lang/String;
    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    .line 1162
    const/4 v6, 0x0

    .line 1161
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1163
    .local v12, "cursor":Landroid/database/Cursor;
    if-eqz v12, :cond_2

    .line 1164
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1165
    const-string/jumbo v3, "_id"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 1166
    .local v8, "colId":I
    const-string/jumbo v3, "display_name"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 1167
    .local v9, "colName":I
    const-string/jumbo v3, "phonetic_name"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 1170
    .local v10, "colPhoneticName":I
    :cond_0
    invoke-interface {v12, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 1171
    .local v14, "id":I
    invoke-interface {v12, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 1172
    .local v13, "displayName":Ljava/lang/String;
    invoke-interface {v12, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1173
    .local v15, "phoneticName":Ljava/lang/String;
    new-instance v11, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .end local v11    # "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    int-to-long v0, v14

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-direct {v11, v0, v1, v13, v15}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    .line 1174
    .restart local v11    # "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1177
    .end local v8    # "colId":I
    .end local v9    # "colName":I
    .end local v10    # "colPhoneticName":I
    .end local v13    # "displayName":Ljava/lang/String;
    .end local v14    # "id":I
    .end local v15    # "phoneticName":Ljava/lang/String;
    :cond_1
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1179
    :cond_2
    return-object v11
.end method

.method private getFirstGivenNameIntoContactCache(Landroid/content/Context;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 707
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactNameCache:Ljava/util/HashMap;

    .line 708
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 710
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v1, 0x3

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 711
    const-string/jumbo v4, "contact_id"

    aput-object v4, v2, v1

    const/4 v1, 0x1

    .line 712
    const-string/jumbo v4, "data3"

    aput-object v4, v2, v1

    const/4 v1, 0x2

    .line 713
    const-string/jumbo v4, "data2"

    aput-object v4, v2, v1

    .line 716
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v3, "mimetype = \'vnd.android.cursor.item/name\'"

    .line 719
    .local v3, "where":Ljava/lang/String;
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    .line 720
    const/4 v4, 0x0

    const-string/jumbo v5, "contact_id"

    .line 719
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 721
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_4

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_4

    .line 723
    const-string/jumbo v1, "CDH"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "getFirstGivenNameIntoContactCache : cur len = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 726
    const-string/jumbo v1, "contact_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 728
    .local v8, "colId":I
    const-string/jumbo v1, "data3"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 730
    .local v6, "colFamilyName":I
    const-string/jumbo v1, "data2"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 733
    .local v7, "colGivenName":I
    :cond_0
    invoke-interface {v9, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 734
    .local v12, "id":I
    invoke-interface {v9, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 735
    .local v10, "familyName":Ljava/lang/String;
    invoke-interface {v9, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 737
    .local v11, "givenName":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactNameCache:Ljava/util/HashMap;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;

    .line 738
    .local v13, "nameMap":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;
    if-nez v13, :cond_3

    .line 739
    new-instance v13, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;

    .end local v13    # "nameMap":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;
    invoke-direct {v13, v10, v11}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    .restart local v13    # "nameMap":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactNameCache:Ljava/util/HashMap;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 745
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 748
    .end local v6    # "colFamilyName":I
    .end local v7    # "colGivenName":I
    .end local v8    # "colId":I
    .end local v10    # "familyName":Ljava/lang/String;
    .end local v11    # "givenName":Ljava/lang/String;
    .end local v12    # "id":I
    .end local v13    # "nameMap":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 756
    :cond_2
    :goto_1
    return-void

    .line 742
    .restart local v6    # "colFamilyName":I
    .restart local v7    # "colGivenName":I
    .restart local v8    # "colId":I
    .restart local v10    # "familyName":Ljava/lang/String;
    .restart local v11    # "givenName":Ljava/lang/String;
    .restart local v12    # "id":I
    .restart local v13    # "nameMap":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;
    :cond_3
    invoke-virtual {v13, v10}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;->setFamilyName(Ljava/lang/String;)V

    .line 743
    invoke-virtual {v13, v11}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;->setGivenName(Ljava/lang/String;)V

    goto :goto_0

    .line 752
    .end local v6    # "colFamilyName":I
    .end local v7    # "colGivenName":I
    .end local v8    # "colId":I
    .end local v10    # "familyName":Ljava/lang/String;
    .end local v11    # "givenName":Ljava/lang/String;
    .end local v12    # "id":I
    .end local v13    # "nameMap":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;
    :cond_4
    if-eqz v9, :cond_2

    .line 753
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method private getHighQualityContactImage(Landroid/content/Context;J)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contactId"    # J

    .prologue
    const/4 v7, 0x0

    .line 1479
    const-wide/16 v8, 0x0

    cmp-long v8, p2, v8

    if-gez v8, :cond_1

    move-object v0, v7

    .line 1502
    :cond_0
    :goto_0
    return-object v0

    .line 1483
    :cond_1
    const/4 v0, 0x0

    .line 1484
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1486
    .local v2, "contentResolver":Landroid/content/ContentResolver;
    sget-object v8, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v8, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 1487
    .local v1, "contactUri":Landroid/net/Uri;
    const-string/jumbo v8, "display_photo"

    invoke-static {v1, v8}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1489
    .local v3, "displayPhotoUri":Landroid/net/Uri;
    const-string/jumbo v8, "r"

    invoke-virtual {v2, v3, v8}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v5

    .line 1490
    .local v5, "fd":Landroid/content/res/AssetFileDescriptor;
    if-eqz v5, :cond_0

    .line 1491
    invoke-virtual {v5}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v6

    .line 1493
    .local v6, "photoDataStream":Ljava/io/InputStream;
    invoke-static {v6}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1498
    .end local v1    # "contactUri":Landroid/net/Uri;
    .end local v2    # "contentResolver":Landroid/content/ContentResolver;
    .end local v3    # "displayPhotoUri":Landroid/net/Uri;
    .end local v5    # "fd":Landroid/content/res/AssetFileDescriptor;
    .end local v6    # "photoDataStream":Ljava/io/InputStream;
    :catch_0
    move-exception v4

    .line 1499
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .end local v4    # "e":Ljava/lang/Exception;
    :goto_1
    move-object v0, v7

    .line 1502
    goto :goto_0

    .line 1497
    :catch_1
    move-exception v8

    goto :goto_1
.end method

.method private getMainPhoneNumber(Landroid/content/Context;J)Ljava/lang/String;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contactId"    # J

    .prologue
    const/4 v2, 0x0

    .line 1320
    const/4 v9, 0x0

    .line 1321
    .local v9, "mainPhoneNumber":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1322
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "content://com.android.contacts/contacts/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/data"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .local v1, "uri":Landroid/net/Uri;
    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 1323
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1325
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_3

    .line 1326
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1328
    :cond_0
    const-string/jumbo v2, "is_super_primary"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 1330
    .local v8, "isSuperPrimary":I
    const-string/jumbo v2, "data1"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 1332
    .local v11, "phoneIndex":I
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1333
    .local v7, "isSuper":I
    invoke-interface {v6, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1335
    .local v10, "number":Ljava/lang/String;
    const/4 v2, 0x1

    if-ne v7, v2, :cond_1

    .line 1336
    move-object v9, v10

    .line 1338
    :cond_1
    const-string/jumbo v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "[snowdeer] isSuper : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1339
    const-string/jumbo v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "[snowdeer] number : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1341
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1344
    .end local v7    # "isSuper":I
    .end local v8    # "isSuperPrimary":I
    .end local v10    # "number":Ljava/lang/String;
    .end local v11    # "phoneIndex":I
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1347
    :cond_3
    return-object v9
.end method

.method private getPhoneNumberIntoContactCache(Landroid/content/Context;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 661
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 662
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactImplCache:Ljava/util/HashMap;

    .line 663
    const/4 v1, 0x3

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 664
    const-string/jumbo v4, "contact_id"

    aput-object v4, v2, v1

    const/4 v1, 0x1

    .line 665
    const-string/jumbo v4, "data2"

    aput-object v4, v2, v1

    const/4 v1, 0x2

    .line 666
    const-string/jumbo v4, "data1"

    aput-object v4, v2, v1

    .line 669
    .local v2, "projection":[Ljava/lang/String;
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    .line 670
    const-string/jumbo v5, "contact_id"

    move-object v4, v3

    .line 669
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 671
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_3

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_3

    .line 673
    const-string/jumbo v1, "CDH"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "getPhoneNumberIntoContactCache : cur len = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 674
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 675
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactImplCache:Ljava/util/HashMap;

    .line 676
    const-string/jumbo v1, "contact_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 677
    .local v6, "colId":I
    const-string/jumbo v1, "data2"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 679
    .local v8, "colType":I
    const-string/jumbo v1, "data1"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 682
    .local v7, "colPhoneNumber":I
    :cond_0
    invoke-interface {v9, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 683
    .local v10, "id":I
    invoke-interface {v9, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 684
    .local v13, "type":I
    invoke-interface {v9, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 686
    .local v12, "phoneNumber":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactImplCache:Ljava/util/HashMap;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/ArrayList;

    .line 687
    .local v11, "phoneList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    if-nez v11, :cond_2

    .line 688
    new-instance v11, Ljava/util/ArrayList;

    .end local v11    # "phoneList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 689
    .restart local v11    # "phoneList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    invoke-direct {v1, v13, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;-><init>(ILjava/lang/String;)V

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 690
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactImplCache:Ljava/util/HashMap;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 694
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 703
    .end local v6    # "colId":I
    .end local v7    # "colPhoneNumber":I
    .end local v8    # "colType":I
    .end local v10    # "id":I
    .end local v11    # "phoneList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    .end local v12    # "phoneNumber":Ljava/lang/String;
    .end local v13    # "type":I
    :cond_1
    :goto_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 704
    return-void

    .line 692
    .restart local v6    # "colId":I
    .restart local v7    # "colPhoneNumber":I
    .restart local v8    # "colType":I
    .restart local v10    # "id":I
    .restart local v11    # "phoneList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    .restart local v12    # "phoneNumber":Ljava/lang/String;
    .restart local v13    # "type":I
    :cond_2
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    invoke-direct {v1, v13, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;-><init>(ILjava/lang/String;)V

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 699
    .end local v6    # "colId":I
    .end local v7    # "colPhoneNumber":I
    .end local v8    # "colType":I
    .end local v10    # "id":I
    .end local v11    # "phoneList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    .end local v12    # "phoneNumber":Ljava/lang/String;
    .end local v13    # "type":I
    :cond_3
    if-eqz v9, :cond_1

    .line 700
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method private getSQLWhereInSubQuery(I)Ljava/lang/String;
    .locals 3
    .param p1, "itemCount"    # I

    .prologue
    .line 405
    new-instance v0, Ljava/lang/StringBuilder;

    mul-int/lit8 v2, p1, 0x2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 406
    .local v0, "inList":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    if-lt v1, p1, :cond_0

    .line 412
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 407
    :cond_0
    if-lez v1, :cond_1

    .line 408
    const-string/jumbo v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    :cond_1
    const-string/jumbo v2, "?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 406
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private getThumbnailContactImage(Landroid/content/Context;J)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contactId"    # J

    .prologue
    const/4 v5, 0x0

    .line 1507
    const-wide/16 v6, 0x0

    cmp-long v6, p2, v6

    if-gez v6, :cond_0

    move-object v0, v5

    .line 1521
    :goto_0
    return-object v0

    .line 1511
    :cond_0
    const/4 v0, 0x0

    .line 1512
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1514
    .local v2, "contentResolver":Landroid/content/ContentResolver;
    sget-object v6, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 1515
    .local v1, "contactPhotoUri":Landroid/net/Uri;
    invoke-static {v2, v1}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v4

    .line 1517
    .local v4, "photoDataStream":Ljava/io/InputStream;
    invoke-static {v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1518
    goto :goto_0

    .line 1519
    .end local v1    # "contactPhotoUri":Landroid/net/Uri;
    .end local v2    # "contentResolver":Landroid/content/ContentResolver;
    .end local v4    # "photoDataStream":Ljava/io/InputStream;
    :catch_0
    move-exception v3

    .line 1520
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v5

    .line 1521
    goto :goto_0
.end method

.method private declared-synchronized initializeCaches(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 113
    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v6}, Landroid/util/SparseArray;->clear()V

    .line 114
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactIdHashmapWithPhoneNumber:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    .line 116
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->buildContactCacheWireFrame(Landroid/content/Context;)V

    .line 117
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 119
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->fillPhoneNumberIntoContactCache(Landroid/content/Context;)V

    .line 120
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->fillFirstGivenNameIntoContactCache(Landroid/content/Context;)V

    .line 122
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-lt v2, v6, :cond_1

    .line 141
    monitor-exit p0

    return-void

    .line 123
    :cond_1
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v6, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 125
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v5

    .line 127
    .local v5, "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    if-eqz v5, :cond_2

    .line 128
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v3, v6, :cond_3

    .line 122
    .end local v3    # "j":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 129
    .restart local v3    # "j":I
    :cond_3
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    .line 130
    .local v1, "dlPhoneNumber":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    if-eqz v1, :cond_4

    .line 131
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v4

    .line 132
    .local v4, "phoneNumber":Ljava/lang/String;
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_4

    .line 133
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactIdHashmapWithPhoneNumber:Ljava/util/HashMap;

    .line 134
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v7

    long-to-int v7, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 133
    invoke-virtual {v6, v4, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 128
    .end local v4    # "phoneNumber":Ljava/lang/String;
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 113
    .end local v0    # "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v1    # "dlPhoneNumber":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    .end local v2    # "i":I
    .end local v3    # "j":I
    .end local v5    # "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method

.method private isDuplicatedPhoneNumber(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;)Z
    .locals 6
    .param p2, "phoneNumber"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    const/4 v4, 0x1

    .line 1380
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1400
    :cond_0
    :goto_0
    return v4

    .line 1384
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v0, v5, :cond_2

    .line 1400
    const/4 v4, 0x0

    goto :goto_0

    .line 1385
    :cond_2
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    .line 1386
    .local v1, "source":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    if-eqz v1, :cond_3

    .line 1387
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1388
    .local v2, "str1":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1390
    .local v3, "str2":Ljava/lang/String;
    if-nez v2, :cond_4

    .line 1384
    .end local v2    # "str1":Ljava/lang/String;
    .end local v3    # "str2":Ljava/lang/String;
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1394
    .restart local v2    # "str1":Ljava/lang/String;
    .restart local v3    # "str2":Ljava/lang/String;
    :cond_4
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    goto :goto_0
.end method

.method private declared-synchronized isExistContactInFavorite(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Z
    .locals 8
    .param p1, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 897
    monitor-enter p0

    const/4 v3, 0x0

    .line 900
    .local v3, "result":Z
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mDbHelper:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 901
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "WHERE contactId = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 903
    .local v4, "where":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "SELECT contactId FROM TABLE_FAVORITE_CONTACT "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 904
    const/4 v6, 0x0

    .line 903
    invoke-virtual {v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 905
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_1

    .line 906
    :cond_0
    const/4 v3, 0x1

    .line 909
    :cond_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 910
    if-eqz v0, :cond_2

    .line 911
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 917
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v4    # "where":Ljava/lang/String;
    :cond_2
    :goto_0
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mDbHelper:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 919
    monitor-exit p0

    return v3

    .line 913
    :catch_0
    move-exception v2

    .line 914
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 897
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method


# virtual methods
.method public addContactPhoneNumber(Ljava/util/ArrayList;JILjava/lang/String;)V
    .locals 4
    .param p2, "contactId"    # J
    .param p4, "type"    # I
    .param p5, "phoneNumber"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;JI",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 535
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 549
    :cond_0
    :goto_0
    return-void

    .line 539
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 540
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 542
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    if-eqz v0, :cond_2

    .line 543
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->getContactId()J

    move-result-wide v2

    cmp-long v2, v2, p2

    if-nez v2, :cond_2

    .line 544
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    invoke-direct {v2, p4, p5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->addPhoneNumber(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;)V

    goto :goto_0

    .line 539
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public declared-synchronized addFavoriteContact(ILcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Z
    .locals 6
    .param p1, "type"    # I
    .param p2, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    const/4 v3, 0x0

    .line 923
    monitor-enter p0

    if-nez p2, :cond_1

    .line 949
    :cond_0
    :goto_0
    monitor-exit p0

    return v3

    .line 927
    :cond_1
    :try_start_0
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->isExistContactInFavorite(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_0

    .line 932
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mDbHelper:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 934
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 936
    .local v2, "row":Landroid/content/ContentValues;
    const-string/jumbo v3, "contactId"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 937
    const-string/jumbo v3, "contactType"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 938
    const-string/jumbo v3, "orderValue"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 940
    const-string/jumbo v3, "TABLE_FAVORITE_CONTACT"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 942
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 947
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "row":Landroid/content/ContentValues;
    :goto_1
    :try_start_2
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mDbHelper:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;->close()V

    .line 949
    const/4 v3, 0x1

    goto :goto_0

    .line 943
    :catch_0
    move-exception v1

    .line 944
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 923
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public deleteFavoriteContact(II)V
    .locals 5
    .param p1, "type"    # I
    .param p2, "contactId"    # I

    .prologue
    .line 954
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mDbHelper:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 956
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "contactId = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " AND contactType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 958
    .local v2, "strFilter":Ljava/lang/String;
    const-string/jumbo v3, "TABLE_FAVORITE_CONTACT"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 960
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 965
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "strFilter":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mDbHelper:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;->close()V

    .line 966
    return-void

    .line 961
    :catch_0
    move-exception v1

    .line 962
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public declared-synchronized deleteFavoriteContact(ILcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
    .locals 6
    .param p1, "type"    # I
    .param p2, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 970
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mDbHelper:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 972
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "contactId = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " AND contactType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 973
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 972
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 975
    .local v2, "strFilter":Ljava/lang/String;
    const-string/jumbo v3, "TABLE_FAVORITE_CONTACT"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 977
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 982
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "strFilter":Ljava/lang/String;
    :goto_0
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mDbHelper:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 983
    monitor-exit p0

    return-void

    .line 978
    :catch_0
    move-exception v1

    .line 979
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 970
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public getCallLogImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "calllog"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .prologue
    const/4 v4, 0x0

    .line 1579
    if-nez p2, :cond_1

    move-object v0, v4

    .line 1595
    :cond_0
    :goto_0
    return-object v0

    .line 1583
    :cond_1
    const/4 v0, 0x0

    .line 1585
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, p1, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getContactIDFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    int-to-long v1, v5

    .line 1587
    .local v1, "contactId":J
    invoke-direct {p0, p1, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getHighQualityContactImage(Landroid/content/Context;J)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1588
    if-nez v0, :cond_0

    .line 1589
    invoke-direct {p0, p1, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getThumbnailContactImage(Landroid/content/Context;J)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1593
    .end local v1    # "contactId":J
    :catch_0
    move-exception v3

    .line 1594
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v4

    .line 1595
    goto :goto_0
.end method

.method public getContactDisplayName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 1050
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 1083
    .end local p2    # "phoneNumber":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p2

    .line 1054
    .restart local p2    # "phoneNumber":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1056
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const-wide/16 v6, -0x1

    .line 1072
    .local v6, "contactId":J
    sget-object v1, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p2}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1073
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    .line 1074
    const-string/jumbo v4, "_id"

    aput-object v4, v2, v9

    move-object v4, v3

    move-object v5, v3

    .line 1071
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1076
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_3

    .line 1077
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1078
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1081
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1083
    :cond_3
    invoke-virtual {p0, p1, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getDisplayName(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method public getContactFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 1186
    const/4 v0, -0x1

    .line 1193
    .local v0, "contactId":I
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getContactIDFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 1196
    if-gez v0, :cond_0

    .line 1197
    const/4 v1, 0x0

    .line 1201
    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getContactInfo(Landroid/content/Context;I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v1

    goto :goto_0
.end method

.method public getContactIDFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)I
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    const/4 v9, -0x1

    .line 1427
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "-2"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move v6, v9

    .line 1474
    :goto_0
    return v6

    .line 1431
    :cond_1
    const/4 v6, -0x1

    .line 1450
    .local v6, "contactId":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1454
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    :try_start_0
    sget-object v1, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p2}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1455
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 1456
    const-string/jumbo v4, "_id"

    aput-object v4, v2, v3

    .line 1457
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1453
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1459
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_2

    move v6, v9

    .line 1460
    goto :goto_0

    .line 1462
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1463
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 1468
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1469
    .end local v7    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v8

    .line 1470
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    .line 1471
    const-string/jumbo v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[snowdeer] ERROR getContactIDFromPhoneNumber : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    const/4 v4, 0x0

    .line 1554
    if-nez p2, :cond_1

    move-object v1, v4

    .line 1573
    :cond_0
    :goto_0
    return-object v1

    .line 1558
    :cond_1
    :try_start_0
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-gez v5, :cond_2

    move-object v1, v4

    .line 1559
    goto :goto_0

    .line 1562
    :cond_2
    const/4 v1, 0x0

    .line 1563
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    move-object v0, p2

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    move-object v2, v0

    .line 1565
    .local v2, "contactInfo":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->getContactId()J

    move-result-wide v5

    invoke-direct {p0, p1, v5, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getHighQualityContactImage(Landroid/content/Context;J)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1566
    if-nez v1, :cond_0

    .line 1567
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->getContactId()J

    move-result-wide v5

    invoke-direct {p0, p1, v5, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getThumbnailContactImage(Landroid/content/Context;J)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 1571
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "contactInfo":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    :catch_0
    move-exception v3

    .line 1572
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    move-object v1, v4

    .line 1573
    goto :goto_0
.end method

.method public getContactImageFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 1601
    if-nez p2, :cond_1

    move-object v0, v4

    .line 1617
    :cond_0
    :goto_0
    return-object v0

    .line 1605
    :cond_1
    const/4 v0, 0x0

    .line 1607
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getContactIDFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    int-to-long v1, v5

    .line 1609
    .local v1, "contactId":J
    invoke-direct {p0, p1, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getHighQualityContactImage(Landroid/content/Context;J)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1610
    if-nez v0, :cond_0

    .line 1611
    invoke-direct {p0, p1, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getThumbnailContactImage(Landroid/content/Context;J)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1615
    .end local v1    # "contactId":J
    :catch_0
    move-exception v3

    .line 1616
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v4

    .line 1617
    goto :goto_0
.end method

.method public declared-synchronized getContactList(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 986
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 990
    .local v2, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    if-nez p2, :cond_2

    .line 991
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-lt v0, v3, :cond_1

    .line 1008
    :cond_0
    monitor-exit p0

    return-object v2

    .line 992
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 991
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 998
    .end local v0    # "i":I
    :cond_2
    move v1, p2

    .line 999
    .local v1, "maxItemCount":I
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v3, p2, :cond_3

    .line 1000
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v1

    .line 1003
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    if-ge v0, v1, :cond_0

    .line 1004
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1003
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 986
    .end local v0    # "i":I
    .end local v1    # "maxItemCount":I
    .end local v2    # "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public getDisplayName(Landroid/content/Context;J)Ljava/lang/String;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contactID"    # J

    .prologue
    const/4 v2, 0x0

    .line 1087
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1089
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v8, 0x0

    .line 1090
    .local v8, "displayName":Ljava/lang/String;
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    .line 1091
    const-string/jumbo v3, "_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 1092
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v5

    move-object v5, v2

    .line 1090
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1094
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_1

    .line 1095
    const-string/jumbo v1, "display_name"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 1096
    .local v6, "colName":I
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1097
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1100
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1102
    .end local v6    # "colName":I
    :cond_1
    return-object v8
.end method

.method public declared-synchronized getFavoriteContactList(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 854
    monitor-enter p0

    :try_start_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 855
    .local v5, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "WHERE contactType = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 856
    .local v6, "where":Ljava/lang/String;
    const/4 v3, 0x0

    .line 857
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v2, 0x0

    .line 860
    .local v2, "cursor":Landroid/database/Cursor;
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mDbHelper:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 861
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "SELECT contactId FROM TABLE_FAVORITE_CONTACT "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 866
    :goto_0
    if-eqz v2, :cond_0

    .line 867
    :try_start_2
    sget-object v7, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "getFavoriteContactList : cur len = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 868
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 886
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 887
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 889
    invoke-virtual {p0, p1, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->rescanContactList(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 891
    :cond_0
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mDbHelper:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 893
    monitor-exit p0

    return-object v5

    .line 862
    :catch_0
    move-exception v4

    .line 863
    .local v4, "e":Ljava/lang/Exception;
    :try_start_3
    sget-object v7, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "getFavoriteContactList DB Exception"

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 854
    .end local v2    # "cursor":Landroid/database/Cursor;
    .end local v3    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v5    # "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    .end local v6    # "where":Ljava/lang/String;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 869
    .restart local v2    # "cursor":Landroid/database/Cursor;
    .restart local v3    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v5    # "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    .restart local v6    # "where":Ljava/lang/String;
    :cond_1
    :try_start_4
    sget-object v7, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "[snowdeer] favorite : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x0

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 870
    const/4 v7, 0x0

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 872
    .local v1, "contactId":I
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v7, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 873
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-nez v0, :cond_3

    .line 874
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->rescanContactDB(Landroid/content/Context;)V

    .line 875
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v7, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 876
    .restart local v0    # "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-eqz v0, :cond_2

    .line 877
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v7, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 879
    :cond_2
    const/4 v7, 0x0

    invoke-virtual {p0, v7, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->deleteFavoriteContact(II)V

    goto :goto_1

    .line 882
    :cond_3
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public getInboxListWithContactData(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1109
    .local p2, "inBox":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;>;"
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_2

    :cond_0
    move-object v6, p2

    .line 1144
    :cond_1
    return-object v6

    .line 1113
    :cond_2
    move-object v6, p2

    .line 1115
    .local v6, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v3, v8, :cond_1

    .line 1116
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;

    .line 1117
    .local v0, "boxItem":Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    .line 1118
    .local v5, "phoneNumber":Ljava/lang/String;
    invoke-virtual {p0, p1, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getContactIDFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    .line 1122
    .local v2, "contactId":I
    invoke-direct {p0, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getContactInfo(Landroid/content/Context;I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v1

    .line 1126
    .local v1, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-nez v1, :cond_4

    .line 1127
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_3

    .line 1130
    const-string/jumbo v8, "phone"

    invoke-virtual {p1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    .line 1129
    check-cast v7, Landroid/telephony/TelephonyManager;

    .line 1131
    .local v7, "telephony":Landroid/telephony/TelephonyManager;
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v4

    .line 1133
    .local v4, "myPhoneNumber":Ljava/lang/String;
    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1134
    new-instance v8, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    const/4 v9, 0x1

    invoke-direct {v8, v5, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->setDLContact(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    .line 1141
    .end local v4    # "myPhoneNumber":Ljava/lang/String;
    .end local v7    # "telephony":Landroid/telephony/TelephonyManager;
    :cond_3
    :goto_1
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->setDLContact(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    .line 1115
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    move-object v8, v1

    .line 1138
    check-cast v8, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    invoke-virtual {v8, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->addPhoneNumber(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getPhoneNumberList(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contactId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1282
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1285
    .local v7, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v8, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 1286
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    int-to-long v8, p2

    invoke-direct {p0, p1, v8, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getMainPhoneNumber(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    .line 1288
    .local v3, "mainPhoneNumber":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1289
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v5

    .line 1291
    .local v5, "numberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    if-eqz v5, :cond_0

    .line 1292
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v2, v8, :cond_1

    .line 1314
    .end local v0    # "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v2    # "i":I
    .end local v3    # "mainPhoneNumber":Ljava/lang/String;
    .end local v5    # "numberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    :cond_0
    :goto_1
    return-object v7

    .line 1293
    .restart local v0    # "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .restart local v2    # "i":I
    .restart local v3    # "mainPhoneNumber":Ljava/lang/String;
    .restart local v5    # "numberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    :cond_1
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    .line 1294
    .local v6, "phoneNumber":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;
    invoke-direct {p0, v7, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->isDuplicatedPhoneNumber(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1292
    :cond_2
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1298
    :cond_3
    invoke-virtual {v6, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;->checkIsMainPhoneNumber(Ljava/lang/String;)V

    .line 1299
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;->getPhoneNumber()Ljava/lang/String;

    move-result-object v4

    .line 1300
    .local v4, "number":Ljava/lang/String;
    const-string/jumbo v8, "-1"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    const-string/jumbo v8, "-2"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 1301
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1310
    .end local v0    # "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v2    # "i":I
    .end local v3    # "mainPhoneNumber":Ljava/lang/String;
    .end local v4    # "number":Ljava/lang/String;
    .end local v5    # "numberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    .end local v6    # "phoneNumber":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;
    :catch_0
    move-exception v1

    .line 1311
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public getPhoneNumberList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contactName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1205
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getContactIdFromDisplayName(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 1207
    .local v0, "contactId":I
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getPhoneNumberList(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v1

    return-object v1
.end method

.method public getPhoneNumberType(Landroid/content/Context;Ljava/lang/String;)I
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 223
    const/4 v13, -0x1

    .line 227
    .local v13, "phoneType":I
    :try_start_0
    invoke-virtual/range {p0 .. p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getContactIDFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)I

    move-result v9

    .line 228
    .local v9, "contactID":I
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 230
    .local v1, "contentResolver":Landroid/content/ContentResolver;
    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 231
    const-string/jumbo v5, "contact_id"

    aput-object v5, v3, v2

    const/4 v2, 0x1

    .line 232
    const-string/jumbo v5, "data2"

    aput-object v5, v3, v2

    const/4 v2, 0x2

    .line 233
    const-string/jumbo v5, "data1"

    aput-object v5, v3, v2

    .line 236
    .local v3, "projection":[Ljava/lang/String;
    const-string/jumbo v4, "contact_id = ? "

    .line 239
    .local v4, "where":Ljava/lang/String;
    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    .line 240
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    .line 241
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v5, v6

    .line 242
    const-string/jumbo v6, "contact_id"

    .line 238
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 244
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_2

    .line 245
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 247
    const-string/jumbo v2, "data2"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 249
    .local v8, "colType":I
    const-string/jumbo v2, "data1"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 250
    .local v7, "colPhoneNumber":I
    const-string/jumbo v2, "-"

    const-string/jumbo v5, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v5, " "

    const-string/jumbo v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    .line 252
    :cond_0
    invoke-interface {v10, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 253
    .local v11, "dbstr":Ljava/lang/String;
    const-string/jumbo v2, "-"

    const-string/jumbo v5, ""

    invoke-virtual {v11, v2, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v5, " "

    const-string/jumbo v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    .line 254
    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 255
    invoke-interface {v10, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 261
    .end local v7    # "colPhoneNumber":I
    .end local v8    # "colType":I
    .end local v11    # "dbstr":Ljava/lang/String;
    :cond_1
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 267
    .end local v1    # "contentResolver":Landroid/content/ContentResolver;
    .end local v3    # "projection":[Ljava/lang/String;
    .end local v4    # "where":Ljava/lang/String;
    .end local v9    # "contactID":I
    .end local v10    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    return v13

    .line 258
    .restart local v1    # "contentResolver":Landroid/content/ContentResolver;
    .restart local v3    # "projection":[Ljava/lang/String;
    .restart local v4    # "where":Ljava/lang/String;
    .restart local v7    # "colPhoneNumber":I
    .restart local v8    # "colType":I
    .restart local v9    # "contactID":I
    .restart local v10    # "cursor":Landroid/database/Cursor;
    .restart local v11    # "dbstr":Ljava/lang/String;
    :cond_3
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 263
    .end local v1    # "contentResolver":Landroid/content/ContentResolver;
    .end local v3    # "projection":[Ljava/lang/String;
    .end local v4    # "where":Ljava/lang/String;
    .end local v7    # "colPhoneNumber":I
    .end local v8    # "colType":I
    .end local v9    # "contactID":I
    .end local v10    # "cursor":Landroid/database/Cursor;
    .end local v11    # "dbstr":Ljava/lang/String;
    :catch_0
    move-exception v12

    .line 264
    .local v12, "e":Ljava/lang/Exception;
    const/4 v13, -0x1

    goto :goto_1
.end method

.method public getPhoneNumberTypeName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 204
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getPhoneNumberType(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 212
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 206
    :pswitch_0
    const-string/jumbo v0, "Mobile"

    goto :goto_0

    .line 208
    :pswitch_1
    const-string/jumbo v0, "Work"

    goto :goto_0

    .line 210
    :pswitch_2
    const-string/jumbo v0, "Home"

    goto :goto_0

    .line 204
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getSearchedContactList(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "itemCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1013
    const-string/jumbo v1, ""

    .line 1014
    .local v1, "displayName":Ljava/lang/String;
    const-string/jumbo v5, ""

    .line 1015
    .local v5, "src":Ljava/lang/String;
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 1016
    .local v3, "key":Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1017
    .local v4, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/SearchAlgorithm;->getInstance()Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/SearchAlgorithm;

    .line 1019
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v6

    if-lt v2, v6, :cond_1

    .line 1045
    :cond_0
    return-object v4

    .line 1020
    :cond_1
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v6, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 1023
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-eqz v0, :cond_2

    .line 1024
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 1026
    if-eqz v1, :cond_5

    .line 1027
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    .line 1034
    :cond_2
    :goto_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;->getInstance()Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;

    move-result-object v6

    invoke-virtual {v6, v5, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;->checkString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1035
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1038
    :cond_3
    if-lez p3, :cond_4

    .line 1039
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v6, p3, :cond_0

    .line 1019
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1029
    :cond_5
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public hasContactSaved(Landroid/content/Context;)Z
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1622
    const/4 v7, 0x0

    .line 1623
    .local v7, "hasContact":Z
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1624
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v1, 0x3

    new-array v2, v1, [Ljava/lang/String;

    .line 1625
    const-string/jumbo v1, "_id"

    aput-object v1, v2, v9

    const-string/jumbo v1, "display_name"

    aput-object v1, v2, v8

    const/4 v1, 0x2

    .line 1626
    const-string/jumbo v4, "phonetic_name"

    aput-object v4, v2, v1

    .line 1628
    .local v2, "projection":[Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "has_phone_number"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, " > 0"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1629
    .local v3, "selection":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "display_name"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, " ASC "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1630
    .local v5, "sortOrder":Ljava/lang/String;
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    .line 1631
    const/4 v4, 0x0

    .line 1630
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1632
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 1633
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    move v7, v8

    .line 1634
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1636
    :cond_0
    return v7

    :cond_1
    move v7, v9

    .line 1633
    goto :goto_0
.end method

.method public initialize(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 86
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;

    const-string/jumbo v1, "InitContactCache"

    invoke-direct {v0, p0, v1, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;Ljava/lang/String;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->initThread:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;

    .line 87
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->initThread:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;->start()V

    .line 95
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;

    invoke-direct {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mDbHelper:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;

    .line 96
    const/4 v0, 0x1

    return v0
.end method

.method public loadPhoneNumberListToCache(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1211
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mPhoneNumberContactCache:Ljava/util/HashMap;

    if-nez v9, :cond_0

    .line 1212
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    iput-object v9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mPhoneNumberContactCache:Ljava/util/HashMap;

    .line 1217
    :cond_0
    const/4 v3, 0x0

    .local v3, "index":I
    :goto_0
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v9}, Landroid/util/SparseArray;->size()I

    move-result v9

    if-lt v3, v9, :cond_1

    .line 1251
    :goto_1
    return-void

    .line 1218
    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1220
    .local v8, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v9, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 1221
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    int-to-long v9, v3

    invoke-direct {p0, p1, v9, v10}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getMainPhoneNumber(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    .line 1223
    .local v4, "mainPhoneNumber":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 1224
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v6

    .line 1226
    .local v6, "numberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    if-eqz v6, :cond_2

    .line 1227
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v2, v9, :cond_3

    .line 1240
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mPhoneNumberContactCache:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1217
    .end local v2    # "i":I
    .end local v6    # "numberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1228
    .restart local v2    # "i":I
    .restart local v6    # "numberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    :cond_3
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    .line 1229
    .local v7, "phoneNumber":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;
    invoke-direct {p0, v8, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->isDuplicatedPhoneNumber(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1227
    :cond_4
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1233
    :cond_5
    invoke-virtual {v7, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;->checkIsMainPhoneNumber(Ljava/lang/String;)V

    .line 1234
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    .line 1235
    .local v5, "number":Ljava/lang/String;
    const-string/jumbo v9, "-1"

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    const-string/jumbo v9, "-2"

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 1236
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 1248
    .end local v0    # "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v2    # "i":I
    .end local v4    # "mainPhoneNumber":Ljava/lang/String;
    .end local v5    # "number":Ljava/lang/String;
    .end local v6    # "numberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    .end local v7    # "phoneNumber":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;
    .end local v8    # "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    :catch_0
    move-exception v1

    .line 1249
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public refreshContactInfo(Landroid/content/Context;J)V
    .locals 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contactId"    # J

    .prologue
    .line 552
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 554
    .local v2, "contentResolver":Landroid/content/ContentResolver;
    const/4 v3, 0x3

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 555
    const-string/jumbo v6, "_id"

    aput-object v6, v4, v3

    const/4 v3, 0x1

    const-string/jumbo v6, "display_name"

    aput-object v6, v4, v3

    const/4 v3, 0x2

    .line 556
    const-string/jumbo v6, "phonetic_name"

    aput-object v6, v4, v3

    .line 559
    .local v4, "projection":[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "_id = "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 561
    .local v5, "selection":Ljava/lang/String;
    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    .line 562
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 561
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 564
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v11, :cond_2

    .line 565
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 566
    const-string/jumbo v3, "_id"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 567
    .local v8, "colId":I
    const-string/jumbo v3, "display_name"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 569
    .local v9, "colName":I
    const-string/jumbo v3, "phonetic_name"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 572
    .local v10, "colPhoneticName":I
    :cond_0
    invoke-interface {v11, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 573
    .local v13, "id":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    invoke-virtual {v3, v13}, Landroid/util/SparseArray;->remove(I)V

    .line 575
    invoke-interface {v11, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 576
    .local v12, "displayName":Ljava/lang/String;
    invoke-interface {v11, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 578
    .local v14, "phoneticName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCache:Landroid/util/SparseArray;

    new-instance v6, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    int-to-long v15, v13

    move-wide v0, v15

    invoke-direct {v6, v0, v1, v12, v14}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v13, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 579
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->fillPhoneNumberIntoContactCache(Landroid/content/Context;I)V

    .line 580
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->fillFirstGivenNameIntoContactCache(Landroid/content/Context;I)V

    .line 582
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 585
    .end local v8    # "colId":I
    .end local v9    # "colName":I
    .end local v10    # "colPhoneticName":I
    .end local v12    # "displayName":Ljava/lang/String;
    .end local v13    # "id":I
    .end local v14    # "phoneticName":Ljava/lang/String;
    :cond_1
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 587
    :cond_2
    return-void
.end method

.method public rescanContactDB(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 598
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCacheUpdate:Ljava/lang/Object;

    monitor-enter v1

    .line 656
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->initializeCaches(Landroid/content/Context;)V

    .line 598
    monitor-exit v1

    .line 658
    return-void

    .line 598
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public rescanContactList(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 417
    .local p2, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->updateContactNameInList(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 418
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->updatePhoneNumberInList(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 419
    return-void
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 101
    const-string/jumbo v0, "CDH"

    const-string/jumbo v1, "terminate : before sync"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCacheUpdate:Ljava/lang/Object;

    monitor-enter v1

    .line 104
    :try_start_0
    const-string/jumbo v0, "CDH"

    const-string/jumbo v2, "terminate : after sync"

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->initThread:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->initThread:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;->stopThread()V

    .line 107
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->finalizeCaches()V

    .line 103
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    const-string/jumbo v0, "CDH"

    const-string/jumbo v1, "terminate : end sync"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    return-void

    .line 103
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public updateContactDisplayName(Ljava/util/ArrayList;JLjava/lang/String;)V
    .locals 4
    .param p2, "contactId"    # J
    .param p4, "displayName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;J",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 467
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 481
    :cond_0
    :goto_0
    return-void

    .line 471
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 472
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 474
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    if-eqz v0, :cond_2

    .line 475
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->getContactId()J

    move-result-wide v2

    cmp-long v2, v2, p2

    if-nez v2, :cond_2

    .line 476
    invoke-virtual {v0, p4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->setDisplayName(Ljava/lang/String;)V

    goto :goto_0

    .line 471
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public updateContactNameInList(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 19
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 423
    .local p2, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 463
    :cond_0
    :goto_0
    return-void

    .line 427
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 429
    .local v2, "contentResolver":Landroid/content/ContentResolver;
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v6, v3, [Ljava/lang/String;

    .line 430
    .local v6, "contactIds":[Ljava/lang/String;
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v13, v3, :cond_4

    .line 437
    array-length v3, v6

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getSQLWhereInSubQuery(I)Ljava/lang/String;

    move-result-object v16

    .line 439
    .local v16, "inQuery":Ljava/lang/String;
    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 440
    const-string/jumbo v7, "_id"

    aput-object v7, v4, v3

    const/4 v3, 0x1

    const-string/jumbo v7, "display_name"

    aput-object v7, v4, v3

    .line 443
    .local v4, "projection":[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "_id IN ("

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v7, ") "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 445
    .local v5, "where":Ljava/lang/String;
    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    .line 446
    const/4 v7, 0x0

    .line 445
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 448
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v11, :cond_0

    .line 449
    const-string/jumbo v3, "CDH"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v17, "updateContactNameInList : cur len ="

    move-object/from16 v0, v17

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 451
    const-string/jumbo v3, "_id"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 452
    .local v8, "colId":I
    const-string/jumbo v3, "display_name"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 455
    .local v9, "colName":I
    :cond_2
    invoke-interface {v11, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 456
    .local v14, "id":J
    invoke-interface {v11, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 457
    .local v12, "displayName":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v14, v15, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->updateContactDisplayName(Ljava/util/ArrayList;JLjava/lang/String;)V

    .line 458
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 461
    .end local v8    # "colId":I
    .end local v9    # "colName":I
    .end local v12    # "displayName":Ljava/lang/String;
    .end local v14    # "id":J
    :cond_3
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 431
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v5    # "where":Ljava/lang/String;
    .end local v11    # "cursor":Landroid/database/Cursor;
    .end local v16    # "inQuery":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 432
    .local v10, "contact":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    if-eqz v10, :cond_5

    .line 433
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->getContactId()J

    move-result-wide v17

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v13

    .line 430
    :cond_5
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_1
.end method

.method public updatePhoneNumberInList(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 484
    .local p2, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 531
    :cond_0
    :goto_0
    return-void

    .line 488
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 490
    .local v2, "contentResolver":Landroid/content/ContentResolver;
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v6, v3, [Ljava/lang/String;

    .line 491
    .local v6, "contactIds":[Ljava/lang/String;
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_1
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move/from16 v0, v18

    if-lt v0, v3, :cond_4

    .line 498
    array-length v3, v6

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getSQLWhereInSubQuery(I)Ljava/lang/String;

    move-result-object v19

    .line 500
    .local v19, "inQuery":Ljava/lang/String;
    const/4 v3, 0x3

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 501
    const-string/jumbo v7, "contact_id"

    aput-object v7, v4, v3

    const/4 v3, 0x1

    .line 502
    const-string/jumbo v7, "data2"

    aput-object v7, v4, v3

    const/4 v3, 0x2

    .line 503
    const-string/jumbo v7, "data1"

    aput-object v7, v4, v3

    .line 506
    .local v4, "projection":[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "contact_id IN ("

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v7, ") "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 508
    .local v5, "where":Ljava/lang/String;
    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    .line 509
    const-string/jumbo v7, "contact_id"

    .line 508
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 511
    .local v17, "cursor":Landroid/database/Cursor;
    if-eqz v17, :cond_0

    .line 512
    const-string/jumbo v3, "CDH"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "updatePhoneNumberInList : cur len ="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 515
    const-string/jumbo v3, "contact_id"

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 516
    .local v13, "colId":I
    const-string/jumbo v3, "data2"

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 518
    .local v15, "colType":I
    const-string/jumbo v3, "data1"

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 521
    .local v14, "colPhoneNumber":I
    :cond_2
    move-object/from16 v0, v17

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 522
    .local v9, "id":J
    move-object/from16 v0, v17

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 523
    .local v11, "type":I
    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .local v12, "phoneNumber":Ljava/lang/String;
    move-object/from16 v7, p0

    move-object/from16 v8, p2

    .line 525
    invoke-virtual/range {v7 .. v12}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->addContactPhoneNumber(Ljava/util/ArrayList;JILjava/lang/String;)V

    .line 526
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 529
    .end local v9    # "id":J
    .end local v11    # "type":I
    .end local v12    # "phoneNumber":Ljava/lang/String;
    .end local v13    # "colId":I
    .end local v14    # "colPhoneNumber":I
    .end local v15    # "colType":I
    :cond_3
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 492
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v5    # "where":Ljava/lang/String;
    .end local v17    # "cursor":Landroid/database/Cursor;
    .end local v19    # "inQuery":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 493
    .local v16, "contact":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    if-eqz v16, :cond_5

    .line 494
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->getContactId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v18

    .line 491
    :cond_5
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_1
.end method
