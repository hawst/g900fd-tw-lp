.class Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$3;
.super Ljava/lang/Object;
.source "NotificationLocationRequestDialogView.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->initPlaceOnDisclaimnerDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;

    .line 271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickListener(Z)V
    .locals 1
    .param p1, "accept"    # Z

    .prologue
    .line 275
    if-eqz p1, :cond_0

    .line 276
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->acceptDisclaimer()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->access$10(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->disableTimer()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->access$3(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)V

    .line 278
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->dismiss()V

    .line 313
    :goto_0
    return-void

    .line 307
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 308
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 306
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 309
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->onNotifyLocationRequestIgnored()V

    .line 310
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->disableTimer()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->access$3(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)V

    .line 311
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->dismiss()V

    goto :goto_0
.end method
