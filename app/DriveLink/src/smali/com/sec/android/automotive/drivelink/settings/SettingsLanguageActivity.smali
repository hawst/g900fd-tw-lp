.class public Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "SettingsLanguageActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$CustomAdapter;
    }
.end annotation


# static fields
.field public static final LANGUAGE_CHANGED:Ljava/lang/String; = "com.vlingo.LANGUAGE_CHANGED"

.field public static isLaunchBySettings:Z

.field private static locales:[Ljava/lang/String;

.field private static final log:Lcom/vlingo/core/internal/logging/Logger;

.field private static loggedPhoneLocales:Z

.field private static unsupportedLocales:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private arrayAdapter:Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$CustomAdapter;

.field private mListView:Landroid/widget/ListView;

.field private settableLanguageDescriptions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private settableLanguages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    const-class v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 63
    sput-boolean v1, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->isLaunchBySettings:Z

    .line 65
    sput-boolean v1, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->loggedPhoneLocales:Z

    .line 67
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    .line 56
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->arrayAdapter:Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$CustomAdapter;

    .line 57
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->mListView:Landroid/widget/ListView;

    .line 59
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->settableLanguages:Ljava/util/ArrayList;

    .line 60
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->settableLanguageDescriptions:Ljava/util/ArrayList;

    .line 50
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->settableLanguageDescriptions:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->settableLanguages:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static convertToBargeinISOLanguage(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "vlingoLanguage"    # Ljava/lang/String;

    .prologue
    .line 425
    const-string/jumbo v0, "v-es-LA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 427
    const-string/jumbo p0, "es-US"

    .line 431
    .end local p0    # "vlingoLanguage":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 428
    .restart local p0    # "vlingoLanguage":Ljava/lang/String;
    :cond_1
    const-string/jumbo v0, "v-es-NA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 429
    const-string/jumbo p0, "es-US"

    goto :goto_0
.end method

.method public static initializeUnsupportedLocales()V
    .locals 3

    .prologue
    .line 284
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    .line 286
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_AU"

    const-string/jumbo v2, "en_GB"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_BE"

    const-string/jumbo v2, "en_US"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_CA"

    const-string/jumbo v2, "en_US"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_HK"

    const-string/jumbo v2, "en_US"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_IE"

    const-string/jumbo v2, "en_GB"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_IN"

    const-string/jumbo v2, "en_US"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_NZ"

    const-string/jumbo v2, "en_GB"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_PH"

    const-string/jumbo v2, "en_US"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_SG"

    const-string/jumbo v2, "en_US"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_ZA"

    const-string/jumbo v2, "en_GB"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "fr_BE"

    const-string/jumbo v2, "fr_FR"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "fr_CA"

    const-string/jumbo v2, "fr_FR"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "fr_LU"

    const-string/jumbo v2, "fr_FR"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "fr_CH"

    const-string/jumbo v2, "fr_FR"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "de_AT"

    const-string/jumbo v2, "de_DE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "de_CH"

    const-string/jumbo v2, "de_DE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "de_LU"

    const-string/jumbo v2, "de_DE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    return-void
.end method

.method public static phoneSupportsLanguage(Ljava/lang/String;)Z
    .locals 7
    .param p0, "vlingoLocale"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 235
    sget-boolean v4, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->loggedPhoneLocales:Z

    if-nez v4, :cond_0

    .line 239
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->locales:[Ljava/lang/String;

    array-length v4, v4

    if-lt v1, v4, :cond_2

    .line 243
    .end local v1    # "i":I
    :cond_0
    sput-boolean v3, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->loggedPhoneLocales:Z

    .line 245
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->initializeUnsupportedLocales()V

    .line 247
    const-string/jumbo v4, "v-"

    invoke-virtual {p0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 248
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->phoneSupportsVlingoSpecificLanguage(Ljava/lang/String;)Z

    move-result v3

    .line 267
    :cond_1
    :goto_1
    return v3

    .line 240
    .restart local v1    # "i":I
    :cond_2
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "phone locale \'"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->locales:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 239
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 251
    .end local v1    # "i":I
    :cond_3
    invoke-static {p0}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 253
    .local v2, "isoLocale":Ljava/lang/String;
    const/16 v4, 0x2d

    const/16 v5, 0x5f

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    .line 255
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->locales:[Ljava/lang/String;

    array-length v4, v4

    if-lt v1, v4, :cond_4

    .line 267
    const/4 v3, 0x0

    goto :goto_1

    .line 256
    :cond_4
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->locales:[Ljava/lang/String;

    aget-object v0, v4, v1

    .line 257
    .local v0, "foundLocale":Ljava/lang/String;
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 258
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "foundLocale":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 262
    .restart local v0    # "foundLocale":Ljava/lang/String;
    :cond_5
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 255
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public static phoneSupportsVlingoSpecificLanguage(Ljava/lang/String;)Z
    .locals 7
    .param p0, "vlingoLocale"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x0

    .line 271
    const/4 v4, 0x4

    invoke-virtual {p0, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 272
    .local v2, "language":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->locales:[Ljava/lang/String;

    array-length v4, v4

    if-lt v1, v4, :cond_0

    .line 280
    :goto_1
    return v3

    .line 273
    :cond_0
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->locales:[Ljava/lang/String;

    aget-object v0, v4, v1

    .line 274
    .local v0, "foundLocale":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_1

    .line 275
    invoke-virtual {v0, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 276
    const/4 v3, 0x1

    goto :goto_1

    .line 272
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private setMultiWindowTrayHide()V
    .locals 2

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 194
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 196
    return-void
.end method


# virtual methods
.method initPreference()V
    .locals 8

    .prologue
    .line 200
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSupportedLanguages()[Ljava/lang/CharSequence;

    move-result-object v4

    .line 202
    .local v4, "supportedLanguages":[Ljava/lang/CharSequence;
    const-string/jumbo v5, "SettingsLanguageActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "language cnt = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v7, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSupportedLanguageDescriptions()[Ljava/lang/CharSequence;

    move-result-object v3

    .line 206
    .local v3, "supportedLanguageDescriptions":[Ljava/lang/CharSequence;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->settableLanguages:Ljava/util/ArrayList;

    .line 207
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->settableLanguageDescriptions:Ljava/util/ArrayList;

    .line 209
    const/4 v2, 0x1

    .line 211
    .local v2, "showAllLanguages":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v5, v4

    if-lt v0, v5, :cond_0

    .line 232
    return-void

    .line 212
    :cond_0
    aget-object v5, v4, v0

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 216
    .local v1, "language":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->phoneSupportsLanguage(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 217
    if-eqz v2, :cond_2

    .line 218
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->settableLanguages:Ljava/util/ArrayList;

    aget-object v6, v4, v0

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->settableLanguageDescriptions:Ljava/util/ArrayList;

    .line 220
    aget-object v6, v3, v0

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 211
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 226
    :cond_2
    sget-object v5, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "not adding \'"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v7, v4, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\', desc \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 228
    aget-object v7, v3, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 226
    invoke-virtual {v5, v6}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 82
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 83
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 21
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    .line 86
    invoke-super/range {p0 .. p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 88
    const/16 v18, 0x0

    sput-boolean v18, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->isLaunchBySettings:Z

    .line 89
    const/16 v18, 0x0

    sput-object v18, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->unsupportedLocales:Ljava/util/HashMap;

    .line 90
    const/16 v18, 0x0

    sput-boolean v18, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->loggedPhoneLocales:Z

    .line 92
    const/16 v18, 0x0

    sput-object v18, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->locales:[Ljava/lang/String;

    .line 96
    sget-object v18, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v19, "qs lan set\'"

    invoke-virtual/range {v18 .. v19}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 97
    const v18, 0x7f03002d

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->setContentView(I)V

    .line 100
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, Lcom/android/internal/app/LocalePicker;->getAllAssetLocales(Landroid/content/Context;Z)Ljava/util/List;

    move-result-object v11

    .line 102
    .local v11, "localeInfos":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/app/LocalePicker$LocaleInfo;>;"
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v7

    .line 104
    .local v7, "finalSize":I
    new-array v0, v7, [Ljava/lang/String;

    move-object/from16 v18, v0

    sput-object v18, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->locales:[Ljava/lang/String;

    .line 106
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-lt v8, v7, :cond_1

    .line 117
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->initPreference()V

    .line 119
    const-string/jumbo v18, "SettingsLanguageActivity"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "locale.length = "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v20, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->locales:[Ljava/lang/String;

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    new-instance v18, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$CustomAdapter;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, p0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$CustomAdapter;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;Landroid/app/Activity;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->arrayAdapter:Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$CustomAdapter;

    .line 122
    const v18, 0x7f09012c

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/ListView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->mListView:Landroid/widget/ListView;

    .line 123
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->mListView:Landroid/widget/ListView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->arrayAdapter:Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$CustomAdapter;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->mListView:Landroid/widget/ListView;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 126
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->mListView:Landroid/widget/ListView;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 128
    const-string/jumbo v18, "language"

    const-string/jumbo v19, "en-US"

    invoke-static/range {v18 .. v19}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 129
    .local v10, "language":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->mListView:Landroid/widget/ListView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->settableLanguages:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v19

    const/16 v20, 0x1

    invoke-virtual/range {v18 .. v20}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->mListView:Landroid/widget/ListView;

    move-object/from16 v18, v0

    new-instance v19, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$1;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;)V

    invoke-virtual/range {v18 .. v19}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 141
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v15

    .line 142
    .local v15, "titlebar":Landroid/app/ActionBar;
    if-eqz v15, :cond_0

    .line 143
    const-string/jumbo v18, "Change language"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 146
    :cond_0
    const v18, 0x7f09012b

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 147
    .local v3, "btnNavBack":Landroid/widget/LinearLayout;
    new-instance v18, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$2;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;)V

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 156
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->getWindow()Landroid/view/Window;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v12

    .line 157
    .local v12, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v18

    const-string/jumbo v19, "privateFlags"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v13

    .line 158
    .local v13, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v18

    const-string/jumbo v19, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v14

    .line 159
    .local v14, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v18

    const-string/jumbo v19, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    .line 161
    .local v5, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v4, 0x0

    .local v4, "currentPrivateFlags":I
    const/16 v17, 0x0

    .local v17, "valueofFlagsEnableStatusBar":I
    const/16 v16, 0x0

    .line 162
    .local v16, "valueofFlagsDisableTray":I
    invoke-virtual {v13, v12}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v4

    .line 163
    invoke-virtual {v14, v12}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v17

    .line 164
    invoke-virtual {v5, v12}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v16

    .line 166
    or-int v4, v4, v17

    .line 167
    or-int v4, v4, v16

    .line 169
    invoke-virtual {v13, v12, v4}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 170
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->getWindow()Landroid/view/Window;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 179
    .end local v4    # "currentPrivateFlags":I
    .end local v5    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v12    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v13    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v14    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v16    # "valueofFlagsDisableTray":I
    .end local v17    # "valueofFlagsEnableStatusBar":I
    :goto_1
    return-void

    .line 107
    .end local v3    # "btnNavBack":Landroid/widget/LinearLayout;
    .end local v10    # "language":Ljava/lang/String;
    .end local v15    # "titlebar":Landroid/app/ActionBar;
    :cond_1
    invoke-interface {v11, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    .line 108
    .local v9, "info":Lcom/android/internal/app/LocalePicker$LocaleInfo;
    sget-object v18, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->locales:[Ljava/lang/String;

    invoke-virtual {v9}, Lcom/android/internal/app/LocalePicker$LocaleInfo;->getLocale()Ljava/util/Locale;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v18, v8

    .line 106
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 171
    .end local v9    # "info":Lcom/android/internal/app/LocalePicker$LocaleInfo;
    .restart local v3    # "btnNavBack":Landroid/widget/LinearLayout;
    .restart local v10    # "language":Ljava/lang/String;
    .restart local v15    # "titlebar":Landroid/app/ActionBar;
    :catch_0
    move-exception v6

    .line 173
    .local v6, "e":Ljava/lang/NoSuchFieldException;
    sget-object v18, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "NoSuchFieldException "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/NoSuchFieldException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, ", continuing"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1

    .line 174
    .end local v6    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v6

    .line 176
    .local v6, "e":Ljava/lang/Exception;
    sget-object v18, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "Exception "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, ", continuing"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 177
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public onLanguageChanged()V
    .locals 5

    .prologue
    .line 354
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->isRunning()Z

    move-result v3

    if-nez v3, :cond_0

    .line 360
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$3;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 365
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 367
    :cond_0
    const-string/jumbo v3, "language"

    .line 368
    const-string/jumbo v4, "en-US"

    .line 367
    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 373
    .local v2, "language":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 374
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/vlingo/core/internal/settings/Settings;->setLanguageApplication(Ljava/lang/String;Landroid/content/Context;Landroid/content/SharedPreferences$Editor;)V

    .line 375
    invoke-static {v0}, Lcom/vlingo/core/internal/settings/Settings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    .line 385
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getLmttClientEventHandler()Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;->onLanguageChanged()V

    .line 387
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->convertToBargeinISOLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 388
    .local v1, "langToChange":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 389
    const-string/jumbo v4, "voicetalk_language"

    .line 388
    invoke-static {v3, v4, v1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 390
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v4, "com.vlingo.LANGUAGE_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 392
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->finishDialog()V

    .line 393
    const-string/jumbo v3, "JINSEIL"

    const-string/jumbo v4, "JINSEIL finish!"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->finish()V

    .line 395
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 183
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 187
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 185
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->finish()V

    goto :goto_0

    .line 183
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 310
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onPause()V

    .line 312
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 316
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onResume()V

    .line 326
    const-string/jumbo v1, "language"

    const-string/jumbo v2, "en-US"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 327
    .local v0, "language":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->settableLanguages:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 328
    return-void
.end method
