.class public Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;
.super Ljava/lang/Object;
.source "Participants.java"


# instance fields
.field private mPhoneNumber:Ljava/lang/String;

.field private mUpdateUrl:Ljava/lang/String;

.field private mUserId:I

.field private mUserName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mPhoneNumber:Ljava/lang/String;

    .line 5
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mUpdateUrl:Ljava/lang/String;

    .line 6
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mUserId:I

    .line 7
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mUserName:Ljava/lang/String;

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .param p2, "updateUrl"    # Ljava/lang/String;
    .param p3, "userId"    # I
    .param p4, "userName"    # Ljava/lang/String;

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mPhoneNumber:Ljava/lang/String;

    .line 5
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mUpdateUrl:Ljava/lang/String;

    .line 6
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mUserId:I

    .line 7
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mUserName:Ljava/lang/String;

    .line 11
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mPhoneNumber:Ljava/lang/String;

    .line 12
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mUpdateUrl:Ljava/lang/String;

    .line 13
    iput p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mUserId:I

    .line 14
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mUserName:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method public getmPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getmUpdateUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mUpdateUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getmUserId()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mUserId:I

    return v0
.end method

.method public getmUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mUserName:Ljava/lang/String;

    return-object v0
.end method

.method public setmPhoneNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPhoneNumber"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mPhoneNumber:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public setmUpdateUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "mUpdateUrl"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mUpdateUrl:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public setmUserId(I)V
    .locals 0
    .param p1, "mUserId"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mUserId:I

    .line 42
    return-void
.end method

.method public setmUserName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mUserName"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;->mUserName:Ljava/lang/String;

    .line 50
    return-void
.end method
