.class Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$3;
.super Ljava/lang/Object;
.source "VoiceMusicPlayerActionBarLayout.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->startScaleAnimation(FJLandroid/view/animation/Interpolator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    .line 451
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 455
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 459
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->isRestore:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->access$9(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 460
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->access$10(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;Z)V

    .line 461
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    const/high16 v1, 0x3f800000    # 1.0f

    const-wide/16 v2, 0x64

    # getter for: Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->access$11()Landroid/view/animation/Interpolator;

    move-result-object v4

    # invokes: Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->startScaleAnimation(FJLandroid/view/animation/Interpolator;)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->access$12(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;FJLandroid/view/animation/Interpolator;)V

    .line 465
    :goto_0
    return-void

    .line 463
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->access$10(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;Z)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 470
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 475
    return-void
.end method
