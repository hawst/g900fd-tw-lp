.class public Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DLBroadcastReceiver.java"


# static fields
.field public static final ALERT_ALARM_ID:Ljava/lang/String; = "alertAlarmID"

.field public static final ALERT_EVENT_ID:Ljava/lang/String; = "eventid"

.field public static final INTERVAL_TIME:I = 0x3e8

.field public static final LOW_BATTERY:I = 0x1


# instance fields
.field private mAlarmDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/AlarmDataHandler;

.field private mContext:Landroid/content/Context;

.field private mFilterScheduleId:J

.field private mLastReceiveTime:J

.field private mNotificationHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 35
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 28
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mNotificationHandler:Landroid/os/Handler;

    .line 29
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/AlarmDataHandler;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/AlarmDataHandler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mAlarmDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/AlarmDataHandler;

    .line 30
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mContext:Landroid/content/Context;

    .line 31
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mFilterScheduleId:J

    .line 32
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mLastReceiveTime:J

    .line 36
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mNotificationHandler:Landroid/os/Handler;

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 2
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 28
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mNotificationHandler:Landroid/os/Handler;

    .line 29
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/AlarmDataHandler;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/AlarmDataHandler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mAlarmDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/AlarmDataHandler;

    .line 30
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mContext:Landroid/content/Context;

    .line 31
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mFilterScheduleId:J

    .line 32
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mLastReceiveTime:J

    .line 42
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mNotificationHandler:Landroid/os/Handler;

    .line 43
    return-void
.end method

.method private processAlarm(Landroid/content/Intent;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 223
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mContext:Landroid/content/Context;

    if-nez v5, :cond_0

    .line 224
    const-string/jumbo v5, "MY_TAG"

    const-string/jumbo v6, "[YANG] processAlarm mContext == null"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    :goto_0
    return-void

    .line 227
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string/jumbo v6, "alertAlarmID"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 228
    .local v2, "id":I
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 229
    .local v1, "cal":Ljava/util/Calendar;
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 230
    .local v3, "time":J
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mAlarmDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/AlarmDataHandler;

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/AlarmDataHandler;->getAlarmInfo(Landroid/content/Context;I)Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;

    move-result-object v0

    .line 231
    .local v0, "alarmInfo":Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;
    invoke-virtual {v0, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->setAlertTime(J)V

    .line 233
    const/4 v5, 0x7

    const/4 v6, 0x0

    invoke-direct {p0, v5, v6, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    .line 234
    const-string/jumbo v5, "MY_TAG"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "[YANG] BaadddroadcastReceiver processAlarm :: id "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 235
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " time : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->getAlertTime()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " snz :: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 236
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->getSNZActive()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 234
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private processBatteryState(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 206
    const-string/jumbo v4, "status"

    .line 207
    const/4 v5, 0x1

    .line 206
    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 208
    .local v0, "battStatus":I
    const-string/jumbo v4, "scale"

    const/16 v5, 0x64

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 209
    .local v3, "scale":I
    const-string/jumbo v4, "level"

    invoke-virtual {p1, v4, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 211
    .local v2, "level":I
    mul-int/lit8 v4, v2, 0x64

    int-to-float v4, v4

    int-to-float v5, v3

    div-float v1, v4, v5

    .line 213
    .local v1, "batteryPct":F
    const/high16 v4, 0x3f800000    # 1.0f

    cmpg-float v4, v1, v4

    if-gtz v4, :cond_0

    .line 214
    const/4 v4, 0x2

    if-eq v0, v4, :cond_0

    .line 215
    const/4 v4, 0x6

    float-to-int v5, v1

    .line 216
    const/4 v6, 0x0

    .line 215
    invoke-direct {p0, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    .line 217
    const-string/jumbo v4, "MY_TAG"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "[YANG]call Intent.ACTION_BATTERY_LOW : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 218
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "level : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " scale : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 217
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    :cond_0
    return-void
.end method

.method private processCallState(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 168
    const/4 v1, 0x0

    .line 169
    .local v1, "inNumber":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 170
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v3, "state"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 171
    .local v2, "newState":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 194
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    const-string/jumbo v3, "incoming_number"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 176
    const-string/jumbo v3, "MY_TAG"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "[YANG] onReceive!!!!!!!!!ACTION_PHONE_STATE :: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 177
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 176
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    sget-object v3, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 179
    const-string/jumbo v3, "MY_TAG"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "[YANG] onReceive!!!!!!!!!EXTRA_STATE_IDLE :: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 180
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 179
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;

    .line 182
    invoke-direct {v3, v7, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    invoke-direct {p0, v6, v6, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto :goto_0

    .line 183
    :cond_2
    sget-object v3, Landroid/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 184
    const-string/jumbo v3, "MY_TAG"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "[YANG] onReceive!!!!!!!!!EXTRA_STATE_RINGING:: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 185
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 184
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    const/4 v3, 0x1

    .line 187
    new-instance v4, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;

    invoke-direct {v4, v7, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-direct {p0, v3, v6, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto :goto_0

    .line 188
    :cond_3
    sget-object v3, Landroid/telephony/TelephonyManager;->EXTRA_STATE_OFFHOOK:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 189
    const-string/jumbo v3, "MY_TAG"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "[YANG] onReceive!!!!!!!!!EXTRA_STATE_OFFHOOK:: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 190
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 189
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    const/4 v3, 0x2

    .line 192
    new-instance v4, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;

    invoke-direct {v4, v7, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-direct {p0, v3, v6, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private processOutgoingCall(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 197
    const/4 v0, 0x0

    .line 198
    .local v0, "inNumber":Ljava/lang/String;
    const-string/jumbo v1, "MY_TAG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[YANG] onReceive!!!!!!!!!ACTION_OUTGOING_CALL "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 199
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 198
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    const/16 v1, 0x8

    const/4 v2, 0x0

    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;

    .line 201
    const/4 v4, 0x0

    invoke-direct {v3, v4, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    .line 202
    return-void
.end method

.method private processSMS(Landroid/content/Intent;)V
    .locals 14
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x0

    .line 125
    const/4 v3, 0x0

    .line 126
    .local v3, "inNumber":Ljava/lang/String;
    const/4 v4, 0x0

    .line 127
    .local v4, "message":Ljava/lang/String;
    const-wide/16 v8, 0x0

    .line 129
    .local v8, "time":J
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 130
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v10, "pdus"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/Object;

    .line 131
    .local v6, "pdusObj":[Ljava/lang/Object;
    array-length v10, v6

    new-array v5, v10, [Landroid/telephony/SmsMessage;

    .line 132
    .local v5, "messages":[Landroid/telephony/SmsMessage;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v10, v6

    if-lt v2, v10, :cond_1

    .line 136
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 137
    .local v7, "sb":Ljava/lang/StringBuilder;
    array-length v12, v5

    move v10, v11

    :goto_1
    if-lt v10, v12, :cond_2

    .line 142
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 144
    if-eqz v4, :cond_3

    .line 146
    const-string/jumbo v10, "!Let me know your location."

    invoke-virtual {v4, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 148
    const-string/jumbo v10, "!I\'m here now. See me here!"

    invoke-virtual {v4, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 150
    const-string/jumbo v10, "!Join group location sharing."

    invoke-virtual {v4, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 152
    const-string/jumbo v10, "!Let\'s meet here."

    invoke-virtual {v4, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 153
    const-string/jumbo v10, "!Stop group location sharing."

    invoke-virtual {v4, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 154
    :cond_0
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->getUrlFromLocationMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_3

    .line 155
    const-string/jumbo v10, "MY_TAG"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "[YANG] onReceive!!!!!!!!!processSMS Location :: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 156
    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 155
    invoke-static {v10, v12}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    const/16 v10, 0xa

    .line 158
    new-instance v12, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    invoke-direct {v12, v3, v4, v8, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 157
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    .line 165
    :goto_2
    return-void

    .line 133
    .end local v7    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    aget-object v10, v6, v2

    check-cast v10, [B

    invoke-static {v10}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    move-result-object v10

    aput-object v10, v5, v2

    .line 132
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 137
    .restart local v7    # "sb":Ljava/lang/StringBuilder;
    :cond_2
    aget-object v1, v5, v10

    .line 138
    .local v1, "currentMessage":Landroid/telephony/SmsMessage;
    invoke-virtual {v1}, Landroid/telephony/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    move-result-object v3

    .line 139
    invoke-virtual {v1}, Landroid/telephony/SmsMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    invoke-virtual {v1}, Landroid/telephony/SmsMessage;->getTimestampMillis()J

    move-result-wide v8

    .line 137
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 160
    .end local v1    # "currentMessage":Landroid/telephony/SmsMessage;
    :cond_3
    const-string/jumbo v10, "MY_TAG"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "[YANG] onReceive!!!!!!!!!processSMS justSMS :: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 161
    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 160
    invoke-static {v10, v12}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    const/4 v10, 0x3

    new-instance v12, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    .line 163
    invoke-direct {v12, v3, v4, v8, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 162
    invoke-direct {p0, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto :goto_2
.end method

.method private processSMSWithMSGID(Landroid/content/Intent;)V
    .locals 14
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v13, 0x0

    .line 80
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    .line 81
    .local v6, "bundle":Landroid/os/Bundle;
    const-string/jumbo v3, "msg_type"

    invoke-virtual {v6, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 82
    .local v10, "msg_type":Ljava/lang/String;
    const-string/jumbo v3, "msgid"

    invoke-virtual {v6, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    .line 83
    .local v7, "msgId":J
    const-string/jumbo v3, "msg_body"

    invoke-virtual {v6, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 84
    .local v2, "message":Ljava/lang/String;
    const-string/jumbo v3, "msg_address"

    invoke-virtual {v6, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 85
    .local v1, "inNumber":Ljava/lang/String;
    const-string/jumbo v3, "date"

    invoke-virtual {v6, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 87
    .local v4, "time":J
    new-instance v9, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    invoke-direct {v9, v1, v2, v4, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 88
    .local v9, "msgInfo":Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;

    .line 89
    long-to-int v3, v7

    .line 88
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;-><init>(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 90
    .local v0, "messageImp":Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;
    invoke-virtual {v9, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->setDLMessage(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)V

    .line 92
    const-string/jumbo v3, "sms"

    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 93
    if-eqz v2, :cond_2

    .line 95
    const-string/jumbo v3, "!Let me know your location."

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 97
    const-string/jumbo v3, "!I\'m here now. See me here!"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 99
    const-string/jumbo v3, "!Join group location sharing."

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 101
    const-string/jumbo v3, "!Let\'s meet here."

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 102
    const-string/jumbo v3, "!Stop group location sharing."

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 103
    :cond_0
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->getUrlFromLocationMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 104
    const-string/jumbo v3, "MY_TAG"

    .line 105
    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "[YANG] onReceive!!!!!processSMSWithMSGID Location :: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 106
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 105
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 104
    invoke-static {v3, v11}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    const/16 v3, 0xa

    invoke-direct {p0, v3, v13, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    .line 122
    :cond_1
    :goto_0
    return-void

    .line 109
    :cond_2
    const-string/jumbo v3, "MY_TAG"

    .line 110
    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "[YANG] onReceive!!!processSMSWithMSGID justSMS :: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 111
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 110
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 109
    invoke-static {v3, v11}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    const/4 v3, 0x3

    invoke-direct {p0, v3, v13, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto :goto_0

    .line 115
    :cond_3
    const-string/jumbo v3, "mms"

    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 116
    const-string/jumbo v3, "MY_TAG"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "[YANG] onReceive!!!processSMSWithMSGID MMS :: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 117
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 116
    invoke-static {v3, v11}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    const/4 v3, 0x4

    invoke-direct {p0, v3, v13, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto :goto_0
.end method

.method private processScheduleAlarm(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 240
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string/jumbo v5, "eventid"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 241
    .local v2, "id":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 243
    .local v0, "curTime":J
    const-string/jumbo v4, "MY_TAG"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "[YANG] aa id :"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ", mFilterScheduleId : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 244
    iget-wide v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mFilterScheduleId:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "time gap : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 245
    iget-wide v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mLastReceiveTime:J

    sub-long v6, v0, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 243
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    iget-wide v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mFilterScheduleId:J

    cmp-long v4, v2, v4

    if-nez v4, :cond_0

    .line 247
    iget-wide v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mLastReceiveTime:J

    sub-long v4, v0, v4

    const-wide/16 v6, 0x3e8

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    .line 248
    const-string/jumbo v4, "MY_TAG"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "[YANG] ACTION_SCHEDULE_ALERT() same ID curtime : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 249
    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " , lasttime: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mLastReceiveTime:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 248
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mLastReceiveTime:J

    .line 264
    :goto_0
    return-void

    .line 253
    :cond_0
    iput-wide v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mFilterScheduleId:J

    .line 254
    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mLastReceiveTime:J

    .line 255
    const-string/jumbo v4, "MY_TAG"

    .line 256
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "[YANG] ACTION_SCHEDULE_ALERT() :: mFilterScheduleId "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 257
    iget-wide v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mFilterScheduleId:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " time : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mLastReceiveTime:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 256
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 255
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    const/16 v4, 0x9

    long-to-int v5, v2

    const/4 v6, 0x0

    invoke-direct {p0, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    .line 261
    const-string/jumbo v4, "MY_TAG"

    .line 262
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "[YANG] BaadddroadcastReceiver ACTION_SCHEDULE_ALERT() :: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 263
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 262
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 261
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private sendMessage(IILjava/lang/Object;)V
    .locals 2
    .param p1, "notiType"    # I
    .param p2, "arg1"    # I
    .param p3, "data"    # Ljava/lang/Object;

    .prologue
    .line 267
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mNotificationHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 268
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 269
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 270
    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 272
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mNotificationHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 273
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 47
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mNotificationHandler:Landroid/os/Handler;

    if-nez v1, :cond_1

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 53
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v1, "MY_TAG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[YANG] onReceive() :: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    const-string/jumbo v1, "com.android.mms.RECEIVED_MSG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 56
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->processSMSWithMSGID(Landroid/content/Intent;)V

    goto :goto_0

    .line 57
    :cond_2
    const-string/jumbo v1, "android.provider.Telephony.SMS_RECEIVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 58
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->processSMS(Landroid/content/Intent;)V

    goto :goto_0

    .line 59
    :cond_3
    const-string/jumbo v1, "android.intent.action.PHONE_STATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 60
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->processCallState(Landroid/content/Intent;)V

    goto :goto_0

    .line 61
    :cond_4
    const-string/jumbo v1, "android.intent.action.NEW_OUTGOING_CALL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 62
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->processOutgoingCall(Landroid/content/Intent;)V

    goto :goto_0

    .line 63
    :cond_5
    const-string/jumbo v1, "android.intent.action.BATTERY_LOW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 64
    const-string/jumbo v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 65
    :cond_6
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->processBatteryState(Landroid/content/Intent;)V

    goto :goto_0

    .line 66
    :cond_7
    const-string/jumbo v1, "com.samsung.sec.android.clockpackage.alarm.ALARM_STARTED_IN_DRIVELINK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 67
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->processAlarm(Landroid/content/Intent;)V

    goto :goto_0

    .line 68
    :cond_8
    const-string/jumbo v1, "com.android.calendar.SEND_ALERTINFO_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 69
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->processScheduleAlarm(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 70
    :cond_9
    const-string/jumbo v1, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    const/16 v1, 0xd

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLBroadcastReceiver;->mContext:Landroid/content/Context;

    .line 77
    return-void
.end method
