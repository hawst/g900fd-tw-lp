.class public Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "WhatCanISayActivity.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnGroupExpandListener;


# static fields
.field private static final KEY_PREV_GROUP_WHAT_CAN_I_SAY:Ljava/lang/String; = "KEY_PREV_GROUP_WHAT_CAN_I_SAY"


# instance fields
.field private listAdapter:Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;

.field private lstWhatCanISay:Landroid/widget/ExpandableListView;

.field private prevGrouplstWhatCanISay:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->prevGrouplstWhatCanISay:I

    .line 32
    return-void
.end method

.method private addListAdapterGroup(IIII)V
    .locals 7
    .param p1, "imgResourceId"    # I
    .param p2, "titleStringId"    # I
    .param p3, "subtitleStringId"    # I
    .param p4, "childrenStringArrayId"    # I

    .prologue
    .line 141
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 142
    .local v1, "children":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 146
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->listAdapter:Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;

    invoke-virtual {p0, p2}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 147
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p0, p3}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 146
    invoke-virtual {v2, p1, v3, v4, v1}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;->addGroup(ILjava/lang/String;Ljava/lang/String;Ljava/util/LinkedList;)V

    .line 148
    return-void

    .line 142
    :cond_0
    aget-object v0, v3, v2

    .line 144
    .local v0, "child":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 142
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private initListAdapter()V
    .locals 8

    .prologue
    const v7, 0x7f0a03ca

    const v6, 0x7f0a03c9

    const v5, 0x7f02028a

    .line 95
    new-instance v1, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->listAdapter:Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;

    .line 96
    const v1, 0x7f02028c

    .line 97
    const v2, 0x7f0a03c7

    .line 98
    const v3, 0x7f0a03c8

    .line 99
    const v4, 0x7f07000d

    .line 96
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->addListAdapterGroup(IIII)V

    .line 100
    const-string/jumbo v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "salesCode":Ljava/lang/String;
    const-string/jumbo v1, "KDI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    const v1, 0x7f07001b

    .line 102
    invoke-direct {p0, v5, v6, v7, v1}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->addListAdapterGroup(IIII)V

    .line 113
    :goto_0
    const v1, 0x7f02028d

    .line 114
    const v2, 0x7f0a03cb

    .line 115
    const v3, 0x7f0a03cc

    .line 116
    const v4, 0x7f07000f

    .line 113
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->addListAdapterGroup(IIII)V

    .line 117
    const v1, 0x7f02028b

    .line 118
    const v2, 0x7f0a03cd

    .line 119
    const v3, 0x7f0a03ce

    .line 120
    const v4, 0x7f070010

    .line 117
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->addListAdapterGroup(IIII)V

    .line 124
    return-void

    .line 110
    :cond_0
    const v1, 0x7f07000e

    .line 107
    invoke-direct {p0, v5, v6, v7, v1}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->addListAdapterGroup(IIII)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v3, 0x7f0a025c

    .line 48
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const v1, 0x7f03003e

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->setContentView(I)V

    .line 53
    const v1, 0x7f090197

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ExpandableListView;

    .line 52
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->lstWhatCanISay:Landroid/widget/ExpandableListView;

    .line 55
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->initListAdapter()V

    .line 59
    const v1, 0x7f090196

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 58
    check-cast v0, Landroid/widget/LinearLayout;

    .line 60
    .local v0, "ibNavBack":Landroid/widget/LinearLayout;
    new-instance v1, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    new-instance v1, Ljava/lang/StringBuilder;

    .line 69
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 70
    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 71
    const v2, 0x7f0a03f4

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 68
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 73
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->setTitle(I)V

    .line 75
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->lstWhatCanISay:Landroid/widget/ExpandableListView;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->listAdapter:Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 76
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->lstWhatCanISay:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, p0}, Landroid/widget/ExpandableListView;->setOnGroupExpandListener(Landroid/widget/ExpandableListView$OnGroupExpandListener;)V

    .line 78
    if-eqz p1, :cond_0

    .line 80
    const-string/jumbo v1, "KEY_PREV_GROUP_WHAT_CAN_I_SAY"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    const-string/jumbo v1, "KEY_PREV_GROUP_WHAT_CAN_I_SAY"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 81
    iput v1, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->prevGrouplstWhatCanISay:I

    .line 84
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 159
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onDestroy()V

    .line 160
    return-void
.end method

.method public onGroupExpand(I)V
    .locals 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 193
    iget v0, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->prevGrouplstWhatCanISay:I

    if-eq p1, v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->lstWhatCanISay:Landroid/widget/ExpandableListView;

    iget v1, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->prevGrouplstWhatCanISay:I

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 196
    :cond_0
    iput p1, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->prevGrouplstWhatCanISay:I

    .line 197
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 169
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onPause()V

    .line 170
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 179
    const-string/jumbo v0, "DM_WHAT_CAN_I_SAY"

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->setActivityFlowID(Ljava/lang/String;)V

    .line 180
    const-string/jumbo v0, "DM_WHAT_CAN_I_SAY"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 181
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onResume()V

    .line 182
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 208
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 209
    const-string/jumbo v0, "KEY_PREV_GROUP_WHAT_CAN_I_SAY"

    .line 210
    iget v1, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;->prevGrouplstWhatCanISay:I

    .line 209
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 211
    return-void
.end method
