.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationLocationShareDialogView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I = null

.field private static final TAG:Ljava/lang/String; = "NotificationLocationShareDialogView"

.field protected static mContext:Landroid/content/Context;


# instance fields
.field private mCancelButton:Landroid/widget/Button;

.field private mImageView:Landroid/widget/ImageView;

.field private mName:Ljava/lang/String;

.field private mNameView:Landroid/widget/TextView;

.field private mNumber:Ljava/lang/String;

.field private mRouteButton:Landroid/widget/Button;

.field private mUrl:Ljava/lang/String;

.field private mViewLocal:Landroid/view/View;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mContext:Landroid/content/Context;

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v1, 0x0

    .line 83
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 31
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mNameView:Landroid/widget/TextView;

    .line 32
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mImageView:Landroid/widget/ImageView;

    .line 33
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mViewLocal:Landroid/view/View;

    .line 36
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mName:Ljava/lang/String;

    .line 37
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mNumber:Ljava/lang/String;

    .line 38
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mUrl:Ljava/lang/String;

    .line 85
    invoke-virtual {p4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;

    .line 87
    .local v0, "locInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;
    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mName:Ljava/lang/String;

    .line 89
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getNumber()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mNumber:Ljava/lang/String;

    .line 90
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getUrl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mUrl:Ljava/lang/String;

    .line 97
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 98
    return-void

    .line 92
    :cond_0
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mName:Ljava/lang/String;

    .line 93
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mNumber:Ljava/lang/String;

    .line 94
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 31
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mNameView:Landroid/widget/TextView;

    .line 32
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mImageView:Landroid/widget/ImageView;

    .line 33
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mViewLocal:Landroid/view/View;

    .line 36
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mName:Ljava/lang/String;

    .line 37
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mNumber:Ljava/lang/String;

    .line 38
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mUrl:Ljava/lang/String;

    .line 65
    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;

    .line 67
    .local v0, "locInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;
    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mName:Ljava/lang/String;

    .line 69
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getNumber()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mNumber:Ljava/lang/String;

    .line 70
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getUrl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mUrl:Ljava/lang/String;

    .line 77
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 78
    return-void

    .line 72
    :cond_0
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mName:Ljava/lang/String;

    .line 73
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mNumber:Ljava/lang/String;

    .line 74
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 31
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mNameView:Landroid/widget/TextView;

    .line 32
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mImageView:Landroid/widget/ImageView;

    .line 33
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mViewLocal:Landroid/view/View;

    .line 36
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mName:Ljava/lang/String;

    .line 37
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mNumber:Ljava/lang/String;

    .line 38
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mUrl:Ljava/lang/String;

    .line 46
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;

    .line 48
    .local v0, "locInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;
    if-eqz v0, :cond_0

    .line 49
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mName:Ljava/lang/String;

    .line 50
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getNumber()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mNumber:Ljava/lang/String;

    .line 51
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getUrl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mUrl:Ljava/lang/String;

    .line 58
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 59
    return-void

    .line 53
    :cond_0
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mName:Ljava/lang/String;

    .line 54
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mNumber:Ljava/lang/String;

    .line 55
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mNumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method private disclaimer()V
    .locals 0

    .prologue
    .line 226
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->initPlaceOnDisclaimnerDialog()V

    .line 227
    return-void
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x0

    .line 101
    if-nez p1, :cond_0

    .line 154
    :goto_0
    return-void

    .line 104
    :cond_0
    sput-object p1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mContext:Landroid/content/Context;

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 107
    const-string/jumbo v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 106
    check-cast v1, Landroid/view/LayoutInflater;

    .line 109
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mViewLocal:Landroid/view/View;

    if-nez v6, :cond_2

    .line 112
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 113
    const v7, 0x7f0a0608

    .line 112
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 115
    .local v3, "msg":Ljava/lang/String;
    const v6, 0x7f0300b3

    .line 114
    invoke-virtual {v1, v6, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mViewLocal:Landroid/view/View;

    .line 117
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mViewLocal:Landroid/view/View;

    const v7, 0x7f0902de

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mNameView:Landroid/widget/TextView;

    .line 118
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mViewLocal:Landroid/view/View;

    .line 119
    const v7, 0x7f0902cd

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 118
    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mImageView:Landroid/widget/ImageView;

    .line 120
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mViewLocal:Landroid/view/View;

    .line 121
    const v7, 0x7f0902b8

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    .line 120
    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mRouteButton:Landroid/widget/Button;

    .line 122
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mViewLocal:Landroid/view/View;

    .line 123
    const v7, 0x7f0902b7

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    .line 122
    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mCancelButton:Landroid/widget/Button;

    .line 124
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mRouteButton:Landroid/widget/Button;

    invoke-virtual {v6, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v6, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 129
    const v7, 0x7f020238

    .line 127
    invoke-static {v6, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 131
    .local v5, "shareBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 132
    const v7, 0x7f02023b

    .line 130
    invoke-static {v6, v5, v7}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 133
    .local v2, "maskedBitmap":Landroid/graphics/Bitmap;
    if-nez v2, :cond_1

    .line 135
    sget-object v6, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 136
    const v7, 0x7f02023d

    .line 137
    const v8, 0x7f02023b

    .line 134
    invoke-static {v6, v7, v8}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 139
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 140
    const v7, 0x7f02023c

    .line 139
    invoke-static {v6, v2, v7}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageOverlay(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 142
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 144
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mName:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 145
    .local v4, "notifcationMessage":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->readNotificationTTS()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    .end local v2    # "maskedBitmap":Landroid/graphics/Bitmap;
    .end local v3    # "msg":Ljava/lang/String;
    .end local v4    # "notifcationMessage":Ljava/lang/String;
    .end local v5    # "shareBitmap":Landroid/graphics/Bitmap;
    :cond_2
    :goto_1
    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->setUseAutoTimeout(Z)V

    goto/16 :goto_0

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private initPlaceOnDisclaimnerDialog()V
    .locals 3

    .prologue
    .line 202
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;-><init>()V

    .line 203
    .local v0, "dialog":Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;
    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->setOnClickListener(Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;)V

    .line 221
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    .line 222
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "dialog 3"

    .line 221
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 223
    return-void
.end method

.method private readNotificationTTS()V
    .locals 3

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cancelNotiFlow()V

    .line 158
    const-string/jumbo v0, "DM_LOCATION_REQ_CONFIRM"

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->setVoiceFlowId(Ljava/lang/String;)V

    .line 159
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addBegin()V

    .line 160
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->getTTSText()Ljava/lang/String;

    move-result-object v1

    .line 161
    const-string/jumbo v2, "DM_LOCATION_REQ_CONFIRM"

    .line 160
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTSWithFlowId(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 163
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 165
    const v2, 0x7f0a03a8

    .line 164
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 163
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTS(Ljava/lang/String;)V

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addFinish()V

    .line 167
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdFlowStart()V

    .line 168
    return-void
.end method

.method public static setContext(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 171
    sput-object p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mContext:Landroid/content/Context;

    .line 172
    return-void
.end method


# virtual methods
.method protected getTTSText()Ljava/lang/String;
    .locals 5

    .prologue
    .line 231
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 232
    const v3, 0x7f0a0607

    .line 231
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 233
    .local v0, "prompt":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mName:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 234
    .local v1, "promptDefault":Ljava/lang/String;
    return-object v1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 176
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 177
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 194
    :goto_0
    return-void

    .line 179
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->dismiss()V

    goto :goto_0

    .line 182
    :pswitch_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 183
    const-string/jumbo v2, "PREF_ACCEPT_DISCLAIMER_PLACE_ON"

    .line 184
    const/4 v3, 0x0

    .line 182
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v1

    .line 184
    if-eqz v1, :cond_0

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->positiveButton()V

    .line 186
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->dismiss()V

    goto :goto_0

    .line 188
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->disclaimer()V

    goto :goto_0

    .line 177
    nop

    :pswitch_data_0
    .packed-switch 0x7f0902b7
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onFlowCommandCancel(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 270
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->dismiss()V

    .line 271
    return-void
.end method

.method public onFlowCommandExcute(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 281
    const-string/jumbo v0, "DM_LOCATION_REQ_CONFIRM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->positiveButton()V

    .line 284
    :cond_0
    return-void
.end method

.method public onFlowCommandNo(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->dismiss()V

    .line 276
    return-void
.end method

.method public onFlowCommandRoute(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 266
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->positiveButton()V

    .line 267
    return-void
.end method

.method public onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 239
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p2}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 250
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V

    .line 251
    return-void

    .line 241
    :pswitch_0
    new-array v0, v4, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mRouteButton:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mCancelButton:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->setButtonQuoted(Z[Landroid/widget/Button;)V

    goto :goto_0

    .line 244
    :pswitch_1
    new-array v0, v4, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mRouteButton:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mCancelButton:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->setButtonQuoted(Z[Landroid/widget/Button;)V

    goto :goto_0

    .line 247
    :pswitch_2
    new-array v0, v4, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mRouteButton:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mCancelButton:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->setButtonQuoted(Z[Landroid/widget/Button;)V

    goto :goto_0

    .line 239
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onNotiCommandFinished(I)V
    .locals 3
    .param p1, "cmd"    # I

    .prologue
    .line 255
    const-string/jumbo v0, "TESTE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, " "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    const/16 v0, 0x9

    if-ne p1, v0, :cond_0

    .line 259
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->dismiss()V

    .line 261
    :cond_0
    return-void
.end method

.method public positiveButton()V
    .locals 4

    .prologue
    .line 197
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->getInstance()Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mContext:Landroid/content/Context;

    .line 198
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mNumber:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mUrl:Ljava/lang/String;

    .line 197
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->acceptLocationShared(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    return-void
.end method
