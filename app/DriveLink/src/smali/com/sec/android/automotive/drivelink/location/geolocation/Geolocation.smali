.class public Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;
.super Ljava/lang/Object;
.source "Geolocation.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "GeoLocation"

.field private static mInstance:Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;


# instance fields
.field private mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;->mInstance:Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;->mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    .line 21
    const-string/jumbo v0, "GeoLocation"

    const-string/jumbo v1, "Geolocation constructor"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 23
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;->mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    .line 24
    return-void
.end method

.method public static getInstance()Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;
    .locals 2

    .prologue
    .line 27
    const-string/jumbo v0, "GeoLocation"

    const-string/jumbo v1, "Geolocation getInstance"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;->mInstance:Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;->mInstance:Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;

    .line 31
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;->mInstance:Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;

    return-object v0
.end method


# virtual methods
.method public isProviderEnabled(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;->mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->isProviderEnabled(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public requestGeolocation(Landroid/content/Context;JLcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "timeout"    # J
    .param p4, "listener"    # Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;->mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->stop()V

    .line 51
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;->mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    invoke-virtual {v0, p4}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->setResponseListener(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;)V

    .line 52
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;->mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->execute(Landroid/content/Context;J)Z

    move-result v0

    return v0
.end method

.method protected requestGeolocation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;->mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->stop()V

    .line 67
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;->mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    invoke-virtual {v0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->setResponseListener(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;->mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->execute(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
