.class public Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "SetupWizardActivity.java"


# static fields
.field public static final EXTRA_NAME_FIRST_ACCESS:Ljava/lang/String; = "EXTRA_NAME_FIRST_ACCESS"

.field private static final IS_EXIT_DIALLOG:Ljava/lang/String; = "isExitDiallog"


# instance fields
.field public mIsExitdialog:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;->mIsExitdialog:Z

    .line 23
    return-void
.end method

.method private DialogExit()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 72
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;->mIsExitdialog:Z

    .line 73
    new-instance v1, Landroid/app/AlertDialog$Builder;

    .line 74
    const/4 v2, 0x4

    .line 73
    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 75
    .local v1, "exitdlg":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0a0256

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 76
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 77
    const v3, 0x7f0a0254

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 78
    new-instance v4, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity$1;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;)V

    .line 77
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 89
    const v3, 0x7f0a0255

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 90
    new-instance v4, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity$2;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;)V

    .line 89
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 98
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 100
    .local v0, "alert":Landroid/app/AlertDialog;
    const v2, 0x7f0a0258

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 102
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 103
    return-void
.end method

.method private initDefaultRejectMessage()V
    .locals 4

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 58
    const-string/jumbo v1, "drivelink_rejectmessage_body"

    .line 56
    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 62
    const-string/jumbo v1, "drivelink_rejectmessage_body"

    .line 60
    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 66
    const-string/jumbo v1, "drivelink_rejectmessage_body"

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 68
    const v3, 0x7f0a03dc

    .line 67
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 64
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 69
    :cond_1
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 31
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    if-eqz p1, :cond_0

    .line 34
    const-string/jumbo v2, "isExitDiallog"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 35
    const-string/jumbo v2, "isExitDiallog"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 36
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;->DialogExit()V

    .line 39
    :cond_0
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->setContext(Landroid/content/Context;)V

    .line 40
    invoke-static {}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->setUserRunBasicSettingOnce()V

    .line 42
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;->initDefaultRejectMessage()V

    .line 44
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 46
    .local v1, "preference":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v2, "PREF_SETTINGS_SUGGEST_CONTACTS"

    .line 45
    invoke-virtual {v1, v2, v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->setDefaultValue(Ljava/lang/String;Z)V

    .line 48
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 49
    const-class v3, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    .line 48
    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 50
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "EXTRA_NAME_FIRST_ACCESS"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 51
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;->startActivity(Landroid/content/Intent;)V

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;->finish()V

    .line 53
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 107
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 108
    const-string/jumbo v0, "isExitDiallog"

    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;->mIsExitdialog:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 109
    return-void
.end method
