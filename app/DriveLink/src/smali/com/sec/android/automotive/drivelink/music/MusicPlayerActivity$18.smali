.class Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$18;
.super Ljava/lang/Object;
.source "MusicPlayerActivity.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->initSearchTextField()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    .line 2089
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "editText"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2094
    packed-switch p2, :pswitch_data_0

    .line 2133
    :cond_0
    :goto_0
    return v6

    .line 2096
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V
    invoke-static {v4, v6}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 2098
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2100
    const-string/jumbo v4, "DM_MUSIC_PLAYER"

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v0

    .line 2101
    .local v0, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v0, :cond_1

    .line 2102
    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 2103
    iput-object v7, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 2104
    :cond_1
    new-instance v1, Landroid/content/Intent;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    .line 2105
    const-class v5, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    .line 2104
    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2106
    .local v1, "intent":Landroid/content/Intent;
    const/4 v4, 0x1

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->setIntentSearchMode(Z)V

    .line 2107
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$33(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2109
    .local v2, "keyword":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 2110
    const-string/jumbo v4, "keyword"

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$33(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    .line 2111
    invoke-interface {v5}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2110
    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2112
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    invoke-virtual {v4, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->startActivity(Landroid/content/Intent;)V

    .line 2113
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mTextfieldLayout:Landroid/view/ViewGroup;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$30(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/view/ViewGroup;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2114
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$31(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->showVoiceLayout()V

    .line 2115
    const-string/jumbo v4, "JINSEIL"

    const-string/jumbo v5, "IME_ACTION_SEARCH setText"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2116
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$33(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2117
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    invoke-static {v4, v6}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$32(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    goto/16 :goto_0

    .line 2123
    .end local v0    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "keyword":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 2125
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v4

    .line 2126
    invoke-interface {v4}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 2128
    const v5, 0x7f0a0199

    .line 2127
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2129
    .local v3, "systemTurnText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v4

    invoke-interface {v4, v3}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2094
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
