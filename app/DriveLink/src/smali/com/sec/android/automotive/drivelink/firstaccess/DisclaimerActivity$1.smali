.class Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$1;
.super Ljava/lang/Object;
.source "DisclaimerActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    .line 320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 329
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    .line 330
    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 329
    invoke-static {v0, p2}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->setWhenOnConnected(Landroid/content/Context;Landroid/os/IBinder;)V

    .line 332
    sget-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mNotification:Landroid/app/Notification;

    if-eqz v0, :cond_0

    .line 333
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 334
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    .line 335
    const/16 v1, 0x1e3e

    .line 336
    sget-object v2, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mNotification:Landroid/app/Notification;

    .line 334
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->startForegroundService(ILandroid/app/Notification;)V

    .line 338
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 324
    const-string/jumbo v0, "MUSIC"

    const-string/jumbo v1, "Music finished"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    return-void
.end method
