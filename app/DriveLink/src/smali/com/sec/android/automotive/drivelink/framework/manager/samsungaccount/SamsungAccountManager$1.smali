.class Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager$1;
.super Ljava/lang/Object;
.source "SamsungAccountManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountConnectService(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    invoke-static {p2}, Lcom/msc/sa/aidl/ISAService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/msc/sa/aidl/ISAService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;Lcom/msc/sa/aidl/ISAService;)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mISAService:Lcom/msc/sa/aidl/ISAService;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;)Lcom/msc/sa/aidl/ISAService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v0

    .line 97
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onResponseSamsungAccountConnectService(Z)V

    .line 102
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v0

    .line 100
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onResponseSamsungAccountConnectService(Z)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;Lcom/msc/sa/aidl/ISAService;)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v0

    .line 109
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onResponseSamsungAccountDisconnectService()V

    .line 110
    return-void
.end method
