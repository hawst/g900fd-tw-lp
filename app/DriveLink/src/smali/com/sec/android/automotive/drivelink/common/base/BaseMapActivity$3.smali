.class Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;
.super Ljava/lang/Object;
.source "BaseMapActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMapCreatedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->setMapInstance(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

.field private final synthetic val$temp:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->val$temp:Landroid/os/Bundle;

    .line 378
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapCreated()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 382
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->val$temp:Landroid/os/Bundle;

    const-string/jumbo v2, "address"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$4(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Ljava/lang/String;)V

    .line 383
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->val$temp:Landroid/os/Bundle;

    const-string/jumbo v2, "timestamp"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$8(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Ljava/lang/String;)V

    .line 384
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->val$temp:Landroid/os/Bundle;

    const-string/jumbo v2, "timestampEnd"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$9(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Ljava/lang/String;)V

    .line 385
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->val$temp:Landroid/os/Bundle;

    const-string/jumbo v3, "latitude"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->val$temp:Landroid/os/Bundle;

    .line 386
    const-string/jumbo v5, "longitude"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;-><init>(DD)V

    .line 385
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$3(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;)V

    .line 387
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->val$temp:Landroid/os/Bundle;

    const-string/jumbo v2, "ballon_disable"

    invoke-virtual {v1, v2, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$10(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;I)V

    .line 388
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->val$temp:Landroid/os/Bundle;

    .line 389
    const-string/jumbo v2, "myloc_enable"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 388
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$11(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Z)V

    .line 390
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->val$temp:Landroid/os/Bundle;

    const-string/jumbo v2, "custom_pin"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$12(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;I)V

    .line 391
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    .line 392
    .local v6, "bundle":Landroid/os/Bundle;
    if-eqz v6, :cond_0

    .line 393
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    const-string/jumbo v1, "userIndex"

    invoke-virtual {v6, v1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$13(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;I)V

    .line 395
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    invoke-virtual {v0, v7}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setZoomControlsEnabled(Z)V

    .line 397
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$6(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$6(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v3

    .line 398
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddress:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$7(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Ljava/lang/String;

    move-result-object v5

    .line 397
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->createMarks(DDLjava/lang/String;)V

    .line 399
    return-void
.end method
