.class public final Lcom/sec/android/automotive/drivelink/R$anim;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "anim"
.end annotation


# static fields
.field public static final ani_fade_in:I = 0x7f040000

.field public static final ani_fade_out:I = 0x7f040001

.field public static final ani_home_create:I = 0x7f040002

.field public static final ani_home_create_buttons:I = 0x7f040003

.field public static final ani_home_create_dialog:I = 0x7f040004

.field public static final ani_home_enter_actionbar:I = 0x7f040005

.field public static final ani_home_enter_buttons:I = 0x7f040006

.field public static final ani_home_enter_dialog:I = 0x7f040007

.field public static final ani_home_enter_drawer_menu_btn:I = 0x7f040008

.field public static final ani_home_enter_more_btn:I = 0x7f040009

.field public static final ani_home_exit:I = 0x7f04000a

.field public static final ani_home_exit_actionbar:I = 0x7f04000b

.field public static final ani_home_exit_buttons:I = 0x7f04000c

.field public static final ani_home_exit_dialog:I = 0x7f04000d

.field public static final ani_home_exit_drawer_menu_btn:I = 0x7f04000e

.field public static final ani_home_exit_more_btn:I = 0x7f04000f

.field public static final ani_info_count_num:I = 0x7f040010

.field public static final ani_info_count_ring:I = 0x7f040011

.field public static final ani_list_activity_create_back_btn:I = 0x7f040012

.field public static final ani_list_activity_create_dialog:I = 0x7f040013

.field public static final ani_list_activity_create_list:I = 0x7f040014

.field public static final ani_list_activity_create_navigation_layout:I = 0x7f040015

.field public static final ani_list_activity_create_search_btn:I = 0x7f040016

.field public static final ani_list_activity_exit_back_btn:I = 0x7f040017

.field public static final ani_list_activity_exit_dialog:I = 0x7f040018

.field public static final ani_list_activity_exit_list:I = 0x7f040019

.field public static final ani_list_activity_exit_navigation_layout:I = 0x7f04001a

.field public static final ani_list_activity_exit_search_btn:I = 0x7f04001b

.field public static final ani_list_location_activity_create_list:I = 0x7f04001c

.field public static final ani_mic_rotation:I = 0x7f04001d

.field public static final ani_mic_startq:I = 0x7f04001e

.field public static final ani_multiple_image:I = 0x7f04001f

.field public static final ani_process_bar:I = 0x7f040020

.field public static final ani_slide_down:I = 0x7f040021

.field public static final ani_slide_down_from_out:I = 0x7f040022

.field public static final ani_slide_up:I = 0x7f040023

.field public static final ani_slide_up_to_out:I = 0x7f040024

.field public static final ani_stay:I = 0x7f040025

.field public static final ani_tuto_call_count:I = 0x7f040026

.field public static final initial_content_up:I = 0x7f040027

.field public static final initial_profile_image_fade_in:I = 0x7f040028

.field public static final initial_profile_intro_fade_out:I = 0x7f040029

.field public static final initial_profile_scale_extend:I = 0x7f04002a

.field public static final noti_appear:I = 0x7f04002b

.field public static final noti_appear_in_multi:I = 0x7f04002c

.field public static final noti_disappear:I = 0x7f04002d

.field public static final noti_disappear_in_multi:I = 0x7f04002e

.field public static final pickup_popup_animation:I = 0x7f04002f

.field public static final rotate:I = 0x7f040030


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
