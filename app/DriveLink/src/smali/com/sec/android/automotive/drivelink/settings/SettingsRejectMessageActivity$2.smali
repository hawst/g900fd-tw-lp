.class Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$2;
.super Ljava/lang/Object;
.source "SettingsRejectMessageActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 70
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    .line 71
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 72
    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 73
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 74
    const v3, 0x7f0a03dc

    .line 73
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 74
    if-eqz v1, :cond_0

    .line 75
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->savePrefSettings(Ljava/lang/String;Z)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;Ljava/lang/String;Z)V

    .line 79
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 81
    .local v0, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    .line 80
    invoke-virtual {v0, v1, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 82
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->finish()V

    .line 83
    return-void

    .line 77
    .end local v0    # "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->savePrefSettings(Ljava/lang/String;Z)V
    invoke-static {v1, v2, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;Ljava/lang/String;Z)V

    goto :goto_0
.end method
