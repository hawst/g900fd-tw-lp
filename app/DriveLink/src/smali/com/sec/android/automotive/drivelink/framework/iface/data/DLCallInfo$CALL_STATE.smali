.class public final enum Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;
.super Ljava/lang/Enum;
.source "DLCallInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CALL_STATE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

.field public static final enum STATE_IDLE:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

.field public static final enum STATE_OFFHOOK:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

.field public static final enum STATE_RINGING:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

.field public static final enum UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    const-string/jumbo v1, "STATE_IDLE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;->STATE_IDLE:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    const-string/jumbo v1, "STATE_RINGING"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;->STATE_RINGING:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    const-string/jumbo v1, "STATE_OFFHOOK"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;->STATE_OFFHOOK:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    const-string/jumbo v1, "UNKNOWN"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    .line 4
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;->STATE_IDLE:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;->STATE_RINGING:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;->STATE_OFFHOOK:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
