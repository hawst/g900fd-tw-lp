.class public Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TaskManagerReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$OnTaskManagerReceiverListener;
    }
.end annotation


# static fields
.field public static final ACTION_S_VOICE_FOREGROUND:Ljava/lang/String; = "com.vlingo.midas.action.svoiceforeground"

.field public static final ACTION_TASK_CALL_END:Ljava/lang/String; = "com.sec.android.automotive.drivelink.ACTION_TASK_CALL_END"

.field public static final ACTION_TASK_MANAGER:Ljava/lang/String; = "com.sec.android.automotive.drivelink.ACTION_TASK_MANAGER"

.field public static final ACTION_TASK_RESTART_BG_WAKEUP:Ljava/lang/String; = "com.sec.android.automotive.drivelink.ACTION_TASK_RESTART_BG_WAKEUP"

.field public static final ACTION_VOICE_WAKEUP:Ljava/lang/String; = "com.sec.android.automotive.drivelink.ACTION_VOICE_WAKEUP"

.field public static final EXTRA_KEY_TASK_MANAGER_STATE:Ljava/lang/String; = "EXTRA_KEY_TASK_MANAGER_STATE"

.field public static final EXTRA_VOICE_WAKEUP:Ljava/lang/String; = "EXTRA_VOICE_WAKEUP"

.field private static final S_VOICE_PACKAGENAME:Ljava/lang/String; = "com.vlingo.midas"

.field private static final TAG:Ljava/lang/String; = "TaskManagerReceiver"

.field public static final TASK_MANAGER_STATE_NOT_RUNNING:I = 0x0

.field public static final TASK_MANAGER_STATE_PAUSE:I = 0x2

.field public static final TASK_MANAGER_STATE_RESUME:I = 0x1

.field private static mIsSVoiceRunning:Z

.field private static mPrevState:I

.field private static mState:I

.field private static mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private static sTaskManagerListener:Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$OnTaskManagerReceiverListener;


# instance fields
.field private cancelTurnHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 55
    sput v0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mPrevState:I

    .line 56
    sput v0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mState:I

    .line 57
    sput-boolean v0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mIsSVoiceRunning:Z

    .line 59
    sput-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 63
    sput-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->sTaskManagerListener:Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$OnTaskManagerReceiverListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mContext:Landroid/content/Context;

    .line 222
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$1;-><init>(Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->cancelTurnHandler:Landroid/os/Handler;

    .line 35
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1()I
    .locals 1

    .prologue
    .line 55
    sget v0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mPrevState:I

    return v0
.end method

.method static synthetic access$2(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 382
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->stopTaskObserverService(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->cancelTurnHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static checkIfCallerIsForegroundUser()Z
    .locals 8

    .prologue
    .line 316
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v2

    .line 317
    .local v2, "callingUser":I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 318
    .local v0, "callingIdentuty":J
    const/4 v4, 0x0

    .line 321
    .local v4, "valid":Z
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v3

    .line 322
    .local v3, "foregroundUser":I
    const-string/jumbo v5, "TaskManagerReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "UserCheck - foreground User :"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    if-ne v2, v3, :cond_0

    .line 325
    const/4 v4, 0x1

    .line 328
    :cond_0
    const-string/jumbo v5, "TaskManagerReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "UserCheck : valid="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "CallingUser :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 329
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ", foregroundUser :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 328
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 334
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 337
    .end local v3    # "foregroundUser":I
    :goto_0
    return v4

    .line 331
    :catch_0
    move-exception v5

    .line 334
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 333
    :catchall_0
    move-exception v5

    .line 334
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 335
    throw v5
.end method

.method public static getCarModeState()I
    .locals 3

    .prologue
    .line 295
    const-string/jumbo v0, "TaskManagerReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "getCarModeState : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    sget v0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mState:I

    return v0
.end method

.method private static getState()I
    .locals 1

    .prologue
    .line 125
    sget v0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mState:I

    return v0
.end method

.method public static getTaskManagerListener()Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$OnTaskManagerReceiverListener;
    .locals 1

    .prologue
    .line 497
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->sTaskManagerListener:Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$OnTaskManagerReceiverListener;

    return-object v0
.end method

.method private static getTelephonyManage(Landroid/content/Context;)Landroid/telephony/TelephonyManager;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 483
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    .line 485
    const-string/jumbo v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 484
    check-cast v0, Landroid/telephony/TelephonyManager;

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 486
    const-string/jumbo v0, "TaskManagerReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "getTelephonyManager : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method private handleCallEndAction(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 421
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->getState()I

    move-result v0

    .line 423
    .local v0, "curState":I
    const-string/jumbo v2, "TaskManagerReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "[handleCallEndAction] TaskManager State :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    .line 428
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$5;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$5;-><init>(Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;)V

    .line 434
    const-wide/16 v4, 0xc8

    .line 428
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 446
    :cond_0
    :goto_0
    return-void

    .line 436
    :cond_1
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 437
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v1

    .line 439
    .local v1, "flowID":Ljava/lang/String;
    const-string/jumbo v2, "TaskManagerReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "[handleCallEndAction] cur FlowID :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    if-eqz v1, :cond_0

    .line 442
    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0
.end method

.method private handleTaskObserverAction(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 390
    .line 391
    const-string/jumbo v2, "EXTRA_TASK_PKG_NAME"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 392
    .local v0, "pkgName":Ljava/lang/String;
    const-string/jumbo v2, "EXTRA_TASK_STATE"

    .line 393
    const/4 v3, 0x0

    .line 392
    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 395
    .local v1, "state":I
    const-string/jumbo v2, "TaskManagerReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "[handleTaskObserverAction] state : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 396
    const-string/jumbo v4, ", pkgName :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 395
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    const-string/jumbo v2, "com.vlingo.midas"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 398
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->getState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 399
    if-ne v1, v5, :cond_1

    .line 400
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getIntance()Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3, v5}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->stopWakeupBargeIn(Landroid/content/Context;Z)V

    .line 406
    :cond_0
    :goto_0
    return-void

    .line 402
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getIntance()Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->startWakeupBargeIn(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static isCallStateIdle(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 470
    const/4 v1, 0x1

    .line 471
    .local v1, "isIdle":Z
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->getTelephonyManage(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    .line 473
    .local v0, "callState":I
    if-eqz v0, :cond_0

    .line 474
    const/4 v1, 0x0

    .line 477
    :cond_0
    const-string/jumbo v2, "TaskManagerReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "isCallStateIdle : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    return v1
.end method

.method public static isCarModeOn(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 261
    const/4 v1, 0x0

    .line 264
    .local v1, "isCarModeOn":Z
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 265
    const-string/jumbo v3, "car_mode_on"

    .line 264
    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 265
    const/4 v3, 0x1

    .line 264
    if-ne v2, v3, :cond_0

    .line 266
    const/4 v1, 0x1

    .line 272
    :cond_0
    :goto_0
    return v1

    .line 268
    :catch_0
    move-exception v0

    .line 269
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static isCarModePaused(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 285
    const/4 v0, 0x0

    .line 287
    .local v0, "ret":Z
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->isCarModeOn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mState:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 288
    const/4 v0, 0x1

    .line 291
    :cond_0
    return v0
.end method

.method public static isCarModeResumed()Z
    .locals 2

    .prologue
    .line 276
    sget v0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 277
    sget v0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mState:I

    if-eqz v0, :cond_0

    .line 278
    const/4 v0, 0x1

    .line 281
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSvoiceRunning(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 456
    const-string/jumbo v1, "TaskManagerReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "getSvoiceRunning :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v3, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mIsSVoiceRunning:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    const-string/jumbo v1, "power"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 458
    check-cast v0, Landroid/os/PowerManager;

    .line 461
    .local v0, "powerManager":Landroid/os/PowerManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    if-nez v1, :cond_0

    .line 462
    const-string/jumbo v1, "TaskManagerReceiver"

    const-string/jumbo v2, "getSvoiceRunning : false - lcd off"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    const/4 v1, 0x0

    .line 466
    :goto_0
    return v1

    :cond_0
    const-string/jumbo v1, "com.vlingo.midas"

    invoke-static {p0, v1}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->IsAppRunning(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method private onAppFinished()V
    .locals 3

    .prologue
    .line 238
    const-string/jumbo v1, "TaskManagerReceiver"

    const-string/jumbo v2, "[onAppFinished]"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 241
    .local v0, "service":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 242
    const-string/jumbo v1, "TaskManagerReceiver"

    const-string/jumbo v2, "drive link is enabling the quick menu"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 246
    :cond_0
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$4;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$4;-><init>(Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 256
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 258
    return-void
.end method

.method private onAppPaused()V
    .locals 2

    .prologue
    .line 198
    const-string/jumbo v0, "TaskManagerReceiver"

    const-string/jumbo v1, "[onAppPaused]"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$3;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$3;-><init>(Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 219
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 220
    return-void
.end method

.method private onAppResumed()V
    .locals 2

    .prologue
    .line 169
    const-string/jumbo v0, "TaskManagerReceiver"

    const-string/jumbo v1, "[onAppResumed]"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$2;-><init>(Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 193
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 195
    return-void
.end method

.method private onStateChange(I)V
    .locals 4
    .param p1, "newState"    # I

    .prologue
    .line 129
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->getState()I

    move-result v0

    .line 132
    .local v0, "getState":I
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getIntance()Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    .line 133
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    .line 135
    sget v1, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mPrevState:I

    if-eq v1, p1, :cond_0

    .line 136
    const-string/jumbo v1, "TaskManagerReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[onStateChange] state conflict change - getstate :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 137
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", newState :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 136
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    sput p1, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mState:I

    .line 143
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 165
    :goto_1
    sput p1, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mPrevState:I

    .line 166
    return-void

    .line 140
    :cond_0
    const-string/jumbo v1, "TaskManagerReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[onStateChange] state :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 145
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->onAppResumed()V

    goto :goto_1

    .line 149
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->isCarModeOn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 150
    const-string/jumbo v1, "TaskManagerReceiver"

    const-string/jumbo v2, "[onStateChange] - Paused - CarMode on"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->onAppPaused()V

    goto :goto_1

    .line 153
    :cond_1
    const-string/jumbo v1, "TaskManagerReceiver"

    const-string/jumbo v2, "[onStateChange] - Paused - CarMode off"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->onStateChange(I)V

    goto :goto_1

    .line 160
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->onAppFinished()V

    goto :goto_1

    .line 143
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static sendTaskManagerBroadCase(Landroid/content/Context;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "state"    # I

    .prologue
    .line 366
    const-string/jumbo v1, "TaskManagerReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "sendTaskManagerBroadCase:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 369
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.automotive.drivelink.ACTION_TASK_MANAGER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 370
    const-string/jumbo v1, "EXTRA_KEY_TASK_MANAGER_STATE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 371
    const-string/jumbo v1, "com.sec.android.automotive.drivelink"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 372
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 373
    return-void
.end method

.method public static setState(Landroid/content/Context;I)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "state"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 341
    const-string/jumbo v0, "TaskManagerReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setState - :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    packed-switch p1, :pswitch_data_0

    .line 359
    :goto_0
    sget v0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mState:I

    if-ne v0, p1, :cond_0

    if-eq p1, v4, :cond_1

    .line 360
    :cond_0
    sput p1, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mState:I

    .line 361
    invoke-static {p0, p1}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->sendTaskManagerBroadCase(Landroid/content/Context;I)V

    .line 363
    :cond_1
    return-void

    .line 345
    :pswitch_0
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/nuance/drivelink/DLAppUiUpdater;->setInForeground(Z)V

    goto :goto_0

    .line 350
    :pswitch_1
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->startTaskObserverService(Landroid/content/Context;)V

    .line 351
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->setLastMicState(Lcom/nuance/sample/MicState;)V

    .line 352
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 354
    :pswitch_2
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/nuance/drivelink/DLAppUiUpdater;->setInForeground(Z)V

    goto :goto_0

    .line 343
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static setTaskManagerListener(Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$OnTaskManagerReceiverListener;)V
    .locals 0
    .param p0, "listener"    # Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$OnTaskManagerReceiverListener;

    .prologue
    .line 493
    sput-object p0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->sTaskManagerListener:Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$OnTaskManagerReceiverListener;

    .line 494
    return-void
.end method

.method private startPauseLock()V
    .locals 3

    .prologue
    .line 409
    const-string/jumbo v1, "TaskManagerReceiver"

    const-string/jumbo v2, "[startPauseLock] state : "

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    :try_start_0
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getWindowManagerService()Landroid/view/IWindowManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/IWindowManager;->lockNow(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 418
    :goto_0
    return-void

    .line 414
    :catch_0
    move-exception v0

    .line 416
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private static startTaskObserverService(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 376
    const-string/jumbo v1, "TaskManagerReceiver"

    const-string/jumbo v2, "[startService]"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 379
    .local v0, "service":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 380
    return-void
.end method

.method private startVoiceWakeup(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 300
    const-string/jumbo v1, "TaskManagerReceiver"

    const-string/jumbo v2, "CP wakeup spotted : resume app"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->checkIfCallerIsForegroundUser()Z

    move-result v1

    if-nez v1, :cond_0

    .line 312
    :goto_0
    return-void

    .line 306
    :cond_0
    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIsWakeup(Landroid/content/Context;Z)V

    .line 308
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 309
    .local v0, "mIntent":Landroid/content/Intent;
    const/high16 v1, 0x30000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 311
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private static stopTaskObserverService(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 383
    const-string/jumbo v1, "TaskManagerReceiver"

    const-string/jumbo v2, "[stopService]"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 386
    .local v0, "service":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 387
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    .line 72
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mContext:Landroid/content/Context;

    .line 74
    if-nez p2, :cond_0

    .line 122
    :goto_0
    return-void

    .line 78
    :cond_0
    const-string/jumbo v1, "com.sec.android.automotive.drivelink.ACTION_TASK_MANAGER"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 80
    const-string/jumbo v1, "EXTRA_KEY_TASK_MANAGER_STATE"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 81
    .local v0, "newState":I
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->onStateChange(I)V

    goto :goto_0

    .line 83
    .end local v0    # "newState":I
    :cond_1
    const-string/jumbo v1, "com.sec.android.automotive.drivelink.ACTION_VOICE_WAKEUP"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 85
    const-string/jumbo v1, "TaskManagerReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onReceived : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->startVoiceWakeup(Landroid/content/Context;)V

    goto :goto_0

    .line 88
    :cond_2
    const-string/jumbo v1, "com.sec.android.automotive.drivelink.ACTION_TASK_CALL_END"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 90
    const-string/jumbo v1, "TaskManagerReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onReceived : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->handleCallEndAction(Landroid/content/Context;)V

    .line 93
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->getCallEndCheckSingleton()Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->setCallEndFlag(Z)V

    .line 96
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->sTaskManagerListener:Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$OnTaskManagerReceiverListener;

    if-eqz v1, :cond_3

    .line 97
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->sTaskManagerListener:Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$OnTaskManagerReceiverListener;

    invoke-interface {v1}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$OnTaskManagerReceiverListener;->onReceiveTask()V

    .line 100
    :cond_3
    const-string/jumbo v1, ""

    const-string/jumbo v2, "CallEndCheckSingleton :::: EndCallReceiver true"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 102
    :cond_4
    const-string/jumbo v1, "com.sec.android.automotive.drivelink.ACTION_TASK_OBSERVER"

    .line 103
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 102
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 103
    if-eqz v1, :cond_5

    .line 105
    const-string/jumbo v1, "TaskManagerReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onReceived : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->handleTaskObserverAction(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 107
    :cond_5
    const-string/jumbo v1, "com.vlingo.midas.action.svoiceforeground"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 109
    const-string/jumbo v1, "TaskManagerReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onReceived : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    const-string/jumbo v1, "com.vlingo.midas"

    .line 110
    invoke-static {v4, v1}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->getTaskObserverIntent(ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 113
    :cond_6
    const-string/jumbo v1, "com.sec.android.automotive.drivelink.ACTION_TASK_RESTART_BG_WAKEUP"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 115
    const-string/jumbo v1, "TaskManagerReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onReceived : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getIntance()Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->startWakeupBargeIn(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 119
    :cond_7
    const-string/jumbo v1, "TaskManagerReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onReceived : what? "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
