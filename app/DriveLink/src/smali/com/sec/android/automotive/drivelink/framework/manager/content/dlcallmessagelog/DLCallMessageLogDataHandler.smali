.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/dlcallmessagelog/DLCallMessageLogDataHandler;
.super Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;
.source "DLCallMessageLogDataHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;-><init>()V

    .line 16
    return-void
.end method


# virtual methods
.method public addDLCallMessageLogData(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "DLLog"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .prologue
    .line 35
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->addDLCallMessageLog(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)V

    .line 36
    return-void
.end method

.method public getDLCallMessageLogList(Landroid/content/Context;Ljava/util/Date;I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fromDate"    # Ljava/util/Date;
    .param p3, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Date;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDLCallMessageLogList(Ljava/util/Date;I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public initialize(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    const/4 v0, 0x0

    return v0
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    return-void
.end method
