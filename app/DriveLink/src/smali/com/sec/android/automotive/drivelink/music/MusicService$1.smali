.class Lcom/sec/android/automotive/drivelink/music/MusicService$1;
.super Ljava/lang/Object;
.source "MusicService.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/music/MusicService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    .line 1090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMusicPlayCompleted(Landroid/media/MediaPlayer;)V
    .locals 0
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 1188
    return-void
.end method

.method public onMusicPlayError(Landroid/media/MediaPlayer;IILjava/lang/Object;)V
    .locals 5
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1160
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFPrevPlaying:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$8(Lcom/sec/android/automotive/drivelink/music/MusicService;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1161
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$5(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V

    .line 1162
    :cond_1
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "onMusicPlayError() "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1164
    const v0, 0x15f90

    if-ne p2, v0, :cond_3

    .line 1165
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setUserSettingPauseOrStop()V

    .line 1166
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyError(I)V
    invoke-static {v0, p2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$9(Lcom/sec/android/automotive/drivelink/music/MusicService;I)V

    .line 1183
    :cond_2
    :goto_0
    return-void

    .line 1168
    :cond_3
    const/16 v0, -0x26

    if-ne p2, v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mbIsFirstMusicPlay:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$10(Lcom/sec/android/automotive/drivelink/music/MusicService;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1172
    :cond_4
    const/16 v0, 0x64

    if-eq p2, v0, :cond_7

    .line 1173
    const/16 v0, -0x13

    if-eq p2, v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mOnErrorPrevPlay:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$4(Lcom/sec/android/automotive/drivelink/music/MusicService;)Z

    move-result v0

    if-nez v0, :cond_5

    const/16 v0, 0x26

    if-ne p2, v0, :cond_6

    .line 1174
    :cond_5
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->nextPlayer()V

    .line 1175
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, " File Not Found nextPlayer"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1177
    :cond_6
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->nextPlayerNoPlay()V

    .line 1178
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, " File Not Found nextPlayerNoPlay"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1181
    :cond_7
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyMusicChanged(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$2(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V

    goto :goto_0
.end method

.method public onMusicPlayPaused(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;I)V
    .locals 5
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .param p2, "position"    # I

    .prologue
    .line 1144
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onMusicPlayPaused"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1145
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    iget-boolean v0, v0, Lcom/sec/android/automotive/drivelink/music/MusicService;->b_isListenerOn:Z

    if-nez v0, :cond_1

    .line 1146
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/automotive/drivelink/music/MusicService;->b_isListenerOn:Z

    .line 1156
    :cond_0
    :goto_0
    return-void

    .line 1148
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyMusicChanged(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$2(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V

    .line 1150
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "mIsPausedFromAudioFocusLoss : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsPausedFromAudioFocusLoss:Z
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$7(Lcom/sec/android/automotive/drivelink/music/MusicService;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1151
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsPausedFromAudioFocusLoss:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$7(Lcom/sec/android/automotive/drivelink/music/MusicService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1152
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$0(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$1(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->pausePlayer(I)V

    .line 1153
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$0(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mDefaultAlbumArt:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicService;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->setMetaData(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public onMusicPlayResumed(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 4
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 1132
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onMusicPlayResumed"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1133
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyMusicChanged(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$2(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V

    .line 1135
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$0(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$1(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->startPlayer(I)V

    .line 1136
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$0(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mDefaultAlbumArt:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicService;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->setMetaData(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Landroid/graphics/Bitmap;)V

    .line 1138
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mOnErrorPrevPlay:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$4(Lcom/sec/android/automotive/drivelink/music/MusicService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1139
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$5(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V

    .line 1140
    :cond_0
    return-void
.end method

.method public onMusicPlaySeekComplete(Landroid/media/MediaPlayer;)V
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 1126
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onMusicPlaySeekComplete"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1127
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifySeekComplte()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$6(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    .line 1128
    return-void
.end method

.method public onMusicPlayStarted(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Z)V
    .locals 5
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .param p2, "bPlayNow"    # Z

    .prologue
    .line 1109
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "onMusicPlayStarted playNow : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1110
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyMusicChanged(Z)V
    invoke-static {v0, p2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$2(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V

    .line 1112
    if-eqz p2, :cond_1

    .line 1113
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$0(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$1(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->startPlayer(I)V

    .line 1118
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$0(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mDefaultAlbumArt:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicService;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->setMetaData(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Landroid/graphics/Bitmap;)V

    .line 1120
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mOnErrorPrevPlay:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$4(Lcom/sec/android/automotive/drivelink/music/MusicService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1121
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$5(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V

    .line 1122
    :cond_0
    return-void

    .line 1115
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$0(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$1(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->pausePlayer(I)V

    goto :goto_0
.end method

.method public onMusicPlayStopped(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 5
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    const/4 v4, 0x0

    .line 1094
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onMusicPlayStopped"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1095
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setUserSettingPauseOrStop()V

    .line 1098
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$0(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    move-result-object v0

    const-wide/16 v1, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->setPlaybackState(ZJF)V

    .line 1101
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$1(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->startPlayer(Z)V

    .line 1105
    return-void
.end method

.method public onMusicPlayerEnvInfoChanged(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;)V
    .locals 0
    .param p1, "musicPlayerEnvInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    .prologue
    .line 1203
    return-void
.end method
