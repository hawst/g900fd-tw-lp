.class Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;
.super Ljava/lang/Object;
.source "SearchMusicPageAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "imageLoadRunnable"
.end annotation


# instance fields
.field private mAlbum:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

.field private mArtist:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mImageView:Landroid/widget/ImageView;

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;Landroid/widget/ImageView;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)V
    .locals 1
    .param p2, "imageView"    # Landroid/widget/ImageView;
    .param p3, "album"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    .prologue
    const/4 v0, 0x0

    .line 97
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mImageView:Landroid/widget/ImageView;

    .line 88
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mArtist:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .line 89
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mAlbum:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    .line 90
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mBitmap:Landroid/graphics/Bitmap;

    .line 98
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mImageView:Landroid/widget/ImageView;

    .line 99
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mAlbum:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    .line 100
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;Landroid/widget/ImageView;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)V
    .locals 1
    .param p2, "imageView"    # Landroid/widget/ImageView;
    .param p3, "artist"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .prologue
    const/4 v0, 0x0

    .line 92
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mImageView:Landroid/widget/ImageView;

    .line 88
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mArtist:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .line 89
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mAlbum:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    .line 90
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mBitmap:Landroid/graphics/Bitmap;

    .line 93
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mImageView:Landroid/widget/ImageView;

    .line 94
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mArtist:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .line 95
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mImageView:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 127
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mArtist:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    if-eqz v0, :cond_3

    .line 109
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    .line 110
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mArtist:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .line 109
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getMusicAlbumArt(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mBitmap:Landroid/graphics/Bitmap;

    .line 116
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    .line 117
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->access$0(Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 118
    const v1, 0x7f020223

    .line 117
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mBitmap:Landroid/graphics/Bitmap;

    .line 121
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->access$1(Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable$1;-><init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 111
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mAlbum:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    if-eqz v0, :cond_1

    .line 112
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    .line 113
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mAlbum:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    .line 112
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getMusicAlbumArt(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_1
.end method
