.class Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$2;
.super Ljava/lang/Object;
.source "SettingsAddContactsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->buildItemRow(Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;Landroid/view/View;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

.field private final synthetic val$holder:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$2;->this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$2;->val$holder:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;

    .line 673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 676
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$2;->val$holder:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->cbFavoriteContacts:Landroid/widget/CheckBox;

    .line 677
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$2;->val$holder:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->cbFavoriteContacts:Landroid/widget/CheckBox;

    .line 678
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 677
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 679
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$2;->val$holder:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->cbFavoriteContacts:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->callOnClick()Z

    .line 680
    return-void

    .line 677
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
