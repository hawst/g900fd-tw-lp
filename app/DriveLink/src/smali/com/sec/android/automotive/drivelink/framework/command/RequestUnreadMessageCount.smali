.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCount;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestUnreadMessageCount.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .prologue
    .line 15
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 10
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCount;->mContext:Landroid/content/Context;

    .line 11
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCount;->mCount:I

    .line 17
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCount;->mContext:Landroid/content/Context;

    .line 18
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCount;->mContext:Landroid/content/Context;

    .line 39
    return-void
.end method

.method protected process()V
    .locals 2

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCount;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 23
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 24
    .local v0, "contentManager":Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCount;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getUnreadMessageCount(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCount;->mCount:I

    .line 25
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCount;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 30
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMessageListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;

    move-result-object v0

    .line 31
    .local v0, "messageListener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;
    if-eqz v0, :cond_0

    .line 32
    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCount;->mCount:I

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;->onResponseRequestUnreadMessageCount(I)V

    .line 34
    :cond_0
    return-void
.end method
