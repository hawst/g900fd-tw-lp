.class Lcom/sec/android/automotive/drivelink/home/HomeActivity$8;
.super Ljava/lang/Object;
.source "HomeActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/home/HomeActivity;->moreMenuSelected(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

.field private final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/home/HomeActivity;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    iput p2, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$8;->val$position:I

    .line 700
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 704
    iget v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$8;->val$position:I

    packed-switch v3, :pswitch_data_0

    .line 799
    :cond_0
    :goto_0
    return-void

    .line 708
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    new-instance v4, Landroid/content/Intent;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    .line 709
    const-class v6, Lcom/sec/android/automotive/drivelink/help/WhatCanISayActivity;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 708
    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 714
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mIsNowDriving:Z
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$11(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 715
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    new-instance v4, Landroid/content/Intent;

    .line 716
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    const-class v6, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 715
    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 722
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mIsNowDriving:Z
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$11(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 726
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 727
    const-string/jumbo v4, "com.samsung.helphub"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 728
    .local v1, "info":Landroid/content/pm/PackageInfo;
    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 735
    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 759
    new-instance v2, Landroid/content/Intent;

    .line 760
    const-string/jumbo v3, "com.samsung.helphub.HELP"

    .line 759
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 761
    .local v2, "intent":Landroid/content/Intent;
    const/high16 v3, 0x30200000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 764
    const-string/jumbo v3, "helphub:section"

    const-string/jumbo v4, "car_mode"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 765
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    invoke-virtual {v3, v2}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 792
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    .end local v2    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 794
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 767
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v1    # "info":Landroid/content/pm/PackageInfo;
    :cond_1
    :try_start_1
    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 778
    new-instance v2, Landroid/content/Intent;

    .line 779
    const-string/jumbo v3, "com.samsung.helphub.HELP"

    .line 778
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 780
    .restart local v2    # "intent":Landroid/content/Intent;
    const/high16 v3, 0x30200000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 783
    const-string/jumbo v3, "helphub:appid"

    const-string/jumbo v4, "car_mode"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 787
    sget v3, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->ZONE_A:I

    .line 788
    sget v4, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->ZONE_B:I

    .line 787
    or-int/2addr v3, v4

    .line 789
    const/4 v4, 0x0

    .line 785
    invoke-static {v2, v3, v4}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->makeMultiWindowIntent(Landroid/content/Intent;ILandroid/graphics/Rect;)Landroid/content/Intent;

    move-result-object v2

    .line 790
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    invoke-virtual {v3, v2}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 704
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
