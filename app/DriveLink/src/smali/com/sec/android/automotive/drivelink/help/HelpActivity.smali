.class public Lcom/sec/android/automotive/drivelink/help/HelpActivity;
.super Landroid/app/Activity;
.source "HelpActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/help/HelpActivity$HelpListAdapter;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/sec/android/automotive/drivelink/help/HelpActivity$HelpListAdapter;

.field private mBackLayout:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private final mListItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->mListItems:Ljava/util/ArrayList;

    .line 32
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/help/HelpActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/help/HelpActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->mListItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method private initHelpContentsPage()V
    .locals 3

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->mListItems:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04b0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->mListItems:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04b1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->mListItems:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04b2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    const v0, 0x7f090074

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->mListView:Landroid/widget/ListView;

    .line 67
    new-instance v0, Lcom/sec/android/automotive/drivelink/help/HelpActivity$HelpListAdapter;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/help/HelpActivity$HelpListAdapter;-><init>(Lcom/sec/android/automotive/drivelink/help/HelpActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->mAdapter:Lcom/sec/android/automotive/drivelink/help/HelpActivity$HelpListAdapter;

    .line 68
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->mAdapter:Lcom/sec/android/automotive/drivelink/help/HelpActivity$HelpListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 69
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/android/automotive/drivelink/help/HelpActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/help/HelpActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/help/HelpActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 84
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 44
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v0, 0x7f03000e

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->setContentView(I)V

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->mContext:Landroid/content/Context;

    .line 47
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->initHelpContentsPage()V

    .line 49
    const v0, 0x7f090072

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->mBackLayout:Landroid/widget/LinearLayout;

    .line 50
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->mBackLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/help/HelpActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/help/HelpActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/help/HelpActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    return-void
.end method
