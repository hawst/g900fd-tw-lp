.class Lcom/sec/android/automotive/drivelink/location/MapViewActivity$4;
.super Ljava/lang/Object;
.source "MapViewActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/MapViewActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/MapViewActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/location/MapViewActivity;

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/location/MapViewActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->isProviderEnabled()Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->access$1(Lcom/sec/android/automotive/drivelink/location/MapViewActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/location/MapViewActivity;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    .line 114
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    .line 113
    # invokes: Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->hasGoogleAccount(Landroid/content/Context;)Z
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->access$2(Lcom/sec/android/automotive/drivelink/location/MapViewActivity;Landroid/content/Context;)Z

    move-result v0

    .line 114
    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/location/MapViewActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->startNavigation()V

    .line 122
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/location/MapViewActivity;

    const v1, 0x7f0a01fb

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->showToast(I)V

    goto :goto_0

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/location/MapViewActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->getEnableGPSPDialog()V

    goto :goto_0
.end method
