.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationDailyCommuteDialogView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationDailyCommuteDialogView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field static TAG:Ljava/lang/String;

.field private static isDebug:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-string/jumbo v0, "NotificationDailyCommuteDialogView"

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationDailyCommuteDialogView;->TAG:Ljava/lang/String;

    .line 189
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationDailyCommuteDialogView;->isDebug:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    .line 27
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationDailyCommuteDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationDailyCommuteDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 46
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationDailyCommuteDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 47
    return-void
.end method

.method private static Debug(Ljava/lang/String;)V
    .locals 3
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 192
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationDailyCommuteDialogView;->isDebug:Z

    if-eqz v0, :cond_0

    .line 193
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/NotificationDailyCommuteDialogView;->TAG:Ljava/lang/String;

    invoke-static {v0, v1, v2, p0}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_0
    return-void
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 85
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 93
    .local v0, "id":I
    return-void
.end method

.method public onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    .line 99
    invoke-super {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V

    .line 100
    return-void
.end method

.method public onNotificationDisappeared()V
    .locals 1

    .prologue
    .line 78
    const-string/jumbo v0, "onNotificationDisappeared"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationDailyCommuteDialogView;->Debug(Ljava/lang/String;)V

    .line 80
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onNotificationDisappeared()V

    .line 81
    return-void
.end method
