.class Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;
.super Ljava/lang/Object;
.source "Controller.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DropAnimateUpdater"
.end annotation


# instance fields
.field public FROM:F

.field public TO:F

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;


# direct methods
.method private constructor <init>(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;->this$0:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;)V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;-><init>(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;)V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 159
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;->this$0:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mbStopped:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->access$3(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;->this$0:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;

    const/16 v1, 0x190

    const/16 v2, 0x12c

    # invokes: Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->playRotate(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->access$4(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;II)V

    .line 167
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 172
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 177
    return-void
.end method

.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 149
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 151
    .local v0, "ratio":F
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;->this$0:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mUpperSandView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->access$0(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;)Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, v0

    iget v3, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;->FROM:F

    iget v4, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;->TO:F

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->render(FFF)V

    .line 152
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;->this$0:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mLowerSandView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->access$1(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;)Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;->FROM:F

    iget v3, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;->TO:F

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->render(FFF)V

    .line 153
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;->this$0:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropSandView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->access$2(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;)Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;->FROM:F

    iget v3, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;->TO:F

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->render(FFF)V

    .line 154
    return-void
.end method
