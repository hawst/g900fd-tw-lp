.class Lcom/sec/android/automotive/drivelink/location/LocationActivity$7;
.super Ljava/lang/Object;
.source "LocationActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationActivity;->initSearchTextField()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    .line 518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 525
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v1

    if-nez v1, :cond_0

    .line 526
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchMode:Z

    .line 528
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$6(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->showSearch()V

    .line 530
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->setAutoShrink(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$2(Lcom/sec/android/automotive/drivelink/location/LocationActivity;Z)V

    .line 543
    :goto_0
    return-void

    .line 535
    :cond_0
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 537
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v1

    .line 538
    invoke-interface {v1}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 540
    const v2, 0x7f0a0199

    .line 539
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 541
    .local v0, "systemTurnText":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    goto :goto_0
.end method
