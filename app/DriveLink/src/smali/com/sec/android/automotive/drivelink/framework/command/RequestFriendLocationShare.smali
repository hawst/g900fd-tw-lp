.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestFriendLocationShare.java"


# instance fields
.field private error:Ljava/lang/Exception;

.field private mContext:Landroid/content/Context;

.field private mPhoneNumber:Ljava/lang/String;

.field private mResult:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "friendPhoneNumber"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 17
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 9
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;->mContext:Landroid/content/Context;

    .line 10
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;->mPhoneNumber:Ljava/lang/String;

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;->mResult:Z

    .line 18
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;->mContext:Landroid/content/Context;

    .line 19
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;->mPhoneNumber:Ljava/lang/String;

    .line 20
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;->mContext:Landroid/content/Context;

    .line 44
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;->mPhoneNumber:Ljava/lang/String;

    .line 45
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;->error:Ljava/lang/Exception;

    .line 47
    return-void
.end method

.method protected process()V
    .locals 4

    .prologue
    .line 25
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v1

    .line 26
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->requestFriendLocation(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    .line 25
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;->mResult:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    :goto_0
    return-void

    .line 27
    :catch_0
    move-exception v0

    .line 28
    .local v0, "e":Ljava/lang/Exception;
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;->error:Ljava/lang/Exception;

    goto :goto_0
.end method

.method protected response()V
    .locals 3

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 35
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    move-result-object v0

    .line 36
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    if-eqz v0, :cond_0

    .line 37
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;->error:Ljava/lang/Exception;

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;->mResult:Z

    invoke-interface {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;->onResponseRequestFriendLocationShare(Ljava/lang/Exception;Z)V

    .line 39
    :cond_0
    return-void
.end method
