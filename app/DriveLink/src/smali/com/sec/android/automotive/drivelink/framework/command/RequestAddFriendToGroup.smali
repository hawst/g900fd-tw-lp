.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestAddFriendToGroup.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFriends:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;"
        }
    .end annotation
.end field

.field private mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

.field private mResult:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p4, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "friends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;->mContext:Landroid/content/Context;

    .line 14
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;->mFriends:Ljava/util/ArrayList;

    .line 15
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;->mResult:Z

    .line 22
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;->mContext:Landroid/content/Context;

    .line 23
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;->mFriends:Ljava/util/ArrayList;

    .line 24
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .line 25
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;->mContext:Landroid/content/Context;

    .line 46
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;->mFriends:Ljava/util/ArrayList;

    .line 47
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .line 48
    return-void
.end method

.method protected process()V
    .locals 4

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;->mFriends:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->requestAddFriendsToGroup(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v0

    .line 29
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;->mResult:Z

    .line 31
    return-void
.end method

.method protected response()V
    .locals 4

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 36
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    move-result-object v0

    .line 37
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    if-eqz v0, :cond_0

    .line 38
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;->mResult:Z

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;->mFriends:Ljava/util/ArrayList;

    .line 39
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .line 38
    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;->onResponseRequestAddFriendsToGroup(ZLjava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V

    .line 41
    :cond_0
    return-void
.end method
