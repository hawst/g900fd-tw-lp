.class public Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;
.super Ljava/lang/Object;
.source "UpdateFileUtil.java"


# instance fields
.field private mAbsolutePath:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mFile:Ljava/io/File;

.field private mFileName:Ljava/lang/String;

.field private mIsPrepared:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mIsPrepared:Z

    .line 15
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mContext:Landroid/content/Context;

    .line 16
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mFileName:Ljava/lang/String;

    .line 17
    return-void
.end method

.method private createDir(Ljava/lang/String;)Z
    .locals 3
    .param p1, "dir"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 38
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 40
    .local v0, "root":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 47
    :cond_0
    :goto_0
    return v1

    .line 44
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    .line 47
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private createInternalStorageFile(Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 51
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2, p1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 52
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, "absolutePath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 56
    .local v1, "ret":Ljava/io/File;
    return-object v1
.end method


# virtual methods
.method public delete()Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mIsPrepared:Z

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    .line 34
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public exists()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mIsPrepared:Z

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    .line 92
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAbsolutePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mAbsolutePath:Ljava/lang/String;

    return-object v0
.end method

.method public getFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mFile:Ljava/io/File;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method public length()J
    .locals 2

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mIsPrepared:Z

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 100
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public prepare()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 60
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mIsPrepared:Z

    if-eqz v2, :cond_0

    .line 80
    :goto_0
    return v0

    .line 64
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 65
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->createDir(Ljava/lang/String;)Z

    .line 67
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mFileName:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->createInternalStorageFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mFile:Ljava/io/File;

    .line 69
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mFile:Ljava/io/File;

    if-nez v2, :cond_1

    move v0, v1

    .line 70
    goto :goto_0

    .line 73
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mAbsolutePath:Ljava/lang/String;

    .line 75
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mIsPrepared:Z

    goto :goto_0

    :cond_2
    move v0, v1

    .line 77
    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mContext:Landroid/content/Context;

    .line 25
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mFile:Ljava/io/File;

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->mIsPrepared:Z

    .line 27
    return-void
.end method
