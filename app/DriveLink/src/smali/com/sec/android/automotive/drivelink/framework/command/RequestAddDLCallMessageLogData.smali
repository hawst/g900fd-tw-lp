.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestAddDLCallMessageLogData;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestAddDLCallMessageLogData.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDLLog:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "dlLog"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 10
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddDLCallMessageLogData;->mContext:Landroid/content/Context;

    .line 11
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddDLCallMessageLogData;->mDLLog:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .line 17
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddDLCallMessageLogData;->mContext:Landroid/content/Context;

    .line 18
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddDLCallMessageLogData;->mDLLog:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .line 19
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddDLCallMessageLogData;->mContext:Landroid/content/Context;

    .line 40
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddDLCallMessageLogData;->mDLLog:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .line 41
    return-void
.end method

.method protected process()V
    .locals 3

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddDLCallMessageLogData;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 24
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddDLCallMessageLogData;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddDLCallMessageLogData;->mDLLog:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->addDLCallMessageLogData(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)V

    .line 25
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddDLCallMessageLogData;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 30
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkDLCallMessageLogListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDLCallMessageLogListener;

    move-result-object v0

    .line 32
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDLCallMessageLogListener;
    if-eqz v0, :cond_0

    .line 33
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDLCallMessageLogListener;->onResponseAddDLCallMessageLogData(Z)V

    .line 35
    :cond_0
    return-void
.end method
