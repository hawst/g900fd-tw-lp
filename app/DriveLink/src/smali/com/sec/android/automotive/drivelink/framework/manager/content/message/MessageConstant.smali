.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageConstant;
.super Ljava/lang/Object;
.source "MessageConstant.java"


# static fields
.field static final AudioType:[Ljava/lang/String;

.field static final MMS_MSG:I = 0x1

.field static final NATIVE_MSG_APP:Ljava/lang/String; = "com.android.mms"

.field static final SMS_MSG:I

.field static final calendarType:[Ljava/lang/String;

.field static final contactType:[Ljava/lang/String;

.field static final imageType:[Ljava/lang/String;

.field static final textType:[Ljava/lang/String;

.field static final videoType:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 9
    const/16 v0, 0x1a

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "audio/basic"

    aput-object v1, v0, v3

    const-string/jumbo v1, "audio/midi"

    aput-object v1, v0, v4

    .line 10
    const-string/jumbo v1, "audio/mpeg"

    aput-object v1, v0, v5

    const-string/jumbo v1, "audio/mpegurl"

    aput-object v1, v0, v6

    const-string/jumbo v1, "audio/x-mpegurl"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "audio/prs.sid"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 11
    const-string/jumbo v2, "audio/x-aiff"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "audio/x-gsm"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "audio/x-ms-wma"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "audio/x-ms-wax"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 12
    const-string/jumbo v2, "audio/x-pn-realaudio"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "audio/x-realaudio"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "audio/x-scpls"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 13
    const-string/jumbo v2, "audio/x-sd2"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "audio/x-wav"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "audio/amr"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "audio/amr-wb"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 14
    const-string/jumbo v2, "audio/aac"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "audio/imelody"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "audio/flac"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "audio/mp4"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 15
    const-string/jumbo v2, "audio/mobile-xmf"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "audio/3gpp"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "audio/vnd.qcelp"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "audio/qcelp"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 16
    const-string/jumbo v2, "audio/evrc"

    aput-object v2, v0, v1

    .line 9
    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageConstant;->AudioType:[Ljava/lang/String;

    .line 18
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "video/3gpp"

    aput-object v1, v0, v3

    const-string/jumbo v1, "video/dl"

    aput-object v1, v0, v4

    const-string/jumbo v1, "video/dv"

    aput-object v1, v0, v5

    .line 19
    const-string/jumbo v1, "video/dv"

    aput-object v1, v0, v6

    const-string/jumbo v1, "video/fli"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "video/mp4"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "video/mpeg"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "video/mpeg4"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 20
    const-string/jumbo v2, "video/quicktime"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "video/vnd.mpegurl"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "video/x-la-asf"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 21
    const-string/jumbo v2, "video/x-la-asf"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "video/x-mng"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "video/x-ms-asf"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "video/x-ms-wm"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 22
    const-string/jumbo v2, "video/x-ms-wmv"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "video/x-ms-wmx"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "video/x-ms-wvx"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 23
    const-string/jumbo v2, "video/x-msvideo"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "video/x-sgi-movie"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "video/h263"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "video/divx"

    aput-object v2, v0, v1

    .line 18
    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageConstant;->videoType:[Ljava/lang/String;

    .line 25
    const/16 v0, 0x1d

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "image/bmp"

    aput-object v1, v0, v3

    const-string/jumbo v1, "image/x-ms-bmp"

    aput-object v1, v0, v4

    .line 26
    const-string/jumbo v1, "image/gif"

    aput-object v1, v0, v5

    const-string/jumbo v1, "image/ico"

    aput-object v1, v0, v6

    const-string/jumbo v1, "image/ief"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "image/jpg"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "image/jpeg"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 27
    const-string/jumbo v2, "image/pcx"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "image/png"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "image/svg+xml"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "image/tiff"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 28
    const-string/jumbo v2, "image/vnd.djvu"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "image/vnd.wap.wbmp"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "image/x-cmu-raster"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 29
    const-string/jumbo v2, "image/x-coreldraw"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "image/x-coreldrawpattern"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 30
    const-string/jumbo v2, "image/x-coreldrawtemplate"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "image/x-corelphotopaint"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 31
    const-string/jumbo v2, "image/x-icon"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "image/x-jg"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "image/x-jng"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "image/x-photoshop"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 32
    const-string/jumbo v2, "image/x-portable-anymap"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "image/x-portable-bitmap"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 33
    const-string/jumbo v2, "image/x-portable-graymap"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "image/x-portable-pixmap"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    .line 34
    const-string/jumbo v2, "image/x-rgb"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "image/x-xpixmap"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "image/x-xwindowdump"

    aput-object v2, v0, v1

    .line 25
    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageConstant;->imageType:[Ljava/lang/String;

    .line 36
    const/16 v0, 0x24

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "text/plain"

    aput-object v1, v0, v3

    const-string/jumbo v1, "text/richtext"

    aput-object v1, v0, v4

    .line 37
    const-string/jumbo v1, "text/rtf"

    aput-object v1, v0, v5

    const-string/jumbo v1, "text/texmacs"

    aput-object v1, v0, v6

    const-string/jumbo v1, "text/text"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 38
    const-string/jumbo v2, "text/tab-separated-values"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "text/xml"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "text/x-bibtex"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 39
    const-string/jumbo v2, "text/x-boo"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "text/x-c++hdr"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "text/x-c++src"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "text/x-chdr"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 40
    const-string/jumbo v2, "text/x-component"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "text/x-csh"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "text/x-csrc"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "text/x-dsrc"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 41
    const-string/jumbo v2, "text/x-haskell"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "text/x-java"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "text/x-literate-haskell"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 42
    const-string/jumbo v2, "text/x-moc"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "text/x-pascal"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "text/x-pcs-gcd"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "text/x-setext"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 43
    const-string/jumbo v2, "text/x-tcl"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "text/x-tex"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "text/x-vcalendar"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "text/x-vcard"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 44
    const-string/jumbo v2, "text/x-vnote"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "text/html"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "text/x-vtodo"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "text/calendar"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    .line 45
    const-string/jumbo v2, "text/comma-separated-values"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string/jumbo v2, "text/css"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string/jumbo v2, "text/h323"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    .line 46
    const-string/jumbo v2, "text/iuls"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string/jumbo v2, "text/mathml"

    aput-object v2, v0, v1

    .line 36
    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageConstant;->textType:[Ljava/lang/String;

    .line 48
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "text/x-vCalendar"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageConstant;->calendarType:[Ljava/lang/String;

    .line 50
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "text/x-vcard"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageConstant;->contactType:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
