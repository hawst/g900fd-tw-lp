.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestSetMyLocation;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestSetMyLocation.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

.field private mResult:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    const/4 v1, 0x0

    .line 17
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 10
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSetMyLocation;->mContext:Landroid/content/Context;

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSetMyLocation;->mResult:Z

    .line 12
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSetMyLocation;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 18
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSetMyLocation;->mContext:Landroid/content/Context;

    .line 19
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSetMyLocation;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 20
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSetMyLocation;->mContext:Landroid/content/Context;

    .line 40
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSetMyLocation;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 41
    return-void
.end method

.method protected process()V
    .locals 3

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSetMyLocation;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v0

    .line 25
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSetMyLocation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSetMyLocation;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->setMyLocation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v0

    .line 24
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSetMyLocation;->mResult:Z

    .line 26
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSetMyLocation;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 31
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    move-result-object v0

    .line 32
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    if-eqz v0, :cond_0

    .line 33
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSetMyLocation;->mResult:Z

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;->onResponseRequestSetMyLocation(Z)V

    .line 35
    :cond_0
    return-void
.end method
