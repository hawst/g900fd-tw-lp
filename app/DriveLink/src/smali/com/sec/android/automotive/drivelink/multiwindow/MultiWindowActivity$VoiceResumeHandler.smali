.class Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$VoiceResumeHandler;
.super Landroid/os/Handler;
.source "MultiWindowActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "VoiceResumeHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)V
    .locals 0

    .prologue
    .line 1006
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$VoiceResumeHandler;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1010
    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->resumeStarted:Z
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$12()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1028
    :goto_0
    return-void

    .line 1013
    :cond_0
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$13(Z)V

    .line 1015
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$VoiceResumeHandler;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->indicatorFragment:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$14(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1016
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$VoiceResumeHandler;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->indicatorFragment:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$14(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->setMicClickable(Z)V

    .line 1019
    :cond_1
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->resume(Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;)Z

    .line 1021
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$VoiceResumeHandler;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->indicatorFragment:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$14(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1022
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$VoiceResumeHandler;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->indicatorFragment:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$14(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->setMicClickable(Z)V

    .line 1025
    :cond_2
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$13(Z)V

    .line 1027
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0
.end method
