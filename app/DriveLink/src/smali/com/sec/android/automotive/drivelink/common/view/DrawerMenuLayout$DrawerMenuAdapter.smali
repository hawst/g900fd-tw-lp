.class public Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;
.super Landroid/widget/BaseAdapter;
.source "DrawerMenuLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DrawerMenuAdapter"
.end annotation


# instance fields
.field private mCurrentMenuType:I

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mMenuIconArray:[[I

.field private mMenuItemEnableStateArray:[Z

.field private mMenuNameArray:[Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;Landroid/content/Context;I)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "currentMenuType"    # I

    .prologue
    const/4 v3, 0x2

    .line 597
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 588
    const/4 v0, 0x7

    new-array v0, v0, [[I

    const/4 v1, 0x0

    .line 589
    new-array v2, v3, [I

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 590
    new-array v2, v3, [I

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    .line 591
    new-array v1, v3, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    const/4 v1, 0x3

    .line 592
    new-array v2, v3, [I

    fill-array-data v2, :array_3

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 593
    new-array v2, v3, [I

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 594
    new-array v2, v3, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 595
    new-array v2, v3, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuIconArray:[[I

    .line 598
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 599
    invoke-virtual {p0, p3}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->updateList(I)V

    .line 600
    return-void

    .line 589
    :array_0
    .array-data 4
        0x201
        0x7f020163
    .end array-data

    .line 590
    :array_1
    .array-data 4
        0x211
        0x7f020171
    .end array-data

    .line 591
    :array_2
    .array-data 4
        0x212
        0x7f020172
    .end array-data

    .line 592
    :array_3
    .array-data 4
        0x202
        0x7f02016e
    .end array-data

    .line 593
    :array_4
    .array-data 4
        0x221
        0x7f02016a
    .end array-data

    .line 594
    :array_5
    .array-data 4
        0x222
        0x7f02016c
    .end array-data

    .line 595
    :array_6
    .array-data 4
        0x224
        0x7f02016b
    .end array-data
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;)Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;
    .locals 1

    .prologue
    .line 582
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    return-object v0
.end method

.method private findMenuIconId(I)I
    .locals 3
    .param p1, "menuItemType"    # I

    .prologue
    const/4 v1, 0x0

    .line 843
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuIconArray:[[I

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 849
    :goto_1
    return v1

    .line 844
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuIconArray:[[I

    aget-object v2, v2, v0

    aget v2, v2, v1

    if-ne v2, p1, :cond_1

    .line 845
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuIconArray:[[I

    aget-object v1, v1, v0

    const/4 v2, 0x1

    aget v1, v1, v2

    goto :goto_1

    .line 843
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 631
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuNameArray:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 636
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuNameArray:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 641
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x100

    .line 646
    const/4 v2, -0x1

    .line 647
    .local v2, "itemType":I
    const/4 v1, 0x1

    .line 649
    .local v1, "isEnable":Z
    iget v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mCurrentMenuType:I

    packed-switch v5, :pswitch_data_0

    .line 788
    :cond_0
    :goto_0
    :pswitch_0
    and-int/lit16 v5, v2, 0x100

    if-ne v5, v6, :cond_3

    .line 789
    if-eqz p2, :cond_1

    .line 790
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v5

    and-int/lit16 v5, v5, 0x100

    if-eq v5, v6, :cond_2

    .line 791
    :cond_1
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 792
    const v6, 0x7f030045

    .line 791
    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 796
    :cond_2
    const v5, 0x7f0901a7

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 795
    check-cast v4, Landroid/widget/TextView;

    .line 797
    .local v4, "subTitle":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuNameArray:[Ljava/lang/String;

    aget-object v5, v5, p1

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 798
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 799
    invoke-virtual {p2, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 800
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuItemEnableStateArray:[Z

    const/4 v6, 0x0

    aput-boolean v6, v5, p1

    .line 832
    .end local v4    # "subTitle":Landroid/widget/TextView;
    :goto_1
    invoke-virtual {p2, v2}, Landroid/view/View;->setId(I)V

    .line 834
    return-object p2

    .line 651
    :pswitch_1
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 653
    :pswitch_2
    const/16 v2, 0x201

    .line 654
    goto :goto_0

    .line 656
    :pswitch_3
    const/16 v2, 0x202

    .line 657
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDriving:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$3(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 658
    const/4 v1, 0x0

    goto :goto_0

    .line 664
    :pswitch_4
    packed-switch p1, :pswitch_data_2

    goto :goto_0

    .line 666
    :pswitch_5
    const/16 v2, 0x201

    .line 667
    goto :goto_0

    .line 675
    :pswitch_6
    const/16 v2, 0x221

    .line 676
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDriving:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$3(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 677
    const/4 v1, 0x0

    .line 679
    goto :goto_0

    .line 681
    :pswitch_7
    const/16 v2, 0x222

    .line 682
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDriving:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$3(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 683
    const/4 v1, 0x0

    .line 685
    goto :goto_0

    .line 687
    :pswitch_8
    const/16 v2, 0x224

    .line 688
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDriving:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$3(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 689
    const/4 v1, 0x0

    goto :goto_0

    .line 695
    :pswitch_9
    packed-switch p1, :pswitch_data_3

    goto :goto_0

    .line 697
    :pswitch_a
    const/16 v2, 0x201

    .line 698
    goto :goto_0

    .line 700
    :pswitch_b
    const/16 v2, 0x101

    .line 701
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDriving:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$3(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoHome:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$4(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoOffice:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$5(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 702
    const/4 v1, 0x0

    .line 704
    goto/16 :goto_0

    .line 706
    :pswitch_c
    const/16 v2, 0x211

    .line 707
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDriving:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$3(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoHome:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$4(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 708
    const/4 v1, 0x0

    .line 710
    goto/16 :goto_0

    .line 712
    :pswitch_d
    const/16 v2, 0x212

    .line 713
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDriving:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$3(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoOffice:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$5(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 714
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 732
    :pswitch_e
    packed-switch p1, :pswitch_data_4

    goto/16 :goto_0

    .line 734
    :pswitch_f
    const/16 v2, 0x201

    .line 735
    goto/16 :goto_0

    .line 737
    :pswitch_10
    const/16 v2, 0x101

    .line 738
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDriving:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$3(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoHome:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$4(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoOffice:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$5(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 739
    const/4 v1, 0x0

    .line 741
    goto/16 :goto_0

    .line 743
    :pswitch_11
    const/16 v2, 0x211

    .line 744
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDriving:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$3(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoHome:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$4(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 745
    const/4 v1, 0x0

    .line 747
    goto/16 :goto_0

    .line 749
    :pswitch_12
    const/16 v2, 0x212

    .line 750
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDriving:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$3(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoOffice:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$5(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 751
    const/4 v1, 0x0

    .line 753
    goto/16 :goto_0

    .line 755
    :pswitch_13
    const/16 v2, 0x102

    .line 756
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDriving:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$3(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 757
    const/4 v1, 0x0

    .line 759
    goto/16 :goto_0

    .line 767
    :pswitch_14
    const/16 v2, 0x221

    .line 768
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDriving:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$3(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 769
    const/4 v1, 0x0

    .line 771
    goto/16 :goto_0

    .line 773
    :pswitch_15
    const/16 v2, 0x222

    .line 774
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDriving:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$3(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 775
    const/4 v1, 0x0

    .line 777
    goto/16 :goto_0

    .line 779
    :pswitch_16
    const/16 v2, 0x224

    .line 780
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDriving:Z
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$3(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 781
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 802
    :cond_3
    if-eqz p2, :cond_4

    .line 803
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v5

    and-int/lit16 v5, v5, 0x100

    if-ne v5, v6, :cond_5

    .line 804
    :cond_4
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 805
    const v6, 0x7f030044

    .line 804
    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 806
    new-instance v5, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter$1;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter$1;-><init>(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;)V

    invoke-virtual {p2, v5}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 820
    :cond_5
    const v5, 0x7f0901a5

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 819
    check-cast v0, Landroid/widget/ImageView;

    .line 821
    .local v0, "icon":Landroid/widget/ImageView;
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->findMenuIconId(I)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 824
    const v5, 0x7f0901a6

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 823
    check-cast v3, Landroid/widget/TextView;

    .line 825
    .local v3, "menuText":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuNameArray:[Ljava/lang/String;

    aget-object v5, v5, p1

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 827
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 828
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 829
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuItemEnableStateArray:[Z

    aput-boolean v1, v5, p1

    goto/16 :goto_1

    .line 649
    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_1
        :pswitch_4
        :pswitch_9
        :pswitch_0
        :pswitch_e
    .end packed-switch

    .line 651
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 664
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 695
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 732
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
    .end packed-switch
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 839
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuItemEnableStateArray:[Z

    aget-boolean v0, v0, p1

    return v0
.end method

.method public updateList(I)V
    .locals 2
    .param p1, "currentMenuType"    # I

    .prologue
    .line 603
    iput p1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mCurrentMenuType:I

    .line 605
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mCurrentMenuType:I

    packed-switch v0, :pswitch_data_0

    .line 627
    :goto_0
    :pswitch_0
    return-void

    .line 607
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$2(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 608
    const v1, 0x7f070009

    .line 607
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuNameArray:[Ljava/lang/String;

    .line 609
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuNameArray:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuItemEnableStateArray:[Z

    goto :goto_0

    .line 612
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$2(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 613
    const v1, 0x7f07000a

    .line 612
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuNameArray:[Ljava/lang/String;

    .line 614
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuNameArray:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuItemEnableStateArray:[Z

    goto :goto_0

    .line 617
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$2(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 618
    const v1, 0x7f07000b

    .line 617
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuNameArray:[Ljava/lang/String;

    .line 619
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuNameArray:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuItemEnableStateArray:[Z

    goto :goto_0

    .line 622
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$2(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 623
    const v1, 0x7f07000c

    .line 622
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuNameArray:[Ljava/lang/String;

    .line 624
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuNameArray:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->mMenuItemEnableStateArray:[Z

    goto :goto_0

    .line 605
    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
