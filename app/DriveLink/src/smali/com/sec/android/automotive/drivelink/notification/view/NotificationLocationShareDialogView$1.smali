.class Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView$1;
.super Ljava/lang/Object;
.source "NotificationLocationShareDialogView.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->initPlaceOnDisclaimnerDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;

    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickListener(Z)V
    .locals 4
    .param p1, "accept"    # Z

    .prologue
    .line 207
    if-eqz p1, :cond_0

    .line 208
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->getInstance()Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    move-result-object v0

    .line 209
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mNumber:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->access$0(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->mUrl:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->access$1(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;)Ljava/lang/String;

    move-result-object v3

    .line 208
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->acceptLocationShared(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;->dismiss()V

    .line 218
    return-void

    .line 213
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 214
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 212
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 215
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->onNotifyLocationSharedIgnored()V

    goto :goto_0
.end method
