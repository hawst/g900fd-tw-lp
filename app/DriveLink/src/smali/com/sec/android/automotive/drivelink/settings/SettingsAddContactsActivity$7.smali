.class Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;
.super Ljava/lang/Object;
.source "SettingsAddContactsActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    .line 285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4
    .param p1, "arg0"    # Landroid/text/Editable;

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 298
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 299
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->btnSearchContactsCancel:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$20(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->btnSearchContactsCancel:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$20(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 301
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->tvSearchContacts:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$6(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 309
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$21(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;I)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$8(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Z)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$10(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Z)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 315
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestFavoriteContactList(Landroid/content/Context;)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mRequestType:I
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$22(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)I

    move-result v0

    if-nez v0, :cond_2

    .line 317
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 318
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    const v2, 0xc350

    .line 317
    invoke-interface {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestContactList(Landroid/content/Context;I)V

    .line 324
    :cond_0
    :goto_1
    return-void

    .line 304
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->btnSearchContactsCancel:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$20(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 305
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->btnSearchContactsCancel:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$20(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 306
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->tvSearchContacts:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$6(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 320
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 321
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    .line 322
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 320
    invoke-interface {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestSearchedContactList(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 294
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 289
    return-void
.end method
