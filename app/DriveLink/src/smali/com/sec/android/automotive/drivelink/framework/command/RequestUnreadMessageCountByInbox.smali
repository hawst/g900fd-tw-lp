.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCountByInbox;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestUnreadMessageCountByInbox.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCount:I

.field private mDLInbox:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "inbox"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    .param p3, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .prologue
    const/4 v0, 0x0

    .line 17
    invoke-direct {p0, p3}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 11
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCountByInbox;->mContext:Landroid/content/Context;

    .line 12
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCountByInbox;->mDLInbox:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 13
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCountByInbox;->mCount:I

    .line 19
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCountByInbox;->mContext:Landroid/content/Context;

    .line 20
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCountByInbox;->mDLInbox:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 21
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCountByInbox;->mContext:Landroid/content/Context;

    .line 43
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCountByInbox;->mDLInbox:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 44
    return-void
.end method

.method protected process()V
    .locals 3

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCountByInbox;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 26
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 27
    .local v0, "contentManager":Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCountByInbox;->mContext:Landroid/content/Context;

    .line 28
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCountByInbox;->mDLInbox:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 27
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getMegetUnreadMessageCountByInbox(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCountByInbox;->mCount:I

    .line 29
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCountByInbox;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 34
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMessageListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;

    move-result-object v0

    .line 35
    .local v0, "messageListener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;
    if-eqz v0, :cond_0

    .line 36
    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCountByInbox;->mCount:I

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;->onResponseRequestUnreadMessageCountByInbox(I)V

    .line 38
    :cond_0
    return-void
.end method
