.class Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$2;
.super Ljava/lang/Object;
.source "DrivingManager.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->initialize(Landroid/content/Context;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    .line 209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 245
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsFirstAccelerometerUpdateEvent:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->access$5(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->access$6(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;Z)V

    .line 238
    :goto_0
    return-void

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mDrivingStatusLocationManager:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;)Landroid/location/LocationManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 230
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mSensorManager:Landroid/hardware/SensorManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->access$3(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;)Landroid/hardware/SensorManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 231
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    monitor-enter v1

    .line 232
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->processSensorChangeEvent(Landroid/hardware/SensorEvent;)V
    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->access$7(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;Landroid/hardware/SensorEvent;)V

    .line 231
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 235
    :cond_1
    const-string/jumbo v0, "DrivingManager.onLocationChanged()"

    .line 236
    const-string/jumbo v1, "Main Application is probably down!"

    .line 235
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
