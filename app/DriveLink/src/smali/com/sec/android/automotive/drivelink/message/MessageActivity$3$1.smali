.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$1;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->OnContactTypeInfo(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    .line 2056
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 2091
    const-string/jumbo v0, "MessageActivity"

    .line 2092
    const-string/jumbo v1, "OnContactTypeInfo - setAutoShrink : false n true"

    .line 2091
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2093
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 2094
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 2095
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 3
    .param p1, "arg0"    # I
    .param p2, "arg1"    # F
    .param p3, "arg2"    # I

    .prologue
    const/4 v2, 0x0

    .line 2070
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->isLoaded:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2071
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;Z)V

    .line 2072
    if-nez p1, :cond_1

    .line 2073
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$13(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    .line 2074
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v1

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$9(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;

    move-result-object v1

    .line 2075
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->getInitialPosition()I

    move-result v1

    .line 2074
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2076
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$15(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2077
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$9(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2078
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$9(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;

    move-result-object v0

    .line 2079
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->getPageCount()I

    move-result v0

    .line 2078
    if-eqz v0, :cond_0

    .line 2080
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$15(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 2087
    :cond_0
    :goto_0
    return-void

    .line 2082
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$15(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 2083
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v1

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$9(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;

    move-result-object v1

    .line 2084
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 2082
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    goto :goto_0
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 2060
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$9(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->getPageCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2061
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$15(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 2062
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v1

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$9(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;

    move-result-object v1

    .line 2063
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 2061
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 2065
    :cond_0
    return-void
.end method
