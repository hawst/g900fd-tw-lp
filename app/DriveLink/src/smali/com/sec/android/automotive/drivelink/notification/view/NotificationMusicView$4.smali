.class Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$4;
.super Ljava/lang/Object;
.source "NotificationMusicView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->init(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$4;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;

    .line 432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    const/4 v2, 0x1

    .line 437
    const-string/jumbo v4, "JINSEIL"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "JINSEIL = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_2

    .line 440
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$4;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setAutoShrink(Z)V
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->access$1(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;Z)V

    :cond_0
    :goto_0
    move v2, v3

    .line 461
    :cond_1
    :goto_1
    return v2

    .line 443
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    .line 445
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 446
    .local v0, "x":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 448
    .local v1, "y":F
    cmpg-float v4, v0, v7

    if-lez v4, :cond_3

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v4, v0, v4

    if-gez v4, :cond_3

    cmpg-float v4, v1, v7

    if-lez v4, :cond_3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v4, v1, v4

    if-ltz v4, :cond_0

    .line 450
    :cond_3
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$4;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->longPressed:Z
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->access$5(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 451
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$4;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;

    invoke-virtual {v3, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->onLongClick(Landroid/view/View;)Z

    goto :goto_1

    .line 457
    .end local v0    # "x":F
    .end local v1    # "y":F
    :cond_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-ne v4, v2, :cond_0

    .line 459
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$4;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setAutoShrink(Z)V
    invoke-static {v4, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->access$1(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;Z)V

    goto :goto_0
.end method
