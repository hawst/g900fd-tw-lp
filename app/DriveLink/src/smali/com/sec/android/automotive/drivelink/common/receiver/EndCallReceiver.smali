.class public Lcom/sec/android/automotive/drivelink/common/receiver/EndCallReceiver;
.super Landroid/content/BroadcastReceiver;
.source "EndCallReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field static mState:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/sec/android/automotive/drivelink/common/receiver/EndCallReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/EndCallReceiver;->TAG:Ljava/lang/String;

    .line 16
    sget-object v0, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/EndCallReceiver;->mState:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static enable(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "enable"    # Z

    .prologue
    const/4 v2, 0x1

    .line 93
    new-instance v0, Landroid/content/ComponentName;

    .line 94
    const-class v3, Lcom/sec/android/automotive/drivelink/common/receiver/EndCallReceiver;

    .line 93
    invoke-direct {v0, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 95
    .local v0, "component":Landroid/content/ComponentName;
    if-eqz p1, :cond_0

    move v1, v2

    .line 97
    .local v1, "flag":I
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v0, v1, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 99
    return-void

    .line 96
    .end local v1    # "flag":I
    :cond_0
    const/4 v1, 0x2

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 20
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/EndCallReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onReceive EXTRA_STATE"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 21
    const-string/jumbo v1, "state"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 22
    .local v0, "state":Ljava/lang/String;
    sget-object v1, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 23
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 24
    const-string/jumbo v2, "PREF_AUTO_LAUNCH_DURING_CALL"

    .line 23
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v1

    .line 25
    if-eqz v1, :cond_0

    .line 26
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 27
    const-string/jumbo v2, "PREF_CAR_APP_RUNNING"

    .line 26
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v1

    .line 27
    if-nez v1, :cond_0

    .line 28
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/EndCallReceiver;->TAG:Ljava/lang/String;

    .line 29
    const-string/jumbo v2, "Auto launch car app after call by BT connection"

    .line 28
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->launchApplication(Landroid/content/Context;)V

    .line 31
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 32
    const-string/jumbo v2, "PREF_AUTO_LAUNCH_DURING_CALL"

    .line 31
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 33
    invoke-static {p1, v3}, Lcom/sec/android/automotive/drivelink/common/receiver/EndCallReceiver;->enable(Landroid/content/Context;Z)V

    .line 63
    :cond_0
    return-void
.end method
