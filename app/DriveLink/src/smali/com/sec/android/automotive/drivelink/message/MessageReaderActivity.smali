.class public Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;
.source "MessageReaderActivity.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# static fields
.field public static final BUTTON_ICON_SIZE:I = 0x27

.field public static final BUTTON_ICON_TEXT_GAB:I = 0xf

.field public static final MESSAGE_COUNT:Ljava/lang/String; = "MESSAGE_COUNT"

.field public static final MESSAGE_TTS_COUNT:Ljava/lang/String; = "MESSAGE_TTS_COUNT"

.field public static final MESSAGE_TTS_STATE:Ljava/lang/String; = "MESSAGE_TTS_STATE"

.field public static final READ_MSG_COMPLETE:I = 0x3

.field public static final READ_MSG_PLAYING:I = 0x2

.field public static final READ_MSG_READY:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MessageReaderActivity"

.field private static isDebug:Z


# instance fields
.field private isVisible:Z

.field private mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mCallBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

.field private mContactImage:Landroid/widget/ImageView;

.field private mCurrentPage:I

.field private mDLInboxInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

.field mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

.field private mHandler:Landroid/os/Handler;

.field private mIsTTSlist:Z

.field private mMMSImage:Landroid/widget/ImageView;

.field private mMessageBody:Ljava/lang/String;

.field private mMessageCountTTS:Ljava/lang/String;

.field private mMessageInfoForTTS:Ljava/lang/String;

.field private mMessageName:Ljava/lang/String;

.field private mMessageNumber:Ljava/lang/String;

.field private mMessageReaderBodyLayout:Landroid/widget/LinearLayout;

.field private mMessageReaderPageAdaper:Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;

.field private mMessageReaderViewPager:Landroid/support/v4/view/ViewPager;

.field private mMessageTime:Ljava/lang/String;

.field private mMsgBody:Landroid/widget/TextView;

.field private mMsgChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MessageChangeListener;

.field private mMsgCount:Landroid/widget/TextView;

.field private mMsgInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;",
            ">;"
        }
    .end annotation
.end field

.field private mMsgScrollBody:Landroid/widget/TextView;

.field private mMsgScrollView:Landroid/widget/ScrollView;

.field private mName:Landroid/widget/TextView;

.field private mNextBtn:Landroid/widget/Button;

.field private mNoMsgBody:Landroid/widget/LinearLayout;

.field private mPrevBtn:Landroid/widget/Button;

.field mReaderStateListener:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;

.field private mReplyBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

.field private mTTSHandler:Landroid/os/Handler;

.field private mTTSListIndex:I

.field private mTTSMessageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTTSMsgPageIndex:I

.field private mTTSMsgState:I

.field mTTSPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

.field private mTime:Landroid/widget/TextView;

.field private mTotalMsgCount:I

.field private mTtsBtn:Landroid/widget/ImageButton;

.field private mUnreadMsgCount:I

.field private pageSet:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->isDebug:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;-><init>()V

    .line 102
    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTotalMsgCount:I

    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mUnreadMsgCount:I

    .line 109
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->isVisible:Z

    .line 112
    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgPageIndex:I

    .line 113
    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgState:I

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMessageList:Ljava/util/ArrayList;

    .line 115
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mIsTTSlist:Z

    .line 116
    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSListIndex:I

    .line 117
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSHandler:Landroid/os/Handler;

    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mAudioManager:Landroid/media/AudioManager;

    .line 126
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mHandler:Landroid/os/Handler;

    .line 232
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mReaderStateListener:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;

    .line 854
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MessageChangeListener;

    .line 935
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 1089
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    .line 75
    return-void
.end method

.method public static Debug(Ljava/lang/String;)V
    .locals 3
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 121
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->isDebug:Z

    if-eqz v0, :cond_0

    .line 122
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MessageReaderActivity"

    invoke-static {v0, v1, v2, p0}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    :cond_0
    return-void
.end method

.method private abandonAudioFocus()V
    .locals 1

    .prologue
    .line 931
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 933
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCallBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mReplyBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Z)V
    .locals 0

    .prologue
    .line 115
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mIsTTSlist:Z

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;I)V
    .locals 0

    .prologue
    .line 113
    iput p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgState:I

    return-void
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Z)V
    .locals 0

    .prologue
    .line 687
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->setCurrentPage(Z)V

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V
    .locals 0

    .prologue
    .line 1028
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->callReader()V

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V
    .locals 0

    .prologue
    .line 1006
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->replayReader()V

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageCountTTS:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$17(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V
    .locals 0

    .prologue
    .line 589
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->startMsgReader()V

    return-void
.end method

.method static synthetic access$18(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageReaderViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$19(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->pageSet:Z

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V
    .locals 0

    .prologue
    .line 406
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->statusIncommingMsgListInitBody()V

    return-void
.end method

.method static synthetic access$20(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    return v0
.end method

.method static synthetic access$21(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTotalMsgCount:I

    return v0
.end method

.method static synthetic access$22(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Z)V
    .locals 0

    .prologue
    .line 109
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->pageSet:Z

    return-void
.end method

.method static synthetic access$23(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgInfo:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$24(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;I)V
    .locals 0

    .prologue
    .line 102
    iput p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    return-void
.end method

.method static synthetic access$25(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 627
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getDate(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$26(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageTime:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$27(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$28(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageTime:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$29(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageBody:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;I)V
    .locals 0

    .prologue
    .line 102
    iput p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mUnreadMsgCount:I

    return-void
.end method

.method static synthetic access$30(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageBody:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMessageList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgState:I

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mIsTTSlist:Z

    return v0
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V
    .locals 0

    .prologue
    .line 930
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->abandonAudioFocus()V

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSListIndex:I

    return v0
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;I)V
    .locals 0

    .prologue
    .line 116
    iput p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSListIndex:I

    return-void
.end method

.method private calStringRealLength(Ljava/lang/String;Landroid/widget/Button;)I
    .locals 8
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "button"    # Landroid/widget/Button;

    .prologue
    .line 825
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    new-array v5, v6, [F

    .line 826
    .local v5, "widths":[F
    invoke-virtual {p2}, Landroid/widget/Button;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    .line 827
    .local v2, "mPaint":Landroid/graphics/Paint;
    const/4 v6, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v2, p1, v6, v7, v5}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;II[F)I

    move-result v0

    .line 828
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "sumOfLength":I
    const/4 v4, 0x0

    .line 830
    .local v4, "sumOfLengthDp":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 834
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->pxToDp(I)I

    move-result v4

    .line 836
    return v4

    .line 831
    :cond_0
    int-to-float v6, v3

    aget v7, v5, v1

    add-float/2addr v6, v7

    float-to-int v3, v6

    .line 830
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private callReader()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 1030
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 1031
    const-string/jumbo v1, "CM11"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 1033
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mDLInboxInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getDLContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v0

    .line 1034
    .local v0, "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-nez v0, :cond_0

    .line 1035
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1036
    .local v6, "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    new-instance v9, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    .line 1037
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageName:Ljava/lang/String;

    .line 1036
    invoke-direct {v9, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;-><init>(ILjava/lang/String;)V

    .line 1038
    .local v9, "dlPhoneNumber":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1039
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .end local v0    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    const-wide/16 v1, 0x0

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1042
    .end local v6    # "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    .end local v9    # "dlPhoneNumber":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    .restart local v0    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :cond_0
    invoke-static {v0}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v8

    .line 1044
    .local v8, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v7}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 1045
    .local v7, "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput-object v8, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 1046
    const/4 v1, 0x0

    iput v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    .line 1047
    const/4 v1, 0x1

    iput-boolean v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mIsFromMsgReader:Z

    .line 1048
    const-string/jumbo v1, "DM_DIAL"

    invoke-static {v1, v7}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 1049
    return-void
.end method

.method private dpToPx(I)I
    .locals 3
    .param p1, "dp"    # I

    .prologue
    .line 849
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 850
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    const/4 v1, 0x1

    int-to-float v2, p1

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    return v1
.end method

.method private getDate(J)Ljava/lang/String;
    .locals 16
    .param p1, "date"    # J

    .prologue
    .line 633
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 634
    .local v2, "cal":Ljava/util/Calendar;
    new-instance v10, Ljava/util/Date;

    invoke-direct {v10}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2, v10}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 636
    const/4 v10, 0x6

    invoke-virtual {v2, v10}, Ljava/util/Calendar;->get(I)I

    move-result v9

    .line 637
    .local v9, "today":I
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    .line 639
    .local v6, "lToday":J
    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 640
    const/4 v10, 0x6

    invoke-virtual {v2, v10}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 642
    .local v3, "day":I
    sub-int v4, v9, v3

    .line 644
    .local v4, "diffDay":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v5

    .line 646
    .local v5, "is24":Z
    if-nez v4, :cond_1

    .line 648
    if-eqz v5, :cond_0

    .line 649
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    const v11, 0x7f0a016d

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    .line 650
    const-string/jumbo v14, "kk:mm"

    new-instance v15, Ljava/util/Date;

    move-wide/from16 v0, p1

    invoke-direct {v15, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-static {v14, v15}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v14

    invoke-interface {v14}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    .line 649
    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 684
    .local v8, "ret":Ljava/lang/String;
    :goto_0
    return-object v8

    .line 652
    .end local v8    # "ret":Ljava/lang/String;
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    .line 653
    const v11, 0x7f0a016d

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    .line 654
    const-string/jumbo v14, "hh:mm AA"

    new-instance v15, Ljava/util/Date;

    move-wide/from16 v0, p1

    invoke-direct {v15, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-static {v14, v15}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v14

    .line 655
    invoke-interface {v14}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    .line 652
    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 657
    .restart local v8    # "ret":Ljava/lang/String;
    goto :goto_0

    .end local v8    # "ret":Ljava/lang/String;
    :cond_1
    const/4 v10, 0x1

    if-ne v4, v10, :cond_3

    .line 658
    if-eqz v5, :cond_2

    .line 659
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    .line 660
    const v11, 0x7f0a016c

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    .line 661
    const-string/jumbo v14, "kk:mm"

    new-instance v15, Ljava/util/Date;

    move-wide/from16 v0, p1

    invoke-direct {v15, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-static {v14, v15}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v14

    invoke-interface {v14}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    .line 659
    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 662
    .restart local v8    # "ret":Ljava/lang/String;
    goto :goto_0

    .line 663
    .end local v8    # "ret":Ljava/lang/String;
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    .line 664
    const v11, 0x7f0a016c

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    .line 665
    const-string/jumbo v14, "hh:mm AA"

    new-instance v15, Ljava/util/Date;

    move-wide/from16 v0, p1

    invoke-direct {v15, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-static {v14, v15}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v14

    .line 666
    invoke-interface {v14}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    .line 663
    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 668
    .restart local v8    # "ret":Ljava/lang/String;
    goto :goto_0

    .line 675
    .end local v8    # "ret":Ljava/lang/String;
    :cond_3
    if-eqz v5, :cond_4

    .line 676
    const-string/jumbo v10, "MM/dd kk:mm"

    new-instance v11, Ljava/util/Date;

    move-wide/from16 v0, p1

    invoke-direct {v11, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-static {v10, v11}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v10

    .line 677
    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    .line 678
    .restart local v8    # "ret":Ljava/lang/String;
    goto/16 :goto_0

    .line 679
    .end local v8    # "ret":Ljava/lang/String;
    :cond_4
    const-string/jumbo v10, "MM/dd hh:mm AA"

    new-instance v11, Ljava/util/Date;

    move-wide/from16 v0, p1

    invoke-direct {v11, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-static {v10, v11}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v10

    .line 680
    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    .line 679
    .restart local v8    # "ret":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method private getTTSBtnOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 614
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$11;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$11;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V

    return-object v0
.end method

.method private pxToDp(I)I
    .locals 6
    .param p1, "px"    # I

    .prologue
    .line 840
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 841
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 842
    .local v1, "met":Landroid/util/DisplayMetrics;
    int-to-float v3, p1

    iget v4, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v4, v4

    const/high16 v5, 0x43200000    # 160.0f

    div-float/2addr v4, v5

    div-float v2, v3, v4

    .line 843
    .local v2, "pxtodp":F
    float-to-int v0, v2

    .line 845
    .local v0, "dp":I
    return v0
.end method

.method private replayReader()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 1008
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 1009
    const-string/jumbo v1, "CM10"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 1011
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mDLInboxInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getDLContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v0

    .line 1012
    .local v0, "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-nez v0, :cond_0

    .line 1013
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1014
    .local v6, "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    new-instance v9, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    .line 1015
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageName:Ljava/lang/String;

    .line 1014
    invoke-direct {v9, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;-><init>(ILjava/lang/String;)V

    .line 1016
    .local v9, "dlPhoneNumber":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1017
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .end local v0    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    const-wide/16 v1, 0x0

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1020
    .end local v6    # "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    .end local v9    # "dlPhoneNumber":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    .restart local v0    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :cond_0
    invoke-static {v0}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v8

    .line 1022
    .local v8, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v7}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 1023
    .local v7, "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput-object v8, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 1024
    const-string/jumbo v1, "DM_SMS_COMPOSE"

    invoke-static {v1, v7}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 1026
    return-void
.end method

.method private requestAudioFocus()V
    .locals 4

    .prologue
    .line 569
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mAudioManager:Landroid/media/AudioManager;

    const/16 v2, 0x9

    .line 570
    const/4 v3, 0x1

    .line 569
    invoke-virtual {v1, p0, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 587
    .local v0, "result":I
    return-void
.end method

.method private setCurrentPage(Z)V
    .locals 7
    .param p1, "isDriving"    # Z

    .prologue
    .line 689
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;

    .line 690
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgInfo:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageTime:Ljava/lang/String;

    .line 691
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageName:Ljava/lang/String;

    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getTTSBtnOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v6

    move v3, p1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;-><init>(Landroid/content/Context;Ljava/util/ArrayList;ZLjava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 689
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageReaderPageAdaper:Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;

    .line 692
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageReaderViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageReaderPageAdaper:Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 693
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageReaderViewPager:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 695
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageReaderViewPager:Landroid/support/v4/view/ViewPager;

    .line 696
    new-instance v1, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 812
    return-void
.end method

.method private setMessageCountState()V
    .locals 3

    .prologue
    .line 815
    const-string/jumbo v0, ""

    .line 817
    .local v0, "countstate":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 818
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 819
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTotalMsgCount:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 821
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgCount:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 822
    return-void
.end method

.method private startMsgListReader()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 554
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mIsTTSlist:Z

    .line 555
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSListIndex:I

    .line 556
    const-string/jumbo v1, "MessageReaderActivity"

    const-string/jumbo v2, "MessageReader - startMsgListReader"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    const v0, 0x3f8ccccd    # 1.1f

    .line 558
    .local v0, "speechRate":F
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/facade/CoreManager;->ttsEngine(Landroid/content/Context;)Lcom/vlingo/core/facade/tts/ITTSEngine;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/vlingo/core/facade/tts/ITTSEngine;->setTtsSpeechRate(F)V

    .line 561
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 563
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelDialog()V

    .line 564
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMessageList:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSListIndex:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    .line 565
    iget v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSListIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSListIndex:I

    .line 566
    return-void
.end method

.method private startMsgReader()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 590
    const-string/jumbo v0, "MessageReaderActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "MessageReader - !!!!!!!!!!! mCurrentPage : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 591
    const-string/jumbo v2, " mTTSMsgIndex : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgPageIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " mTTSMsgState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 592
    iget v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 590
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 593
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    iget v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgPageIndex:I

    if-eq v0, v1, :cond_0

    .line 594
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->requestAudioFocus()V

    .line 596
    iput v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgState:I

    .line 597
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgPageIndex:I

    .line 598
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->startMsgListReader()V

    .line 611
    :goto_0
    return-void

    .line 599
    :cond_0
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    iget v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgPageIndex:I

    if-ne v0, v1, :cond_1

    .line 600
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgState:I

    if-ne v0, v3, :cond_1

    .line 601
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->requestAudioFocus()V

    .line 603
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->startMsgListReader()V

    goto :goto_0

    .line 605
    :cond_1
    const-string/jumbo v0, "MessageReaderActivity"

    .line 606
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "MessageReader - !!!!!!!!!!! cannot play msg mCurrentPage : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 607
    iget v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " mTTSMsgIndex : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 608
    iget v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgPageIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " mTTSMsgState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 609
    iget v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 606
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 605
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private startMsgReaderDmFlow()V
    .locals 3

    .prologue
    .line 905
    const-string/jumbo v1, "MessageReaderActivity"

    const-string/jumbo v2, "MessageReader - startMsgReaderDmFlow"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 907
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelDialog()V

    .line 909
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->addListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 914
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 915
    .local v0, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageCountTTS:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageInfoForTTS:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 916
    const-string/jumbo v1, "DM_SMS_READBACK"

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 917
    const-string/jumbo v1, "DM_SMS_READBACK"

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->setActivityFlowID(Ljava/lang/String;)V

    .line 918
    return-void
.end method

.method private statusIncommingMsgListInitBody()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 408
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getIncommingMessageList()Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgInfo:Ljava/util/ArrayList;

    .line 411
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgInfo:Ljava/util/ArrayList;

    if-nez v5, :cond_1

    .line 551
    :cond_0
    :goto_0
    return-void

    .line 415
    :cond_1
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgInfo:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    iput v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTotalMsgCount:I

    .line 422
    const-string/jumbo v5, "MessageReaderActivity"

    const-string/jumbo v6, "MessageReader - start"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v5

    .line 425
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;

    move-result-object v2

    .line 426
    .local v2, "dlspeedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    if-eqz v2, :cond_6

    .line 427
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;->getCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    move-result-object v5

    sget-object v6, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;->CAR_SPEED_STATUS_STOPPED:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    if-eq v5, v6, :cond_6

    .line 428
    invoke-direct {p0, v9}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->setCurrentPage(Z)V

    .line 433
    :goto_1
    iget v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    add-int/lit8 v5, v5, -0x1

    if-ltz v5, :cond_8

    iget v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    add-int/lit8 v5, v5, -0x1

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgInfo:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v5, v6, :cond_8

    .line 434
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgInfo:Ljava/util/ArrayList;

    iget v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .line 435
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;->getReceivedTime()J

    move-result-wide v5

    .line 434
    invoke-direct {p0, v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getDate(J)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageTime:Ljava/lang/String;

    .line 437
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/provider/Telephony$Sms;->getDefaultSmsPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 438
    .local v3, "nDefaultSmsApplication":Ljava/lang/String;
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x13

    if-lt v5, v6, :cond_2

    .line 439
    const-string/jumbo v5, "com.android.mms"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 440
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v6

    .line 441
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgInfo:Ljava/util/ArrayList;

    iget v7, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .line 440
    invoke-virtual {v6, v5}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->setMessageStateToRead(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)V

    .line 443
    :cond_2
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTime:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageTime:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 444
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgInfo:Ljava/util/ArrayList;

    iget v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;->getMsgType()I

    move-result v5

    if-nez v5, :cond_7

    .line 446
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgInfo:Ljava/util/ArrayList;

    iget v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;->getMsgBody()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageBody:Ljava/lang/String;

    .line 456
    .end local v3    # "nDefaultSmsApplication":Ljava/lang/String;
    :goto_2
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageBody:Ljava/lang/String;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageBody:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 457
    :cond_3
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgInfo:Ljava/util/ArrayList;

    iget v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;->getMsgType()I

    move-result v5

    if-nez v5, :cond_9

    .line 458
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 459
    const v6, 0x7f0a03b0

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 458
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageBody:Ljava/lang/String;

    .line 466
    :cond_4
    :goto_3
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mDLInboxInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    if-eqz v5, :cond_0

    .line 469
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mDLInboxInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getDLContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v5

    if-eqz v5, :cond_c

    .line 470
    const/4 v0, 0x0

    .line 471
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mDLInboxInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getDLContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageName:Ljava/lang/String;

    .line 473
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageName:Ljava/lang/String;

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_a

    .line 474
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mName:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 475
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageName:Ljava/lang/String;

    .line 504
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .local v4, "ttsName":Ljava/lang/String;
    :goto_4
    iget v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    iget v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgPageIndex:I

    if-ne v5, v6, :cond_5

    .line 505
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMessageList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 508
    :cond_5
    iget v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    if-ne v5, v9, :cond_10

    .line 509
    iget v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mUnreadMsgCount:I

    if-lez v5, :cond_e

    .line 510
    iget v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mUnreadMsgCount:I

    if-ne v5, v9, :cond_d

    .line 511
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 512
    const v6, 0x7f0a01e9

    .line 511
    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageCountTTS:Ljava/lang/String;

    .line 519
    :goto_5
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMessageList:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageCountTTS:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 540
    :goto_6
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMessageList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    .line 541
    const v7, 0x7f0a01eb

    new-array v8, v9, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageBody:Ljava/lang/String;

    aput-object v9, v8, v10

    .line 540
    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 545
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMessageList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    .line 546
    const v7, 0x7f0a0183

    .line 545
    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 549
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->startMsgReader()V

    goto/16 :goto_0

    .line 430
    .end local v4    # "ttsName":Ljava/lang/String;
    :cond_6
    invoke-direct {p0, v10}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->setCurrentPage(Z)V

    goto/16 :goto_1

    .line 449
    .restart local v3    # "nDefaultSmsApplication":Ljava/lang/String;
    :cond_7
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgInfo:Ljava/util/ArrayList;

    iget v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .line 450
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;->getAttachmentInfo()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessageAttachment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessageAttachment;->getText()Ljava/lang/String;

    move-result-object v5

    .line 449
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageBody:Ljava/lang/String;

    goto/16 :goto_2

    .line 453
    .end local v3    # "nDefaultSmsApplication":Ljava/lang/String;
    :cond_8
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "mCurrentPage"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTotalMsgCount:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->Debug(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 461
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 462
    const v6, 0x7f0a018c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 461
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageBody:Ljava/lang/String;

    goto/16 :goto_3

    .line 477
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_a
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mDLInboxInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageName:Ljava/lang/String;

    .line 478
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mDLInboxInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageNumber:Ljava/lang/String;

    .line 479
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 480
    const-string/jumbo v6, "country_detector"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 479
    check-cast v1, Landroid/location/CountryDetector;

    .line 482
    .local v1, "detector":Landroid/location/CountryDetector;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageNumber:Ljava/lang/String;

    invoke-virtual {v1}, Landroid/location/CountryDetector;->detectCountry()Landroid/location/Country;

    move-result-object v6

    .line 483
    invoke-virtual {v6}, Landroid/location/Country;->getCountryIso()Ljava/lang/String;

    move-result-object v6

    .line 481
    invoke-static {v5, v6}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageNumber:Ljava/lang/String;

    .line 484
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageNumber:Ljava/lang/String;

    if-nez v5, :cond_b

    .line 485
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mDLInboxInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageNumber:Ljava/lang/String;

    .line 487
    :cond_b
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mName:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageNumber:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 488
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageName:Ljava/lang/String;

    .line 489
    .restart local v4    # "ttsName":Ljava/lang/String;
    const-string/jumbo v5, ""

    const-string/jumbo v6, " "

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 492
    goto/16 :goto_4

    .line 493
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "detector":Landroid/location/CountryDetector;
    .end local v4    # "ttsName":Ljava/lang/String;
    :cond_c
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mDLInboxInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageName:Ljava/lang/String;

    .line 494
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mDLInboxInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageNumber:Ljava/lang/String;

    .line 495
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 496
    const-string/jumbo v6, "country_detector"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 495
    check-cast v1, Landroid/location/CountryDetector;

    .line 497
    .restart local v1    # "detector":Landroid/location/CountryDetector;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageNumber:Ljava/lang/String;

    .line 498
    invoke-virtual {v1}, Landroid/location/CountryDetector;->detectCountry()Landroid/location/Country;

    move-result-object v6

    invoke-virtual {v6}, Landroid/location/Country;->getCountryIso()Ljava/lang/String;

    move-result-object v6

    .line 497
    invoke-static {v5, v6}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageNumber:Ljava/lang/String;

    .line 499
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mName:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageNumber:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 500
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageName:Ljava/lang/String;

    .line 501
    .restart local v4    # "ttsName":Ljava/lang/String;
    const-string/jumbo v5, ""

    const-string/jumbo v6, " "

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_4

    .line 514
    .end local v1    # "detector":Landroid/location/CountryDetector;
    :cond_d
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 516
    const v6, 0x7f0a003e

    new-array v7, v9, [Ljava/lang/Object;

    .line 517
    iget v8, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mUnreadMsgCount:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    .line 515
    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 514
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageCountTTS:Ljava/lang/String;

    goto/16 :goto_5

    .line 521
    :cond_e
    iget v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTotalMsgCount:I

    if-ne v5, v9, :cond_f

    .line 522
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 523
    const v6, 0x7f0a01e8

    .line 522
    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageCountTTS:Ljava/lang/String;

    .line 529
    :goto_7
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMessageList:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageCountTTS:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 525
    :cond_f
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 526
    const v6, 0x7f0a018d

    new-array v7, v9, [Ljava/lang/Object;

    .line 527
    iget v8, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTotalMsgCount:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    .line 525
    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageCountTTS:Ljava/lang/String;

    goto :goto_7

    .line 532
    :cond_10
    const-string/jumbo v5, ""

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageCountTTS:Ljava/lang/String;

    goto/16 :goto_6
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 2
    .param p1, "focusChange"    # I

    .prologue
    .line 1132
    packed-switch p1, :pswitch_data_0

    .line 1159
    :goto_0
    :pswitch_0
    return-void

    .line 1143
    :pswitch_1
    const-string/jumbo v0, "MessageReaderActivity"

    const-string/jumbo v1, "MessageReaderActivity AUDIOFOCUS_LOSS_TRANSIENT"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1156
    :pswitch_2
    const-string/jumbo v0, "MessageReaderActivity"

    const-string/jumbo v1, "MessageReaderActivity AUDIOFOCUS_GAIN"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1132
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 399
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onBackPressed()V

    .line 401
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateInboxList()V

    .line 403
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelDialog()V

    .line 404
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 285
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 286
    const v0, 0x7f03001c

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->setContentView(I)V

    .line 288
    const v0, 0x7f0900c7

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCallBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 289
    const v0, 0x7f0900c6

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mReplyBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 290
    const v0, 0x7f090369

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mName:Landroid/widget/TextView;

    .line 291
    const v0, 0x7f090367

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTime:Landroid/widget/TextView;

    .line 292
    const v0, 0x7f0900c4

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageReaderBodyLayout:Landroid/widget/LinearLayout;

    .line 293
    const v0, 0x7f0900c5

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageReaderViewPager:Landroid/support/v4/view/ViewPager;

    .line 295
    const v0, 0x7f0900c3

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;

    .line 296
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$8;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 310
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mReaderStateListener:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->registerStateListener(Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;)V

    .line 312
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 314
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->statusIncommingMsgListInitBody()V

    .line 316
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCallBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$9;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$9;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 331
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mReplyBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$10;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$10;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 353
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x1

    .line 137
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 139
    const v0, 0x7f03001c

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->setContentView(I)V

    .line 141
    const v0, 0x7f0900c7

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCallBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 142
    const v0, 0x7f0900c6

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mReplyBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 143
    const v0, 0x7f090369

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mName:Landroid/widget/TextView;

    .line 144
    const v0, 0x7f090367

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTime:Landroid/widget/TextView;

    .line 145
    const v0, 0x7f0900c4

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageReaderBodyLayout:Landroid/widget/LinearLayout;

    .line 146
    const v0, 0x7f0900c5

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageReaderViewPager:Landroid/support/v4/view/ViewPager;

    .line 148
    const v0, 0x7f0900c3

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;

    .line 149
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;

    new-instance v2, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$5;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mReaderStateListener:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->registerStateListener(Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;)V

    .line 165
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;

    invoke-virtual {v0, v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->addListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 167
    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    .line 169
    if-nez p1, :cond_1

    move v0, v1

    :goto_1
    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgPageIndex:I

    .line 171
    if-nez p1, :cond_2

    :goto_2
    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgState:I

    .line 174
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;

    move-result-object v0

    .line 175
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    .line 174
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->addDrivingChangeListener(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;)V

    .line 176
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;

    move-result-object v0

    .line 177
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MessageChangeListener;

    .line 176
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->addMessageChangeListener(Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MessageChangeListener;)V

    .line 181
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/MessageHolder;->getMessageHolder()Lcom/sec/android/automotive/drivelink/message/MessageHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageHolder;->getCurrentInbox()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mDLInboxInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 182
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mDLInboxInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    if-eqz v0, :cond_3

    .line 183
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    .line 184
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mDLInboxInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 183
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateUnreadMessageCountByInbox(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;)V

    .line 185
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    .line 186
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mDLInboxInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 185
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateIncomingMessageList(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;)V

    .line 191
    :goto_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCallBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mReplyBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$7;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 229
    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 228
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mAudioManager:Landroid/media/AudioManager;

    .line 230
    return-void

    .line 168
    :cond_0
    const-string/jumbo v0, "MESSAGE_COUNT"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    .line 170
    :cond_1
    const-string/jumbo v0, "MESSAGE_TTS_COUNT"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_1

    .line 172
    :cond_2
    const-string/jumbo v0, "MESSAGE_TTS_STATE"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    goto :goto_2

    .line 188
    :cond_3
    const-string/jumbo v0, "what happed ? mDLInboxInfo == null"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->Debug(Ljava/lang/String;)V

    goto :goto_3
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->removeListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 272
    const-string/jumbo v0, "DM_SMS_READBACK"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->removeFlowList(Ljava/lang/String;)V

    .line 273
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;

    move-result-object v0

    .line 274
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->removeDrivingChangeListener(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;)V

    .line 275
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;

    move-result-object v0

    .line 276
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MessageChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->removeMessageChangeListener(Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MessageChangeListener;)V

    .line 277
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 279
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onDestroy()V

    .line 280
    return-void
.end method

.method public onFlowCommandCall(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 1058
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->callReader()V

    .line 1059
    return-void
.end method

.method public onFlowCommandNext(Ljava/lang/String;)V
    .locals 3
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 1076
    iget v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    iget v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTotalMsgCount:I

    if-ge v1, v2, :cond_1

    .line 1078
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageReaderViewPager:Landroid/support/v4/view/ViewPager;

    iget v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1087
    :cond_0
    :goto_0
    return-void

    .line 1079
    :cond_1
    iget v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    iget v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTotalMsgCount:I

    if-ne v1, v2, :cond_0

    .line 1080
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelDialog()V

    .line 1081
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 1082
    .local v0, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1083
    const v2, 0x7f0a0185

    .line 1082
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 1085
    const-string/jumbo v1, "DM_SMS_READBACK"

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0
.end method

.method public onFlowCommandRead(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 1065
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->startMsgReader()V

    .line 1066
    return-void
.end method

.method public onFlowCommandReply(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 1053
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->replayReader()V

    .line 1054
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 363
    const/4 v1, 0x3

    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgState:I

    .line 364
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mIsTTSlist:Z

    .line 365
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelDialog()V

    .line 367
    const v0, 0x3f99999a    # 1.2f

    .line 368
    .local v0, "speechRate":F
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/facade/CoreManager;->ttsEngine(Landroid/content/Context;)Lcom/vlingo/core/facade/tts/ITTSEngine;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/vlingo/core/facade/tts/ITTSEngine;->setTtsSpeechRate(F)V

    .line 370
    const-string/jumbo v1, ""

    const-string/jumbo v2, "mTTSPlaybackListener :::: onPause abandonAudioFocus"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->abandonAudioFocus()V

    .line 373
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onPause()V

    .line 374
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 898
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 899
    const-string/jumbo v0, "MESSAGE_COUNT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    .line 900
    const-string/jumbo v0, "MESSAGE_TTS_COUNT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgPageIndex:I

    .line 901
    const-string/jumbo v0, "MESSAGE_TTS_STATE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgState:I

    .line 902
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 384
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->isVisible:Z

    .line 385
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelDialog()V

    .line 386
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onResume()V

    .line 388
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageReaderPageAdaper:Lcom/sec/android/automotive/drivelink/message/MessageReaderPageAdaper;

    if-eqz v0, :cond_0

    .line 389
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    .line 390
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mDLInboxInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 389
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateIncomingMessageList(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;)V

    .line 392
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 889
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 890
    const-string/jumbo v0, "MESSAGE_COUNT"

    iget v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 891
    const-string/jumbo v0, "MESSAGE_TTS_COUNT"

    iget v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgPageIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 892
    const-string/jumbo v0, "MESSAGE_TTS_STATE"

    iget v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 893
    return-void
.end method

.method protected setActivityFlowID(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 1071
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->setActivityFlowID(Ljava/lang/String;)V

    .line 1072
    return-void
.end method
