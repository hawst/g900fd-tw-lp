.class Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$2;
.super Ljava/lang/Object;
.source "PrepareDialActivity.java"

# interfaces
.implements Lcom/sec/android/app/IWSpeechRecognizer/IWSpeechRecognizerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    .line 251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResults([Ljava/lang/String;)V
    .locals 5
    .param p1, "arg0"    # [Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 256
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$0(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->getIntBargeInResult()I

    move-result v0

    .line 258
    .local v0, "result":I
    const-string/jumbo v1, "PrepareDial"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "bargein result : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 261
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$0(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->stopBargeIn()V

    .line 263
    packed-switch v0, :pswitch_data_0

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 266
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$4(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;I)V

    .line 267
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->timer:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 269
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 270
    const-string/jumbo v1, "CM23"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 272
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneNumberSize:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$18(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)I

    move-result v1

    if-le v1, v4, :cond_1

    .line 273
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mIsFromMsgReader:Z
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$19(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 274
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    .line 275
    # invokes: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->startNumberModePhoneActivity()V
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$20(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)V

    .line 277
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    invoke-static {v1, v4}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$21(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;Z)V

    .line 279
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->finish()V

    goto :goto_0

    .line 263
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
