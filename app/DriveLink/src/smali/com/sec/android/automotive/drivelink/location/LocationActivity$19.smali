.class Lcom/sec/android/automotive/drivelink/location/LocationActivity$19;
.super Ljava/lang/Object;
.source "LocationActivity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationActivity;->sortLocationList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$19;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    .line 1444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;)I
    .locals 3
    .param p1, "lhs"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    .param p2, "rhs"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;

    .prologue
    const/4 v0, -0x1

    .line 1449
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "location_home"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1514
    :cond_0
    :goto_0
    return v0

    .line 1453
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "location_office"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1454
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "location_home"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1458
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    move-result-object v1

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->MY_PLACE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    if-ne v1, v2, :cond_4

    .line 1459
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v1

    .line 1460
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidCoords()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1461
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    move-result-object v1

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->MY_PLACE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    if-eq v1, v2, :cond_4

    .line 1462
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v1

    .line 1463
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidCoords()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1464
    :cond_3
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "location_office"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1465
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "location_home"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1466
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "google_map"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1470
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    move-result-object v1

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->MY_PLACE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    if-ne v1, v2, :cond_6

    .line 1471
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v1

    .line 1472
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidCoords()Z

    move-result v1

    if-nez v1, :cond_6

    .line 1476
    :cond_5
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "location_office"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1477
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "location_home"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1478
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "google_map"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1482
    :cond_6
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "google_map"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1483
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "location_home"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1484
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "location_office"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1514
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;

    check-cast p2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$19;->compare(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;)I

    move-result v0

    return v0
.end method
