.class Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$7;
.super Ljava/lang/Object;
.source "NotificationItem.java"

# interfaces
.implements Lcom/sec/android/app/IWSpeechRecognizer/IWSpeechRecognizerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$7;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .line 1073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResults([Ljava/lang/String;)V
    .locals 4
    .param p1, "arg0"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1078
    const-string/jumbo v1, "[NotificationItem]"

    const-string/jumbo v2, "Barge In Recognizer End."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1079
    const/4 v0, -0x1

    .line 1080
    .local v0, "result":I
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$7;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    if-eqz v1, :cond_0

    .line 1081
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$7;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v1}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->stopBargeIn()V

    .line 1082
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$7;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v1}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->getIntBargeInResult()I

    move-result v0

    .line 1083
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$7;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iput-object v3, v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    .line 1086
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$7;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mCurrentListener:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$BargeInResultListener;

    if-eqz v1, :cond_1

    .line 1087
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$7;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mCurrentListener:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$BargeInResultListener;

    invoke-interface {v1, v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$BargeInResultListener;->onResult(I)V

    .line 1088
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$7;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iput-object v3, v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mCurrentListener:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$BargeInResultListener;

    .line 1091
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$7;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->nextCmdHandler:Landroid/os/Handler;

    if-eqz v1, :cond_2

    .line 1092
    const-string/jumbo v1, "[NotificationItem]"

    .line 1093
    const-string/jumbo v2, "call nextCmdHandler <= IWSpeechRecognizerListener onResults"

    .line 1092
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1094
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$7;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->nextCmdHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1096
    :cond_2
    return-void
.end method
