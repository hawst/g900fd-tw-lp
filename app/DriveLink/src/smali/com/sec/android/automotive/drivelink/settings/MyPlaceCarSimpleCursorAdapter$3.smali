.class Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$3;
.super Ljava/lang/Object;
.source "MyPlaceCarSimpleCursorAdapter.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->showDisconnectDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    .line 306
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v3, 0x0

    .line 309
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->pendingTasks:Ljava/util/Queue;

    .line 310
    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;

    .line 311
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->disconnectBtAddress:Ljava/lang/String;

    .line 310
    invoke-direct {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 313
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->pendingTasks:Ljava/util/Queue;

    .line 314
    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    .line 315
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    # invokes: Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->doNextTask()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->access$1(Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;)V

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    iput-boolean v3, v0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->isDialogOpen:Z

    .line 318
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->access$0(Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    move-result-object v0

    .line 319
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->refreshList()V

    .line 320
    return-void
.end method
