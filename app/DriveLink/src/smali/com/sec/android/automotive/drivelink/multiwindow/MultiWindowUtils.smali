.class public Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;
.super Ljava/lang/Object;
.source "MultiWindowUtils.java"


# static fields
.field public static final DRIVELINK_MULTIMODE_PAUSE:I = 0x0

.field public static final DRIVELINK_MULTIMODE_RESUME:I = 0x1

.field public static final SETTING_DRIVLINK_MULTI:Ljava/lang/String; = "drive_link_multi_setting"

.field private static final TAG:Ljava/lang/String; = "[MultiWindowUtils]"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addNotiVoiceUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V
    .locals 2
    .param p0, "viv"    # Lcom/nuance/drivelink/DLUiUpdater;

    .prologue
    .line 57
    if-eqz p0, :cond_0

    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    if-nez v0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/nuance/drivelink/DLAppUiUpdater;->hasNotiVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/nuance/drivelink/DLAppUiUpdater;->addNotiVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 63
    const-string/jumbo v0, "[MultiWindowUtils]"

    const-string/jumbo v1, "Add Noti. Voice Listener"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static moveOutOfHomeOverlayService(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 140
    if-nez p0, :cond_0

    .line 141
    const-string/jumbo v1, "[MultiWindowUtils]"

    const-string/jumbo v2, "Context is null."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :goto_0
    return-void

    .line 144
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 145
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.automotive.drivelink.ACTION_MOVE_OUTOF_MULTI"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 146
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public static removeAllNotiVoiceUpdater()V
    .locals 2

    .prologue
    .line 79
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    if-nez v0, :cond_0

    .line 85
    :goto_0
    return-void

    .line 83
    :cond_0
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeNotiAllVoiceUiUpdater()V

    .line 84
    const-string/jumbo v0, "[MultiWindowUtils]"

    const-string/jumbo v1, "Remove All Voice Listener"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static removeNotiVoiceUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V
    .locals 2
    .param p0, "viv"    # Lcom/nuance/drivelink/DLUiUpdater;

    .prologue
    .line 68
    if-eqz p0, :cond_0

    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    if-nez v0, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/nuance/drivelink/DLAppUiUpdater;->hasNotiVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeNotiVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 74
    const-string/jumbo v0, "[MultiWindowUtils]"

    const-string/jumbo v1, "Remove Voice Listener"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setMultiModeFullSize(Landroid/content/Context;Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "multiWindowActivity"    # Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .prologue
    .line 42
    const/4 v2, 0x1

    .line 43
    .local v2, "rtn":Z
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 44
    invoke-virtual {p1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 45
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 46
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    new-instance v1, Landroid/graphics/Point;

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-direct {v1, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    .line 47
    .local v1, "point":Landroid/graphics/Point;
    const-string/jumbo v3, "[MultiWindowUtils]"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "setMultiModeFullSize:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v1, Landroid/graphics/Point;->x:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    invoke-virtual {p1, v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setIsolatedCenterPoint(Landroid/graphics/Point;)V

    .line 53
    .end local v0    # "metrics":Landroid/util/DisplayMetrics;
    .end local v1    # "point":Landroid/graphics/Point;
    :goto_0
    return v2

    .line 50
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static setMultiModeSize(Landroid/content/Context;Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;II)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "multiWindowActivity"    # Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .param p2, "centerDimenXId"    # I
    .param p3, "centerDimenYId"    # I

    .prologue
    .line 24
    const/4 v1, 0x1

    .line 26
    .local v1, "rtn":Z
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 27
    invoke-virtual {p1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 28
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 29
    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 30
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 28
    invoke-direct {v0, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 31
    .local v0, "point":Landroid/graphics/Point;
    const-string/jumbo v2, "[MultiWindowUtils]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "setMultiModeSize:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v0, Landroid/graphics/Point;->x:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setIsolatedCenterPoint(Landroid/graphics/Point;)V

    .line 37
    .end local v0    # "point":Landroid/graphics/Point;
    :goto_0
    return v1

    .line 34
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static setMultiWindowState(Landroid/content/Context;I)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "state"    # I

    .prologue
    .line 106
    const/4 v1, 0x0

    .line 108
    .local v1, "oldVal":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 109
    const-string/jumbo v3, "drive_link_multi_setting"

    .line 108
    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 113
    :goto_0
    const-string/jumbo v2, "[MultiWindowUtils]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "setMultiWindowState - old:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", new:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    if-ne v1, p1, :cond_0

    .line 116
    const-string/jumbo v2, "[MultiWindowUtils]"

    const-string/jumbo v3, "state isn\'t changed. return"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    :goto_1
    return-void

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 120
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 134
    const-string/jumbo v2, "[MultiWindowUtils]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "SETTING_DRIVLINK_MULTI is set to = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 122
    :pswitch_0
    const-string/jumbo v2, "[MultiWindowUtils]"

    const-string/jumbo v3, "SETTING_DRIVLINK_MULTI is set to 1"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 124
    const-string/jumbo v3, "drive_link_multi_setting"

    const/4 v4, 0x1

    .line 123
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    .line 128
    :pswitch_1
    const-string/jumbo v2, "[MultiWindowUtils]"

    const-string/jumbo v3, "SETTING_DRIVLINK_MULTI is set to 0"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 130
    const-string/jumbo v3, "drive_link_multi_setting"

    const/4 v4, 0x0

    .line 129
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    .line 120
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static setMultiWindowTrayHide(Landroid/view/Window;)V
    .locals 3
    .param p0, "window"    # Landroid/view/Window;

    .prologue
    .line 88
    if-eqz p0, :cond_0

    .line 89
    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 91
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    .line 92
    invoke-virtual {p0, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 96
    .end local v0    # "lp":Landroid/view/WindowManager$LayoutParams;
    :goto_0
    return-void

    .line 94
    :cond_0
    const-string/jumbo v1, "[MultiWindowUtils]"

    const-string/jumbo v2, "window is null"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
