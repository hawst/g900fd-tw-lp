.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;
.super Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialogFragment;
.source "NotificationPlaceOnDisclaimerDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;
    }
.end annotation


# static fields
.field public static TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

.field private mLocationListner:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

.field private mOnClickListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;

.field private mShared:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;

    .line 22
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 21
    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->TAG:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialogFragment;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mLocationListner:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    .line 19
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mShared:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mOnClickListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;)Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mLocationListner:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 70
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 76
    :try_start_0
    check-cast p1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mOnClickListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$2;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mLocationListner:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    .line 105
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mLocationListner:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->addLocationReqListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V

    .line 106
    :goto_0
    return-void

    .line 77
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Ljava/lang/ClassCastException;
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Activity don\u00b4t implement listener."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 38
    const v2, 0x7f0a0346

    .line 39
    const v3, 0x7f0a0345

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;-><init>(Landroid/content/Context;II)V

    .line 37
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    .line 40
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mContext:Landroid/content/Context;

    .line 42
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mShared:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    .line 44
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    const v1, 0x7f0a0343

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->setTitle(I)V

    .line 45
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    const v1, 0x7f0a0344

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->setMessage(I)V

    .line 47
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->setOnClickListener(Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog$BasicDialogListener;)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public setOnClickListener(Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mOnClickListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;

    .line 110
    return-void
.end method
