.class public Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;
.super Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
.source "UplusNavigationAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UPlusNavigationAdapter"


# instance fields
.field private final UPLUS_NAVI_EXTRA_KIND:Ljava/lang/String;

.field private final UPLUS_NAVI_EXTRA_VALUE:Ljava/lang/String;

.field private final UPLUS_NAVI_INTENT:Ljava/lang/String;

.field private final UPLUS_NAVI_PACKAGE:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field public navi_resul:Landroid/content/ComponentName;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->navi_resul:Landroid/content/ComponentName;

    .line 28
    const-string/jumbo v0, "com.mnsoft.lgunavi"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->UPLUS_NAVI_PACKAGE:Ljava/lang/String;

    .line 29
    const-string/jumbo v0, "com.mnsoft.mappy.MAPPYSMART_EXTERNAL_SERVICE"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->UPLUS_NAVI_INTENT:Ljava/lang/String;

    .line 30
    const-string/jumbo v0, "com.mnsoft.mappy.extra.EXTRA_VALUE"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->UPLUS_NAVI_EXTRA_VALUE:Ljava/lang/String;

    .line 31
    const-string/jumbo v0, "com.mnsoft.mappy.extra.EXTRA_KIND"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->UPLUS_NAVI_EXTRA_KIND:Ljava/lang/String;

    .line 36
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->setNeedOrigin(Z)V

    .line 37
    return-void
.end method


# virtual methods
.method public getNaviPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210
    const-string/jumbo v0, "com.mnsoft.lgunavi"

    return-object v0
.end method

.method public getNavigationIntent(DDLjava/lang/String;)Landroid/content/Intent;
    .locals 7
    .param p1, "lat"    # D
    .param p3, "lng"    # D
    .param p5, "address"    # Ljava/lang/String;

    .prologue
    const/high16 v6, 0x10000

    const/4 v5, 0x0

    const-wide/16 v3, 0x0

    .line 44
    const/4 v1, 0x0

    .line 45
    .local v1, "navigationIntent":Landroid/content/Intent;
    cmpl-double v2, p1, v3

    if-eqz v2, :cond_0

    cmpl-double v2, p3, v3

    if-eqz v2, :cond_0

    .line 46
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "navigationIntent":Landroid/content/Intent;
    const-string/jumbo v2, "com.mnsoft.mappy.MAPPYSMART_EXTERNAL_SERVICE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 48
    .restart local v1    # "navigationIntent":Landroid/content/Intent;
    const-string/jumbo v2, "com.mnsoft.mappy.action.REQUEST_TO_MAPPYSMART"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 50
    const-string/jumbo v2, "com.mnsoft.mappy.extra.EXTRA_KIND"

    const-string/jumbo v3, "lonlat"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    const/4 v2, 0x2

    new-array v0, v2, [D

    aput-wide p1, v0, v5

    const/4 v2, 0x1

    aput-wide p3, v0, v2

    .line 52
    .local v0, "location":[D
    const-string/jumbo v2, "com.mnsoft.mappy.extra.EXTRA_VALUE"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[D)Landroid/content/Intent;

    .line 67
    .end local v0    # "location":[D
    :goto_0
    return-object v1

    .line 54
    :cond_0
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "navigationIntent":Landroid/content/Intent;
    const-string/jumbo v2, "com.mnsoft.mappy.MAPPYSMART_EXTERNAL_SERVICE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 56
    .restart local v1    # "navigationIntent":Landroid/content/Intent;
    const-string/jumbo v2, "com.mnsoft.mappy.action.REQUEST_TO_MAPPYSMART"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 58
    const-string/jumbo v2, "com.mnsoft.mappy.extra.EXTRA_KIND"

    const-string/jumbo v3, "poi"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    invoke-virtual {p5}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x64

    if-lt v2, v3, :cond_1

    .line 60
    const-string/jumbo v2, "com.mnsoft.mappy.extra.EXTRA_VALUE"

    .line 61
    const/16 v3, 0x63

    invoke-virtual {p5, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 60
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 63
    :cond_1
    const-string/jumbo v2, "com.mnsoft.mappy.extra.EXTRA_VALUE"

    invoke-virtual {v1, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public isNaviAppSupportedCarmode(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    .line 104
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 105
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v0, 0x0

    .line 107
    .local v0, "app_supported":Z
    const/16 v3, 0x80

    :try_start_0
    invoke-virtual {v2, p2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget v3, v3, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    .line 108
    const/4 v0, 0x1

    .line 112
    :cond_0
    :goto_0
    return v0

    .line 109
    :catch_0
    move-exception v1

    .line 110
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startMap(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    .line 73
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->startMap(Landroid/content/Context;)V

    .line 74
    if-eqz p1, :cond_0

    .line 75
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    .line 80
    :goto_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "com.mnsoft.lgunavi"

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 82
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    .line 83
    const v5, 0x7f0a061c

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 85
    .local v1, "mTts":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    .line 86
    const-string/jumbo v5, "com.mnsoft.lgunavi"

    const-string/jumbo v6, ""

    .line 85
    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    .local v3, "multiData":Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v4, v7, v1, v3}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->startMultiWindowWithIndicator(Landroid/content/Context;ZLjava/lang/String;Landroid/os/Parcelable;)V

    .line 101
    .end local v1    # "mTts":Ljava/lang/String;
    .end local v3    # "multiData":Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;
    :goto_1
    return-void

    .line 77
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    goto :goto_0

    .line 91
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 90
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v4

    .line 93
    const-string/jumbo v5, "PREF_SETTINGS_MY_NAVIGATION"

    .line 92
    invoke-virtual {v4, v5, v7}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v2

    .line 95
    .local v2, "mapID":I
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->getInstallNavigationApp(I)Landroid/content/Intent;

    move-result-object v0

    .line 96
    .local v0, "goToMarket":Landroid/content/Intent;
    if-eqz v0, :cond_2

    .line 97
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 99
    :cond_2
    const-string/jumbo v4, "Error U+ Navi"

    const-string/jumbo v5, "Error on starting U+ Navigation Intent"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public startMultiNavi(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;)Z
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pacel"    # Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    .prologue
    const v12, 0x10208000

    .line 116
    const/4 v11, 0x1

    .line 117
    .local v11, "ret":Z
    const/4 v6, 0x0

    .line 119
    .local v6, "START_AS_SERVICE":Z
    if-eqz p1, :cond_0

    .line 120
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    .line 125
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "com.mnsoft.lgunavi"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 126
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "com.mnsoft.lgunavi"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->isNaviAppSupportedCarmode(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 127
    iget v0, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->isStartMap:I

    if-nez v0, :cond_1

    .line 128
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    .line 130
    .local v10, "pm":Landroid/content/pm/PackageManager;
    const-string/jumbo v0, "com.mnsoft.lgunavi"

    invoke-virtual {v10, v0}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    .line 135
    .local v9, "navigationIntent":Landroid/content/Intent;
    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->setMultiWindowIntentToNavi(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v9

    .line 137
    invoke-virtual {v9, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 140
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v9}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 176
    .end local v9    # "navigationIntent":Landroid/content/Intent;
    .end local v10    # "pm":Landroid/content/pm/PackageManager;
    :goto_1
    return v11

    .line 122
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    goto :goto_0

    .line 142
    :cond_1
    iget-wide v1, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLat:D

    .line 143
    iget-wide v3, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLng:D

    iget-object v5, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mTts:Ljava/lang/String;

    move-object v0, p0

    .line 142
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->getNavigationIntent(DDLjava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    .line 145
    .restart local v9    # "navigationIntent":Landroid/content/Intent;
    if-eqz v6, :cond_2

    .line 146
    const/high16 v0, 0x10000000

    invoke-virtual {v9, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 147
    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->setMultiWindowIntentToNavi(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v9

    .line 150
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v9}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_1

    .line 152
    :cond_2
    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->setMultiWindowIntentToNavi(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v9

    .line 155
    invoke-virtual {v9, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 158
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v9}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 164
    .end local v9    # "navigationIntent":Landroid/content/Intent;
    :cond_3
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 163
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 166
    const-string/jumbo v1, "PREF_SETTINGS_MY_NAVIGATION"

    .line 167
    const/4 v2, 0x0

    .line 165
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v8

    .line 168
    .local v8, "mapID":I
    invoke-virtual {p0, v8}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->getInstallNavigationApp(I)Landroid/content/Intent;

    move-result-object v7

    .line 169
    .local v7, "goToMarket":Landroid/content/Intent;
    if-eqz v7, :cond_4

    .line 170
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v7}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 172
    :cond_4
    const-string/jumbo v0, "Error U+ Navi"

    const-string/jumbo v1, "Error on starting U+ Navigation Intent"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public startNavigation(Landroid/content/Context;DDLjava/lang/String;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "lat"    # D
    .param p4, "lng"    # D
    .param p6, "tts"    # Ljava/lang/String;

    .prologue
    .line 182
    if-eqz p1, :cond_0

    .line 183
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    .line 188
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "com.mnsoft.lgunavi"

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 189
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "com.mnsoft.lgunavi"

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->isNaviAppSupportedCarmode(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 190
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;-><init>(Landroid/content/Context;DDLjava/lang/String;)V

    .line 192
    .local v0, "multiData":Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, p6, v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->startMultiWindowWithIndicator(Landroid/content/Context;ZLjava/lang/String;Landroid/os/Parcelable;)V

    .line 206
    .end local v0    # "multiData":Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;
    :goto_1
    return-void

    .line 185
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    goto :goto_0

    .line 196
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 195
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 198
    const-string/jumbo v2, "PREF_SETTINGS_MY_NAVIGATION"

    .line 199
    const/4 v3, 0x0

    .line 197
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v8

    .line 200
    .local v8, "mapID":I
    invoke-virtual {p0, v8}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->getInstallNavigationApp(I)Landroid/content/Intent;

    move-result-object v7

    .line 201
    .local v7, "goToMarket":Landroid/content/Intent;
    if-eqz v7, :cond_2

    .line 202
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v7}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 204
    :cond_2
    const-string/jumbo v1, "Error U+ Navi"

    const-string/jumbo v2, "Error on starting U+ Navigation Intent"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
