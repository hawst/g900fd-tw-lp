.class public Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "SuggestionsLocationPageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static final ITEM_NUM_PER_PAGE:I = 0x4

.field private static final MAX_PAGE_TO_LOOP:I = 0x4e20

.field private static isNightMode:Z


# instance fields
.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mInitialPosition:I

.field private mPageCount:I

.field private mSuggestionsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->isNightMode:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "clickListener"    # Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mContext:Landroid/content/Context;

    .line 41
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    .line 42
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->getPageCount()I

    move-result v0

    .line 44
    .local v0, "pageCount":I
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 46
    const/16 v1, 0x2710

    rem-int/2addr v1, v0

    rsub-int v1, v1, 0x2710

    .line 45
    iput v1, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mInitialPosition:I

    .line 47
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mPageCount:I

    .line 52
    :goto_0
    return-void

    .line 49
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mInitialPosition:I

    .line 50
    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mPageCount:I

    goto :goto_0
.end method

.method public static setDayMode()V
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->isNightMode:Z

    .line 206
    return-void
.end method

.method public static setNightMode()V
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->isNightMode:Z

    .line 202
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 166
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 167
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mPageCount:I

    return v0
.end method

.method public getInitialPosition()I
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mInitialPosition:I

    return v0
.end method

.method public getPageCount()I
    .locals 3

    .prologue
    .line 175
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    div-int/lit8 v0, v2, 0x4

    .line 176
    .local v0, "page":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    rem-int/lit8 v1, v2, 0x4

    .line 177
    .local v1, "remainder":I
    if-lez v1, :cond_0

    .line 178
    add-int/lit8 v0, v0, 0x1

    .line 181
    :cond_0
    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 13
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 56
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mContext:Landroid/content/Context;

    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 57
    .local v6, "layoutInflater":Landroid/view/LayoutInflater;
    const v9, 0x7f030093

    const/4 v10, 0x0

    invoke-virtual {v6, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 59
    .local v8, "v":Landroid/view/View;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 60
    .local v7, "listItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/widget/RelativeLayout;>;"
    const v9, 0x7f090207

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    const v9, 0x7f090208

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    const v9, 0x7f090209

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    const v9, 0x7f09020a

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->getPageCount()I

    move-result v9

    rem-int v9, p2, v9

    mul-int/lit8 v4, v9, 0x4

    .line 70
    .local v4, "index":I
    const/4 v3, 0x1

    .line 71
    .local v3, "i":I
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_0

    .line 159
    invoke-virtual {v8, p2}, Landroid/view/View;->setId(I)V

    .line 160
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    invoke-virtual {p1, v8}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    .line 161
    return-object v8

    .line 71
    .restart local p1    # "container":Landroid/view/ViewGroup;
    :cond_0
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    .line 72
    .local v5, "layout":Landroid/widget/RelativeLayout;
    new-instance v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;

    const/4 v9, 0x0

    invoke-direct {v2, p0, v9}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;-><init>(Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;)V

    .line 75
    .local v2, "holder":Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;
    const v9, 0x7f0901f7

    invoke-virtual {v5, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 74
    iput-object v9, v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    .line 77
    const v9, 0x7f09028f

    invoke-virtual {v5, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 76
    iput-object v9, v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;->ivContactMaskImage:Landroid/widget/ImageView;

    .line 79
    const v9, 0x7f0901f8

    invoke-virtual {v5, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 78
    iput-object v9, v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    .line 81
    const v9, 0x7f090290

    invoke-virtual {v5, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 80
    iput-object v9, v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;->tvNumber:Landroid/widget/TextView;

    .line 83
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-le v9, v4, :cond_6

    .line 84
    invoke-virtual {v5, v3}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 85
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v9}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 88
    .local v1, "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mContext:Landroid/content/Context;

    .line 89
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 88
    invoke-interface {v1, v11, v9}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 90
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 91
    const v11, 0x7f02023b

    .line 90
    invoke-static {v9, v0, v11}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 93
    if-nez v0, :cond_1

    .line 94
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 95
    const v11, 0x7f02023d

    .line 96
    const v12, 0x7f02023b

    .line 94
    invoke-static {v9, v11, v12}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 99
    :cond_1
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 100
    const v11, 0x7f02023c

    .line 99
    invoke-static {v9, v0, v11}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageOverlay(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 101
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 103
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    const/4 v11, 0x2

    if-ne v9, v11, :cond_4

    .line 104
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    const/4 v11, 0x1

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setLines(I)V

    .line 109
    :goto_1
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 110
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v9

    .line 111
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_2

    .line 112
    iget-object v11, v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;->tvNumber:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 113
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v9

    const/4 v12, 0x0

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v9

    .line 112
    invoke-virtual {v11, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    :cond_2
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v11, ""

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 117
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_3

    .line 118
    iget-object v11, v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 119
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v9

    .line 118
    invoke-virtual {v11, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    :cond_3
    sget-boolean v9, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->isNightMode:Z

    if-eqz v9, :cond_5

    .line 127
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;->ivContactMaskImage:Landroid/widget/ImageView;

    .line 128
    const v11, 0x7f02020a

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 129
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v11

    .line 130
    invoke-virtual {v11}, Lcom/sec/android/automotive/drivelink/DLApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 131
    const v12, 0x7f080030

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    .line 129
    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setTextColor(I)V

    .line 132
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;->tvNumber:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v11

    .line 133
    invoke-virtual {v11}, Lcom/sec/android/automotive/drivelink/DLApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 134
    const v12, 0x7f080030

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    .line 132
    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setTextColor(I)V

    .line 135
    const v9, 0x7f08002e

    invoke-virtual {v5, v9}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 155
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    :goto_2
    add-int/lit8 v4, v4, 0x1

    .line 156
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 106
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v1    # "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    :cond_4
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    const/4 v11, 0x2

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setLines(I)V

    goto/16 :goto_1

    .line 137
    :cond_5
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;->ivContactMaskImage:Landroid/widget/ImageView;

    .line 138
    const v11, 0x7f020209

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 139
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v11

    .line 140
    invoke-virtual {v11}, Lcom/sec/android/automotive/drivelink/DLApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 141
    const v12, 0x7f08002f

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    .line 139
    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setTextColor(I)V

    .line 142
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;->tvNumber:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v11

    .line 143
    invoke-virtual {v11}, Lcom/sec/android/automotive/drivelink/DLApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 144
    const v12, 0x7f08002f

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    .line 142
    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setTextColor(I)V

    .line 145
    const v9, 0x7f02024f

    invoke-virtual {v5, v9}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_2

    .line 149
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    :cond_6
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    const/16 v11, 0x8

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 150
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;->ivContactMaskImage:Landroid/widget/ImageView;

    const/16 v11, 0x8

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 151
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    const/16 v11, 0x8

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 152
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter$ViewHolder;->tvNumber:Landroid/widget/TextView;

    const/16 v11, 0x8

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 186
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setContactList(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 209
    .local p1, "contacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    .line 211
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->getPageCount()I

    move-result v0

    .line 212
    .local v0, "pageCount":I
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 214
    const/16 v1, 0x2710

    rem-int/2addr v1, v0

    rsub-int v1, v1, 0x2710

    .line 213
    iput v1, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mInitialPosition:I

    .line 215
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mPageCount:I

    .line 220
    :goto_0
    return-void

    .line 217
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mInitialPosition:I

    .line 218
    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->mPageCount:I

    goto :goto_0
.end method
