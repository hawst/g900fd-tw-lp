.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$1;
.super Landroid/os/Handler;
.source "MessageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 417
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 420
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 422
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 430
    :cond_0
    :goto_0
    return-void

    .line 424
    :pswitch_0
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/drivelink/DLAppUiUpdater;->isTTSPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 425
    const-string/jumbo v0, "MessageActivity"

    const-string/jumbo v1, "MSG_AUTO_SHRINK - finish "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->finish()V

    goto :goto_0

    .line 422
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
