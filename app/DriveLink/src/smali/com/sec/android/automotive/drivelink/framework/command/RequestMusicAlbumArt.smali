.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicAlbumArt;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestMusicAlbumArt.java"


# instance fields
.field private mAlbumArt:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field private mDLMusic:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicAlbumArt;->mContext:Landroid/content/Context;

    .line 14
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicAlbumArt;->mDLMusic:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 15
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicAlbumArt;->mAlbumArt:Landroid/graphics/Bitmap;

    .line 21
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicAlbumArt;->mContext:Landroid/content/Context;

    .line 22
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicAlbumArt;->mDLMusic:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 23
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicAlbumArt;->mContext:Landroid/content/Context;

    .line 49
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicAlbumArt;->mDLMusic:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 50
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicAlbumArt;->mAlbumArt:Landroid/graphics/Bitmap;

    .line 51
    return-void
.end method

.method protected process()V
    .locals 3

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicAlbumArt;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 28
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 29
    .local v0, "contentManager":Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicAlbumArt;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicAlbumArt;->mDLMusic:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicAlbumArt;->mAlbumArt:Landroid/graphics/Bitmap;

    .line 30
    return-void
.end method

.method protected response()V
    .locals 3

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicAlbumArt;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 35
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

    move-result-object v0

    .line 36
    .local v0, "musicListener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicAlbumArt;->mDLMusic:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicAlbumArt;->mAlbumArt:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setAlbumArt(Landroid/graphics/Bitmap;)V

    .line 37
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicAlbumArt;->mDLMusic:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setCachedFlag(Z)V

    .line 39
    if-eqz v0, :cond_0

    .line 40
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicAlbumArt;->mAlbumArt:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 41
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicAlbumArt;->mDLMusic:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;->onResponseRequestMusicAlbumArt(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    .line 44
    :cond_0
    return-void
.end method
