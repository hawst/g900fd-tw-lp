.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
.super Ljava/lang/Object;
.source "DLPlaceONProfile.java"


# static fields
.field public static final KEY_BIRTHDAY:Ljava/lang/String; = "birthday"

.field public static final KEY_COUNTRY_CODE:Ljava/lang/String; = "countryCode"

.field public static final KEY_DEVICE_ID:Ljava/lang/String; = "deviceId"

.field public static final KEY_EMAIL:Ljava/lang/String; = "email"

.field public static final KEY_EXP_TIME:Ljava/lang/String; = "expiredTime"

.field public static final KEY_GENDER:Ljava/lang/String; = "gender"

.field public static final KEY_MODEL:Ljava/lang/String; = "model"

.field public static final KEY_PHONENUMBER:Ljava/lang/String; = "phoneNumber"

.field public static final KEY_PUSH_ID:Ljava/lang/String; = "pushId"

.field public static final KEY_SSPGUID:Ljava/lang/String; = "sspGuid"

.field public static final KEY_USERNAME:Ljava/lang/String; = "userName"

.field public static final KEY_USER_ID:Ljava/lang/String; = "userId"


# instance fields
.field private mBirthday:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "birthday"
    .end annotation
.end field

.field private mCountryCode:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "countryCode"
    .end annotation
.end field

.field private mDeviceId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "deviceId"
    .end annotation
.end field

.field private mEmail:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "email"
    .end annotation
.end field

.field private mExpiredTime:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "expiredTime"
    .end annotation
.end field

.field private mGender:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "gender"
    .end annotation
.end field

.field private mModel:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "model"
    .end annotation
.end field

.field private mPhoneNumber:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "phoneNumber"
    .end annotation
.end field

.field private mPushId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pushId"
    .end annotation
.end field

.field private mSspGuid:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "sspGuid"
    .end annotation
.end field

.field private mUserId:I
    .annotation runtime Lcom/google/gson/annotations/Expose;
        deserialize = true
        serialize = false
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "userId"
    .end annotation
.end field

.field private mUserName:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "userName"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mUserId:I

    .line 59
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mSspGuid:Ljava/lang/String;

    .line 60
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mUserName:Ljava/lang/String;

    .line 61
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mPhoneNumber:Ljava/lang/String;

    .line 62
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mGender:Ljava/lang/String;

    .line 63
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mBirthday:Ljava/lang/String;

    .line 64
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mEmail:Ljava/lang/String;

    .line 65
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mCountryCode:Ljava/lang/String;

    .line 66
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mExpiredTime:Ljava/lang/String;

    .line 67
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mPushId:Ljava/lang/String;

    .line 68
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mModel:Ljava/lang/String;

    .line 69
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mDeviceId:Ljava/lang/String;

    .line 70
    return-void
.end method


# virtual methods
.method public getBirthday()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mBirthday:Ljava/lang/String;

    return-object v0
.end method

.method public getCountryCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mCountryCode:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mEmail:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiredTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mExpiredTime:Ljava/lang/String;

    return-object v0
.end method

.method public getGender()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mGender:Ljava/lang/String;

    return-object v0
.end method

.method public getModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mModel:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getPushId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mPushId:Ljava/lang/String;

    return-object v0
.end method

.method public getSspGuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mSspGuid:Ljava/lang/String;

    return-object v0
.end method

.method public getUserId()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mUserId:I

    return v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mUserName:Ljava/lang/String;

    return-object v0
.end method

.method public setBirthday(Ljava/lang/String;)V
    .locals 0
    .param p1, "mBirthday"    # Ljava/lang/String;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mBirthday:Ljava/lang/String;

    .line 126
    return-void
.end method

.method public setCountryCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "mCoutryCode"    # Ljava/lang/String;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mCountryCode:Ljava/lang/String;

    .line 142
    return-void
.end method

.method public setDeviceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mDeviceId:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0
    .param p1, "mEmail"    # Ljava/lang/String;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mEmail:Ljava/lang/String;

    .line 134
    return-void
.end method

.method public setExpiredTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "mExpiredTime"    # Ljava/lang/String;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mExpiredTime:Ljava/lang/String;

    .line 150
    return-void
.end method

.method public setGender(Ljava/lang/String;)V
    .locals 0
    .param p1, "mGender"    # Ljava/lang/String;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mGender:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public setModel(Ljava/lang/String;)V
    .locals 0
    .param p1, "mModel"    # Ljava/lang/String;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mModel:Ljava/lang/String;

    .line 166
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPhoneNumber"    # Ljava/lang/String;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mPhoneNumber:Ljava/lang/String;

    .line 110
    return-void
.end method

.method public setPushId(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPushId"    # Ljava/lang/String;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mPushId:Ljava/lang/String;

    .line 158
    return-void
.end method

.method public setSspGuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "mSspGuid"    # Ljava/lang/String;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mSspGuid:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public setUserId(I)V
    .locals 0
    .param p1, "mUserId"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mUserId:I

    .line 86
    return-void
.end method

.method public setUserName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mUserName"    # Ljava/lang/String;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->mUserName:Ljava/lang/String;

    .line 102
    return-void
.end method
