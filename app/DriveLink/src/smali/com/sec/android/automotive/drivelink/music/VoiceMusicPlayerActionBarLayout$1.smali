.class Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$1;
.super Landroid/os/Handler;
.source "VoiceMusicPlayerActionBarLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    .line 699
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 703
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->getMicState()Lcom/nuance/sample/MicState;

    move-result-object v0

    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsMicDisplayed:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->access$0(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 704
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->access$1(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 705
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->access$2(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 706
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->access$3(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 707
    # getter for: Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->access$4()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "mPhraseSpotterHandler mStandByLayout visible"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    :cond_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 710
    return-void
.end method
