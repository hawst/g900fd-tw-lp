.class public Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;
.super Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;
.source "LocationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;
    }
.end annotation


# static fields
.field private static final MYPLACE_NAME:Ljava/lang/String; = "location_myplace"

.field private static final SENT:Ljava/lang/String; = "SMS_SENT"

.field private static final TAG:Ljava/lang/String; = "LocationManager"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mUserId:I

.field private mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

.field private final smsReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mContext:Landroid/content/Context;

    .line 46
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    .line 507
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mUserId:I

    .line 1523
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->smsReceiver:Landroid/content/BroadcastReceiver;

    .line 49
    return-void
.end method

.method private InvitationAddSend(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "profile"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1475
    .local p2, "grParticipants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1497
    :goto_1
    return-void

    .line 1475
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 1476
    .local v0, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1477
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1481
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getUrl()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1482
    :cond_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v4

    .line 1483
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getLocationGroup()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    move-result-object v1

    .line 1484
    .local v1, "dbGroup":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    if-nez v1, :cond_3

    .line 1485
    const-string/jumbo v3, "LocationManager"

    const-string/jumbo v4, "Invalid buddy url from create share group."

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1488
    :cond_3
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setUrl(Ljava/lang/String;)V

    .line 1492
    .end local v1    # "dbGroup":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    :cond_4
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->createGroupShareMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1495
    .local v2, "smsContent":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p1, v4, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->sendLocationSMS(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private acceptSharedLocation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "profile"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .param p3, "friendPhoneNumber"    # Ljava/lang/String;
    .param p4, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    const/4 v2, 0x0

    .line 975
    const/4 v0, 0x0

    .line 980
    .local v0, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-virtual {p4, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipantFromPhoneNumber(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    move-result-object v0

    .line 982
    if-nez v0, :cond_1

    move-object v0, v2

    .line 1010
    .end local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    :cond_0
    :goto_0
    return-object v0

    .line 991
    .restart local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    .line 990
    invoke-virtual {p0, p1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getContactFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v1

    .line 992
    .local v1, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setContact(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    .line 994
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 995
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getFriendName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 996
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getFriendName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 997
    :cond_2
    invoke-virtual {v0, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setFriendName(Ljava/lang/String;)V

    .line 1002
    :cond_3
    :goto_1
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->ACCEPTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setStatus(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;)V

    .line 1004
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->saveLocationFriendShared(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z

    move-result v3

    if-nez v3, :cond_0

    move-object v0, v2

    .line 1007
    goto :goto_0

    .line 999
    :cond_4
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setFriendName(Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    return-object v0
.end method

.method private checkGroupFriendsContacts(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 1088
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1104
    :cond_0
    return-void

    .line 1091
    :cond_1
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 1092
    .local v0, "friend":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->setContactToLocationItem(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1098
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v2

    .line 1099
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v3

    .line 1098
    invoke-virtual {v2, v3, v0}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->saveGroupParticipant(ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z

    goto :goto_0
.end method

.method private checkLocationsContacts(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1071
    .local p2, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1085
    return-void

    .line 1071
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;

    .line 1072
    .local v1, "locationItem":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    invoke-direct {p0, p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->setContactToLocationItem(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v0, v1

    .line 1075
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 1077
    .local v0, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    const-string/jumbo v3, "LocationManager"

    const-string/jumbo v4, "Updating friend name on shared location."

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1079
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v3

    .line 1080
    invoke-virtual {v3, v0}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->saveLocationFriendShared(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z

    goto :goto_0
.end method

.method private getPlaceOnRegisteredPhoneNumber(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Ljava/lang/String;
    .locals 6
    .param p1, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    const/4 v4, 0x0

    .line 443
    const/4 v2, 0x0

    .line 444
    .local v2, "phoneNumber":Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->isValidContact(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 457
    :cond_0
    :goto_0
    return-object v4

    .line 448
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v3

    .line 451
    .local v3, "phones":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 452
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 453
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    .line 454
    .local v1, "phone":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    move-object v4, v2

    .line 455
    goto :goto_0
.end method

.method private getUserId(Landroid/content/Context;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 510
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mUserId:I

    return v0
.end method

.method private static isValidContact(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Z
    .locals 4
    .param p0, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    const/4 v0, 0x0

    .line 384
    if-nez p0, :cond_0

    .line 385
    const-string/jumbo v1, "LocationManager"

    const-string/jumbo v2, "Invalid contact with null value."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    :goto_0
    return v0

    .line 389
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 390
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 391
    :cond_1
    const-string/jumbo v1, "LocationManager"

    .line 392
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Phone number not found to contact "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 393
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 392
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 391
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 397
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static isValidContext(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 288
    if-nez p0, :cond_0

    .line 289
    const-string/jumbo v0, "LocationManager"

    const-string/jumbo v1, "Invalid context with null value."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    const/4 v0, 0x0

    .line 293
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private saveUserId(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "userId"    # I

    .prologue
    .line 516
    iput p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mUserId:I

    .line 517
    return-void
.end method

.method private sendCreatedGroupSMS(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 306
    if-nez p2, :cond_0

    .line 307
    const-string/jumbo v5, "LocationManager"

    const-string/jumbo v6, "Invalid group to send message notification."

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    :goto_0
    return-void

    .line 311
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v3

    .line 312
    .local v3, "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    invoke-virtual {p2, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getMyBuddy(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    move-result-object v2

    .line 314
    .local v2, "me":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 315
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 349
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getNotificationManager()Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    move-result-object v5

    .line 350
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->NotifyLocationGroupSharingRequestSent()V

    goto :goto_0

    .line 316
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 317
    .local v0, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    if-eq v2, v0, :cond_1

    .line 320
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 321
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 327
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupUrl()Ljava/lang/String;

    move-result-object v4

    .line 329
    .local v4, "url":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getUrl()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 330
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getUrl()Ljava/lang/String;

    move-result-object v4

    .line 332
    :cond_3
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    move-result-object v5

    sget-object v6, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;->SHARE_MYLOCATION:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    if-ne v5, v6, :cond_4

    .line 333
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    .line 334
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->createLocationShareMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 333
    invoke-direct {p0, p1, v5, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->sendLocationSMS(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getNotificationManager()Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    move-result-object v5

    .line 336
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getFriendName()Ljava/lang/String;

    move-result-object v6

    .line 335
    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->NotifyLocationShareSent(Ljava/lang/String;)V

    goto :goto_0

    .line 338
    :cond_4
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    move-result-object v5

    sget-object v6, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;->REQUEST_LOCATION:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    if-ne v5, v6, :cond_5

    .line 339
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    .line 340
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->createRequestLocationMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 339
    invoke-direct {p0, p1, v5, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->sendLocationSMS(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getNotificationManager()Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    move-result-object v5

    .line 342
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->NotifyLocationRequestSent(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    goto/16 :goto_0

    .line 345
    :cond_5
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    .line 346
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->createGroupShareMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 345
    invoke-direct {p0, p1, v5, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->sendLocationSMS(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private sendLocationSMS(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phone"    # Ljava/lang/String;
    .param p3, "smsContent"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 362
    if-eqz p3, :cond_0

    const-string/jumbo v1, ""

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 363
    :cond_0
    const-string/jumbo v1, "LocationManager"

    const-string/jumbo v2, "Message content is empty."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    :goto_0
    return-void

    .line 369
    :cond_1
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    .line 373
    .local v0, "smsManager":Landroid/telephony/SmsManager;
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v3, "SMS_SENT"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 372
    invoke-static {p1, v5, v1, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .local v4, "SentIntent":Landroid/app/PendingIntent;
    move-object v1, p2

    move-object v3, p3

    move-object v5, v2

    .line 374
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private setContactToLocationItem(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "locationItem"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;

    .prologue
    const/4 v2, 0x0

    .line 1108
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1109
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1135
    .end local p2    # "locationItem":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    :cond_0
    :goto_0
    return v2

    .line 1112
    .restart local p2    # "locationItem":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v3

    .line 1114
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getPhoneNumber()Ljava/lang/String;

    move-result-object v4

    .line 1113
    invoke-virtual {v3, p1, v4}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContactFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v1

    .line 1115
    .local v1, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-nez v1, :cond_2

    .line 1116
    check-cast p2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .end local p2    # "locationItem":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    invoke-virtual {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->checkUserIsMe(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z

    move-result v2

    goto :goto_0

    .line 1119
    .restart local p2    # "locationItem":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    :cond_2
    invoke-virtual {p2, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->setContact(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    .line 1121
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    move-result-object v3

    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->FRIENDS_SHARED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    if-eq v3, v4, :cond_3

    .line 1122
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    move-result-object v3

    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->GROUP_SHARING:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    if-ne v3, v4, :cond_0

    .line 1125
    :cond_3
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1126
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1129
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move-object v0, p2

    .line 1132
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 1134
    .local v0, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setFriendName(Ljava/lang/String;)V

    .line 1135
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public NotifyDailyCommuteEvent(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLRouteInfo;)V
    .locals 0
    .param p1, "routeInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLRouteInfo;

    .prologue
    .line 1162
    return-void
.end method

.method public acceptShareMyLocation(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)Z
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "groupIdUrl"    # Ljava/lang/String;
    .param p3, "status"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;
    .param p4, "locationShared"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p5, "duration"    # J

    .prologue
    .line 800
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 801
    const-string/jumbo v0, "LocationManager"

    .line 802
    const-string/jumbo v1, "Invalid context on \'Accept Share My Location\' function."

    .line 801
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    const/4 v0, 0x0

    .line 840
    :goto_0
    return v0

    .line 806
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 807
    :cond_1
    const-string/jumbo v0, "LocationManager"

    .line 808
    const-string/jumbo v1, "Invalid group Id or Url to \'Accept Share My Location\' function."

    .line 807
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 809
    const/4 v0, 0x0

    goto :goto_0

    .line 812
    :cond_2
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getUserProfile(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v9

    .line 814
    .local v9, "profile":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    if-nez v9, :cond_3

    .line 815
    const-string/jumbo v0, "LocationManager"

    .line 816
    const-string/jumbo v1, "Failed to \'Accept Share My Location\'. My own profile not found!"

    .line 815
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    const/4 v0, 0x0

    goto :goto_0

    .line 820
    :cond_3
    const/4 v7, 0x0

    .line 822
    .local v7, "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    const-string/jumbo v0, "http"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 823
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->getGroupByUrl(ILjava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    move-result-object v7

    .line 834
    :goto_1
    if-nez v7, :cond_5

    .line 835
    const-string/jumbo v0, "LocationManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Failed to \'Accept Share My Location\'. Invalid groupId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 836
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 835
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    const/4 v0, 0x0

    goto :goto_0

    .line 827
    :cond_4
    :try_start_0
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 828
    .local v8, "groupId":I
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v1

    invoke-virtual {v0, v1, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->getGroupById(II)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    goto :goto_1

    .line 840
    .end local v8    # "groupId":I
    :cond_5
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-wide v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->updateMyStatus(Landroid/content/Context;ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)Z

    move-result v0

    goto :goto_0

    .line 830
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public acceptSharedLocation(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "friendPhoneNumber"    # Ljava/lang/String;
    .param p3, "groupIdUrl"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 916
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->isValidContext(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 917
    const-string/jumbo v4, "LocationManager"

    .line 918
    const-string/jumbo v5, "Invalid context on \'Accepted Shared Location\' function."

    .line 917
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 962
    :goto_0
    return-object v3

    .line 922
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 923
    :cond_1
    const-string/jumbo v4, "LocationManager"

    .line 924
    const-string/jumbo v5, "Invalid contact phone number on \'Accepted Shared Location\' function."

    .line 923
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 928
    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 929
    :cond_3
    const-string/jumbo v4, "LocationManager"

    .line 930
    const-string/jumbo v5, "Invalid group Id or Url to \'Accepted Shared Location\' function."

    .line 929
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 934
    :cond_4
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getUserProfile(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v2

    .line 936
    .local v2, "profile":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    if-nez v2, :cond_5

    .line 937
    const-string/jumbo v4, "LocationManager"

    .line 938
    const-string/jumbo v5, "Failed to \'Accepted Shared Location\'. My own profile not found!"

    .line 937
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 942
    :cond_5
    const/4 v0, 0x0

    .line 944
    .local v0, "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    const-string/jumbo v4, "http"

    invoke-virtual {p3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 945
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v5

    invoke-virtual {v4, v5, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->getGroupByUrl(ILjava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    move-result-object v0

    .line 956
    :goto_1
    if-nez v0, :cond_7

    .line 957
    const-string/jumbo v4, "LocationManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Failed to \'Accepted Shared Location\'. Invalid groupId "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 958
    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 957
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 949
    :cond_6
    :try_start_0
    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 950
    .local v1, "groupId":I
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v5

    invoke-virtual {v4, v5, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->getGroupById(II)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    .line 962
    .end local v1    # "groupId":I
    :cond_7
    invoke-direct {p0, p1, v2, p2, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->acceptSharedLocation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    move-result-object v3

    goto :goto_0

    .line 952
    :catch_0
    move-exception v4

    goto :goto_1
.end method

.method public changeGroupDestination(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .param p3, "userLocation"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    const/4 v6, 0x0

    .line 1232
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 1233
    :cond_0
    const-string/jumbo v7, "LocationManager"

    const-string/jumbo v8, "Invalid changeGroupDestination parameters ... "

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1286
    :goto_0
    return v6

    .line 1237
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v7

    .line 1238
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getUserProfile()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v3

    .line 1239
    .local v3, "profile":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    if-nez v3, :cond_2

    .line 1240
    const-string/jumbo v7, "LocationManager"

    const-string/jumbo v8, "Fail to get the profile to change group destination"

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1244
    :cond_2
    const-string/jumbo v0, "xxxxxxxxxx"

    .line 1245
    .local v0, "accessToken":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getSspGuid()Ljava/lang/String;

    move-result-object v8

    .line 1246
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1245
    invoke-virtual {v7, v8, v0, v9, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->changeGroupDestination(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v7

    .line 1246
    if-nez v7, :cond_3

    .line 1247
    const-string/jumbo v7, "LocationManager"

    const-string/jumbo v8, "Fail when try to change group destination..."

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1252
    :cond_3
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v7

    .line 1253
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getLocationGroup()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    move-result-object v2

    .line 1254
    .local v2, "dbGroup":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupUrl()Ljava/lang/String;

    move-result-object v5

    .line 1256
    .local v5, "url":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_4
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_5

    .line 1280
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->requestGroupSharedUpdateInfo(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 1281
    const-string/jumbo v7, "LocationManager"

    .line 1282
    const-string/jumbo v8, "Could not restart share group, fail when try get group info"

    .line 1281
    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1256
    :cond_5
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 1257
    .local v1, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 1258
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getPhoneNumber()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 1262
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getUrl()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_6

    .line 1263
    const-string/jumbo v8, "LocationManager"

    const-string/jumbo v9, "Invalid buddy url from create share group."

    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1269
    :goto_2
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getStatus()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    move-result-object v8

    sget-object v9, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->ACCEPTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    if-ne v8, v9, :cond_4

    .line 1273
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->createGroupDestinationChangedMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1276
    .local v4, "smsContent":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, p1, v8, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->sendLocationSMS(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1266
    .end local v4    # "smsContent":Ljava/lang/String;
    :cond_6
    invoke-virtual {v1, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setUrl(Ljava/lang/String;)V

    goto :goto_2

    .line 1286
    .end local v1    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    :cond_7
    const/4 v6, 0x1

    goto/16 :goto_0
.end method

.method checkUserIsMe(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z
    .locals 4
    .param p1, "buddy"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    const/4 v1, 0x0

    .line 1139
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v2

    .line 1140
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getUserProfile()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v0

    .line 1141
    .local v0, "profile":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1151
    :cond_0
    :goto_0
    return v1

    .line 1143
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    .line 1144
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    .line 1143
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->areEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 1144
    if-eqz v2, :cond_0

    .line 1147
    const-string/jumbo v2, "Me"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getFriendName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1150
    const-string/jumbo v1, "Me"

    invoke-virtual {p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setFriendName(Ljava/lang/String;)V

    .line 1151
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public createGroupShare(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .param p3, "locationShared"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p4, "duration"    # J

    .prologue
    .line 720
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 721
    const-string/jumbo v0, "LocationManager"

    const-string/jumbo v1, "Invalid context on \'Create Group Share\' function."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 722
    const/4 v0, 0x0

    .line 783
    :goto_0
    return v0

    .line 725
    :cond_0
    if-nez p2, :cond_1

    .line 726
    const-string/jumbo v0, "LocationManager"

    const-string/jumbo v1, "Invalid group on \'Create Group Share\' function."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    const/4 v0, 0x0

    goto :goto_0

    .line 730
    :cond_1
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;->REQUEST_LOCATION:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    if-eq v0, v1, :cond_3

    .line 731
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidCoords()Z

    move-result v0

    if-nez v0, :cond_3

    .line 732
    :cond_2
    const-string/jumbo v0, "LocationManager"

    .line 733
    const-string/jumbo v1, "Failed to \'Create Group Share\'. Invalid location shared."

    .line 732
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 734
    const/4 v0, 0x0

    goto :goto_0

    .line 737
    :cond_3
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getUserProfile(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v8

    .line 739
    .local v8, "profile":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    if-nez v8, :cond_4

    .line 740
    const-string/jumbo v0, "LocationManager"

    .line 741
    const-string/jumbo v1, "Failed to \'Create Group Share\'. My own profile not found!"

    .line 740
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 742
    const/4 v0, 0x0

    goto :goto_0

    .line 745
    :cond_4
    new-instance v7, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    invoke-direct {v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;-><init>()V

    .line 746
    .local v7, "me":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setPhoneNumber(Ljava/lang/String;)V

    .line 747
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setShared(Z)V

    .line 748
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    add-long/2addr v0, p4

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setExpireTime(J)V

    .line 749
    if-eqz p3, :cond_5

    .line 750
    invoke-virtual {v7, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 752
    :cond_5
    invoke-virtual {p2, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->addGroupParticipant(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V

    .line 754
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;->SHARE_MYLOCATION:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    if-ne v0, v1, :cond_6

    .line 755
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setShareType(I)V

    .line 756
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->ACCEPTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    invoke-virtual {v7, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setStatus(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;)V

    .line 757
    const-string/jumbo v0, "LocationManager"

    const-string/jumbo v1, "Share My Location"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    :goto_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    invoke-virtual {v0, v8, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->createShareGroup(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 769
    const-string/jumbo v0, "LocationManager"

    const-string/jumbo v1, "Failed to \'Create Group Share\' on PlaceON."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 758
    :cond_6
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;->REQUEST_LOCATION:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    if-ne v0, v1, :cond_7

    .line 759
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setShareType(I)V

    .line 760
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->NOT_RESPONSED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    invoke-virtual {v7, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setStatus(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;)V

    .line 761
    const-string/jumbo v0, "LocationManager"

    const-string/jumbo v1, "Request Others Location"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 763
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setShareType(I)V

    .line 764
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->ACCEPTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    invoke-virtual {v7, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setStatus(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;)V

    .line 765
    const-string/jumbo v0, "LocationManager"

    const-string/jumbo v1, "Group Share"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 773
    :cond_8
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;->REQUEST_LOCATION:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    if-eq v0, v1, :cond_9

    .line 774
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v2

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getStatus()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move-wide v5, p4

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->updateMyStatus(Landroid/content/Context;ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)Z

    move-result v0

    .line 775
    if-nez v0, :cond_9

    .line 776
    const-string/jumbo v0, "LocationManager"

    .line 777
    const-string/jumbo v1, "Failed to \'Create Group Share\'. Failed to update my status on PlaceON."

    .line 776
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 781
    :cond_9
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->sendCreatedGroupSMS(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V

    .line 783
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public createGroupShare(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "groupType"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;
    .param p4, "locationShared"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p5, "duration"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            "J)Z"
        }
    .end annotation

    .prologue
    .line 698
    .local p2, "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;-><init>()V

    .line 700
    .local v2, "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 701
    const-wide/32 v3, 0x2932e00

    add-long/2addr v0, v3

    .line 700
    invoke-virtual {v2, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setExpireTime(J)V

    .line 702
    invoke-virtual {v2, p4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 703
    invoke-virtual {v2, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setGroupParticipants(Ljava/util/ArrayList;)V

    .line 704
    invoke-virtual {v2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setGroupType(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;)V

    move-object v0, p0

    move-object v1, p1

    move-object v3, p4

    move-wide v4, p5

    .line 706
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->createGroupShare(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)Z

    move-result v0

    return v0
.end method

.method public createUserProfile(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "profile"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    .prologue
    const/4 v2, 0x0

    .line 1023
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->isValidContext(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1024
    const-string/jumbo v3, "LocationManager"

    const-string/jumbo v4, "Invalid context on \'Create User Profile\' function."

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1066
    :goto_0
    return v2

    .line 1028
    :cond_0
    if-nez p2, :cond_1

    .line 1029
    const-string/jumbo v3, "LocationManager"

    .line 1030
    const-string/jumbo v4, "Failed to \'Create User Profile\'. Invalid new profile object."

    .line 1029
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1034
    :cond_1
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->getMyPhoneNumber(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1035
    .local v1, "phoneNumber":Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->getMyCountryCode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1037
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1038
    :cond_2
    const-string/jumbo v3, "LocationManager"

    .line 1039
    const-string/jumbo v4, "Failed to \'Create User Profile\'. My own phone number not found!"

    .line 1038
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1043
    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1044
    :cond_4
    const-string/jumbo v3, "LocationManager"

    .line 1045
    const-string/jumbo v4, "Failed to \'Create User Profile\'. My own Country Code not found!"

    .line 1044
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1049
    :cond_5
    invoke-virtual {p2, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->setPhoneNumber(Ljava/lang/String;)V

    .line 1050
    invoke-virtual {p2, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->setCountryCode(Ljava/lang/String;)V

    .line 1053
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1054
    :cond_6
    invoke-virtual {p2, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->setUserName(Ljava/lang/String;)V

    .line 1056
    :cond_7
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getEmail()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getEmail()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1057
    :cond_8
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, "@samsung.com"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->setEmail(Ljava/lang/String;)V

    .line 1059
    :cond_9
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    invoke-virtual {v3, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->updateProfile(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 1060
    const-string/jumbo v3, "LocationManager"

    const-string/jumbo v4, "Fail to \'Create User Profile\' on PlaceON server!"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1064
    :cond_a
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v2

    invoke-direct {p0, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->saveUserId(Landroid/content/Context;I)V

    .line 1066
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method public getAllParticipantTracking(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Ljava/util/HashMap;
    .locals 1
    .param p1, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1326
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getAllParticipantTracking(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method public getContactFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 411
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v6

    .line 412
    const/4 v7, 0x0

    invoke-virtual {v6, p1, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getContactList(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v1

    .line 414
    .local v1, "contacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 415
    .local v2, "iterator0":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    .line 430
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    invoke-direct {v0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(Ljava/lang/String;)V

    .line 431
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    invoke-virtual {v0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->setDisplayName(Ljava/lang/String;)V

    .line 432
    invoke-virtual {v0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->setFirstName(Ljava/lang/String;)V

    .line 434
    .end local v0    # "contact":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    :goto_0
    return-object v0

    .line 416
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 418
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v5

    .line 420
    .local v5, "phones":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 421
    .local v3, "iterator1":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 422
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    .line 424
    .local v4, "phone":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->areEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .line 425
    if-eqz v6, :cond_2

    goto :goto_0
.end method

.method public getLocation(DD)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .locals 1
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 1321
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getLocation(DD)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v0

    return-object v0
.end method

.method public getUserProfile(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 525
    const/4 v0, 0x0

    .line 527
    .local v0, "profile":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->isValidContext(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 528
    const-string/jumbo v4, "LocationManager"

    const-string/jumbo v5, "Invalid context to \'Get Profile\'"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    :goto_0
    return-object v3

    .line 532
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getUserProfile()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v0

    .line 533
    if-eqz v0, :cond_1

    move-object v3, v0

    .line 534
    goto :goto_0

    .line 536
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getUserId(Landroid/content/Context;)I

    move-result v2

    .line 538
    .local v2, "userId":I
    if-gez v2, :cond_2

    .line 539
    const-string/jumbo v4, "LocationManager"

    const-string/jumbo v5, "Failed to \'Get Profile\'. My own profile not found!"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 543
    :cond_2
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    invoke-virtual {v4, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->getMyProfile(I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v0

    .line 545
    if-nez v0, :cond_3

    .line 546
    const-string/jumbo v4, "LocationManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Failed to \'Get Profile\'. Profile for profileId  "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 547
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " not found on PlaceOn!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 546
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 551
    :cond_3
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->getInstance()Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->register(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 553
    .local v1, "pushId":Ljava/lang/String;
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 554
    :cond_4
    const-string/jumbo v4, "LocationManager"

    .line 555
    const-string/jumbo v5, "Failed to \'Get Profile\'. Failed to register new pushId on GCM."

    .line 554
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 559
    :cond_5
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->setPushId(Ljava/lang/String;)V

    .line 561
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->updateUserProfile(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 562
    const-string/jumbo v4, "LocationManager"

    .line 563
    const-string/jumbo v5, "Failed to \'Get Profile\'. Failed to update profile pushId on PlaceOn."

    .line 562
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 567
    :cond_6
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v3

    invoke-direct {p0, p1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->saveUserId(Landroid/content/Context;I)V

    move-object v3, v0

    .line 569
    goto :goto_0
.end method

.method public initialize(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    invoke-direct {v0, p1, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    .line 55
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mContext:Landroid/content/Context;

    .line 57
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->smsReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v2, "SMS_SENT"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 60
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->isKDevice(Landroid/content/Context;)Z

    .line 62
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->initialize(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "mCountry"    # Ljava/lang/String;
    .param p2, "mSaleCode"    # Ljava/lang/String;
    .param p3, "mNavigation"    # Ljava/lang/String;

    .prologue
    .line 1462
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public loadLocationDestinationList(IJ)Ljava/util/ArrayList;
    .locals 1
    .param p1, "maxCount"    # I
    .param p2, "fromTimestamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 124
    .local v0, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    return-object v0
.end method

.method public loadLocationFriendPendingList(Landroid/content/Context;IJ)Ljava/util/ArrayList;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "maxCount"    # I
    .param p3, "fromTimestamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v1

    invoke-virtual {v1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->loadLocationFriendPendingList(IJ)Ljava/util/ArrayList;

    move-result-object v0

    .line 113
    .local v0, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->checkLocationsContacts(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 115
    return-object v0
.end method

.method public loadLocationFriendSharedList(Landroid/content/Context;IJ)Ljava/util/ArrayList;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "maxCount"    # I
    .param p3, "fromTimestamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v1

    invoke-virtual {v1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->loadLocationFriendSharedList(IJ)Ljava/util/ArrayList;

    move-result-object v0

    .line 102
    .local v0, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->checkLocationsContacts(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 104
    return-object v0
.end method

.method public loadLocationGroupList(IJ)Ljava/util/ArrayList;
    .locals 2
    .param p1, "maxCount"    # I
    .param p2, "fromExpiretime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->loadLocationGroupList(IJ)Ljava/util/ArrayList;

    move-result-object v0

    .line 209
    .local v0, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    return-object v0
.end method

.method public loadLocationMyPlaceList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 172
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->loadLocationMyPlaceList()Ljava/util/ArrayList;

    move-result-object v0

    .line 174
    .local v0, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    if-nez v0, :cond_0

    .line 175
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 179
    .restart local v0    # "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    :cond_0
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->getMyPlaceList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 181
    .local v1, "myplaces":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 197
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;-><init>()V

    .line 198
    .local v2, "place":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;
    const-string/jumbo v3, "location_myplace"

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->setPlaceName(Ljava/lang/String;)V

    .line 199
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 202
    return-object v0

    .line 181
    .end local v2    # "place":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;

    .line 182
    .restart local v2    # "place":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;
    if-eqz v2, :cond_1

    .line 183
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->getPlaceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->setPlaceName(Ljava/lang/String;)V

    .line 184
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public loadLocationMyPlaceList(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "maxCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->loadLocationMyPlaceList()Ljava/util/ArrayList;

    move-result-object v0

    .line 133
    .local v0, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    if-nez v0, :cond_0

    .line 134
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 138
    .restart local v0    # "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    :cond_0
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->getMyPlaceList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    .line 140
    .local v2, "myplaces":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;>;"
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;-><init>()V

    .line 141
    .local v1, "my_place":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;
    const-string/jumbo v4, "location_myplace"

    invoke-virtual {v1, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->setPlaceName(Ljava/lang/String;)V

    .line 142
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 166
    :cond_2
    return-object v0

    .line 144
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;

    .line 145
    .local v3, "place":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;
    if-eqz v3, :cond_1

    .line 146
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-gt v5, p2, :cond_2

    .line 149
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->getPlaceName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->setPlaceName(Ljava/lang/String;)V

    .line 150
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public loadLocationSchedules(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fromDate"    # Ljava/util/Date;
    .param p3, "toDate"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 93
    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getScheduleEventsWithLocation(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;)Ljava/util/ArrayList;

    move-result-object v0

    .line 92
    return-object v0
.end method

.method public notifyLocationMSG(Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;)V
    .locals 2
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    .prologue
    .line 221
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;)V

    .line 222
    .local v0, "noti":Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 223
    .local v1, "thread":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 224
    return-void
.end method

.method public quitFromGroup(I)Z
    .locals 5
    .param p1, "groupId"    # I

    .prologue
    const/4 v1, 0x0

    .line 1426
    if-gez p1, :cond_0

    .line 1427
    const-string/jumbo v2, "LocationManager"

    const-string/jumbo v3, "Invalid group id to quit."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1443
    :goto_0
    return v1

    .line 1431
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v2

    .line 1432
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getUserProfile()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v0

    .line 1433
    .local v0, "profile":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v2

    if-gez v2, :cond_2

    .line 1434
    :cond_1
    const-string/jumbo v2, "LocationManager"

    const-string/jumbo v3, "Invalid user profile."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1438
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    invoke-virtual {v2, v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->quitFromGroup(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;I)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1439
    const-string/jumbo v2, "LocationManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Fail to quit from group "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1443
    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public removeGroupShared(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    const/4 v3, 0x0

    .line 1330
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v4

    .line 1331
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getUserProfile()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v1

    .line 1333
    .local v1, "profile":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v4

    if-ltz v4, :cond_0

    if-nez p2, :cond_1

    .line 1334
    :cond_0
    const-string/jumbo v4, "LocationManager"

    const-string/jumbo v5, "Could not remove group. Invalid data."

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1362
    :goto_0
    return v3

    .line 1338
    :cond_1
    const-string/jumbo v4, "LocationManager"

    const-string/jumbo v5, "Sending sms for all participants .... "

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1339
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 1357
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    invoke-virtual {v4, v1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->removeGroupShared(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 1358
    const-string/jumbo v4, "LocationManager"

    const-string/jumbo v5, "Could not remove group on PlaceON server .... "

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1339
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 1340
    .local v0, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1341
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getPhoneNumber()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1345
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getUrl()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_4

    .line 1346
    const-string/jumbo v4, "LocationManager"

    const-string/jumbo v5, "Invalid buddy url from create share group."

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1351
    :cond_4
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->createGroupDeletedMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1354
    .local v2, "smsContent":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, p1, v5, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->sendLocationSMS(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1362
    .end local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .end local v2    # "smsContent":Ljava/lang/String;
    :cond_5
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->deleteAllGroupShared()Z

    move-result v3

    goto :goto_0
.end method

.method public requestAddFriendsToGroup(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p2, "friends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    const/4 v1, 0x0

    .line 1166
    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    if-eqz p2, :cond_0

    .line 1167
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_2

    .line 1168
    :cond_0
    const-string/jumbo v2, "LocationManager"

    const-string/jumbo v3, "Invalid data to add participant to group."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1191
    :cond_1
    :goto_0
    return v1

    .line 1172
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v2

    .line 1173
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getUserProfile()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v0

    .line 1174
    .local v0, "profile":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    if-nez v0, :cond_3

    .line 1175
    const-string/jumbo v2, "LocationManager"

    const-string/jumbo v3, "User profile not found!"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1179
    :cond_3
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v2

    if-gez v2, :cond_4

    .line 1180
    const-string/jumbo v2, "LocationManager"

    const-string/jumbo v3, "Invalid profile user id."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1184
    :cond_4
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v3

    invoke-virtual {v2, v3, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->addFriendsToGroup(ILjava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v2

    .line 1185
    if-eqz v2, :cond_1

    .line 1188
    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->InvitationAddSend(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)V

    .line 1191
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->addFriendsToGroupShared(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v1

    goto :goto_0
.end method

.method public requestFriendLocation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "friendContact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 635
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getPlaceOnRegisteredPhoneNumber(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Ljava/lang/String;

    move-result-object v0

    .line 637
    .local v0, "phoneNumber":Ljava/lang/String;
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->requestFriendLocation(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public requestFriendLocation(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "friendPhoneNumber"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 649
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 650
    :cond_0
    const-string/jumbo v0, "LocationManager"

    .line 651
    const-string/jumbo v1, "Invalid contact phone number on \'Share My Location\' function."

    .line 650
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v9

    .line 676
    :goto_0
    return v0

    .line 655
    :cond_1
    new-instance v7, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    invoke-direct {v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;-><init>()V

    .line 657
    .local v7, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-static {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->removeCountryCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 656
    invoke-virtual {v7, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setPhoneNumber(Ljava/lang/String;)V

    .line 658
    invoke-virtual {v7, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setShared(Z)V

    .line 659
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->NOT_RESPONSED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    invoke-virtual {v7, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setStatus(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;)V

    .line 661
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getContactFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v8

    .line 663
    .local v8, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-eqz v8, :cond_2

    .line 664
    invoke-virtual {v7, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setContact(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    .line 665
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setFriendName(Ljava/lang/String;)V

    .line 668
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 669
    .local v2, "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 672
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;->REQUEST_LOCATION:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    const/4 v4, 0x0

    const-wide/32 v5, 0x493e0

    move-object v0, p0

    move-object v1, p1

    .line 671
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->createGroupShare(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)Z

    move-result v0

    .line 672
    if-nez v0, :cond_3

    move v0, v9

    .line 673
    goto :goto_0

    .line 676
    :cond_3
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->saveLocationFriendShared(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z

    move-result v0

    goto :goto_0
.end method

.method public requestGroupSharedUpdateInfo(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .param p3, "myLocation"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    const/4 v1, 0x0

    .line 1198
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v2

    if-ltz v2, :cond_0

    .line 1199
    if-nez p3, :cond_2

    .line 1200
    :cond_0
    const-string/jumbo v2, "LocationManager"

    const-string/jumbo v3, "Invalid data to get group info."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1227
    :cond_1
    :goto_0
    return v1

    .line 1204
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v2

    .line 1205
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getUserProfile()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v0

    .line 1206
    .local v0, "profile":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    if-nez v0, :cond_3

    .line 1207
    const-string/jumbo v2, "LocationManager"

    const-string/jumbo v3, "User profile not found!"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1211
    :cond_3
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v2

    if-gez v2, :cond_4

    .line 1212
    const-string/jumbo v2, "LocationManager"

    const-string/jumbo v3, "Invalid profile user id."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1216
    :cond_4
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v3

    invoke-virtual {v2, v3, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->setMyLocation(ILcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1217
    const-string/jumbo v2, "LocationManager"

    const-string/jumbo v3, "Setting my Location failure .... "

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1221
    :cond_5
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v3

    invoke-virtual {v2, v3, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->getGroupShared(ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1224
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->checkGroupFriendsContacts(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V

    .line 1226
    const-string/jumbo v1, "LocationManager"

    const-string/jumbo v2, "Updating group share on local database .... "

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1227
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->saveLocationGroupShared(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v1

    goto :goto_0
.end method

.method public requestMapAvailableByCountryAndSaleCode(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "mCountryCode"    # Ljava/lang/String;
    .param p2, "mSaleCode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1449
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1450
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1457
    :goto_0
    return-object v0

    .line 1453
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1454
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->loadNavigationPackage(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 1457
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public requestReiniviteFriendToGroup(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 5
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "mFriend"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .param p3, "mGroup"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    const/4 v2, 0x0

    .line 1501
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 1502
    :cond_0
    const-string/jumbo v3, "LocationManager"

    const-string/jumbo v4, "Invalid data to reinvite frend to group."

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1520
    :goto_0
    return v2

    .line 1506
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v3

    .line 1507
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getUserProfile()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v1

    .line 1508
    .local v1, "profile":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    if-nez v1, :cond_2

    .line 1509
    const-string/jumbo v3, "LocationManager"

    const-string/jumbo v4, "User profile not found!"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1513
    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v3

    if-gez v3, :cond_3

    .line 1514
    const-string/jumbo v3, "LocationManager"

    const-string/jumbo v4, "Invalid profile user id."

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1517
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1518
    .local v0, "friends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1519
    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->InvitationAddSend(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)V

    .line 1520
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public restartGroupShared(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .param p3, "currentLocation"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    const/4 v4, 0x0

    .line 1367
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-nez p1, :cond_1

    .line 1368
    :cond_0
    const-string/jumbo v5, "LocationManager"

    const-string/jumbo v6, "Invalid parameters ... "

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1422
    :goto_0
    return v4

    .line 1372
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v5

    .line 1373
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getUserProfile()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v2

    .line 1374
    .local v2, "profile":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v5

    if-gez v5, :cond_3

    .line 1375
    :cond_2
    const-string/jumbo v5, "LocationManager"

    const-string/jumbo v6, "Could not restart group. Invalid data."

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1379
    :cond_3
    const-string/jumbo v0, "xxxxxxxxxx"

    .line 1380
    .local v0, "accessToken":Ljava/lang/String;
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;->SHARE_GROUPLOCATION:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    invoke-virtual {p2, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setGroupType(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;)V

    .line 1381
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getSspGuid()Ljava/lang/String;

    move-result-object v6

    .line 1382
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1381
    invoke-virtual {v5, v6, v0, v7, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->restartGroupShared(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v5

    .line 1382
    if-nez v5, :cond_4

    .line 1383
    const-string/jumbo v5, "LocationManager"

    const-string/jumbo v6, "Could not restart group shared on PlaceON server ... "

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1387
    :cond_4
    const-string/jumbo v5, "LocationManager"

    const-string/jumbo v6, "Sending sms for all participants .... "

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1388
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_6

    .line 1406
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    .line 1407
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v6

    invoke-virtual {v5, v6, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->setMyLocation(ILcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 1408
    const-string/jumbo v5, "LocationManager"

    const-string/jumbo v6, "Setting my Location failure .... "

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1388
    :cond_6
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 1389
    .local v1, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 1390
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getPhoneNumber()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 1394
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getUrl()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_7

    .line 1395
    const-string/jumbo v5, "LocationManager"

    const-string/jumbo v6, "Invalid buddy url from create share group."

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1400
    :cond_7
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->createGroupRestartMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1403
    .local v3, "smsContent":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, p1, v6, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->sendLocationSMS(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1411
    .end local v1    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .end local v3    # "smsContent":Ljava/lang/String;
    :cond_8
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->requestGroupSharedUpdateInfo(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 1412
    const-string/jumbo v5, "LocationManager"

    .line 1413
    const-string/jumbo v6, "Could not restart share group, fail when try get group info"

    .line 1412
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1416
    :cond_9
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v5

    invoke-virtual {v5, p2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->saveLocationGroupShared(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 1417
    const-string/jumbo v5, "LocationManager"

    .line 1418
    const-string/jumbo v6, "Could not restart share group, fail when try to save group in local database"

    .line 1417
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1422
    :cond_a
    const/4 v4, 0x1

    goto/16 :goto_0
.end method

.method public saveLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 1
    .param p1, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 1317
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->saveLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v0

    return v0
.end method

.method public saveMyPlace(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;)Z
    .locals 2
    .param p1, "place"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;

    .prologue
    .line 213
    if-nez p1, :cond_0

    .line 214
    const-string/jumbo v0, "LocationManager"

    const-string/jumbo v1, "Object MyPlace is null ... "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    const/4 v0, 0x0

    .line 217
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->saveMyPlace(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;)Z

    move-result v0

    goto :goto_0
.end method

.method public saveUserProfile(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)Z
    .locals 1
    .param p1, "profile"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    .prologue
    .line 1156
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->saveUserProfile(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)Z

    move-result v0

    return v0
.end method

.method public setMyLocation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "myLocation"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    const/4 v1, 0x0

    .line 1290
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 1291
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidCoords()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1292
    :cond_0
    const-string/jumbo v2, "LocationManager"

    const-string/jumbo v3, "Invalid values to set my location."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1313
    :goto_0
    return v1

    .line 1296
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v2

    .line 1297
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getUserProfile()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v0

    .line 1298
    .local v0, "profile":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    if-nez v0, :cond_2

    .line 1299
    const-string/jumbo v2, "LocationManager"

    const-string/jumbo v3, "User profile not found!"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1303
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v2

    if-gez v2, :cond_3

    .line 1304
    const-string/jumbo v2, "LocationManager"

    const-string/jumbo v3, "Invalid profile user id."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1308
    :cond_3
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v3

    invoke-virtual {v2, v3, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->setMyLocation(ILcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1309
    const-string/jumbo v2, "LocationManager"

    const-string/jumbo v3, "Setting my Location failure .... "

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1313
    :cond_4
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public shareMyLocation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "friendContact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .param p3, "locationShared"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 586
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getPlaceOnRegisteredPhoneNumber(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Ljava/lang/String;

    move-result-object v0

    .line 588
    .local v0, "phoneNumber":Ljava/lang/String;
    invoke-virtual {p0, p1, v0, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->shareMyLocation(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v1

    return v1
.end method

.method public shareMyLocation(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "friendPhoneNumber"    # Ljava/lang/String;
    .param p3, "locationShared"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    const/4 v0, 0x0

    .line 603
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 604
    :cond_0
    const-string/jumbo v1, "LocationManager"

    .line 605
    const-string/jumbo v3, "Invalid contact phone number on \'Share My Location\' function."

    .line 604
    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    :goto_0
    return v0

    .line 609
    :cond_1
    new-instance v7, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    invoke-direct {v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;-><init>()V

    .line 611
    .local v7, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-static {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->removeCountryCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 610
    invoke-virtual {v7, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setPhoneNumber(Ljava/lang/String;)V

    .line 612
    invoke-virtual {v7, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setShared(Z)V

    .line 614
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 615
    .local v2, "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 618
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;->SHARE_MYLOCATION:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    const-wide/32 v5, 0x493e0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    .line 617
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->createGroupShare(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)Z

    move-result v0

    goto :goto_0
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    .line 69
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->smsReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 70
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->terminate(Landroid/content/Context;)V

    .line 71
    return-void
.end method

.method public updateMyStatus(Landroid/content/Context;ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)Z
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "groupId"    # I
    .param p3, "status"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;
    .param p4, "locationShared"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p5, "duration"    # J

    .prologue
    const/4 v7, 0x0

    .line 860
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 861
    const-string/jumbo v0, "LocationManager"

    .line 862
    const-string/jumbo v1, "Invalid context on \'Update Participant Status\' function."

    .line 861
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v7

    .line 902
    :goto_0
    return v0

    .line 866
    :cond_0
    if-gez p2, :cond_1

    .line 867
    const-string/jumbo v0, "LocationManager"

    .line 868
    const-string/jumbo v1, "Failed to \'Update Participant Status\'. Invalid group Id."

    .line 867
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v7

    .line 869
    goto :goto_0

    .line 872
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->ACCEPTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    if-ne p3, v0, :cond_3

    .line 873
    if-eqz p4, :cond_2

    invoke-virtual {p4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidCoords()Z

    move-result v0

    if-nez v0, :cond_3

    .line 874
    :cond_2
    const-string/jumbo v0, "LocationManager"

    .line 875
    const-string/jumbo v1, "Failed to \'Update Participant Status\'. Invalid location shared."

    .line 874
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v7

    .line 876
    goto :goto_0

    .line 879
    :cond_3
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getUserProfile(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v6

    .line 881
    .local v6, "profile":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    if-nez v6, :cond_4

    .line 882
    const-string/jumbo v0, "LocationManager"

    .line 883
    const-string/jumbo v1, "Failed to \'Update Participant Status\'. My own profile not found!"

    .line 882
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v7

    .line 884
    goto :goto_0

    .line 887
    :cond_4
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->ACCEPTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    if-ne p3, v0, :cond_5

    .line 888
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v1

    invoke-virtual {v0, v1, p4}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->setMyLocation(ILcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v0

    .line 889
    if-nez v0, :cond_5

    .line 890
    const-string/jumbo v0, "LocationManager"

    .line 891
    const-string/jumbo v1, "Failed to \'Set My Location\'. Failed to send my location to PlaceON."

    .line 890
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v7

    .line 892
    goto :goto_0

    .line 895
    :cond_5
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v1

    move v2, p2

    move-object v3, p3

    move-wide v4, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->updateParticipantStatus(IILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;J)Z

    move-result v0

    .line 896
    if-nez v0, :cond_6

    .line 897
    const-string/jumbo v0, "LocationManager"

    .line 898
    const-string/jumbo v1, "Failed to \'Update Participant Status\'. Filed to send updates to PlaceON."

    .line 897
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v7

    .line 899
    goto :goto_0

    .line 902
    :cond_6
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public updateUserProfile(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "newProfile"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    .prologue
    const/4 v1, 0x0

    .line 467
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->isValidContext(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 468
    const-string/jumbo v2, "LocationManager"

    const-string/jumbo v3, "Invalid context to \'Update Profile\' function"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    :cond_0
    :goto_0
    return v1

    .line 472
    :cond_1
    if-nez p2, :cond_2

    .line 473
    const-string/jumbo v2, "LocationManager"

    const-string/jumbo v3, "Passing null profile to \'Update Profile\' function"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 477
    :cond_2
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->getMyPhoneNumber(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 479
    .local v0, "phoneNumber":Ljava/lang/String;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 480
    :cond_3
    const-string/jumbo v2, "LocationManager"

    .line 481
    const-string/jumbo v3, "Fail to \'Update Profile\'. My own phone number not found!"

    .line 480
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 485
    :cond_4
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->areEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 486
    if-eqz v2, :cond_0

    .line 494
    invoke-virtual {p2, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->setPhoneNumber(Ljava/lang/String;)V

    .line 496
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mlocationHelper:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;

    invoke-virtual {v2, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->updateProfile(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 497
    const-string/jumbo v2, "LocationManager"

    .line 498
    const-string/jumbo v3, "Fail to \'Update Profile\'. Failed on Server, see JSON result."

    .line 497
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 502
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->setUserProfile(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)V

    .line 504
    const/4 v1, 0x1

    goto :goto_0
.end method
