.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationHasNotSamsungAccountView.java"


# static fields
.field private static mName:Ljava/lang/String;


# instance fields
.field private mImageView:Landroid/widget/ImageView;

.field private mNameView:Landroid/widget/TextView;

.field private mViewLocal:Landroid/view/View;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 21
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->mViewLocal:Landroid/view/View;

    .line 22
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->mNameView:Landroid/widget/TextView;

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 21
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->mViewLocal:Landroid/view/View;

    .line 22
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->mNameView:Landroid/widget/TextView;

    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->initIndicatorView(Landroid/content/Context;)V

    .line 55
    return-void
.end method

.method private getOptionBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # I

    .prologue
    .line 109
    const/4 v0, 0x0

    .line 111
    .local v0, "maskedBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 112
    const v2, 0x7f020204

    .line 111
    invoke-static {v1, p2, v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 114
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 115
    const v2, 0x7f020205

    .line 114
    invoke-static {v1, v0, v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageOverlay(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v10, 0x7f0a039f

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 66
    const-string/jumbo v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 65
    check-cast v0, Landroid/view/LayoutInflater;

    .line 68
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->mViewLocal:Landroid/view/View;

    if-nez v6, :cond_1

    .line 70
    const v6, 0x7f0300af

    .line 69
    invoke-virtual {v0, v6, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->mViewLocal:Landroid/view/View;

    .line 71
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->mViewLocal:Landroid/view/View;

    .line 72
    const v7, 0x7f0902e9

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 71
    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->mImageView:Landroid/widget/ImageView;

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 75
    .local v1, "msg":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->mViewLocal:Landroid/view/View;

    .line 76
    const v7, 0x7f0902ea

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 75
    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->mNameView:Landroid/widget/TextView;

    .line 79
    new-array v6, v9, [Ljava/lang/Object;

    sget-object v7, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->mName:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-static {v1, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 80
    .local v2, "notifcationMessage":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    const v6, 0x7f020239

    .line 81
    invoke-direct {p0, p1, v6}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->getOptionBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 83
    .local v3, "overlayedBitmap":Landroid/graphics/Bitmap;
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 90
    .local v4, "prompt":Ljava/lang/String;
    sget-object v6, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->mName:Ljava/lang/String;

    if-nez v6, :cond_0

    .line 91
    const-string/jumbo v6, ""

    sput-object v6, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->mName:Ljava/lang/String;

    .line 93
    :cond_0
    new-array v6, v9, [Ljava/lang/Object;

    sget-object v7, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->mName:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 94
    .local v5, "promptDefault":Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    .line 97
    .end local v1    # "msg":Ljava/lang/String;
    .end local v2    # "notifcationMessage":Ljava/lang/String;
    .end local v3    # "overlayedBitmap":Landroid/graphics/Bitmap;
    .end local v4    # "prompt":Ljava/lang/String;
    .end local v5    # "promptDefault":Ljava/lang/String;
    :cond_1
    new-instance v6, Landroid/os/Handler;

    invoke-direct {v6}, Landroid/os/Handler;-><init>()V

    new-instance v7, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView$1;

    invoke-direct {v7, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;)V

    .line 103
    const-wide/16 v8, 0x2328

    .line 97
    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 105
    return-void
.end method

.method public static setContent(Ljava/lang/String;)V
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 120
    const-string/jumbo v0, "DEBUG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setContent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    sput-object p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->mName:Ljava/lang/String;

    .line 122
    return-void
.end method
