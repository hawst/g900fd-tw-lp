.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationSharingDialogView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mIsMultiMode:Z

.field protected mViewLocal:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 18
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->mContext:Landroid/content/Context;

    .line 19
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->mViewLocal:Landroid/view/View;

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->mIsMultiMode:Z

    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 18
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->mContext:Landroid/content/Context;

    .line 19
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->mViewLocal:Landroid/view/View;

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->mIsMultiMode:Z

    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 18
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->mContext:Landroid/content/Context;

    .line 19
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->mViewLocal:Landroid/view/View;

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->mIsMultiMode:Z

    .line 40
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->mContext:Landroid/content/Context;

    .line 45
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->isMultiMode:Z

    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->mIsMultiMode:Z

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 48
    const-string/jumbo v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 47
    check-cast v0, Landroid/view/LayoutInflater;

    .line 50
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->mViewLocal:Landroid/view/View;

    if-nez v2, :cond_0

    .line 51
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->mIsMultiMode:Z

    if-eqz v2, :cond_1

    .line 53
    const v2, 0x7f0300cd

    .line 52
    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->mViewLocal:Landroid/view/View;

    .line 57
    :goto_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->mContext:Landroid/content/Context;

    .line 58
    const v3, 0x7f040030

    .line 57
    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 59
    .local v1, "rotation":Landroid/view/animation/Animation;
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 60
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->mViewLocal:Landroid/view/View;

    const v3, 0x7f0902e6

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 63
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView$1;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;)V

    .line 69
    const-wide/16 v4, 0x7d0

    .line 63
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 72
    .end local v1    # "rotation":Landroid/view/animation/Animation;
    :cond_0
    return-void

    .line 56
    :cond_1
    const v2, 0x7f0300cc

    .line 55
    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;->mViewLocal:Landroid/view/View;

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 78
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 83
    .local v0, "id":I
    return-void
.end method
