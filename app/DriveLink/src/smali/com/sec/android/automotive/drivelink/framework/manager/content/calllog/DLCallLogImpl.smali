.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;
.source "DLCallLogImpl.java"


# instance fields
.field private mCallLogType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

.field private mDate:J

.field private mDuration:Ljava/lang/String;

.field private mLogType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

.field private mName:Ljava/lang/String;

.field private mPhoneNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 2
    .param p1, "callType"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;
    .param p2, "logType"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "phoneNumber"    # Ljava/lang/String;
    .param p5, "duration"    # Ljava/lang/String;
    .param p6, "date"    # J

    .prologue
    const/4 v1, 0x0

    .line 14
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;-><init>()V

    .line 7
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;->mCallLogType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    .line 8
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;->mLogType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    .line 9
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;->mName:Ljava/lang/String;

    .line 10
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;->mPhoneNumber:Ljava/lang/String;

    .line 11
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;->mDuration:Ljava/lang/String;

    .line 12
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;->mDate:J

    .line 16
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;->mCallLogType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    .line 17
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;->mLogType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    .line 18
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;->mName:Ljava/lang/String;

    .line 19
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;->mPhoneNumber:Ljava/lang/String;

    .line 20
    iput-object p5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;->mDuration:Ljava/lang/String;

    .line 21
    iput-wide p6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;->mDate:J

    .line 22
    return-void
.end method


# virtual methods
.method public getDate()J
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;->mDate:J

    return-wide v0
.end method

.method public getDuration()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;->mDuration:Ljava/lang/String;

    return-object v0
.end method

.method public getLogType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;->mLogType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;->mPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;->mCallLogType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    return-object v0
.end method
