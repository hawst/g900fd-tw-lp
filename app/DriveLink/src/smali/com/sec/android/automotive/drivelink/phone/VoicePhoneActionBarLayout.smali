.class public Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;
.super Landroid/widget/RelativeLayout;
.source "VoicePhoneActionBarLayout.java"

# interfaces
.implements Lcom/nuance/drivelink/DLUiUpdater;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$SineEaseOut;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I

.field private static final SINE_OUT:Landroid/view/animation/Interpolator;


# instance fields
.field private isRestore:Z

.field private isRunning:Z

.field private mAnimator:Landroid/view/ViewPropertyAnimator;

.field private mBackBtn:Landroid/widget/LinearLayout;

.field private mBackBtnImage:Landroid/widget/ImageView;

.field private mBgView:Landroid/widget/ImageView;

.field private mContext:Landroid/content/Context;

.field private mCurrentMicState:Lcom/nuance/sample/MicState;

.field private mCurrentMode:I

.field private mDialogLayout:Landroid/widget/RelativeLayout;

.field private mDirection:Z

.field private mErrorCharSequence:Ljava/lang/CharSequence;

.field private mHiText:Landroid/widget/TextView;

.field private mIsErrorState:Z

.field private mIsMicDisplayed:Z

.field private mIsPhraseSotting:Z

.field private mIsStartQEnded:Z

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mListeningText:Landroid/widget/TextView;

.field private mMicBtn:Landroid/widget/ImageView;

.field private mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

.field private mMicLayout:Landroid/widget/RelativeLayout;

.field private mMicProcessBg:Landroid/widget/ImageView;

.field private mMicStartQBg:Landroid/widget/ImageView;

.field private mNoNetworkLayout:Landroid/widget/LinearLayout;

.field private mOn2DepthBtnClickListener:Landroid/view/View$OnClickListener;

.field private mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

.field private mOnVoicePhoneActionBarListener:Lcom/sec/android/automotive/drivelink/phone/OnVoicePhoneActionBarListener;

.field private mPhraseSpotterHandler:Landroid/os/Handler;

.field private mProcessText:Landroid/widget/TextView;

.field private mQuoteEnd:Landroid/widget/ImageView;

.field private mQuoteStart:Landroid/widget/ImageView;

.field private mRatio:F

.field private mSayText:Landroid/widget/TextView;

.field private mSearchBtn:Landroid/widget/ImageButton;

.field private mStandByLayout:Landroid/widget/LinearLayout;

.field private mStartQAni:Landroid/view/animation/Animation;

.field private mTTSBar:Landroid/widget/ImageView;

.field private mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

.field private mTTSBarLayout:Landroid/widget/RelativeLayout;

.field private mTTSText:Landroid/widget/TextView;

.field private mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

.field private mTitle:Landroid/widget/TextView;

.field private mVoiceLayout:Landroid/widget/RelativeLayout;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 521
    new-instance v0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$SineEaseOut;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$SineEaseOut;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 96
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 45
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsPhraseSotting:Z

    .line 523
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mRatio:F

    .line 525
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->isRestore:Z

    .line 588
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mDirection:Z

    .line 589
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->isRunning:Z

    .line 762
    new-instance v0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    .line 97
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mContext:Landroid/content/Context;

    .line 98
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->init()V

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 102
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsPhraseSotting:Z

    .line 523
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mRatio:F

    .line 525
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->isRestore:Z

    .line 588
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mDirection:Z

    .line 589
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->isRunning:Z

    .line 762
    new-instance v0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    .line 103
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mContext:Landroid/content/Context;

    .line 104
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->init()V

    .line 105
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 109
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsPhraseSotting:Z

    .line 523
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mRatio:F

    .line 525
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->isRestore:Z

    .line 588
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mDirection:Z

    .line 589
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->isRunning:Z

    .line 762
    new-instance v0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    .line 110
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mContext:Landroid/content/Context;

    .line 111
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->init()V

    .line 112
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsMicDisplayed:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;FJLandroid/view/animation/Interpolator;)V
    .locals 0

    .prologue
    .line 527
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->startScaleAnimation(FJLandroid/view/animation/Interpolator;)V

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)Z
    .locals 1

    .prologue
    .line 589
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->isRunning:Z

    return v0
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)Z
    .locals 1

    .prologue
    .line 588
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mDirection:Z

    return v0
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 588
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mDirection:Z

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)V
    .locals 0

    .prologue
    .line 592
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->runTTSBarAnimation()V

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 589
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->isRunning:Z

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSBar:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)Lcom/sec/android/automotive/drivelink/common/view/ListeningView;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsStartQEnded:Z

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)Z
    .locals 1

    .prologue
    .line 525
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->isRestore:Z

    return v0
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 525
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->isRestore:Z

    return-void
.end method

.method static synthetic access$9()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 521
    sget-object v0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method private init()V
    .locals 5

    .prologue
    .line 116
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 117
    .local v0, "inflater":Landroid/view/LayoutInflater;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;->isListening()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsPhraseSotting:Z

    .line 119
    const v1, 0x7f0300db

    .line 118
    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 121
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 122
    const v2, 0x7f090399

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 121
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    .line 123
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mContext:Landroid/content/Context;

    .line 124
    const v4, 0x7f0a024f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 125
    const-string/jumbo v3, ". "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 126
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mContext:Landroid/content/Context;

    const v4, 0x7f0a03f4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 123
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 128
    const v2, 0x7f09039a

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 127
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mBackBtnImage:Landroid/widget/ImageView;

    .line 129
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f09039b

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTitle:Landroid/widget/TextView;

    .line 139
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 140
    const v2, 0x7f090381

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 139
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    .line 141
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 142
    const v2, 0x7f090382

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 141
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mDialogLayout:Landroid/widget/RelativeLayout;

    .line 144
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 145
    const v2, 0x7f090383

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 144
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    .line 146
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 147
    const v2, 0x7f09031c

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 146
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    .line 148
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090388

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSText:Landroid/widget/TextView;

    .line 149
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 150
    const v2, 0x7f090389

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 149
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    .line 151
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 152
    const v2, 0x7f090391

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 151
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mListeningText:Landroid/widget/TextView;

    .line 153
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090392

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mProcessText:Landroid/widget/TextView;

    .line 154
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090393

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    .line 156
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f09038d

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    .line 157
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f09038e

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    .line 158
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f09038a

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    .line 159
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 160
    const v2, 0x7f09038b

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 159
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    .line 161
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 162
    const v2, 0x7f09038c

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 161
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    .line 164
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 165
    const v2, 0x7f09038f

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 164
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    .line 166
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090390

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSBar:Landroid/widget/ImageView;

    .line 168
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mContext:Landroid/content/Context;

    .line 169
    const v2, 0x7f04001e

    .line 168
    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    .line 170
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    new-instance v2, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$2;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$2;-><init>(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 211
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/nuance/sample/OnClickMicListenerImpl;

    .line 212
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-direct {v2, v3, v4}, Lcom/nuance/sample/OnClickMicListenerImpl;-><init>(Lcom/nuance/sample/MicStateMaster;Landroid/view/View;)V

    .line 211
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->isMicDisplayed()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsMicDisplayed:Z

    .line 215
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090380

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mBgView:Landroid/widget/ImageView;

    .line 216
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090384

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mSayText:Landroid/widget/TextView;

    .line 217
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090386

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mHiText:Landroid/widget/TextView;

    .line 218
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090385

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mQuoteStart:Landroid/widget/ImageView;

    .line 219
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090387

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mQuoteEnd:Landroid/widget/ImageView;

    .line 221
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mHiText:Landroid/widget/TextView;

    .line 222
    const-string/jumbo v2, "/system/fonts/Cooljazz.ttf"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->createTypefaceFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    .line 221
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 224
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mOnVoicePhoneActionBarListener:Lcom/sec/android/automotive/drivelink/phone/OnVoicePhoneActionBarListener;

    .line 226
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->setHasInternetLayout(Z)V

    .line 227
    return-void
.end method

.method private runTTSBarAnimation()V
    .locals 5

    .prologue
    const-wide/16 v3, 0x2bc

    .line 593
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    if-nez v0, :cond_0

    .line 594
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSBar:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    .line 595
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$4;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$4;-><init>(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 623
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$5;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$5;-><init>(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)Landroid/view/ViewPropertyAnimator;

    .line 633
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mDirection:Z

    if-eqz v0, :cond_1

    .line 634
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mContext:Landroid/content/Context;

    const/high16 v2, 0x428c0000    # 70.0f

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->dipToPixels(Landroid/content/Context;F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 635
    invoke-virtual {v0, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 639
    :goto_0
    return-void

    .line 637
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method private startScaleAnimation(FJLandroid/view/animation/Interpolator;)V
    .locals 2
    .param p1, "ratio"    # F
    .param p2, "duration"    # J
    .param p4, "interpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 530
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    if-nez v0, :cond_0

    .line 531
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setScaleX(F)V

    .line 532
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setScaleY(F)V

    .line 534
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    .line 535
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$3;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$3;-><init>(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 563
    :cond_0
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mRatio:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 571
    :goto_0
    return-void

    .line 566
    :cond_1
    iput p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mRatio:F

    .line 568
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 569
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 570
    sget-object v1, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method private startTTSBarAnimation()V
    .locals 1

    .prologue
    .line 642
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->isRunning:Z

    .line 643
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->runTTSBarAnimation()V

    .line 644
    return-void
.end method


# virtual methods
.method public displayError(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 285
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 288
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mErrorCharSequence:Ljava/lang/CharSequence;

    .line 290
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsErrorState:Z

    .line 291
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsStartQEnded:Z

    .line 292
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 293
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->stopTTSBarAnimation()V

    .line 294
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 295
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 297
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 298
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 299
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 301
    return-void
.end method

.method public displaySystemTurn(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 311
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 314
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 315
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 317
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 319
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 321
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 322
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 323
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 324
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 325
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 327
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02038c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 330
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->startTTSBarAnimation()V

    .line 347
    return-void
.end method

.method public displayUserTurn(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 307
    return-void
.end method

.method public displayWidgetContent(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 353
    return-void
.end method

.method protected getErrorCharSequence()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mErrorCharSequence:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 1

    .prologue
    .line 665
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMicState()Lcom/nuance/sample/MicState;
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    return-object v0
.end method

.method public handleUserCancel()V
    .locals 0

    .prologue
    .line 660
    return-void
.end method

.method public hideVoiceLayout()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 690
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 691
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 692
    return-void
.end method

.method protected isErrorState()Z
    .locals 1

    .prologue
    .line 480
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsErrorState:Z

    return v0
.end method

.method protected isTTSState()Z
    .locals 1

    .prologue
    .line 488
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->isRunning:Z

    return v0
.end method

.method public onClickable(Z)V
    .locals 1
    .param p1, "isClickable"    # Z

    .prologue
    .line 468
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 469
    return-void
.end method

.method public onDisplayMic(Z)V
    .locals 4
    .param p1, "isMicDisplayed"    # Z

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 777
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsMicDisplayed:Z

    .line 783
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    if-ne v0, v1, :cond_0

    .line 784
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsMicDisplayed:Z

    if-eqz v0, :cond_1

    .line 785
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsErrorState:Z

    .line 786
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsStartQEnded:Z

    .line 787
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 788
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 790
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 791
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 792
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 793
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 794
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 795
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 797
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 798
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 799
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 800
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 801
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 802
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02038c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 817
    :cond_0
    :goto_0
    return-void

    .line 804
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsErrorState:Z

    .line 805
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsStartQEnded:Z

    .line 806
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 807
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 809
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 810
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 811
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 812
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 813
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 814
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onPhraseSpotterStateChanged(Z)V
    .locals 3
    .param p1, "isSpotting"    # Z

    .prologue
    .line 749
    const-string/jumbo v0, "UiUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " - PhraseSpotting : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 750
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 749
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 752
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsPhraseSotting:Z

    .line 760
    return-void
.end method

.method public setCurrentMode(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    const v3, 0x7f0a024a

    const v2, 0x7f0a020f

    .line 246
    iput p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mCurrentMode:I

    .line 248
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mCurrentMode:I

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 281
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method protected setDayMode()V
    .locals 4

    .prologue
    const v3, 0x7f080031

    .line 734
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mBgView:Landroid/widget/ImageView;

    const v1, 0x7f020001

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 735
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mSayText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 736
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mHiText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080033

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 737
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mQuoteStart:Landroid/widget/ImageView;

    const v1, 0x7f02035b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 738
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mQuoteEnd:Landroid/widget/ImageView;

    const v1, 0x7f020359

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 739
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 740
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 742
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 744
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mBackBtnImage:Landroid/widget/ImageView;

    const v1, 0x7f020044

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 745
    return-void
.end method

.method public setHasInternetLayout(Z)V
    .locals 3
    .param p1, "hasInternet"    # Z

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 231
    if-eqz p1, :cond_0

    .line 233
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 234
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 243
    :goto_0
    return-void

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected setMicDisplayed(Z)V
    .locals 0
    .param p1, "isMicDisplayed"    # Z

    .prologue
    .line 824
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsMicDisplayed:Z

    .line 825
    return-void
.end method

.method protected setNightMode()V
    .locals 3

    .prologue
    const v2, 0x7f080030

    .line 717
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mBgView:Landroid/widget/ImageView;

    const v1, 0x7f08004e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 718
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mSayText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 720
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mHiText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 722
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mQuoteStart:Landroid/widget/ImageView;

    const v1, 0x7f02035e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 723
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mQuoteEnd:Landroid/widget/ImageView;

    const v1, 0x7f02035d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 724
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 726
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 728
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 730
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mBackBtnImage:Landroid/widget/ImageView;

    const v1, 0x7f020045

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 731
    return-void
.end method

.method public setOn2DepthBtnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 472
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mOn2DepthBtnClickListener:Landroid/view/View$OnClickListener;

    .line 473
    return-void
.end method

.method public setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 476
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 477
    return-void
.end method

.method public setOnMicStateChangeListener(Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .prologue
    .line 463
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .line 464
    return-void
.end method

.method public setOnVoicePhoneActionBarListener(Lcom/sec/android/automotive/drivelink/phone/OnVoicePhoneActionBarListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/automotive/drivelink/phone/OnVoicePhoneActionBarListener;

    .prologue
    .line 820
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mOnVoicePhoneActionBarListener:Lcom/sec/android/automotive/drivelink/phone/OnVoicePhoneActionBarListener;

    .line 821
    return-void
.end method

.method protected setTTSState()V
    .locals 1

    .prologue
    .line 492
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->displaySystemTurn(Ljava/lang/CharSequence;)V

    .line 493
    return-void
.end method

.method public showVoiceLayout()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 669
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 671
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 673
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 674
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 681
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 682
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 683
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 684
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 685
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 686
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 687
    return-void

    .line 678
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected stopTTSBarAnimation()V
    .locals 1

    .prologue
    .line 647
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_0

    .line 648
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 650
    :cond_0
    return-void
.end method

.method public updateMicRMSChange(I)V
    .locals 1
    .param p1, "rmsValue"    # I

    .prologue
    .line 500
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsStartQEnded:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    if-eqz v0, :cond_0

    .line 501
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->update(I)V

    .line 503
    :cond_0
    return-void
.end method

.method public updateMicState(Lcom/nuance/sample/MicState;)V
    .locals 5
    .param p1, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 357
    const-string/jumbo v0, "UiUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    .line 360
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    if-nez v0, :cond_3

    .line 361
    invoke-static {}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 448
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mOnVoicePhoneActionBarListener:Lcom/sec/android/automotive/drivelink/phone/OnVoicePhoneActionBarListener;

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mOnVoicePhoneActionBarListener:Lcom/sec/android/automotive/drivelink/phone/OnVoicePhoneActionBarListener;

    .line 451
    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    .line 449
    invoke-interface {v0, v4, v1}, Lcom/sec/android/automotive/drivelink/phone/OnVoicePhoneActionBarListener;->onVoicePhoneActionBarUpdate(II)V

    .line 454
    :cond_0
    return-void

    .line 363
    :pswitch_0
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsErrorState:Z

    .line 364
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsStartQEnded:Z

    .line 365
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 366
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->stopTTSBarAnimation()V

    .line 367
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 375
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsMicDisplayed:Z

    if-eqz v0, :cond_1

    .line 376
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 377
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 378
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 379
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 380
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 381
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 382
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 383
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02038c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 389
    :goto_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 390
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 391
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 392
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 394
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mCurrentMode:I

    const/16 v1, 0x80

    if-ne v0, v1, :cond_2

    .line 395
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSText:Landroid/widget/TextView;

    const v1, 0x7f0a024a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 400
    :goto_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    goto :goto_0

    .line 385
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 386
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x2bc

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 397
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSText:Landroid/widget/TextView;

    const v1, 0x7f0a020f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 404
    :pswitch_1
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsErrorState:Z

    .line 405
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 406
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->stopTTSBarAnimation()V

    .line 407
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 408
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 409
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 410
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 411
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 412
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 414
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 415
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 416
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 417
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 418
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 420
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020385

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 421
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020388

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 422
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 426
    :pswitch_2
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsErrorState:Z

    .line 427
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mIsStartQEnded:Z

    .line 428
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->stopTTSBarAnimation()V

    .line 429
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 430
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 431
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 432
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 433
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 434
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 436
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 437
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 438
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 439
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 440
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 441
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->play(I)V

    goto/16 :goto_0

    .line 445
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    invoke-interface {v0, p0, p1}, Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;->onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V

    goto/16 :goto_0

    .line 361
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
