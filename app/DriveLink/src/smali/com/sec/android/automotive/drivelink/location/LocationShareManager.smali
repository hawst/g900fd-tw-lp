.class public Lcom/sec/android/automotive/drivelink/location/LocationShareManager;
.super Ljava/lang/Object;
.source "LocationShareManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "LocationManager"

.field private static mInstance:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDLinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mSharedPreferences:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mInstance:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mDLinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 33
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 34
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mSharedPreferences:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    .line 35
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    .line 39
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 38
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mDLinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 40
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 42
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 43
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mSharedPreferences:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    .line 47
    :goto_0
    return-void

    .line 45
    :cond_0
    const-string/jumbo v1, "LocationManager"

    const-string/jumbo v2, "Failure to get application context!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/location/LocationShareManager;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mDLinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/location/LocationShareManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/automotive/drivelink/location/LocationShareManager;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mInstance:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mInstance:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    .line 53
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mInstance:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    return-object v0
.end method

.method private getLocationBitmap(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "locationItem"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;

    .prologue
    const v5, 0x7f02023b

    .line 337
    const/4 v1, 0x0

    .line 339
    .local v1, "maskedBitmap":Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v2

    .line 341
    .local v2, "serviceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 342
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 344
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    .line 345
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getPhoneNumber()Ljava/lang/String;

    move-result-object v4

    .line 344
    invoke-interface {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getContactImageFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 346
    .local v0, "contactBitmap":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, v0, v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 350
    .end local v0    # "contactBitmap":Landroid/graphics/Bitmap;
    :cond_0
    if-nez v1, :cond_1

    .line 351
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 352
    const v4, 0x7f02023d

    .line 351
    invoke-static {v3, v4, v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 356
    :cond_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 357
    const v4, 0x7f02023c

    .line 356
    invoke-static {v3, v1, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageOverlay(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v3

    return-object v3
.end method

.method public static releaseInstance()V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mInstance:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    .line 62
    return-void
.end method


# virtual methods
.method public acceptLocationShared(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "friendPhoneNumber"    # Ljava/lang/String;
    .param p3, "url"    # Ljava/lang/String;

    .prologue
    .line 203
    if-nez p1, :cond_0

    .line 210
    :goto_0
    return-void

    .line 206
    :cond_0
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    .line 208
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mDLinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestAcceptLocationShared(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public acceptShareMyLocation(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 168
    if-nez p1, :cond_0

    .line 169
    const-string/jumbo v0, "LocationManager"

    const-string/jumbo v1, "Missing application context!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :goto_0
    return-void

    .line 173
    :cond_0
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    .line 175
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$2;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareManager;Landroid/content/Context;Ljava/lang/String;)V

    .line 197
    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$2;->execute(Landroid/content/Context;)Z

    goto :goto_0
.end method

.method public createUserProfile(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    if-nez p1, :cond_1

    .line 101
    const-string/jumbo v2, "LocationManager"

    const-string/jumbo v3, "Missing application context!"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->hasSamsungAccount(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->showProgressDialog()V

    .line 112
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->getInstance()Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;

    move-result-object v1

    .line 113
    .local v1, "samsungAccount":Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;-><init>()V

    .line 115
    .local v0, "profile":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    const-string/jumbo v2, "u0zz5r9gn0"

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->setSspGuid(Ljava/lang/String;)V

    .line 116
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->getEmail()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->setUserName(Ljava/lang/String;)V

    .line 117
    const-string/jumbo v2, "M"

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->setGender(Ljava/lang/String;)V

    .line 118
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->getBirthday()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->setBirthday(Ljava/lang/String;)V

    .line 119
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->getEmail()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->setEmail(Ljava/lang/String;)V

    .line 120
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->getCc()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->setCountryCode(Ljava/lang/String;)V

    .line 121
    const-string/jumbo v2, "20140312"

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->setExpiredTime(Ljava/lang/String;)V

    .line 122
    const-string/jumbo v2, "NOT REGISTERED YET"

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->setPushId(Ljava/lang/String;)V

    .line 123
    const-string/jumbo v2, "SHV-330"

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->setModel(Ljava/lang/String;)V

    .line 128
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mDLinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    invoke-interface {v2, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestCreateUserProfile(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)V

    goto :goto_0
.end method

.method public dismissProgressDialog()V
    .locals 2

    .prologue
    .line 84
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 88
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mProgressDialog:Landroid/app/ProgressDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :goto_0
    return-void

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public hasAcceptedDiclaimer()Z
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mSharedPreferences:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    .line 96
    const-string/jumbo v1, "PREF_ACCEPT_DISCLAIMER_PLACE_ON"

    const/4 v2, 0x0

    .line 95
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public onResponseCreateUserProfile(Z)V
    .locals 2
    .param p1, "result"    # Z

    .prologue
    .line 225
    if-nez p1, :cond_0

    .line 226
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 228
    const v1, 0x7f0a033c

    .line 227
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->onNotifyLocationFailedRequestSent(I)V

    .line 230
    :cond_0
    return-void
.end method

.method public onResponseRequestAcceptLocationShared(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V
    .locals 9
    .param p1, "friend"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    const v5, 0x7f0a033a

    const/4 v8, 0x1

    .line 283
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v3

    if-nez v3, :cond_1

    .line 285
    :cond_0
    const-string/jumbo v3, "LocationManager"

    const-string/jumbo v4, "Failed to get shared location. Maybe it has expired!"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v3

    .line 288
    const v4, 0x7f0a02bc

    .line 287
    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->onNotifyLocationFailedRequestSent(I)V

    .line 333
    :goto_0
    return-void

    .line 295
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v2

    .line 297
    .local v2, "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidCoords()Z

    move-result v3

    if-nez v3, :cond_2

    .line 299
    const-string/jumbo v3, "LocationManager"

    const-string/jumbo v4, "Failed to get shared location. Maybe it has expired!"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    .line 302
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v3

    .line 303
    invoke-virtual {v3, v5}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->onNotifyLocationFailedRequestSent(I)V

    goto :goto_0

    .line 311
    :cond_2
    const/4 v0, 0x0

    .line 312
    .local v0, "address":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 313
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 315
    :cond_3
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v3

    .line 316
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v5

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    .line 315
    invoke-static {v3, v4, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/location/map/util/GeocodingAPI;->getAddressFromLatLng(DDLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 317
    invoke-virtual {v2, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->setLocationAddress(Ljava/lang/String;)V

    .line 322
    :goto_1
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    const-class v4, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 324
    .local v1, "i":Landroid/content/Intent;
    const-string/jumbo v3, "goto_navigation"

    invoke-virtual {v1, v3, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 325
    const-string/jumbo v3, "name"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getFriendName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 326
    const-string/jumbo v3, "latitude"

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 327
    const-string/jumbo v3, "longitude"

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 328
    const-string/jumbo v3, "address"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 329
    const-string/jumbo v3, "bitmap"

    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->getLocationBitmap(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 331
    const-string/jumbo v3, "isViewSharedLocation"

    invoke-virtual {v1, v3, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 332
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 319
    .end local v1    # "i":Landroid/content/Intent;
    :cond_4
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public onResponseRequestAcceptShareMyLocation(Z)V
    .locals 2
    .param p1, "result"    # Z

    .prologue
    .line 273
    if-nez p1, :cond_0

    .line 274
    const-string/jumbo v0, "LocationManager"

    const-string/jumbo v1, "Failed to accept share my location!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 277
    const v1, 0x7f0a02ba

    .line 276
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->onNotifyLocationFailedRequestSent(I)V

    .line 280
    :cond_0
    return-void
.end method

.method public onResponseRequestFriendLocationShare(Ljava/lang/Exception;Z)V
    .locals 5
    .param p1, "error"    # Ljava/lang/Exception;
    .param p2, "result"    # Z

    .prologue
    .line 256
    move v0, p2

    .line 258
    .local v0, "_result":Z
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$4;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$4;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareManager;Z)V

    .line 268
    const-wide/16 v3, 0x9c4

    .line 258
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 269
    return-void
.end method

.method public onResponseRequestShareMyLocation(Z)V
    .locals 5
    .param p1, "result"    # Z

    .prologue
    .line 233
    move v0, p1

    .line 235
    .local v0, "_result":Z
    if-nez p1, :cond_0

    .line 236
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v1

    .line 237
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v2

    .line 238
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getTopNotificationItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    .line 236
    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->removeNotification(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    .line 241
    :cond_0
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$3;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$3;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareManager;Z)V

    .line 251
    const-wide/16 v3, 0x9c4

    .line 241
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 252
    return-void
.end method

.method public requestFriendsLocation(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "friendPhoneNumber"    # Ljava/lang/String;

    .prologue
    .line 214
    if-nez p1, :cond_0

    .line 215
    const-string/jumbo v0, "LocationManager"

    const-string/jumbo v1, "Missing application context!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    :goto_0
    return-void

    .line 219
    :cond_0
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    .line 221
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mDLinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestFriendLocationShare(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    .line 58
    return-void
.end method

.method public shareMyLocation(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "friendPhoneNumber"    # Ljava/lang/String;

    .prologue
    .line 134
    if-nez p1, :cond_0

    .line 135
    const-string/jumbo v0, "LocationManager"

    const-string/jumbo v1, "Missing application context!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :goto_0
    return-void

    .line 139
    :cond_0
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mContext:Landroid/content/Context;

    .line 141
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$1;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareManager;Landroid/content/Context;Ljava/lang/String;)V

    .line 163
    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$1;->execute(Landroid/content/Context;)Z

    goto :goto_0
.end method

.method public showProgressDialog()V
    .locals 4

    .prologue
    .line 65
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 69
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 72
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    .line 73
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 72
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 74
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 75
    const v3, 0x7f0a02fd

    .line 74
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 77
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 79
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method
