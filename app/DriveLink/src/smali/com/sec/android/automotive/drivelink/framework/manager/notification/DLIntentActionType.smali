.class public Lcom/sec/android/automotive/drivelink/framework/manager/notification/DLIntentActionType;
.super Ljava/lang/Object;
.source "DLIntentActionType.java"


# static fields
.field public static final ACTION_ALARM_ALERT:Ljava/lang/String; = "com.samsung.sec.android.clockpackage.alarm.ALARM_STARTED_IN_DRIVELINK"

.field public static final ACTION_BATTERY_LOW:Ljava/lang/String; = "android.intent.action.BATTERY_LOW"

.field public static final ACTION_MMS_RECEIVED:Ljava/lang/String; = "android.provider.Telephony.WAP_PUSH_RECEIVED"

.field public static final ACTION_OUTGOING_CALL:Ljava/lang/String; = "android.intent.action.NEW_OUTGOING_CALL"

.field public static final ACTION_PHONE_STATE:Ljava/lang/String; = "android.intent.action.PHONE_STATE"

.field public static final ACTION_SCHEDULE_ALERT:Ljava/lang/String; = "com.android.calendar.SEND_ALERTINFO_ACTION"

.field public static final ACTION_SMS_RECEIVED:Ljava/lang/String; = "android.provider.Telephony.SMS_RECEIVED"

.field public static final MIME_TYPE:Ljava/lang/String; = "application/vnd.wap.mms-message"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
