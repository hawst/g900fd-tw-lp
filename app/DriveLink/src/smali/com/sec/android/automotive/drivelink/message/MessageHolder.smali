.class public Lcom/sec/android/automotive/drivelink/message/MessageHolder;
.super Ljava/lang/Object;
.source "MessageHolder.java"


# static fields
.field private static volatile _instance:Lcom/sec/android/automotive/drivelink/message/MessageHolder;

.field static mCurrentInbox:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method

.method public static getMessageHolder()Lcom/sec/android/automotive/drivelink/message/MessageHolder;
    .locals 2

    .prologue
    .line 16
    const-class v1, Lcom/sec/android/automotive/drivelink/message/MessageHolder;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageHolder;->_instance:Lcom/sec/android/automotive/drivelink/message/MessageHolder;

    if-nez v0, :cond_0

    .line 18
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageHolder;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/message/MessageHolder;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/message/MessageHolder;->_instance:Lcom/sec/android/automotive/drivelink/message/MessageHolder;

    .line 16
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageHolder;->_instance:Lcom/sec/android/automotive/drivelink/message/MessageHolder;

    return-object v0

    .line 16
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public getCurrentInbox()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageHolder;->mCurrentInbox:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    return-object v0
.end method

.method public setInbox(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;)V
    .locals 0
    .param p1, "inbox"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .prologue
    .line 27
    sput-object p1, Lcom/sec/android/automotive/drivelink/message/MessageHolder;->mCurrentInbox:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 28
    return-void
.end method
