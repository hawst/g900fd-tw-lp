.class public Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;
.super Ljava/lang/Object;
.source "DriveLinkMusicTableHandler.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkBaseTableHandler;


# static fields
.field private static final TABLE_LAST_MUSICID_LIST:Ljava/lang/String; = "tb_lastmusicidlist"

.field private static final TABLE_LAST_PLAYED_MUSICID:Ljava/lang/String; = "tb_lastplayedmusicid"

.field private static final TABLE_MUSIC_PLAYER_ENV_INFO:Ljava/lang/String; = "tb_musicplayerenvinfo"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method

.method private createMusicTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 37
    const-string/jumbo v0, "CREATE TABLE IF NOT EXISTS tb_lastmusicidlist (_id INTEGER PRIMARY KEY AUTOINCREMENT, music_id INT);"

    .line 39
    .local v0, "sql":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 41
    const-string/jumbo v0, "CREATE TABLE IF NOT EXISTS tb_lastplayedmusicid (_id INTEGER PRIMARY KEY AUTOINCREMENT, music_id INT);"

    .line 43
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 45
    const-string/jumbo v0, "insert into tb_lastplayedmusicid values (NULL, -1)"

    .line 46
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 48
    const-string/jumbo v0, "CREATE TABLE IF NOT EXISTS tb_musicplayerenvinfo (_id INTEGER PRIMARY KEY AUTOINCREMENT, suffle_flag INT, repeat_type INT);"

    .line 51
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 53
    const-string/jumbo v0, "insert into tb_musicplayerenvinfo values (NULL, 0, 0)"

    .line 55
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method private dropMusicTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 60
    const-string/jumbo v0, "DROP TABLE IF EXISTS tb_lastmusicidlist"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 61
    const-string/jumbo v0, "DROP TABLE IF EXISTS tb_lastplayedmusicid"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 62
    const-string/jumbo v0, "DROP TABLE IF EXISTS tb_musicplayerenvinfo"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 63
    return-void
.end method


# virtual methods
.method public createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;->createMusicTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 26
    return-void
.end method

.method public dropTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;->dropMusicTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 32
    return-void
.end method

.method public restoreLastMusicIdList(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/ArrayList;
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 91
    .local v2, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    const-string/jumbo v3, "select * from tb_lastmusicidlist;"

    .line 92
    .local v3, "sql":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 94
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_2

    .line 95
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 97
    :cond_0
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;-><init>()V

    .line 98
    .local v1, "music":Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;
    const/4 v4, 0x1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setId(I)V

    .line 99
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 103
    .end local v1    # "music":Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 106
    :cond_2
    return-object v2
.end method

.method public restoreLastPlayedMusicId(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 116
    const-string/jumbo v2, "select * from tb_lastplayedmusicid where _id = 1;"

    .line 118
    .local v2, "sql":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 120
    .local v0, "cursor":Landroid/database/Cursor;
    const/4 v1, -0x1

    .line 121
    .local v1, "musicId":I
    if-eqz v0, :cond_2

    .line 122
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 124
    :cond_0
    const/4 v3, 0x1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 125
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 128
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 131
    :cond_2
    return v1
.end method

.method public restoreMusicPlayerEnvInfo(Landroid/database/sqlite/SQLiteDatabase;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v4, 0x1

    .line 135
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;-><init>()V

    .line 137
    .local v1, "musicPlayerEnvInfo":Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;
    const-string/jumbo v2, "select * from tb_musicplayerenvinfo where _id = 1;"

    .line 139
    .local v2, "sql":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 141
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_2

    .line 142
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 144
    :cond_0
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-ne v3, v4, :cond_3

    move v3, v4

    :goto_0
    invoke-virtual {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->setShuffle(Z)V

    .line 146
    const/4 v3, 0x2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->getEnum(I)Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    move-result-object v3

    .line 145
    invoke-virtual {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->setMusicRepeatType(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;)V

    .line 147
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 150
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 153
    :cond_2
    return-object v1

    .line 144
    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public saveLastMusicIdList(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 67
    .local p2, "lastMusicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    const-string/jumbo v2, "DELETE FROM tb_lastmusicidlist"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 68
    const-string/jumbo v2, "VACUUM"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 70
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 72
    :try_start_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 74
    const-string/jumbo v2, "insert into tb_lastmusicidlist values (NULL, ?)"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    .line 76
    .local v1, "statement":Landroid/database/sqlite/SQLiteStatement;
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 81
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 86
    .end local v1    # "statement":Landroid/database/sqlite/SQLiteStatement;
    :cond_0
    return-void

    .line 76
    .restart local v1    # "statement":Landroid/database/sqlite/SQLiteStatement;
    :cond_1
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 77
    .local v0, "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    const/4 v3, 0x1

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getId()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 78
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->execute()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 82
    .end local v0    # "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .end local v1    # "statement":Landroid/database/sqlite/SQLiteStatement;
    :catchall_0
    move-exception v2

    .line 83
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 84
    throw v2
.end method

.method public saveLastPlayedMusicId(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 110
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "update tb_lastplayedmusicid set music_id = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 111
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " where _id = 1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 110
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, "sql":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 113
    return-void
.end method

.method public saveMusicPlayerEnvInfo(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "musicPlayerEnvInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    .prologue
    .line 158
    const/4 v0, 0x0

    .line 159
    .local v0, "nShuffleFlag":I
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->isShuffle()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 160
    const/4 v0, 0x1

    .line 163
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "update tb_musicplayerenvinfo set suffle_flag = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 164
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", repeat_type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 165
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->getMusicRepeatType()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 166
    const-string/jumbo v3, " where _id = 1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 163
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 167
    .local v1, "sql":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 168
    return-void
.end method
