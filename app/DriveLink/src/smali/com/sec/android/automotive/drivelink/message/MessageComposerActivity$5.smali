.class Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$5;
.super Ljava/lang/Object;
.source "MessageComposerActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceDrivingChangeUpdater$DRVSTATUS:[I


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceDrivingChangeUpdater$DRVSTATUS()[I
    .locals 3

    .prologue
    .line 1254
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$5;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceDrivingChangeUpdater$DRVSTATUS:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->values()[Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->STATUS_CHANGE_NONE:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->STATUS_NOTIFY_CAR_SPEED_STATUS_CHANGE:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->STATUS_REQUEST_START_DRIVING:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->STATUS_REQUEST_STOP_DRIVING:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$5;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceDrivingChangeUpdater$DRVSTATUS:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    .line 1254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDriveStatusChanged(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;)V
    .locals 8
    .param p1, "Type"    # Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1259
    const/4 v2, 0x3

    const-string/jumbo v3, "mDrivingChangeListener"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Type="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$9(ILjava/lang/String;Ljava/lang/String;)V

    .line 1260
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$5;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceDrivingChangeUpdater$DRVSTATUS()[I

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1286
    :goto_0
    :pswitch_0
    return-void

    .line 1265
    :pswitch_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    .line 1266
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;

    move-result-object v0

    .line 1267
    .local v0, "dlspeedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    if-eqz v0, :cond_0

    .line 1268
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;->getCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    move-result-object v2

    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;->CAR_SPEED_STATUS_STOPPED:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    if-eq v2, v3, :cond_0

    .line 1269
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    invoke-static {v2, v7}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$12(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;Z)V

    goto :goto_0

    .line 1271
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    invoke-static {v2, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$12(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;Z)V

    goto :goto_0

    .line 1275
    .end local v0    # "dlspeedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    :pswitch_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    .line 1276
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->responseCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;

    move-result-object v1

    .line 1278
    .local v1, "speedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    if-eqz v1, :cond_1

    .line 1279
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;->getCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    move-result-object v2

    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;->CAR_SPEED_STATUS_STOPPED:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    if-eq v2, v3, :cond_1

    .line 1280
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    invoke-static {v2, v7}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$12(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;Z)V

    goto :goto_0

    .line 1282
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    invoke-static {v2, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$12(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;Z)V

    goto :goto_0

    .line 1260
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
