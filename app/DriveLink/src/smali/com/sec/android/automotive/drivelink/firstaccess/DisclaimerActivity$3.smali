.class Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$3;
.super Ljava/lang/Object;
.source "DisclaimerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 183
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 185
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->getUserTouringGuideDone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 186
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    .line 187
    const-class v2, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;

    .line 186
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 188
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->startActivity(Landroid/content/Intent;)V

    .line 202
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->finish()V

    .line 203
    return-void

    .line 197
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    .line 198
    const-class v2, Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    .line 197
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 199
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->startActivity(Landroid/content/Intent;)V

    .line 200
    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$1()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "onClick : start HomeActivity"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
