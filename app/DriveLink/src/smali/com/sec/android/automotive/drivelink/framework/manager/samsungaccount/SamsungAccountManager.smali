.class public Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;
.super Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;
.source "SamsungAccountManager.java"


# static fields
.field private static final API_SERVER_URL:Ljava/lang/String; = "api.samsungosp.com"

.field public static final ID_REQUEST_ACCESSTOKEN:I = 0xad2

.field public static final ID_REQUEST_AUTH_CODE:I = 0xad5

.field public static final ID_REQUEST_CHECKLIST_VALIDATION:I = 0xad3

.field public static final ID_REQUEST_CONNECT_SERVICE:I = 0xace

.field public static final ID_REQUEST_DISCLAIMER_AGREEMENT:I = 0xad4

.field public static final ID_REQUEST_DISCONNECT_SERVICE:I = 0xacf

.field public static final ID_REQUEST_REGISTER_CALLBACK:I = 0xad0

.field public static final ID_REQUEST_SCLOUD_ACCESS_TOKEN:I = 0xad6

.field public static final ID_REQUEST_UNREGISTER_CALLBACK:I = 0xad1

.field public static final ID_REQUEST_USER_NAME:I = 0xad7

.field public static final KEY_CLIENTID:Ljava/lang/String; = "clientID"

.field public static final KEY_CLIENTSECRET:Ljava/lang/String; = "clienSecret"

.field public static final KEY_PACKAGENAME:Ljava/lang/String; = "packageName"

.field private static final SSO_ACCOUNT_TYPE:Ljava/lang/String; = "com.osp.app.signin"


# instance fields
.field private isBound:Z

.field private mAccessToken:Ljava/lang/String;

.field mHandler:Landroid/os/Handler;

.field private mISAService:Lcom/msc/sa/aidl/ISAService;

.field private mRegistrationCode:Ljava/lang/String;

.field private mSACallback:Lcom/msc/sa/aidl/ISACallback;

.field private mServiceConnection:Landroid/content/ServiceConnection;

.field private mUserId:Ljava/lang/String;

.field private resultData:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;-><init>()V

    .line 55
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mISAService:Lcom/msc/sa/aidl/ISAService;

    .line 56
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 58
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mRegistrationCode:Ljava/lang/String;

    .line 59
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mSACallback:Lcom/msc/sa/aidl/ISACallback;

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->isBound:Z

    .line 64
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->resultData:Landroid/os/Bundle;

    .line 349
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mHandler:Landroid/os/Handler;

    .line 67
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mRegistrationCode:Ljava/lang/String;

    .line 68
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mSACallback:Lcom/msc/sa/aidl/ISACallback;

    .line 69
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;Lcom/msc/sa/aidl/ISAService;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mISAService:Lcom/msc/sa/aidl/ISAService;

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;)Lcom/msc/sa/aidl/ISAService;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mISAService:Lcom/msc/sa/aidl/ISAService;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->resultData:Landroid/os/Bundle;

    return-object v0
.end method

.method private getUserName(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 19
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "accessToken"    # Ljava/lang/String;

    .prologue
    .line 375
    const/4 v10, 0x0

    .line 377
    .local v10, "line":Ljava/lang/String;
    new-instance v4, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 379
    .local v4, "httpClient":Lorg/apache/http/client/HttpClient;
    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    .line 380
    .local v15, "url":Ljava/lang/StringBuffer;
    const-string/jumbo v16, "https://api.samsungosp.com/v2/profile/user/user/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 381
    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 382
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    .line 384
    .local v12, "requestURL":Ljava/lang/String;
    new-instance v5, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v5, v12}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 385
    .local v5, "httpGet":Lorg/apache/http/client/methods/HttpGet;
    const-string/jumbo v16, "Content-Type"

    const-string/jumbo v17, "text/xml"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v5, v0, v1}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    const-string/jumbo v16, "authorization"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string/jumbo v18, "Bearer "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v5, v0, v1}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    :try_start_0
    invoke-interface {v4, v5}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v13

    .line 391
    .local v13, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v13}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    .line 392
    .local v3, "entity":Lorg/apache/http/HttpEntity;
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v7

    .line 394
    .local v7, "is":Ljava/io/InputStream;
    new-instance v11, Ljava/io/BufferedReader;

    new-instance v16, Ljava/io/InputStreamReader;

    .line 395
    const-string/jumbo v17, "UTF-8"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v0, v7, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 394
    move-object/from16 v0, v16

    invoke-direct {v11, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 396
    .local v11, "reader":Ljava/io/BufferedReader;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 398
    .local v14, "sb":Ljava/lang/StringBuilder;
    :goto_0
    invoke-virtual {v11}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_0

    .line 402
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->onPostExecute(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v6

    .line 403
    .local v6, "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v6, :cond_1

    .line 404
    const/16 v16, 0x0

    .line 435
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    .end local v6    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7    # "is":Ljava/io/InputStream;
    .end local v11    # "reader":Ljava/io/BufferedReader;
    .end local v13    # "response":Lorg/apache/http/HttpResponse;
    .end local v14    # "sb":Ljava/lang/StringBuilder;
    :goto_1
    return v16

    .line 399
    .restart local v3    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v7    # "is":Ljava/io/InputStream;
    .restart local v11    # "reader":Ljava/io/BufferedReader;
    .restart local v13    # "response":Lorg/apache/http/HttpResponse;
    .restart local v14    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v17, "\n"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 429
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    .end local v7    # "is":Ljava/io/InputStream;
    .end local v11    # "reader":Ljava/io/BufferedReader;
    .end local v13    # "response":Lorg/apache/http/HttpResponse;
    .end local v14    # "sb":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v2

    .line 430
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 432
    const/16 v16, 0x0

    goto :goto_1

    .line 407
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v3    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v6    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v7    # "is":Ljava/io/InputStream;
    .restart local v11    # "reader":Ljava/io/BufferedReader;
    .restart local v13    # "response":Lorg/apache/http/HttpResponse;
    .restart local v14    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    :try_start_1
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v9

    .line 408
    .local v9, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-nez v9, :cond_2

    .line 409
    const/16 v16, 0x0

    goto :goto_1

    .line 412
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->resultData:Landroid/os/Bundle;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/os/Bundle;->clear()V

    .line 414
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_2
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-nez v16, :cond_3

    .line 419
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mHandler:Landroid/os/Handler;

    move-object/from16 v16, v0

    new-instance v17, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager$2;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager$2;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;)V

    invoke-virtual/range {v16 .. v17}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 435
    const/16 v16, 0x1

    goto :goto_1

    .line 414
    :cond_3
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 415
    .local v8, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->resultData:Landroid/os/Bundle;

    move-object/from16 v18, v0

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v8, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method private isRegisteredAccount(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 360
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 361
    .local v1, "manager":Landroid/accounts/AccountManager;
    if-nez v1, :cond_1

    .line 371
    :cond_0
    :goto_0
    return v2

    .line 365
    :cond_1
    const-string/jumbo v3, "com.osp.app.signin"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 367
    .local v0, "accountArr":[Landroid/accounts/Account;
    array-length v3, v0

    if-lez v3, :cond_0

    .line 371
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isServiceAvailable(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 352
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mISAService:Lcom/msc/sa/aidl/ISAService;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->isRegisteredAccount(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 353
    :cond_0
    const/4 v0, 0x0

    .line 356
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private onPostExecute(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 17
    .param p1, "result"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 440
    const/4 v11, 0x0

    .line 441
    .local v11, "isFamilyName":Z
    const/4 v12, 0x0

    .line 442
    .local v12, "isGivenName":Z
    const/4 v10, 0x0

    .line 443
    .local v10, "isEmail":Z
    const/4 v9, 0x0

    .line 445
    .local v9, "isCountryCode":Z
    const/4 v5, 0x0

    .line 446
    .local v5, "familyName":Ljava/lang/String;
    const/4 v7, 0x0

    .line 447
    .local v7, "givenName":Ljava/lang/String;
    const/4 v6, 0x0

    .line 448
    .local v6, "fullName":Ljava/lang/String;
    const/4 v14, 0x0

    .line 449
    .local v14, "receiveEmailText":Ljava/lang/String;
    const/4 v1, 0x0

    .line 451
    .local v1, "countryCode":Ljava/lang/String;
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 454
    .local v8, "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v4

    .line 455
    .local v4, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v13

    .line 456
    .local v13, "parser":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v15, Ljava/io/StringReader;

    move-object/from16 v0, p1

    invoke-direct {v15, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v13, v15}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 458
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    .line 459
    .local v3, "eventType":I
    :goto_0
    const/4 v15, 0x1

    if-ne v3, v15, :cond_0

    .line 520
    .end local v3    # "eventType":I
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v8    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v13    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :goto_1
    return-object v8

    .line 460
    .restart local v3    # "eventType":I
    .restart local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v8    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v13    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :cond_0
    packed-switch v3, :pswitch_data_0

    .line 510
    :cond_1
    :goto_2
    :pswitch_0
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    goto :goto_0

    .line 466
    :pswitch_1
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v15

    const-string/jumbo v16, "familyName"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 467
    const/4 v11, 0x1

    .line 469
    :cond_2
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v15

    const-string/jumbo v16, "givenName"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 470
    const/4 v12, 0x1

    .line 472
    :cond_3
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v15

    const-string/jumbo v16, "login_id"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 473
    const/4 v10, 0x1

    .line 475
    :cond_4
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v15

    const-string/jumbo v16, "countryCode"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 476
    const/4 v9, 0x1

    .line 479
    goto :goto_2

    .line 483
    :pswitch_2
    if-eqz v11, :cond_5

    .line 484
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v5

    .line 485
    const/4 v11, 0x0

    .line 486
    goto :goto_2

    :cond_5
    if-eqz v12, :cond_7

    .line 487
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v7

    .line 490
    const/4 v15, 0x0

    invoke-virtual {v5, v15}, Ljava/lang/String;->charAt(I)C

    move-result v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->isHangul(C)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 491
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 496
    :goto_3
    const-string/jumbo v15, "userName"

    invoke-virtual {v8, v15, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 497
    const/4 v12, 0x0

    .line 498
    goto :goto_2

    .line 493
    :cond_6
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_3

    .line 498
    :cond_7
    if-eqz v10, :cond_8

    .line 499
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v14

    .line 500
    const-string/jumbo v15, "eMail"

    invoke-virtual {v8, v15, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501
    const/4 v10, 0x0

    .line 502
    goto/16 :goto_2

    :cond_8
    if-eqz v9, :cond_1

    .line 503
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v1

    .line 504
    const-string/jumbo v15, "countryCode"

    invoke-virtual {v8, v15, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 505
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 512
    .end local v3    # "eventType":I
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v13    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :catch_0
    move-exception v2

    .line 513
    .local v2, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 514
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 515
    .end local v2    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v2

    .line 516
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 517
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 460
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public initialize(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->initialize(Landroid/content/Context;)Z

    .line 78
    const/4 v0, 0x1

    return v0
.end method

.method public isHangul(C)Z
    .locals 1
    .param p1, "c"    # C

    .prologue
    .line 524
    const v0, 0xac00

    if-lt p1, v0, :cond_0

    const v0, 0xd7a3

    if-le p1, v0, :cond_1

    .line 525
    :cond_0
    const/4 v0, 0x0

    .line 528
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public requestSamsungAccountAccessToken(Landroid/content/Context;Landroid/os/Bundle;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 183
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->isServiceAvailable(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v3

    .line 185
    invoke-interface {v3, v2, v6}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onReceiveSamsungAccountAccessToken(ZLandroid/os/Bundle;)V

    move v0, v2

    .line 205
    :cond_0
    :goto_0
    return v0

    .line 190
    :cond_1
    const/4 v0, 0x0

    .line 192
    .local v0, "bSuccess":Z
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mISAService:Lcom/msc/sa/aidl/ISAService;

    const/16 v4, 0xad2

    .line 193
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mRegistrationCode:Ljava/lang/String;

    .line 192
    invoke-interface {v3, v4, v5, p2}, Lcom/msc/sa/aidl/ISAService;->requestAccessToken(ILjava/lang/String;Landroid/os/Bundle;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 200
    :goto_1
    if-nez v0, :cond_0

    .line 201
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v3

    .line 202
    invoke-interface {v3, v2, v6}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onReceiveSamsungAccountAccessToken(ZLandroid/os/Bundle;)V

    goto :goto_0

    .line 194
    :catch_0
    move-exception v1

    .line 195
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 197
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public requestSamsungAccountAuthCode(Landroid/content/Context;Landroid/os/Bundle;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 265
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->isServiceAvailable(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 266
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v3

    .line 267
    invoke-interface {v3, v2, v6}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onReceiveSamsungAccountAuthCode(ZLandroid/os/Bundle;)V

    move v0, v2

    .line 288
    :cond_0
    :goto_0
    return v0

    .line 272
    :cond_1
    const/4 v0, 0x0

    .line 275
    .local v0, "bSuccess":Z
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mISAService:Lcom/msc/sa/aidl/ISAService;

    const/16 v4, 0xad5

    .line 276
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mRegistrationCode:Ljava/lang/String;

    .line 275
    invoke-interface {v3, v4, v5, p2}, Lcom/msc/sa/aidl/ISAService;->requestAuthCode(ILjava/lang/String;Landroid/os/Bundle;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 283
    :goto_1
    if-nez v0, :cond_0

    .line 284
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v3

    .line 285
    invoke-interface {v3, v2, v6}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onReceiveSamsungAccountAuthCode(ZLandroid/os/Bundle;)V

    goto :goto_0

    .line 277
    :catch_0
    move-exception v1

    .line 278
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 280
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public requestSamsungAccountChecklistValidation(Landroid/content/Context;Landroid/os/Bundle;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 210
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->isServiceAvailable(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 211
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v3

    .line 212
    invoke-interface {v3, v2, v6}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onReceiveSamsungAccountChecklistValidation(ZLandroid/os/Bundle;)V

    move v0, v2

    .line 232
    :cond_0
    :goto_0
    return v0

    .line 217
    :cond_1
    const/4 v0, 0x0

    .line 219
    .local v0, "bSuccess":Z
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mISAService:Lcom/msc/sa/aidl/ISAService;

    .line 220
    const/16 v4, 0xad3

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mRegistrationCode:Ljava/lang/String;

    .line 219
    invoke-interface {v3, v4, v5, p2}, Lcom/msc/sa/aidl/ISAService;->requestChecklistValidation(ILjava/lang/String;Landroid/os/Bundle;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 227
    :goto_1
    if-nez v0, :cond_0

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v3

    .line 229
    invoke-interface {v3, v2, v6}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onReceiveSamsungAccountChecklistValidation(ZLandroid/os/Bundle;)V

    goto :goto_0

    .line 221
    :catch_0
    move-exception v1

    .line 222
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 224
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public requestSamsungAccountConnectService(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 112
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->isBound:Z

    if-nez v0, :cond_0

    .line 113
    new-instance v0, Landroid/content/Intent;

    .line 114
    const-string/jumbo v1, "com.msc.action.samsungaccount.REQUEST_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 115
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    .line 113
    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->isBound:Z

    .line 117
    :cond_0
    return-void
.end method

.method public requestSamsungAccountDisclaimerAgreement(Landroid/content/Context;Landroid/os/Bundle;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 237
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->isServiceAvailable(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 238
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v3

    .line 239
    invoke-interface {v3, v2, v6}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onReceiveSamsungAccountDisclaimerAgreement(ZLandroid/os/Bundle;)V

    move v0, v2

    .line 261
    :cond_0
    :goto_0
    return v0

    .line 244
    :cond_1
    const/4 v0, 0x0

    .line 247
    .local v0, "bSuccess":Z
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mISAService:Lcom/msc/sa/aidl/ISAService;

    .line 248
    const/16 v4, 0xad4

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mRegistrationCode:Ljava/lang/String;

    .line 247
    invoke-interface {v3, v4, v5, p2}, Lcom/msc/sa/aidl/ISAService;->requestDisclaimerAgreement(ILjava/lang/String;Landroid/os/Bundle;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 256
    :goto_1
    if-nez v0, :cond_0

    .line 257
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v3

    .line 258
    invoke-interface {v3, v2, v6}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onReceiveSamsungAccountDisclaimerAgreement(ZLandroid/os/Bundle;)V

    goto :goto_0

    .line 250
    :catch_0
    move-exception v1

    .line 251
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 253
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public requestSamsungAccountDisconnectService(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 121
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mRegistrationCode:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mISAService:Lcom/msc/sa/aidl/ISAService;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mRegistrationCode:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/msc/sa/aidl/ISAService;->unregisterCallback(Ljava/lang/String;)Z

    .line 123
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mRegistrationCode:Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mISAService:Lcom/msc/sa/aidl/ISAService;

    if-nez v1, :cond_2

    .line 132
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mServiceConnection:Landroid/content/ServiceConnection;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->isBound:Z

    if-eqz v1, :cond_1

    .line 133
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 134
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 135
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->isBound:Z

    .line 143
    :cond_1
    :goto_0
    return-void

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 140
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 141
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->isBound:Z

    .line 142
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mServiceConnection:Landroid/content/ServiceConnection;

    goto :goto_0
.end method

.method public requestSamsungAccountRegisterCallback(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "clientID"    # Ljava/lang/String;
    .param p3, "clientSecret"    # Ljava/lang/String;
    .param p4, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 148
    :try_start_0
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/ISACallbackImp;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v2

    .line 149
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/ISACallbackImp;-><init>(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;)V

    .line 148
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mSACallback:Lcom/msc/sa/aidl/ISACallback;

    .line 150
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mISAService:Lcom/msc/sa/aidl/ISAService;

    .line 151
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mSACallback:Lcom/msc/sa/aidl/ISACallback;

    .line 150
    invoke-interface {v1, p2, p3, p4, v2}, Lcom/msc/sa/aidl/ISAService;->registerCallback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/sa/aidl/ISACallback;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mRegistrationCode:Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mRegistrationCode:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v1

    .line 162
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onResponseSamsungAccountRegisterCallback(Z)V

    .line 167
    :goto_0
    return-void

    .line 152
    :catch_0
    move-exception v0

    .line 153
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v1

    .line 156
    invoke-interface {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onResponseSamsungAccountRegisterCallback(Z)V

    goto :goto_0

    .line 164
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v1

    .line 165
    invoke-interface {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onResponseSamsungAccountRegisterCallback(Z)V

    goto :goto_0
.end method

.method public requestSamsungAccountSCloudAccessToken(Landroid/content/Context;Landroid/os/Bundle;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 293
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->isServiceAvailable(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 294
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v3

    .line 295
    invoke-interface {v3, v2, v6}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onReceiveSamsungAccountSCloudAccessToken(ZLandroid/os/Bundle;)V

    move v0, v2

    .line 316
    :cond_0
    :goto_0
    return v0

    .line 300
    :cond_1
    const/4 v0, 0x0

    .line 303
    .local v0, "bSuccess":Z
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mISAService:Lcom/msc/sa/aidl/ISAService;

    .line 304
    const/16 v4, 0xad6

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mRegistrationCode:Ljava/lang/String;

    .line 303
    invoke-interface {v3, v4, v5, p2}, Lcom/msc/sa/aidl/ISAService;->requestSCloudAccessToken(ILjava/lang/String;Landroid/os/Bundle;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 311
    :goto_1
    if-nez v0, :cond_0

    .line 312
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v3

    .line 313
    invoke-interface {v3, v2, v6}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onReceiveSamsungAccountSCloudAccessToken(ZLandroid/os/Bundle;)V

    goto :goto_0

    .line 305
    :catch_0
    move-exception v1

    .line 306
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 308
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public requestSamsungAccountUnregisterCallback(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 171
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mISAService:Lcom/msc/sa/aidl/ISAService;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mRegistrationCode:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/msc/sa/aidl/ISAService;->unregisterCallback(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mRegistrationCode:Ljava/lang/String;

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v1

    .line 179
    invoke-interface {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onResponseSamsungAccountUnregisterCallback()V

    .line 180
    return-void

    .line 172
    :catch_0
    move-exception v0

    .line 173
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public requestSamsungAccountUserName(Landroid/content/Context;Landroid/os/Bundle;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 320
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->isServiceAvailable(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 321
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v3

    .line 322
    invoke-interface {v3, v2, v5}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onReceiveSamsungAccountUserName(ZLandroid/os/Bundle;)V

    move v0, v2

    .line 346
    :cond_0
    :goto_0
    return v0

    .line 327
    :cond_1
    const/4 v0, 0x0

    .line 331
    .local v0, "bSuccess":Z
    :try_start_0
    const-string/jumbo v3, "user_id"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mUserId:Ljava/lang/String;

    .line 332
    const-string/jumbo v3, "access_token"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mAccessToken:Ljava/lang/String;

    .line 334
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mUserId:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mAccessToken:Ljava/lang/String;

    invoke-direct {p0, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getUserName(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 341
    :goto_1
    if-nez v0, :cond_0

    .line 342
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v3

    .line 343
    invoke-interface {v3, v2, v5}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onReceiveSamsungAccountUserName(ZLandroid/os/Bundle;)V

    goto :goto_0

    .line 335
    :catch_0
    move-exception v1

    .line 336
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 338
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->mISAService:Lcom/msc/sa/aidl/ISAService;

    .line 85
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->terminate(Landroid/content/Context;)V

    .line 86
    return-void
.end method
