.class public interface abstract Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;
.super Ljava/lang/Object;
.source "OnMusicPlaybackRequestListener.java"


# static fields
.field public static final PLAYBACK_REQUEST_PAUSE:I = 0x0

.field public static final PLAYBACK_REQUEST_PHONE_PAUSE:I = 0x2

.field public static final PLAYBACK_REQUEST_PHONE_RESUME:I = 0x3

.field public static final PLAYBACK_REQUEST_RESUME:I = 0x1


# virtual methods
.method public abstract onPlaybackRequest(I)V
.end method
