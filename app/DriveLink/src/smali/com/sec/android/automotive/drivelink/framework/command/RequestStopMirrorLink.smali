.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestStopMirrorLink;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestStopMirrorLink.java"


# instance fields
.field mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .prologue
    .line 14
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 15
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestStopMirrorLink;->mContext:Landroid/content/Context;

    .line 16
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestStopMirrorLink;->mContext:Landroid/content/Context;

    .line 32
    return-void
.end method

.method protected process()V
    .locals 2

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestStopMirrorLink;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 21
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMirrorLinkManager()Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;

    move-result-object v0

    .line 22
    .local v0, "mirrorlinkMngr":Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestStopMirrorLink;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;->stopMirrorLink(Landroid/content/Context;)V

    .line 23
    return-void
.end method

.method protected response()V
    .locals 0

    .prologue
    .line 27
    return-void
.end method
