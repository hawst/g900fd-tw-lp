.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLBTParingInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getBTPairingType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;
.end method

.method public abstract getDeviceName()Ljava/lang/String;
.end method

.method public abstract getPassKey()I
.end method
