.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationGeneralMessage.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "[NotificationGeneralMessage]"

.field private static final TIMEOUT_SEC:I = 0x5


# instance fields
.field private mContext:Landroid/content/Context;

.field private tvMessage:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    .line 35
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->tvMessage:Landroid/widget/TextView;

    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->initNotification(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->tvMessage:Landroid/widget/TextView;

    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->initNotification(Landroid/content/Context;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->tvMessage:Landroid/widget/TextView;

    .line 24
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->initNotification(Landroid/content/Context;)V

    .line 25
    return-void
.end method

.method private initNotification(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    const-string/jumbo v5, "[NotificationGeneralMessage]"

    const-string/jumbo v6, "initNotification"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->mContext:Landroid/content/Context;

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 47
    const-string/jumbo v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 46
    check-cast v0, Landroid/view/LayoutInflater;

    .line 49
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const/4 v1, 0x0

    .line 50
    .local v1, "layout":I
    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->isMultiMode:Z

    if-eqz v5, :cond_1

    .line 51
    const v1, 0x7f0300a7

    .line 56
    :goto_0
    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 58
    .local v4, "view":Landroid/view/View;
    const v5, 0x7f0902cf

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->tvMessage:Landroid/widget/TextView;

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->hasNotiItemInfo()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v2

    .line 61
    check-cast v2, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;

    .line 64
    .local v2, "notiInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5}, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->getMessage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 65
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5}, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->getMessage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 66
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5}, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->getMessage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 67
    .local v3, "txt":Ljava/lang/String;
    const-string/jumbo v5, "[NotificationGeneralMessage]"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Noti Message:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->tvMessage:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v6}, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->getMessage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5}, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->getTTS(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 70
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5}, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->getTTS(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    .line 75
    .end local v2    # "notiInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;
    .end local v3    # "txt":Ljava/lang/String;
    :cond_0
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->setUseAutoTimeout(Z)V

    .line 76
    return-void

    .line 53
    .end local v4    # "view":Landroid/view/View;
    :cond_1
    const v1, 0x7f0300a6

    goto :goto_0
.end method


# virtual methods
.method protected getTTSText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->hasNotiItemInfo()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    .line 109
    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;

    .line 112
    .local v0, "notiInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->getTTS(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 113
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->getTTS(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 114
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->getTTS(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 117
    .end local v0    # "notiInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;
    :goto_0
    return-object v1

    :cond_0
    const-string/jumbo v1, ""

    goto :goto_0
.end method

.method protected getTimeout()I
    .locals 2

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->hasNotiItemInfo()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    .line 80
    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;

    .line 83
    .local v0, "notiInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->getTimeout()I

    move-result v1

    if-lez v1, :cond_0

    .line 84
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->getTimeout()I

    move-result v1

    .line 88
    .end local v0    # "notiInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x5

    goto :goto_0
.end method

.method protected startDefaultNotiTimeoutFlow()V
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addBegin()V

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->getTTSText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->getTTSText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->getTTSText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTS(Ljava/lang/String;)V

    .line 99
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->getTimeout()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addFinish()V

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdFlowStart()V

    .line 103
    :cond_1
    return-void
.end method
