.class Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$3;
.super Ljava/lang/Object;
.source "RegulationsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->setupView(Landroid/widget/TextView;Landroid/widget/CheckBox;Landroid/widget/TextView;Landroid/widget/LinearLayout;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;

.field private final synthetic val$cbTermsofServiceAgree:Landroid/widget/CheckBox;

.field private final synthetic val$index:I


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;Landroid/widget/CheckBox;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$3;->val$cbTermsofServiceAgree:Landroid/widget/CheckBox;

    iput p3, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$3;->val$index:I

    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 203
    instance-of v2, p1, Landroid/widget/CheckBox;

    if-eqz v2, :cond_1

    .line 204
    check-cast p1, Landroid/widget/CheckBox;

    .end local p1    # "view":Landroid/view/View;
    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 208
    .local v0, "checked":Z
    :goto_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$3;->val$cbTermsofServiceAgree:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 209
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->isChecked:[Z
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->access$0(Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;)[Z

    move-result-object v2

    iget v3, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$3;->val$index:I

    aput-boolean v0, v2, v3

    .line 210
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->toggleEnableNext()V
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->access$1(Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;)V

    .line 212
    invoke-static {}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->getUserRegulationAgreement()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 213
    if-nez v0, :cond_0

    .line 215
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->setUserRegulationAgreement(Z)V

    .line 217
    :cond_0
    return-void

    .line 206
    .end local v0    # "checked":Z
    .restart local p1    # "view":Landroid/view/View;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$3;->val$cbTermsofServiceAgree:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .restart local v0    # "checked":Z
    :goto_1
    goto :goto_0

    .end local v0    # "checked":Z
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method
