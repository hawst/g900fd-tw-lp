.class Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;
.super Ljava/lang/Thread;
.source "NotificationMusicView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FForwardRewindThread"
.end annotation


# instance fields
.field private isFFRE:Z

.field public isRunning:Z

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;Z)V
    .locals 1
    .param p2, "value"    # Z

    .prologue
    .line 791
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 788
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;->isRunning:Z

    .line 789
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;->isFFRE:Z

    .line 792
    iput-boolean p2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;->isFFRE:Z

    .line 793
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 799
    :cond_0
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;->isRunning:Z

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isMusicSetValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 800
    const-wide/16 v1, 0x190

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    .line 801
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;->isFFRE:Z

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setFForwardRewind(Z)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 807
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;->isInterrupted()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 808
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 809
    :goto_0
    return-void

    .line 803
    :catch_0
    move-exception v0

    .line 804
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
