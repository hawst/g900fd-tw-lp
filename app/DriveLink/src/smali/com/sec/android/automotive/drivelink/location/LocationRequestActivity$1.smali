.class Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;
.super Ljava/lang/Object;
.source "LocationRequestActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;)Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    return-object v0
.end method


# virtual methods
.method public onResponseRequestContactList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 196
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    return-void
.end method

.method public onResponseRequestSearchedContactList(Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    const/4 v8, 0x0

    .line 117
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 118
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setResutlText(I)V

    .line 119
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mEmptyListLayout:Landroid/view/View;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$1(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 120
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$2(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 121
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$3(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 122
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$3(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 123
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$3(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->invalidate()V

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    invoke-static {p1}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContactMatchList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 128
    .local v0, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 129
    .local v2, "size":I
    const/16 v3, 0x28

    if-le v2, v3, :cond_2

    .line 130
    const/16 v2, 0x28

    .line 131
    :cond_2
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setResutlText(I)V

    .line 132
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mEmptyListLayout:Landroid/view/View;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$1(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 133
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$2(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 135
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    .line 136
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->adjustToMaxSizeSuggestionList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v4

    .line 135
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$4(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;Ljava/util/ArrayList;)V

    .line 137
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    new-instance v4, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    .line 138
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mContactList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$5(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Ljava/util/ArrayList;

    move-result-object v6

    .line 139
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->getListItemOnClickListener()Landroid/view/View$OnClickListener;
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$6(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/view/View$OnClickListener;

    move-result-object v7

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V

    .line 137
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$7(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;)V

    .line 140
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$2(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$8(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 141
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    new-instance v4, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    .line 142
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;-><init>(Landroid/content/Context;)V

    .line 141
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$9(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;)V

    .line 143
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 144
    const/4 v3, -0x1

    const/4 v4, -0x2

    .line 143
    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 145
    .local v1, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v3, 0xd

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 146
    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 147
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$10(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 148
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$10(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v3

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setGravity(I)V

    .line 149
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$3(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/widget/RelativeLayout;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$10(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 150
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$10(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v3

    .line 151
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$8(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->getPageCount()I

    move-result v4

    .line 152
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$11(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/view/View$OnClickListener;

    move-result-object v5

    .line 150
    invoke-virtual {v3, v4, v8, v5}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setDotIndicator(IILandroid/view/View$OnClickListener;)V

    .line 153
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$2(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v3

    .line 154
    new-instance v4, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1$1;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1$1;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;)V

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 187
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isSearchMode()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 188
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->startConfirm(I)V
    invoke-static {v3, v8}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$14(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;I)V

    goto/16 :goto_0
.end method
