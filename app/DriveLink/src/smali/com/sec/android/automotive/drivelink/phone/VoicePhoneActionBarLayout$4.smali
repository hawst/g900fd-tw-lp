.class Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$4;
.super Ljava/lang/Object;
.source "VoicePhoneActionBarLayout.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->runTTSBarAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$4;->this$0:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    .line 595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 619
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$4;->this$0:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->access$15(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;Z)V

    .line 620
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 611
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$4;->this$0:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->isRunning:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->access$11(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$4;->this$0:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$4;->this$0:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mDirection:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->access$12(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->access$13(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;Z)V

    .line 613
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$4;->this$0:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    # invokes: Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->runTTSBarAnimation()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->access$14(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)V

    .line 615
    :cond_0
    return-void

    .line 612
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 607
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 601
    return-void
.end method
