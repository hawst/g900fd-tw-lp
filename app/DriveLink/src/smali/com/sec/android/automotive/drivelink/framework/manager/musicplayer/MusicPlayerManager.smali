.class public Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
.super Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;
.source "MusicPlayerManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;,
        Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;,
        Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;,
        Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$framework$manager$musicplayer$MusicPlayerManager$MusicDataEject:[I = null

.field private static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$framework$manager$musicplayer$MusicPlayerManager$MusicRepeatType:[I = null

.field private static final EXTERNAL_SDCARD_PATH:Ljava/lang/String; = "extsdcard/"

.field private static final PRIVATE_MODE_CHECK_STRING:Ljava/lang/String; = "/private/"

.field private static final PRIVATE_MODE_CHECK_STRING_KO:Ljava/lang/String; = "/PersonalPage/"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

.field private mMusicPlayerStatus:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

.field private mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

.field private mRecurrence:Z

.field private mbEjectPlayState:Z

.field private mbInitialized:Z


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$framework$manager$musicplayer$MusicPlayerManager$MusicDataEject()[I
    .locals 3

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$framework$manager$musicplayer$MusicPlayerManager$MusicDataEject:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;->values()[Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;->LOCAL:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;->PRIVATE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;->SDCARD:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$framework$manager$musicplayer$MusicPlayerManager$MusicDataEject:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$framework$manager$musicplayer$MusicPlayerManager$MusicRepeatType()[I
    .locals 3

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$framework$manager$musicplayer$MusicPlayerManager$MusicRepeatType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->values()[Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->ALL:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->NONE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->ONE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$framework$manager$musicplayer$MusicPlayerManager$MusicRepeatType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;-><init>()V

    .line 53
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 54
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->NONE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerStatus:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    .line 55
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    .line 56
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    .line 57
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mbInitialized:Z

    .line 58
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mContext:Landroid/content/Context;

    .line 59
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mbEjectPlayState:Z

    .line 60
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mRecurrence:Z

    .line 63
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 64
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->NONE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerStatus:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    .line 65
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    .line 66
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mContext:Landroid/content/Context;

    .line 67
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    return-object v0
.end method

.method static synthetic access$1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerStatus:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    return-object v0
.end method

.method private getMusicPlayerEnvInfo()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    return-object v0
.end method

.method private getMusicPlayerStatus()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;
    .locals 1

    .prologue
    .line 640
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerStatus:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    return-object v0
.end method

.method private isInitialized()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mbInitialized:Z

    return v0
.end method

.method private logMusic(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 656
    return-void
.end method

.method private logMusicList(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 677
    .local p2, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method private moveToFirst()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .locals 1

    .prologue
    .line 624
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->moveToFirst()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    return-object v0
.end method

.method private moveToLast()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->moveToLast()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    return-object v0
.end method

.method private moveToNext()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .locals 1

    .prologue
    .line 632
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->moveToNext()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    return-object v0
.end method

.method private moveToPrev()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .locals 1

    .prologue
    .line 636
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->moveToPrev()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    return-object v0
.end method

.method private playMusic(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Z)Z
    .locals 2
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .param p2, "bPlayNow"    # Z

    .prologue
    .line 680
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setMusicToMediaPlayer(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 681
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "playMusic - setMusicToMedia = true"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    if-eqz p2, :cond_0

    .line 684
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 685
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->STARTED:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setPlayerStatus(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;)V

    .line 690
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDriveLinkMusicPlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;->onMusicPlayStarted(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Z)V

    .line 693
    const/4 v0, 0x1

    .line 695
    :goto_1
    return v0

    .line 687
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->PAUSED:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setPlayerStatus(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;)V

    goto :goto_0

    .line 695
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private restoreMusicPlayerEnvInfo()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;
    .locals 1

    .prologue
    .line 820
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->restoreMusicPlayerEnvInfo()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    move-result-object v0

    return-object v0
.end method

.method private saveMusicPlayerEnvInfo()V
    .locals 3

    .prologue
    .line 824
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    if-eqz v1, :cond_0

    .line 826
    :try_start_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v1

    .line 827
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    .line 826
    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->saveMusicPlayerEnvInfo(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 832
    :cond_0
    :goto_0
    return-void

    .line 828
    :catch_0
    move-exception v0

    .line 829
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private setAllMusicListShuffledStartWithMusicItem(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Z
    .locals 6
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 587
    if-nez p1, :cond_1

    .line 604
    :cond_0
    :goto_0
    return v1

    .line 591
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v3

    .line 592
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Ljava/util/ArrayList;

    move-result-object v0

    .line 593
    .local v0, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 597
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->setMusicPlaylist(Ljava/util/ArrayList;I)V

    .line 598
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setShuffle(Z)V

    .line 600
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getId()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->moveToMusicId(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 602
    const-string/jumbo v1, "setAllMusicListShuffledStartWithMusicItem"

    invoke-direct {p0, v1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->logMusicList(Ljava/lang/String;Ljava/util/ArrayList;)V

    move v1, v2

    .line 604
    goto :goto_0
.end method

.method private setIntialized(Z)V
    .locals 0
    .param p1, "bInit"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mbInitialized:Z

    .line 71
    return-void
.end method

.method private setMusicListAvailable(Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 700
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->getMusicPlayIndex()I

    move-result v3

    .line 701
    .local v3, "playIndex":I
    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "playindex : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " list Size : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 703
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_4

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    .line 704
    move v2, v3

    .local v2, "main":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_0

    .line 729
    .end local v2    # "main":I
    :goto_1
    return-void

    .line 705
    .restart local v2    # "main":I
    :cond_0
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 706
    .local v0, "compMusic":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 707
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v4

    .line 708
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->updatePlayedMusicMetaData(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Z

    .line 710
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 711
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 712
    .local v1, "fp":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 713
    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "play Music : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mRecurrence:Z

    if-eqz v4, :cond_2

    .line 715
    iput-boolean v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mRecurrence:Z

    goto :goto_1

    .line 718
    :cond_2
    iput-boolean v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mRecurrence:Z

    .line 719
    invoke-direct {p0, v0, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->playMusic(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Z)Z

    goto :goto_1

    .line 704
    .end local v1    # "fp":Ljava/io/File;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 725
    .end local v0    # "compMusic":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .end local v2    # "main":I
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDriveLinkMusicPlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v4

    .line 726
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    const v6, 0x15f90

    .line 727
    const/4 v7, 0x0

    .line 725
    invoke-interface {v4, v5, v6, v8, v7}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;->onMusicPlayError(Landroid/media/MediaPlayer;IILjava/lang/Object;)V

    goto :goto_1
.end method

.method private setMusicToMediaPlayer(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Z
    .locals 11
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    const/4 v7, 0x0

    .line 732
    if-eqz p1, :cond_0

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v8, :cond_1

    .line 816
    :cond_0
    :goto_0
    return v7

    .line 736
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_2

    .line 741
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v8

    .line 742
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mContext:Landroid/content/Context;

    .line 741
    invoke-virtual {v8, v9, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->updatePlayedMusicMetaData(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Z

    .line 754
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v8

    .line 755
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mContext:Landroid/content/Context;

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Ljava/util/ArrayList;

    move-result-object v0

    .line 756
    .local v0, "allMusicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->getMusicPlaylist()Ljava/util/ArrayList;

    move-result-object v6

    .line 757
    .local v6, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_4

    .line 758
    :cond_3
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v8, v0, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->setMusicPlaylist(Ljava/util/ArrayList;I)V

    .line 759
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDriveLinkMusicPlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v8

    .line 760
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    const v10, 0x15f90

    .line 759
    invoke-interface {v8, v9, v10, v7, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;->onMusicPlayError(Landroid/media/MediaPlayer;IILjava/lang/Object;)V

    .line 764
    :cond_4
    const/4 v4, 0x0

    .line 767
    .local v4, "fis":Ljava/io/FileInputStream;
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v8}, Landroid/media/MediaPlayer;->reset()V

    .line 768
    new-instance v5, Ljava/io/FileInputStream;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 769
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .local v5, "fis":Ljava/io/FileInputStream;
    :try_start_1
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V

    .line 770
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v8}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 804
    if-eqz v5, :cond_5

    .line 806
    :try_start_2
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 811
    :cond_5
    :goto_1
    const/4 v4, 0x0

    .line 814
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    const-string/jumbo v7, "playMusic - setMusicToMediaPlayer():Success"

    invoke-direct {p0, v7, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->logMusic(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    .line 816
    const/4 v7, 0x1

    goto :goto_0

    .line 771
    :catch_0
    move-exception v2

    .line 773
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v8}, Landroid/media/MediaPlayer;->reset()V

    .line 774
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->NONE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    iput-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerStatus:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    .line 776
    invoke-direct {p0, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setMusicListAvailable(Ljava/util/ArrayList;)V

    .line 777
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)V

    .line 778
    .local v1, "checker":Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->run()V

    .line 792
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 794
    if-eqz v4, :cond_6

    .line 795
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 797
    :cond_6
    const/4 v4, 0x0

    .line 804
    :goto_3
    if-eqz v4, :cond_7

    .line 806
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 811
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_7
    :goto_4
    const/4 v4, 0x0

    .line 802
    goto/16 :goto_0

    .line 798
    .restart local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 799
    .local v3, "ex":Ljava/lang/Exception;
    :try_start_6
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_3

    .line 803
    .end local v1    # "checker":Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    .line 804
    :goto_5
    if-eqz v4, :cond_8

    .line 806
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 811
    :cond_8
    :goto_6
    const/4 v4, 0x0

    .line 812
    throw v7

    .line 807
    .restart local v1    # "checker":Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;
    .restart local v2    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v2

    .line 808
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 807
    .end local v1    # "checker":Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 808
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 807
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catch_4
    move-exception v2

    .line 808
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 803
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_5

    .line 771
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catch_5
    move-exception v2

    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method private setPlayerStatus(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;)V
    .locals 0
    .param p1, "status"    # Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    .prologue
    .line 644
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerStatus:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    .line 645
    return-void
.end method


# virtual methods
.method public getCurrentPosition()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 385
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 397
    :cond_0
    :goto_0
    return v0

    .line 389
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 390
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMusicPlayerStatus()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    move-result-object v1

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->STOPPED:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    if-eq v1, v2, :cond_0

    .line 394
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    .line 395
    .local v0, "position":I
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Log Music: getCurrentPosition() - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 401
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 402
    const/4 v0, 0x0

    .line 409
    :goto_0
    return v0

    .line 405
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_1

    .line 406
    const/4 v0, -0x1

    goto :goto_0

    .line 409
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    goto :goto_0
.end method

.method public getMusicPlayIndex()I
    .locals 2

    .prologue
    .line 251
    const-string/jumbo v0, "getMusicPlayIndex()"

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->logMusic(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->getMusicPlayIndex()I

    move-result v0

    return v0
.end method

.method public getMusicPlaylist()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 608
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 609
    const/4 v0, 0x0

    .line 612
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->getMusicPlaylist()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getPlayerStatus()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;
    .locals 1

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 175
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->NONE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    .line 178
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerStatus:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    goto :goto_0
.end method

.method public getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .locals 1

    .prologue
    .line 616
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 617
    const/4 v0, 0x0

    .line 620
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    goto :goto_0
.end method

.method public getRepeatType()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;
    .locals 1

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 183
    const/4 v0, 0x0

    .line 190
    :goto_0
    return-object v0

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    if-nez v0, :cond_1

    .line 187
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    .line 190
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->getMusicRepeatType()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    move-result-object v0

    goto :goto_0
.end method

.method public handleMediaEject()V
    .locals 3

    .prologue
    .line 928
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "handleMediaEject"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 929
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isPlayingMusicInPrivateMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 930
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isPlayerPlaying()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mbEjectPlayState:Z

    .line 931
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "handleMediaEject : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mbEjectPlayState:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 932
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "handleMediaEject : stopPlayer"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 933
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 934
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 936
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->NONE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerStatus:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    .line 937
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;->PRIVATE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setMusicControllerNativeMusic(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;)V

    .line 938
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "mMediaPlayer stop"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 940
    :cond_0
    return-void
.end method

.method public handleMusicListRenew()V
    .locals 5

    .prologue
    .line 943
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->getMusicPlaylist()Ljava/util/ArrayList;

    move-result-object v1

    .line 945
    .local v1, "playlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 946
    :cond_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "handleMusicListRenew Local is null setting playlist"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 947
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v2

    .line 948
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Ljava/util/ArrayList;

    move-result-object v0

    .line 949
    .local v0, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->setMusicPlaylist(Ljava/util/ArrayList;I)V

    .line 950
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setShuffle(Z)V

    .line 952
    .end local v0    # "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    :cond_1
    return-void
.end method

.method public handlePlayOnCompletion()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 835
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 836
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->getMusicPlaylist()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_1

    .line 867
    :cond_0
    :goto_0
    return-void

    .line 840
    :cond_1
    const/4 v0, 0x0

    .line 842
    .local v0, "nextMusic":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$framework$manager$musicplayer$MusicPlayerManager$MusicRepeatType()[I

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->getMusicRepeatType()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 844
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->moveToNext()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 846
    if-nez v0, :cond_2

    .line 847
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->stopPlayer()V

    goto :goto_0

    .line 849
    :cond_2
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->startPlayer(Z)V

    goto :goto_0

    .line 854
    :pswitch_1
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->startPlayer(Z)V

    goto :goto_0

    .line 858
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->moveToNext()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 860
    if-nez v0, :cond_3

    .line 861
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->moveToFirst()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 864
    :cond_3
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->startPlayer(Z)V

    goto :goto_0

    .line 842
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public handleSDCardEject()V
    .locals 3

    .prologue
    .line 910
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "handleSDCardEject"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 911
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isPlayingMusicInSDCard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 912
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isPlayerPlaying()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mbEjectPlayState:Z

    .line 913
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "handleMediaEject : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mbEjectPlayState:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 914
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "handleSDCardEject : stopPlayer"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 915
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 916
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 917
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 918
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 919
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "mMediaPlayer stop"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 921
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->NONE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerStatus:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    .line 922
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;->SDCARD:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setMusicControllerNativeMusic(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;)V

    .line 925
    :cond_0
    return-void
.end method

.method public initialize(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 79
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    const/4 v0, 0x0

    .line 140
    :goto_0
    return v0

    .line 83
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->initialize(Landroid/content/Context;)Z

    .line 85
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mContext:Landroid/content/Context;

    .line 87
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 89
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->STOPPED:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerStatus:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    .line 92
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->restoreMusicPlayerEnvInfo()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    .line 94
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 95
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMusicPlaylistManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    move-result-object v1

    .line 94
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    .line 96
    const-string/jumbo v1, "Restored Music Play List"

    .line 97
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->getMusicPlaylist()Ljava/util/ArrayList;

    move-result-object v2

    .line 96
    invoke-direct {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->logMusicList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 100
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->isShuffled()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->setShuffle(Z)V

    .line 102
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$1;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 113
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$2;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$2;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 121
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$3;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$3;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 138
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setIntialized(Z)V

    goto :goto_0
.end method

.method public isPlayerPlaying()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 425
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 433
    :cond_0
    :goto_0
    return v0

    .line 429
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 433
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    goto :goto_0
.end method

.method public isPlayingMusicInPrivateMode()Z
    .locals 4

    .prologue
    .line 870
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "isPlayingMusicInPrivateMode"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 873
    .local v0, "currentMusic":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    .line 874
    .local v1, "locale":Ljava/util/Locale;
    if-eqz v0, :cond_0

    .line 876
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v2

    .line 877
    invoke-virtual {v2, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 878
    const-string/jumbo v3, "/private/"

    invoke-virtual {v3, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 880
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v2

    .line 881
    invoke-virtual {v2, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 883
    const-string/jumbo v3, "/PersonalPage/"

    .line 884
    invoke-virtual {v3, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 882
    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    .line 884
    if-eqz v2, :cond_1

    .line 885
    :cond_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "isPlayingMusicInPrivateMode : true"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 886
    const/4 v2, 0x1

    .line 889
    :goto_0
    return v2

    .line 888
    :cond_1
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "isPlayingMusicInPrivateMode : false"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 889
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isPlayingMusicInSDCard()Z
    .locals 4

    .prologue
    .line 893
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "isPlayingMusicInSDCard"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 894
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 896
    .local v0, "currentMusic":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    .line 897
    .local v1, "locale":Ljava/util/Locale;
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 899
    if-eqz v0, :cond_0

    .line 900
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 901
    const-string/jumbo v3, "extsdcard/"

    invoke-virtual {v3, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 902
    :cond_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "isPlayingMusicInSDCard : true"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 903
    const/4 v2, 0x1

    .line 906
    :goto_0
    return v2

    .line 905
    :cond_1
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "isPlayingMusicInSDCard : false"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 906
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isShuffled()Z
    .locals 1

    .prologue
    .line 211
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    const/4 v0, 0x0

    .line 219
    :goto_0
    return v0

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    if-nez v0, :cond_1

    .line 216
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    .line 219
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->isShuffle()Z

    move-result v0

    goto :goto_0
.end method

.method public pausePlayer(Z)V
    .locals 4
    .param p1, "isByVoice"    # Z

    .prologue
    .line 315
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_1

    .line 334
    :cond_0
    :goto_0
    return-void

    .line 319
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_0

    .line 323
    if-nez p1, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayerStatus()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    move-result-object v2

    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->RESUMED:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    if-eq v2, v3, :cond_2

    .line 324
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayerStatus()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    move-result-object v2

    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->STARTED:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    if-ne v2, v3, :cond_0

    .line 325
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->pause()V

    .line 327
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->PAUSED:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setPlayerStatus(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;)V

    .line 329
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 330
    .local v0, "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getCurrentPosition()I

    move-result v1

    .line 331
    .local v1, "position":I
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDriveLinkMusicPlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;->onMusicPlayPaused(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;I)V

    goto :goto_0
.end method

.method public playNextMusic(Z)Z
    .locals 3
    .param p1, "bPlayNow"    # Z

    .prologue
    .line 257
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 258
    const/4 v1, 0x0

    .line 274
    :goto_0
    return v1

    .line 261
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->moveToNext()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 263
    .local v0, "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getRepeatType()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    move-result-object v1

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->ALL:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    if-ne v1, v2, :cond_1

    .line 264
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->moveToFirst()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 266
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "playNextMusic("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 267
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 268
    const-string/jumbo v2, ") : if(music == null && getRepeatType() == MusicRepeatType.ALL)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 266
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 269
    const/4 v2, 0x0

    .line 265
    invoke-direct {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->logMusic(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    .line 272
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "playNextMusic("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->logMusic(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    .line 274
    invoke-direct {p0, v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->playMusic(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Z)Z

    move-result v1

    goto :goto_0
.end method

.method public playPrevMusic(Z)Z
    .locals 3
    .param p1, "bPlayNow"    # Z

    .prologue
    .line 278
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 279
    const/4 v1, 0x0

    .line 295
    :goto_0
    return v1

    .line 282
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->moveToPrev()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 284
    .local v0, "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getRepeatType()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    move-result-object v1

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->ALL:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    if-ne v1, v2, :cond_1

    .line 285
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->moveToLast()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 287
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "playPrevMusic("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 288
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 289
    const-string/jumbo v2, ") : if(music == null && getRepeatType() == MusicRepeatType.ALL)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 287
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 290
    const/4 v2, 0x0

    .line 286
    invoke-direct {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->logMusic(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    .line 293
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "playPrevMusic("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->logMusic(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    .line 295
    invoke-direct {p0, v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->playMusic(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Z)Z

    move-result v1

    goto :goto_0
.end method

.method public resumePlayer()V
    .locals 3

    .prologue
    .line 337
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 341
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 345
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayerStatus()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    move-result-object v1

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->PAUSED:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    if-ne v1, v2, :cond_2

    .line 346
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 348
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->RESUMED:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setPlayerStatus(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;)V

    .line 350
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 351
    .local v0, "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDriveLinkMusicPlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;->onMusicPlayResumed(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    goto :goto_0

    .line 354
    .end local v0    # "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayerStatus()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    move-result-object v1

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->NONE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    if-eq v1, v2, :cond_3

    .line 355
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayerStatus()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    move-result-object v1

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->STOPPED:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    if-ne v1, v2, :cond_0

    .line 357
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 359
    .restart local v0    # "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->playMusic(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Z)Z

    goto :goto_0
.end method

.method public seekToPlayer(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 413
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 422
    :cond_0
    :goto_0
    return-void

    .line 417
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    goto :goto_0
.end method

.method public setAllMusicListShuffled()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 548
    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "setAllMusicListShuffled"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v4

    .line 550
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Ljava/util/ArrayList;

    move-result-object v1

    .line 559
    .local v1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    if-nez v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 560
    :cond_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v4, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->setMusicPlaylist(Ljava/util/ArrayList;I)V

    .line 561
    const-string/jumbo v4, "setAllMusicListShuffledStartWithMusicItem"

    invoke-direct {p0, v4, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->logMusicList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 562
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 563
    .local v0, "currentMusic":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->startPlayer(Z)V

    .line 564
    if-eqz v0, :cond_1

    .line 565
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    .line 566
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "playing music is not null : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 565
    invoke-static {v2, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDriveLinkMusicPlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v2

    .line 568
    invoke-interface {v2, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;->onMusicPlayResumed(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    .line 576
    :goto_0
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setShuffle(Z)V

    move v2, v3

    .line 582
    .end local v0    # "currentMusic":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    :goto_1
    return v2

    .line 570
    .restart local v0    # "currentMusic":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    :cond_1
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "currentMusic":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 571
    .restart local v0    # "currentMusic":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "playing music is null : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDriveLinkMusicPlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v2

    .line 573
    invoke-interface {v2, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;->onMusicPlayResumed(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    goto :goto_0

    .line 579
    .end local v0    # "currentMusic":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDriveLinkMusicPlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v3

    .line 580
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    const v5, 0x15f90

    .line 579
    invoke-interface {v3, v4, v5, v2, v6}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;->onMusicPlayError(Landroid/media/MediaPlayer;IILjava/lang/Object;)V

    goto :goto_1
.end method

.method public setMusicControllerNativeMusic(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;)V
    .locals 12
    .param p1, "dataType"    # Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 464
    sget-object v6, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v7, "setMusicControllerNativeMusic"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v6

    .line 466
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7, v11}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Ljava/util/ArrayList;

    move-result-object v4

    .line 467
    .local v4, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 468
    .local v5, "setMusicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    if-eqz v4, :cond_1

    .line 469
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_1

    .line 470
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v1, v6, :cond_2

    .line 530
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v6, v5, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->setMusicPlaylist(Ljava/util/ArrayList;I)V

    .line 531
    const-string/jumbo v6, "setMusicControllerNativeMusic"

    invoke-direct {p0, v6, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->logMusicList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 532
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 533
    invoke-virtual {p0, v10}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setShuffle(Z)V

    .line 534
    :cond_0
    sget-object v6, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "setMusicControllerNativeMusic : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 535
    iget-boolean v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mbEjectPlayState:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 534
    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_1

    .line 537
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDriveLinkMusicPlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v6

    .line 538
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 539
    const v8, 0x15f90

    .line 538
    invoke-interface {v6, v7, v8, v9, v11}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;->onMusicPlayError(Landroid/media/MediaPlayer;IILjava/lang/Object;)V

    .line 545
    .end local v1    # "i":I
    :cond_1
    return-void

    .line 471
    .restart local v1    # "i":I
    :cond_2
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 473
    .local v3, "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_4

    .line 470
    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 477
    :cond_4
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v0

    .line 479
    .local v0, "dataPath":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    .line 481
    .local v2, "locale":Ljava/util/Locale;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$framework$manager$musicplayer$MusicPlayerManager$MusicDataEject()[I

    move-result-object v6

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicDataEject;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 503
    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    .line 504
    const-string/jumbo v7, "/private/"

    invoke-virtual {v7, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    .line 503
    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    .line 504
    if-nez v6, :cond_3

    .line 505
    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    .line 506
    const-string/jumbo v7, "/PersonalPage/"

    .line 507
    invoke-virtual {v7, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    .line 505
    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    .line 507
    if-nez v6, :cond_3

    .line 511
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v6

    .line 512
    invoke-virtual {v6, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    .line 514
    const-string/jumbo v7, "extsdcard/"

    .line 515
    invoke-virtual {v7, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    .line 513
    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    .line 515
    if-nez v6, :cond_3

    .line 520
    :cond_5
    sget-object v6, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "addMusic data : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 523
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ne v6, v10, :cond_3

    .line 524
    iget-boolean v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mbEjectPlayState:Z

    invoke-direct {p0, v3, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->playMusic(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Z)Z

    .line 525
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDriveLinkMusicPlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v6

    .line 526
    invoke-interface {v6, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;->onMusicPlayResumed(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    .line 527
    iput-boolean v9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mbEjectPlayState:Z

    goto/16 :goto_1

    .line 484
    :pswitch_0
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v6

    .line 485
    invoke-virtual {v6, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    .line 487
    const-string/jumbo v7, "extsdcard/"

    .line 488
    invoke-virtual {v7, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    .line 486
    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    .line 488
    if-eqz v6, :cond_5

    goto/16 :goto_1

    .line 493
    :pswitch_1
    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    .line 494
    const-string/jumbo v7, "/private/"

    invoke-virtual {v7, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    .line 493
    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    .line 494
    if-nez v6, :cond_3

    .line 495
    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    .line 496
    const-string/jumbo v7, "/PersonalPage/"

    .line 497
    invoke-virtual {v7, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    .line 495
    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    .line 497
    if-eqz v6, :cond_5

    goto/16 :goto_1

    .line 481
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setMusicPlaylist(Ljava/util/ArrayList;I)V
    .locals 4
    .param p2, "indexMusicPlayed"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    const/4 v3, 0x1

    .line 438
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "setMusicPlayList"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 461
    :cond_0
    :goto_0
    return-void

    .line 443
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    .line 444
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setIntialized(Z)V

    .line 448
    :cond_2
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setMusicPlaylist musiclist size : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    const-string/jumbo v0, "setMusicPlaylist"

    invoke-direct {p0, v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->logMusicList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 454
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 455
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setAllMusicListShuffledStartWithMusicItem(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 460
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->setMusicPlaylist(Ljava/util/ArrayList;I)V

    goto :goto_0
.end method

.method public setOnMusicPlayerListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    :goto_0
    return-void

    .line 170
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkMusicPlayerListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;)V

    goto :goto_0
.end method

.method public setRepeatType(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;)V
    .locals 2
    .param p1, "repeatType"    # Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    .prologue
    .line 194
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 208
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->getMusicRepeatType()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 202
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->setMusicRepeatType(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;)V

    .line 204
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->saveMusicPlayerEnvInfo()V

    .line 206
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDriveLinkMusicPlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v0

    .line 207
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMusicPlayerEnvInfo()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;->onMusicPlayerEnvInfoChanged(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;)V

    goto :goto_0
.end method

.method public setShuffle(Z)V
    .locals 2
    .param p1, "bShuffle"    # Z

    .prologue
    .line 223
    .line 224
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "setShuffle("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ") <"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 225
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->isShuffle()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 224
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 226
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v1

    .line 223
    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->logMusic(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    .line 228
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->setShuffle(Z)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->isShuffle()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 239
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->setShuffle(Z)V

    .line 241
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->saveMusicPlayerEnvInfo()V

    goto :goto_0
.end method

.method public setVolume(F)V
    .locals 1
    .param p1, "vol"    # F

    .prologue
    .line 955
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1, p1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 956
    return-void
.end method

.method public startPlayer(Z)V
    .locals 3
    .param p1, "bPlayNow"    # Z

    .prologue
    .line 299
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 312
    :goto_0
    return-void

    .line 303
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 305
    .local v0, "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    if-nez v0, :cond_1

    .line 306
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->moveToFirst()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 309
    :cond_1
    invoke-direct {p0, v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->playMusic(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Z)Z

    .line 311
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "startPlayer("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->logMusic(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    goto :goto_0
.end method

.method public stopPlayer()V
    .locals 3

    .prologue
    .line 364
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "MusicPlayerManagerstopPlayer"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 382
    :goto_0
    return-void

    .line 369
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_1

    .line 370
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V

    .line 371
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->reset()V

    .line 372
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "MusicPlayerManagermMediaPlayer stop"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    :cond_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->STOPPED:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setPlayerStatus(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;)V

    .line 377
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->startPlayer(Z)V

    .line 379
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 380
    .local v0, "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDriveLinkMusicPlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;->onMusicPlayStopped(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    goto :goto_0
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 145
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    :goto_0
    return-void

    .line 148
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setIntialized(Z)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 154
    :cond_1
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 156
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->NONE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerStatus:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    .line 157
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    .line 158
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    .line 160
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mContext:Landroid/content/Context;

    .line 162
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->terminate(Landroid/content/Context;)V

    goto :goto_0
.end method
