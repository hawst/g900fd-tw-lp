.class Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$2;
.super Ljava/lang/Object;
.source "MultiWindowActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    .line 735
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public resetFlowId()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 748
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "resetFlowId:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$8()Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 749
    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->flowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$8()Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 750
    const-string/jumbo v0, "HOME_MODE_MULTIWINDOW"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->resetMainFlow(Ljava/lang/String;)V

    .line 751
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    const-string/jumbo v1, "DM_MAIN"

    # invokes: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->setActivityFlowID(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$9(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;Ljava/lang/String;)V

    .line 752
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$10(Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;)V

    .line 753
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$11(Ljava/lang/String;)V

    .line 755
    :cond_0
    return-void
.end method

.method public setFlowId(Ljava/lang/String;)V
    .locals 3
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 739
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setFlowId:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 740
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 741
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->setVoiceFlowId(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$7(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;Ljava/lang/String;)V

    .line 744
    :cond_0
    return-void
.end method
