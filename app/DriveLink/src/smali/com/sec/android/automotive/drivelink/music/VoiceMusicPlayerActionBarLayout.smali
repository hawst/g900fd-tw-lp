.class public Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;
.super Landroid/widget/RelativeLayout;
.source "VoiceMusicPlayerActionBarLayout.java"

# interfaces
.implements Lcom/nuance/drivelink/DLUiUpdater;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$SineEaseOut;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I = null

.field public static final DELAY_TIME_FOR_AUTO_SHRINK:J = 0x1388L

.field public static final DELAY_TIME_FOR_AUTO_SHRINK_PLAYING:J = 0x3a98L

.field private static final SINE_OUT:Landroid/view/animation/Interpolator;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isRestore:Z

.field private isRunning:Z

.field private mAnimator:Landroid/view/ViewPropertyAnimator;

.field private mBackBtn:Landroid/widget/LinearLayout;

.field private mBackBtnImage:Landroid/widget/ImageView;

.field private mContext:Landroid/content/Context;

.field private mCurrentMicState:Lcom/nuance/sample/MicState;

.field private mDialogLayout:Landroid/widget/RelativeLayout;

.field private mDirection:Z

.field private mErrorCharSequence:Ljava/lang/CharSequence;

.field private mHiText:Landroid/widget/TextView;

.field private mIsErrorState:Z

.field private mIsMicDisplayed:Z

.field private mIsPhraseSotting:Z

.field private mIsStartQEnded:Z

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mListeningText:Landroid/widget/TextView;

.field private mMicBtn:Landroid/widget/ImageView;

.field private mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

.field private mMicLayout:Landroid/widget/RelativeLayout;

.field private mMicProcessBg:Landroid/widget/ImageView;

.field private mMicStartQBg:Landroid/widget/ImageView;

.field private mNoNetworkLayout:Landroid/widget/LinearLayout;

.field private mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

.field private mOnVoiceMusicPlayerActionBarListener:Lcom/sec/android/automotive/drivelink/music/OnVoiceMusicPlayerActionBarListener;

.field private mPhraseSpotterHandler:Landroid/os/Handler;

.field private mProcessText:Landroid/widget/TextView;

.field private mQuoteEnd:Landroid/widget/ImageView;

.field private mQuoteStart:Landroid/widget/ImageView;

.field private mRatio:F

.field private mSayText:Landroid/widget/TextView;

.field private mSearchBtn:Landroid/widget/ImageButton;

.field private mStandByLayout:Landroid/widget/LinearLayout;

.field private mStartQAni:Landroid/view/animation/Animation;

.field private mTTSBar:Landroid/widget/ImageView;

.field private mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

.field private mTTSBarLayout:Landroid/widget/RelativeLayout;

.field private mTTSText:Landroid/widget/TextView;

.field private mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

.field private mTitle:Landroid/widget/TextView;

.field private mVoiceLayout:Landroid/widget/RelativeLayout;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    .line 45
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 44
    sput-object v0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->TAG:Ljava/lang/String;

    .line 437
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$SineEaseOut;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$SineEaseOut;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 56
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsPhraseSotting:Z

    .line 439
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mRatio:F

    .line 441
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->isRestore:Z

    .line 504
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mDirection:Z

    .line 505
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->isRunning:Z

    .line 699
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    .line 95
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mContext:Landroid/content/Context;

    .line 96
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->init()V

    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 100
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsPhraseSotting:Z

    .line 439
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mRatio:F

    .line 441
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->isRestore:Z

    .line 504
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mDirection:Z

    .line 505
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->isRunning:Z

    .line 699
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    .line 101
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mContext:Landroid/content/Context;

    .line 102
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->init()V

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 107
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsPhraseSotting:Z

    .line 439
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mRatio:F

    .line 441
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->isRestore:Z

    .line 504
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mDirection:Z

    .line 505
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->isRunning:Z

    .line 699
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    .line 108
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mContext:Landroid/content/Context;

    .line 109
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->init()V

    .line 110
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsMicDisplayed:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 441
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->isRestore:Z

    return-void
.end method

.method static synthetic access$11()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 437
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;FJLandroid/view/animation/Interpolator;)V
    .locals 0

    .prologue
    .line 443
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->startScaleAnimation(FJLandroid/view/animation/Interpolator;)V

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)Z
    .locals 1

    .prologue
    .line 505
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->isRunning:Z

    return v0
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)Z
    .locals 1

    .prologue
    .line 504
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mDirection:Z

    return v0
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 504
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mDirection:Z

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)V
    .locals 0

    .prologue
    .line 508
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->runTTSBarAnimation()V

    return-void
.end method

.method static synthetic access$17(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 505
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->isRunning:Z

    return-void
.end method

.method static synthetic access$18(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSBar:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)Lcom/sec/android/automotive/drivelink/common/view/ListeningView;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsStartQEnded:Z

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)Z
    .locals 1

    .prologue
    .line 441
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->isRestore:Z

    return v0
.end method

.method private init()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 115
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 116
    .local v0, "inflater":Landroid/view/LayoutInflater;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;->isListening()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsPhraseSotting:Z

    .line 119
    const v1, 0x7f0300da

    .line 118
    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 121
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 122
    const v2, 0x7f09037d

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 121
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    .line 123
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mContext:Landroid/content/Context;

    .line 124
    const v4, 0x7f0a0253

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 125
    const-string/jumbo v3, ". "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 126
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mContext:Landroid/content/Context;

    const v4, 0x7f0a03f4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 123
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 128
    const v2, 0x7f09037e

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 127
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mBackBtnImage:Landroid/widget/ImageView;

    .line 129
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f09037f

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTitle:Landroid/widget/TextView;

    .line 130
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 131
    const v2, 0x7f09036a

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 130
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    .line 132
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 133
    const v2, 0x7f09036b

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 132
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mDialogLayout:Landroid/widget/RelativeLayout;

    .line 134
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 135
    const v2, 0x7f09036c

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 134
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    .line 136
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 137
    const v2, 0x7f09031c

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 136
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    .line 138
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090371

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    .line 139
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 140
    const v2, 0x7f090372

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 139
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    .line 141
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 142
    const v2, 0x7f090379

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 141
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    .line 143
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f09037a

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    .line 144
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f09037b

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    .line 146
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f09009a

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    .line 147
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090376

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    .line 148
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090373

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    .line 149
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 150
    const v2, 0x7f090374

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 149
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    .line 151
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 152
    const v2, 0x7f090375

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 151
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    .line 154
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 155
    const v2, 0x7f090377

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 154
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    .line 156
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090378

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSBar:Landroid/widget/ImageView;

    .line 158
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mContext:Landroid/content/Context;

    .line 159
    const v2, 0x7f04001e

    .line 158
    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    .line 160
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    new-instance v2, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$2;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$2;-><init>(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 178
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/nuance/sample/OnClickMicListenerImpl;

    .line 179
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-direct {v2, v3, v5, v4}, Lcom/nuance/sample/OnClickMicListenerImpl;-><init>(Lcom/nuance/sample/MicStateMaster;Ljava/lang/String;Landroid/view/View;)V

    .line 178
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 180
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->isMicDisplayed()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsMicDisplayed:Z

    .line 182
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f09036d

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mSayText:Landroid/widget/TextView;

    .line 183
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f09036f

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mHiText:Landroid/widget/TextView;

    .line 184
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f09036e

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mQuoteStart:Landroid/widget/ImageView;

    .line 185
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090370

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mQuoteEnd:Landroid/widget/ImageView;

    .line 187
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mHiText:Landroid/widget/TextView;

    .line 188
    const-string/jumbo v2, "/system/fonts/Cooljazz.ttf"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->createTypefaceFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    .line 187
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 189
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mOnVoiceMusicPlayerActionBarListener:Lcom/sec/android/automotive/drivelink/music/OnVoiceMusicPlayerActionBarListener;

    .line 191
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->setHasInternetLayout(Z)V

    .line 192
    return-void
.end method

.method private runTTSBarAnimation()V
    .locals 5

    .prologue
    const-wide/16 v3, 0x2bc

    .line 509
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    if-nez v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSBar:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    .line 511
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$4;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$4;-><init>(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 539
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$5;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$5;-><init>(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)Landroid/view/ViewPropertyAnimator;

    .line 549
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mDirection:Z

    if-eqz v0, :cond_1

    .line 550
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mContext:Landroid/content/Context;

    const/high16 v2, 0x428c0000    # 70.0f

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->dipToPixels(Landroid/content/Context;F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 551
    invoke-virtual {v0, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 555
    :goto_0
    return-void

    .line 553
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method private startScaleAnimation(FJLandroid/view/animation/Interpolator;)V
    .locals 2
    .param p1, "ratio"    # F
    .param p2, "duration"    # J
    .param p4, "interpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 446
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    if-nez v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setScaleX(F)V

    .line 448
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setScaleY(F)V

    .line 450
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    .line 451
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$3;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$3;-><init>(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 479
    :cond_0
    iget v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mRatio:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 487
    :goto_0
    return-void

    .line 482
    :cond_1
    iput p1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mRatio:F

    .line 484
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 485
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 486
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method private startTTSBarAnimation()V
    .locals 1

    .prologue
    .line 558
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->isRunning:Z

    .line 559
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->runTTSBarAnimation()V

    .line 560
    return-void
.end method


# virtual methods
.method public displayError(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 220
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mErrorCharSequence:Ljava/lang/CharSequence;

    .line 225
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsErrorState:Z

    .line 226
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsStartQEnded:Z

    .line 227
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->stopTTSBarAnimation()V

    .line 229
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 230
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 232
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 233
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 234
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 236
    return-void
.end method

.method public displaySystemTurn(Ljava/lang/CharSequence;)V
    .locals 4
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 246
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "displaySystemTurn mTTSBarLayout visible"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 250
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 252
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 258
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02038c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 267
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->startTTSBarAnimation()V

    .line 274
    return-void
.end method

.method public displayUserTurn(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 242
    return-void
.end method

.method public displayWidgetContent(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 280
    return-void
.end method

.method protected getErrorCharSequence()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mErrorCharSequence:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 1

    .prologue
    .line 581
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMicState()Lcom/nuance/sample/MicState;
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    return-object v0
.end method

.method public handleUserCancel()V
    .locals 0

    .prologue
    .line 576
    return-void
.end method

.method public hideVoiceLayout()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 612
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 613
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 614
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 615
    return-void
.end method

.method protected isErrorState()Z
    .locals 1

    .prologue
    .line 396
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsErrorState:Z

    return v0
.end method

.method protected isTTSState()Z
    .locals 1

    .prologue
    .line 404
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->isRunning:Z

    return v0
.end method

.method public onClickable(Z)V
    .locals 1
    .param p1, "isClickable"    # Z

    .prologue
    .line 392
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 393
    return-void
.end method

.method public onDisplayMic(Z)V
    .locals 4
    .param p1, "isMicDisplayed"    # Z

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 716
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsMicDisplayed:Z

    .line 722
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    if-ne v0, v1, :cond_0

    .line 723
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsMicDisplayed:Z

    if-eqz v0, :cond_1

    .line 724
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsErrorState:Z

    .line 725
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsStartQEnded:Z

    .line 726
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 727
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 729
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 730
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 731
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 732
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 733
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 734
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 736
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 737
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 738
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 739
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 740
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 741
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02038c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 757
    :cond_0
    :goto_0
    return-void

    .line 743
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsErrorState:Z

    .line 744
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsStartQEnded:Z

    .line 745
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 746
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 748
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 749
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 750
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 751
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 752
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 753
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 754
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onPhraseSpotterStateChanged(Z)V
    .locals 4
    .param p1, "isSpotting"    # Z

    .prologue
    .line 680
    const-string/jumbo v1, "UiUpdater"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, " - PhraseSpotting : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 681
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 680
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsPhraseSotting:Z

    .line 685
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mOnVoiceMusicPlayerActionBarListener:Lcom/sec/android/automotive/drivelink/music/OnVoiceMusicPlayerActionBarListener;

    if-eqz v1, :cond_1

    .line 686
    const/4 v0, 0x0

    .line 688
    .local v0, "spotterState":I
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsPhraseSotting:Z

    if-eqz v1, :cond_0

    .line 689
    const/4 v0, 0x1

    .line 692
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mOnVoiceMusicPlayerActionBarListener:Lcom/sec/android/automotive/drivelink/music/OnVoiceMusicPlayerActionBarListener;

    .line 694
    const/4 v2, 0x1

    .line 693
    invoke-interface {v1, v2, v0}, Lcom/sec/android/automotive/drivelink/music/OnVoiceMusicPlayerActionBarListener;->onVoiceMusicPlayerActionBarUpdate(II)V

    .line 697
    .end local v0    # "spotterState":I
    :cond_1
    return-void
.end method

.method public setCurrentMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 216
    return-void
.end method

.method protected setDayMode()V
    .locals 4

    .prologue
    const v3, 0x7f080031

    .line 664
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mSayText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 665
    const v2, 0x7f080074

    .line 664
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 666
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mHiText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 667
    const v2, 0x7f080076

    .line 666
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 668
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mQuoteStart:Landroid/widget/ImageView;

    const v1, 0x7f02035b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 669
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mQuoteEnd:Landroid/widget/ImageView;

    const v1, 0x7f020359

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 670
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 671
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 673
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 675
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mBackBtnImage:Landroid/widget/ImageView;

    const v1, 0x7f020044

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 676
    return-void
.end method

.method public setHasInternetLayout(Z)V
    .locals 3
    .param p1, "hasInternet"    # Z

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 196
    if-eqz p1, :cond_0

    .line 198
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 199
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 208
    :goto_0
    return-void

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 205
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected setMicDisplayed(Z)V
    .locals 0
    .param p1, "isMicDisplayed"    # Z

    .prologue
    .line 760
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsMicDisplayed:Z

    .line 761
    return-void
.end method

.method protected setNightMode()V
    .locals 4

    .prologue
    const v3, 0x7f080030

    .line 648
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mSayText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 649
    const v2, 0x7f080075

    .line 648
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 650
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mHiText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 651
    const v2, 0x7f080077

    .line 650
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 652
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mQuoteStart:Landroid/widget/ImageView;

    const v1, 0x7f02035e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 653
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mQuoteEnd:Landroid/widget/ImageView;

    const v1, 0x7f02035d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 654
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 656
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 658
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 660
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mBackBtnImage:Landroid/widget/ImageView;

    const v1, 0x7f020045

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 661
    return-void
.end method

.method public setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 618
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 619
    return-void
.end method

.method public setOnMicStateChangeListener(Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .prologue
    .line 387
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .line 388
    return-void
.end method

.method public setOnSearchBtnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 622
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 623
    return-void
.end method

.method public setOnVoiceMusicPlayerActionBarListener(Lcom/sec/android/automotive/drivelink/music/OnVoiceMusicPlayerActionBarListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/automotive/drivelink/music/OnVoiceMusicPlayerActionBarListener;

    .prologue
    .line 212
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mOnVoiceMusicPlayerActionBarListener:Lcom/sec/android/automotive/drivelink/music/OnVoiceMusicPlayerActionBarListener;

    .line 213
    return-void
.end method

.method protected setTTSState()V
    .locals 1

    .prologue
    .line 408
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->displaySystemTurn(Ljava/lang/CharSequence;)V

    .line 409
    return-void
.end method

.method public showVoiceLayout()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 585
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    const-string/jumbo v2, "audio"

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 586
    .local v0, "audioMgr":Landroid/media/AudioManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 588
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v1

    if-nez v1, :cond_0

    .line 590
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 591
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 604
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 605
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 606
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 607
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 608
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 609
    return-void

    .line 595
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v1

    if-nez v1, :cond_1

    .line 596
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 597
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 599
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 600
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 601
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected stopTTSBarAnimation()V
    .locals 1

    .prologue
    .line 563
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_0

    .line 564
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 566
    :cond_0
    return-void
.end method

.method public updateMicRMSChange(I)V
    .locals 1
    .param p1, "rmsValue"    # I

    .prologue
    .line 416
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsStartQEnded:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    if-eqz v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->update(I)V

    .line 419
    :cond_0
    return-void
.end method

.method public updateMicState(Lcom/nuance/sample/MicState;)V
    .locals 5
    .param p1, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 284
    const-string/jumbo v0, "UiUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    .line 287
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    if-nez v0, :cond_3

    .line 288
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 372
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mOnVoiceMusicPlayerActionBarListener:Lcom/sec/android/automotive/drivelink/music/OnVoiceMusicPlayerActionBarListener;

    if-eqz v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mOnVoiceMusicPlayerActionBarListener:Lcom/sec/android/automotive/drivelink/music/OnVoiceMusicPlayerActionBarListener;

    .line 376
    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    .line 374
    invoke-interface {v0, v4, v1}, Lcom/sec/android/automotive/drivelink/music/OnVoiceMusicPlayerActionBarListener;->onVoiceMusicPlayerActionBarUpdate(II)V

    .line 378
    :cond_0
    return-void

    .line 290
    :pswitch_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 291
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    iput-boolean v4, v0, Lcom/sec/android/automotive/drivelink/music/MusicService;->playByVoice:Z

    .line 293
    :cond_1
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsErrorState:Z

    .line 294
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsStartQEnded:Z

    .line 295
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 296
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->stopTTSBarAnimation()V

    .line 297
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 305
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsMicDisplayed:Z

    if-eqz v0, :cond_2

    .line 306
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 310
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 311
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02038c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 319
    :goto_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 320
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 321
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 322
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 324
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    const v1, 0x7f0a024b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 325
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    goto :goto_0

    .line 315
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x2bc

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 329
    :pswitch_1
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsErrorState:Z

    .line 330
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 331
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->stopTTSBarAnimation()V

    .line 332
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 333
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 334
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 336
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 337
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 338
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 339
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 340
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 341
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 342
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020385

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 345
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020388

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 349
    :pswitch_2
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsErrorState:Z

    .line 350
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mIsStartQEnded:Z

    .line 351
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->stopTTSBarAnimation()V

    .line 352
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 353
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 354
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 355
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 356
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 357
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 358
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 359
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 360
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 361
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 362
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 363
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->play(I)V

    goto/16 :goto_0

    .line 369
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    invoke-interface {v0, p0, p1}, Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;->onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V

    goto/16 :goto_0

    .line 288
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
