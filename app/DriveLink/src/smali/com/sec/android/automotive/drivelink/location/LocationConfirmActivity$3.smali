.class Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$3;
.super Ljava/lang/Object;
.source "LocationConfirmActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 131
    const-string/jumbo v2, "ro.csc.countryiso_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, "country":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 135
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    .line 136
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 134
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v2

    .line 138
    const-string/jumbo v3, "PREF_SETTINGS_MY_NAVIGATION"

    .line 139
    const/4 v4, 0x0

    .line 137
    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v1

    .line 140
    .local v1, "mapID":I
    const-string/jumbo v2, "CN"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 141
    sget v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_GOOGLE_MAPS:I

    if-ne v1, v2, :cond_1

    .line 142
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v3

    .line 143
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v3

    .line 142
    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->hasGoogleAccount(Landroid/content/Context;)Z
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->access$1(Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;Landroid/content/Context;)Z

    move-result v2

    .line 143
    if-nez v2, :cond_1

    .line 144
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;

    const v3, 0x7f0a01fb

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->showToast(I)V

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 146
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->IsOffline()Z
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->access$2(Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 147
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->isProviderEnabled()Z
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->access$3(Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 149
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;

    const/4 v3, 0x0

    .line 150
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mAddress:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->access$4(Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;)Ljava/lang/String;

    move-result-object v4

    .line 148
    invoke-static {v2, v3, v4}, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->startNavigation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;Ljava/lang/String;)V

    goto :goto_0

    .line 152
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->getEnableGPSPDialog()V

    goto :goto_0
.end method
