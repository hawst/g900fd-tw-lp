.class public Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;
.super Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;
.source "MusicRemotePlayerManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;
    }
.end annotation


# static fields
.field public static final EXTRA_ALBUM:Ljava/lang/String; = "album"

.field public static final EXTRA_ARTIST:Ljava/lang/String; = "artist"

.field public static final EXTRA_AUDIO_ID:Ljava/lang/String; = "id"

.field public static final EXTRA_BASE_URI:Ljava/lang/String; = "base_uri"

.field public static final EXTRA_LIST_COUNT:Ljava/lang/String; = "mediaCount"

.field public static final EXTRA_LIST_POSITION:Ljava/lang/String; = "listPosition"

.field public static final EXTRA_MEDIA_DURATION:Ljava/lang/String; = "trackLength"

.field public static final EXTRA_MEDIA_POSITION:Ljava/lang/String; = "position"

.field public static final EXTRA_PLAYING:Ljava/lang/String; = "playing"

.field public static final EXTRA_PREV_POSITION:Ljava/lang/String; = "prev_position"

.field public static final EXTRA_PREV_URI:Ljava/lang/String; = "prev_uri"

.field public static final EXTRA_TRACK:Ljava/lang/String; = "track"

.field public static final META_CHANGED:Ljava/lang/String; = "com.android.music.metachanged"

.field public static final PLAYER_ATTR_SHUFFLE:B = 0x3t

.field public static final PLAYSTATE_CHANGED:Ljava/lang/String; = "com.android.music.playstatechanged"

.field public static final SHUFFLE_NONE:I = 0x0

.field public static final SHUFFLE_NORMAL:I = 0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field final ACTION_AVRCP:Ljava/lang/String;

.field final SHUFFLE:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mIsRegisted:Z

.field mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

.field private final mMusicButtonHandler:Landroid/os/Handler;

.field private mSession:Landroid/media/session/MediaSession;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    .line 26
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 25
    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->TAG:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 67
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mContext:Landroid/content/Context;

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    .line 30
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    .line 31
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mIsRegisted:Z

    .line 64
    const-string/jumbo v0, "shuffle"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->SHUFFLE:Ljava/lang/String;

    .line 65
    const-string/jumbo v0, "com.samsung.android.bt.AVRCP"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->ACTION_AVRCP:Ljava/lang/String;

    .line 582
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMusicButtonHandler:Landroid/os/Handler;

    .line 68
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mIsRegisted:Z

    .line 69
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public forwardPlayer()V
    .locals 5

    .prologue
    .line 472
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    if-nez v0, :cond_0

    .line 482
    :goto_0
    return-void

    .line 475
    :cond_0
    const-string/jumbo v0, "MY_TAG"

    .line 476
    const-string/jumbo v1, "[YANG] setPlaybackState :RemoteControlClient.PLAYSTATE_FAST_FORWARDING "

    .line 475
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const/4 v1, 0x4

    const-wide/16 v2, 0x0

    .line 478
    const/high16 v4, 0x3f800000    # 1.0f

    .line 477
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/media/session/PlaybackState$Builder;->setState(IJF)Landroid/media/session/PlaybackState$Builder;

    .line 479
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v1}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    goto :goto_0
.end method

.method public declared-synchronized initialize(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->initialize(Landroid/content/Context;)Z

    .line 73
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mContext:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public pausePlayer(I)V
    .locals 5
    .param p1, "timeInMs"    # I

    .prologue
    .line 432
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    if-nez v0, :cond_0

    .line 445
    :goto_0
    return-void

    .line 435
    :cond_0
    const-string/jumbo v0, "MY_TAG"

    .line 436
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[YANG] setPlaybackState :RemoteControlClient.PLAYSTATE_PAUSED "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 437
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 436
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 435
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const/4 v1, 0x2

    .line 439
    int-to-long v2, p1

    const/high16 v4, 0x3f800000    # 1.0f

    .line 438
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/media/session/PlaybackState$Builder;->setState(IJF)Landroid/media/session/PlaybackState$Builder;

    .line 441
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v1}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    goto :goto_0
.end method

.method public playNextMusic()V
    .locals 5

    .prologue
    .line 485
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    if-nez v0, :cond_0

    .line 495
    :goto_0
    return-void

    .line 488
    :cond_0
    const-string/jumbo v0, "MY_TAG"

    .line 489
    const-string/jumbo v1, "[YANG] setPlaybackState :RemoteControlClient.PLAYSTATE_SKIPPING_FORWARDS "

    .line 488
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const/16 v1, 0xa

    const-wide/16 v2, 0x0

    .line 491
    const/high16 v4, 0x3f800000    # 1.0f

    .line 490
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/media/session/PlaybackState$Builder;->setState(IJF)Landroid/media/session/PlaybackState$Builder;

    .line 492
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v1}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    goto :goto_0
.end method

.method public playPrevMusic()V
    .locals 5

    .prologue
    .line 498
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    if-nez v0, :cond_0

    .line 508
    :goto_0
    return-void

    .line 501
    :cond_0
    const-string/jumbo v0, "MY_TAG"

    .line 502
    const-string/jumbo v1, "[YANG] setPlaybackState :RemoteControlClient.PLAYSTATE_SKIPPING_BACKWARDS "

    .line 501
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const/16 v1, 0x9

    .line 504
    const-wide/16 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 503
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/media/session/PlaybackState$Builder;->setState(IJF)Landroid/media/session/PlaybackState$Builder;

    .line 505
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v1}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    goto :goto_0
.end method

.method public declared-synchronized registerMusicRemoteControl(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mIsRegisted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 192
    :goto_0
    monitor-exit p0

    return-void

    .line 88
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    if-nez v1, :cond_1

    .line 89
    new-instance v1, Landroid/media/session/MediaSession;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "CarMode"

    invoke-direct {v1, v2, v3}, Landroid/media/session/MediaSession;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    .line 90
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;

    .line 91
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMusicButtonHandler:Landroid/os/Handler;

    .line 90
    invoke-direct {v0, p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;Landroid/os/Handler;)V

    .line 92
    .local v0, "mCallback":Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    invoke-virtual {v1, v0}, Landroid/media/session/MediaSession;->setCallback(Landroid/media/session/MediaSession$Callback;)V

    .line 93
    new-instance v1, Landroid/media/session/PlaybackState$Builder;

    invoke-direct {v1}, Landroid/media/session/PlaybackState$Builder;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    .line 94
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const-wide/16 v2, 0x37f

    invoke-virtual {v1, v2, v3}, Landroid/media/session/PlaybackState$Builder;->setActions(J)Landroid/media/session/PlaybackState$Builder;

    .line 103
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v2}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    .line 104
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/session/MediaSession;->setFlags(I)V

    .line 108
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const/4 v2, 0x6

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/media/session/PlaybackState$Builder;->setState(IJF)Landroid/media/session/PlaybackState$Builder;

    .line 109
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v2}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    .line 111
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/session/MediaSession;->setActive(Z)V

    .line 190
    .end local v0    # "mCallback":Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mIsRegisted:Z

    .line 191
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "[YANG] mSession end register"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 84
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized resetMetaData()V
    .locals 7

    .prologue
    .line 256
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_1

    .line 294
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 259
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v3

    .line 260
    .local v3, "serviceProvider":Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    if-eqz v3, :cond_0

    .line 265
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMusicPlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v2

    .line 266
    .local v2, "playerManager":Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
    if-eqz v2, :cond_0

    .line 270
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMusicPlaylist()Ljava/util/ArrayList;

    move-result-object v1

    .line 271
    .local v1, "musicPlaylist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    if-eqz v1, :cond_0

    .line 275
    new-instance v0, Landroid/media/MediaMetadata$Builder;

    invoke-direct {v0}, Landroid/media/MediaMetadata$Builder;-><init>()V

    .line 276
    .local v0, "ed":Landroid/media/MediaMetadata$Builder;
    const-string/jumbo v4, "android.media.metadata.TITLE"

    const-string/jumbo v5, ""

    invoke-virtual {v0, v4, v5}, Landroid/media/MediaMetadata$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaMetadata$Builder;

    .line 277
    const-string/jumbo v4, "android.media.metadata.ALBUM"

    const-string/jumbo v5, ""

    invoke-virtual {v0, v4, v5}, Landroid/media/MediaMetadata$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaMetadata$Builder;

    .line 278
    const-string/jumbo v4, "android.media.metadata.ARTIST"

    const-string/jumbo v5, ""

    invoke-virtual {v0, v4, v5}, Landroid/media/MediaMetadata$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaMetadata$Builder;

    .line 279
    const-string/jumbo v4, "android.media.metadata.ALBUM_ARTIST"

    const-string/jumbo v5, ""

    invoke-virtual {v0, v4, v5}, Landroid/media/MediaMetadata$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaMetadata$Builder;

    .line 280
    const-string/jumbo v4, "android.media.metadata.GENRE"

    const-string/jumbo v5, ""

    invoke-virtual {v0, v4, v5}, Landroid/media/MediaMetadata$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaMetadata$Builder;

    .line 281
    const-string/jumbo v4, "android.media.metadata.DISC_NUMBER"

    const-wide/16 v5, 0x0

    invoke-virtual {v0, v4, v5, v6}, Landroid/media/MediaMetadata$Builder;->putLong(Ljava/lang/String;J)Landroid/media/MediaMetadata$Builder;

    .line 284
    const-string/jumbo v4, "android.media.metadata.TRACK_NUMBER"

    const-wide/16 v5, 0x0

    invoke-virtual {v0, v4, v5, v6}, Landroid/media/MediaMetadata$Builder;->putLong(Ljava/lang/String;J)Landroid/media/MediaMetadata$Builder;

    .line 289
    const-string/jumbo v4, "android.media.metadata.DURATION"

    const-wide/16 v5, 0x0

    invoke-virtual {v0, v4, v5, v6}, Landroid/media/MediaMetadata$Builder;->putLong(Ljava/lang/String;J)Landroid/media/MediaMetadata$Builder;

    .line 291
    const-string/jumbo v4, "android.media.metadata.ALBUM_ART"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/media/MediaMetadata$Builder;->putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/media/MediaMetadata$Builder;

    .line 292
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    invoke-virtual {v0}, Landroid/media/MediaMetadata$Builder;->build()Landroid/media/MediaMetadata;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/media/session/MediaSession;->setMetadata(Landroid/media/MediaMetadata;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 256
    .end local v0    # "ed":Landroid/media/MediaMetadata$Builder;
    .end local v1    # "musicPlaylist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    .end local v2    # "playerManager":Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
    .end local v3    # "serviceProvider":Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public rewindPlayer()V
    .locals 5

    .prologue
    .line 460
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    if-nez v0, :cond_0

    .line 469
    :goto_0
    return-void

    .line 463
    :cond_0
    const-string/jumbo v0, "MY_TAG"

    .line 464
    const-string/jumbo v1, "[YANG] setPlaybackState :RemoteControlClient.PLAYSTATE_REWINDING "

    .line 463
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const/4 v1, 0x5

    const-wide/16 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/media/session/PlaybackState$Builder;->setState(IJF)Landroid/media/session/PlaybackState$Builder;

    .line 466
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v1}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    goto :goto_0
.end method

.method public declared-synchronized setMetaData(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Landroid/graphics/Bitmap;)V
    .locals 8
    .param p1, "musicInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .param p2, "defaultBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 342
    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v5, :cond_1

    .line 386
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 345
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v4

    .line 346
    .local v4, "serviceProvider":Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    if-eqz v4, :cond_0

    .line 351
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMusicPlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v3

    .line 352
    .local v3, "playerManager":Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
    if-eqz v3, :cond_0

    .line 356
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMusicPlaylist()Ljava/util/ArrayList;

    move-result-object v2

    .line 357
    .local v2, "musicPlaylist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    if-eqz v2, :cond_0

    .line 361
    new-instance v1, Landroid/media/MediaMetadata$Builder;

    invoke-direct {v1}, Landroid/media/MediaMetadata$Builder;-><init>()V

    .line 362
    .local v1, "ed":Landroid/media/MediaMetadata$Builder;
    const-string/jumbo v5, "android.media.metadata.TITLE"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/media/MediaMetadata$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaMetadata$Builder;

    .line 363
    const-string/jumbo v5, "android.media.metadata.ALBUM"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getAlbum()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/media/MediaMetadata$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaMetadata$Builder;

    .line 364
    const-string/jumbo v5, "android.media.metadata.ARTIST"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getArtist()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/media/MediaMetadata$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaMetadata$Builder;

    .line 365
    const-string/jumbo v5, "android.media.metadata.ALBUM_ARTIST"

    .line 366
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getArtist()Ljava/lang/String;

    move-result-object v6

    .line 365
    invoke-virtual {v1, v5, v6}, Landroid/media/MediaMetadata$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaMetadata$Builder;

    .line 367
    const-string/jumbo v5, "android.media.metadata.GENRE"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getGenreName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/media/MediaMetadata$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaMetadata$Builder;

    .line 368
    const-string/jumbo v5, "android.media.metadata.DISC_NUMBER"

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v1, v5, v6, v7}, Landroid/media/MediaMetadata$Builder;->putLong(Ljava/lang/String;J)Landroid/media/MediaMetadata$Builder;

    .line 371
    const-string/jumbo v5, "android.media.metadata.TRACK_NUMBER"

    .line 372
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMusicPlayIndex()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    int-to-long v6, v6

    .line 371
    invoke-virtual {v1, v5, v6, v7}, Landroid/media/MediaMetadata$Builder;->putLong(Ljava/lang/String;J)Landroid/media/MediaMetadata$Builder;

    .line 377
    const-string/jumbo v5, "android.media.metadata.DURATION"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getDuration()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v1, v5, v6, v7}, Landroid/media/MediaMetadata$Builder;->putLong(Ljava/lang/String;J)Landroid/media/MediaMetadata$Builder;

    .line 379
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v5

    .line 380
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mContext:Landroid/content/Context;

    .line 379
    invoke-virtual {v5, v6, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 381
    .local v0, "albumArt":Landroid/graphics/Bitmap;
    if-nez v0, :cond_2

    .line 382
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v6, 0x1

    invoke-virtual {p2, v5, v6}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 384
    :cond_2
    const-string/jumbo v5, "android.media.metadata.ALBUM_ART"

    invoke-virtual {v1, v5, v0}, Landroid/media/MediaMetadata$Builder;->putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/media/MediaMetadata$Builder;

    .line 385
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    invoke-virtual {v1}, Landroid/media/MediaMetadata$Builder;->build()Landroid/media/MediaMetadata;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/media/session/MediaSession;->setMetadata(Landroid/media/MediaMetadata;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 342
    .end local v0    # "albumArt":Landroid/graphics/Bitmap;
    .end local v1    # "ed":Landroid/media/MediaMetadata$Builder;
    .end local v2    # "musicPlaylist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    .end local v3    # "playerManager":Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
    .end local v4    # "serviceProvider":Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method public setPlaybackState(ZJF)V
    .locals 3
    .param p1, "isPlaying"    # Z
    .param p2, "position"    # J
    .param p4, "speed"    # F

    .prologue
    .line 390
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/media/session/PlaybackState$Builder;->setState(IJF)Landroid/media/session/PlaybackState$Builder;

    .line 392
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v1}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    .line 394
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->STOPPED:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 395
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMusicPlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayerStatus()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    move-result-object v1

    .line 394
    if-ne v0, v1, :cond_1

    .line 396
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->TAG:Ljava/lang/String;

    .line 397
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[YANG] setPlaybackState :RemoteControlClient.PLAYSTATE "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 398
    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 397
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 396
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/media/session/PlaybackState$Builder;->setState(IJF)Landroid/media/session/PlaybackState$Builder;

    .line 402
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v1}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    .line 412
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/media/session/PlaybackState$Builder;->setState(IJF)Landroid/media/session/PlaybackState$Builder;

    .line 407
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v1}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    goto :goto_0
.end method

.method public setShuffle(I)V
    .locals 4
    .param p1, "shuffleVal"    # I

    .prologue
    .line 511
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    if-nez v1, :cond_0

    .line 525
    :goto_0
    return-void

    .line 515
    :cond_0
    const-string/jumbo v1, "MY_TAG"

    .line 516
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[YANG] setSuffle :RemoteControlClientCompat.PLAYER_ATTR_SHUFFLE  shuffleVal is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 517
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 516
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 515
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 520
    .local v0, "data":Landroid/os/Bundle;
    const-string/jumbo v1, "shuffle"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 521
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    const-string/jumbo v2, "com.samsung.android.bt.AVRCP"

    invoke-virtual {v1, v2, v0}, Landroid/media/session/MediaSession;->sendSessionEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public startPlayer(I)V
    .locals 5
    .param p1, "timeInMs"    # I

    .prologue
    .line 416
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    if-nez v0, :cond_0

    .line 429
    :goto_0
    return-void

    .line 419
    :cond_0
    const-string/jumbo v0, "MY_TAG"

    .line 420
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[YANG] setPlaybackState :RemoteControlClient.PLAYSTATE_STARTED "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 421
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 420
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 419
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const/4 v1, 0x3

    .line 423
    int-to-long v2, p1

    const/high16 v4, 0x3f800000    # 1.0f

    .line 422
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/media/session/PlaybackState$Builder;->setState(IJF)Landroid/media/session/PlaybackState$Builder;

    .line 425
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v1}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    goto :goto_0
.end method

.method public stopPlayer()V
    .locals 5

    .prologue
    .line 448
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    if-nez v0, :cond_0

    .line 457
    :goto_0
    return-void

    .line 451
    :cond_0
    const-string/jumbo v0, "MY_TAG"

    .line 452
    const-string/jumbo v1, "[YANG] setPlaybackState :RemoteControlClient.PLAYSTATE_STOPPED "

    .line 451
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/media/session/PlaybackState$Builder;->setState(IJF)Landroid/media/session/PlaybackState$Builder;

    .line 454
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v1}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    goto :goto_0
.end method

.method public declared-synchronized terminate(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->unregisterMusicRemotecontrol(Landroid/content/Context;)V

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mContext:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    monitor-exit p0

    return-void

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized unregisterMusicRemotecontrol(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 195
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mIsRegisted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 215
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 199
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mIsRegisted:Z

    .line 211
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;

    invoke-virtual {v0}, Landroid/media/session/MediaSession;->release()V

    .line 213
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->mSession:Landroid/media/session/MediaSession;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
