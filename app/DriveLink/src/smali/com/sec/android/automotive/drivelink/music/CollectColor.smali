.class public Lcom/sec/android/automotive/drivelink/music/CollectColor;
.super Ljava/lang/Object;
.source "CollectColor.java"


# static fields
.field private static final ColorMask:I = -0x1f1f20

.field private static final REPRESENTATION_IMAGE_COUNT:I = 0x1

.field private static final TARGET_SIZE:I = 0x3c


# instance fields
.field private mBitmapSmall:Landroid/graphics/Bitmap;

.field private mFindResultIndex:I

.field private mHash:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mHeight:I

.field private mRand:Ljava/util/Random;

.field private mResultCount:I

.field private mSort:Lcom/sec/android/automotive/drivelink/music/CollectColorSort;

.field private mWidth:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mHash:Ljava/util/Hashtable;

    .line 34
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/CollectColorSort;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/music/CollectColorSort;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mSort:Lcom/sec/android/automotive/drivelink/music/CollectColorSort;

    .line 36
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mRand:Ljava/util/Random;

    .line 37
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mRand:Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Random;->setSeed(J)V

    .line 38
    return-void
.end method

.method private addHash(I)V
    .locals 4
    .param p1, "key"    # I

    .prologue
    .line 85
    const v0, -0x1f1f20

    and-int/2addr p1, v0

    .line 86
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mHash:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mHash:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mHash:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mHash:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private copyToArrayForThread(Ljava/util/Hashtable;)[Lcom/sec/android/automotive/drivelink/music/CollectColorNode;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)[",
            "Lcom/sec/android/automotive/drivelink/music/CollectColorNode;"
        }
    .end annotation

    .prologue
    .line 95
    .local p1, "hash":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget v6, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mResultCount:I

    new-array v2, v6, [Lcom/sec/android/automotive/drivelink/music/CollectColorNode;

    .line 96
    .local v2, "nodes":[Lcom/sec/android/automotive/drivelink/music/CollectColorNode;
    const/4 v4, 0x0

    .line 98
    .local v4, "nodes_index":I
    const/4 v3, 0x0

    .line 99
    .local v3, "nodesIsFull":Z
    invoke-virtual {p1}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    .line 101
    .local v0, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v6, v2

    if-lt v1, v6, :cond_1

    .line 104
    :cond_0
    :goto_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-nez v6, :cond_2

    .line 119
    iput v4, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mFindResultIndex:I

    .line 121
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mSort:Lcom/sec/android/automotive/drivelink/music/CollectColorSort;

    invoke-virtual {v6, v2}, Lcom/sec/android/automotive/drivelink/music/CollectColorSort;->makeTree([Lcom/sec/android/automotive/drivelink/music/CollectColorNode;)V

    .line 122
    return-object v2

    .line 102
    :cond_1
    new-instance v6, Lcom/sec/android/automotive/drivelink/music/CollectColorNode;

    invoke-direct {v6}, Lcom/sec/android/automotive/drivelink/music/CollectColorNode;-><init>()V

    aput-object v6, v2, v1

    .line 101
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 105
    :cond_2
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 107
    .local v5, "temp_key":I
    if-eqz v3, :cond_3

    .line 108
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct {p0, v5, v6, v2}, Lcom/sec/android/automotive/drivelink/music/CollectColor;->insert(II[Lcom/sec/android/automotive/drivelink/music/CollectColorNode;)Z

    goto :goto_1

    .line 110
    :cond_3
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct {p0, v5, v6, v2, v4}, Lcom/sec/android/automotive/drivelink/music/CollectColor;->insert(II[Lcom/sec/android/automotive/drivelink/music/CollectColorNode;I)Z

    .line 111
    add-int/lit8 v4, v4, 0x1

    .line 112
    array-length v6, v2

    if-ne v4, v6, :cond_0

    .line 113
    const/4 v3, 0x1

    .line 114
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mSort:Lcom/sec/android/automotive/drivelink/music/CollectColorSort;

    invoke-virtual {v6, v2}, Lcom/sec/android/automotive/drivelink/music/CollectColorSort;->makeTree([Lcom/sec/android/automotive/drivelink/music/CollectColorNode;)V

    goto :goto_1
.end method

.method private decodeImage(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    const/16 v1, 0x3c

    .line 67
    .line 68
    const/4 v0, 0x1

    .line 67
    invoke-static {p1, v1, v1, v0}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mBitmapSmall:Landroid/graphics/Bitmap;

    .line 70
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mBitmapSmall:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mWidth:I

    .line 71
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mBitmapSmall:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mHeight:I

    .line 72
    return-void
.end method

.method private getImage(Landroid/graphics/Bitmap;I)I
    .locals 8
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "resultCount"    # I

    .prologue
    .line 45
    const/4 v6, 0x0

    iput v6, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mFindResultIndex:I

    .line 46
    iput p2, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mResultCount:I

    .line 47
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/music/CollectColor;->decodeImage(Landroid/graphics/Bitmap;)V

    .line 48
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mHash:Ljava/util/Hashtable;

    invoke-virtual {v6}, Ljava/util/Hashtable;->clear()V

    .line 49
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/CollectColor;->runDetail()V

    .line 51
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mHash:Ljava/util/Hashtable;

    invoke-direct {p0, v6}, Lcom/sec/android/automotive/drivelink/music/CollectColor;->copyToArrayForThread(Ljava/util/Hashtable;)[Lcom/sec/android/automotive/drivelink/music/CollectColorNode;

    move-result-object v4

    .line 52
    .local v4, "nodes":[Lcom/sec/android/automotive/drivelink/music/CollectColorNode;
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mSort:Lcom/sec/android/automotive/drivelink/music/CollectColorSort;

    invoke-virtual {v6, v4}, Lcom/sec/android/automotive/drivelink/music/CollectColorSort;->sort([Lcom/sec/android/automotive/drivelink/music/CollectColorNode;)V

    .line 53
    invoke-direct {p0, v4}, Lcom/sec/android/automotive/drivelink/music/CollectColor;->getResult([Lcom/sec/android/automotive/drivelink/music/CollectColorNode;)[I

    move-result-object v1

    .line 54
    .local v1, "colors":[I
    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/music/CollectColor;->getRGB([I)[I

    move-result-object v5

    .line 56
    .local v5, "result":[I
    const/4 v0, 0x0

    .line 57
    .local v0, "color":I
    const/4 v3, 0x0

    .line 58
    .local v3, "index":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v6, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mResultCount:I

    if-lt v2, v6, :cond_0

    .line 63
    return v0

    .line 59
    :cond_0
    const/high16 v6, -0x1000000

    aget v7, v5, v3

    shl-int/lit8 v7, v7, 0x10

    or-int/2addr v6, v7

    .line 60
    add-int/lit8 v7, v3, 0x1

    aget v7, v5, v7

    shl-int/lit8 v7, v7, 0x8

    .line 59
    or-int/2addr v6, v7

    .line 60
    add-int/lit8 v7, v3, 0x2

    aget v7, v5, v7

    .line 59
    or-int v0, v6, v7

    .line 61
    add-int/lit8 v3, v3, 0x3

    .line 58
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private getInt(III)I
    .locals 3
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I

    .prologue
    .line 193
    const/high16 v1, -0x1000000

    shl-int/lit8 v2, p1, 0x10

    or-int/2addr v1, v2

    shl-int/lit8 v2, p2, 0x8

    or-int/2addr v1, v2

    or-int v0, v1, p3

    .line 194
    .local v0, "result":I
    return v0
.end method

.method private getRGB([I)[I
    .locals 6
    .param p1, "color"    # [I

    .prologue
    .line 198
    array-length v4, p1

    mul-int/lit8 v4, v4, 0x3

    new-array v3, v4, [I

    .line 199
    .local v3, "result":[I
    const/4 v1, 0x0

    .line 200
    .local v1, "index":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, p1

    if-lt v0, v4, :cond_0

    .line 205
    return-object v3

    .line 201
    :cond_0
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .local v2, "index":I
    aget v4, p1, v0

    const/high16 v5, 0xff0000

    and-int/2addr v4, v5

    shr-int/lit8 v4, v4, 0x10

    aput v4, v3, v1

    .line 202
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    aget v4, p1, v0

    const v5, 0xff00

    and-int/2addr v4, v5

    shr-int/lit8 v4, v4, 0x8

    aput v4, v3, v2

    .line 203
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    aget v4, p1, v0

    and-int/lit16 v4, v4, 0xff

    aput v4, v3, v1

    .line 200
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    .end local v2    # "index":I
    .restart local v1    # "index":I
    goto :goto_0
.end method

.method private getResult([Lcom/sec/android/automotive/drivelink/music/CollectColorNode;)[I
    .locals 3
    .param p1, "nodes"    # [Lcom/sec/android/automotive/drivelink/music/CollectColorNode;

    .prologue
    .line 142
    iget v2, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mResultCount:I

    new-array v1, v2, [I

    .line 143
    .local v1, "result":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_0

    .line 151
    return-object v1

    .line 144
    :cond_0
    iget v2, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mFindResultIndex:I

    if-gt v2, v0, :cond_1

    .line 145
    const/4 v2, 0x0

    aget v2, v1, v2

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/music/CollectColor;->getSimAverage(I)I

    move-result v2

    aput v2, v1, v0

    .line 143
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 149
    :cond_1
    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/CollectColorNode;->getKey()I

    move-result v2

    aput v2, v1, v0

    goto :goto_1
.end method

.method private getSimAverage(I)I
    .locals 8
    .param p1, "color"    # I

    .prologue
    const/16 v7, 0xff

    .line 155
    const/16 v3, 0x8

    .line 156
    .local v3, "multi":I
    const/high16 v6, 0xff0000

    and-int/2addr v6, p1

    shr-int/lit8 v4, v6, 0x10

    .line 157
    .local v4, "r":I
    const v6, 0xff00

    and-int/2addr v6, p1

    shr-int/lit8 v2, v6, 0x8

    .line 158
    .local v2, "g":I
    and-int/lit16 v1, p1, 0xff

    .line 159
    .local v1, "b":I
    add-int v6, v4, v2

    add-int/2addr v6, v1

    div-int/lit8 v6, v6, 0x3

    div-int/lit8 v6, v6, 0x2

    div-int v0, v6, v3

    .line 161
    .local v0, "average":I
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextBoolean()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 162
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v6, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    mul-int/2addr v6, v3

    add-int/2addr v4, v6

    .line 165
    :goto_0
    if-le v4, v7, :cond_4

    .line 166
    const/16 v4, 0xff

    .line 170
    :cond_0
    :goto_1
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextBoolean()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 171
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v6, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    mul-int/2addr v6, v3

    add-int/2addr v2, v6

    .line 174
    :goto_2
    if-le v2, v7, :cond_6

    .line 175
    const/16 v2, 0xff

    .line 179
    :cond_1
    :goto_3
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextBoolean()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 180
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v6, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    mul-int/2addr v6, v3

    add-int/2addr v1, v6

    .line 183
    :goto_4
    if-le v1, v7, :cond_8

    .line 184
    const/16 v1, 0xff

    .line 188
    :cond_2
    :goto_5
    invoke-direct {p0, v4, v2, v1}, Lcom/sec/android/automotive/drivelink/music/CollectColor;->getInt(III)I

    move-result v5

    .line 189
    .local v5, "result":I
    return v5

    .line 164
    .end local v5    # "result":I
    :cond_3
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v6, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    mul-int/2addr v6, v3

    sub-int/2addr v4, v6

    goto :goto_0

    .line 167
    :cond_4
    if-gez v4, :cond_0

    .line 168
    const/4 v4, 0x0

    goto :goto_1

    .line 173
    :cond_5
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v6, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    mul-int/2addr v6, v3

    sub-int/2addr v2, v6

    goto :goto_2

    .line 176
    :cond_6
    if-gez v2, :cond_1

    .line 177
    const/4 v2, 0x0

    goto :goto_3

    .line 182
    :cond_7
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v6, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    mul-int/2addr v6, v3

    sub-int/2addr v1, v6

    goto :goto_4

    .line 185
    :cond_8
    if-gez v1, :cond_2

    .line 186
    const/4 v1, 0x0

    goto :goto_5
.end method

.method private insert(II[Lcom/sec/android/automotive/drivelink/music/CollectColorNode;)Z
    .locals 2
    .param p1, "key"    # I
    .param p2, "value"    # I
    .param p3, "nodes"    # [Lcom/sec/android/automotive/drivelink/music/CollectColorNode;

    .prologue
    const/4 v0, 0x0

    .line 126
    aget-object v1, p3, v0

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/CollectColorNode;->getValue()I

    move-result v1

    if-ge v1, p2, :cond_0

    .line 127
    aget-object v0, p3, v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/music/CollectColorNode;->setNode(II)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mSort:Lcom/sec/android/automotive/drivelink/music/CollectColorSort;

    invoke-virtual {v0, p3}, Lcom/sec/android/automotive/drivelink/music/CollectColorSort;->checkTree([Lcom/sec/android/automotive/drivelink/music/CollectColorNode;)V

    .line 129
    const/4 v0, 0x1

    .line 131
    :cond_0
    return v0
.end method

.method private insert(II[Lcom/sec/android/automotive/drivelink/music/CollectColorNode;I)Z
    .locals 1
    .param p1, "key"    # I
    .param p2, "value"    # I
    .param p3, "nodes"    # [Lcom/sec/android/automotive/drivelink/music/CollectColorNode;
    .param p4, "nodes_count"    # I

    .prologue
    .line 136
    aget-object v0, p3, p4

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/music/CollectColorNode;->setKey(I)V

    .line 137
    aget-object v0, p3, p4

    invoke-virtual {v0, p2}, Lcom/sec/android/automotive/drivelink/music/CollectColorNode;->setValue(I)V

    .line 138
    const/4 v0, 0x1

    return v0
.end method

.method private runDetail()V
    .locals 4

    .prologue
    .line 76
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mHeight:I

    if-lt v0, v3, :cond_0

    .line 82
    return-void

    .line 77
    :cond_0
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v3, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mWidth:I

    if-lt v1, v3, :cond_1

    .line 76
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    :cond_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/CollectColor;->mBitmapSmall:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v1, v0}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v2

    .line 79
    .local v2, "key":I
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/music/CollectColor;->addHash(I)V

    .line 77
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public getImage(Landroid/graphics/Bitmap;)I
    .locals 1
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 41
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/music/CollectColor;->getImage(Landroid/graphics/Bitmap;I)I

    move-result v0

    return v0
.end method
