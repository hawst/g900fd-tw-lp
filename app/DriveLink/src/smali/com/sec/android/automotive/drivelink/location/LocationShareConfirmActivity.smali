.class public Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;
.source "LocationShareConfirmActivity.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I = null

.field public static final PARAM_PHONE:Ljava/lang/String; = "phone"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private btnCancel:Landroid/widget/Button;

.field private btnShare:Landroid/widget/Button;

.field private cancelTTS:Z

.field private mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

.field private mBuddyPhoneNumber:Ljava/lang/String;

.field mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field final mHanlder:Landroid/os/Handler;

.field private final mNotifiyNewMicState:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;

.field private passByListening:I

.field private passToThinking:Z

.field private user:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;-><init>()V

    .line 41
    const-string/jumbo v0, "LocationShareConfirmActivity"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->TAG:Ljava/lang/String;

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mBuddyPhoneNumber:Ljava/lang/String;

    .line 49
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->cancelTTS:Z

    .line 50
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->passToThinking:Z

    .line 51
    iput v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->passByListening:I

    .line 53
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mHanlder:Landroid/os/Handler;

    .line 309
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mNotifiyNewMicState:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;

    .line 31
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;Lcom/nuance/sample/MicState;)V
    .locals 0

    .prologue
    .line 279
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->handleMicStateChanged(Lcom/nuance/sample/MicState;)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;Z)V
    .locals 0

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->cancelTTS:Z

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;)Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->cancelTTS:Z

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    return-object v0
.end method

.method private gotoHome()V
    .locals 3

    .prologue
    .line 167
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 168
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "fromShare"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 169
    const-string/jumbo v1, "dlcontact"

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->user:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 170
    const-string/jumbo v1, "phoneNumber"

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mBuddyPhoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 171
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 172
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->startActivity(Landroid/content/Intent;)V

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->finish()V

    .line 174
    return-void
.end method

.method private handleMicStateChanged(Lcom/nuance/sample/MicState;)V
    .locals 7
    .param p1, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 280
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 307
    :goto_0
    return-void

    .line 282
    :pswitch_0
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->passByListening:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->passByListening:I

    .line 283
    new-array v0, v6, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->btnShare:Landroid/widget/Button;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->btnCancel:Landroid/widget/Button;

    aput-object v1, v0, v4

    invoke-virtual {p0, v5, v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->setButtonQuoted(Z[Landroid/widget/Button;)V

    .line 284
    new-array v0, v6, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->btnShare:Landroid/widget/Button;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->btnCancel:Landroid/widget/Button;

    aput-object v1, v0, v4

    invoke-virtual {p0, v4, v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->setButtonQuoted(Z[Landroid/widget/Button;)V

    goto :goto_0

    .line 287
    :pswitch_1
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->passToThinking:Z

    goto :goto_0

    .line 290
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->passToThinking:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->passByListening:I

    if-gt v0, v4, :cond_0

    .line 291
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mHanlder:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;)V

    .line 302
    const-wide/16 v2, 0x1388

    .line 291
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 304
    :cond_0
    new-array v0, v6, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->btnShare:Landroid/widget/Button;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->btnCancel:Landroid/widget/Button;

    aput-object v1, v0, v4

    invoke-virtual {p0, v5, v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->setButtonQuoted(Z[Landroid/widget/Button;)V

    goto :goto_0

    .line 280
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private sendLocationSharedMessage()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 204
    const-string/jumbo v1, "DM_LOCATION_SHARE_CONFIRM"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v0

    .line 205
    .local v0, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v0, :cond_1

    .line 206
    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->extras:Landroid/os/Bundle;

    .line 207
    const-string/jumbo v2, "CONTINUE_REQUEST_SHARE_LOCATION"

    .line 206
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 207
    if-eqz v1, :cond_1

    .line 208
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->getInstance()Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    move-result-object v1

    .line 209
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUrl:Ljava/lang/String;

    .line 208
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->acceptShareMyLocation(Landroid/content/Context;Ljava/lang/String;)V

    .line 210
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->gotoHome()V

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    const-string/jumbo v1, "VAC_CLIENT_DM"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 214
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->isFlowManagerIntent(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 216
    const-string/jumbo v1, "DM_LOCATION_SHARE_CONFIRM"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v0

    .line 218
    if-eqz v0, :cond_0

    .line 227
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mBuddyPhoneNumber:Ljava/lang/String;

    .line 229
    const-string/jumbo v1, "VAC_CLIENT_DM"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 230
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->isFlowManagerIntent(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 232
    const-string/jumbo v1, "DM_LOCATION_SHARE_CONFIRM"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v0

    .line 233
    if-eqz v0, :cond_3

    .line 234
    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/ContactData;

    iget-object v1, v1, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mBuddyPhoneNumber:Ljava/lang/String;

    .line 239
    :cond_3
    :goto_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->getInstance()Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    .line 240
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mBuddyPhoneNumber:Ljava/lang/String;

    .line 239
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->shareMyLocation(Landroid/content/Context;Ljava/lang/String;)V

    .line 241
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->gotoHome()V

    goto :goto_0

    .line 222
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "phone"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 223
    const-string/jumbo v1, "LocationShareConfirmActivity"

    const-string/jumbo v2, "Friend\'s phone number not found."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 236
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "phone"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mBuddyPhoneNumber:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public finish()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 324
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->cancelTTS:Z

    .line 326
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->cancelTTS:Z

    .line 327
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mHanlder:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 331
    :goto_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->finish()V

    .line 332
    return-void

    .line 328
    :catch_0
    move-exception v0

    .line 329
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "LocationShareConfirmActivity"

    const-string/jumbo v2, "finish"

    invoke-static {v1, v2, v0}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected getLayoutId()I
    .locals 1

    .prologue
    .line 159
    const v0, 0x7f030018

    return v0
.end method

.method protected getLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    .locals 1

    .prologue
    .line 265
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    const v12, 0x7f0a084d

    const v11, 0x7f0a05ed

    const v8, 0x7f0a0249

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 57
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v6

    .line 58
    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 60
    iput-boolean v10, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->cancelTTS:Z

    .line 61
    const v6, 0x7f0900aa

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 63
    const v6, 0x7f090098

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->btnCancel:Landroid/widget/Button;

    .line 64
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->btnCancel:Landroid/widget/Button;

    new-instance v7, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity$2;

    invoke-direct {v7, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;)V

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v6, v8}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setSayText(I)V

    .line 74
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v6, v8}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setSayListeningText(I)V

    .line 75
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    new-instance v7, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity$3;

    invoke-direct {v7, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;)V

    invoke-virtual {v6, v7}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    const v6, 0x7f0900a9

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->btnShare:Landroid/widget/Button;

    .line 85
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->btnShare:Landroid/widget/Button;

    const v7, 0x7f0a084c

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setText(I)V

    .line 86
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->btnShare:Landroid/widget/Button;

    new-instance v7, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity$4;

    invoke-direct {v7, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;)V

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    :try_start_0
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v6

    sget-object v7, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v6, v7}, Lcom/nuance/drivelink/DLAppUiUpdater;->updateMicState(Lcom/nuance/sample/MicState;)V

    .line 96
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v6, v7}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :cond_0
    :goto_0
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mNotifiyNewMicState:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;

    invoke-virtual {v6, v7}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setNotifiyNewMicState(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;)V

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->isFlowManagerIntent(Landroid/content/Intent;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    .line 112
    const-string/jumbo v7, "EXTRA_FLOW_ID"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 114
    .local v2, "flowID":Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v3

    .line 116
    .local v3, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v3, :cond_1

    .line 117
    iget-object v6, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-static {v6}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getDlContact(Lcom/vlingo/core/internal/contacts/ContactMatch;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->user:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 125
    .end local v2    # "flowID":Ljava/lang/String;
    .end local v3    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_1
    :goto_1
    const-string/jumbo v5, ""

    .line 126
    .local v5, "ttsString":Ljava/lang/String;
    const-string/jumbo v0, ""

    .line 127
    .local v0, "displayName":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->user:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->user:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 128
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->user:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 131
    :cond_2
    const-string/jumbo v6, "DM_LOCATION_SHARE_CONFIRM"

    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v3

    .line 132
    .restart local v3    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iget-object v6, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->extras:Landroid/os/Bundle;

    .line 133
    const-string/jumbo v7, "CONTINUE_REQUEST_SHARE_LOCATION"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 134
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 135
    iget-object v8, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v8, v8, Lcom/vlingo/core/internal/contacts/ContactMatch;->firstName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 134
    invoke-virtual {v6, v7}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setTitleBar(Ljava/lang/String;)V

    .line 138
    iget-object v5, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 149
    :goto_2
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_5

    .line 150
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v6, v5}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setVoiceLayoutOnClickListener(Ljava/lang/String;)V

    .line 155
    :goto_3
    return-void

    .line 97
    .end local v0    # "displayName":Ljava/lang/String;
    .end local v3    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v5    # "ttsString":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 98
    .local v1, "e":Ljava/lang/Exception;
    const v6, 0x7f0a0296

    invoke-static {p0, v6, v9}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;II)Landroid/widget/Toast;

    move-result-object v4

    .line 101
    .local v4, "toast":Landroid/widget/Toast;
    if-eqz v4, :cond_0

    .line 102
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 121
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v4    # "toast":Landroid/widget/Toast;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    .line 122
    const-string/jumbo v7, "dlcontact"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 121
    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->user:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    goto :goto_1

    .line 140
    .restart local v0    # "displayName":Ljava/lang/String;
    .restart local v3    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .restart local v5    # "ttsString":Ljava/lang/String;
    :cond_4
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 141
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 140
    invoke-virtual {v6, v7}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setTitleBar(Ljava/lang/String;)V

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 146
    new-array v7, v9, [Ljava/lang/Object;

    aput-object v0, v7, v10

    .line 145
    invoke-virtual {v6, v11, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    .line 152
    :cond_5
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 153
    new-array v8, v9, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->getName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    .line 152
    invoke-virtual {v7, v11, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setVoiceLayoutOnClickListener(Ljava/lang/String;)V

    goto :goto_3
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 191
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onDestroy()V

    .line 199
    :goto_0
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1, v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 200
    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mFlowID:Ljava/lang/String;

    .line 195
    .local v0, "bkp":Ljava/lang/String;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mFlowID:Ljava/lang/String;

    .line 196
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onDestroy()V

    .line 197
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mFlowID:Ljava/lang/String;

    goto :goto_0
.end method

.method public onFlowCommandCancel(Ljava/lang/String;)V
    .locals 2
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 255
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v0

    .line 256
    .local v0, "params":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v0, :cond_0

    .line 258
    const v1, 0x7f0a05eb

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 257
    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 260
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->finish()V

    .line 261
    return-void
.end method

.method public onFlowCommandSend(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 179
    return-void
.end method

.method public onFlowCommandYes(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->sendShare()V

    .line 320
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 246
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 248
    const-string/jumbo v0, "dlcontact"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->user:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 250
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 275
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onResume()V

    .line 276
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->setCustomZoom()V

    .line 277
    return-void
.end method

.method protected sendShare()V
    .locals 0

    .prologue
    .line 163
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->sendLocationSharedMessage()V

    .line 164
    return-void
.end method
