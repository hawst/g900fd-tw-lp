.class Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$1;
.super Landroid/content/BroadcastReceiver;
.source "HomeOverlayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$1;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    .line 66
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 70
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "mLcdOffReceiver onReceive."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$1;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$1;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->pauseNotiFlow()V

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasNotification()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->serviceState:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$1()Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->Paused:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    if-ne v0, v1, :cond_0

    .line 77
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getTopNotificationItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->pauseNotiFlow()V

    goto :goto_0
.end method
