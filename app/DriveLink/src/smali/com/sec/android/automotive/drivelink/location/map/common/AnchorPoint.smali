.class public Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;
.super Ljava/lang/Object;
.source "AnchorPoint.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0xbc3120fd40740e9L


# instance fields
.field private x:F

.field private y:F


# direct methods
.method public constructor <init>(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;->x:F

    .line 20
    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;->y:F

    .line 23
    iput p1, p0, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;->x:F

    .line 24
    iput p2, p0, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;->y:F

    .line 25
    return-void
.end method


# virtual methods
.method public getX()F
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;->x:F

    return v0
.end method

.method public getY()F
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;->y:F

    return v0
.end method
