.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
.source "DLInboxImpl.java"


# instance fields
.field private mDLContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

.field private mDLMessagList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;",
            ">;"
        }
    .end annotation
.end field

.field private mLastReceivedDate:J

.field private mMsgThreadID:I

.field private mPhoneNumber:Ljava/lang/String;

.field private mUnreadCount:I


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;IIJLjava/lang/String;)V
    .locals 2
    .param p1, "DLContact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .param p2, "unreadCount"    # I
    .param p3, "msgThreadID"    # I
    .param p4, "lastReceivedDate"    # J
    .param p6, "phoneNumber"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;-><init>()V

    .line 10
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mDLContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 11
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mMsgThreadID:I

    .line 12
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mUnreadCount:I

    .line 13
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mPhoneNumber:Ljava/lang/String;

    .line 14
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mLastReceivedDate:J

    .line 20
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mDLContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 21
    iput p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mMsgThreadID:I

    .line 22
    iput p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mUnreadCount:I

    .line 23
    iput-wide p4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mLastReceivedDate:J

    .line 24
    iput-object p6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mPhoneNumber:Ljava/lang/String;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mDLMessagList:Ljava/util/ArrayList;

    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;IIJLjava/lang/String;Ljava/util/ArrayList;)V
    .locals 2
    .param p1, "dLContact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .param p2, "unreadCount"    # I
    .param p3, "msgThreadID"    # I
    .param p4, "lastReceivedDate"    # J
    .param p6, "phoneNumber"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            "IIJ",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p7, "dLMessagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;>;"
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;-><init>()V

    .line 10
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mDLContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 11
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mMsgThreadID:I

    .line 12
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mUnreadCount:I

    .line 13
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mPhoneNumber:Ljava/lang/String;

    .line 14
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mLastReceivedDate:J

    .line 32
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mDLContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 33
    iput p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mMsgThreadID:I

    .line 34
    iput p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mUnreadCount:I

    .line 35
    iput-object p6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mPhoneNumber:Ljava/lang/String;

    .line 36
    iput-wide p4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mLastReceivedDate:J

    .line 37
    iput-object p7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mDLMessagList:Ljava/util/ArrayList;

    .line 38
    return-void
.end method


# virtual methods
.method public getDLContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mDLContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    return-object v0
.end method

.method public getDLMessagList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mDLMessagList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLastReceivedDate()J
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mLastReceivedDate:J

    return-wide v0
.end method

.method public getMsgThreadID()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mMsgThreadID:I

    return v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getUnreadMsgCount()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mUnreadCount:I

    return v0
.end method

.method public setDLContact(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
    .locals 0
    .param p1, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mDLContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 61
    return-void
.end method

.method public setDLMessagList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p1, "messagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->mDLMessagList:Ljava/util/ArrayList;

    .line 75
    return-void
.end method
