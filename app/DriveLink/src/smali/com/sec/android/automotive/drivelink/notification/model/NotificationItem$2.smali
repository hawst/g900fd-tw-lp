.class Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$2;
.super Ljava/lang/Object;
.source "NotificationItem.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .line 722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNotiSoundFinished()V
    .locals 3

    .prologue
    .line 726
    const-string/jumbo v0, "[NotificationItem]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onNotiSoundFinished:nextHandler="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->nextCmdHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->nextCmdHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 728
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "call nextCmdHandler <= onNotiSoundFinished"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 729
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->nextCmdHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 731
    :cond_0
    return-void
.end method
