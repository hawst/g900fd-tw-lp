.class public Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;
.super Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;
.source "SandDropView.java"


# instance fields
.field private POSY:[F


# direct methods
.method public constructor <init>(Landroid/content/Context;FF)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentWidth"    # F
    .param p3, "contentHeight"    # F

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;-><init>(Landroid/content/Context;FF)V

    .line 21
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;->init()V

    .line 22
    return-void
.end method

.method private getPaint1(I)Landroid/graphics/Paint;
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 96
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 97
    .local v0, "paint":Landroid/graphics/Paint;
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 98
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 99
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 100
    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 102
    return-object v0
.end method

.method private getPaint2(I)Landroid/graphics/Paint;
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 107
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 108
    .local v0, "paint":Landroid/graphics/Paint;
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 109
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 110
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 112
    return-object v0
.end method

.method private init()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;->setAlpha(F)V

    .line 26
    return-void
.end method

.method private makePathInfo1()Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;
    .locals 4

    .prologue
    const/16 v3, 0xfe

    .line 74
    const/4 v2, 0x3

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;->POSY:[F

    .line 76
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 77
    .local v1, "path":Landroid/graphics/Path;
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;

    invoke-static {v3, v3, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;->getPaint1(I)Landroid/graphics/Paint;

    move-result-object v2

    .line 78
    const/4 v3, 0x1

    .line 77
    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;-><init>(Landroid/graphics/Path;Landroid/graphics/Paint;I)V

    .line 79
    .local v0, "info":Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;->modifyPathInfo(Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;F)V

    .line 81
    return-object v0

    .line 74
    :array_0
    .array-data 4
        0x41100000    # 9.0f
        0x41e00000    # 28.0f
        0x4203999a    # 32.9f
    .end array-data
.end method

.method private makePathInfo2()Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/high16 v4, 0x41880000    # 17.0f

    const/16 v3, 0xfe

    .line 86
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 88
    .local v0, "path":Landroid/graphics/Path;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;->POSY:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-virtual {v0, v4, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 89
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;->POSY:[F

    aget v1, v1, v5

    invoke-virtual {v0, v4, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 91
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;

    invoke-static {v3, v3, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;->getPaint2(I)Landroid/graphics/Paint;

    move-result-object v2

    invoke-direct {v1, v0, v2, v5}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;-><init>(Landroid/graphics/Path;Landroid/graphics/Paint;I)V

    return-object v1
.end method


# virtual methods
.method protected createPathInfos()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 46
    .local v0, "infos":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;>;"
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;->makePathInfo1()Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;->makePathInfo2()Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    return-object v0
.end method

.method protected modifyPathInfo(Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;F)V
    .locals 9
    .param p1, "info"    # Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;
    .param p2, "ratio"    # F

    .prologue
    const/4 v8, 0x0

    const/high16 v7, 0x41880000    # 17.0f

    const/4 v6, 0x1

    .line 55
    iget-object v3, p1, Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;->PAINT:Landroid/graphics/Paint;

    iget v4, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;->mStrokeWidth:F

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 57
    iget v3, p1, Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;->ID:I

    if-ne v3, v6, :cond_0

    .line 59
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;->POSY:[F

    aget v3, v3, v8

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;->POSY:[F

    aget v4, v4, v6

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;->POSY:[F

    aget v5, v5, v8

    sub-float/2addr v4, v5

    mul-float/2addr v4, p2

    add-float v1, v3, v4

    .line 60
    .local v1, "y1":F
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;->POSY:[F

    aget v2, v3, v6

    .line 62
    .local v2, "y2":F
    iget-object v0, p1, Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;->PATH:Landroid/graphics/Path;

    .line 63
    .local v0, "path":Landroid/graphics/Path;
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 65
    invoke-virtual {v0, v7, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 66
    invoke-virtual {v0, v7, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 68
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;->transform(Landroid/graphics/Path;)V

    .line 70
    .end local v0    # "path":Landroid/graphics/Path;
    .end local v1    # "y1":F
    .end local v2    # "y2":F
    :cond_0
    return-void
.end method

.method public render(F)V
    .locals 0
    .param p1, "ratio"    # F

    .prologue
    .line 30
    invoke-virtual {p0, p1, p1, p1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;->render(FFF)V

    .line 31
    return-void
.end method

.method public render(FFF)V
    .locals 1
    .param p1, "ratio"    # F
    .param p2, "from"    # F
    .param p3, "to"    # F

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 36
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;->setAlpha(F)V

    .line 38
    div-float/2addr v0, p3

    mul-float/2addr p1, v0

    .line 39
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->render(FFF)V

    .line 40
    return-void
.end method
