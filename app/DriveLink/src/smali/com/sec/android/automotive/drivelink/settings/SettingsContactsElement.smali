.class public Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;
.super Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;
.source "SettingsContactsElement.java"


# instance fields
.field private contact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
    .locals 0
    .param p1, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;->contact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 14
    return-void
.end method


# virtual methods
.method public getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;->contact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;->contact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    if-nez v0, :cond_0

    .line 19
    const-string/jumbo v0, ""

    .line 20
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;->contact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getType()Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;->ITEM:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;

    return-object v0
.end method
