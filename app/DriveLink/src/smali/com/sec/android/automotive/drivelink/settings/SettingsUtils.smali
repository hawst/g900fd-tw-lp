.class public Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;
.super Ljava/lang/Object;
.source "SettingsUtils.java"


# static fields
.field protected static final CAR_SELECTION:Ljava/lang/String; = "type = ? and (category = ? or _id = 2) and location_type = ? and bluetooth_mac_address not null"

.field protected static final CAR_SELECTION_ARGS:[Ljava/lang/String;

.field public static final ID_UNREGISTERED_DEVICE:I = -0x1

.field private static final IS_EXIT_DIALOG_OPEN:Ljava/lang/String; = "IS_EXIT_DIALOG_OPEN"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 37
    const-class v0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    .line 49
    new-array v0, v4, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 50
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 51
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    .line 52
    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 49
    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->CAR_SELECTION_ARGS:[Ljava/lang/String;

    .line 55
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static SettingsDrivingDialog(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 734
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 735
    const v2, 0x7f0a045e

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 736
    const v2, 0x7f0a045f

    .line 737
    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils$3;

    invoke-direct {v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils$3;-><init>()V

    .line 736
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 742
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 744
    .local v0, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 745
    return-void
.end method

.method static synthetic access$0()V
    .locals 0

    .prologue
    .line 747
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->exitSettings()V

    return-void
.end method

.method public static addRegisteredCarsPreference(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "settingsCarBTDevice"    # Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    .prologue
    .line 477
    if-nez p1, :cond_0

    .line 478
    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    .line 479
    const-string/jumbo v3, "addRegisteredCarsPreference: settingsCarBTDevice is null"

    .line 478
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    :goto_0
    return-void

    .line 483
    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 484
    .local v1, "values":Landroid/content/ContentValues;
    const-string/jumbo v2, "type"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 485
    const-string/jumbo v2, "name"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getPlaceName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    const-string/jumbo v2, "category"

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 487
    const-string/jumbo v2, "location_type"

    .line 488
    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 487
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 489
    const-string/jumbo v2, "bluetooth_mac_address"

    .line 490
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v3

    .line 489
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    const-string/jumbo v2, "bluetooth_name"

    .line 492
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getPlaceName()Ljava/lang/String;

    move-result-object v3

    .line 491
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    const-string/jumbo v2, "monitoring_status"

    .line 494
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 493
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 496
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 497
    sget-object v3, Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    .line 496
    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 499
    .local v0, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_1

    .line 500
    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getPlaceName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 501
    const-string/jumbo v4, " added to My Place."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 500
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v2

    .line 503
    const-string/jumbo v3, "PREF_LAST_CONNECTED_REGISTERED_CAR"

    .line 505
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v4

    .line 504
    invoke-static {p0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->getDeviceRegisteredId(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    .line 502
    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 507
    :cond_1
    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getPlaceName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 508
    const-string/jumbo v4, " add to My Place failed."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 507
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private static exitSettings()V
    .locals 3

    .prologue
    .line 748
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    .line 749
    const-class v2, Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    .line 748
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 750
    .local v0, "i":Landroid/content/Intent;
    const/high16 v1, 0x44000000    # 512.0f

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 752
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->startActivity(Landroid/content/Intent;)V

    .line 753
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->finish()V

    .line 754
    return-void
.end method

.method public static getDeviceRegisteredId(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 270
    if-nez p1, :cond_0

    .line 271
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    .line 272
    const-string/jumbo v1, "getDeviceRegisteredId: bluetoothDevice is null"

    .line 271
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    const/4 v0, -0x1

    .line 275
    :goto_0
    return v0

    .line 276
    :cond_0
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 275
    invoke-static {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->getDeviceRegisteredId(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static getDeviceRegisteredId(Landroid/content/Context;Ljava/lang/String;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "macAddress"    # Ljava/lang/String;

    .prologue
    const/4 v8, -0x1

    .line 293
    if-eqz p1, :cond_0

    const-string/jumbo v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 294
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    .line 295
    const-string/jumbo v1, "getDeviceRegisteredId: mac address is null"

    .line 294
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    :goto_0
    return v8

    .line 299
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->CAR_SELECTION_ARGS:[Ljava/lang/String;

    .line 300
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->CAR_SELECTION_ARGS:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    .line 298
    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    .line 301
    .local v4, "selectionArgs":[Ljava/lang/String;
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->CAR_SELECTION_ARGS:[Ljava/lang/String;

    array-length v0, v0

    aput-object p1, v4, v0

    .line 302
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 303
    sget-object v1, Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    .line 304
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v5, "_id"

    aput-object v5, v2, v3

    .line 305
    const-string/jumbo v3, "type = ? and (category = ? or _id = 2) and location_type = ? and bluetooth_mac_address not null and bluetooth_mac_address = ?"

    .line 307
    const/4 v5, 0x0

    .line 302
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 308
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_2

    .line 309
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "getDeviceRegisteredId: cursor is null"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 312
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 313
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 314
    .local v6, "count":I
    if-gtz v6, :cond_3

    .line 318
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 322
    :cond_3
    const-string/jumbo v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 321
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 323
    .local v8, "registeredID":I
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static getDeviceRegisteredName(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/4 v5, 0x0

    .line 228
    if-nez p1, :cond_0

    .line 229
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    .line 230
    const-string/jumbo v1, "getDeviceRegisteredName: bluetoothDevice is null"

    .line 229
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    :goto_0
    return-object v5

    .line 234
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->CAR_SELECTION_ARGS:[Ljava/lang/String;

    .line 235
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->CAR_SELECTION_ARGS:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    .line 233
    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    .line 236
    .local v4, "selectionArgs":[Ljava/lang/String;
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->CAR_SELECTION_ARGS:[Ljava/lang/String;

    array-length v0, v0

    .line 237
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 236
    aput-object v1, v4, v0

    .line 238
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 239
    sget-object v1, Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    .line 240
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v8, "name"

    aput-object v8, v2, v3

    .line 241
    const-string/jumbo v3, "type = ? and (category = ? or _id = 2) and location_type = ? and bluetooth_mac_address not null and bluetooth_mac_address = ?"

    .line 238
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 244
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_1

    .line 245
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "getDeviceRegisteredName: cursor is null"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    const-string/jumbo v5, ""

    goto :goto_0

    .line 248
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 250
    const-string/jumbo v0, "name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 249
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 251
    .local v7, "registeredName":Ljava/lang/String;
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v5, v7

    .line 252
    goto :goto_0
.end method

.method public static getPairedBluetoothDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;
    .locals 7
    .param p0, "macAddress"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 126
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    .line 127
    .local v0, "bluetoothAdapter":Landroid/bluetooth/BluetoothAdapter;
    if-eqz v0, :cond_3

    if-eqz p0, :cond_3

    .line 129
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v2

    .line 130
    .local v2, "btDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v2, :cond_1

    .line 131
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 146
    :cond_1
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "getPairedBluetoothDevice: device "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 147
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "is not paired."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 146
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v3

    .line 148
    .end local v2    # "btDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    :goto_0
    return-object v1

    .line 131
    .restart local v2    # "btDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 132
    .local v1, "btDevice":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_0

    .line 137
    .end local v1    # "btDevice":Landroid/bluetooth/BluetoothDevice;
    .end local v2    # "btDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_3
    if-eqz p0, :cond_4

    .line 138
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    .line 139
    const-string/jumbo v5, "getPairedBluetoothDevice: bluetoothAdapter is null"

    .line 138
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v3

    .line 140
    goto :goto_0

    .line 142
    :cond_4
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    .line 143
    const-string/jumbo v5, "getPairedBluetoothDevice: macAddress is null"

    .line 142
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v3

    .line 144
    goto :goto_0
.end method

.method public static getPhoneName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 105
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    .line 106
    .local v0, "bluetooth":Landroid/bluetooth/BluetoothAdapter;
    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getName()Ljava/lang/String;

    move-result-object v1

    .line 109
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getRegisteredCar(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v5, 0x0

    const/4 v2, -0x1

    .line 164
    invoke-static {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isLastConnectedRegisteredCar(Landroid/content/Context;I)Z

    move-result v0

    .line 165
    if-nez v0, :cond_0

    .line 170
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 172
    const-string/jumbo v1, "PREF_LAST_CONNECTED_REGISTERED_CAR"

    .line 171
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v0

    .line 167
    invoke-static {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isRegisteredCar(Landroid/content/Context;I)Z

    move-result v0

    .line 173
    if-nez v0, :cond_1

    .line 174
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    .line 175
    const-string/jumbo v1, "getRegisteredCar: Get first registered car"

    .line 174
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 179
    sget-object v1, Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    .line 180
    new-array v2, v3, [Ljava/lang/String;

    const-string/jumbo v3, "_id"

    aput-object v3, v2, v5

    const-string/jumbo v3, "name"

    aput-object v3, v2, v8

    .line 181
    const-string/jumbo v3, "bluetooth_mac_address"

    aput-object v3, v2, v9

    .line 182
    const-string/jumbo v3, "type = ? and (category = ? or _id = 2) and location_type = ? and bluetooth_mac_address not null"

    .line 183
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->CAR_SELECTION_ARGS:[Ljava/lang/String;

    const/4 v5, 0x0

    .line 178
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 200
    .local v6, "cursor":Landroid/database/Cursor;
    :goto_0
    if-nez v6, :cond_2

    .line 201
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "getRegisteredCar: cursor is null"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    new-instance v7, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    const-string/jumbo v0, ""

    const-string/jumbo v1, ""

    invoke-direct {v7, v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :goto_1
    return-object v7

    .line 186
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    .line 187
    const-string/jumbo v1, "getRegisteredCar: Get last connected registered car"

    .line 186
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    new-array v4, v8, [Ljava/lang/String;

    .line 190
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 192
    const-string/jumbo v1, "PREF_LAST_CONNECTED_REGISTERED_CAR"

    .line 191
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v0

    .line 189
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 194
    .local v4, "selectionArgs":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 195
    sget-object v1, Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    .line 196
    new-array v2, v3, [Ljava/lang/String;

    const-string/jumbo v3, "_id"

    aput-object v3, v2, v5

    const-string/jumbo v3, "name"

    aput-object v3, v2, v8

    .line 197
    const-string/jumbo v3, "bluetooth_mac_address"

    aput-object v3, v2, v9

    .line 198
    const-string/jumbo v3, "_id = ?"

    const/4 v5, 0x0

    .line 194
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .restart local v6    # "cursor":Landroid/database/Cursor;
    goto :goto_0

    .line 204
    .end local v4    # "selectionArgs":[Ljava/lang/String;
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 205
    new-instance v7, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    .line 206
    const-string/jumbo v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 207
    const-string/jumbo v1, "name"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 209
    const-string/jumbo v2, "bluetooth_mac_address"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 208
    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 205
    invoke-direct {v7, v0, v1, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 210
    .local v7, "settingsCarBTDevice":Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method public static hasRegisteredDevice(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 450
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 451
    sget-object v1, Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v9, [Ljava/lang/String;

    const-string/jumbo v3, "1"

    aput-object v3, v2, v8

    .line 452
    const-string/jumbo v3, "type = ? and (category = ? or _id = 2) and location_type = ? and bluetooth_mac_address not null"

    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->CAR_SELECTION_ARGS:[Ljava/lang/String;

    .line 453
    const/4 v5, 0x0

    .line 450
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 454
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_0

    .line 455
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "hasRegisteredDevice: cursor is null"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v8

    .line 462
    :goto_0
    return v0

    .line 458
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 459
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 460
    .local v6, "count":I
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "hasRegisteredDevice: count "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 462
    if-lez v6, :cond_1

    move v0, v9

    goto :goto_0

    :cond_1
    move v0, v8

    goto :goto_0
.end method

.method public static initDialogExit(Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;Landroid/os/Bundle;)V
    .locals 1
    .param p0, "activity"    # Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 664
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isExitDialogOpen(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 665
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->showSetupWizardDialogExit(Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;)V

    .line 667
    :cond_0
    return-void
.end method

.method private static isDefaultPlace(Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;)Z
    .locals 2
    .param p0, "settingsCarBTDevice"    # Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    .prologue
    .line 609
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getId()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isExitDialogOpen(Landroid/os/Bundle;)Z
    .locals 1
    .param p0, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 624
    if-eqz p0, :cond_0

    .line 626
    const-string/jumbo v0, "IS_EXIT_DIALOG_OPEN"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 628
    const-string/jumbo v0, "IS_EXIT_DIALOG_OPEN"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629
    const/4 v0, 0x1

    .line 632
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFirstAccess(Landroid/app/Activity;)Z
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 725
    instance-of v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/TutorialGuideActivity01;

    if-eqz v0, :cond_0

    .line 726
    const/4 v0, 0x1

    .line 728
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 729
    const-string/jumbo v1, "EXTRA_NAME_FIRST_ACCESS"

    .line 728
    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isLastConnectedRegisteredCar(Landroid/content/Context;I)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "settingsCarBTDeviceId"    # I

    .prologue
    .line 360
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 362
    const-string/jumbo v2, "PREF_LAST_CONNECTED_REGISTERED_CAR"

    .line 363
    const/4 v3, -0x1

    .line 361
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v0

    .line 364
    .local v0, "lastConnectedRegisteredCarId":I
    if-ne v0, p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isLastConnectedRegisteredCar(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "settingsCarBTDevice"    # Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    .prologue
    .line 340
    .line 341
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getId()I

    move-result v0

    .line 340
    invoke-static {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isLastConnectedRegisteredCar(Landroid/content/Context;I)Z

    move-result v0

    return v0
.end method

.method public static isRegisteredCar(Landroid/content/Context;I)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "settingsCarBTDeviceId"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 380
    new-array v4, v9, [Ljava/lang/String;

    .line 382
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 384
    const-string/jumbo v1, "PREF_LAST_CONNECTED_REGISTERED_CAR"

    .line 385
    const/4 v2, -0x1

    .line 383
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v0

    .line 381
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    .line 386
    .local v4, "selectionArgs":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 387
    sget-object v1, Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    .line 388
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "_id"

    aput-object v3, v2, v8

    const-string/jumbo v3, "name"

    aput-object v3, v2, v9

    const/4 v3, 0x2

    .line 389
    const-string/jumbo v5, "bluetooth_mac_address"

    aput-object v5, v2, v3

    .line 390
    const-string/jumbo v3, "_id = ?"

    const/4 v5, 0x0

    .line 386
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 391
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_0

    .line 392
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "isRegisteredCar: cursor is null"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v8

    .line 398
    :goto_0
    return v0

    .line 395
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 396
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 397
    .local v6, "count":I
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 398
    if-lez v6, :cond_1

    move v0, v9

    goto :goto_0

    :cond_1
    move v0, v8

    goto :goto_0
.end method

.method public static isRegisteredDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 415
    const/4 v0, -0x1

    .line 416
    invoke-static {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->getDeviceRegisteredId(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)I

    move-result v1

    .line 415
    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSameDevice(Landroid/bluetooth/BluetoothDevice;Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;)Z
    .locals 2
    .param p0, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;
    .param p1, "settingsCarBTDevice"    # Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    .prologue
    .line 433
    if-eqz p0, :cond_0

    .line 434
    if-eqz p1, :cond_0

    .line 435
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 436
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v1

    .line 435
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 436
    if-eqz v0, :cond_0

    .line 433
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static onSaveInstanceStateIsExitDialogOpen(Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;Landroid/os/Bundle;)V
    .locals 2
    .param p0, "activity"    # Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 648
    const-string/jumbo v0, "IS_EXIT_DIALOG_OPEN"

    .line 649
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->isExitDialogOpen()Z

    move-result v1

    .line 648
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 650
    return-void
.end method

.method public static removeRegisteredCarsPreference(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "settingsCarBTDevice"    # Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    .prologue
    const/4 v5, 0x0

    .line 525
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getId()I

    move-result v1

    if-nez v1, :cond_1

    .line 526
    :cond_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    .line 527
    const-string/jumbo v2, "removeRegisteredCarsPreference: settingsCarBTDevice or id is null"

    .line 526
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    :goto_0
    return-void

    .line 530
    :cond_1
    invoke-static {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isLastConnectedRegisteredCar(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;)Z

    move-result v1

    .line 531
    if-eqz v1, :cond_2

    .line 532
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 533
    const-string/jumbo v2, "PREF_LAST_CONNECTED_REGISTERED_CAR"

    .line 534
    const/4 v3, -0x1

    .line 532
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;I)V

    .line 536
    :cond_2
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isDefaultPlace(Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 538
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "removeRegisteredCarsPreference: remove "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 539
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getBtName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 538
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 541
    sget-object v2, Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    .line 542
    const-string/jumbo v3, "_id = ?"

    .line 543
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 544
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 541
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 546
    :cond_3
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 547
    .local v0, "values":Landroid/content/ContentValues;
    const-string/jumbo v1, "bluetooth_name"

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    const-string/jumbo v1, "bluetooth_mac_address"

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    .line 552
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "removeRegisteredCarsPreference: remove default "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 553
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getBtName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 552
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 551
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 555
    sget-object v2, Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    .line 556
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getId()I

    move-result v3

    int-to-long v3, v3

    .line 555
    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 554
    invoke-virtual {v1, v2, v0, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static renameRegisteredCarsPreference(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "settingsCarBTDevice"    # Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    .prologue
    const/4 v5, 0x0

    .line 574
    if-eqz p1, :cond_0

    .line 575
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getBtName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 576
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getId()I

    move-result v1

    if-nez v1, :cond_1

    .line 577
    :cond_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    .line 578
    const-string/jumbo v2, "renameRegisteredCarsPreference: settingsCarBTDevice getBtName or id is null"

    .line 577
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    :goto_0
    return-void

    .line 581
    :cond_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->TAG:Ljava/lang/String;

    .line 582
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "rename car to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getBtName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "; id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 583
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 582
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 581
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 584
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 585
    .local v0, "values":Landroid/content/ContentValues;
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isDefaultPlace(Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 586
    const-string/jumbo v1, "name"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getBtName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    :cond_2
    const-string/jumbo v1, "bluetooth_name"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getBtName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 592
    sget-object v2, Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    .line 593
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getId()I

    move-result v3

    int-to-long v3, v3

    .line 592
    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 591
    invoke-virtual {v1, v2, v0, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static showSetupWizardDialogExit(Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;)V
    .locals 5
    .param p0, "activity"    # Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;

    .prologue
    const/4 v3, 0x1

    .line 680
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->setExitDialogOpen(Z)V

    .line 681
    new-instance v1, Landroid/app/AlertDialog$Builder;

    .line 682
    const/4 v2, 0x4

    .line 681
    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 683
    .local v1, "exitdlg":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0a0256

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 684
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 685
    const v3, 0x7f0a0254

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 686
    new-instance v4, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils$1;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils$1;-><init>(Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;)V

    .line 685
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 698
    const v3, 0x7f0a0255

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 699
    new-instance v4, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils$2;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils$2;-><init>(Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;)V

    .line 697
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 707
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 709
    .local v0, "alert":Landroid/app/AlertDialog;
    const v2, 0x7f0a0258

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 711
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 712
    return-void
.end method

.method public static startBluetoothSettings(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    new-instance v0, Landroid/content/Intent;

    .line 72
    const-string/jumbo v1, "android.settings.BLUETOOTH_SETTINGS"

    .line 71
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 73
    .local v0, "settingsIntent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 74
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 75
    return-void
.end method

.method public static startBluetoothSettingsForResult(Landroid/app/Activity;I)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "requestCode"    # I

    .prologue
    .line 89
    new-instance v0, Landroid/content/Intent;

    .line 90
    const-string/jumbo v1, "android.settings.BLUETOOTH_SETTINGS"

    .line 89
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 91
    .local v0, "settingsIntent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 92
    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 93
    return-void
.end method
