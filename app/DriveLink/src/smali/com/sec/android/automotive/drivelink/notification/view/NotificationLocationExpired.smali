.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationLocationExpired.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field protected static mContext:Landroid/content/Context;


# instance fields
.field private mCancelButton:Landroid/widget/Button;

.field private mExtendButton:Landroid/widget/Button;

.field private mImageView:Landroid/widget/ImageView;

.field private mViewLocal:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mContext:Landroid/content/Context;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mViewLocal:Landroid/view/View;

    .line 21
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mImageView:Landroid/widget/ImageView;

    .line 23
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mCancelButton:Landroid/widget/Button;

    .line 24
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mExtendButton:Landroid/widget/Button;

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mViewLocal:Landroid/view/View;

    .line 21
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mImageView:Landroid/widget/ImageView;

    .line 23
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mCancelButton:Landroid/widget/Button;

    .line 24
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mExtendButton:Landroid/widget/Button;

    .line 63
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->initIndicatorView(Landroid/content/Context;)V

    .line 64
    return-void
.end method

.method private static callDurationActivity()V
    .locals 0

    .prologue
    .line 157
    return-void
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v5, 0x7f02023b

    .line 73
    sput-object p1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mContext:Landroid/content/Context;

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 76
    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 75
    check-cast v0, Landroid/view/LayoutInflater;

    .line 78
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mViewLocal:Landroid/view/View;

    if-nez v3, :cond_1

    .line 80
    const v3, 0x7f0300ab

    .line 79
    invoke-virtual {v0, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mViewLocal:Landroid/view/View;

    .line 82
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mViewLocal:Landroid/view/View;

    .line 83
    const v4, 0x7f0902e2

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 82
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mCancelButton:Landroid/widget/Button;

    .line 84
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mViewLocal:Landroid/view/View;

    const v4, 0x7f0902e3

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mExtendButton:Landroid/widget/Button;

    .line 86
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mExtendButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mViewLocal:Landroid/view/View;

    .line 89
    const v4, 0x7f0902e0

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 88
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mImageView:Landroid/widget/ImageView;

    .line 92
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 93
    const v4, 0x7f020238

    .line 91
    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 95
    .local v2, "shareBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 94
    invoke-static {v3, v2, v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 97
    .local v1, "maskedBitmap":Landroid/graphics/Bitmap;
    if-nez v1, :cond_0

    .line 98
    sget-object v3, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 99
    const v4, 0x7f02023d

    .line 98
    invoke-static {v3, v4, v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 102
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 103
    const v4, 0x7f02023c

    .line 102
    invoke-static {v3, v1, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageOverlay(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 105
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 107
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->readNotificationTTS()V

    .line 110
    .end local v1    # "maskedBitmap":Landroid/graphics/Bitmap;
    .end local v2    # "shareBitmap":Landroid/graphics/Bitmap;
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->setUseAutoTimeout(Z)V

    .line 111
    return-void
.end method

.method public static positiveButton()V
    .locals 0

    .prologue
    .line 147
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->callDurationActivity()V

    .line 148
    return-void
.end method

.method private readNotificationTTS()V
    .locals 0

    .prologue
    .line 121
    return-void
.end method


# virtual methods
.method protected getTTSText()Ljava/lang/String;
    .locals 3

    .prologue
    .line 125
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 126
    const v2, 0x7f0a02e1

    .line 125
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 127
    .local v0, "prompt":Ljava/lang/String;
    return-object v0
.end method

.method negativeButton()V
    .locals 0

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->dismiss()V

    .line 161
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 132
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 133
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 144
    :goto_0
    return-void

    .line 135
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->negativeButton()V

    goto :goto_0

    .line 138
    :pswitch_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->positiveButton()V

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;->dismiss()V

    goto :goto_0

    .line 133
    nop

    :pswitch_data_0
    .packed-switch 0x7f0902e2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onFlowCommandCancel(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 176
    return-void
.end method

.method public onFlowCommandNo(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 181
    return-void
.end method

.method public onFlowCommandRoute(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 171
    return-void
.end method

.method public onFlowCommandYes(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 166
    return-void
.end method
