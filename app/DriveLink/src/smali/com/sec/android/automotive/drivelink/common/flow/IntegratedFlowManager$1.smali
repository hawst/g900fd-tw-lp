.class Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager$1;
.super Ljava/lang/Object;
.source "IntegratedFlowManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const v2, 0x7f0a0239

    .line 334
    # getter for: Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->access$0()Landroid/content/Context;

    move-result-object v0

    .line 335
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 336
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    .line 334
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 339
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    .line 341
    # getter for: Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->access$0()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 340
    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->displaySystemTurn(Ljava/lang/CharSequence;)V

    .line 344
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    .line 347
    const-string/jumbo v1, "DM_LOCATION"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->getFiledIdByFlowId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 346
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v1

    .line 345
    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 348
    # invokes: Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startListening()V
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->access$1()V

    .line 349
    return-void
.end method
