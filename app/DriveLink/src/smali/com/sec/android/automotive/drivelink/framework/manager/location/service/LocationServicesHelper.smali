.class public Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;
.super Ljava/lang/Object;
.source "LocationServicesHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "[LocationServicesHelper]"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mbaseManager"    # Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method private groupToJSONObject(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Lorg/json/JSONObject;
    .locals 9
    .param p1, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 231
    const/4 v2, 0x0

    .line 233
    .local v2, "jsonParam":Lorg/json/JSONObject;
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    .end local v2    # "jsonParam":Lorg/json/JSONObject;
    .local v3, "jsonParam":Lorg/json/JSONObject;
    :try_start_1
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getDestination()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 235
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 236
    .local v0, "destination":Lorg/json/JSONObject;
    const-string/jumbo v4, "name"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 237
    const-string/jumbo v4, "address"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v5

    .line 238
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v5

    .line 237
    invoke-virtual {v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 239
    const-string/jumbo v4, "latitude"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v5

    invoke-virtual {v0, v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 241
    const-string/jumbo v4, "longitude"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v5

    invoke-virtual {v0, v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 242
    const-string/jumbo v4, "destination"

    invoke-virtual {v3, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 245
    .end local v0    # "destination":Lorg/json/JSONObject;
    :cond_0
    const-string/jumbo v4, "shareType"

    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 247
    const-string/jumbo v4, "duration"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getDuration()J

    move-result-wide v5

    const-wide/32 v7, 0xea60

    div-long/2addr v5, v7

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v2, v3

    .line 251
    .end local v3    # "jsonParam":Lorg/json/JSONObject;
    :goto_0
    return-object v3

    .line 249
    .restart local v2    # "jsonParam":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 250
    .local v1, "e":Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    move-object v3, v2

    .line 251
    .local v3, "jsonParam":Ljava/lang/Object;
    goto :goto_0

    .line 249
    .end local v1    # "e":Lorg/json/JSONException;
    .end local v2    # "jsonParam":Lorg/json/JSONObject;
    .local v3, "jsonParam":Lorg/json/JSONObject;
    :catch_1
    move-exception v1

    move-object v2, v3

    .end local v3    # "jsonParam":Lorg/json/JSONObject;
    .restart local v2    # "jsonParam":Lorg/json/JSONObject;
    goto :goto_1
.end method


# virtual methods
.method public addFriendsToGroup(ILjava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 1
    .param p1, "userId"    # I
    .param p3, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 135
    .local p2, "friends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    invoke-static {p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->addFriendsToGroup(ILjava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v0

    return v0
.end method

.method public changeGroupDestination(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 10
    .param p1, "sspGuid"    # Ljava/lang/String;
    .param p2, "accessToken"    # Ljava/lang/String;
    .param p3, "userId"    # Ljava/lang/String;
    .param p4, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    const/4 v9, 0x0

    .line 167
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 168
    if-eqz p4, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 194
    :cond_0
    :goto_0
    return v9

    .line 171
    :cond_1
    invoke-virtual {p4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v6

    check-cast v6, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    .line 172
    .local v6, "destination":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;
    if-nez v6, :cond_2

    .line 173
    const-string/jumbo v0, "[LocationServicesHelper]"

    const-string/jumbo v1, "Destination was not setted ... "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 178
    :cond_2
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 179
    .local v5, "jsonParameters":Lorg/json/JSONObject;
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 180
    .local v7, "destinationParams":Lorg/json/JSONObject;
    const-string/jumbo v0, "name"

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->getLocationName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 181
    const-string/jumbo v0, "address"

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->getLocationAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 182
    const-string/jumbo v0, "latitude"

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->getLatitude()D

    move-result-wide v1

    invoke-virtual {v7, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 183
    const-string/jumbo v0, "longitude"

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->getLongitude()D

    move-result-wide v1

    invoke-virtual {v7, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 185
    const-string/jumbo v0, "duration"

    invoke-virtual {p4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getDuration()J

    move-result-wide v1

    const-wide/32 v3, 0xea60

    div-long/2addr v1, v3

    invoke-virtual {v5, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 186
    const-string/jumbo v0, "destination"

    invoke-virtual {v5, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 189
    invoke-virtual {p4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 188
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->updateShareGroup(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILorg/json/JSONObject;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    .line 191
    .local v9, "result":Z
    goto :goto_0

    .line 192
    .end local v5    # "jsonParameters":Lorg/json/JSONObject;
    .end local v7    # "destinationParams":Lorg/json/JSONObject;
    .end local v9    # "result":Z
    :catch_0
    move-exception v8

    .line 193
    .local v8, "e":Lorg/json/JSONException;
    invoke-virtual {v8}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public createShareGroup(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 1
    .param p1, "profile"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 31
    invoke-static {p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->createShareGroup(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v0

    return v0
.end method

.method public getGroupById(II)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .locals 1
    .param p1, "userId"    # I
    .param p2, "groupId"    # I

    .prologue
    .line 208
    invoke-static {p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->getGroupById(II)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    move-result-object v0

    return-object v0
.end method

.method public getGroupByUrl(ILjava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .locals 1
    .param p1, "userId"    # I
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 204
    invoke-static {p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->getGroupByUrl(ILjava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    move-result-object v0

    return-object v0
.end method

.method public getGroupShared(ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 1
    .param p1, "userId"    # I
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 141
    invoke-static {p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->getGroupShared(ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v0

    return v0
.end method

.method public getMyProfile(I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .locals 1
    .param p1, "userId"    # I

    .prologue
    .line 126
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->getMyProfile(I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v0

    return-object v0
.end method

.method public getProfileByPhoneNumber(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;
    .locals 1
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 122
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->getProfileByPhoneNumber(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;

    move-result-object v0

    return-object v0
.end method

.method public getSharedLocationFromGroupId(ILjava/lang/String;I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .locals 1
    .param p1, "userId"    # I
    .param p2, "phoneNumber"    # Ljava/lang/String;
    .param p3, "groupId"    # I

    .prologue
    .line 106
    invoke-static {p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->getSharedLocationFromGroupId(ILjava/lang/String;I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    move-result-object v0

    return-object v0
.end method

.method public getSharedLocationFromUrl(ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .locals 1
    .param p1, "userId"    # I
    .param p2, "phoneNumber"    # Ljava/lang/String;
    .param p3, "url"    # Ljava/lang/String;

    .prologue
    .line 101
    invoke-static {p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->getSharedLocationFromUrl(ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    move-result-object v0

    return-object v0
.end method

.method public quitFromGroup(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;I)Z
    .locals 1
    .param p1, "profile"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .param p2, "groupId"    # I

    .prologue
    .line 151
    invoke-static {p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->quitFromGroup(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;I)Z

    move-result v0

    return v0
.end method

.method public removeGroupShared(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 1
    .param p1, "profile"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 200
    invoke-static {p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->removeGroupShared(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v0

    return v0
.end method

.method public restartGroupShared(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 6
    .param p1, "sspGuid"    # Ljava/lang/String;
    .param p2, "accessToken"    # Ljava/lang/String;
    .param p3, "userId"    # Ljava/lang/String;
    .param p4, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    const/4 v0, 0x0

    .line 213
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 214
    if-eqz p4, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 215
    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v1

    if-ltz v1, :cond_0

    .line 216
    invoke-virtual {p4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 217
    invoke-virtual {p4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_2

    .line 218
    :cond_0
    const-string/jumbo v1, "[LocationServicesHelper]"

    const-string/jumbo v2, "Could not restart shared group ... "

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :cond_1
    :goto_0
    return v0

    .line 221
    :cond_2
    invoke-direct {p0, p4}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->groupToJSONObject(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Lorg/json/JSONObject;

    move-result-object v5

    .line 222
    .local v5, "params":Lorg/json/JSONObject;
    if-eqz v5, :cond_1

    .line 226
    invoke-virtual {p4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 225
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/LocationServicesHelper;->updateShareGroup(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILorg/json/JSONObject;)Z

    move-result v0

    goto :goto_0
.end method

.method public setMyLocation(ILcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 1
    .param p1, "userId"    # I
    .param p2, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 96
    invoke-static {p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->setMyLocation(ILcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v0

    return v0
.end method

.method public updateParticipantStatus(IILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;J)Z
    .locals 1
    .param p1, "userId"    # I
    .param p2, "groupId"    # I
    .param p3, "status"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;
    .param p4, "duration"    # J

    .prologue
    .line 146
    invoke-static {p1, p2, p3, p4, p5}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->updateParticipantStatus(IILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;J)Z

    move-result v0

    return v0
.end method

.method public updateProfile(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)Z
    .locals 1
    .param p1, "profile"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    .prologue
    .line 130
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->updateProfile(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)Z

    move-result v0

    return v0
.end method

.method public updateShareGroup(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILorg/json/JSONObject;)Z
    .locals 2
    .param p1, "sspGuid"    # Ljava/lang/String;
    .param p2, "accessToken"    # Ljava/lang/String;
    .param p3, "userId"    # Ljava/lang/String;
    .param p4, "groupId"    # I
    .param p5, "params"    # Lorg/json/JSONObject;

    .prologue
    .line 156
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 157
    if-eqz p5, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 158
    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 159
    :cond_0
    const/4 v0, 0x0

    .line 162
    :goto_0
    return v0

    .line 160
    :cond_1
    invoke-static {p1, p2, p3, p4, p5}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnHandle;->updateShareGroup(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILorg/json/JSONObject;)Z

    move-result v0

    .line 162
    .local v0, "result":Z
    goto :goto_0
.end method
