.class public Lcom/sec/android/automotive/drivelink/location/adapter/ArrivedParticipantAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ArrivedParticipantAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final inflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p2, "objects":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 22
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/adapter/ArrivedParticipantAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 23
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 33
    if-nez p2, :cond_0

    .line 34
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/adapter/ArrivedParticipantAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 35
    const v4, 0x7f030057

    const/4 v5, 0x0

    .line 34
    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    move-object v1, p2

    .line 38
    check-cast v1, Landroid/widget/TextView;

    .line 40
    .local v1, "tv":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/location/adapter/ArrivedParticipantAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;

    .line 42
    .local v0, "participant":Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getName()Ljava/lang/String;

    move-result-object v2

    .line 43
    .local v2, "txt":Ljava/lang/String;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_2

    .line 44
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getPhone()Ljava/lang/String;

    move-result-object v2

    .line 46
    :cond_2
    if-nez p1, :cond_3

    .line 47
    const v3, 0x7f0203c0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 53
    :goto_0
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 55
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    return-object p2

    .line 48
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/adapter/ArrivedParticipantAdapter;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne p1, v3, :cond_4

    .line 49
    const v3, 0x7f0203be

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_0

    .line 51
    :cond_4
    const v3, 0x7f0203bf

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_0
.end method
