.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLLocationItem.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x5eb0c72768bc9f2dL


# instance fields
.field private mContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

.field private mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

.field private mLocationDescription:Ljava/lang/String;

.field private mLocationTitle:Ljava/lang/String;

.field private mLocationType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

.field private mPhoneNumber:Ljava/lang/String;

.field private mStatus:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    .line 11
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    .line 12
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationTitle:Ljava/lang/String;

    .line 13
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationDescription:Ljava/lang/String;

    .line 16
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->NOT_RESPONSED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mStatus:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    .line 18
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 19
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 20
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mPhoneNumber:Ljava/lang/String;

    .line 23
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mPhoneNumber:Ljava/lang/String;

    .line 24
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 25
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;)V
    .locals 2
    .param p1, "locationType"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    .line 11
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    .line 12
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationTitle:Ljava/lang/String;

    .line 13
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationDescription:Ljava/lang/String;

    .line 16
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->NOT_RESPONSED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mStatus:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    .line 18
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 19
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 20
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mPhoneNumber:Ljava/lang/String;

    .line 28
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    .line 29
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationTitle:Ljava/lang/String;

    .line 30
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationDescription:Ljava/lang/String;

    .line 31
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 32
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 103
    if-ne p0, p1, :cond_1

    .line 136
    :cond_0
    :goto_0
    return v1

    .line 105
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 106
    goto :goto_0

    .line 107
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 108
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 109
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;

    .line 110
    .local v0, "other":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    if-nez v3, :cond_4

    .line 111
    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    if-eqz v3, :cond_7

    move v1, v2

    .line 112
    goto :goto_0

    .line 114
    :cond_4
    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    if-nez v3, :cond_5

    move v1, v2

    .line 115
    goto :goto_0

    .line 117
    :cond_5
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v3

    iget-object v5, v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v5

    cmpl-double v3, v3, v5

    if-nez v3, :cond_6

    .line 118
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v3

    iget-object v5, v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 119
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v5

    .line 118
    cmpl-double v3, v3, v5

    if-eqz v3, :cond_7

    :cond_6
    move v1, v2

    .line 120
    goto :goto_0

    .line 124
    :cond_7
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationTitle:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 125
    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationTitle:Ljava/lang/String;

    if-eqz v3, :cond_9

    move v1, v2

    .line 126
    goto :goto_0

    .line 127
    :cond_8
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationTitle:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationTitle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    .line 128
    goto :goto_0

    .line 129
    :cond_9
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    if-eq v3, v4, :cond_a

    move v1, v2

    .line 130
    goto :goto_0

    .line 131
    :cond_a
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mPhoneNumber:Ljava/lang/String;

    if-nez v3, :cond_b

    .line 132
    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mPhoneNumber:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 133
    goto :goto_0

    .line 134
    :cond_b
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mPhoneNumber:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 135
    goto :goto_0
.end method

.method public getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    return-object v0
.end method

.method public getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    return-object v0
.end method

.method public getLocationDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getLocationTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getLocationType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mStatus:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 88
    const/16 v0, 0x1f

    .line 89
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 91
    .local v1, "result":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    if-nez v2, :cond_0

    move v2, v3

    .line 90
    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 92
    mul-int/lit8 v4, v1, 0x1f

    .line 93
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationTitle:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    .line 92
    :goto_1
    add-int v1, v4, v2

    .line 94
    mul-int/lit8 v4, v1, 0x1f

    .line 95
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    if-nez v2, :cond_2

    move v2, v3

    .line 94
    :goto_2
    add-int v1, v4, v2

    .line 96
    mul-int/lit8 v2, v1, 0x1f

    .line 97
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mPhoneNumber:Ljava/lang/String;

    if-nez v4, :cond_3

    .line 96
    :goto_3
    add-int v1, v2, v3

    .line 98
    return v1

    .line 91
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    .line 93
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationTitle:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 95
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->hashCode()I

    move-result v2

    goto :goto_2

    .line 97
    :cond_3
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_3
.end method

.method public setContact(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
    .locals 0
    .param p1, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 68
    return-void
.end method

.method public setLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 0
    .param p1, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 60
    return-void
.end method

.method protected setLocationDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationDescription:Ljava/lang/String;

    .line 52
    return-void
.end method

.method protected setLocationTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mLocationTitle:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mPhoneNumber:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public setStatus(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;)V
    .locals 0
    .param p1, "statusCode"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->mStatus:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    .line 84
    return-void
.end method
