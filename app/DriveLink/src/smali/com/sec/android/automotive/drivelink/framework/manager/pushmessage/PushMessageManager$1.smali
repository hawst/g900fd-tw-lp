.class Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager$1;
.super Ljava/lang/Object;
.source "PushMessageManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 136
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onServiceConnected"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p2

    .line 137
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$ServiceBinder;

    .line 138
    .local v0, "binder":Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$ServiceBinder;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$ServiceBinder;->registerPushMessageServiceListener(Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/IPushMessageServiceListener;)V

    .line 139
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/ComponentName;

    .prologue
    .line 143
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    return-void
.end method
