.class public Lcom/sec/android/automotive/drivelink/location/places/impl/PlaceParser;
.super Ljava/lang/Object;
.source "PlaceParser.java"


# static fields
.field private static final ROOT_AUTOCOMPLETE:Ljava/lang/String; = "predictions"

.field private static final ROOT_DETAIL:Ljava/lang/String; = "result"

.field private static final ROOT_NEARBY:Ljava/lang/String; = "results"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public parse(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 10
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 43
    .local v5, "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :try_start_0
    const-string/jumbo v8, "results"

    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 49
    .local v7, "results":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-lt v2, v8, :cond_0

    .line 68
    .end local v2    # "i":I
    .end local v7    # "results":Lorg/json/JSONArray;
    :goto_1
    return-object v5

    .line 50
    .restart local v2    # "i":I
    .restart local v7    # "results":Lorg/json/JSONArray;
    :cond_0
    invoke-virtual {v7, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 51
    .local v6, "result":Lorg/json/JSONObject;
    const-string/jumbo v8, "geometry"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 52
    .local v1, "geometry":Lorg/json/JSONObject;
    const-string/jumbo v8, "location"

    invoke-virtual {v1, v8}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/json/JSONObject;

    .line 54
    .local v3, "location":Lorg/json/JSONObject;
    new-instance v4, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;

    invoke-direct {v4}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;-><init>()V

    .line 55
    .local v4, "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    const-string/jumbo v8, "vicinity"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->setAddress(Ljava/lang/String;)V

    .line 56
    const-string/jumbo v8, "icon"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->setIcon(Ljava/lang/String;)V

    .line 57
    const-string/jumbo v8, "name"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->setName(Ljava/lang/String;)V

    .line 58
    const-string/jumbo v8, "id"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->setId(Ljava/lang/String;)V

    .line 59
    const-string/jumbo v8, "lat"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->setLat(D)V

    .line 60
    const-string/jumbo v8, "lng"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->setLng(D)V

    .line 62
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 64
    .end local v1    # "geometry":Lorg/json/JSONObject;
    .end local v2    # "i":I
    .end local v3    # "location":Lorg/json/JSONObject;
    .end local v4    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    .end local v6    # "result":Lorg/json/JSONObject;
    .end local v7    # "results":Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 65
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method

.method public parseAutocomplete(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 7
    .param p1, "json"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 110
    .local v3, "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :try_start_0
    const-string/jumbo v6, "predictions"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 116
    .local v5, "results":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-lt v1, v6, :cond_0

    .line 130
    .end local v1    # "i":I
    .end local v5    # "results":Lorg/json/JSONArray;
    :goto_1
    return-object v3

    .line 117
    .restart local v1    # "i":I
    .restart local v5    # "results":Lorg/json/JSONArray;
    :cond_0
    invoke-virtual {v5, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 119
    .local v4, "result":Lorg/json/JSONObject;
    new-instance v2, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;-><init>()V

    .line 120
    .local v2, "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    const-string/jumbo v6, "description"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->setName(Ljava/lang/String;)V

    .line 121
    const-string/jumbo v6, "id"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->setId(Ljava/lang/String;)V

    .line 122
    const-string/jumbo v6, "reference"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->setReference(Ljava/lang/String;)V

    .line 124
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 126
    .end local v1    # "i":I
    .end local v2    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    .end local v4    # "result":Lorg/json/JSONObject;
    .end local v5    # "results":Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method

.method public parseDetail(Lorg/json/JSONObject;)Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    .locals 8
    .param p1, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 79
    const/4 v3, 0x0

    .line 85
    .local v3, "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    :try_start_0
    const-string/jumbo v6, "result"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 86
    .local v5, "result":Lorg/json/JSONObject;
    const-string/jumbo v6, "geometry"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 87
    .local v1, "geometry":Lorg/json/JSONObject;
    const-string/jumbo v6, "location"

    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/json/JSONObject;

    .line 89
    .local v2, "location":Lorg/json/JSONObject;
    new-instance v4, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;

    invoke-direct {v4}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    .end local v3    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    .local v4, "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    :try_start_1
    const-string/jumbo v6, "lat"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->setLat(D)V

    .line 91
    const-string/jumbo v6, "lng"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->setLng(D)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v3, v4

    .line 96
    .end local v1    # "geometry":Lorg/json/JSONObject;
    .end local v2    # "location":Lorg/json/JSONObject;
    .end local v4    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    .end local v5    # "result":Lorg/json/JSONObject;
    .restart local v3    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    :goto_0
    return-object v3

    .line 92
    :catch_0
    move-exception v0

    .line 93
    .local v0, "e":Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 92
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v3    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    .restart local v1    # "geometry":Lorg/json/JSONObject;
    .restart local v2    # "location":Lorg/json/JSONObject;
    .restart local v4    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    .restart local v5    # "result":Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    move-object v3, v4

    .end local v4    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    .restart local v3    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    goto :goto_1
.end method
