.class public Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/CMCCNavigationAdapter;
.super Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
.source "CMCCNavigationAdapter.java"


# static fields
.field private static final INTENT_ACTION_NAV:Ljava/lang/String; = "com.cmcc.cmnavi.action.NAV"

.field private static final KEY_LATITUTDE:Ljava/lang/String; = "latitude"

.field private static final KEY_LONGTITUDE:Ljava/lang/String; = "longtitude"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;-><init>()V

    .line 27
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/CMCCNavigationAdapter;->setNeedOrigin(Z)V

    .line 28
    return-void
.end method


# virtual methods
.method public getNaviPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    const-string/jumbo v0, "com.autonavi.cmccmap"

    return-object v0
.end method

.method public getNavigationIntent(DD)Landroid/content/Intent;
    .locals 3
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 35
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 36
    .local v0, "navigationIntent":Landroid/content/Intent;
    const-string/jumbo v1, "com.cmcc.cmnavi.action.NAV"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 37
    const-string/jumbo v1, "latitude"

    invoke-static {p1, p2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 38
    const-string/jumbo v1, "longtitude"

    invoke-static {p3, p4}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 39
    const/high16 v1, 0x10200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 42
    return-object v0
.end method

.method public startMap(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->startMap(Landroid/content/Context;)V

    .line 48
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 49
    .local v3, "navigationIntent":Landroid/content/Intent;
    const-string/jumbo v4, "com.cmcc.cmnavi.action.NAV"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    const/high16 v4, 0x10200000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 53
    :try_start_0
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/CMCCNavigationAdapter;->setMultiWindowIntentToNavi(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v3

    .line 56
    invoke-virtual {p1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :goto_0
    return-void

    .line 57
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 58
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v4

    .line 61
    const-string/jumbo v5, "PREF_SETTINGS_MY_NAVIGATION"

    .line 62
    const/4 v6, 0x0

    .line 60
    invoke-virtual {v4, v5, v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v2

    .line 63
    .local v2, "mapID":I
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/CMCCNavigationAdapter;->getInstallNavigationApp(I)Landroid/content/Intent;

    move-result-object v1

    .line 64
    .local v1, "goToMarket":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 65
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 67
    :cond_0
    const-string/jumbo v4, "Error CMCC Navi"

    .line 68
    const-string/jumbo v5, "Error on starting CMCC Navigation Intent"

    .line 67
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startMultiNavi(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;)Z
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pacel"    # Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    .prologue
    .line 73
    const/4 v5, 0x1

    .line 75
    .local v5, "ret":Z
    iget v6, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->isStartMap:I

    if-eqz v6, :cond_0

    .line 77
    iget v6, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->isStartMap:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 82
    const v6, 0x7f0a061c

    :try_start_0
    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 84
    .local v2, "mTts":Ljava/lang/String;
    iget-wide v6, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLat:D

    .line 85
    iget-wide v8, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLng:D

    .line 84
    invoke-virtual {p0, v6, v7, v8, v9}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/CMCCNavigationAdapter;->getNavigationIntent(DD)Landroid/content/Intent;

    move-result-object v4

    .line 86
    .local v4, "navigationIntent":Landroid/content/Intent;
    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/CMCCNavigationAdapter;->setMultiWindowIntentToNavi(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v4

    .line 89
    invoke-virtual {p1, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    .end local v2    # "mTts":Ljava/lang/String;
    .end local v4    # "navigationIntent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return v5

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/Exception;
    const/4 v5, 0x0

    .line 94
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v6

    .line 95
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    .line 93
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v6

    .line 97
    const-string/jumbo v7, "PREF_SETTINGS_MY_NAVIGATION"

    .line 98
    const/4 v8, 0x0

    .line 96
    invoke-virtual {v6, v7, v8}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v3

    .line 99
    .local v3, "mapID":I
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/CMCCNavigationAdapter;->getInstallNavigationApp(I)Landroid/content/Intent;

    move-result-object v1

    .line 100
    .local v1, "goToMarket":Landroid/content/Intent;
    if-eqz v1, :cond_1

    .line 101
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 103
    :cond_1
    const-string/jumbo v6, "Error CMCC Navi"

    .line 104
    const-string/jumbo v7, "Error on starting CMCC Navigation Intent"

    .line 103
    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startNavigation(Landroid/content/Context;DDLjava/lang/String;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "lat"    # D
    .param p4, "lng"    # D
    .param p6, "tts"    # Ljava/lang/String;

    .prologue
    .line 115
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;-><init>(Landroid/content/Context;DDLjava/lang/String;)V

    .line 117
    .local v0, "multiData":Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, p6, v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/CMCCNavigationAdapter;->startMultiWindowWithIndicator(Landroid/content/Context;ZLjava/lang/String;Landroid/os/Parcelable;)V

    .line 122
    return-void
.end method
