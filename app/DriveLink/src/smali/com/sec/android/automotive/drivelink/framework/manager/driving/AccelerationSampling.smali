.class public Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;
.super Ljava/lang/Object;
.source "AccelerationSampling.java"


# instance fields
.field private mAccelerationSamplingList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;->mAccelerationSamplingList:Ljava/util/ArrayList;

    .line 12
    return-void
.end method


# virtual methods
.method public addData(F)V
    .locals 2
    .param p1, "acceleration"    # F

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;->mAccelerationSamplingList:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/Float;

    invoke-direct {v1, p1}, Ljava/lang/Float;-><init>(F)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 17
    return-void
.end method

.method public clearData()V
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;->mAccelerationSamplingList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 22
    return-void
.end method

.method public getAverageAcceleration()F
    .locals 3

    .prologue
    .line 31
    const/4 v0, 0x0

    .line 33
    .local v0, "averageAcceleration":F
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;->mAccelerationSamplingList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 39
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;->mAccelerationSamplingList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    int-to-float v2, v2

    div-float v2, v0, v2

    return v2

    .line 35
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;->mAccelerationSamplingList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    .line 36
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    add-float/2addr v0, v2

    .line 33
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;->mAccelerationSamplingList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
