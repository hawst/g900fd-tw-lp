.class public Lcom/sec/android/automotive/drivelink/util/CommonUtil;
.super Ljava/lang/Object;
.source "CommonUtil.java"


# static fields
.field private static final FIRST_CHARACTER:[C

.field private static TAG:Ljava/lang/String;

.field private static isBackupRingerMode:Z

.field private static isNfcDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    const-string/jumbo v0, "CommonUtil"

    sput-object v0, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    .line 472
    const/16 v0, 0x13

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->FIRST_CHARACTER:[C

    .line 778
    sput-boolean v1, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->isBackupRingerMode:Z

    .line 831
    sput-boolean v1, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->isNfcDisabled:Z

    return-void

    .line 472
    :array_0
    .array-data 2
        0x3131s
        0x3132s
        0x3134s
        0x3137s
        0x3138s
        0x3139s
        0x3141s
        0x3142s
        0x3143s
        0x3145s
        0x3146s
        0x3147s
        0x3148s
        0x3149s
        0x314as
        0x314bs
        0x314cs
        0x314ds
        0x314es
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "level"    # Ljava/lang/String;
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "sub"    # Ljava/lang/String;
    .param p3, "msg"    # Ljava/lang/String;

    .prologue
    .line 637
    const-string/jumbo v0, "e"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 638
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    :goto_0
    return-void

    .line 639
    :cond_0
    const-string/jumbo v0, "w"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 640
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 641
    :cond_1
    const-string/jumbo v0, "v"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 642
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/android/automotive/drivelink/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 643
    :cond_2
    const-string/jumbo v0, "d"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 644
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 645
    :cond_3
    const-string/jumbo v0, "i"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 646
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 648
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static changeNfcMode(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 833
    if-nez p0, :cond_1

    .line 834
    const/4 v2, 0x0

    .line 844
    :cond_0
    :goto_0
    return v2

    .line 837
    :cond_1
    const-string/jumbo v3, "nfc"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/nfc/NfcManager;

    .line 838
    .local v1, "mgr":Landroid/nfc/NfcManager;
    invoke-virtual {v1}, Landroid/nfc/NfcManager;->getDefaultAdapter()Landroid/nfc/NfcAdapter;

    move-result-object v0

    .line 839
    .local v0, "adapter":Landroid/nfc/NfcAdapter;
    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 840
    sget-object v3, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "NFC is Enabled. set disable NFC."

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 841
    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->disable()Z

    .line 842
    sput-boolean v2, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->isNfcDisabled:Z

    goto :goto_0
.end method

.method public static changeSoundMode(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x1

    .line 808
    if-nez p0, :cond_1

    .line 809
    sget-object v2, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "restoreRingerMode: context is null!!"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    const/4 v2, 0x0

    .line 827
    :cond_0
    :goto_0
    return v2

    .line 812
    :cond_1
    const-string/jumbo v3, "audio"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 813
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    .line 815
    .local v1, "currentSoundMode":I
    sget-boolean v3, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->isBackupRingerMode:Z

    if-nez v3, :cond_2

    .line 816
    sget-object v3, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "RingerMode: current on System ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " will backup."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v3

    .line 818
    const-string/jumbo v4, "PREF_USER_SOUND_MODE_PREVIOUS_STATE"

    .line 817
    invoke-virtual {v3, v4, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;I)V

    .line 820
    sput-boolean v2, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->isBackupRingerMode:Z

    .line 823
    :cond_2
    if-eq v1, v6, :cond_0

    .line 824
    invoke-virtual {v0, v6}, Landroid/media/AudioManager;->setRingerMode(I)V

    goto :goto_0
.end method

.method public static checkSimState()I
    .locals 3

    .prologue
    .line 584
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    const-string/jumbo v2, "phone"

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 583
    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 585
    .local v0, "telMgr":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    return v1
.end method

.method public static convertToMiles(D)D
    .locals 3
    .param p0, "m"    # D

    .prologue
    .line 743
    new-instance v0, Ljava/text/DecimalFormat;

    const-string/jumbo v1, "#.#"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 744
    .local v0, "d":Ljava/text/DecimalFormat;
    const-wide v1, 0x3fe3e245d68a2112L    # 0.621371192

    mul-double/2addr v1, p0

    invoke-virtual {v0, v1, v2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    return-wide v1
.end method

.method public static createBitmapMarkerFromIcon(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "markerIcon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 160
    const v0, 0x7f0202dc

    .line 161
    const/4 v1, 0x0

    const v2, 0x7f0202e1

    .line 160
    invoke-static {p0, p1, v0, v1, v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->createBitmapMarkerFromIcon(Landroid/content/res/Resources;Landroid/graphics/Bitmap;ILjava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static createBitmapMarkerFromIcon(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "markerIcon"    # Landroid/graphics/Bitmap;
    .param p2, "markerPin"    # I

    .prologue
    .line 166
    const/4 v0, 0x0

    .line 167
    const v1, 0x7f0201ac

    .line 166
    invoke-static {p0, p1, p2, v0, v1}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->createBitmapMarkerFromIcon(Landroid/content/res/Resources;Landroid/graphics/Bitmap;ILjava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static createBitmapMarkerFromIcon(Landroid/content/res/Resources;Landroid/graphics/Bitmap;ILjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "markerIcon"    # Landroid/graphics/Bitmap;
    .param p2, "markerPin"    # I
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 172
    .line 173
    const v0, 0x7f0201ac

    .line 172
    invoke-static {p0, p1, p2, p3, v0}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->createBitmapMarkerFromIcon(Landroid/content/res/Resources;Landroid/graphics/Bitmap;ILjava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static createBitmapMarkerFromIcon(Landroid/content/res/Resources;Landroid/graphics/Bitmap;ILjava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "markerIcon"    # Landroid/graphics/Bitmap;
    .param p2, "markerPin"    # I
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "mask"    # I

    .prologue
    .line 178
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    .line 180
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-static {p0, p4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 182
    .local v3, "pinMaskBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 183
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 182
    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 185
    .local v1, "newMarkerBitmap":Landroid/graphics/Bitmap;
    if-eqz p1, :cond_0

    .line 191
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    const/4 v6, 0x1

    .line 190
    invoke-static {p1, v4, v5, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 192
    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 193
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v0, p1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 195
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 196
    .local v2, "paint":Landroid/graphics/Paint;
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 197
    new-instance v4, Landroid/graphics/PorterDuffXfermode;

    .line 198
    sget-object v5, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v4, v5}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    .line 197
    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 200
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 201
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 202
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 220
    .end local v2    # "paint":Landroid/graphics/Paint;
    :goto_0
    invoke-static {p0, p2, v1}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageOverlay(Landroid/content/res/Resources;ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 223
    return-object v1

    .line 204
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 205
    invoke-static {p0, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 207
    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {p3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const/high16 v5, -0x1000000

    const/16 v6, 0x32

    .line 206
    invoke-static {v3, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/location/map/util/TextMarker;->addTextToBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 208
    goto :goto_0

    .line 209
    :cond_1
    const v4, 0x7f0202e1

    if-eq p4, v4, :cond_2

    .line 211
    const v4, 0x7f0201ab

    .line 212
    const v5, 0x7f0201ac

    .line 211
    invoke-static {p0, v4, v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 213
    goto :goto_0

    .line 215
    :cond_2
    const v4, 0x7f0202e0

    const v5, 0x7f0202e1

    .line 214
    invoke-static {p0, v4, v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method public static createTypefaceFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 5
    .param p0, "fontpath"    # Ljava/lang/String;

    .prologue
    .line 763
    const/4 v1, 0x0

    .line 765
    .local v1, "typeface":Landroid/graphics/Typeface;
    :try_start_0
    invoke-static {p0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 769
    if-nez v1, :cond_0

    .line 770
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 774
    :cond_0
    :goto_0
    return-object v1

    .line 766
    :catch_0
    move-exception v0

    .line 767
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v2, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "font ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ") is not enabled."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 769
    if-nez v1, :cond_0

    .line 770
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto :goto_0

    .line 768
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 769
    if-nez v1, :cond_1

    .line 770
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 772
    :cond_1
    throw v2
.end method

.method public static dipToPixels(Landroid/content/Context;F)F
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dipValue"    # F

    .prologue
    .line 577
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 578
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    const/4 v1, 0x1

    invoke-static {v1, p1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    return v1
.end method

.method public static drawBitmapBalloonToMarker(Landroid/app/Activity;Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "balloonMarker"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "mAddress"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 301
    .line 302
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    .line 301
    invoke-static/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->drawBitmapBalloonToMarker(Landroid/app/Activity;Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 303
    return-void
.end method

.method public static drawBitmapBalloonToMarker(Landroid/app/Activity;Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 13
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "balloonMarker"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "mAddress"    # Ljava/lang/String;
    .param p4, "timestamp"    # Ljava/lang/String;
    .param p5, "timestampEnd"    # Ljava/lang/String;
    .param p6, "forceBallonTop"    # Z

    .prologue
    .line 322
    if-eqz p2, :cond_0

    .line 324
    const v9, 0x7f09000a

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 323
    check-cast v6, Landroid/widget/TextView;

    .line 325
    .local v6, "tvBalloon":Landroid/widget/TextView;
    invoke-virtual {v6, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 327
    .end local v6    # "tvBalloon":Landroid/widget/TextView;
    :cond_0
    if-eqz p3, :cond_1

    .line 329
    const v9, 0x7f09000b

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 328
    check-cast v6, Landroid/widget/TextView;

    .line 330
    .restart local v6    # "tvBalloon":Landroid/widget/TextView;
    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 333
    .end local v6    # "tvBalloon":Landroid/widget/TextView;
    :cond_1
    if-eqz p4, :cond_2

    .line 334
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_2

    .line 336
    const v9, 0x7f09000c

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 335
    check-cast v6, Landroid/widget/TextView;

    .line 337
    .restart local v6    # "tvBalloon":Landroid/widget/TextView;
    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 338
    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 342
    .end local v6    # "tvBalloon":Landroid/widget/TextView;
    :cond_2
    if-eqz p5, :cond_3

    .line 343
    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_3

    .line 345
    const v9, 0x7f09000d

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 344
    check-cast v6, Landroid/widget/TextView;

    .line 346
    .restart local v6    # "tvBalloon":Landroid/widget/TextView;
    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 347
    move-object/from16 v0, p5

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 352
    .end local v6    # "tvBalloon":Landroid/widget/TextView;
    :cond_3
    const v9, 0x7f090009

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 351
    check-cast v3, Landroid/widget/LinearLayout;

    .line 353
    .local v3, "llBalloon":Landroid/widget/LinearLayout;
    const/4 v9, 0x1

    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->setDrawingCacheEnabled(Z)V

    .line 355
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iget v9, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/high16 v10, 0x40000000    # 2.0f

    .line 354
    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    .line 356
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    .line 354
    invoke-virtual {v3, v9, v10}, Landroid/widget/LinearLayout;->measure(II)V

    .line 357
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v11

    .line 358
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v12

    .line 357
    invoke-virtual {v3, v9, v10, v11, v12}, Landroid/widget/LinearLayout;->layout(IIII)V

    .line 361
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0202dc

    .line 360
    invoke-static {v9, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 363
    .local v5, "pinBgBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->buildDrawingCache()V

    .line 366
    const/4 v9, 0x1

    :try_start_0
    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->getDrawingCache(Z)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 367
    .local v4, "llBalloonBitmap":Landroid/graphics/Bitmap;
    invoke-static {p0, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->wrappMarkerWithPaddingImage(Landroid/content/Context;Landroid/widget/LinearLayout;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 379
    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v1, v9, Landroid/content/res/Configuration;->orientation:I

    .line 380
    .local v1, "currentOrientation":I
    if-nez p6, :cond_4

    .line 381
    const/4 v9, 0x2

    if-ne v1, v9, :cond_4

    .line 382
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    const/high16 v10, -0x40000000    # -2.0f

    div-float v7, v9, v10

    .line 383
    .local v7, "xBalloonBitmap":F
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v7, v9

    .line 385
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    const/high16 v10, 0x41f00000    # 30.0f

    sub-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float v8, v9, v10

    .line 386
    .local v8, "yBalloonBitmap":F
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    div-float v9, v8, v9

    const/high16 v10, 0x3f000000    # 0.5f

    add-float v8, v9, v10

    .line 388
    invoke-virtual {p1, v7, v8}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->setAnchor(FF)V

    .line 395
    :goto_1
    invoke-virtual {p1, v7, v8}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->setAnchor(FF)V

    .line 396
    invoke-virtual {p1, v4}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->setIcon(Landroid/graphics/Bitmap;)V

    .line 397
    return-void

    .line 370
    .end local v1    # "currentOrientation":I
    .end local v4    # "llBalloonBitmap":Landroid/graphics/Bitmap;
    .end local v7    # "xBalloonBitmap":F
    .end local v8    # "yBalloonBitmap":F
    :catch_0
    move-exception v2

    .line 372
    .local v2, "e":Ljava/lang/Exception;
    const/4 v9, 0x1

    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->getDrawingCache(Z)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 371
    invoke-static {v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    .restart local v4    # "llBalloonBitmap":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 390
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v1    # "currentOrientation":I
    :cond_4
    const/high16 v7, 0x3f000000    # 0.5f

    .line 391
    .restart local v7    # "xBalloonBitmap":F
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    const/high16 v10, 0x41f00000    # 30.0f

    sub-float v8, v9, v10

    .line 392
    .restart local v8    # "yBalloonBitmap":F
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    div-float v9, v8, v9

    const/high16 v10, 0x3f800000    # 1.0f

    add-float v8, v9, v10

    goto :goto_1
.end method

.method public static drawBitmapBalloonToMarker(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;Ljava/lang/String;)V
    .locals 7
    .param p0, "activity"    # Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
    .param p1, "balloonMarker"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    .param p2, "mAddress"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 295
    .line 296
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, v2

    move-object v5, v2

    .line 295
    invoke-static/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->drawBitmapBalloonToMarker(Landroid/app/Activity;Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 297
    return-void
.end method

.method public static drawBitmapBalloonToSimpleMarker(Landroid/app/Activity;Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;Landroid/view/View;)V
    .locals 9
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "balloonMarker"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    .param p2, "tvBalloon"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 230
    .line 231
    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 232
    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 230
    invoke-virtual {p2, v5, v6}, Landroid/view/View;->measure(II)V

    .line 233
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 234
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 233
    invoke-virtual {p2, v7, v7, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 236
    invoke-virtual {p2, v8}, Landroid/view/View;->buildDrawingCache(Z)V

    .line 239
    const/4 v5, 0x1

    :try_start_0
    invoke-virtual {p2, v5}, Landroid/view/View;->getDrawingCache(Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 247
    .local v1, "llBalloonBitmap":Landroid/graphics/Bitmap;
    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0202dc

    .line 246
    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 250
    .local v2, "pinBgBitmap":Landroid/graphics/Bitmap;
    const/high16 v3, 0x3f000000    # 0.5f

    .line 251
    .local v3, "xBalloonBitmap":F
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x41f00000    # 30.0f

    sub-float v4, v5, v6

    .line 252
    .local v4, "yBalloonBitmap":F
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float v5, v4, v5

    const/high16 v6, 0x3f800000    # 1.0f

    add-float v4, v5, v6

    .line 254
    invoke-virtual {p1, v3, v4}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->setAnchor(FF)V

    .line 255
    invoke-virtual {p1, v1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->setIcon(Landroid/graphics/Bitmap;)V

    .line 256
    return-void

    .line 240
    .end local v1    # "llBalloonBitmap":Landroid/graphics/Bitmap;
    .end local v2    # "pinBgBitmap":Landroid/graphics/Bitmap;
    .end local v3    # "xBalloonBitmap":F
    .end local v4    # "yBalloonBitmap":F
    :catch_0
    move-exception v0

    .line 242
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p2, v8}, Landroid/view/View;->getDrawingCache(Z)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 241
    invoke-static {v5}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .restart local v1    # "llBalloonBitmap":Landroid/graphics/Bitmap;
    goto :goto_0
.end method

.method public static drawBitmapBalloonToSimpleMarker(Landroid/app/Activity;Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;Ljava/lang/String;)V
    .locals 10
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "balloonMarker"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 261
    .line 262
    const v6, 0x7f09000a

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 261
    check-cast v3, Landroid/widget/TextView;

    .line 263
    .local v3, "tvBalloon":Landroid/widget/TextView;
    invoke-virtual {v3, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 267
    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 265
    invoke-virtual {v3, v6, v7}, Landroid/widget/TextView;->measure(II)V

    .line 268
    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v6

    .line 269
    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    .line 268
    invoke-virtual {v3, v8, v8, v6, v7}, Landroid/widget/TextView;->layout(IIII)V

    .line 271
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->buildDrawingCache(Z)V

    .line 274
    const/4 v6, 0x1

    :try_start_0
    invoke-virtual {v3, v6}, Landroid/widget/TextView;->getDrawingCache(Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 282
    .local v1, "llBalloonBitmap":Landroid/graphics/Bitmap;
    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0201a7

    .line 281
    invoke-static {v6, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 285
    .local v2, "pinBgBitmap":Landroid/graphics/Bitmap;
    const/high16 v4, 0x3f000000    # 0.5f

    .line 286
    .local v4, "xBalloonBitmap":F
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x41200000    # 10.0f

    sub-float v5, v6, v7

    .line 287
    .local v5, "yBalloonBitmap":F
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    div-float v6, v5, v6

    const/high16 v7, 0x3f800000    # 1.0f

    add-float v5, v6, v7

    .line 289
    invoke-virtual {p1, v4, v5}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->setAnchor(FF)V

    .line 290
    invoke-virtual {p1, v1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->setIcon(Landroid/graphics/Bitmap;)V

    .line 291
    return-void

    .line 275
    .end local v1    # "llBalloonBitmap":Landroid/graphics/Bitmap;
    .end local v2    # "pinBgBitmap":Landroid/graphics/Bitmap;
    .end local v4    # "xBalloonBitmap":F
    .end local v5    # "yBalloonBitmap":F
    :catch_0
    move-exception v0

    .line 277
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->getDrawingCache(Z)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 276
    invoke-static {v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .restart local v1    # "llBalloonBitmap":Landroid/graphics/Bitmap;
    goto :goto_0
.end method

.method public static getCurrentTTSBarImage(Landroid/content/res/Resources;F)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "left"    # F

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 81
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    .line 84
    .local v0, "c":Landroid/graphics/Canvas;
    const v6, 0x7f020389

    .line 83
    invoke-static {p0, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 86
    .local v3, "orig":Landroid/graphics/Bitmap;
    const v6, 0x7f02038b

    .line 85
    invoke-static {p0, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 87
    .local v1, "mask":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 88
    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 87
    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 90
    .local v2, "masked":Landroid/graphics/Bitmap;
    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 91
    invoke-virtual {v0, v3, p1, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 93
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 94
    .local v4, "p":Landroid/graphics/Paint;
    invoke-virtual {v4, v11}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 95
    new-instance v6, Landroid/graphics/PorterDuffXfermode;

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v6, v7}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 97
    invoke-virtual {v0, v1, v9, v9, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 98
    invoke-virtual {v4, v10}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 99
    invoke-virtual {v0, v2, v9, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 101
    float-to-int v6, p1

    .line 102
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    .line 101
    invoke-static {v2, v6, v11, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 104
    .local v5, "result":Landroid/graphics/Bitmap;
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 105
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 106
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 108
    return-object v5
.end method

.method public static getDistance(DDDD)F
    .locals 9
    .param p0, "startLati"    # D
    .param p2, "startLongi"    # D
    .param p4, "goalLati"    # D
    .param p6, "goalLongi"    # D

    .prologue
    .line 619
    const/4 v0, 0x5

    new-array v8, v0, [F

    .local v8, "resultArray":[F
    move-wide v0, p0

    move-wide v2, p2

    move-wide v4, p4

    move-wide v6, p6

    .line 620
    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    .line 622
    const/4 v0, 0x0

    aget v0, v8, v0

    return v0
.end method

.method public static getDistanceInKm(DDDD)Ljava/lang/String;
    .locals 4
    .param p0, "startLati"    # D
    .param p2, "startLongi"    # D
    .param p4, "goalLati"    # D
    .param p6, "goalLongi"    # D

    .prologue
    .line 629
    invoke-static/range {p0 .. p7}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->getDistance(DDDD)F

    move-result v1

    .line 630
    .local v1, "distance":F
    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    .line 632
    new-instance v0, Ljava/text/DecimalFormat;

    const-string/jumbo v2, "0.0"

    invoke-direct {v0, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 633
    .local v0, "df":Ljava/text/DecimalFormat;
    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getHighlightedText(Ljava/lang/String;Ljava/lang/String;I)Landroid/text/SpannableString;
    .locals 9
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "search"    # Ljava/lang/String;
    .param p2, "typeFace"    # I

    .prologue
    .line 552
    const/4 v3, 0x0

    .line 553
    .local v3, "res":Landroid/text/SpannableString;
    const/4 v2, 0x0

    .line 554
    .local v2, "p":Ljava/util/regex/Pattern;
    const/4 v1, 0x0

    .line 557
    .local v1, "m":Ljava/util/regex/Matcher;
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "("

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    invoke-static {v5, v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 558
    invoke-virtual {v2, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 559
    new-instance v4, Landroid/text/SpannableString;

    invoke-direct {v4, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_0 .. :try_end_0} :catch_1

    .line 561
    .end local v3    # "res":Landroid/text/SpannableString;
    .local v4, "res":Landroid/text/SpannableString;
    :goto_0
    :try_start_1
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z
    :try_end_1
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v5

    if-nez v5, :cond_1

    move-object v3, v4

    .line 573
    .end local v4    # "res":Landroid/text/SpannableString;
    .restart local v3    # "res":Landroid/text/SpannableString;
    :goto_1
    if-nez v3, :cond_0

    new-instance v3, Landroid/text/SpannableString;

    .end local v3    # "res":Landroid/text/SpannableString;
    invoke-direct {v3, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    :cond_0
    return-object v3

    .line 564
    .restart local v4    # "res":Landroid/text/SpannableString;
    :cond_1
    :try_start_2
    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, p2}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v6

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v7

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V
    :try_end_2
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 567
    :catch_0
    move-exception v0

    move-object v3, v4

    .line 568
    .end local v4    # "res":Landroid/text/SpannableString;
    .local v0, "e":Ljava/util/regex/PatternSyntaxException;
    .restart local v3    # "res":Landroid/text/SpannableString;
    :goto_2
    sget-object v5, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/regex/PatternSyntaxException;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    sget-object v5, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/regex/PatternSyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    sget-object v5, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/regex/PatternSyntaxException;->getPattern()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 567
    .end local v0    # "e":Ljava/util/regex/PatternSyntaxException;
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static getHighlightedText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/text/SpannableString;
    .locals 9
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "search"    # Ljava/lang/String;
    .param p2, "color"    # Ljava/lang/String;
    .param p3, "fontFamily"    # I

    .prologue
    .line 447
    const/4 v3, 0x0

    .line 448
    .local v3, "res":Landroid/text/SpannableString;
    const/4 v2, 0x0

    .line 449
    .local v2, "p":Ljava/util/regex/Pattern;
    const/4 v1, 0x0

    .line 452
    .local v1, "m":Ljava/util/regex/Matcher;
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "("

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    invoke-static {v5, v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 453
    invoke-virtual {v2, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 454
    new-instance v4, Landroid/text/SpannableString;

    invoke-direct {v4, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_0 .. :try_end_0} :catch_1

    .line 456
    .end local v3    # "res":Landroid/text/SpannableString;
    .local v4, "res":Landroid/text/SpannableString;
    :goto_0
    :try_start_1
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z
    :try_end_1
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v5

    if-nez v5, :cond_1

    move-object v3, v4

    .line 469
    .end local v4    # "res":Landroid/text/SpannableString;
    .restart local v3    # "res":Landroid/text/SpannableString;
    :goto_1
    if-nez v3, :cond_0

    new-instance v3, Landroid/text/SpannableString;

    .end local v3    # "res":Landroid/text/SpannableString;
    invoke-direct {v3, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    :cond_0
    return-object v3

    .line 459
    .restart local v4    # "res":Landroid/text/SpannableString;
    :cond_1
    :try_start_2
    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, p3}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v6

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v7

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 460
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    invoke-static {p2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-direct {v5, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 461
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v6

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v7

    const/4 v8, 0x0

    .line 460
    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V
    :try_end_2
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 463
    :catch_0
    move-exception v0

    move-object v3, v4

    .line 464
    .end local v4    # "res":Landroid/text/SpannableString;
    .local v0, "e":Ljava/util/regex/PatternSyntaxException;
    .restart local v3    # "res":Landroid/text/SpannableString;
    :goto_2
    sget-object v5, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/regex/PatternSyntaxException;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    sget-object v5, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/regex/PatternSyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    sget-object v5, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/regex/PatternSyntaxException;->getPattern()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 463
    .end local v0    # "e":Ljava/util/regex/PatternSyntaxException;
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static getInitialText(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "search"    # Ljava/lang/String;

    .prologue
    const v11, 0xd7af

    const v10, 0xac00

    .line 477
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 478
    :cond_0
    const/4 p1, 0x0

    .line 538
    .end local p1    # "search":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object p1

    .line 480
    .restart local p1    # "search":Ljava/lang/String;
    :cond_2
    sget-object v7, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "[getInitialText] text : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    sget-object v7, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "[getInitialText] search : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    invoke-virtual {p0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 485
    sget-object v7, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "[getInitialText] contains!!"

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 489
    :cond_3
    const-string/jumbo v4, ""

    .line 490
    .local v4, "initText":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    if-lt v2, v7, :cond_4

    .line 501
    const-string/jumbo v3, ""

    .line 502
    .local v3, "initSearch":Ljava/lang/String;
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-lt v2, v7, :cond_6

    .line 513
    sget-object v7, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "[getInitialText] initText : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    sget-object v7, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "[getInitialText] initSearch : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    invoke-virtual {v4, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 518
    invoke-virtual {v4, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 519
    .local v6, "start":I
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    add-int v1, v6, v7

    .line 521
    .local v1, "end":I
    const-string/jumbo v5, ""

    .line 523
    .local v5, "returnText":Ljava/lang/String;
    sget-object v7, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "startIndex["

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "], lastIndex["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    sget-object v7, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "initText substring : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    sget-object v7, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "text substring : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    sget-object v7, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "text substring : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v6, v1}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    if-ne v6, v1, :cond_8

    .line 530
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_3
    move-object p1, v5

    .line 535
    goto/16 :goto_0

    .line 492
    .end local v1    # "end":I
    .end local v3    # "initSearch":Ljava/lang/String;
    .end local v5    # "returnText":Ljava/lang/String;
    .end local v6    # "start":I
    :cond_4
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-lt v7, v10, :cond_5

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-gt v7, v11, :cond_5

    .line 493
    sget-object v7, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->FIRST_CHARACTER:[C

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    sub-int/2addr v8, v10

    div-int/lit16 v8, v8, 0x24c

    aget-char v0, v7, v8

    .line 498
    .local v0, "c":C
    :goto_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 490
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 495
    .end local v0    # "c":C
    :cond_5
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .restart local v0    # "c":C
    goto :goto_4

    .line 504
    .end local v0    # "c":C
    .restart local v3    # "initSearch":Ljava/lang/String;
    :cond_6
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-lt v7, v10, :cond_7

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-gt v7, v11, :cond_7

    .line 505
    sget-object v7, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->FIRST_CHARACTER:[C

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    sub-int/2addr v8, v10

    div-int/lit16 v8, v8, 0x24c

    aget-char v0, v7, v8

    .line 510
    .restart local v0    # "c":C
    :goto_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 502
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    .line 507
    .end local v0    # "c":C
    :cond_7
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .restart local v0    # "c":C
    goto :goto_5

    .line 532
    .end local v0    # "c":C
    .restart local v1    # "end":I
    .restart local v5    # "returnText":Ljava/lang/String;
    .restart local v6    # "start":I
    :cond_8
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v6, v1}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_3
.end method

.method public static imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "origId"    # I
    .param p2, "maskId"    # I

    .prologue
    .line 50
    invoke-static {p0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "origBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "maskId"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x0

    .line 56
    if-nez p1, :cond_0

    move-object v4, v5

    .line 77
    :goto_0
    return-object v4

    .line 59
    :cond_0
    invoke-static {p0, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 61
    .local v1, "maskBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    const/4 v8, 0x1

    .line 60
    invoke-static {p1, v6, v7, v8}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 63
    .local v3, "resizedBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    .line 64
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 65
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 64
    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 66
    .local v4, "resultBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v0, v4}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 67
    invoke-virtual {v0, v3, v9, v9, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 69
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 70
    .local v2, "paint":Landroid/graphics/Paint;
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 71
    new-instance v6, Landroid/graphics/PorterDuffXfermode;

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v6, v7}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 73
    invoke-virtual {v0, v1, v9, v9, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 74
    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 75
    invoke-virtual {v0, v4, v9, v9, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public static imageOverlay(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "origId"    # I
    .param p2, "overlayId"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 125
    invoke-static {p0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 126
    .local v1, "origBitmap":Landroid/graphics/Bitmap;
    invoke-static {p0, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 127
    .local v2, "overlayBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 128
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v6

    .line 127
    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 130
    .local v3, "resultBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 131
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v0, v1, v7, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 132
    invoke-virtual {v0, v2, v7, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 134
    return-object v3
.end method

.method public static imageOverlay(Landroid/content/res/Resources;ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "origId"    # I
    .param p2, "overlayId"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 139
    invoke-static {p0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 140
    .local v1, "origBitmap":Landroid/graphics/Bitmap;
    move-object v2, p2

    .line 141
    .local v2, "overlayBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 142
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v6

    .line 141
    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 144
    .local v3, "resultBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 145
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v0, v1, v7, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 146
    invoke-virtual {v0, v2, v7, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 148
    return-object v3
.end method

.method public static imageOverlay(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "origBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "overlayId"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 113
    invoke-static {p0, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 114
    .local v1, "overlayBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 115
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v5

    .line 114
    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 117
    .local v2, "resultBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 118
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v0, p1, v6, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 119
    invoke-virtual {v0, v1, v6, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 121
    return-object v2
.end method

.method public static isBrPhone()Z
    .locals 2

    .prologue
    .line 701
    const-string/jumbo v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 703
    .local v0, "salesCode":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 704
    const-string/jumbo v1, "ZTM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 705
    const-string/jumbo v1, "ZTR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 706
    const-string/jumbo v1, "ZTA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 707
    const-string/jumbo v1, "ZTO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 708
    const-string/jumbo v1, "ZVV"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 709
    :cond_0
    const/4 v1, 0x1

    .line 711
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isCnPhone()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 732
    const-string/jumbo v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 733
    .local v1, "salesCode":Ljava/lang/String;
    const-string/jumbo v3, "ro.csc.countryiso_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 735
    .local v0, "country":Ljava/lang/String;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 736
    const-string/jumbo v3, "CN"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v3, "CHN"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v3, "CHM"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v3, "CHU"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 737
    const-string/jumbo v3, "CTC"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v3, "CHC"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 739
    :cond_0
    :goto_0
    return v2

    .line 736
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static isKoreanPhone()Z
    .locals 3

    .prologue
    .line 748
    const-string/jumbo v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 749
    .local v1, "salesCode":Ljava/lang/String;
    const-string/jumbo v2, "ro.csc.countryiso_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 751
    .local v0, "country":Ljava/lang/String;
    const-string/jumbo v2, "KR"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 752
    const-string/jumbo v2, "SKT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "SKC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 753
    const-string/jumbo v2, "SKO"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "KTT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 754
    const-string/jumbo v2, "KTC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "KTO"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 755
    const-string/jumbo v2, "LGT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "LUC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 756
    const-string/jumbo v2, "LUO"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 757
    :cond_0
    const/4 v2, 0x1

    .line 759
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isUScsc()Z
    .locals 2

    .prologue
    .line 684
    const-string/jumbo v1, "ro.csc.countryiso_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 686
    .local v0, "country":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string/jumbo v1, "US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->isUsPhone()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 687
    :cond_0
    const/4 v1, 0x1

    .line 689
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isUsPhone()Z
    .locals 2

    .prologue
    .line 719
    const-string/jumbo v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 721
    .local v0, "salesCode":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 722
    const-string/jumbo v1, "SPR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 723
    const-string/jumbo v1, "VMU"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 724
    const-string/jumbo v1, "BST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 725
    const-string/jumbo v1, "XAS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 726
    :cond_0
    const/4 v1, 0x1

    .line 728
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isValidSIMCARDSOperator()Z
    .locals 3

    .prologue
    .line 676
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    const-string/jumbo v2, "phone"

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 675
    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 677
    .local v0, "telMgr":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 678
    const/4 v1, 0x0

    .line 680
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static loadStringByName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resName"    # Ljava/lang/String;

    .prologue
    .line 662
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 665
    .local v0, "res":Landroid/content/res/Resources;
    const-string/jumbo v2, "string"

    .line 666
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 665
    invoke-virtual {v0, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 667
    .local v1, "resId":I
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static restoreNfcMode(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 848
    if-nez p0, :cond_0

    .line 859
    :goto_0
    return v2

    .line 852
    :cond_0
    const-string/jumbo v3, "nfc"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/nfc/NfcManager;

    .line 853
    .local v1, "mgr":Landroid/nfc/NfcManager;
    invoke-virtual {v1}, Landroid/nfc/NfcManager;->getDefaultAdapter()Landroid/nfc/NfcAdapter;

    move-result-object v0

    .line 854
    .local v0, "adapter":Landroid/nfc/NfcAdapter;
    sget-boolean v3, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->isNfcDisabled:Z

    if-eqz v3, :cond_1

    .line 855
    sget-object v3, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "*restore* NFC is disabled. set enable NFC."

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 856
    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->enable()Z

    .line 857
    sput-boolean v2, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->isNfcDisabled:Z

    .line 859
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static restoreRingerMode(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 780
    if-nez p0, :cond_0

    .line 781
    sget-object v4, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "restoreRingerMode: context is null!!"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    :goto_0
    return v3

    .line 786
    :cond_0
    const-string/jumbo v4, "audio"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 785
    check-cast v0, Landroid/media/AudioManager;

    .line 787
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    .line 790
    .local v1, "currentSoundMode":I
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v4

    .line 792
    const-string/jumbo v5, "PREF_USER_SOUND_MODE_PREVIOUS_STATE"

    .line 791
    invoke-virtual {v4, v5, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v2

    .line 795
    .local v2, "previousSoundMode":I
    sget-object v4, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "restoreRingerMode : RingerMode: current in CarMode="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ", previous on System="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 796
    if-eq v1, v2, :cond_1

    .line 797
    sget-object v4, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "restoreRingerMode : The Sound Mode is changed so restore the value"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 801
    :cond_1
    sput-boolean v3, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->isBackupRingerMode:Z

    .line 803
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public static showToast(Landroid/app/Activity;I)V
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "simState"    # I

    .prologue
    .line 589
    packed-switch p1, :pswitch_data_0

    .line 614
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 591
    :pswitch_1
    const v1, 0x7f0a02fe

    .line 592
    const/4 v2, 0x0

    .line 591
    invoke-static {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;II)Landroid/widget/Toast;

    move-result-object v0

    .line 594
    .local v0, "toast":Landroid/widget/Toast;
    if-eqz v0, :cond_0

    .line 595
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 589
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static wrappMarkerWithPaddingImage(Landroid/content/Context;Landroid/widget/LinearLayout;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "srcViewLayout"    # Landroid/widget/LinearLayout;
    .param p2, "srcMarkerImage"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 412
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 411
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v4

    .line 413
    const-string/jumbo v5, "PREF_SETTINGS_MY_NAVIGATION"

    .line 412
    invoke-virtual {v4, v5, v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v1

    .line 415
    .local v1, "mapID":I
    sget v4, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_BAIDU_NAVI:I

    if-eq v1, v4, :cond_0

    sget v4, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_CMCC_NAVI:I

    if-eq v1, v4, :cond_0

    .line 442
    .end local p2    # "srcMarkerImage":Landroid/graphics/Bitmap;
    :goto_0
    return-object p2

    .line 419
    .restart local p2    # "srcMarkerImage":Landroid/graphics/Bitmap;
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0001

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    int-to-float v4, v4

    .line 418
    invoke-static {p0, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->dipToPixels(Landroid/content/Context;F)F

    move-result v4

    float-to-int v2, v4

    .line 420
    .local v2, "paddingBallon":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v0, v4, Landroid/content/res/Configuration;->orientation:I

    .line 421
    .local v0, "currentOrientation":I
    new-instance v3, Landroid/widget/ImageView;

    invoke-direct {v3, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 422
    .local v3, "tmpImage":Landroid/widget/ImageView;
    invoke-virtual {v3, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 423
    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setDrawingCacheEnabled(Z)V

    .line 424
    const v4, 0x106000d

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 425
    if-ne v0, v7, :cond_1

    .line 426
    invoke-virtual {v3, v6, v6, v6, v2}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 431
    :goto_1
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/high16 v5, 0x40000000    # 2.0f

    .line 430
    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 432
    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 430
    invoke-virtual {v3, v4, v5}, Landroid/widget/ImageView;->measure(II)V

    .line 433
    if-ne v0, v7, :cond_2

    .line 434
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v4

    .line 435
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v2

    .line 434
    invoke-virtual {v3, v6, v6, v4, v5}, Landroid/widget/ImageView;->layout(IIII)V

    .line 440
    :goto_2
    invoke-virtual {v3}, Landroid/widget/ImageView;->buildDrawingCache()V

    .line 442
    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->getDrawingCache(Z)Landroid/graphics/Bitmap;

    move-result-object p2

    goto :goto_0

    .line 428
    :cond_1
    invoke-virtual {v3, v2, v6, v6, v6}, Landroid/widget/ImageView;->setPadding(IIII)V

    goto :goto_1

    .line 437
    :cond_2
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v4

    .line 438
    add-int/2addr v4, v2

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v5

    .line 437
    invoke-virtual {v3, v6, v6, v4, v5}, Landroid/widget/ImageView;->layout(IIII)V

    goto :goto_2
.end method
