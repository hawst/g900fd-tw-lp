.class final Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$PushMessageHandler;
.super Landroid/os/Handler;
.source "PushMessageService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PushMessageHandler"
.end annotation


# static fields
.field private static final PARAM_GROUP_ID:Ljava/lang/String; = "groupId"

.field private static final PARAM_GROUP_TYPE:Ljava/lang/String; = "groupType"

.field private static final PARAM_OWNER:Ljava/lang/String; = "owner"

.field private static final PARAM_USER:Ljava/lang/String; = "user"


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;


# direct methods
.method private constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$PushMessageHandler;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$PushMessageHandler;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$PushMessageHandler;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 86
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Landroid/content/Intent;

    .line 87
    .local v5, "intent":Landroid/content/Intent;
    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 89
    .local v1, "extras":Landroid/os/Bundle;
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$PushMessageHandler;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;

    .line 90
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    .line 89
    invoke-static {v9}, Lcom/google/android/gms/gcm/GoogleCloudMessaging;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/gcm/GoogleCloudMessaging;

    move-result-object v2

    .line 92
    .local v2, "gcm":Lcom/google/android/gms/gcm/GoogleCloudMessaging;
    invoke-virtual {v2, v5}, Lcom/google/android/gms/gcm/GoogleCloudMessaging;->getMessageType(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v8

    .line 93
    .local v8, "messageType":Ljava/lang/String;
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->access$0()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "handleMessage "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    invoke-virtual {v1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 96
    const-string/jumbo v9, "send_error"

    .line 97
    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 98
    const-string/jumbo v9, "deleted_messages"

    .line 99
    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 100
    const-string/jumbo v9, "gcm"

    .line 101
    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 103
    const/4 v6, 0x0

    .line 105
    .local v6, "jObj":Lorg/json/JSONObject;
    :try_start_0
    new-instance v7, Lorg/json/JSONObject;

    const-string/jumbo v9, "gcmPush"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    .end local v6    # "jObj":Lorg/json/JSONObject;
    .local v7, "jObj":Lorg/json/JSONObject;
    :try_start_1
    const-string/jumbo v9, "groupId"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 108
    .local v3, "groupId":Ljava/lang/String;
    const-string/jumbo v9, "groupType"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 116
    .local v4, "groupType":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$PushMessageHandler;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->listener:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/IPushMessageServiceListener;
    invoke-static {v9}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;)Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/IPushMessageServiceListener;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 117
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$PushMessageHandler;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->listener:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/IPushMessageServiceListener;
    invoke-static {v9}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;)Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/IPushMessageServiceListener;

    move-result-object v9

    .line 118
    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v10

    .line 117
    invoke-interface {v9, v3, v4, v10}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/IPushMessageServiceListener;->onMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 127
    .end local v3    # "groupId":Ljava/lang/String;
    .end local v4    # "groupType":Ljava/lang/String;
    .end local v7    # "jObj":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageBroadcastReceiver;->completeWakefulIntent(Landroid/content/Intent;)Z

    .line 128
    return-void

    .line 120
    .restart local v6    # "jObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 122
    .local v0, "e":Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 120
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v6    # "jObj":Lorg/json/JSONObject;
    .restart local v7    # "jObj":Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    move-object v6, v7

    .end local v7    # "jObj":Lorg/json/JSONObject;
    .restart local v6    # "jObj":Lorg/json/JSONObject;
    goto :goto_1
.end method
