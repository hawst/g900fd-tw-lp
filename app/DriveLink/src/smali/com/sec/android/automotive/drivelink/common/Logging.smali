.class public Lcom/sec/android/automotive/drivelink/common/Logging;
.super Ljava/lang/Object;
.source "Logging.java"


# static fields
.field public static final APP_ID:Ljava/lang/String; = "com.sec.android.automotive.drivelink"

.field public static final CM_DRAWER_CONSUMABLES:Ljava/lang/String; = "CM29"

.field public static final CM_DRAWER_DIAGNOSTIC:Ljava/lang/String; = "CM28"

.field public static final CM_DRAWER_DRIVING_BEHAVIOR:Ljava/lang/String; = "CM27"

.field public static final CM_HOME_TOUCH_MESSAGE:Ljava/lang/String; = "CM08"

.field public static final CM_HOME_TOUCH_NAVI:Ljava/lang/String; = "CM12"

.field public static final CM_HOME_TOUCH_PHONE:Ljava/lang/String; = "CM05"

.field public static final CM_HOME_VOICE_CALL:Ljava/lang/String; = "CM01"

.field public static final CM_HOME_VOICE_MESSAGE:Ljava/lang/String; = "CM02"

.field public static final CM_HOME_VOICE_MUSIC:Ljava/lang/String; = "CM04"

.field public static final CM_HOME_VOICE_NAVI:Ljava/lang/String; = "CM03"

.field public static final CM_MESSAGE_READ_MESSAGE_CALL:Ljava/lang/String; = "CM11"

.field public static final CM_MESSAGE_READ_MESSAGE_REPLY:Ljava/lang/String; = "CM10"

.field public static final CM_MESSAGE_SIP_TYPE_MESSAGE:Ljava/lang/String; = "CM09"

.field public static final CM_MYCAR_BT_LAUNCH:Ljava/lang/String; = "CM24"

.field public static final CM_NAVI_TOUCH_GAS_SEARCH:Ljava/lang/String; = "CM14"

.field public static final CM_NAVI_TOUCH_GAS_SEARCH_DESTINATION:Ljava/lang/String; = "CM15"

.field public static final CM_NAVI_TOUCH_MYPLACE:Ljava/lang/String; = "CM18"

.field public static final CM_NAVI_TOUCH_MYPLACE_DESTINATION:Ljava/lang/String; = "CM19"

.field public static final CM_NAVI_TOUCH_NAVI:Ljava/lang/String; = "CM13"

.field public static final CM_NAVI_TOUCH_PARKING_SEARCH:Ljava/lang/String; = "CM16"

.field public static final CM_NAVI_TOUCH_PARKING_SEARCH_DESTINATION:Ljava/lang/String; = "CM17"

.field public static final CM_NAVI_TOUCH_SCHEDULE:Ljava/lang/String; = "CM20"

.field public static final CM_NAVI_TOUCH_SCHEDULE_DESTINATION:Ljava/lang/String; = "CM21"

.field public static final CM_OBD_DRIVING:Ljava/lang/String; = "CM25"

.field public static final CM_PHONE_BARGEIN_CANCEL:Ljava/lang/String; = "CM23"

.field public static final CM_PHONE_SIP_SEARCH:Ljava/lang/String; = "CM06"

.field public static final CM_PHONE_SUGGESTION_CALL:Ljava/lang/String; = "CM07"

.field public static final CM_SETTINGS_SMART_ALERT_ON:Ljava/lang/String; = "CM26"

.field public static final CM_VOICE_WAKEUP:Ljava/lang/String; = "CM22"

.field private static final TAG:Ljava/lang/String; = "Logging"

.field private static mContext:Landroid/content/Context;

.field private static volatile mInstance:Lcom/sec/android/automotive/drivelink/common/Logging;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/Logging;->mInstance:Lcom/sec/android/automotive/drivelink/common/Logging;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/Logging;->mInstance:Lcom/sec/android/automotive/drivelink/common/Logging;

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/Logging;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/Logging;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/Logging;->mInstance:Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 55
    :cond_0
    if-eqz p0, :cond_1

    .line 56
    sput-object p0, Lcom/sec/android/automotive/drivelink/common/Logging;->mContext:Landroid/content/Context;

    .line 58
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/Logging;->mInstance:Lcom/sec/android/automotive/drivelink/common/Logging;

    return-object v0
.end method

.method private static getVersionOfContextProviders()I
    .locals 6

    .prologue
    .line 86
    const/4 v2, -0x1

    .line 88
    .local v2, "version":I
    :try_start_0
    sget-object v3, Lcom/sec/android/automotive/drivelink/common/Logging;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 89
    const-string/jumbo v4, "com.samsung.android.providers.context"

    .line 90
    const/16 v5, 0x80

    .line 89
    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 91
    .local v1, "pInfo":Landroid/content/pm/PackageInfo;
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    .end local v1    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v2

    .line 92
    :catch_0
    move-exception v0

    .line 93
    .local v0, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string/jumbo v3, "Logging"

    const-string/jumbo v4, "[SW] Could not find ContextProvider"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static insertLog(Ljava/lang/String;)V
    .locals 6
    .param p0, "feature"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/Logging;->getVersionOfContextProviders()I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_0

    .line 83
    :goto_0
    return-void

    .line 69
    :cond_0
    const-string/jumbo v4, "content://com.samsung.android.providers.context.log.use_app_feature_survey"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 73
    .local v3, "uri":Landroid/net/Uri;
    :try_start_0
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/Logging;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 74
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 75
    .local v2, "row":Landroid/content/ContentValues;
    const-string/jumbo v4, "app_id"

    const-string/jumbo v5, "com.sec.android.automotive.drivelink"

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string/jumbo v4, "feature"

    invoke-virtual {v2, v4, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 78
    const-string/jumbo v4, "Logging"

    const-string/jumbo v5, "ContextProvider insertion operation is performed."

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 79
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "row":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    .line 80
    .local v1, "ex":Ljava/lang/Exception;
    const-string/jumbo v4, "Logging"

    const-string/jumbo v5, "Error while using the ContextProvider"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    const-string/jumbo v4, "Logging"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
