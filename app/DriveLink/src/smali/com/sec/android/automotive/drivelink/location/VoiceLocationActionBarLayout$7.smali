.class Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$7;
.super Ljava/lang/Object;
.source "VoiceLocationActionBarLayout.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->runTTSBarAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$7;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 778
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 802
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$7;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$19(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;Z)V

    .line 803
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 794
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$7;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isRunning:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$15(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 795
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$7;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$7;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mDirection:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$16(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$17(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;Z)V

    .line 796
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$7;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    # invokes: Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->runTTSBarAnimation()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$18(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)V

    .line 798
    :cond_0
    return-void

    .line 795
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 790
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 784
    return-void
.end method
