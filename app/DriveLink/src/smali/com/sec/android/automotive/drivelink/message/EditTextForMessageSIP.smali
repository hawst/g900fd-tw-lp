.class public Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP;
.super Landroid/widget/EditText;
.source "EditTextForMessageSIP.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;,
        Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$OnMessageSipStateListener;
    }
.end annotation


# instance fields
.field mMessageTextViewStateListener:Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$OnMessageSipStateListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method


# virtual methods
.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 35
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 36
    invoke-super {p0, p1}, Landroid/widget/EditText;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 43
    :goto_0
    return v0

    .line 40
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;->SIP_CLOSE:Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP;->setMessageStatechanged(Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;)V

    .line 43
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/EditText;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setMessageStatechanged(Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;)V
    .locals 1
    .param p1, "state"    # Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP;->mMessageTextViewStateListener:Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$OnMessageSipStateListener;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP;->mMessageTextViewStateListener:Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$OnMessageSipStateListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$OnMessageSipStateListener;->OnStateChanged(Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;)V

    .line 53
    :cond_0
    return-void
.end method

.method public setOnMessageSipStateListener(Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$OnMessageSipStateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$OnMessageSipStateListener;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP;->mMessageTextViewStateListener:Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$OnMessageSipStateListener;

    .line 48
    return-void
.end method
