.class public Lcom/sec/android/automotive/drivelink/common/view/easing/ExponentialEaseOut;
.super Ljava/lang/Object;
.source "ExponentialEaseOut.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field public s:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/ExponentialEaseOut;->s:F

    .line 12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/ExponentialEaseOut;->s:F

    .line 15
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 6
    .param p1, "input"    # F

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 18
    cmpl-float v2, p1, v5

    if-nez v2, :cond_0

    :goto_0
    double-to-float v0, v0

    return v0

    :cond_0
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    const/high16 v4, -0x3ee00000    # -10.0f

    mul-float/2addr v4, p1

    .line 19
    div-float/2addr v4, v5

    float-to-double v4, v4

    .line 18
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    neg-double v2, v2

    .line 19
    add-double/2addr v2, v0

    mul-double/2addr v0, v2

    const-wide/16 v2, 0x0

    add-double/2addr v0, v2

    goto :goto_0
.end method
