.class Lcom/sec/android/automotive/drivelink/common/DLServiceManager$5;
.super Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;
.source "DLServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getOnDLLocationSuggestionListener()Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$5;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    .line 590
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseRequestRecommendedLocationList(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 596
    .local p1, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    const-string/jumbo v0, "DLServiceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[LOADINGLIST] Loading list"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 597
    const-string/jumbo v2, " LOCATIONS"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 596
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$5;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$21(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V

    .line 601
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$5;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mOnUpdateLocationSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$22(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 602
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$5;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mOnUpdateLocationSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$22(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;

    move-result-object v0

    .line 603
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$5;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mLocationSuggestionList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$23(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;->onUpdateLocationSuggestionList(Ljava/util/ArrayList;)V

    .line 606
    :cond_0
    return-void
.end method
