.class public Lcom/sec/android/automotive/drivelink/notification/NotificationController;
.super Ljava/lang/Object;
.source "NotificationController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/notification/NotificationController$NotificationCallbackListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "[NotificationController]"

.field private static nextCommand:Lcom/sec/android/automotive/drivelink/notification/INotiCommand;


# instance fields
.field bEndAnimationStarted:Z

.field private mContext:Landroid/content/Context;

.field private mLayoutNotification:Landroid/view/ViewGroup;

.field private mNotiCallbackListener:Lcom/sec/android/automotive/drivelink/notification/NotificationController$NotificationCallbackListener;

.field mNotiChangeListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

.field private mNotiReqListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

.field private mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

.field private mNotificationAppearListener:Landroid/view/animation/Animation$AnimationListener;

.field private mNotificationDisappearListener:Landroid/view/animation/Animation$AnimationListener;

.field private mNotificationDisappearListenerWithCommand:Landroid/view/animation/Animation$AnimationListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->nextCommand:Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/sec/android/automotive/drivelink/notification/NotificationController$NotificationCallbackListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # Landroid/view/ViewGroup;
    .param p3, "listener"    # Lcom/sec/android/automotive/drivelink/notification/NotificationController$NotificationCallbackListener;

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mLayoutNotification:Landroid/view/ViewGroup;

    .line 24
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiCallbackListener:Lcom/sec/android/automotive/drivelink/notification/NotificationController$NotificationCallbackListener;

    .line 25
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mContext:Landroid/content/Context;

    .line 26
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .line 58
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationController$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/NotificationController;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiChangeListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    .line 68
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationController$2;-><init>(Lcom/sec/android/automotive/drivelink/notification/NotificationController;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiReqListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    .line 217
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationController$3;-><init>(Lcom/sec/android/automotive/drivelink/notification/NotificationController;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotificationAppearListener:Landroid/view/animation/Animation$AnimationListener;

    .line 249
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->bEndAnimationStarted:Z

    .line 251
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$4;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationController$4;-><init>(Lcom/sec/android/automotive/drivelink/notification/NotificationController;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotificationDisappearListener:Landroid/view/animation/Animation$AnimationListener;

    .line 276
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$5;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationController$5;-><init>(Lcom/sec/android/automotive/drivelink/notification/NotificationController;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotificationDisappearListenerWithCommand:Landroid/view/animation/Animation$AnimationListener;

    .line 37
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mLayoutNotification:Landroid/view/ViewGroup;

    .line 38
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiCallbackListener:Lcom/sec/android/automotive/drivelink/notification/NotificationController$NotificationCallbackListener;

    .line 39
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mContext:Landroid/content/Context;

    .line 40
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/notification/NotificationController;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->dismissNotification(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/notification/INotiCommand;)V
    .locals 0

    .prologue
    .line 27
    sput-object p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->nextCommand:Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/notification/NotificationController;)Lcom/sec/android/automotive/drivelink/notification/NotificationController$NotificationCallbackListener;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiCallbackListener:Lcom/sec/android/automotive/drivelink/notification/NotificationController$NotificationCallbackListener;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/notification/NotificationController;Z)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->removeNotification(Z)V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/notification/NotificationController;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/notification/NotificationController;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/notification/NotificationController;Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    .locals 0

    .prologue
    .line 189
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    return-void
.end method

.method static synthetic access$7()Lcom/sec/android/automotive/drivelink/notification/INotiCommand;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->nextCommand:Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    return-object v0
.end method

.method private dismissNotification(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V
    .locals 3
    .param p1, "noti"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    .param p2, "hasCommand"    # Z

    .prologue
    .line 130
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mLayoutNotification:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 131
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_DISAPPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    invoke-direct {p0, v1, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 134
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mContext:Landroid/content/Context;

    .line 135
    const v2, 0x7f04002d

    .line 134
    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 136
    .local v0, "animation":Landroid/view/animation/Animation;
    if-eqz p2, :cond_1

    .line 138
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotificationDisappearListenerWithCommand:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 142
    :goto_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 143
    invoke-virtual {p1, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 144
    const/4 v1, 0x4

    invoke-virtual {p1, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->setVisibility(I)V

    .line 146
    .end local v0    # "animation":Landroid/view/animation/Animation;
    :cond_0
    return-void

    .line 140
    .restart local v0    # "animation":Landroid/view/animation/Animation;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotificationDisappearListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_0
.end method

.method private notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    .locals 5
    .param p1, "state"    # Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;
    .param p2, "notiView"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .prologue
    .line 193
    if-nez p2, :cond_1

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    move-object v0, p2

    .line 199
    .local v0, "changeListener":Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;
    :try_start_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_APPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    if-ne p1, v2, :cond_2

    .line 200
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;->onNotificationWillAppear()V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 209
    :catch_0
    move-exception v1

    .line 210
    .local v1, "e":Ljava/lang/UnsupportedOperationException;
    const-string/jumbo v2, "[NotificationController]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "This NotificationView("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 211
    const-string/jumbo v4, ") does not support Change Listener"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 210
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    invoke-virtual {v1}, Ljava/lang/UnsupportedOperationException;->printStackTrace()V

    goto :goto_0

    .line 201
    .end local v1    # "e":Ljava/lang/UnsupportedOperationException;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->APPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    if-ne p1, v2, :cond_3

    .line 202
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;->onNotificationAppeared()V

    goto :goto_0

    .line 203
    :cond_3
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_DISAPPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    if-ne p1, v2, :cond_4

    .line 204
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;->onNotificationWillDisappear()V

    goto :goto_0

    .line 205
    :cond_4
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->DISAPPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    if-ne p1, v2, :cond_0

    .line 206
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;->onNotificationDisappeared()V
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private removeNotification(Z)V
    .locals 3
    .param p1, "shouldCheckNewNoti"    # Z

    .prologue
    .line 113
    const-string/jumbo v0, "[NotificationController]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "removeNotification: check New Noti? "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->cancelNotiFlow()V

    .line 116
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mLayoutNotification:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 118
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v1

    .line 117
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->removeNotification(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    .line 119
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->DISAPPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .line 121
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->checkNotification(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiCallbackListener:Lcom/sec/android/automotive/drivelink/notification/NotificationController$NotificationCallbackListener;

    if-eqz v0, :cond_0

    .line 123
    const-string/jumbo v0, "[NotificationController]"

    const-string/jumbo v1, "no more notification exist! finish"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiCallbackListener:Lcom/sec/android/automotive/drivelink/notification/NotificationController$NotificationCallbackListener;

    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationController$NotificationCallbackListener;->onNotificationIsEmpty()V

    .line 127
    :cond_0
    return-void
.end method


# virtual methods
.method protected checkNotification(Z)Z
    .locals 6
    .param p1, "bAnimation"    # Z

    .prologue
    .line 149
    const/4 v1, 0x0

    .line 150
    .local v1, "bNotified":Z
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mLayoutNotification:Landroid/view/ViewGroup;

    if-nez v3, :cond_0

    .line 151
    const-string/jumbo v3, "[NotificationController]"

    const-string/jumbo v4, "Notification Layout is Null!!"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 186
    .end local v1    # "bNotified":Z
    .local v2, "bNotified":I
    :goto_0
    return v2

    .line 155
    .end local v2    # "bNotified":I
    .restart local v1    # "bNotified":Z
    :cond_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v3

    .line 156
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getTopNotificationView()Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    move-result-object v3

    .line 155
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .line 158
    const-string/jumbo v3, "[NotificationController]"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "NotiView:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v3, :cond_3

    .line 161
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiReqListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->setOnNotificationRequestListener(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;)V

    .line 163
    sget-object v3, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_APPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-direct {p0, v3, v4}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 165
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mLayoutNotification:Landroid/view/ViewGroup;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setFocusable(Z)V

    .line 166
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mLayoutNotification:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 167
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    if-eqz v3, :cond_1

    .line 168
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 171
    :cond_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mLayoutNotification:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 172
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;

    move-result-object v3

    .line 173
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->setOnMicStateChangeListener(Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;)V

    .line 175
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->isAppeared()Z

    move-result v3

    if-nez v3, :cond_2

    .line 176
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mContext:Landroid/content/Context;

    .line 177
    const v4, 0x7f04002b

    .line 176
    invoke-static {v3, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 178
    .local v0, "animation":Landroid/view/animation/Animation;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotificationAppearListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 179
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v3, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 180
    const-string/jumbo v3, "[NotificationController]"

    const-string/jumbo v4, "Noti Appear Animation will start."

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    .end local v0    # "animation":Landroid/view/animation/Animation;
    :cond_2
    const/4 v1, 0x1

    :cond_3
    move v2, v1

    .line 186
    .restart local v2    # "bNotified":I
    goto/16 :goto_0
.end method

.method public pause()V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method public resume()V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method public start()V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public stop()V
    .locals 0

    .prologue
    .line 56
    return-void
.end method
