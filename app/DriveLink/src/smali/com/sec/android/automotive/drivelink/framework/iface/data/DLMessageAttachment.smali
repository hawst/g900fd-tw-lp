.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessageAttachment;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLMessageAttachment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract IsText()Z
.end method

.method public abstract getAudioCount()I
.end method

.method public abstract getCalendarCount()I
.end method

.method public abstract getContactCount()I
.end method

.method public abstract getImageCount()I
.end method

.method public abstract getText()Ljava/lang/String;
.end method

.method public abstract getVideoCount()I
.end method
