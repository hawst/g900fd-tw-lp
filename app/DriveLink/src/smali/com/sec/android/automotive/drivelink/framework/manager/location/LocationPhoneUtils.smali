.class public Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;
.super Ljava/lang/Object;
.source "LocationPhoneUtils.java"


# static fields
.field private static mCountryCode:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method public static areEquals(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "phone1"    # Ljava/lang/String;
    .param p1, "phone2"    # Ljava/lang/String;

    .prologue
    .line 52
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 53
    :cond_0
    const/4 v0, 0x0

    .line 64
    :goto_0
    return v0

    .line 55
    :cond_1
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->removeCountryCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 56
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->removeCountryCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 58
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v0, v1, :cond_3

    .line 59
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 64
    :cond_2
    :goto_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 60
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v0, v1, :cond_2

    .line 61
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1
.end method

.method private static getCountryCode(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 82
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->initCountryTable()V

    .line 85
    const-string/jumbo v2, "phone"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 84
    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 86
    .local v1, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v0

    .line 88
    .local v0, "simCountryCode":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->mCountryCode:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    return-object v2
.end method

.method public static getMyCountryCode(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    .line 42
    const-string/jumbo v2, "phone"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 41
    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 44
    .local v1, "telemamanger":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 48
    :cond_0
    return-object v0
.end method

.method public static getMyPhoneNumber(Landroid/content/Context;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    .line 20
    const-string/jumbo v5, "phone"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 19
    check-cast v4, Landroid/telephony/TelephonyManager;

    .line 22
    .local v4, "telemamanger":Landroid/telephony/TelephonyManager;
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v3

    .line 23
    .local v3, "phoneNumber":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 24
    :cond_0
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 25
    .local v2, "am":Landroid/accounts/AccountManager;
    if-eqz v2, :cond_1

    .line 26
    invoke-virtual {v2}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    .line 27
    .local v1, "accounts":[Landroid/accounts/Account;
    if-eqz v1, :cond_1

    .line 28
    array-length v6, v1

    const/4 v5, 0x0

    :goto_0
    if-lt v5, v6, :cond_2

    .line 37
    .end local v1    # "accounts":[Landroid/accounts/Account;
    .end local v2    # "am":Landroid/accounts/AccountManager;
    :cond_1
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->removeCountryCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 28
    .restart local v1    # "accounts":[Landroid/accounts/Account;
    .restart local v2    # "am":Landroid/accounts/AccountManager;
    :cond_2
    aget-object v0, v1, v5

    .line 29
    .local v0, "ac":Landroid/accounts/Account;
    iget-object v7, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string/jumbo v8, "com.whatsapp"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 30
    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 28
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method private static initCountryTable()V
    .locals 3

    .prologue
    .line 92
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->mCountryCode:Ljava/util/HashMap;

    .line 93
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->mCountryCode:Ljava/util/HashMap;

    const-string/jumbo v1, "BR"

    const-string/jumbo v2, "55"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->mCountryCode:Ljava/util/HashMap;

    const-string/jumbo v1, "US"

    const-string/jumbo v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->mCountryCode:Ljava/util/HashMap;

    const-string/jumbo v1, "KR"

    const-string/jumbo v2, "82"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    return-void
.end method

.method public static removeCountryCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 68
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    :cond_0
    const-string/jumbo v0, ""

    .line 78
    :goto_0
    return-object v0

    .line 71
    :cond_1
    const-string/jumbo v0, "-"

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 72
    const/16 v0, 0x2b

    const/16 v1, 0x20

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p0

    .line 78
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
