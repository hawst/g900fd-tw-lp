.class Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3$1;
.super Ljava/lang/Object;
.source "MessageReaderActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;

    .line 974
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 978
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 979
    .local v0, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    move-result-object v1

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMessageList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$4(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/util/ArrayList;

    move-result-object v1

    .line 980
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    move-result-object v2

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSListIndex:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$8(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 979
    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 982
    const-string/jumbo v1, "DM_SMS_READBACK"

    .line 981
    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 983
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    move-result-object v1

    const-string/jumbo v2, "DM_SMS_READBACK"

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->setActivityFlowID(Ljava/lang/String;)V

    .line 984
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$9(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;I)V

    .line 985
    return-void
.end method
