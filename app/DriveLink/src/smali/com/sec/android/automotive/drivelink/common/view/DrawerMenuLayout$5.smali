.class Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;
.super Ljava/lang/Object;
.source "DrawerMenuLayout.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private PRECISION_MARGIN:F

.field btnB:Ljava/math/BigDecimal;

.field private downTime:J

.field private downX:F

.field private isDragging:Z

.field layoutB:Ljava/math/BigDecimal;

.field listWidth:Ljava/math/BigDecimal;

.field private startX:Ljava/math/BigDecimal;

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)V
    .locals 3

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    .line 247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->PRECISION_MARGIN:F

    .line 249
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->downX:F

    .line 251
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->isDragging:Z

    .line 252
    new-instance v0, Ljava/math/BigDecimal;

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    invoke-direct {v0, v1, v2}, Ljava/math/BigDecimal;-><init>(D)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->layoutB:Ljava/math/BigDecimal;

    .line 253
    new-instance v0, Ljava/math/BigDecimal;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$2(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/content/Context;

    move-result-object v1

    .line 254
    const/high16 v2, -0x3f600000    # -5.0f

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->dipToPixels(Landroid/content/Context;F)F

    move-result v1

    float-to-double v1, v1

    invoke-direct {v0, v1, v2}, Ljava/math/BigDecimal;-><init>(D)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->btnB:Ljava/math/BigDecimal;

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 260
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_0
    move v2, v3

    .line 341
    :goto_1
    return v2

    .line 262
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->downX:F

    .line 263
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->downTime:J

    .line 264
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->listWidth:Ljava/math/BigDecimal;

    if-nez v2, :cond_0

    .line 265
    new-instance v2, Ljava/math/BigDecimal;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuList:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$10(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->getWidth()I

    move-result v4

    invoke-direct {v2, v4}, Ljava/math/BigDecimal;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->listWidth:Ljava/math/BigDecimal;

    goto :goto_0

    .line 269
    :pswitch_1
    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->isDragging:Z

    if-eqz v4, :cond_3

    .line 270
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->isDragging:Z

    .line 273
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    float-to-double v3, v3

    invoke-direct {v0, v3, v4}, Ljava/math/BigDecimal;-><init>(D)V

    .line 274
    .local v0, "currX":Ljava/math/BigDecimal;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->startX:Ljava/math/BigDecimal;

    invoke-virtual {v3, v0}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v3}, Ljava/math/BigDecimal;->floatValue()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuList:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$10(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/ListView;

    move-result-object v4

    .line 275
    invoke-virtual {v4}, Landroid/widget/ListView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 276
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->openDrawerMenu()V

    .line 281
    :goto_2
    iget v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->downX:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    sub-float/2addr v3, v4

    const/high16 v4, 0x42480000    # 50.0f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    .line 282
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->downTime:J

    sub-long/2addr v3, v5

    const-wide/16 v5, 0x12c

    cmp-long v3, v3, v5

    if-gez v3, :cond_1

    .line 283
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->closeDrawerMenu()V

    .line 286
    :cond_1
    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->startX:Ljava/math/BigDecimal;

    .line 287
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    invoke-static {v3, v7}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$6(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;Landroid/view/View;)V

    .line 288
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    invoke-static {v3, v7}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$7(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 278
    :cond_2
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->closeDrawerMenu()V

    goto :goto_2

    .end local v0    # "currX":Ljava/math/BigDecimal;
    :cond_3
    move v2, v3

    .line 291
    goto/16 :goto_1

    .line 294
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsScrolling:Z
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$13(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 295
    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->isDragging:Z

    if-eqz v4, :cond_6

    .line 296
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    float-to-double v3, v3

    invoke-direct {v0, v3, v4}, Ljava/math/BigDecimal;-><init>(D)V

    .line 297
    .restart local v0    # "currX":Ljava/math/BigDecimal;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->startX:Ljava/math/BigDecimal;

    invoke-virtual {v0, v3}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 299
    .local v1, "moveX":Ljava/math/BigDecimal;
    invoke-virtual {v1}, Ljava/math/BigDecimal;->floatValue()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->listWidth:Ljava/math/BigDecimal;

    invoke-virtual {v4}, Ljava/math/BigDecimal;->negate()Ljava/math/BigDecimal;

    move-result-object v4

    .line 300
    invoke-virtual {v4}, Ljava/math/BigDecimal;->floatValue()F

    move-result v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_4

    .line 301
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->startX:Ljava/math/BigDecimal;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->listWidth:Ljava/math/BigDecimal;

    invoke-virtual {v4, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->startX:Ljava/math/BigDecimal;

    .line 302
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuList:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$10(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/ListView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->listWidth:Ljava/math/BigDecimal;

    invoke-virtual {v4}, Ljava/math/BigDecimal;->negate()Ljava/math/BigDecimal;

    move-result-object v4

    .line 303
    invoke-virtual {v4}, Ljava/math/BigDecimal;->floatValue()F

    move-result v4

    .line 302
    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setX(F)V

    .line 304
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuBgFrame:Landroid/widget/FrameLayout;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$11(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/FrameLayout;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/FrameLayout;->setAlpha(F)V

    .line 305
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuBtn:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$12(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setX(F)V

    goto/16 :goto_1

    .line 306
    :cond_4
    invoke-virtual {v1}, Ljava/math/BigDecimal;->floatValue()F

    move-result v3

    cmpl-float v3, v3, v5

    if-lez v3, :cond_5

    .line 308
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->startX:Ljava/math/BigDecimal;

    invoke-virtual {v3, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->startX:Ljava/math/BigDecimal;

    .line 309
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuList:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$10(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/ListView;->setX(F)V

    .line 310
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuBgFrame:Landroid/widget/FrameLayout;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$11(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/FrameLayout;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setAlpha(F)V

    .line 311
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuBtn:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$12(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/ImageView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->btnB:Ljava/math/BigDecimal;

    invoke-virtual {v4}, Ljava/math/BigDecimal;->floatValue()F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setX(F)V

    goto/16 :goto_1

    .line 313
    :cond_5
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuList:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$10(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v1}, Ljava/math/BigDecimal;->floatValue()F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setX(F)V

    .line 314
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuBgFrame:Landroid/widget/FrameLayout;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$11(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/FrameLayout;

    move-result-object v3

    .line 315
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->layoutB:Ljava/math/BigDecimal;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->listWidth:Ljava/math/BigDecimal;

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 314
    invoke-virtual {v1, v4}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 316
    invoke-virtual {v4}, Ljava/math/BigDecimal;->floatValue()F

    move-result v4

    .line 317
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->listWidth:Ljava/math/BigDecimal;

    invoke-virtual {v5}, Ljava/math/BigDecimal;->floatValue()F

    move-result v5

    div-float/2addr v4, v5

    .line 314
    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setAlpha(F)V

    .line 318
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuBtn:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$12(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/ImageView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->btnB:Ljava/math/BigDecimal;

    invoke-virtual {v1, v4}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 319
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->btnB:Ljava/math/BigDecimal;

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->listWidth:Ljava/math/BigDecimal;

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 320
    invoke-virtual {v4}, Ljava/math/BigDecimal;->floatValue()F

    move-result v4

    .line 321
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->listWidth:Ljava/math/BigDecimal;

    invoke-virtual {v5}, Ljava/math/BigDecimal;->floatValue()F

    move-result v5

    div-float/2addr v4, v5

    .line 318
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setX(F)V

    goto/16 :goto_1

    .line 325
    .end local v0    # "currX":Ljava/math/BigDecimal;
    .end local v1    # "moveX":Ljava/math/BigDecimal;
    :cond_6
    iget v4, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->downX:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v5

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->PRECISION_MARGIN:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 326
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->isDragging:Z

    .line 327
    new-instance v2, Ljava/math/BigDecimal;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    float-to-double v4, v4

    invoke-direct {v2, v4, v5}, Ljava/math/BigDecimal;-><init>(D)V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->startX:Ljava/math/BigDecimal;

    .line 328
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mPressedView:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$14(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 329
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mPressedMotionEvent:Landroid/view/MotionEvent;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$15(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/view/MotionEvent;

    move-result-object v2

    .line 330
    const/4 v4, 0x3

    invoke-virtual {v2, v4}, Landroid/view/MotionEvent;->setAction(I)V

    .line 331
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mPressedView:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$14(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/view/View;

    move-result-object v2

    .line 332
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mPressedMotionEvent:Landroid/view/MotionEvent;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$15(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/view/MotionEvent;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 333
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    invoke-static {v2, v7}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$6(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;Landroid/view/View;)V

    .line 334
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    invoke-static {v2, v7}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$7(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 260
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
