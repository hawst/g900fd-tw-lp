.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
.source "DLLocationImp.java"


# static fields
.field private static final serialVersionUID:J = 0x129c098f52891610L


# instance fields
.field private mCount:I

.field private mLatitude:D

.field protected mLocationAddress:Ljava/lang/String;

.field private mLocationID:Ljava/lang/String;

.field protected mLocationName:Ljava/lang/String;

.field private mLocationType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

.field private mLongitude:D

.field private mTimestamp:J


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const-wide/16 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const-wide v1, 0x40c3878000000000L    # 9999.0

    .line 19
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;-><init>()V

    .line 9
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLocationName:Ljava/lang/String;

    .line 10
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLocationAddress:Ljava/lang/String;

    .line 12
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLocationType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    .line 13
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLocationID:Ljava/lang/String;

    .line 14
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLatitude:D

    .line 15
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLongitude:D

    .line 16
    iput-wide v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mTimestamp:J

    .line 17
    iput v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mCount:I

    .line 20
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLocationName:Ljava/lang/String;

    .line 21
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLocationAddress:Ljava/lang/String;

    .line 22
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLocationID:Ljava/lang/String;

    .line 23
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLatitude:D

    .line 24
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLongitude:D

    .line 25
    iput-wide v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mTimestamp:J

    .line 26
    iput v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mCount:I

    .line 27
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mCount:I

    return v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLatitude:D

    return-wide v0
.end method

.method public getLocationAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLocationAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getLocationID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLocationID:Ljava/lang/String;

    return-object v0
.end method

.method public getLocationName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLocationName:Ljava/lang/String;

    return-object v0
.end method

.method public getLocationType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLocationType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    return-object v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLongitude:D

    return-wide v0
.end method

.method public getTimestamp()J
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mTimestamp:J

    return-wide v0
.end method

.method public isSamePosition(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 14
    .param p1, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    const-wide v12, 0x3f50624dd2f1a9fcL    # 0.001

    const-wide/16 v10, 0x0

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    .line 113
    iget-wide v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLatitude:D

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v6

    sub-double v0, v4, v6

    .line 114
    .local v0, "errorLat":D
    iget-wide v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLongitude:D

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v6

    sub-double v2, v4, v6

    .line 116
    .local v2, "errorLng":D
    cmpl-double v4, v0, v10

    if-lez v4, :cond_0

    .line 117
    :goto_0
    cmpl-double v4, v2, v10

    if-lez v4, :cond_1

    .line 119
    :goto_1
    cmpg-double v4, v0, v12

    if-gez v4, :cond_2

    cmpg-double v4, v2, v12

    if-gez v4, :cond_2

    const/4 v4, 0x1

    :goto_2
    return v4

    .line 116
    :cond_0
    mul-double/2addr v0, v8

    goto :goto_0

    .line 117
    :cond_1
    mul-double/2addr v2, v8

    goto :goto_1

    .line 119
    :cond_2
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public isValidAddres()Z
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLocationAddress:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLocationAddress:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValidCoords()Z
    .locals 4

    .prologue
    .line 95
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLatitude:D

    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLongitude:D

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->isValidCoords(DD)Z

    move-result v0

    return v0
.end method

.method public setCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 90
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mCount:I

    .line 91
    return-void
.end method

.method public setLatitude(D)V
    .locals 0
    .param p1, "lat"    # D

    .prologue
    .line 66
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLatitude:D

    .line 67
    return-void
.end method

.method public setLocationAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLocationAddress:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public setLocationID(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLocationID:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setLocationName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLocationName:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public setLocationType(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;)V
    .locals 0
    .param p1, "locationType"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLocationType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    .line 35
    return-void
.end method

.method public setLongitude(D)V
    .locals 0
    .param p1, "lng"    # D

    .prologue
    .line 74
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLongitude:D

    .line 75
    return-void
.end method

.method public setTimestamp(J)V
    .locals 0
    .param p1, "timestamp"    # J

    .prologue
    .line 82
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mTimestamp:J

    .line 83
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 105
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Location name: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLocationName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", Address: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 106
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLocationAddress:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", Latitude: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLatitude:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 107
    const-string/jumbo v2, ", Longitude: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->mLongitude:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 105
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 108
    .local v0, "str":Ljava/lang/String;
    return-object v0
.end method
