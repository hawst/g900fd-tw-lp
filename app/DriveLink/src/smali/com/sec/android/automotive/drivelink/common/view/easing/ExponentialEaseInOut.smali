.class public Lcom/sec/android/automotive/drivelink/common/view/easing/ExponentialEaseInOut;
.super Ljava/lang/Object;
.source "ExponentialEaseInOut.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field public s:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/ExponentialEaseInOut;->s:F

    .line 12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/ExponentialEaseInOut;->s:F

    .line 15
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 9
    .param p1, "input"    # F

    .prologue
    const/4 v0, 0x0

    const-wide/high16 v7, 0x3fe0000000000000L    # 0.5

    const-wide/16 v5, 0x0

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 19
    cmpl-float v2, p1, v0

    if-nez v2, :cond_0

    .line 28
    :goto_0
    return v0

    .line 22
    :cond_0
    cmpl-float v0, p1, v1

    if-nez v0, :cond_1

    move v0, v1

    .line 23
    goto :goto_0

    .line 25
    :cond_1
    const/high16 v0, 0x3f000000    # 0.5f

    div-float/2addr p1, v0

    cmpg-float v0, p1, v1

    if-gez v0, :cond_2

    .line 26
    const/high16 v0, 0x41200000    # 10.0f

    sub-float v1, p1, v1

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v3, v4, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    mul-double/2addr v0, v7

    add-double/2addr v0, v5

    double-to-float v0, v0

    goto :goto_0

    .line 28
    :cond_2
    const/high16 v0, -0x3ee00000    # -10.0f

    sub-float/2addr p1, v1

    mul-float/2addr v0, p1

    float-to-double v0, v0

    invoke-static {v3, v4, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    neg-double v0, v0

    add-double/2addr v0, v3

    mul-double/2addr v0, v7

    add-double/2addr v0, v5

    double-to-float v0, v0

    goto :goto_0
.end method
