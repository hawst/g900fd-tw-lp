.class Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$2;
.super Landroid/os/Handler;
.source "DLWakeupManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    .line 193
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 197
    # getter for: Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "mAudioFocusLossHandler"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->access$2(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;)Landroid/content/Context;

    move-result-object v1

    # invokes: Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getAudioManager(Landroid/content/Context;)Landroid/media/AudioManager;
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->access$4(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;Landroid/content/Context;)Landroid/media/AudioManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    # getter for: Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "mAudioFocusLossHandler : Music Active"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->access$2(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->stopWakeupBargeIn(Landroid/content/Context;Z)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->access$2(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;)Landroid/content/Context;

    move-result-object v1

    # invokes: Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getAudioManager(Landroid/content/Context;)Landroid/media/AudioManager;
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->access$4(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;Landroid/content/Context;)Landroid/media/AudioManager;

    move-result-object v0

    .line 204
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mChipWakeupFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->access$5(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;)Landroid/media/AudioManager$OnAudioFocusChangeListener;

    move-result-object v1

    .line 203
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 205
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->access$2(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;)Landroid/content/Context;

    move-result-object v1

    # invokes: Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->isEnableWakeup(Landroid/content/Context;)Z
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->access$6(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->access$2(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;)Landroid/content/Context;

    move-result-object v1

    # invokes: Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->startAPWakeupBargeIn(Landroid/content/Context;)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->access$7(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;Landroid/content/Context;)V

    .line 215
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 216
    return-void

    .line 211
    :cond_1
    # getter for: Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 212
    const-string/jumbo v1, "mAudioFocusLossHandler : not Music active (recording)"

    .line 211
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
