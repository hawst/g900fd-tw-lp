.class public Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;
.super Ljava/lang/Thread;
.source "WorkerThread.java"


# instance fields
.field private final channel:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

.field private m_bStopped:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "channel"    # Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 11
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;->channel:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;->m_bStopped:Z

    .line 14
    return-void
.end method

.method private isStopped()Z
    .locals 1

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;->m_bStopped:Z

    return v0
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 25
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;->isStopped()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 37
    :goto_1
    return-void

    .line 26
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;->channel:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->takeRequest()Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;

    move-result-object v0

    .line 27
    .local v0, "request":Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;->isStopped()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 28
    const/4 v0, 0x0

    .line 29
    goto :goto_1

    .line 32
    :cond_2
    if-eqz v0, :cond_0

    .line 33
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;->execute()V

    goto :goto_0
.end method

.method public stopThread()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;->m_bStopped:Z

    .line 18
    return-void
.end method
