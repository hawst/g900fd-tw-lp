.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
.super Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;
.source "DLPlaceONBuddy.java"


# static fields
.field private static final serialVersionUID:J = 0x4e90a704f4c1be6bL


# instance fields
.field private mExpireTime:J

.field private mIsOwner:Z

.field private mShared:Z

.field private mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 12
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;-><init>()V

    .line 5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->mUrl:Ljava/lang/String;

    .line 6
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->mShared:Z

    .line 7
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->mIsOwner:Z

    .line 8
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->mExpireTime:J

    .line 13
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->mUrl:Ljava/lang/String;

    .line 14
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getDuration()J
    .locals 4

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getIsExpired()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    const-wide/16 v0, 0x0

    .line 44
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->mExpireTime:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public getExpireTime()J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->mExpireTime:J

    return-wide v0
.end method

.method public getIsExpired()Z
    .locals 4

    .prologue
    .line 48
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->mExpireTime:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsOwner()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->mIsOwner:Z

    return v0
.end method

.method public getShared()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->mShared:Z

    return v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;->hashCode()I

    move-result v0

    return v0
.end method

.method public setExpireTime(J)V
    .locals 0
    .param p1, "time"    # J

    .prologue
    .line 56
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->mExpireTime:J

    .line 57
    return-void
.end method

.method public setIsOwner(Z)V
    .locals 0
    .param p1, "owner"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->mIsOwner:Z

    .line 39
    return-void
.end method

.method public setShared(Z)V
    .locals 0
    .param p1, "shared"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->mShared:Z

    .line 31
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->mUrl:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 60
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Buddy name: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getFriendName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", Phone: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 61
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", IsOwner: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getIsOwner()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "treu"

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", Status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getStatus()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->getValue()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 60
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 64
    .local v0, "str":Ljava/lang/String;
    return-object v0

    .line 62
    .end local v0    # "str":Ljava/lang/String;
    :cond_0
    const-string/jumbo v1, "false"

    goto :goto_0
.end method
