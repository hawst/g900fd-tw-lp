.class Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;
.super Landroid/os/Handler;
.source "ConnectivityManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    .line 1496
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1499
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 1697
    :cond_0
    :goto_0
    return-void

    .line 1502
    :sswitch_0
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1503
    const-string/jumbo v1, "handleMessage BLUETOOTH_PAIRING_REQUEST"

    .line 1502
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1504
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->sendBTPairingRequestNotification(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;)V

    goto :goto_0

    .line 1507
    :sswitch_1
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1508
    const-string/jumbo v1, "handleMessage BLUETOOTH_BOND_STATE_BONDING"

    .line 1507
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1511
    :sswitch_2
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1512
    const-string/jumbo v2, "handleMessage BLUETOOTH_BOND_STATE_BONDED"

    .line 1511
    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1513
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 1515
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1516
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkConnectivityListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1517
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1518
    const-string/jumbo v1, "handleMessage BLUETOOTH_BOND_STATE_BONDED onNotifyBluetoothDevicePaired"

    .line 1517
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1519
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkConnectivityListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;

    move-result-object v0

    .line 1520
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;->onNotifyBluetoothDevicePaired()V

    goto :goto_0

    .line 1524
    :sswitch_3
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1525
    const-string/jumbo v1, "handleMessage BLUETOOTH_BOND_STATE_NONE"

    .line 1524
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1526
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 1527
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1528
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkConnectivityListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1529
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1530
    const-string/jumbo v1, "handleMessage BLUETOOTH_BOND_STATE_NONE onNotifyBluetoothDeviceUnpaired"

    .line 1529
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1531
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkConnectivityListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;

    move-result-object v0

    .line 1532
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;->onNotifyBluetoothDeviceUnpaired()V

    goto/16 :goto_0

    .line 1536
    :sswitch_4
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1537
    const-string/jumbo v3, "handleMessage BLUETOOTH_ACL_CONNECTED"

    .line 1536
    invoke-static {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1538
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->saveBTConnection(Ljava/lang/String;)V
    invoke-static {v3, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$3(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Ljava/lang/String;)V

    .line 1539
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getBluetoothDeviceByName(Ljava/lang/String;)V
    invoke-static {v3, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$4(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Ljava/lang/String;)V

    .line 1540
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$5(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 1541
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v0, :cond_1

    .line 1542
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothA2dp;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    .line 1543
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothA2dp;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1541
    :goto_1
    invoke-static {v3, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$6(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 1544
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v3, :cond_2

    .line 1545
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    .line 1546
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1544
    :goto_2
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$7(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 1548
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1549
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkConnectivityListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1550
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1551
    const-string/jumbo v1, "handleMessage BLUETOOTH_ACL_CONNECTED onNotifyBluetoothDeviceConnected"

    .line 1550
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1552
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkConnectivityListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;

    move-result-object v0

    .line 1553
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;->onNotifyBluetoothDeviceConnected()V

    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 1543
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1546
    goto :goto_2

    .line 1557
    :sswitch_5
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1558
    const-string/jumbo v1, "handleMessage BLUETOOTH_ACTION_FOUND"

    .line 1557
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1559
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mListOfBluetoothDevicesDiscovered:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$8(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1560
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1563
    :sswitch_6
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1564
    const-string/jumbo v2, "handleMessage BLUETOOTH_STATE_CONNECTED"

    .line 1563
    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1565
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$5(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    goto/16 :goto_0

    .line 1568
    :sswitch_7
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1569
    const-string/jumbo v3, "handleMessage BLUETOOTH_ACL_DISCONNECTED"

    .line 1568
    invoke-static {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1570
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$5(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 1571
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v0, :cond_4

    .line 1572
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothA2dp;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    .line 1573
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothA2dp;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 1571
    :goto_3
    invoke-static {v3, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$6(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 1574
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v3, :cond_5

    .line 1575
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    .line 1576
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 1574
    :goto_4
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$7(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 1582
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1583
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    .line 1584
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicRemotePlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v0

    .line 1583
    if-eqz v0, :cond_3

    .line 1585
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedA2DP:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$9(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedHFP:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$10(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1586
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicRemotePlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v0

    .line 1587
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;->onMusicRemotePlayerErrorStop()V

    .line 1590
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->objPollDisconnect:Ljava/lang/Object;

    monitor-enter v1

    .line 1592
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->objPollDisconnect:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 1590
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1595
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1596
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkConnectivityListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1597
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1598
    const-string/jumbo v1, "handleMessage BLUETOOTH_ACL_DISCONNECTED onNotifyBluetoothDeviceDisconnected"

    .line 1597
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1599
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkConnectivityListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;

    move-result-object v0

    .line 1600
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;->onNotifyBluetoothDeviceDisconnected()V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 1573
    goto/16 :goto_3

    :cond_5
    move v1, v2

    .line 1576
    goto :goto_4

    .line 1590
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1604
    :sswitch_8
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1605
    const-string/jumbo v1, "handleMessage BLUETOOTH_STATE_DISCONNECTED"

    .line 1604
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1606
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$5(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    goto/16 :goto_0

    .line 1609
    :sswitch_9
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1610
    const-string/jumbo v1, "handleMessage BLUETOOTH_FORCE_RECOVERY"

    .line 1609
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1611
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mCurrentBluetoothState:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$11(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)I

    move-result v1

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->recoverBluetoothOperation(I)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$12(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V

    goto/16 :goto_0

    .line 1614
    :sswitch_a
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1615
    const-string/jumbo v1, "handleMessage UNBOND_REASON_AUTH_FAILED"

    .line 1614
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1616
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$13(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V

    goto/16 :goto_0

    .line 1619
    :sswitch_b
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1620
    const-string/jumbo v1, "handleMessage UNBOND_REASON_AUTH_REJECTED"

    .line 1619
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1621
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    const/4 v1, -0x2

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$13(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V

    goto/16 :goto_0

    .line 1624
    :sswitch_c
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1625
    const-string/jumbo v1, "handleMessage UNBOND_REASON_AUTH_TIMEOUT"

    .line 1624
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1626
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    const/4 v1, -0x5

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$13(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V

    goto/16 :goto_0

    .line 1629
    :sswitch_d
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1630
    const-string/jumbo v1, "handleMessage UNBOND_REASON_DISCOVERY_IN_PROGRESS"

    .line 1629
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1631
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    const/4 v1, -0x4

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$13(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V

    goto/16 :goto_0

    .line 1634
    :sswitch_e
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1635
    const-string/jumbo v1, "handleMessage UNBOND_REASON_REMOTE_AUTH_CANCELED"

    .line 1634
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1636
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    const/4 v1, -0x7

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$13(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V

    goto/16 :goto_0

    .line 1639
    :sswitch_f
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1640
    const-string/jumbo v1, "handleMessage UNBOND_REASON_REMOTE_DEVICE_DOWN"

    .line 1639
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1641
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    const/4 v1, -0x3

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$13(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V

    goto/16 :goto_0

    .line 1644
    :sswitch_10
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1645
    const-string/jumbo v1, "handleMessage UNBOND_REASON_REPEATED_ATTEMPTS"

    .line 1644
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1646
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    const/4 v1, -0x6

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$13(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V

    goto/16 :goto_0

    .line 1649
    :sswitch_11
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1650
    const-string/jumbo v1, "handleMessage UNBOND_REASON_UNKNOWN"

    .line 1649
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1651
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    const/4 v1, -0x8

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$13(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V

    goto/16 :goto_0

    .line 1654
    :sswitch_12
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1655
    const-string/jumbo v1, "handleMessage INITIAL_STATE_OFF"

    .line 1654
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1656
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    const/16 v1, 0x65

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$14(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V

    goto/16 :goto_0

    .line 1659
    :sswitch_13
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "handleMessage INITIAL_STATE_ON"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1660
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    const/16 v1, 0x66

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$14(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V

    goto/16 :goto_0

    .line 1663
    :sswitch_14
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1664
    const-string/jumbo v1, "handleMessage PERIPHERAL_SEARCH"

    .line 1663
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1665
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    const/16 v1, 0x67

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$14(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V

    goto/16 :goto_0

    .line 1668
    :sswitch_15
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1669
    const-string/jumbo v1, "handleMessage PASSKEY_VERIFICATION"

    .line 1668
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1670
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    const/16 v1, 0x68

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$14(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V

    goto/16 :goto_0

    .line 1673
    :sswitch_16
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "handleMessage PAIRING_DEVICES"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1674
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    const/16 v1, 0x69

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$14(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V

    goto/16 :goto_0

    .line 1677
    :sswitch_17
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1678
    const-string/jumbo v1, "handleMessage CONNECTION_ESTABLISHMENT"

    .line 1677
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1679
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    const/16 v1, 0x6a

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$14(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V

    goto/16 :goto_0

    .line 1682
    :sswitch_18
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1683
    const-string/jumbo v1, "handleMessage CONNECTION_TERMINATING"

    .line 1682
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1684
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    const/16 v1, 0x6b

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$14(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V

    goto/16 :goto_0

    .line 1687
    :sswitch_19
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 1688
    const-string/jumbo v1, "handleMessage CONNECTION_COMPLETE"

    .line 1687
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1689
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    const/16 v1, 0x6c

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$14(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V

    goto/16 :goto_0

    .line 1692
    :sswitch_1a
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "handleMessage 0"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1693
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$15(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1499
    nop

    :sswitch_data_0
    .sparse-switch
        -0x8 -> :sswitch_11
        -0x7 -> :sswitch_e
        -0x6 -> :sswitch_10
        -0x5 -> :sswitch_c
        -0x4 -> :sswitch_d
        -0x3 -> :sswitch_f
        -0x2 -> :sswitch_b
        -0x1 -> :sswitch_a
        0x0 -> :sswitch_1a
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_7
        0x7 -> :sswitch_6
        0x8 -> :sswitch_8
        0x9 -> :sswitch_5
        0xa -> :sswitch_9
        0x65 -> :sswitch_12
        0x66 -> :sswitch_13
        0x67 -> :sswitch_14
        0x68 -> :sswitch_15
        0x69 -> :sswitch_16
        0x6a -> :sswitch_17
        0x6b -> :sswitch_18
        0x6c -> :sswitch_19
    .end sparse-switch
.end method
