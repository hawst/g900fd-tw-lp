.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$3;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->OnInboxChoice(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$3;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    .line 2238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 2273
    const-string/jumbo v0, "MessageActivity"

    .line 2274
    const-string/jumbo v1, "OnInboxChoice - setAutoShrink : false n true"

    .line 2273
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2275
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$3;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 2276
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$3;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 2277
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 3
    .param p1, "arg0"    # I
    .param p2, "arg1"    # F
    .param p3, "arg2"    # I

    .prologue
    const/4 v2, 0x0

    .line 2252
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$3;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->isLoaded:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2253
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$3;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;Z)V

    .line 2254
    if-nez p1, :cond_1

    .line 2255
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$3;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$28(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    .line 2256
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$3;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v1

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v1

    .line 2257
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getInitialPosition()I

    move-result v1

    .line 2256
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2258
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$3;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2259
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$3;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2260
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$3;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v0

    .line 2261
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getPageCount()I

    move-result v0

    .line 2260
    if-eqz v0, :cond_0

    .line 2262
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$3;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 2269
    :cond_0
    :goto_0
    return-void

    .line 2264
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$3;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 2265
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$3;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v1

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v1

    .line 2266
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 2264
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    goto :goto_0
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 2242
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$3;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getPageCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2243
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$3;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 2244
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$3;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v1

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v1

    .line 2245
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 2243
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 2247
    :cond_0
    return-void
.end method
