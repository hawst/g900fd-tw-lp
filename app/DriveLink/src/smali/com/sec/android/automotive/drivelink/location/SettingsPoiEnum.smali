.class public final enum Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;
.super Ljava/lang/Enum;
.source "SettingsPoiEnum.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DISTANCE:Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;

.field public static final enum PRICE:Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;

.field private static codeToStatusMapping:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private code:I

.field private description:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 12
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;

    const-string/jumbo v1, "DISTANCE"

    const v2, 0x7f0a0450

    invoke-direct {v0, v1, v4, v3, v2}, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;->DISTANCE:Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;

    new-instance v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;

    const-string/jumbo v1, "PRICE"

    .line 13
    const v2, 0x7f0a0451

    invoke-direct {v0, v1, v3, v5, v2}, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;->PRICE:Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;

    new-array v0, v5, [Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;

    sget-object v1, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;->DISTANCE:Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;->PRICE:Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;

    .line 18
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "code"    # I
    .param p4, "description"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 21
    iput p3, p0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;->code:I

    .line 22
    iput p4, p0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;->description:I

    .line 23
    return-void
.end method

.method public static getStatus(Ljava/lang/String;Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;
    .locals 1
    .param p0, "mKey"    # Ljava/lang/String;
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;->codeToStatusMapping:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 27
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;->initMapping(Landroid/content/Context;)V

    .line 29
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;->codeToStatusMapping:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;

    return-object v0
.end method

.method public static initMapping(Landroid/content/Context;)V
    .locals 6
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 33
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;->codeToStatusMapping:Ljava/util/Map;

    .line 34
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;->values()[Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 40
    return-void

    .line 34
    :cond_0
    aget-object v1, v3, v2

    .line 36
    .local v1, "s":Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;
    iget v5, v1, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;->description:I

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 38
    .local v0, "mKey":Ljava/lang/String;
    sget-object v5, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;->codeToStatusMapping:Ljava/util/Map;

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;->code:I

    return v0
.end method

.method public getDescription(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiEnum;->description:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
