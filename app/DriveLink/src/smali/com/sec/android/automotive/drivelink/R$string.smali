.class public final Lcom/sec/android/automotive/drivelink/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final CannotReadMessages:I = 0x7f0a0501

.field public static final CannotReadMessages_Spoken:I = 0x7f0a0502

.field public static final NavPrompt:I = 0x7f0a05de

.field public static final SpeakAContact:I = 0x7f0a04fd

.field public static final SpeakAContact_Spoken:I = 0x7f0a04fe

.field public static final SpeakReadCallOrCancel:I = 0x7f0a04ff

.field public static final SpeakReadCallOrCancel_Spoken:I = 0x7f0a0500

.field public static final UnsupportedAction:I = 0x7f0a05df

.field public static final about_privacy_url:I = 0x7f0a059d

.field public static final about_tos_url:I = 0x7f0a059c

.field public static final action_settings:I = 0x7f0a03b3

.field public static final activating_carmode_1:I = 0x7f0a04bf

.field public static final activating_carmode_2:I = 0x7f0a04c0

.field public static final activating_carmode_tip:I = 0x7f0a04c1

.field public static final adapt_voice:I = 0x7f0a06f7

.field public static final airplane_alert:I = 0x7f0a04db

.field public static final alarm:I = 0x7f0a07bd

.field public static final alarm_notification_summary:I = 0x7f0a07be

.field public static final alert_no:I = 0x7f0a0707

.field public static final alert_yes:I = 0x7f0a0706

.field public static final all_day:I = 0x7f0a071e

.field public static final alternates_for_d:I = 0x7f0a0505

.field public static final alternates_for_g:I = 0x7f0a050a

.field public static final alternates_for_l:I = 0x7f0a0509

.field public static final alternates_for_r:I = 0x7f0a0506

.field public static final alternates_for_t:I = 0x7f0a0507

.field public static final alternates_for_z:I = 0x7f0a0508

.field public static final am:I = 0x7f0a06fa

.field public static final am_day:I = 0x7f0a06fc

.field public static final am_morning:I = 0x7f0a06fb

.field public static final and:I = 0x7f0a049e

.field public static final ans_rej_call_1:I = 0x7f0a04ca

.field public static final ans_rej_call_2:I = 0x7f0a04cb

.field public static final ans_rej_call_tip_1:I = 0x7f0a04cc

.field public static final ans_rej_call_tip_2:I = 0x7f0a04cd

.field public static final app_name:I = 0x7f0a001a

.field public static final app_settings:I = 0x7f0a0628

.field public static final arrays_autodial_always:I = 0x7f0a0629

.field public static final arrays_autodial_confident:I = 0x7f0a062a

.field public static final arrays_autodial_never:I = 0x7f0a062b

.field public static final arrays_lang_de_de:I = 0x7f0a050b

.field public static final arrays_lang_en_uk:I = 0x7f0a050c

.field public static final arrays_lang_en_us:I = 0x7f0a050d

.field public static final arrays_lang_es_es:I = 0x7f0a050e

.field public static final arrays_lang_fr_fr:I = 0x7f0a0510

.field public static final arrays_lang_it_it:I = 0x7f0a0511

.field public static final arrays_lang_ja_jp:I = 0x7f0a0512

.field public static final arrays_lang_ko_kr:I = 0x7f0a0513

.field public static final arrays_lang_pr_br:I = 0x7f0a0516

.field public static final arrays_lang_ru_ru:I = 0x7f0a0515

.field public static final arrays_lang_v_es_la:I = 0x7f0a050f

.field public static final arrays_lang_zh_cn:I = 0x7f0a0514

.field public static final asr_edit_dm_command_cancel:I = 0x7f0a046f

.field public static final asr_edit_dm_command_replace:I = 0x7f0a046e

.field public static final asr_edit_dm_command_send:I = 0x7f0a046d

.field public static final asr_edit_dm_dial:I = 0x7f0a0468

.field public static final asr_edit_dm_dial_contact:I = 0x7f0a0465

.field public static final asr_edit_dm_dial_contact_search_list:I = 0x7f0a0466

.field public static final asr_edit_dm_dial_type:I = 0x7f0a0467

.field public static final asr_edit_dm_ordinal_1st:I = 0x7f0a0470

.field public static final asr_edit_dm_ordinal_2nd:I = 0x7f0a0471

.field public static final asr_edit_dm_ordinal_3rd:I = 0x7f0a0472

.field public static final asr_edit_dm_ordinal_4th:I = 0x7f0a0473

.field public static final asr_edit_dm_ordinal_5th:I = 0x7f0a0474

.field public static final asr_edit_dm_ordinal_6th:I = 0x7f0a0475

.field public static final asr_edit_dm_sms_contact:I = 0x7f0a0469

.field public static final asr_edit_dm_sms_contact_search_list:I = 0x7f0a046a

.field public static final asr_edit_dm_sms_msg:I = 0x7f0a046c

.field public static final asr_edit_dm_sms_type:I = 0x7f0a046b

.field public static final asr_edit_phone_type_home:I = 0x7f0a0464

.field public static final asr_edit_phone_type_mobile:I = 0x7f0a0462

.field public static final asr_edit_phone_type_office:I = 0x7f0a0463

.field public static final auth_client_needs_enabling_title:I = 0x7f0a0015

.field public static final auth_client_needs_installation_title:I = 0x7f0a0016

.field public static final auth_client_needs_update_title:I = 0x7f0a0017

.field public static final auth_client_play_services_err_notification_msg:I = 0x7f0a0018

.field public static final auth_client_requested_by_msg:I = 0x7f0a0019

.field public static final auth_client_using_bad_version_title:I = 0x7f0a0014

.field public static final back_to_main:I = 0x7f0a084b

.field public static final bg_wakeup_notification_text:I = 0x7f0a07a4

.field public static final bg_wakeup_notification_title:I = 0x7f0a07a3

.field public static final birthday_greet:I = 0x7f0a07ca

.field public static final block_carmode_with_talkback_on:I = 0x7f0a04ac

.field public static final block_change_sound_to_mute_vibrate:I = 0x7f0a026c

.field public static final bt_disconnect_toast:I = 0x7f0a0269

.field public static final button_back:I = 0x7f0a023a

.field public static final button_inbox:I = 0x7f0a023e

.field public static final button_label_back:I = 0x7f0a05ce

.field public static final button_label_inbox:I = 0x7f0a05d4

.field public static final button_label_location:I = 0x7f0a05cb

.field public static final button_label_logs:I = 0x7f0a05cc

.field public static final button_label_message:I = 0x7f0a05c9

.field public static final button_label_mic:I = 0x7f0a05c7

.field public static final button_label_music:I = 0x7f0a05ca

.field public static final button_label_music_just_album:I = 0x7f0a05d2

.field public static final button_label_music_just_artist:I = 0x7f0a05d1

.field public static final button_label_music_just_play:I = 0x7f0a05d0

.field public static final button_label_music_just_playlist:I = 0x7f0a05d3

.field public static final button_label_phone:I = 0x7f0a05c8

.field public static final button_label_suggestions:I = 0x7f0a05cd

.field public static final button_location_contact:I = 0x7f0a05f6

.field public static final button_location_destination:I = 0x7f0a05f9

.field public static final button_location_emulate:I = 0x7f0a0606

.field public static final button_location_group:I = 0x7f0a05f7

.field public static final button_location_group_expire:I = 0x7f0a05f8

.field public static final button_location_request_approved:I = 0x7f0a05fa

.field public static final button_location_share:I = 0x7f0a05cf

.field public static final button_logs:I = 0x7f0a023d

.field public static final button_map:I = 0x7f0a0342

.field public static final button_message_emulate:I = 0x7f0a0614

.field public static final button_message_inbox:I = 0x7f0a0835

.field public static final button_return_to_carmode_main:I = 0x7f0a045f

.field public static final button_safereader_multiple_messages:I = 0x7f0a0617

.field public static final button_safereader_read_message:I = 0x7f0a0618

.field public static final button_safereader_single_message:I = 0x7f0a0616

.field public static final button_suggestions:I = 0x7f0a023c

.field public static final button_suggestions_port:I = 0x7f0a023b

.field public static final bvoice_ui_focus_text:I = 0x7f0a0824

.field public static final bvra_unsupported_toast:I = 0x7f0a07e7

.field public static final call:I = 0x7f0a062c

.field public static final callback_btn:I = 0x7f0a062d

.field public static final cancel:I = 0x7f0a062e

.field public static final cancel_btn:I = 0x7f0a062f

.field public static final car_already_proceeding_command:I = 0x7f0a012b

.field public static final car_call_another_log:I = 0x7f0a0139

.field public static final car_call_no_last_log:I = 0x7f0a013a

.field public static final car_call_no_line_type:I = 0x7f0a0138

.field public static final car_call_no_log:I = 0x7f0a0135

.field public static final car_call_other_line_type:I = 0x7f0a0137

.field public static final car_call_other_line_type_help:I = 0x7f0a01c6

.field public static final car_call_redial_no_log:I = 0x7f0a01c7

.field public static final car_call_sms_log:I = 0x7f0a011e

.field public static final car_call_video_call:I = 0x7f0a013b

.field public static final car_call_video_call_slot:I = 0x7f0a013c

.field public static final car_calling:I = 0x7f0a0136

.field public static final car_calling_number:I = 0x7f0a01c5

.field public static final car_cancel:I = 0x7f0a01f6

.field public static final car_cancel_home:I = 0x7f0a01f7

.field public static final car_choose_list:I = 0x7f0a011f

.field public static final car_contact_multiple_full_match:I = 0x7f0a0121

.field public static final car_contact_multiple_partial_match:I = 0x7f0a0122

.field public static final car_contact_no_match:I = 0x7f0a0123

.field public static final car_drawer_alert:I = 0x7f0a01c1

.field public static final car_google_account:I = 0x7f0a01a3

.field public static final car_home_command:I = 0x7f0a01bc

.field public static final car_home_command_custom:I = 0x7f0a01bd

.field public static final car_home_commute_alert:I = 0x7f0a01c0

.field public static final car_home_drawer_alert:I = 0x7f0a01be

.field public static final car_home_more_alert:I = 0x7f0a01bf

.field public static final car_iux_ok:I = 0x7f0a0630

.field public static final car_iux_oops:I = 0x7f0a0631

.field public static final car_location_group_route:I = 0x7f0a016b

.field public static final car_location_navigate_my_place_no_registered_in_motion:I = 0x7f0a01dd

.field public static final car_location_navigate_name:I = 0x7f0a0163

.field public static final car_location_navigate_name_start:I = 0x7f0a0164

.field public static final car_location_navigate_place:I = 0x7f0a0161

.field public static final car_location_navigate_place_Confirm:I = 0x7f0a01d8

.field public static final car_location_navigate_place_start:I = 0x7f0a0162

.field public static final car_location_navigate_place_start_confirm:I = 0x7f0a01d9

.field public static final car_location_navigate_schedule_wrong_address:I = 0x7f0a01e4

.field public static final car_location_navigate_share_wrong_address:I = 0x7f0a01e5

.field public static final car_location_navigate_start_navigation:I = 0x7f0a01dc

.field public static final car_location_received_confirm:I = 0x7f0a015d

.field public static final car_location_received_prompt:I = 0x7f0a015c

.field public static final car_location_received_route:I = 0x7f0a015e

.field public static final car_location_request_confirm:I = 0x7f0a01e1

.field public static final car_location_request_confirmed:I = 0x7f0a01e3

.field public static final car_location_request_contact:I = 0x7f0a01e0

.field public static final car_location_request_sent_fail:I = 0x7f0a01e2

.field public static final car_location_say_destination:I = 0x7f0a018b

.field public static final car_location_search_gas_station_no_list:I = 0x7f0a01da

.field public static final car_location_search_parking_no_list:I = 0x7f0a01db

.field public static final car_location_search_petrol_station_no_list:I = 0x7f0a01f5

.field public static final car_location_share_comfirm:I = 0x7f0a0159

.field public static final car_location_share_confirm:I = 0x7f0a0166

.field public static final car_location_share_done:I = 0x7f0a015b

.field public static final car_location_share_done_2:I = 0x7f0a0167

.field public static final car_location_share_progress:I = 0x7f0a015a

.field public static final car_location_share_prompt:I = 0x7f0a0158

.field public static final car_location_share_request:I = 0x7f0a0169

.field public static final car_location_share_request_confirm:I = 0x7f0a016a

.field public static final car_location_share_request_whom:I = 0x7f0a0168

.field public static final car_location_share_sent:I = 0x7f0a01de

.field public static final car_location_share_sent_fail:I = 0x7f0a01df

.field public static final car_location_share_type:I = 0x7f0a0160

.field public static final car_location_share_whom:I = 0x7f0a0165

.field public static final car_location_where_togo:I = 0x7f0a015f

.field public static final car_message_message_contents:I = 0x7f0a01eb

.field public static final car_mic_in_use:I = 0x7f0a01e6

.field public static final car_mms_audio:I = 0x7f0a0179

.field public static final car_mms_contact:I = 0x7f0a017b

.field public static final car_mms_event:I = 0x7f0a017a

.field public static final car_mms_file:I = 0x7f0a017c

.field public static final car_mms_files:I = 0x7f0a017d

.field public static final car_mms_image:I = 0x7f0a0175

.field public static final car_mms_images:I = 0x7f0a0176

.field public static final car_mms_no_body:I = 0x7f0a01d4

.field public static final car_mms_prompt_message_1:I = 0x7f0a0172

.field public static final car_mms_prompt_message_2:I = 0x7f0a0173

.field public static final car_mms_prompt_message_3:I = 0x7f0a0174

.field public static final car_mms_video:I = 0x7f0a0177

.field public static final car_mms_videos:I = 0x7f0a0178

.field public static final car_moving_alert:I = 0x7f0a01a2

.field public static final car_moving_alert_add_my_place:I = 0x7f0a01fa

.field public static final car_moving_alert_google_account:I = 0x7f0a01fb

.field public static final car_msg_first_page:I = 0x7f0a0184

.field public static final car_msg_last_page:I = 0x7f0a0185

.field public static final car_msg_mms_no_text:I = 0x7f0a018c

.field public static final car_msg_no_text:I = 0x7f0a019a

.field public static final car_msg_only_attached:I = 0x7f0a019b

.field public static final car_msg_reply_call_next:I = 0x7f0a0183

.field public static final car_music_no_match_artist:I = 0x7f0a0154

.field public static final car_music_no_match_artist_shuffle:I = 0x7f0a0155

.field public static final car_music_no_match_foldername:I = 0x7f0a0156

.field public static final car_music_no_match_foldername_shuffle:I = 0x7f0a0157

.field public static final car_music_no_match_name:I = 0x7f0a0152

.field public static final car_music_no_match_name_shuffle:I = 0x7f0a0153

.field public static final car_music_no_music:I = 0x7f0a014e

.field public static final car_music_now_playing:I = 0x7f0a0150

.field public static final car_music_now_playing_one:I = 0x7f0a0151

.field public static final car_music_play:I = 0x7f0a01f9

.field public static final car_music_select:I = 0x7f0a014f

.field public static final car_music_select_2:I = 0x7f0a01d7

.field public static final car_network_error:I = 0x7f0a0120

.field public static final car_not_supported:I = 0x7f0a012a

.field public static final car_not_supported_text:I = 0x7f0a0461

.field public static final car_noti_alarm_no_title:I = 0x7f0a01fc

.field public static final car_noti_alarm_smart_alert:I = 0x7f0a01ad

.field public static final car_noti_alarm_time:I = 0x7f0a01a9

.field public static final car_noti_call_ended:I = 0x7f0a012d

.field public static final car_noti_call_incomming:I = 0x7f0a012c

.field public static final car_noti_call_incomming_number:I = 0x7f0a01a5

.field public static final car_noti_commute_duration_time_placename:I = 0x7f0a01bb

.field public static final car_noti_commute_placename:I = 0x7f0a01ba

.field public static final car_noti_consumables_approaching:I = 0x7f0a01b8

.field public static final car_noti_consumables_passed:I = 0x7f0a01b9

.field public static final car_noti_diagnostic:I = 0x7f0a01a8

.field public static final car_noti_group_location_sharing_changed_destination:I = 0x7f0a0134

.field public static final car_noti_location_request:I = 0x7f0a0131

.field public static final car_noti_location_request_alert:I = 0x7f0a01b5

.field public static final car_noti_location_request_reminder:I = 0x7f0a01b6

.field public static final car_noti_mms_attached:I = 0x7f0a0142

.field public static final car_noti_mms_incomming:I = 0x7f0a01af

.field public static final car_noti_mms_incomming_number:I = 0x7f0a01b0

.field public static final car_noti_mms_read:I = 0x7f0a01b3

.field public static final car_noti_mms_read_help:I = 0x7f0a01b4

.field public static final car_noti_my_location_shared:I = 0x7f0a0132

.field public static final car_noti_my_location_sharing_ignored:I = 0x7f0a0133

.field public static final car_noti_schedule_location_participants_time:I = 0x7f0a01ac

.field public static final car_noti_schedule_location_time:I = 0x7f0a01aa

.field public static final car_noti_schedule_location_with_all_day:I = 0x7f0a0205

.field public static final car_noti_schedule_no_title_with_all_date:I = 0x7f0a01ff

.field public static final car_noti_schedule_no_title_with_all_day:I = 0x7f0a01fe

.field public static final car_noti_schedule_no_title_with_time:I = 0x7f0a01fd

.field public static final car_noti_schedule_participants_time:I = 0x7f0a01ab

.field public static final car_noti_schedule_title_location_all_date:I = 0x7f0a0206

.field public static final car_noti_schedule_title_location_all_day:I = 0x7f0a0209

.field public static final car_noti_schedule_title_location_all_day_date:I = 0x7f0a020a

.field public static final car_noti_schedule_title_location_time:I = 0x7f0a0207

.field public static final car_noti_schedule_title_location_time_date:I = 0x7f0a0208

.field public static final car_noti_schedule_title_with_all_day:I = 0x7f0a0202

.field public static final car_noti_schedule_title_with_location:I = 0x7f0a0203

.field public static final car_noti_schedule_title_with_time:I = 0x7f0a0200

.field public static final car_noti_schedule_title_with_time_date:I = 0x7f0a0201

.field public static final car_noti_schedule_title_with_time_location:I = 0x7f0a0204

.field public static final car_noti_sms_incomming:I = 0x7f0a012e

.field public static final car_noti_sms_incomming_number:I = 0x7f0a01ae

.field public static final car_noti_sms_no_body:I = 0x7f0a0143

.field public static final car_noti_sms_read:I = 0x7f0a01b1

.field public static final car_noti_sms_read_help:I = 0x7f0a01b2

.field public static final car_noti_sms_sent:I = 0x7f0a012f

.field public static final car_noti_sms_text:I = 0x7f0a0141

.field public static final car_noti_somones_location_shared:I = 0x7f0a0130

.field public static final car_noti_somones_location_shared_reminder:I = 0x7f0a01b7

.field public static final car_noti_tts_alarm:I = 0x7f0a01ec

.field public static final car_noti_tts_schedule:I = 0x7f0a01ed

.field public static final car_noti_tts_schedule_location:I = 0x7f0a01ef

.field public static final car_noti_tts_schedule_title:I = 0x7f0a01ee

.field public static final car_noti_tts_schedule_today:I = 0x7f0a01f0

.field public static final car_noti_tts_schedule_tomorrow:I = 0x7f0a01f1

.field public static final car_noti_video_call_incomming:I = 0x7f0a01a6

.field public static final car_noti_video_call_incomming_number:I = 0x7f0a01a7

.field public static final car_phone_prompt_empty:I = 0x7f0a01c4

.field public static final car_phone_prompt_second:I = 0x7f0a01c2

.field public static final car_phone_prompt_third:I = 0x7f0a01c3

.field public static final car_recognition_failure:I = 0x7f0a0126

.field public static final car_recognition_failure_2:I = 0x7f0a0127

.field public static final car_recognition_failure_3:I = 0x7f0a0128

.field public static final car_recognition_failure_display:I = 0x7f0a0129

.field public static final car_sample_command:I = 0x7f0a019c

.field public static final car_server_error:I = 0x7f0a01a4

.field public static final car_sip_alert:I = 0x7f0a01a1

.field public static final car_sms_choose_message_tts:I = 0x7f0a0147

.field public static final car_sms_choose_person_send:I = 0x7f0a0148

.field public static final car_sms_days_ago:I = 0x7f0a016e

.field public static final car_sms_done:I = 0x7f0a0140

.field public static final car_sms_header_name_new:I = 0x7f0a018e

.field public static final car_sms_header_name_new_one:I = 0x7f0a01e9

.field public static final car_sms_header_name_no_new:I = 0x7f0a018d

.field public static final car_sms_header_name_no_new_one:I = 0x7f0a01e8

.field public static final car_sms_header_time:I = 0x7f0a018f

.field public static final car_sms_inbox:I = 0x7f0a01cb

.field public static final car_sms_inbox_enter:I = 0x7f0a01ca

.field public static final car_sms_inbox_not_found:I = 0x7f0a0190

.field public static final car_sms_message_body:I = 0x7f0a014a

.field public static final car_sms_message_body_again:I = 0x7f0a01ce

.field public static final car_sms_message_body_name_1:I = 0x7f0a01cc

.field public static final car_sms_message_body_number:I = 0x7f0a01cd

.field public static final car_sms_message_body_replace:I = 0x7f0a014b

.field public static final car_sms_message_readout:I = 0x7f0a014c

.field public static final car_sms_message_reply_call_back:I = 0x7f0a014d

.field public static final car_sms_new_message:I = 0x7f0a0180

.field public static final car_sms_no_message:I = 0x7f0a0149

.field public static final car_sms_no_new_text:I = 0x7f0a017e

.field public static final car_sms_one_new_message:I = 0x7f0a0181

.field public static final car_sms_prompt_last_message:I = 0x7f0a0146

.field public static final car_sms_prompt_message:I = 0x7f0a0144

.field public static final car_sms_prompt_message_first:I = 0x7f0a016f

.field public static final car_sms_prompt_message_last:I = 0x7f0a0171

.field public static final car_sms_prompt_message_next:I = 0x7f0a0170

.field public static final car_sms_prompt_next_message:I = 0x7f0a0145

.field public static final car_sms_read_message:I = 0x7f0a01cf

.field public static final car_sms_read_message_after:I = 0x7f0a01d1

.field public static final car_sms_read_message_name_time:I = 0x7f0a01d0

.field public static final car_sms_read_message_select_first_message:I = 0x7f0a01d5

.field public static final car_sms_read_message_select_last_message:I = 0x7f0a01d6

.field public static final car_sms_read_no_contact:I = 0x7f0a01d3

.field public static final car_sms_replace_tts:I = 0x7f0a0186

.field public static final car_sms_send_confirm:I = 0x7f0a013d

.field public static final car_sms_send_no_contact:I = 0x7f0a01d2

.field public static final car_sms_sending:I = 0x7f0a013e

.field public static final car_sms_sent:I = 0x7f0a013f

.field public static final car_sms_text_who_1:I = 0x7f0a01c8

.field public static final car_sms_text_who_2:I = 0x7f0a01c9

.field public static final car_sms_to_whom_should_i_send_a_message:I = 0x7f0a019d

.field public static final car_sms_today:I = 0x7f0a016d

.field public static final car_sms_yesterday:I = 0x7f0a016c

.field public static final car_spot_text:I = 0x7f0a05bb

.field public static final car_stop_navigation:I = 0x7f0a01f8

.field public static final car_tts_KEYWORD_SPOT_ON_DEMAND:I = 0x7f0a05c2

.field public static final car_tutorial_step2:I = 0x7f0a019e

.field public static final car_tutorial_step3:I = 0x7f0a019f

.field public static final car_tutorial_step5:I = 0x7f0a01a0

.field public static final car_tutorial_tts_01:I = 0x7f0a0187

.field public static final car_tutorial_tts_03:I = 0x7f0a0188

.field public static final car_tutorial_tts_04:I = 0x7f0a0189

.field public static final car_tutorial_tts_hi_galaxy:I = 0x7f0a018a

.field public static final car_vehicle_moving_not_supported:I = 0x7f0a0199

.field public static final car_voice_network_error:I = 0x7f0a0124

.field public static final car_voice_server_error:I = 0x7f0a0125

.field public static final card_label_for_daily_commute_arrival_time:I = 0x7f0a0485

.field public static final card_label_for_daily_commute_driving_duration:I = 0x7f0a0484

.field public static final card_label_for_daily_commute_traffic_information:I = 0x7f0a0486

.field public static final card_label_for_gas_station:I = 0x7f0a047e

.field public static final card_label_for_location_service_unavailable:I = 0x7f0a0481

.field public static final card_label_for_network_unavailable:I = 0x7f0a0480

.field public static final card_label_for_parking_lot:I = 0x7f0a047c

.field public static final card_label_for_service_unavailable:I = 0x7f0a047f

.field public static final card_label_for_top_story:I = 0x7f0a047a

.field public static final card_text_for_daily_commute_to_home:I = 0x7f0a0482

.field public static final card_text_for_daily_commute_to_work:I = 0x7f0a0483

.field public static final card_text_for_gas_station:I = 0x7f0a047d

.field public static final card_text_for_parking_lot:I = 0x7f0a047b

.field public static final card_text_for_top_story:I = 0x7f0a0479

.field public static final change_now:I = 0x7f0a0632

.field public static final character_max_length_reached:I = 0x7f0a082d

.field public static final chatbot_could_not_load_news:I = 0x7f0a07d3

.field public static final chatbot_just_read_first_news:I = 0x7f0a07d4

.field public static final chatbot_just_read_last_news:I = 0x7f0a07d5

.field public static final chatbot_next_news:I = 0x7f0a07d1

.field public static final chatbot_prev_news:I = 0x7f0a07d2

.field public static final chatbot_read_news:I = 0x7f0a07d0

.field public static final chatbot_stop_news:I = 0x7f0a07d6

.field public static final checkEventTitleTimeDetail:I = 0x7f0a0633

.field public static final check_phone_event_main:I = 0x7f0a076c

.field public static final close_button:I = 0x7f0a077a

.field public static final close_carmode_with_airplane_mode:I = 0x7f0a04ae

.field public static final close_carmode_with_talkback_on:I = 0x7f0a04ad

.field public static final comma:I = 0x7f0a052e

.field public static final common_google_play_services_enable_button:I = 0x7f0a0006

.field public static final common_google_play_services_enable_text:I = 0x7f0a0005

.field public static final common_google_play_services_enable_title:I = 0x7f0a0004

.field public static final common_google_play_services_install_button:I = 0x7f0a0003

.field public static final common_google_play_services_install_text_phone:I = 0x7f0a0001

.field public static final common_google_play_services_install_text_tablet:I = 0x7f0a0002

.field public static final common_google_play_services_install_title:I = 0x7f0a0000

.field public static final common_google_play_services_invalid_account_text:I = 0x7f0a000c

.field public static final common_google_play_services_invalid_account_title:I = 0x7f0a000b

.field public static final common_google_play_services_network_error_text:I = 0x7f0a000a

.field public static final common_google_play_services_network_error_title:I = 0x7f0a0009

.field public static final common_google_play_services_unknown_issue:I = 0x7f0a000d

.field public static final common_google_play_services_unsupported_date_text:I = 0x7f0a0010

.field public static final common_google_play_services_unsupported_text:I = 0x7f0a000f

.field public static final common_google_play_services_unsupported_title:I = 0x7f0a000e

.field public static final common_google_play_services_update_button:I = 0x7f0a0011

.field public static final common_google_play_services_update_text:I = 0x7f0a0008

.field public static final common_google_play_services_update_title:I = 0x7f0a0007

.field public static final common_signin_button_text:I = 0x7f0a0012

.field public static final common_signin_button_text_long:I = 0x7f0a0013

.field public static final contact_name_unknown:I = 0x7f0a03a4

.field public static final core_address_book_is_empty:I = 0x7f0a00c5

.field public static final core_alarm_set:I = 0x7f0a001b

.field public static final core_alert_message:I = 0x7f0a001c

.field public static final core_and:I = 0x7f0a0113

.field public static final core_answer_prompt:I = 0x7f0a001d

.field public static final core_bluetooth_setting_change_on_error:I = 0x7f0a00db

.field public static final core_call_to_whom:I = 0x7f0a00bb

.field public static final core_call_who_should_i_call:I = 0x7f0a00ba

.field public static final core_car_event_save_confirm:I = 0x7f0a001e

.field public static final core_car_event_saved:I = 0x7f0a001f

.field public static final core_car_redial_confirm_contact:I = 0x7f0a0114

.field public static final core_car_redial_confirm_number:I = 0x7f0a0115

.field public static final core_car_safereader_hidden_message_body:I = 0x7f0a0020

.field public static final core_car_sms_error_help_tts:I = 0x7f0a0021

.field public static final core_car_sms_error_sending:I = 0x7f0a0022

.field public static final core_car_sms_message_empty_tts:I = 0x7f0a0023

.field public static final core_car_sms_send_confirm:I = 0x7f0a0024

.field public static final core_car_sms_speak_msg_tts:I = 0x7f0a0025

.field public static final core_car_sms_text_who:I = 0x7f0a0026

.field public static final core_car_social_final_prompt:I = 0x7f0a0027

.field public static final core_car_social_prompt_ex1:I = 0x7f0a0028

.field public static final core_car_social_service_prompt:I = 0x7f0a0029

.field public static final core_car_social_status_prompt:I = 0x7f0a002a

.field public static final core_car_task_save_cancel_update_prompt:I = 0x7f0a002b

.field public static final core_car_tts_ALBUM_PROMPT_DEMAND:I = 0x7f0a002c

.field public static final core_car_tts_ARTIST_PROMPT_DEMAND:I = 0x7f0a002d

.field public static final core_car_tts_EVENT_SAY_TITLE:I = 0x7f0a002e

.field public static final core_car_tts_GOTO_URL:I = 0x7f0a002f

.field public static final core_car_tts_LAUNCHAPP_PROMPT_DEMAND:I = 0x7f0a0030

.field public static final core_car_tts_MAP_OF:I = 0x7f0a0031

.field public static final core_car_tts_MEMO_SAVED:I = 0x7f0a00be

.field public static final core_car_tts_MUSIC_PROMPT_DEMAND:I = 0x7f0a0032

.field public static final core_car_tts_NAVIGATE_TO:I = 0x7f0a0033

.field public static final core_car_tts_NO_ALBUMMATCH_DEMAND:I = 0x7f0a0034

.field public static final core_car_tts_NO_ANYMATCH_DEMAND:I = 0x7f0a0117

.field public static final core_car_tts_NO_APPMATCH_DEMAND:I = 0x7f0a0035

.field public static final core_car_tts_NO_ARTISTMATCH_DEMAND:I = 0x7f0a0037

.field public static final core_car_tts_NO_MATCH_DEMAND_CALL:I = 0x7f0a0038

.field public static final core_car_tts_NO_MATCH_DEMAND_MESSAGE:I = 0x7f0a0039

.field public static final core_car_tts_NO_MUSICMATCH_DEMAND:I = 0x7f0a003a

.field public static final core_car_tts_NO_PLAYLISTMATCH_DEMAND:I = 0x7f0a003b

.field public static final core_car_tts_NO_SPOKEN_APPMATCH_DEMAND:I = 0x7f0a0036

.field public static final core_car_tts_NO_SUPPORT_IN_MUSIC:I = 0x7f0a01e7

.field public static final core_car_tts_NO_TITLEMATCH_DEMAND:I = 0x7f0a003c

.field public static final core_car_tts_PLAYPLAYLIST_PROMPT_DEMAND:I = 0x7f0a003d

.field public static final core_car_tts_SAFEREADER_MULTIPLE_NEW_MESSAGES:I = 0x7f0a003e

.field public static final core_car_tts_SAFEREADER_MULTI_SMS_FROM_INTRO:I = 0x7f0a00f8

.field public static final core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN:I = 0x7f0a0041

.field public static final core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN_NOCALL:I = 0x7f0a00d6

.field public static final core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN:I = 0x7f0a0042

.field public static final core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN_NOCALL:I = 0x7f0a00d7

.field public static final core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN:I = 0x7f0a003f

.field public static final core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN_NOCALL_SHOWN:I = 0x7f0a00d4

.field public static final core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN_NOCALL_SPOKEN:I = 0x7f0a00d5

.field public static final core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SPOKEN:I = 0x7f0a0040

.field public static final core_car_tts_SAFEREADER_NEW_MESSAGE_FROM_BODY_NO_PROMPT:I = 0x7f0a0043

.field public static final core_car_tts_SAFEREADER_NEW_MSG_SHOWN:I = 0x7f0a0044

.field public static final core_car_tts_SAFEREADER_NEW_MSG_SPOKEN:I = 0x7f0a0045

.field public static final core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_NOCALL_SHOWN:I = 0x7f0a00d8

.field public static final core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_NOCALL_SPOKEN:I = 0x7f0a00d9

.field public static final core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SHOWN:I = 0x7f0a0046

.field public static final core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SPOKEN:I = 0x7f0a0047

.field public static final core_car_tts_SAFEREADER_NEW_SMS_FROM:I = 0x7f0a0048

.field public static final core_car_tts_SAFEREADER_NEW_SMS_FROM_INTRO:I = 0x7f0a0049

.field public static final core_car_tts_SAFEREADER_NO_REPLY:I = 0x7f0a00dc

.field public static final core_car_tts_SCHEDULE_EVENTS:I = 0x7f0a004a

.field public static final core_car_tts_SCHEDULE_NO_EVENTS:I = 0x7f0a004b

.field public static final core_car_tts_SMS_SENT_CONFIRM_DEMAND:I = 0x7f0a004c

.field public static final core_car_tts_TASK_CANCELLED:I = 0x7f0a004d

.field public static final core_car_tts_TASK_SAY_TITLE:I = 0x7f0a004e

.field public static final core_car_tts_TITLE_PROMPT_DEMAND:I = 0x7f0a004f

.field public static final core_car_tts_TITLE_PROMPT_DEMAND2:I = 0x7f0a0050

.field public static final core_car_tts_VD_CALLING_CONFIRM_DEMAND:I = 0x7f0a0051

.field public static final core_car_tts_VD_MULTIPLE_CONTACTS_DEMAND:I = 0x7f0a0052

.field public static final core_car_tts_VD_MULTIPLE_TYPES_DEMAND:I = 0x7f0a0053

.field public static final core_car_tts_WHICH_CONTACT_DEMAND:I = 0x7f0a0054

.field public static final core_car_util_loading:I = 0x7f0a0055

.field public static final core_checkEventJapaneseMoreTenYouHave:I = 0x7f0a0059

.field public static final core_checkEventOneYouHave:I = 0x7f0a0058

.field public static final core_checkEventRussianTwoThreeFourYouHave:I = 0x7f0a0057

.field public static final core_checkEventTitleTimeDetailBrief:I = 0x7f0a0056

.field public static final core_checkEventYouHave:I = 0x7f0a005a

.field public static final core_checkEventYouHaveOnDate:I = 0x7f0a005b

.field public static final core_checkEventYouHaveToday:I = 0x7f0a005c

.field public static final core_colon:I = 0x7f0a00c9

.field public static final core_comma:I = 0x7f0a00c8

.field public static final core_contact_address:I = 0x7f0a011c

.field public static final core_contact_address_not_found:I = 0x7f0a005f

.field public static final core_contact_birthday_not_found:I = 0x7f0a005e

.field public static final core_contact_email:I = 0x7f0a011b

.field public static final core_contact_email_not_found:I = 0x7f0a005d

.field public static final core_contact_phone_number:I = 0x7f0a011a

.field public static final core_contacts_no_match_openquote:I = 0x7f0a0060

.field public static final core_cradle_date:I = 0x7f0a0116

.field public static final core_current_location:I = 0x7f0a00d3

.field public static final core_default_alarm_title:I = 0x7f0a0061

.field public static final core_dot:I = 0x7f0a00c7

.field public static final core_ellipsis:I = 0x7f0a00cb

.field public static final core_error:I = 0x7f0a00e2

.field public static final core_format_time_AM:I = 0x7f0a00e0

.field public static final core_format_time_PM:I = 0x7f0a00e1

.field public static final core_local_search_blank_request_message_shown:I = 0x7f0a00c3

.field public static final core_local_search_blank_request_message_spoken:I = 0x7f0a00c4

.field public static final core_localsearch:I = 0x7f0a0062

.field public static final core_localsearch_bad_location:I = 0x7f0a020b

.field public static final core_localsearch_bad_response:I = 0x7f0a020c

.field public static final core_localsearch_default_location:I = 0x7f0a0198

.field public static final core_localsearch_no_location:I = 0x7f0a0063

.field public static final core_localsearch_no_results:I = 0x7f0a020d

.field public static final core_localsearch_provider_dianping:I = 0x7f0a0191

.field public static final core_memo_confirm_delete:I = 0x7f0a0064

.field public static final core_memo_multiple_found:I = 0x7f0a0065

.field public static final core_memo_not_saved:I = 0x7f0a0066

.field public static final core_message_none_from_sender:I = 0x7f0a00e5

.field public static final core_message_none_from_sender_spoken:I = 0x7f0a00e6

.field public static final core_message_reading_multi_from_sender:I = 0x7f0a00d2

.field public static final core_message_reading_none:I = 0x7f0a00d0

.field public static final core_message_reading_none_spoken:I = 0x7f0a00e4

.field public static final core_message_reading_preface:I = 0x7f0a00cf

.field public static final core_message_reading_single_from_sender:I = 0x7f0a00d1

.field public static final core_message_sending:I = 0x7f0a00cd

.field public static final core_message_sending_readout:I = 0x7f0a00ce

.field public static final core_mic_in_use:I = 0x7f0a00bd

.field public static final core_multiple_applications:I = 0x7f0a0067

.field public static final core_multiple_contacts:I = 0x7f0a0068

.field public static final core_multiple_contacts2:I = 0x7f0a0069

.field public static final core_multiple_phone_numbers:I = 0x7f0a006a

.field public static final core_multiple_phone_numbers2:I = 0x7f0a006b

.field public static final core_nav_home_prompt_shown:I = 0x7f0a006c

.field public static final core_nav_home_prompt_spoken:I = 0x7f0a006d

.field public static final core_navigate_home:I = 0x7f0a006e

.field public static final core_network_error:I = 0x7f0a00c6

.field public static final core_no_call_log:I = 0x7f0a006f

.field public static final core_no_memo_saved:I = 0x7f0a0070

.field public static final core_no_memo_saved_about:I = 0x7f0a0071

.field public static final core_no_more_messages_regular:I = 0x7f0a00e8

.field public static final core_no_more_messages_spoken:I = 0x7f0a00e9

.field public static final core_no_more_messages_verbose:I = 0x7f0a00e7

.field public static final core_not_detected_current_location:I = 0x7f0a00de

.field public static final core_opening_app:I = 0x7f0a00cc

.field public static final core_permission_internet_error:I = 0x7f0a00e3

.field public static final core_phone_in_use:I = 0x7f0a00bc

.field public static final core_playing_music:I = 0x7f0a00dd

.field public static final core_qa_more:I = 0x7f0a0072

.field public static final core_qa_tts_NO_ANS_WEB_SEARCH:I = 0x7f0a0073

.field public static final core_qzone_dialog_title:I = 0x7f0a0197

.field public static final core_readout_multi_message_nocall_shown:I = 0x7f0a0110

.field public static final core_readout_multi_message_nocall_spoken:I = 0x7f0a0111

.field public static final core_readout_multi_message_nocall_withoutaskingmsg_spoken:I = 0x7f0a0112

.field public static final core_readout_multi_message_overflow:I = 0x7f0a00fc

.field public static final core_readout_multi_message_shown:I = 0x7f0a00f9

.field public static final core_readout_multi_message_spoken:I = 0x7f0a00fa

.field public static final core_readout_multi_message_withoutaskingmsg_spoken:I = 0x7f0a00fb

.field public static final core_readout_multi_sender:I = 0x7f0a00fd

.field public static final core_readout_multi_sender_command_spoken:I = 0x7f0a0100

.field public static final core_readout_multi_sender_dm_overflow:I = 0x7f0a0101

.field public static final core_readout_multi_sender_info_verbose_overflow_spoken:I = 0x7f0a00ff

.field public static final core_readout_multi_sender_info_verbose_spoken:I = 0x7f0a00fe

.field public static final core_readout_multi_sender_overflow:I = 0x7f0a0104

.field public static final core_readout_multi_sender_overflow_spoken:I = 0x7f0a0102

.field public static final core_readout_multi_sender_overflow_withoutaskingmsg_spoken:I = 0x7f0a0103

.field public static final core_readout_new_messages_for_russian_1:I = 0x7f0a0193

.field public static final core_readout_new_messages_for_russian_2:I = 0x7f0a0194

.field public static final core_readout_new_messages_for_russian_3:I = 0x7f0a0195

.field public static final core_readout_no_match_found_shown:I = 0x7f0a00ea

.field public static final core_readout_no_match_found_spoken:I = 0x7f0a00eb

.field public static final core_readout_single_message:I = 0x7f0a00f5

.field public static final core_readout_single_message_initial_hidden:I = 0x7f0a00ec

.field public static final core_readout_single_message_initial_hidden_nocall:I = 0x7f0a0105

.field public static final core_readout_single_message_initial_hidden_nocall_spoken:I = 0x7f0a0106

.field public static final core_readout_single_message_initial_hidden_nocall_withoutaskingmsg_spoken:I = 0x7f0a0107

.field public static final core_readout_single_message_initial_hidden_spoken:I = 0x7f0a00ed

.field public static final core_readout_single_message_initial_hidden_withoutaskingmsg_spoken:I = 0x7f0a00ee

.field public static final core_readout_single_message_initial_nocall_spoken:I = 0x7f0a0108

.field public static final core_readout_single_message_initial_nocall_withoutaskingmsg_spoken:I = 0x7f0a0109

.field public static final core_readout_single_message_initial_shown:I = 0x7f0a00ef

.field public static final core_readout_single_message_initial_spoken:I = 0x7f0a00f0

.field public static final core_readout_single_message_initial_withoutaskingmsg_spoken:I = 0x7f0a00f1

.field public static final core_readout_single_message_next:I = 0x7f0a00f2

.field public static final core_readout_single_message_next_nocall:I = 0x7f0a010a

.field public static final core_readout_single_message_next_nocall_spoken:I = 0x7f0a010b

.field public static final core_readout_single_message_next_nocall_withoutaskingmsg_spoken:I = 0x7f0a010c

.field public static final core_readout_single_message_next_spoken:I = 0x7f0a00f3

.field public static final core_readout_single_message_next_withoutaskingmsg_spoken:I = 0x7f0a00f4

.field public static final core_readout_single_message_nocall:I = 0x7f0a010d

.field public static final core_readout_single_message_nocall_spoken:I = 0x7f0a010e

.field public static final core_readout_single_message_nocall_withoutaskingmsg_spoken:I = 0x7f0a010f

.field public static final core_readout_single_message_spoken:I = 0x7f0a00f6

.field public static final core_readout_single_message_withoutaskingmsg_spoken:I = 0x7f0a00f7

.field public static final core_redial:I = 0x7f0a0079

.field public static final core_safe_reader_default_error:I = 0x7f0a007a

.field public static final core_safereader_enabled:I = 0x7f0a007b

.field public static final core_safereader_new_sms_from:I = 0x7f0a007c

.field public static final core_safereader_notif_reading_ticker:I = 0x7f0a007d

.field public static final core_safereader_notif_reading_title:I = 0x7f0a007e

.field public static final core_safereader_notif_title:I = 0x7f0a007f

.field public static final core_safereader_subject:I = 0x7f0a0080

.field public static final core_say_command:I = 0x7f0a0081

.field public static final core_schedule_all_day:I = 0x7f0a0082

.field public static final core_schedule_to:I = 0x7f0a0083

.field public static final core_search_web_label_button:I = 0x7f0a0084

.field public static final core_semicolon:I = 0x7f0a00ca

.field public static final core_single_contact:I = 0x7f0a0085

.field public static final core_social_api_err_auth1:I = 0x7f0a0086

.field public static final core_social_api_err_auth2:I = 0x7f0a0087

.field public static final core_social_api_error:I = 0x7f0a0088

.field public static final core_social_api_qzone_err_login:I = 0x7f0a008f

.field public static final core_social_api_qzone_error:I = 0x7f0a0090

.field public static final core_social_api_qzone_update_error:I = 0x7f0a0091

.field public static final core_social_api_qzone_update_error_publish:I = 0x7f0a0092

.field public static final core_social_api_twitter_err_login:I = 0x7f0a0098

.field public static final core_social_api_twitter_error:I = 0x7f0a0099

.field public static final core_social_api_twitter_update_error:I = 0x7f0a009a

.field public static final core_social_api_weibo_err_login:I = 0x7f0a008a

.field public static final core_social_api_weibo_error:I = 0x7f0a0089

.field public static final core_social_api_weibo_update_error:I = 0x7f0a008b

.field public static final core_social_err_msg1:I = 0x7f0a009b

.field public static final core_social_err_msg2:I = 0x7f0a009c

.field public static final core_social_login_to_facebook_msg:I = 0x7f0a009d

.field public static final core_social_login_to_network_msg:I = 0x7f0a009e

.field public static final core_social_login_to_qzone_msg:I = 0x7f0a0093

.field public static final core_social_login_to_twitter_msg:I = 0x7f0a009f

.field public static final core_social_login_to_weibo_msg:I = 0x7f0a008c

.field public static final core_social_logout_facebook:I = 0x7f0a00a0

.field public static final core_social_logout_facebook_msg:I = 0x7f0a00a1

.field public static final core_social_logout_qzone:I = 0x7f0a0094

.field public static final core_social_logout_qzone_msg:I = 0x7f0a0097

.field public static final core_social_logout_twitter:I = 0x7f0a00a2

.field public static final core_social_logout_twitter_msg:I = 0x7f0a00a3

.field public static final core_social_logout_weibo:I = 0x7f0a008d

.field public static final core_social_logout_weibo_msg:I = 0x7f0a008e

.field public static final core_social_no_network:I = 0x7f0a00a4

.field public static final core_social_no_status:I = 0x7f0a00a5

.field public static final core_social_status_updated:I = 0x7f0a00a6

.field public static final core_social_too_long:I = 0x7f0a00a7

.field public static final core_social_update_failed:I = 0x7f0a00a8

.field public static final core_space:I = 0x7f0a0192

.field public static final core_time_at_present:I = 0x7f0a011d

.field public static final core_to_whom_text:I = 0x7f0a0182

.field public static final core_today:I = 0x7f0a00b3

.field public static final core_tomorrow:I = 0x7f0a00b4

.field public static final core_tts_NO_ANS_GOOGLE_NOW_SEARCH:I = 0x7f0a0119

.field public static final core_tts_NO_ANS_WEB_SEARCH:I = 0x7f0a0074

.field public static final core_tts_NO_ANS_WEB_SEARCH_1:I = 0x7f0a0075

.field public static final core_tts_NO_ANS_WEB_SEARCH_2:I = 0x7f0a0076

.field public static final core_tts_NO_ANS_WEB_SEARCH_3:I = 0x7f0a0077

.field public static final core_tts_NO_ANS_WEB_SEARCH_4:I = 0x7f0a0078

.field public static final core_tts_local_fallback_engine_name:I = 0x7f0a051a

.field public static final core_tts_local_required_engine_name:I = 0x7f0a0519

.field public static final core_unknown:I = 0x7f0a00df

.field public static final core_voice_recognition_service_not_thru_iux_error:I = 0x7f0a00a9

.field public static final core_voicedial_call_name:I = 0x7f0a00aa

.field public static final core_voicedial_call_name_type:I = 0x7f0a00ab

.field public static final core_voicevideodial_call_name:I = 0x7f0a00ac

.field public static final core_voicevideodial_call_name_type:I = 0x7f0a00ad

.field public static final core_wcis_social_facebook:I = 0x7f0a00ae

.field public static final core_wcis_social_qzone:I = 0x7f0a0096

.field public static final core_wcis_social_twitter:I = 0x7f0a00af

.field public static final core_wcis_social_weibo:I = 0x7f0a0095

.field public static final core_weather_current:I = 0x7f0a00b0

.field public static final core_weather_date_display:I = 0x7f0a00b1

.field public static final core_weather_date_tts:I = 0x7f0a00b2

.field public static final core_weather_default_location:I = 0x7f0a0118

.field public static final core_weather_general:I = 0x7f0a00b5

.field public static final core_weather_no_results:I = 0x7f0a00b6

.field public static final core_weather_plus_seven:I = 0x7f0a00b7

.field public static final core_weather_with_locatin:I = 0x7f0a00b8

.field public static final core_weibo_dialog_title:I = 0x7f0a0196

.field public static final core_what_is_the_weather_date_var:I = 0x7f0a00c2

.field public static final core_what_is_the_weather_date_var_location_var:I = 0x7f0a00bf

.field public static final core_what_is_the_weather_today:I = 0x7f0a00c0

.field public static final core_what_is_the_weather_today_location_var:I = 0x7f0a00c1

.field public static final core_who_do_you_want:I = 0x7f0a017f

.field public static final core_who_would_you_like_to_call:I = 0x7f0a00b9

.field public static final core_wifi_setting_change_on_error:I = 0x7f0a00da

.field public static final cradle_home_date:I = 0x7f0a06fd

.field public static final cradle_month:I = 0x7f0a075b

.field public static final cradle_year:I = 0x7f0a075a

.field public static final custom_wake_up_info_body:I = 0x7f0a0634

.field public static final custom_wake_up_info_title:I = 0x7f0a0635

.field public static final custom_wakeup_disabled:I = 0x7f0a0757

.field public static final custom_wakeup_enabled:I = 0x7f0a0756

.field public static final daily_commute_hour:I = 0x7f0a0220

.field public static final daily_commute_min:I = 0x7f0a0221

.field public static final daily_commute_notification_message:I = 0x7f0a049d

.field public static final debug_settings:I = 0x7f0a0860

.field public static final debug_settings_show_enabled:I = 0x7f0a078c

.field public static final delete:I = 0x7f0a0636

.field public static final destination:I = 0x7f0a07a6

.field public static final destination_hint:I = 0x7f0a07a8

.field public static final device_id:I = 0x7f0a07e4

.field public static final dialog_set_myplace_message:I = 0x7f0a04f0

.field public static final dialog_set_myplace_message_home_and_work:I = 0x7f0a0341

.field public static final dialog_set_myplace_message_home_to_office:I = 0x7f0a033f

.field public static final dialog_set_myplace_message_office_to_home:I = 0x7f0a0340

.field public static final dialog_set_myplace_title:I = 0x7f0a033e

.field public static final did_you_know:I = 0x7f0a0637

.field public static final did_you_know_driving_mode:I = 0x7f0a0638

.field public static final did_you_know_driving_mode_no_call:I = 0x7f0a0639

.field public static final did_you_know_local_listings:I = 0x7f0a0738

.field public static final did_you_know_message:I = 0x7f0a063a

.field public static final did_you_know_music:I = 0x7f0a05ac

.field public static final did_you_know_music_in_car:I = 0x7f0a05ad

.field public static final did_you_know_navigate:I = 0x7f0a063b

.field public static final did_you_know_news:I = 0x7f0a05b1

.field public static final do_not_display_again:I = 0x7f0a0702

.field public static final done:I = 0x7f0a0596

.field public static final downloadable_dialog_popup_text:I = 0x7f0a0831

.field public static final drawer_menu_commute_forecasts:I = 0x7f0a0289

.field public static final drawer_menu_consumables:I = 0x7f0a0290

.field public static final drawer_menu_diagnostic:I = 0x7f0a028f

.field public static final drawer_menu_drive_behavior:I = 0x7f0a028e

.field public static final drawer_menu_find_my_car:I = 0x7f0a028d

.field public static final drawer_menu_home:I = 0x7f0a0288

.field public static final drawer_menu_home_to_office:I = 0x7f0a028b

.field public static final drawer_menu_management:I = 0x7f0a028c

.field public static final drawer_menu_office_to_home:I = 0x7f0a028a

.field public static final drive_greeting_processing:I = 0x7f0a07ce

.field public static final drive_help_greeting_call_land:I = 0x7f0a05ae

.field public static final drive_help_greeting_message_land:I = 0x7f0a05af

.field public static final drive_help_greeting_navi_land:I = 0x7f0a05b0

.field public static final drivine_mode_news_updated:I = 0x7f0a07da

.field public static final driving_call_reject_message:I = 0x7f0a07c5

.field public static final driving_help_wakeup_custom_tts:I = 0x7f0a07ea

.field public static final driving_help_wakeup_default_tts:I = 0x7f0a07e9

.field public static final driving_listening_under_text:I = 0x7f0a07f1

.field public static final driving_mode_alert_notification_body:I = 0x7f0a07f3

.field public static final driving_mode_already_disabled:I = 0x7f0a0799

.field public static final driving_mode_already_disabled_tts:I = 0x7f0a079a

.field public static final driving_mode_already_enabled:I = 0x7f0a0797

.field public static final driving_mode_already_enabled_tts:I = 0x7f0a0798

.field public static final driving_mode_description:I = 0x7f0a07b4

.field public static final driving_mode_message_body_hidden:I = 0x7f0a07de

.field public static final driving_mode_news_headlines:I = 0x7f0a07d9

.field public static final driving_mode_notification_bar_summary:I = 0x7f0a07cf

.field public static final driving_mode_off_root_display:I = 0x7f0a07f7

.field public static final driving_mode_off_root_tts:I = 0x7f0a07f8

.field public static final driving_mode_off_suffix_display:I = 0x7f0a07fb

.field public static final driving_mode_off_suffix_tts:I = 0x7f0a07fc

.field public static final driving_mode_on_root_display:I = 0x7f0a07f9

.field public static final driving_mode_on_root_tts:I = 0x7f0a07fa

.field public static final driving_mode_on_suffix_display:I = 0x7f0a07fd

.field public static final driving_mode_on_suffix_tts:I = 0x7f0a07fe

.field public static final driving_mode_settings_title:I = 0x7f0a07b2

.field public static final driving_mode_title:I = 0x7f0a07b3

.field public static final driving_tap_the_screen:I = 0x7f0a07e5

.field public static final dueto:I = 0x7f0a063c

.field public static final dummy_button:I = 0x7f0a022f

.field public static final dummy_content:I = 0x7f0a0230

.field public static final email_type_home:I = 0x7f0a05c6

.field public static final error_createuserprofile:I = 0x7f0a033c

.field public static final error_failed_to_user_location:I = 0x7f0a030e

.field public static final error_label_ocurred:I = 0x7f0a030b

.field public static final error_message_2depth_pressed:I = 0x7f0a032c

.field public static final error_message_drivelink_music_unknown:I = 0x7f0a0325

.field public static final error_message_fail_save_location:I = 0x7f0a032b

.field public static final error_message_failed_accept_group_shared:I = 0x7f0a0328

.field public static final error_message_failed_get_current_location:I = 0x7f0a032f

.field public static final error_message_failed_get_group_details:I = 0x7f0a0327

.field public static final error_message_failed_get_group_shared:I = 0x7f0a0326

.field public static final error_message_failed_get_location_shared:I = 0x7f0a0329

.field public static final error_message_google_maps_not_installed:I = 0x7f0a0324

.field public static final error_message_gps_not_enabled:I = 0x7f0a032d

.field public static final error_message_invalid_location_shared:I = 0x7f0a032a

.field public static final error_message_invalid_values_save_instance_state:I = 0x7f0a032e

.field public static final error_message_need_location:I = 0x7f0a0330

.field public static final error_message_no_data_network:I = 0x7f0a0321

.field public static final error_message_no_network:I = 0x7f0a0322

.field public static final error_message_slow_connection:I = 0x7f0a0323

.field public static final error_mic_state:I = 0x7f0a0296

.field public static final error_no_gas_station:I = 0x7f0a030c

.field public static final error_no_gas_station_tts:I = 0x7f0a030d

.field public static final error_no_parking_lots:I = 0x7f0a0308

.field public static final error_no_parking_lots_tts:I = 0x7f0a0309

.field public static final examples:I = 0x7f0a063e

.field public static final expired_group_share:I = 0x7f0a0489

.field public static final facebooksso_google_account:I = 0x7f0a07ab

.field public static final featured:I = 0x7f0a063f

.field public static final find_route:I = 0x7f0a0768

.field public static final finish:I = 0x7f0a0640

.field public static final fmc_k_update:I = 0x7f0a048f

.field public static final fmc_no_address:I = 0x7f0a021b

.field public static final fmc_save_btn_text:I = 0x7f0a0217

.field public static final fmc_title_activity:I = 0x7f0a0216

.field public static final force_close:I = 0x7f0a0784

.field public static final fri1:I = 0x7f0a0736

.field public static final full_fir:I = 0x7f0a072f

.field public static final full_mon:I = 0x7f0a072b

.field public static final full_sat:I = 0x7f0a0730

.field public static final full_sun:I = 0x7f0a072a

.field public static final full_thu:I = 0x7f0a072e

.field public static final full_tue:I = 0x7f0a072c

.field public static final full_wed:I = 0x7f0a072d

.field public static final function_item_check_schedule:I = 0x7f0a0727

.field public static final google_btn:I = 0x7f0a04a5

.field public static final google_launch_popup_text:I = 0x7f0a04a4

.field public static final gps_could_not_be_activated:I = 0x7f0a0848

.field public static final gps_enable_cancel:I = 0x7f0a04a6

.field public static final gps_enable_message:I = 0x7f0a04a9

.field public static final gps_enable_title:I = 0x7f0a04a8

.field public static final gps_enable_turn_on:I = 0x7f0a04a7

.field public static final greeting_tts_checking_1:I = 0x7f0a07f2

.field public static final greeting_tts_checking_2:I = 0x7f0a07f4

.field public static final greeting_tts_dawn_1:I = 0x7f0a080e

.field public static final greeting_tts_daytime_1:I = 0x7f0a0811

.field public static final greeting_tts_default:I = 0x7f0a080d

.field public static final greeting_tts_evening_1:I = 0x7f0a0812

.field public static final greeting_tts_evening_2:I = 0x7f0a0813

.field public static final greeting_tts_help_1:I = 0x7f0a07e8

.field public static final greeting_tts_morning_1:I = 0x7f0a080f

.field public static final greeting_tts_morning_2:I = 0x7f0a0810

.field public static final greeting_tts_night_1:I = 0x7f0a0814

.field public static final greeting_tts_night_2:I = 0x7f0a0815

.field public static final greeting_tts_others_1:I = 0x7f0a081d

.field public static final greeting_tts_others_2:I = 0x7f0a081e

.field public static final greeting_tts_week_last_1:I = 0x7f0a0818

.field public static final greeting_tts_week_last_2:I = 0x7f0a0819

.field public static final greeting_tts_week_last_3:I = 0x7f0a081a

.field public static final greeting_tts_week_start_1:I = 0x7f0a0816

.field public static final greeting_tts_week_start_2:I = 0x7f0a0817

.field public static final greeting_tts_weekends_1:I = 0x7f0a081b

.field public static final greeting_tts_weekends_2:I = 0x7f0a081c

.field public static final group_loc_share_menu_add:I = 0x7f0a02c9

.field public static final group_loc_share_menu_navigate:I = 0x7f0a02c8

.field public static final group_loc_share_menu_quit:I = 0x7f0a02ca

.field public static final group_loc_share_title:I = 0x7f0a02cb

.field public static final group_location:I = 0x7f0a0320

.field public static final group_location_share_missing:I = 0x7f0a02ef

.field public static final group_share_arrived:I = 0x7f0a0312

.field public static final group_share_contact_joined:I = 0x7f0a0332

.field public static final group_share_contact_left:I = 0x7f0a0334

.field public static final group_share_contact_not_joined:I = 0x7f0a0333

.field public static final group_share_contact_reinvite:I = 0x7f0a0335

.field public static final group_share_host:I = 0x7f0a0331

.field public static final group_share_left:I = 0x7f0a0313

.field public static final group_share_not_available:I = 0x7f0a0337

.field public static final group_share_not_joined:I = 0x7f0a0311

.field public static final group_share_not_moving:I = 0x7f0a0336

.field public static final group_share_rejected:I = 0x7f0a0314

.field public static final group_sharing_activity_info:I = 0x7f0a0267

.field public static final grp_loc_label_me:I = 0x7f0a0318

.field public static final grp_loc_sh_btn_new:I = 0x7f0a02d0

.field public static final grp_loc_sh_btn_rejoin:I = 0x7f0a02cf

.field public static final grp_loc_sh_btn_restart:I = 0x7f0a02ce

.field public static final grp_loc_sh_btn_start:I = 0x7f0a02cc

.field public static final grp_loc_sh_btn_view:I = 0x7f0a02cd

.field public static final grp_loc_sh_destination_label:I = 0x7f0a02ea

.field public static final grp_loc_sh_duration_expired:I = 0x7f0a02e3

.field public static final grp_loc_sh_duration_hour_unit_label:I = 0x7f0a0849

.field public static final grp_loc_sh_duration_label:I = 0x7f0a02e5

.field public static final grp_loc_sh_duration_remained:I = 0x7f0a02e4

.field public static final grp_loc_sh_duration_unit_label:I = 0x7f0a02e6

.field public static final grp_loc_sh_group_destination_not_changed:I = 0x7f0a02fc

.field public static final grp_loc_sh_group_not_created:I = 0x7f0a02fa

.field public static final grp_loc_sh_group_participant_not_added:I = 0x7f0a02f9

.field public static final grp_loc_sh_group_restart_not_exec:I = 0x7f0a02fb

.field public static final grp_loc_sh_missing_data:I = 0x7f0a02ec

.field public static final grp_loc_sh_missing_phone_number:I = 0x7f0a02ed

.field public static final grp_loc_sh_nodestination_label:I = 0x7f0a02eb

.field public static final grp_loc_sh_participants_label:I = 0x7f0a02e7

.field public static final grp_loc_sh_participants_sub:I = 0x7f0a04ef

.field public static final grp_loc_sh_participants_sub_joined:I = 0x7f0a02e9

.field public static final grp_loc_sh_participants_sub_selected:I = 0x7f0a02e8

.field public static final grp_loc_sh_request_remove_error:I = 0x7f0a0301

.field public static final grp_loc_sh_request_timeout:I = 0x7f0a0310

.field public static final grp_shr_loc_button_duration_1:I = 0x7f0a031b

.field public static final grp_shr_loc_button_duration_12:I = 0x7f0a031f

.field public static final grp_shr_loc_button_duration_2:I = 0x7f0a031c

.field public static final grp_shr_loc_button_duration_3:I = 0x7f0a031d

.field public static final grp_shr_loc_button_duration_5:I = 0x7f0a031e

.field public static final guide_ewys_memo_content:I = 0x7f0a05b6

.field public static final guide_ewys_memo_prompt:I = 0x7f0a05b5

.field public static final hands_free_info_body:I = 0x7f0a082c

.field public static final hands_free_info_title:I = 0x7f0a082b

.field public static final handsfree_mode_popup_enable:I = 0x7f0a082a

.field public static final handsfree_mode_securelock_popup_message:I = 0x7f0a0829

.field public static final handsfree_mode_voicewakeup_popup_message:I = 0x7f0a0828

.field public static final handwriting:I = 0x7f0a0759

.field public static final handwriting_mode_off:I = 0x7f0a075e

.field public static final handwriting_mode_on:I = 0x7f0a075d

.field public static final hello_world:I = 0x7f0a03b4

.field public static final help:I = 0x7f0a0641

.field public static final help_about_copyright:I = 0x7f0a0642

.field public static final help_about_privacy:I = 0x7f0a0643

.field public static final help_about_terms:I = 0x7f0a0644

.field public static final help_about_vlingo:I = 0x7f0a0645

.field public static final help_about_wakeup_powered_by:I = 0x7f0a0646

.field public static final help_call_driving:I = 0x7f0a05a0

.field public static final help_example:I = 0x7f0a03c5

.field public static final help_invalid_action:I = 0x7f0a03c6

.field public static final help_item_1:I = 0x7f0a04b0

.field public static final help_item_2:I = 0x7f0a04b1

.field public static final help_item_3:I = 0x7f0a04b3

.field public static final help_item_4:I = 0x7f0a04b2

.field public static final help_item_5:I = 0x7f0a04b4

.field public static final help_item_6:I = 0x7f0a04b5

.field public static final help_item_7:I = 0x7f0a04b6

.field public static final help_item_8:I = 0x7f0a04b7

.field public static final help_memo_driving:I = 0x7f0a05a4

.field public static final help_menu7:I = 0x7f0a0647

.field public static final help_menu8:I = 0x7f0a0648

.field public static final help_message:I = 0x7f0a04af

.field public static final help_music_driving:I = 0x7f0a059e

.field public static final help_navigate_driving:I = 0x7f0a05a1

.field public static final help_news_driving:I = 0x7f0a05a5

.field public static final help_online_help:I = 0x7f0a03d2

.field public static final help_schedule_driving:I = 0x7f0a05a3

.field public static final help_subtitle:I = 0x7f0a03d3

.field public static final help_text_driving:I = 0x7f0a05a2

.field public static final help_title:I = 0x7f0a03d1

.field public static final help_tooltip_available_commands_tap:I = 0x7f0a0832

.field public static final help_tooltip_edit:I = 0x7f0a0803

.field public static final help_tooltip_help_button:I = 0x7f0a0800

.field public static final help_tooltip_keypad:I = 0x7f0a0804

.field public static final help_tooltip_mic:I = 0x7f0a07ff

.field public static final help_tooltip_question_tap:I = 0x7f0a0830

.field public static final help_tooltip_voice_tap:I = 0x7f0a0801

.field public static final help_tooltip_wakeup_unlock:I = 0x7f0a0802

.field public static final help_version:I = 0x7f0a0649

.field public static final help_wcis_check_weather:I = 0x7f0a0591

.field public static final help_wcis_check_weather_sub1:I = 0x7f0a0592

.field public static final help_wcis_dial:I = 0x7f0a064a

.field public static final help_wcis_dial_sub1:I = 0x7f0a054d

.field public static final help_wcis_driving_mode:I = 0x7f0a054e

.field public static final help_wcis_driving_mode_sub1:I = 0x7f0a054f

.field public static final help_wcis_findcontact:I = 0x7f0a0561

.field public static final help_wcis_findcontact_sub1:I = 0x7f0a0562

.field public static final help_wcis_get_an_answer:I = 0x7f0a0593

.field public static final help_wcis_get_an_answer_sub1:I = 0x7f0a0594

.field public static final help_wcis_home_subtitle:I = 0x7f0a03d0

.field public static final help_wcis_home_title:I = 0x7f0a03cf

.field public static final help_wcis_local_listings:I = 0x7f0a0598

.field public static final help_wcis_local_listings_sub1:I = 0x7f0a0599

.field public static final help_wcis_location_subtitle:I = 0x7f0a03cc

.field public static final help_wcis_location_title:I = 0x7f0a03cb

.field public static final help_wcis_memo:I = 0x7f0a0550

.field public static final help_wcis_memo_sub1:I = 0x7f0a0551

.field public static final help_wcis_message_subtitle:I = 0x7f0a03ca

.field public static final help_wcis_message_title:I = 0x7f0a03c9

.field public static final help_wcis_movie:I = 0x7f0a064b

.field public static final help_wcis_music:I = 0x7f0a0552

.field public static final help_wcis_music_sub1:I = 0x7f0a0553

.field public static final help_wcis_music_subtitle:I = 0x7f0a03ce

.field public static final help_wcis_music_title:I = 0x7f0a03cd

.field public static final help_wcis_nav:I = 0x7f0a0554

.field public static final help_wcis_nav_sub1:I = 0x7f0a0555

.field public static final help_wcis_news:I = 0x7f0a05a8

.field public static final help_wcis_news_sub1:I = 0x7f0a05a9

.field public static final help_wcis_open_app:I = 0x7f0a0556

.field public static final help_wcis_open_app_sub1:I = 0x7f0a0557

.field public static final help_wcis_open_applications:I = 0x7f0a064c

.field public static final help_wcis_phone_subtitle:I = 0x7f0a03c8

.field public static final help_wcis_phone_title:I = 0x7f0a03c7

.field public static final help_wcis_recordvoice:I = 0x7f0a0558

.field public static final help_wcis_recordvoice_sub1:I = 0x7f0a0559

.field public static final help_wcis_schedule:I = 0x7f0a064d

.field public static final help_wcis_schedule_lookup_sub1:I = 0x7f0a055b

.field public static final help_wcis_schedule_sub1:I = 0x7f0a055a

.field public static final help_wcis_search:I = 0x7f0a064e

.field public static final help_wcis_search_sub1:I = 0x7f0a055c

.field public static final help_wcis_set_an_alarm:I = 0x7f0a058c

.field public static final help_wcis_set_an_alarm_lookup_sub1:I = 0x7f0a058e

.field public static final help_wcis_set_an_alarm_sub1:I = 0x7f0a058d

.field public static final help_wcis_simple_setting_controls_sub1:I = 0x7f0a0595

.field public static final help_wcis_simple_setting_controls_sub1_for_chinese:I = 0x7f0a05b4

.field public static final help_wcis_social_update:I = 0x7f0a055d

.field public static final help_wcis_social_update_sub1:I = 0x7f0a055e

.field public static final help_wcis_social_update_sub1_cn:I = 0x7f0a055f

.field public static final help_wcis_spoken_prompt:I = 0x7f0a059f

.field public static final help_wcis_start_timer:I = 0x7f0a058f

.field public static final help_wcis_start_timer_sub1:I = 0x7f0a0590

.field public static final help_wcis_task:I = 0x7f0a0563

.field public static final help_wcis_task_lookup_sub1:I = 0x7f0a0565

.field public static final help_wcis_task_sub1:I = 0x7f0a0564

.field public static final help_wcis_text_message:I = 0x7f0a064f

.field public static final help_wcis_text_message_sub1:I = 0x7f0a0560

.field public static final help_wcis_world_clock:I = 0x7f0a05b7

.field public static final help_wcis_world_clock_sub1:I = 0x7f0a05b8

.field public static final help_weather_driving:I = 0x7f0a05a6

.field public static final home_blocking_carmode_during_call:I = 0x7f0a026a

.field public static final home_blocking_noti_during_call:I = 0x7f0a026b

.field public static final home_bubble_text:I = 0x7f0a0268

.field public static final home_button_accept:I = 0x7f0a025e

.field public static final home_button_decline:I = 0x7f0a025f

.field public static final home_button_later:I = 0x7f0a0261

.field public static final home_button_now:I = 0x7f0a0260

.field public static final home_button_start:I = 0x7f0a025d

.field public static final home_dialog_cancel:I = 0x7f0a0255

.field public static final home_dialog_exit:I = 0x7f0a0256

.field public static final home_dialog_next:I = 0x7f0a0266

.field public static final home_dialog_ok:I = 0x7f0a0254

.field public static final home_dialog_title:I = 0x7f0a0257

.field public static final home_dialog_title_warning:I = 0x7f0a0262

.field public static final home_exit_title:I = 0x7f0a0258

.field public static final home_group_location_share_button:I = 0x7f0a0259

.field public static final home_help_button:I = 0x7f0a025c

.field public static final home_inrix_incidents_button:I = 0x7f0a04f1

.field public static final home_location_button:I = 0x7f0a0252

.field public static final home_msg_button:I = 0x7f0a0250

.field public static final home_music_button:I = 0x7f0a0253

.field public static final home_ongoing_noti_text:I = 0x7f0a0265

.field public static final home_ongoing_noti_title:I = 0x7f0a0264

.field public static final home_phone_button:I = 0x7f0a024f

.field public static final home_routes_button:I = 0x7f0a0251

.field public static final home_settins_button:I = 0x7f0a025b

.field public static final home_warning_text:I = 0x7f0a0263

.field public static final home_what_can_i_say_button:I = 0x7f0a025a

.field public static final img_open_fold_desc:I = 0x7f0a04da

.field public static final incall_error_emergency_only:I = 0x7f0a0362

.field public static final incar_greeting_hi_galaxy:I = 0x7f0a05c4

.field public static final incar_greeting_wakeup:I = 0x7f0a05c5

.field public static final incident_type_accident:I = 0x7f0a0228

.field public static final incident_type_construction:I = 0x7f0a0229

.field public static final incident_type_event:I = 0x7f0a022a

.field public static final incident_type_flow:I = 0x7f0a022b

.field public static final incident_type_police:I = 0x7f0a022d

.field public static final incident_type_road_closure:I = 0x7f0a022c

.field public static final incoming_call:I = 0x7f0a07b5

.field public static final incoming_call_notification_summary:I = 0x7f0a07b6

.field public static final inrix_incident_backup_header:I = 0x7f0a04f3

.field public static final inrix_incident_delay_header:I = 0x7f0a04f2

.field public static final inrix_incident_end_time:I = 0x7f0a04f5

.field public static final inrix_incident_start_time:I = 0x7f0a04f4

.field public static final iux_edit_what_you_said_1:I = 0x7f0a065b

.field public static final iux_edit_what_you_said_2:I = 0x7f0a065c

.field public static final iux_handwriting_1:I = 0x7f0a0658

.field public static final iux_handwriting_2:I = 0x7f0a0659

.field public static final iux_handwriting_3:I = 0x7f0a065a

.field public static final iux_tips_about:I = 0x7f0a0650

.field public static final iux_wake_up_1:I = 0x7f0a05bc

.field public static final iux_wake_up_2:I = 0x7f0a0651

.field public static final iux_wake_up_3:I = 0x7f0a0652

.field public static final iux_wake_up_3_no_home_key:I = 0x7f0a0653

.field public static final iux_wake_up_4:I = 0x7f0a0654

.field public static final iux_what_you_want_1:I = 0x7f0a0655

.field public static final iux_what_you_want_2:I = 0x7f0a0656

.field public static final iux_what_you_want_3:I = 0x7f0a0657

.field public static final joined_participant:I = 0x7f0a0847

.field public static final label_btn_route:I = 0x7f0a030f

.field public static final label_nav_destination_changed:I = 0x7f0a0339

.field public static final label_notification_location_request:I = 0x7f0a0316

.field public static final lbl_best_time_to_leave:I = 0x7f0a0222

.field public static final lbl_error_connection_or_service:I = 0x7f0a04a2

.field public static final lbl_gps_provider_disabled:I = 0x7f0a04a3

.field public static final lbl_info_arrival:I = 0x7f0a0219

.field public static final lbl_info_duration:I = 0x7f0a0218

.field public static final lbl_invalid_coord_destination:I = 0x7f0a04a1

.field public static final lbl_invalid_destination:I = 0x7f0a04a0

.field public static final lbl_retry_search:I = 0x7f0a084a

.field public static final lbl_traffic_forcast:I = 0x7f0a021f

.field public static final lbl_traffic_forecast:I = 0x7f0a049c

.field public static final lbl_unable_to_get_current_location:I = 0x7f0a049f

.field public static final left_group_share:I = 0x7f0a0488

.field public static final lgu_plus_navigation_disabled:I = 0x7f0a04aa

.field public static final line1:I = 0x7f0a065d

.field public static final line2:I = 0x7f0a065e

.field public static final loading:I = 0x7f0a0850

.field public static final loc_share_group:I = 0x7f0a04ed

.field public static final loc_share_group_expired:I = 0x7f0a02e2

.field public static final loc_share_group_expired_message:I = 0x7f0a02e1

.field public static final loc_share_group_extend_message_button:I = 0x7f0a048e

.field public static final loc_share_group_land:I = 0x7f0a02b5

.field public static final loc_share_group_port:I = 0x7f0a02b2

.field public static final loc_share_group_quit_sharing_detail_title:I = 0x7f0a02c6

.field public static final loc_share_group_quit_sharing_dialog_message:I = 0x7f0a02bd

.field public static final loc_share_group_quit_sharing_dialog_quit_button:I = 0x7f0a02c5

.field public static final loc_share_group_quit_sharing_dialog_title:I = 0x7f0a02bf

.field public static final loc_share_group_stop_sharing_message:I = 0x7f0a02be

.field public static final loc_share_request:I = 0x7f0a04ec

.field public static final loc_share_request_land:I = 0x7f0a0210

.field public static final loc_share_request_port:I = 0x7f0a02b1

.field public static final loc_share_send:I = 0x7f0a04de

.field public static final loc_share_send_land:I = 0x7f0a02b3

.field public static final loc_share_send_port:I = 0x7f0a02b0

.field public static final loc_share_share_land:I = 0x7f0a02b4

.field public static final local_search_missing_search_parameter:I = 0x7f0a060b

.field public static final local_search_results_search:I = 0x7f0a060d

.field public static final local_search_results_search_cheapest:I = 0x7f0a060c

.field public static final local_search_unsupported_category:I = 0x7f0a060e

.field public static final localsearch_details:I = 0x7f0a065f

.field public static final localsearch_powered_by:I = 0x7f0a0660

.field public static final localsearch_reviews:I = 0x7f0a0661

.field public static final location:I = 0x7f0a0662

.field public static final location_2depth_label:I = 0x7f0a029e

.field public static final location_address_missing:I = 0x7f0a02ee

.field public static final location_button_add:I = 0x7f0a02b7

.field public static final location_button_done:I = 0x7f0a02b6

.field public static final location_button_request:I = 0x7f0a0215

.field public static final location_button_send:I = 0x7f0a02b8

.field public static final location_button_share:I = 0x7f0a04eb

.field public static final location_destination_changed_confirmed:I = 0x7f0a05fd

.field public static final location_google_moving:I = 0x7f0a04f9

.field public static final location_group_expiration_extend_confirmed:I = 0x7f0a061a

.field public static final location_group_request_confirmed:I = 0x7f0a05fc

.field public static final location_group_share_destination_details:I = 0x7f0a02d4

.field public static final location_group_share_destination_details_change:I = 0x7f0a02d6

.field public static final location_group_share_destination_details_route:I = 0x7f0a02d5

.field public static final location_group_share_duration_warning_land:I = 0x7f0a02d1

.field public static final location_group_share_duration_warning_port:I = 0x7f0a02d2

.field public static final location_home:I = 0x7f0a02f0

.field public static final location_home_dialog_message:I = 0x7f0a02c1

.field public static final location_map_navi:I = 0x7f0a0307

.field public static final location_multiple_phone_numbers:I = 0x7f0a0612

.field public static final location_my_place_moving:I = 0x7f0a04fa

.field public static final location_my_place_moving_text_flow:I = 0x7f0a04fc

.field public static final location_my_place_moving_voice_flow:I = 0x7f0a04fb

.field public static final location_myplace:I = 0x7f0a02f2

.field public static final location_myplace_accept:I = 0x7f0a02f4

.field public static final location_myplace_decline:I = 0x7f0a02f3

.field public static final location_myplaces:I = 0x7f0a02f1

.field public static final location_myplaces_dialog:I = 0x7f0a02c3

.field public static final location_myplaces_dialog_message:I = 0x7f0a02c0

.field public static final location_myplaces_moving:I = 0x7f0a02c4

.field public static final location_nearby_poi:I = 0x7f0a0302

.field public static final location_no_list:I = 0x7f0a02ad

.field public static final location_no_match_contact:I = 0x7f0a0613

.field public static final location_office:I = 0x7f0a02f5

.field public static final location_office_dialog_message:I = 0x7f0a02c2

.field public static final location_pending:I = 0x7f0a0300

.field public static final location_request_approved:I = 0x7f0a0608

.field public static final location_request_confirm_text:I = 0x7f0a0212

.field public static final location_request_confirm_text_:I = 0x7f0a02db

.field public static final location_request_text_who:I = 0x7f0a0611

.field public static final location_request_waiting_label:I = 0x7f0a02af

.field public static final location_route_confirm_text:I = 0x7f0a02de

.field public static final location_route_confirm_text_tts:I = 0x7f0a04ee

.field public static final location_search:I = 0x7f0a02ae

.field public static final location_search_no_results:I = 0x7f0a031a

.field public static final location_search_result_found_:I = 0x7f0a084e

.field public static final location_share_2depth_label:I = 0x7f0a02aa

.field public static final location_share_2depth_label_port:I = 0x7f0a02ab

.field public static final location_share_allow_dialog_body:I = 0x7f0a02d8

.field public static final location_share_allow_dialog_cancel:I = 0x7f0a02d9

.field public static final location_share_allow_dialog_header:I = 0x7f0a02d7

.field public static final location_share_allow_dialog_ok:I = 0x7f0a02da

.field public static final location_share_confirm_text:I = 0x7f0a02dc

.field public static final location_share_confirm_text_:I = 0x7f0a02dd

.field public static final location_share_fail_accept_request:I = 0x7f0a02ba

.field public static final location_share_fail_accept_share:I = 0x7f0a02bc

.field public static final location_share_fail_request:I = 0x7f0a02bb

.field public static final location_share_fail_share:I = 0x7f0a02b9

.field public static final location_share_invalid_location:I = 0x7f0a033a

.field public static final location_share_request_ignored:I = 0x7f0a02df

.field public static final location_share_text_who:I = 0x7f0a0610

.field public static final location_shared_ignored:I = 0x7f0a02e0

.field public static final location_sharing_confirmed:I = 0x7f0a0602

.field public static final location_sharing_request_confirmed:I = 0x7f0a05fb

.field public static final location_sharing_request_sent:I = 0x7f0a05f0

.field public static final location_simulation_text:I = 0x7f0a0605

.field public static final location_suggestions:I = 0x7f0a02f7

.field public static final location_suggestions_none_address:I = 0x7f0a02d3

.field public static final location_sumulate_group_expire:I = 0x7f0a0604

.field public static final location_title_req_wait:I = 0x7f0a02ac

.field public static final location_title_request_to:I = 0x7f0a029b

.field public static final location_title_share:I = 0x7f0a029d

.field public static final location_title_suggestions:I = 0x7f0a029c

.field public static final location_to_share_confirm_text:I = 0x7f0a0214

.field public static final location_work:I = 0x7f0a02f6

.field public static final location_youarehere:I = 0x7f0a02f8

.field public static final ls_listing_detail_activity_reserve:I = 0x7f0a0663

.field public static final making_call_screen_1:I = 0x7f0a04c5

.field public static final making_call_screen_2:I = 0x7f0a04c6

.field public static final making_call_screen_3:I = 0x7f0a04c7

.field public static final making_call_screen_tip_1:I = 0x7f0a04c8

.field public static final making_call_screen_tip_2:I = 0x7f0a04c9

.field public static final map:I = 0x7f0a0664

.field public static final mapview_button_cancel:I = 0x7f0a0298

.field public static final mapview_button_navigate:I = 0x7f0a0299

.field public static final memo_delete_confirmation:I = 0x7f0a0665

.field public static final memo_delete_fail:I = 0x7f0a0666

.field public static final menu_custom_wake_up:I = 0x7f0a0667

.field public static final menu_driving_mode:I = 0x7f0a0668

.field public static final menu_driving_mode_off:I = 0x7f0a0669

.field public static final menu_driving_mode_on:I = 0x7f0a066a

.field public static final menu_setting:I = 0x7f0a066b

.field public static final menu_use_external_speaker:I = 0x7f0a066c

.field public static final menu_use_internal_speaker:I = 0x7f0a066d

.field public static final merry_christmas_greet:I = 0x7f0a07c9

.field public static final merry_christmas_title:I = 0x7f0a07c8

.field public static final message:I = 0x7f0a07b7

.field public static final message_compose_button_cancel:I = 0x7f0a03ac

.field public static final message_compose_button_replace:I = 0x7f0a03ae

.field public static final message_compose_button_send:I = 0x7f0a03ad

.field public static final message_hour:I = 0x7f0a048a

.field public static final message_hours:I = 0x7f0a048b

.field public static final message_inbox_no_list:I = 0x7f0a03ab

.field public static final message_keyboard:I = 0x7f0a0853

.field public static final message_list_title_inbox:I = 0x7f0a03aa

.field public static final message_list_title_suggestions:I = 0x7f0a03a9

.field public static final message_maximun_character_sms:I = 0x7f0a03b1

.field public static final message_minute:I = 0x7f0a048c

.field public static final message_minutes:I = 0x7f0a048d

.field public static final message_notification_summary:I = 0x7f0a07b8

.field public static final message_read_need_downloading:I = 0x7f0a03b7

.field public static final message_read_no_text_land:I = 0x7f0a03af

.field public static final message_read_no_text_por:I = 0x7f0a03b0

.field public static final message_read_only_attachment:I = 0x7f0a03b6

.field public static final mic_button:I = 0x7f0a0776

.field public static final midas_greeting:I = 0x7f0a066e

.field public static final mon1:I = 0x7f0a0732

.field public static final mood_music_notification:I = 0x7f0a07db

.field public static final more_btn:I = 0x7f0a0769

.field public static final movie_actors:I = 0x7f0a075f

.field public static final movie_booking:I = 0x7f0a0760

.field public static final movie_directors:I = 0x7f0a0761

.field public static final movie_grade:I = 0x7f0a0762

.field public static final movie_grade_12_over:I = 0x7f0a0772

.field public static final movie_grade_15_over:I = 0x7f0a0773

.field public static final movie_grade_18_over:I = 0x7f0a0774

.field public static final movie_grade_all:I = 0x7f0a0775

.field public static final movie_more_info:I = 0x7f0a0764

.field public static final movie_now_showing:I = 0x7f0a076b

.field public static final movie_same_name:I = 0x7f0a0765

.field public static final movie_soon:I = 0x7f0a076a

.field public static final msg_from:I = 0x7f0a066f

.field public static final msg_sent_fail:I = 0x7f0a078b

.field public static final msg_sent_successful:I = 0x7f0a078a

.field public static final msg_to:I = 0x7f0a0670

.field public static final multiple_phone_numbers_message:I = 0x7f0a05f3

.field public static final multiwindow_direction:I = 0x7f0a01f4

.field public static final music_ambiguous_command:I = 0x7f0a0838

.field public static final music_calling_no_play_msg:I = 0x7f0a036b

.field public static final music_list_string_album:I = 0x7f0a0366

.field public static final music_list_string_albums:I = 0x7f0a0368

.field public static final music_list_string_allsongs:I = 0x7f0a0369

.field public static final music_list_string_song:I = 0x7f0a0365

.field public static final music_list_string_songs:I = 0x7f0a0367

.field public static final music_main:I = 0x7f0a0364

.field public static final music_main_no_list:I = 0x7f0a0363

.field public static final music_player_error:I = 0x7f0a036f

.field public static final music_playing_calm:I = 0x7f0a07ee

.field public static final music_playing_exciting:I = 0x7f0a07ec

.field public static final music_playing_joyful:I = 0x7f0a07ed

.field public static final music_playing_passionate:I = 0x7f0a07ef

.field public static final music_prompt_1:I = 0x7f0a060f

.field public static final music_search_music:I = 0x7f0a0370

.field public static final music_search_sip_one_result:I = 0x7f0a0372

.field public static final music_search_sip_result:I = 0x7f0a0371

.field public static final music_toast_no_song:I = 0x7f0a036a

.field public static final music_unknown_artist:I = 0x7f0a036e

.field public static final music_unknown_msg:I = 0x7f0a036c

.field public static final music_unknown_title:I = 0x7f0a036d

.field public static final my_place_for_driving_mode:I = 0x7f0a07cb

.field public static final my_place_summary_for_driving_mode:I = 0x7f0a07cd

.field public static final myplace_alert_summary:I = 0x7f0a07d8

.field public static final myplace_alert_summary_for_chinese:I = 0x7f0a0806

.field public static final myplace_connect_to_vehicle:I = 0x7f0a07eb

.field public static final myplace_title:I = 0x7f0a07cc

.field public static final naver_connection_failure:I = 0x7f0a075c

.field public static final navigate:I = 0x7f0a0671

.field public static final navigate_confirmation:I = 0x7f0a0836

.field public static final navigate_office:I = 0x7f0a0863

.field public static final navigate_office_fail:I = 0x7f0a0865

.field public static final navigate_office_notsupport:I = 0x7f0a0864

.field public static final navigate_prompt:I = 0x7f0a0837

.field public static final navigate_recentdest:I = 0x7f0a0866

.field public static final navigate_recentdest_notsupport:I = 0x7f0a0867

.field public static final navigation:I = 0x7f0a0843

.field public static final navigation_domain_title:I = 0x7f0a029a

.field public static final new_address:I = 0x7f0a0766

.field public static final new_email:I = 0x7f0a07b9

.field public static final new_email_notification_summary:I = 0x7f0a07ba

.field public static final new_voicemail:I = 0x7f0a07bb

.field public static final new_voicemail_notification_summary:I = 0x7f0a07bc

.field public static final new_year_greet:I = 0x7f0a07c7

.field public static final new_year_title:I = 0x7f0a07c6

.field public static final newscp_flipboard:I = 0x7f0a07df

.field public static final newscp_noflipboard:I = 0x7f0a07e1

.field public static final newscp_yonhap:I = 0x7f0a07e0

.field public static final next:I = 0x7f0a0672

.field public static final no_address_to_search:I = 0x7f0a0338

.field public static final no_data_found:I = 0x7f0a030a

.field public static final no_list:I = 0x7f0a020e

.field public static final no_match_contact_dial:I = 0x7f0a05f4

.field public static final no_name_reprompt_name:I = 0x7f0a05e8

.field public static final no_network:I = 0x7f0a0673

.field public static final no_search_items_groupshare_destination:I = 0x7f0a0297

.field public static final no_search_list:I = 0x7f0a035a

.field public static final no_simcard:I = 0x7f0a02fe

.field public static final no_suggestions:I = 0x7f0a0359

.field public static final no_text:I = 0x7f0a073a

.field public static final no_title:I = 0x7f0a0739

.field public static final not_have_the_song:I = 0x7f0a0741

.field public static final not_tracing:I = 0x7f0a0763

.field public static final noti_alarm_am:I = 0x7f0a0380

.field public static final noti_alarm_button_snooze:I = 0x7f0a037e

.field public static final noti_alarm_button_stop:I = 0x7f0a037f

.field public static final noti_alarm_pm:I = 0x7f0a0381

.field public static final noti_bt_req_help_confirm:I = 0x7f0a039b

.field public static final noti_bt_req_help_pin:I = 0x7f0a039a

.field public static final noti_bt_req_title:I = 0x7f0a0399

.field public static final noti_bt_req_tts:I = 0x7f0a039e

.field public static final noti_bt_req_tts_confirm:I = 0x7f0a039d

.field public static final noti_bt_req_tts_pin:I = 0x7f0a039c

.field public static final noti_charger_remains:I = 0x7f0a0394

.field public static final noti_charger_title:I = 0x7f0a0393

.field public static final noti_dialog_allow:I = 0x7f0a0383

.field public static final noti_dialog_call:I = 0x7f0a0387

.field public static final noti_dialog_cancel:I = 0x7f0a038a

.field public static final noti_dialog_group_share_expired_title:I = 0x7f0a0385

.field public static final noti_dialog_group_share_request_title:I = 0x7f0a0384

.field public static final noti_dialog_navigate:I = 0x7f0a038c

.field public static final noti_dialog_ok:I = 0x7f0a0389

.field public static final noti_dialog_read:I = 0x7f0a0386

.field public static final noti_dialog_reply:I = 0x7f0a0388

.field public static final noti_dialog_share:I = 0x7f0a038b

.field public static final noti_failed_request_location:I = 0x7f0a0390

.field public static final noti_failed_share_location:I = 0x7f0a0391

.field public static final noti_group_location_sharing_stop_message:I = 0x7f0a0317

.field public static final noti_has_no_samsung_account:I = 0x7f0a039f

.field public static final noti_ignore_invitations:I = 0x7f0a0398

.field public static final noti_ignore_loc_group_share_invitations:I = 0x7f0a0397

.field public static final noti_location_group_sharing_ignored:I = 0x7f0a0392

.field public static final noti_location_shared:I = 0x7f0a021d

.field public static final noti_location_shared_ended:I = 0x7f0a038f

.field public static final noti_location_sharing_changed_destination:I = 0x7f0a03a0

.field public static final noti_location_sharing_send_request:I = 0x7f0a03a1

.field public static final noti_location_sharing_waiting_request:I = 0x7f0a03a2

.field public static final noti_message_check_navigation_menu:I = 0x7f0a03a8

.field public static final noti_message_message_read:I = 0x7f0a01ea

.field public static final noti_message_tts:I = 0x7f0a03a7

.field public static final noti_message_type_mms:I = 0x7f0a03a5

.field public static final noti_message_type_sms:I = 0x7f0a03a6

.field public static final noti_sending_changed_destination:I = 0x7f0a0396

.field public static final noti_sending_fail_text:I = 0x7f0a038e

.field public static final noti_sending_invitations:I = 0x7f0a0395

.field public static final noti_sending_result_text:I = 0x7f0a021e

.field public static final noti_sending_text:I = 0x7f0a038d

.field public static final noti_sharing_text:I = 0x7f0a021c

.field public static final noti_smartalert_ahead:I = 0x7f0a0224

.field public static final noti_smartalert_backup:I = 0x7f0a0225

.field public static final noti_smartalert_connection_error:I = 0x7f0a03a3

.field public static final noti_smartalert_miles:I = 0x7f0a0227

.field public static final noti_smartalert_minutes:I = 0x7f0a0226

.field public static final noti_smartalert_receive:I = 0x7f0a0223

.field public static final noti_tts_schedule:I = 0x7f0a0382

.field public static final noti_tts_schedule_location:I = 0x7f0a01f3

.field public static final noti_tts_scheudle_untitled:I = 0x7f0a04f8

.field public static final noti_tts_time:I = 0x7f0a01f2

.field public static final notice_using_data_body_chinese:I = 0x7f0a0834

.field public static final notice_using_data_title:I = 0x7f0a0833

.field public static final notification_button_cancel:I = 0x7f0a03b8

.field public static final notification_button_save:I = 0x7f0a03b9

.field public static final object_is_null:I = 0x7f0a0789

.field public static final ok_lc:I = 0x7f0a0674

.field public static final ok_uc:I = 0x7f0a0675

.field public static final open_app_multiple_matches:I = 0x7f0a0293

.field public static final open_app_no_match_found:I = 0x7f0a0292

.field public static final open_app_no_ordinal_support:I = 0x7f0a0291

.field public static final ordinal_displayed_suffix_nd:I = 0x7f0a05e6

.field public static final ordinal_displayed_suffix_rd:I = 0x7f0a05e7

.field public static final ordinal_displayed_suffix_st:I = 0x7f0a05e5

.field public static final ordinal_displayed_suffix_th:I = 0x7f0a05e4

.field public static final ordinal_spoken_first:I = 0x7f0a05e0

.field public static final ordinal_spoken_fourth:I = 0x7f0a05e3

.field public static final ordinal_spoken_second:I = 0x7f0a05e1

.field public static final ordinal_spoken_third:I = 0x7f0a05e2

.field public static final participants:I = 0x7f0a0676

.field public static final phone_answer:I = 0x7f0a0352

.field public static final phone_call:I = 0x7f0a0354

.field public static final phone_call_ended:I = 0x7f0a034d

.field public static final phone_cancel_call:I = 0x7f0a0350

.field public static final phone_dialing:I = 0x7f0a034b

.field public static final phone_end_call:I = 0x7f0a0351

.field public static final phone_in_call:I = 0x7f0a034c

.field public static final phone_in_use:I = 0x7f0a0782

.field public static final phone_incoming:I = 0x7f0a034e

.field public static final phone_list_number_fax:I = 0x7f0a035f

.field public static final phone_list_number_home:I = 0x7f0a035c

.field public static final phone_list_number_mobile:I = 0x7f0a035d

.field public static final phone_list_number_office:I = 0x7f0a035e

.field public static final phone_list_number_phone:I = 0x7f0a0360

.field public static final phone_list_title_logs:I = 0x7f0a0357

.field public static final phone_list_title_number:I = 0x7f0a035b

.field public static final phone_list_title_search:I = 0x7f0a0358

.field public static final phone_list_title_suggestions:I = 0x7f0a0356

.field public static final phone_message:I = 0x7f0a0355

.field public static final phone_preparing_dial:I = 0x7f0a034f

.field public static final phone_reject:I = 0x7f0a0353

.field public static final phone_there_is_no_log_to_show:I = 0x7f0a0361

.field public static final phone_type_home:I = 0x7f0a05bd

.field public static final phone_type_main:I = 0x7f0a0624

.field public static final phone_type_mobile:I = 0x7f0a05be

.field public static final phone_type_other:I = 0x7f0a05bf

.field public static final phone_type_pager:I = 0x7f0a0625

.field public static final phone_type_work:I = 0x7f0a05c0

.field public static final phrasespotter_SEARCH_GRAMMAR_RES_NAME:I = 0x7f0a051b

.field public static final placeon_accept_message:I = 0x7f0a0346

.field public static final placeon_decline_message:I = 0x7f0a0345

.field public static final placeon_text_disclaimer:I = 0x7f0a0344

.field public static final placeon_title_disclaimer:I = 0x7f0a0343

.field public static final playlists_quicklist_default_value:I = 0x7f0a0295

.field public static final please_wait:I = 0x7f0a0319

.field public static final pm:I = 0x7f0a06fe

.field public static final pm_evening:I = 0x7f0a06ff

.field public static final pm_night:I = 0x7f0a0700

.field public static final poi_gas:I = 0x7f0a0306

.field public static final poi_gas_lot:I = 0x7f0a0305

.field public static final poi_parking:I = 0x7f0a0303

.field public static final poi_parking_lot:I = 0x7f0a0304

.field public static final preference_file_key:I = 0x7f0a0476

.field public static final previous:I = 0x7f0a0677

.field public static final processing:I = 0x7f0a0240

.field public static final progress_dialog_message:I = 0x7f0a02fd

.field public static final prompt_generic_hintset_eightth_1:I = 0x7f0a0546

.field public static final prompt_generic_hintset_eightth_2:I = 0x7f0a0547

.field public static final prompt_generic_hintset_eightth_3:I = 0x7f0a0548

.field public static final prompt_generic_hintset_eightth_4:I = 0x7f0a0549

.field public static final prompt_generic_hintset_fifth_1:I = 0x7f0a053c

.field public static final prompt_generic_hintset_fifth_2:I = 0x7f0a053d

.field public static final prompt_generic_hintset_fifth_3:I = 0x7f0a053e

.field public static final prompt_generic_hintset_fifth_4:I = 0x7f0a053f

.field public static final prompt_generic_hintset_first_1:I = 0x7f0a052f

.field public static final prompt_generic_hintset_first_2:I = 0x7f0a0530

.field public static final prompt_generic_hintset_first_3:I = 0x7f0a0531

.field public static final prompt_generic_hintset_first_4:I = 0x7f0a0532

.field public static final prompt_generic_hintset_fourth_1:I = 0x7f0a0538

.field public static final prompt_generic_hintset_fourth_2:I = 0x7f0a0539

.field public static final prompt_generic_hintset_fourth_3:I = 0x7f0a053a

.field public static final prompt_generic_hintset_fourth_4:I = 0x7f0a053b

.field public static final prompt_generic_hintset_ninth_1:I = 0x7f0a054a

.field public static final prompt_generic_hintset_ninth_2:I = 0x7f0a054b

.field public static final prompt_generic_hintset_ninth_3:I = 0x7f0a054c

.field public static final prompt_generic_hintset_second_1:I = 0x7f0a0533

.field public static final prompt_generic_hintset_second_2:I = 0x7f0a0534

.field public static final prompt_generic_hintset_second_3:I = 0x7f0a0535

.field public static final prompt_generic_hintset_seventh_1:I = 0x7f0a0543

.field public static final prompt_generic_hintset_seventh_2:I = 0x7f0a0544

.field public static final prompt_generic_hintset_seventh_3:I = 0x7f0a0545

.field public static final prompt_generic_hintset_sixth_1:I = 0x7f0a0540

.field public static final prompt_generic_hintset_sixth_2:I = 0x7f0a0541

.field public static final prompt_generic_hintset_sixth_3:I = 0x7f0a0542

.field public static final prompt_generic_hintset_third_1:I = 0x7f0a0536

.field public static final prompt_generic_hintset_third_2:I = 0x7f0a0537

.field public static final prompt_group_location_share_missing:I = 0x7f0a05ee

.field public static final prompt_group_location_share_restart:I = 0x7f0a05ef

.field public static final prompt_group_location_sharing_stopped:I = 0x7f0a0621

.field public static final prompt_group_share_has_started:I = 0x7f0a0623

.field public static final prompt_launching_navigation:I = 0x7f0a061c

.field public static final prompt_location_no_contacts_found:I = 0x7f0a033d

.field public static final prompt_location_request_sent:I = 0x7f0a05f1

.field public static final prompt_location_sharing_confirmed:I = 0x7f0a0603

.field public static final prompt_nav_destination_changed_confirm:I = 0x7f0a05ff

.field public static final prompt_nav_directions:I = 0x7f0a04e9

.field public static final prompt_nav_group_request_confirm:I = 0x7f0a05fe

.field public static final prompt_nav_office:I = 0x7f0a0619

.field public static final prompt_nav_request_approved:I = 0x7f0a0607

.field public static final prompt_nav_request_confirm:I = 0x7f0a0213

.field public static final prompt_nav_route_confirm:I = 0x7f0a05ec

.field public static final prompt_nav_share_confirm:I = 0x7f0a05ed

.field public static final prompt_nav_share_request_confirm:I = 0x7f0a05f5

.field public static final prompt_nav_to_home:I = 0x7f0a0609

.field public static final prompt_nav_to_office:I = 0x7f0a060a

.field public static final prompt_noti_dialog_group_share_request_title:I = 0x7f0a0620

.field public static final prompt_noti_group_share_expired:I = 0x7f0a0622

.field public static final prompt_noti_ignore_invitations:I = 0x7f0a061e

.field public static final prompt_noti_ignore_loc_group_share_invitations:I = 0x7f0a061f

.field public static final prompt_request_location_dialog:I = 0x7f0a061d

.field public static final prompt_request_share_location:I = 0x7f0a084f

.field public static final prompt_schecule_directions_error:I = 0x7f0a033b

.field public static final prompt_smart_alert_notification:I = 0x7f0a022e

.field public static final prompt_starting_navigation:I = 0x7f0a061b

.field public static final prompt_view_location_changed_confirm:I = 0x7f0a0600

.field public static final prompt_view_location_request_confirm:I = 0x7f0a0601

.field public static final prompt_what_would_you_like_to_text_message:I = 0x7f0a05ea

.field public static final prompt_who_would_you_like_to_request:I = 0x7f0a0211

.field public static final prompt_who_would_you_like_to_share:I = 0x7f0a05eb

.field public static final prompt_who_would_you_like_to_text_message:I = 0x7f0a05e9

.field public static final qa_tts_NO_ANS_LOCAL_SEARCH:I = 0x7f0a0742

.field public static final qa_tts_NO_ANS_LOCAL_SEARCH_NON_US:I = 0x7f0a0743

.field public static final readout_btn:I = 0x7f0a0678

.field public static final real_time_tracking:I = 0x7f0a0487

.field public static final reco_error_0:I = 0x7f0a04df

.field public static final reco_error_1:I = 0x7f0a04e0

.field public static final reco_error_10:I = 0x7f0a04e1

.field public static final reco_error_2:I = 0x7f0a04e2

.field public static final reco_error_3:I = 0x7f0a04e3

.field public static final reco_error_4:I = 0x7f0a04e4

.field public static final reco_error_5:I = 0x7f0a04e5

.field public static final reco_error_6:I = 0x7f0a04e6

.field public static final reco_error_7:I = 0x7f0a04e7

.field public static final reco_error_8:I = 0x7f0a04e8

.field public static final reco_error_text:I = 0x7f0a0460

.field public static final reco_warning_nothing_recognized:I = 0x7f0a07f0

.field public static final recognized_wakeup:I = 0x7f0a0805

.field public static final register_carmode_1:I = 0x7f0a04b9

.field public static final register_carmode_2:I = 0x7f0a04ba

.field public static final register_carmode_3:I = 0x7f0a04bb

.field public static final register_carmode_4:I = 0x7f0a04bc

.field public static final register_carmode_5:I = 0x7f0a04bd

.field public static final register_carmode_tip:I = 0x7f0a04be

.field public static final register_terms_of_service_agree:I = 0x7f0a0278

.field public static final register_terms_of_service_title1:I = 0x7f0a0276

.field public static final register_terms_of_service_title2:I = 0x7f0a0277

.field public static final register_tutorial_01_explain_01:I = 0x7f0a027a

.field public static final register_tutorial_01_explain_02:I = 0x7f0a027b

.field public static final register_tutorial_01_explain_03:I = 0x7f0a027c

.field public static final register_tutorial_01_explain_04_land:I = 0x7f0a027d

.field public static final register_tutorial_01_explain_04_port:I = 0x7f0a027e

.field public static final register_tutorial_01_title:I = 0x7f0a0279

.field public static final register_tutorial_02_call_james:I = 0x7f0a0282

.field public static final register_tutorial_03_explain_land:I = 0x7f0a0281

.field public static final register_tutorial_03_explain_port:I = 0x7f0a0280

.field public static final register_tutorial_03_title:I = 0x7f0a027f

.field public static final register_tutorial_bubble_text_01:I = 0x7f0a0271

.field public static final register_tutorial_bubble_text_02:I = 0x7f0a0272

.field public static final register_tutorial_bubble_text_03:I = 0x7f0a0273

.field public static final register_tutorial_bubble_text_04:I = 0x7f0a0274

.field public static final register_tutorial_bubble_text_05:I = 0x7f0a0275

.field public static final register_tutorial_guidance_explain:I = 0x7f0a0285

.field public static final register_tutorial_loading:I = 0x7f0a0284

.field public static final register_tutorial_name:I = 0x7f0a0283

.field public static final register_warning:I = 0x7f0a0270

.field public static final register_welcome:I = 0x7f0a026e

.field public static final register_welcome_explain:I = 0x7f0a026f

.field public static final register_welcome_samsung:I = 0x7f0a026d

.field public static final reinvite:I = 0x7f0a0844

.field public static final reminder:I = 0x7f0a063d

.field public static final reply_btn:I = 0x7f0a0679

.field public static final reset_dialog_message:I = 0x7f0a0705

.field public static final retry_wake_up_word:I = 0x7f0a071c

.field public static final returning_to_carmode_1:I = 0x7f0a04c2

.field public static final returning_to_carmode_2:I = 0x7f0a04c3

.field public static final returning_to_carmode_tip:I = 0x7f0a04c4

.field public static final route:I = 0x7f0a0851

.field public static final s_voice_disclaimer_not_confirmed:I = 0x7f0a0242

.field public static final safereader_simulation_text:I = 0x7f0a0615

.field public static final samsung_powered_by_vlingo:I = 0x7f0a07f6

.field public static final samsung_version:I = 0x7f0a0808

.field public static final sat1:I = 0x7f0a0737

.field public static final save_btn:I = 0x7f0a067a

.field public static final say_keywords_for_song:I = 0x7f0a024b

.field public static final say_save_memo:I = 0x7f0a05c3

.field public static final say_send_or_replace:I = 0x7f0a0246

.field public static final say_the_command:I = 0x7f0a0249

.field public static final say_the_destination:I = 0x7f0a0247

.field public static final say_the_destination_port:I = 0x7f0a04dd

.field public static final say_the_message:I = 0x7f0a0245

.field public static final say_the_name:I = 0x7f0a020f

.field public static final say_the_name_again:I = 0x7f0a024c

.field public static final say_what_you_want:I = 0x7f0a067b

.field public static final say_which_phone_number:I = 0x7f0a024a

.field public static final say_who_would_you_like_to_request:I = 0x7f0a024d

.field public static final say_who_would_you_like_to_share:I = 0x7f0a024e

.field public static final schedule:I = 0x7f0a07bf

.field public static final schedule_notification_summary:I = 0x7f0a07c0

.field public static final screen_title_home:I = 0x7f0a05d5

.field public static final screen_title_location:I = 0x7f0a05d9

.field public static final screen_title_message_inbox:I = 0x7f0a05dc

.field public static final screen_title_message_suggestions:I = 0x7f0a05db

.field public static final screen_title_music:I = 0x7f0a05d8

.field public static final screen_title_phone_logs:I = 0x7f0a05d7

.field public static final screen_title_phone_suggestions:I = 0x7f0a05d6

.field public static final screen_title_sharing:I = 0x7f0a05da

.field public static final search_contacts:I = 0x7f0a0243

.field public static final search_destination:I = 0x7f0a0845

.field public static final search_label_button:I = 0x7f0a079b

.field public static final search_music:I = 0x7f0a04ea

.field public static final search_sip_result:I = 0x7f0a0244

.field public static final searching:I = 0x7f0a021a

.field public static final select_bookmark:I = 0x7f0a07a5

.field public static final selected_function_already_exist:I = 0x7f0a071b

.field public static final send_btn:I = 0x7f0a067c

.field public static final send_message_to:I = 0x7f0a07c4

.field public static final sending_msg_2:I = 0x7f0a04ce

.field public static final sending_msg_3:I = 0x7f0a04cf

.field public static final sending_msg_4:I = 0x7f0a04d0

.field public static final sending_msg_5:I = 0x7f0a04d1

.field public static final sending_msg_tip_1:I = 0x7f0a04d2

.field public static final sending_msg_tip_2:I = 0x7f0a04d3

.field public static final sentence_separators:I = 0x7f0a0504

.field public static final set:I = 0x7f0a067d

.field public static final set_address:I = 0x7f0a0315

.field public static final setting_about_version:I = 0x7f0a0439

.field public static final setting_dest_2:I = 0x7f0a04d4

.field public static final setting_dest_3:I = 0x7f0a04d5

.field public static final setting_dest_4:I = 0x7f0a04d6

.field public static final setting_dest_tip_1:I = 0x7f0a04d7

.field public static final setting_dest_tip_2:I = 0x7f0a04d8

.field public static final setting_myplace_hint_search:I = 0x7f0a0437

.field public static final setting_wakeup_idle_hint:I = 0x7f0a0721

.field public static final setting_wakeup_recognize_fail_try_again:I = 0x7f0a067e

.field public static final setting_wakeup_screenoff:I = 0x7f0a080b

.field public static final setting_wakeup_screenoff_title:I = 0x7f0a080a

.field public static final setting_wakeup_secure_body:I = 0x7f0a0826

.field public static final setting_wakeup_secure_popupbody:I = 0x7f0a0827

.field public static final setting_wakeup_secure_title:I = 0x7f0a0825

.field public static final setting_wakeup_set_wakeup_command_dialog_title:I = 0x7f0a0680

.field public static final setting_wakeup_set_wakeup_command_reset:I = 0x7f0a0681

.field public static final setting_wakeup_speak_now:I = 0x7f0a0682

.field public static final setting_wakeup_tts_RECOGNIZE_PARTIAL_SUCCESS_LOCK_ADAPT:I = 0x7f0a0724

.field public static final setting_wakeup_tts_RECOGNIZE_PARTIAL_SUCCESS_LOCK_ADAPT_US:I = 0x7f0a0725

.field public static final setting_wakeup_tts_RECOGNIZE_SUCCESS:I = 0x7f0a0683

.field public static final setting_wakeup_tts_RECOGNIZE_SUCCESS_FUNCTION:I = 0x7f0a0722

.field public static final setting_wakeup_tts_RECOGNIZE_SUCCESS_FUNCTION_US:I = 0x7f0a0723

.field public static final setting_wakeup_tts_RECOGNIZE_SUCCESS_FUNCTION_toast:I = 0x7f0a07e3

.field public static final setting_wakeup_tts_RECOGNIZE_SUCCESS_LOCK_ADAPT:I = 0x7f0a0726

.field public static final setting_wakeup_tts_RECOGNIZE_SUCCESS_toast:I = 0x7f0a07e2

.field public static final setting_wakeup_tts_TAP_BUTTON_BELOW:I = 0x7f0a0684

.field public static final setting_wakeup_tts_TAP_BUTTON_BELOW_UNLOCK:I = 0x7f0a071f

.field public static final setting_wakeup_tts_TAP_BUTTON_BELOW_UNLOCK_ADAPT:I = 0x7f0a0720

.field public static final setting_wakeup_unlock_screen:I = 0x7f0a0728

.field public static final setting_wakeup_unlock_screen_non_motion:I = 0x7f0a0729

.field public static final setting_wakeup_unlock_screen_title:I = 0x7f0a073c

.field public static final settings_EnableAudioFileLog:I = 0x7f0a0692

.field public static final settings_EnableServerLog:I = 0x7f0a0693

.field public static final settings_EnableTimingLog:I = 0x7f0a0694

.field public static final settings_account:I = 0x7f0a0402

.field public static final settings_account_not_signed_in:I = 0x7f0a0435

.field public static final settings_account_not_signed_in_app:I = 0x7f0a0436

.field public static final settings_account_sign_in_needed:I = 0x7f0a0403

.field public static final settings_accounts:I = 0x7f0a0405

.field public static final settings_active_running_display:I = 0x7f0a03df

.field public static final settings_active_running_display_subtitle:I = 0x7f0a03e0

.field public static final settings_add_account:I = 0x7f0a0685

.field public static final settings_add_contacts:I = 0x7f0a0446

.field public static final settings_advanced_BTlisten_summary:I = 0x7f0a0688

.field public static final settings_advanced_BTlisten_title:I = 0x7f0a0689

.field public static final settings_advanced_autopunc_summary:I = 0x7f0a0686

.field public static final settings_advanced_autopunc_title:I = 0x7f0a0687

.field public static final settings_advanced_useloc_summary:I = 0x7f0a068a

.field public static final settings_advanced_useloc_title:I = 0x7f0a068b

.field public static final settings_auto_exit:I = 0x7f0a03f1

.field public static final settings_auto_exit_subtitle:I = 0x7f0a03f2

.field public static final settings_auto_launch:I = 0x7f0a03ef

.field public static final settings_auto_launch_subtitle:I = 0x7f0a03f0

.field public static final settings_auto_suggestion_contacts:I = 0x7f0a0453

.field public static final settings_auto_suggestion_contacts_subtitle:I = 0x7f0a0454

.field public static final settings_autodial_dialogtitle:I = 0x7f0a068c

.field public static final settings_autodial_summary:I = 0x7f0a068d

.field public static final settings_autodial_title:I = 0x7f0a068e

.field public static final settings_baidu:I = 0x7f0a03fb

.field public static final settings_basic_settings:I = 0x7f0a03d5

.field public static final settings_button_back:I = 0x7f0a0406

.field public static final settings_button_cancel:I = 0x7f0a0408

.field public static final settings_button_done:I = 0x7f0a0409

.field public static final settings_button_ok:I = 0x7f0a0407

.field public static final settings_button_save:I = 0x7f0a040a

.field public static final settings_button_set:I = 0x7f0a040b

.field public static final settings_call_title:I = 0x7f0a068f

.field public static final settings_car_rename_error:I = 0x7f0a03ec

.field public static final settings_click_to_login:I = 0x7f0a0690

.field public static final settings_click_to_login_weibo:I = 0x7f0a07b1

.field public static final settings_cmcc_navi:I = 0x7f0a03fd

.field public static final settings_commute_notification_from_dummy:I = 0x7f0a0431

.field public static final settings_commute_notification_prompt:I = 0x7f0a042a

.field public static final settings_commute_notification_prompt_mini:I = 0x7f0a042b

.field public static final settings_commute_notification_set_date:I = 0x7f0a0438

.field public static final settings_commute_notification_summary:I = 0x7f0a0411

.field public static final settings_commute_notification_title:I = 0x7f0a0410

.field public static final settings_commute_notification_to_dummy:I = 0x7f0a0432

.field public static final settings_configure_home_address:I = 0x7f0a0691

.field public static final settings_connected:I = 0x7f0a03e6

.field public static final settings_connecting:I = 0x7f0a03e7

.field public static final settings_custom_wakeup_speaknow:I = 0x7f0a0703

.field public static final settings_custom_wakeup_times:I = 0x7f0a0704

.field public static final settings_daum_maps:I = 0x7f0a03f7

.field public static final settings_detailed_tts_feedback_summary:I = 0x7f0a07ad

.field public static final settings_detailed_tts_feedback_title:I = 0x7f0a07ac

.field public static final settings_device_connected:I = 0x7f0a03e9

.field public static final settings_dialog_cancel:I = 0x7f0a043b

.field public static final settings_dialog_ok:I = 0x7f0a043a

.field public static final settings_disconnect:I = 0x7f0a03ea

.field public static final settings_disconnect_msg:I = 0x7f0a03eb

.field public static final settings_edit_suggested_contacts:I = 0x7f0a0445

.field public static final settings_fuel_type:I = 0x7f0a043e

.field public static final settings_fuel_type_diesel:I = 0x7f0a044e

.field public static final settings_fuel_type_lpg:I = 0x7f0a044f

.field public static final settings_fuel_type_petrol_mid:I = 0x7f0a044c

.field public static final settings_fuel_type_petrol_premium:I = 0x7f0a044d

.field public static final settings_fuel_type_petrol_regular:I = 0x7f0a044b

.field public static final settings_general:I = 0x7f0a0695

.field public static final settings_go_home:I = 0x7f0a0430

.field public static final settings_go_to_work:I = 0x7f0a042f

.field public static final settings_google_navi:I = 0x7f0a03f6

.field public static final settings_header_settings:I = 0x7f0a03d4

.field public static final settings_headset_mode:I = 0x7f0a0696

.field public static final settings_headset_mode_summary:I = 0x7f0a0697

.field public static final settings_headset_mode_title:I = 0x7f0a0698

.field public static final settings_help_set_home_address:I = 0x7f0a067f

.field public static final settings_holiday:I = 0x7f0a042e

.field public static final settings_home:I = 0x7f0a0400

.field public static final settings_incar_BTlanuch_summary:I = 0x7f0a069b

.field public static final settings_incar_BTlaunch_title:I = 0x7f0a069c

.field public static final settings_incar_autostart_speakerphone_summary:I = 0x7f0a0699

.field public static final settings_incar_autostart_speakerphone_title:I = 0x7f0a069a

.field public static final settings_incar_homeaddress_summary:I = 0x7f0a069d

.field public static final settings_incar_homeaddress_title:I = 0x7f0a069e

.field public static final settings_incar_motion_summary:I = 0x7f0a069f

.field public static final settings_incar_motion_title:I = 0x7f0a06a0

.field public static final settings_incar_navigation:I = 0x7f0a06a1

.field public static final settings_incar_officeaddress_summary:I = 0x7f0a0861

.field public static final settings_incar_officeaddress_title:I = 0x7f0a0862

.field public static final settings_incar_onwhilecharging_summary:I = 0x7f0a06a2

.field public static final settings_incar_onwhilecharging_title:I = 0x7f0a06a3

.field public static final settings_incar_say_your_wakeup_title:I = 0x7f0a0701

.field public static final settings_incar_set_summary:I = 0x7f0a06a4

.field public static final settings_incar_set_title:I = 0x7f0a06a5

.field public static final settings_incar_speakshow_msgbodies_summary:I = 0x7f0a06a6

.field public static final settings_incar_speakshow_msgbodies_title:I = 0x7f0a06a7

.field public static final settings_incar_texttospeech:I = 0x7f0a06a8

.field public static final settings_incar_wakeup:I = 0x7f0a06a9

.field public static final settings_incar_wakeup_summary:I = 0x7f0a05c1

.field public static final settings_incar_wakeup_title:I = 0x7f0a06aa

.field public static final settings_information:I = 0x7f0a0441

.field public static final settings_language:I = 0x7f0a0444

.field public static final settings_language_dialogtitle:I = 0x7f0a06ab

.field public static final settings_language_summary:I = 0x7f0a06ac

.field public static final settings_language_title:I = 0x7f0a06ad

.field public static final settings_launch_voicetalk_summary:I = 0x7f0a06ae

.field public static final settings_launch_voicetalk_title:I = 0x7f0a06af

.field public static final settings_location:I = 0x7f0a040e

.field public static final settings_location_listing_preference:I = 0x7f0a0448

.field public static final settings_logged_in_as:I = 0x7f0a06b0

.field public static final settings_login_facebook:I = 0x7f0a06b1

.field public static final settings_login_twitter:I = 0x7f0a06b2

.field public static final settings_login_weibo:I = 0x7f0a07b0

.field public static final settings_logout:I = 0x7f0a06b3

.field public static final settings_message:I = 0x7f0a040d

.field public static final settings_my_car:I = 0x7f0a03e5

.field public static final settings_my_car_add_car:I = 0x7f0a0415

.field public static final settings_my_car_after_pairing_autolaunch:I = 0x7f0a0421

.field public static final settings_my_car_connected_profiles:I = 0x7f0a0424

.field public static final settings_my_car_connected_profiles_on:I = 0x7f0a0425

.field public static final settings_my_car_connected_remove:I = 0x7f0a0423

.field public static final settings_my_car_connected_rename:I = 0x7f0a0422

.field public static final settings_my_car_connected_unregistered_msg:I = 0x7f0a0416

.field public static final settings_my_car_connected_unregistered_msg_basic:I = 0x7f0a0417

.field public static final settings_my_car_connected_unregistered_msg_basic2:I = 0x7f0a0418

.field public static final settings_my_car_not_connected_cant_find:I = 0x7f0a041c

.field public static final settings_my_car_not_connected_msg:I = 0x7f0a04dc

.field public static final settings_my_car_not_connected_msg1:I = 0x7f0a0419

.field public static final settings_my_car_not_connected_msg2:I = 0x7f0a041a

.field public static final settings_my_car_not_connected_msg3:I = 0x7f0a041b

.field public static final settings_my_car_not_connected_register:I = 0x7f0a041e

.field public static final settings_my_car_not_connected_skip_msg:I = 0x7f0a041d

.field public static final settings_my_car_prev_registered_add:I = 0x7f0a0420

.field public static final settings_my_car_prev_registered_vehicle:I = 0x7f0a041f

.field public static final settings_my_car_registered_car:I = 0x7f0a0414

.field public static final settings_my_location:I = 0x7f0a03f3

.field public static final settings_my_place:I = 0x7f0a03fe

.field public static final settings_my_place_sub:I = 0x7f0a03ff

.field public static final settings_my_place_subtitle:I = 0x7f0a0427

.field public static final settings_myplace_hint_search:I = 0x7f0a0433

.field public static final settings_naver_maps:I = 0x7f0a03f8

.field public static final settings_navigate_up:I = 0x7f0a03f4

.field public static final settings_navigation:I = 0x7f0a03f5

.field public static final settings_navigation_subtitle:I = 0x7f0a0426

.field public static final settings_nearby_parking:I = 0x7f0a044a

.field public static final settings_nearby_petrol_stations:I = 0x7f0a0449

.field public static final settings_no_more_registered_samsung_account:I = 0x7f0a0443

.field public static final settings_not_connected:I = 0x7f0a03e8

.field public static final settings_not_registered:I = 0x7f0a03ee

.field public static final settings_off:I = 0x7f0a0457

.field public static final settings_office:I = 0x7f0a0401

.field public static final settings_olleh_navi:I = 0x7f0a03fa

.field public static final settings_order:I = 0x7f0a0452

.field public static final settings_order_distance:I = 0x7f0a0450

.field public static final settings_order_price:I = 0x7f0a0451

.field public static final settings_phone:I = 0x7f0a040c

.field public static final settings_phone_messages:I = 0x7f0a0447

.field public static final settings_profanityfilter_summary:I = 0x7f0a06b4

.field public static final settings_profanityfilter_title:I = 0x7f0a06b5

.field public static final settings_readout:I = 0x7f0a03e1

.field public static final settings_readout_notification:I = 0x7f0a03e3

.field public static final settings_readout_notification_subtitle:I = 0x7f0a03e4

.field public static final settings_readout_speed:I = 0x7f0a03e2

.field public static final settings_register_car:I = 0x7f0a043c

.field public static final settings_register_my_place:I = 0x7f0a042c

.field public static final settings_register_obd:I = 0x7f0a043d

.field public static final settings_registered:I = 0x7f0a03ed

.field public static final settings_reject_message:I = 0x7f0a03db

.field public static final settings_reject_message_alert:I = 0x7f0a03dd

.field public static final settings_reject_message_default:I = 0x7f0a03dc

.field public static final settings_reject_message_max_error:I = 0x7f0a03de

.field public static final settings_repeat_day:I = 0x7f0a0428

.field public static final settings_samsung_account:I = 0x7f0a0404

.field public static final settings_samsung_account_skip:I = 0x7f0a0434

.field public static final settings_samsungaccount:I = 0x7f0a04f7

.field public static final settings_search:I = 0x7f0a03d6

.field public static final settings_search_engine_dialogtitle:I = 0x7f0a06b6

.field public static final settings_search_engine_title:I = 0x7f0a06b7

.field public static final settings_server_data_reset:I = 0x7f0a0458

.field public static final settings_server_data_reset_content:I = 0x7f0a045a

.field public static final settings_server_data_reset_description1:I = 0x7f0a0459

.field public static final settings_server_data_reset_description2:I = 0x7f0a045b

.field public static final settings_server_data_reset_sub:I = 0x7f0a045c

.field public static final settings_set_favorite_contacts:I = 0x7f0a0455

.field public static final settings_set_favorite_contacts_subtitle:I = 0x7f0a0456

.field public static final settings_sign_in:I = 0x7f0a0429

.field public static final settings_smart_alert_summary:I = 0x7f0a0413

.field public static final settings_smart_alert_title:I = 0x7f0a0412

.field public static final settings_social_facebook_summary:I = 0x7f0a06b8

.field public static final settings_social_facebook_title:I = 0x7f0a06b9

.field public static final settings_social_qzone_summary:I = 0x7f0a06ba

.field public static final settings_social_qzone_title:I = 0x7f0a06bb

.field public static final settings_social_summary:I = 0x7f0a06bc

.field public static final settings_social_title:I = 0x7f0a06bd

.field public static final settings_social_twitter_summary:I = 0x7f0a06be

.field public static final settings_social_twitter_title:I = 0x7f0a06bf

.field public static final settings_space_accounts:I = 0x7f0a06c0

.field public static final settings_space_minute:I = 0x7f0a06c1

.field public static final settings_space_minutes:I = 0x7f0a06c2

.field public static final settings_suggestion_phone_add:I = 0x7f0a0440

.field public static final settings_suggestion_phone_category:I = 0x7f0a043f

.field public static final settings_terms_of_use:I = 0x7f0a0442

.field public static final settings_tmap:I = 0x7f0a03f9

.field public static final settings_traffic_alert:I = 0x7f0a040f

.field public static final settings_uplus_navi:I = 0x7f0a03fc

.field public static final settings_user_data_deletion_confirm_server_failed:I = 0x7f0a045d

.field public static final settings_uuid_summary:I = 0x7f0a06c3

.field public static final settings_uuid_title:I = 0x7f0a06c4

.field public static final settings_voice_talk_help_title:I = 0x7f0a06c5

.field public static final settings_voice_wake_up:I = 0x7f0a03d7

.field public static final settings_voice_wake_up_subtitle:I = 0x7f0a03d8

.field public static final settings_wakeup_via_bt_headset:I = 0x7f0a03d9

.field public static final settings_wakeup_via_bt_headset_subtitle:I = 0x7f0a03da

.field public static final settings_weekday:I = 0x7f0a042d

.field public static final setup_basic_settings_next:I = 0x7f0a03c3

.field public static final setup_button_cancel:I = 0x7f0a03c2

.field public static final setup_button_done:I = 0x7f0a03c0

.field public static final setup_button_ok:I = 0x7f0a03c1

.field public static final setup_my_car:I = 0x7f0a03bb

.field public static final setup_my_car_subtitle:I = 0x7f0a03bc

.field public static final setup_my_place:I = 0x7f0a03be

.field public static final setup_my_place_subtitle:I = 0x7f0a03bf

.field public static final setup_navigation:I = 0x7f0a03bd

.field public static final share:I = 0x7f0a084c

.field public static final shortcut_name:I = 0x7f0a07a7

.field public static final shortcut_name_hint:I = 0x7f0a07a9

.field public static final simcard_not_registred:I = 0x7f0a02ff

.field public static final skip:I = 0x7f0a06c6

.field public static final social_facebook_comm_error:I = 0x7f0a06c7

.field public static final social_facebook_login_error:I = 0x7f0a06c8

.field public static final social_no:I = 0x7f0a06cb

.field public static final social_not_supported:I = 0x7f0a073d

.field public static final social_qzone_login_error:I = 0x7f0a06c9

.field public static final social_twitter_login_error:I = 0x7f0a06cc

.field public static final social_wait_logging_in:I = 0x7f0a06cd

.field public static final social_weibo_login_error:I = 0x7f0a06ca

.field public static final social_yes:I = 0x7f0a06ce

.field public static final space:I = 0x7f0a052d

.field public static final speak_now:I = 0x7f0a023f

.field public static final splash_screen:I = 0x7f0a0231

.field public static final ssaccount_accept_message:I = 0x7f0a034a

.field public static final ssaccount_decline_message:I = 0x7f0a0349

.field public static final ssaccount_text_dialog:I = 0x7f0a0348

.field public static final ssaccount_title_dialog:I = 0x7f0a0347

.field public static final start_automation_playback:I = 0x7f0a0785

.field public static final start_automation_recording:I = 0x7f0a0787

.field public static final starting_voice_recorder:I = 0x7f0a07e6

.field public static final stop_automation_playback:I = 0x7f0a0786

.field public static final stop_automation_recording:I = 0x7f0a0788

.field public static final string_for_replace:I = 0x7f0a04d9

.field public static final success:I = 0x7f0a0823

.field public static final sun1:I = 0x7f0a0731

.field public static final sview_cover_alwaysmicon:I = 0x7f0a081f

.field public static final svoice_btn:I = 0x7f0a0287

.field public static final svoice_disabled_exit_popup:I = 0x7f0a04ab

.field public static final svoice_launch_poopup_text:I = 0x7f0a0286

.field public static final svoice_not_ready:I = 0x7f0a05dd

.field public static final svoice_unlock:I = 0x7f0a073b

.field public static final talkback_album_img:I = 0x7f0a0379

.field public static final talkback_bubble_editable:I = 0x7f0a0796

.field public static final talkback_clear_query_button:I = 0x7f0a0855

.field public static final talkback_mic_button_idle:I = 0x7f0a0791

.field public static final talkback_mic_button_recognizing:I = 0x7f0a0792

.field public static final talkback_navigate_up:I = 0x7f0a0856

.field public static final talkback_next_btn:I = 0x7f0a0374

.field public static final talkback_pause_btn:I = 0x7f0a0375

.field public static final talkback_play_btn:I = 0x7f0a0376

.field public static final talkback_prev_btn:I = 0x7f0a0373

.field public static final talkback_search:I = 0x7f0a0854

.field public static final talkback_search_back_btn:I = 0x7f0a037c

.field public static final talkback_search_clear_btn:I = 0x7f0a037d

.field public static final talkback_shuffle_off_btn:I = 0x7f0a0377

.field public static final talkback_shuffle_on_btn:I = 0x7f0a0378

.field public static final talkback_voice_prompt_off:I = 0x7f0a0794

.field public static final talkback_voice_prompt_on:I = 0x7f0a0793

.field public static final talkback_volum_btn:I = 0x7f0a037b

.field public static final talkback_volume_seekbar:I = 0x7f0a037a

.field public static final talkback_widget_detail:I = 0x7f0a0795

.field public static final talkback_you_said:I = 0x7f0a07a2

.field public static final test_schedule:I = 0x7f0a0783

.field public static final text_copied_to_clipboard:I = 0x7f0a082f

.field public static final text_message:I = 0x7f0a058b

.field public static final this_menu_is_not_available_while_driving:I = 0x7f0a045e

.field public static final thu1:I = 0x7f0a0735

.field public static final time_prefix_0_1:I = 0x7f0a0821

.field public static final time_prefix_2_23:I = 0x7f0a0822

.field public static final time_suffix_am:I = 0x7f0a0477

.field public static final time_suffix_pm:I = 0x7f0a0478

.field public static final timer_hour:I = 0x7f0a06cf

.field public static final timer_hr:I = 0x7f0a06d0

.field public static final timer_hrs:I = 0x7f0a0777

.field public static final timer_min:I = 0x7f0a06d1

.field public static final timer_mins:I = 0x7f0a0778

.field public static final timer_minute:I = 0x7f0a06d2

.field public static final timer_sec:I = 0x7f0a06d3

.field public static final timer_second:I = 0x7f0a06d4

.field public static final timer_seconds:I = 0x7f0a0779

.field public static final timer_widget_time_info:I = 0x7f0a07aa

.field public static final tip:I = 0x7f0a04b8

.field public static final tip_bubble_on_wake_up_function_list:I = 0x7f0a07dd

.field public static final tip_bubble_on_wake_up_function_list_h_device:I = 0x7f0a080c

.field public static final tip_bubble_on_wake_up_whole_list:I = 0x7f0a07dc

.field public static final title_activity_location_request:I = 0x7f0a029f

.field public static final title_activity_location_request_confirm:I = 0x7f0a02a0

.field public static final title_activity_location_share:I = 0x7f0a03b2

.field public static final title_activity_location_share_confirm:I = 0x7f0a02a1

.field public static final title_activity_location_share_map:I = 0x7f0a03b5

.field public static final title_activity_location_share_options:I = 0x7f0a02a2

.field public static final title_address:I = 0x7f0a070a

.field public static final title_birthday:I = 0x7f0a070b

.field public static final title_bookmark_shortcut:I = 0x7f0a076f

.field public static final title_change_voice_command:I = 0x7f0a0717

.field public static final title_change_voice_function:I = 0x7f0a0718

.field public static final title_check_missed_call:I = 0x7f0a070c

.field public static final title_check_missed_message:I = 0x7f0a070d

.field public static final title_direct_dial:I = 0x7f0a076d

.field public static final title_direct_message:I = 0x7f0a076e

.field public static final title_listen_my_voice_command:I = 0x7f0a0719

.field public static final title_location_group_destination:I = 0x7f0a02a5

.field public static final title_location_group_inv_participants:I = 0x7f0a02a8

.field public static final title_location_group_set_duration:I = 0x7f0a02a4

.field public static final title_location_group_setting_destination:I = 0x7f0a02a6

.field public static final title_location_group_setting_destination_edit_text:I = 0x7f0a02a7

.field public static final title_location_group_sharing_participants:I = 0x7f0a02a9

.field public static final title_location_inrix_place:I = 0x7f0a04f6

.field public static final title_location_share_group:I = 0x7f0a02a3

.field public static final title_location_share_group_detail:I = 0x7f0a02c7

.field public static final title_navigation_shortcut:I = 0x7f0a0770

.field public static final title_notification:I = 0x7f0a071a

.field public static final title_play_music:I = 0x7f0a070f

.field public static final title_play_radio:I = 0x7f0a0710

.field public static final title_record_voice:I = 0x7f0a0711

.field public static final title_voice_camera:I = 0x7f0a070e

.field public static final title_wake_up_auto_function1:I = 0x7f0a0713

.field public static final title_wake_up_auto_function2:I = 0x7f0a0714

.field public static final title_wake_up_auto_function3:I = 0x7f0a0715

.field public static final title_wake_up_auto_function4:I = 0x7f0a0716

.field public static final title_wake_up_voice_talk:I = 0x7f0a0712

.field public static final to_:I = 0x7f0a084d

.field public static final tos_accept:I = 0x7f0a06d5

.field public static final tos_accept_for_pt_br:I = 0x7f0a0780

.field public static final tos_accept_samsung:I = 0x7f0a0626

.field public static final tos_accept_samsung_for_pt_br:I = 0x7f0a077f

.field public static final tos_already_accepted_still_binding_base:I = 0x7f0a083e

.field public static final tos_decline:I = 0x7f0a06d6

.field public static final tos_decline_for_pt_br:I = 0x7f0a0781

.field public static final tos_decline_samsung:I = 0x7f0a06d7

.field public static final tos_dialog_view_terms_old:I = 0x7f0a06d8

.field public static final tos_dialog_view_terms_old_for_pt_br:I = 0x7f0a077e

.field public static final tos_dialog_view_terms_samsung:I = 0x7f0a0627

.field public static final tos_dialog_view_terms_samsung_for_pt_br:I = 0x7f0a077c

.field public static final tos_must_accept_to_use:I = 0x7f0a06d9

.field public static final tos_notice_regarding_service_agreement:I = 0x7f0a083d

.field public static final tos_popup_negative_button:I = 0x7f0a083c

.field public static final tos_popup_positive_button:I = 0x7f0a083b

.field public static final tos_popup_text:I = 0x7f0a083a

.field public static final tos_popup_title:I = 0x7f0a0839

.field public static final tos_url_href:I = 0x7f0a083f

.field public static final tos_vlingo_terms:I = 0x7f0a06da

.field public static final tos_vlingo_terms_for_pt_br:I = 0x7f0a077d

.field public static final tos_vlingo_terms_samsung:I = 0x7f0a06db

.field public static final tos_vlingo_terms_samsung_for_pt_br:I = 0x7f0a077b

.field public static final try_again:I = 0x7f0a06f8

.field public static final tts_more_options:I = 0x7f0a0852

.field public static final tts_music_back:I = 0x7f0a0857

.field public static final tts_music_fastforward_button:I = 0x7f0a085f

.field public static final tts_music_pause_button:I = 0x7f0a085d

.field public static final tts_music_play_button:I = 0x7f0a085c

.field public static final tts_music_rewind_button:I = 0x7f0a085e

.field public static final tts_music_seekbar:I = 0x7f0a0858

.field public static final tts_music_shuffle_off:I = 0x7f0a085b

.field public static final tts_music_shuffle_on:I = 0x7f0a085a

.field public static final tts_music_volume:I = 0x7f0a0859

.field public static final tts_time_format:I = 0x7f0a0771

.field public static final tue1:I = 0x7f0a0733

.field public static final unhandled_domain:I = 0x7f0a0294

.field public static final unit_temperature_farenheit:I = 0x7f0a0744

.field public static final unknown_search_1:I = 0x7f0a079c

.field public static final unknown_search_2:I = 0x7f0a079d

.field public static final unknown_search_3:I = 0x7f0a079e

.field public static final unknown_search_4:I = 0x7f0a079f

.field public static final unknown_search_5:I = 0x7f0a07a0

.field public static final unknown_search_6:I = 0x7f0a07a1

.field public static final unlock_screen:I = 0x7f0a07c1

.field public static final unlock_screen_contents_summary:I = 0x7f0a07c2

.field public static final unlock_screen_contents_summary_wifi:I = 0x7f0a07c3

.field public static final update:I = 0x7f0a0493

.field public static final update_appname:I = 0x7f0a0491

.field public static final update_btn:I = 0x7f0a06f9

.field public static final update_description:I = 0x7f0a0490

.field public static final update_downloading:I = 0x7f0a0496

.field public static final update_fail:I = 0x7f0a0498

.field public static final update_install1:I = 0x7f0a0492

.field public static final update_install2:I = 0x7f0a0495

.field public static final update_installing:I = 0x7f0a0497

.field public static final update_later:I = 0x7f0a0494

.field public static final update_progress:I = 0x7f0a049b

.field public static final update_remain:I = 0x7f0a0499

.field public static final update_total:I = 0x7f0a049a

.field public static final user_data_deletion_confirm:I = 0x7f0a0840

.field public static final user_data_deletion_confirm_server_failed:I = 0x7f0a0841

.field public static final user_data_deletion_confirm_title:I = 0x7f0a0842

.field public static final util_WEB_SEARCH_NAME_DEFAULT:I = 0x7f0a0517

.field public static final util_WEB_SEARCH_NAME_DEFAULT_CN:I = 0x7f0a0518

.field public static final util_WEB_SEARCH_URL_BAIDU_DEFAULT:I = 0x7f0a051c

.field public static final util_WEB_SEARCH_URL_BAIDU_HOME_DEFAULT:I = 0x7f0a051d

.field public static final util_WEB_SEARCH_URL_BING_DEFAULT:I = 0x7f0a051e

.field public static final util_WEB_SEARCH_URL_BING_HOME_DEFAULT:I = 0x7f0a051f

.field public static final util_WEB_SEARCH_URL_DAUM_DEFAULT:I = 0x7f0a052b

.field public static final util_WEB_SEARCH_URL_DAUM_HOME_DEFAULT:I = 0x7f0a052c

.field public static final util_WEB_SEARCH_URL_DEFAULT:I = 0x7f0a0520

.field public static final util_WEB_SEARCH_URL_DEFAULT_CN:I = 0x7f0a0521

.field public static final util_WEB_SEARCH_URL_GOOGLE_DEFAULT:I = 0x7f0a0523

.field public static final util_WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT:I = 0x7f0a0524

.field public static final util_WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT_US:I = 0x7f0a0522

.field public static final util_WEB_SEARCH_URL_HOME_DEFAULT:I = 0x7f0a0525

.field public static final util_WEB_SEARCH_URL_HOME_DEFAULT_CN:I = 0x7f0a0526

.field public static final util_WEB_SEARCH_URL_NAVER_DEFAULT:I = 0x7f0a0529

.field public static final util_WEB_SEARCH_URL_NAVER_HOME_DEFAULT:I = 0x7f0a052a

.field public static final util_WEB_SEARCH_URL_YAHOO_DEFAULT:I = 0x7f0a0527

.field public static final util_WEB_SEARCH_URL_YAHOO_HOME_DEFAULT:I = 0x7f0a0528

.field public static final video_call_not_supported:I = 0x7f0a05f2

.field public static final view_location:I = 0x7f0a0846

.field public static final voice_feedback:I = 0x7f0a0758

.field public static final voice_hi_galaxy:I = 0x7f0a0234

.field public static final voice_ind:I = 0x7f0a03ba

.field public static final voice_input_control_alarm:I = 0x7f0a0749

.field public static final voice_input_control_alarm_summary:I = 0x7f0a074a

.field public static final voice_input_control_camera:I = 0x7f0a074b

.field public static final voice_input_control_camera_summary:I = 0x7f0a074c

.field public static final voice_input_control_cancel:I = 0x7f0a0755

.field public static final voice_input_control_chatonv:I = 0x7f0a07ae

.field public static final voice_input_control_chatonv_summary:I = 0x7f0a07af

.field public static final voice_input_control_incomming_calls:I = 0x7f0a0747

.field public static final voice_input_control_incomming_calls_summary:I = 0x7f0a0748

.field public static final voice_input_control_message:I = 0x7f0a0752

.field public static final voice_input_control_message_no_call:I = 0x7f0a0753

.field public static final voice_input_control_message_santosh:I = 0x7f0a0807

.field public static final voice_input_control_music:I = 0x7f0a074d

.field public static final voice_input_control_music_summary:I = 0x7f0a074e

.field public static final voice_input_control_ok:I = 0x7f0a0754

.field public static final voice_input_control_radio:I = 0x7f0a074f

.field public static final voice_input_control_radio_summary:I = 0x7f0a0750

.field public static final voice_input_control_summry:I = 0x7f0a0746

.field public static final voice_input_control_title:I = 0x7f0a0745

.field public static final voice_input_control_warrning:I = 0x7f0a0751

.field public static final voice_location_ready:I = 0x7f0a0239

.field public static final voice_message_create_start:I = 0x7f0a0238

.field public static final voice_message_ready:I = 0x7f0a0237

.field public static final voice_message_sending:I = 0x7f0a0241

.field public static final voice_phone_ready:I = 0x7f0a0235

.field public static final voice_press_button:I = 0x7f0a0232

.field public static final voice_prompt_confirmation_cancel:I = 0x7f0a06dc

.field public static final voice_prompt_confirmation_disable:I = 0x7f0a06dd

.field public static final voice_prompt_confirmation_enable:I = 0x7f0a06de

.field public static final voice_prompt_confirmation_for_off:I = 0x7f0a06df

.field public static final voice_prompt_confirmation_for_on:I = 0x7f0a06e0

.field public static final voice_prompt_confirmation_ok:I = 0x7f0a06e1

.field public static final voice_say:I = 0x7f0a0233

.field public static final voice_tts_sample_text:I = 0x7f0a0236

.field public static final wake_up_enabled:I = 0x7f0a0597

.field public static final wake_up_record_already_exist:I = 0x7f0a082e

.field public static final wake_up_word_already_assigned:I = 0x7f0a071d

.field public static final wakeup_auto_function_1:I = 0x7f0a078e

.field public static final wakeup_auto_function_2:I = 0x7f0a078f

.field public static final wakeup_auto_function_3:I = 0x7f0a0790

.field public static final wakeup_lock_screen_dialog_message:I = 0x7f0a073f

.field public static final wakeup_lock_screen_dialog_message2:I = 0x7f0a0740

.field public static final wakeup_lock_screen_dialog_title:I = 0x7f0a073e

.field public static final wakeup_screen_off_dialog_message:I = 0x7f0a0809

.field public static final wakeup_setting_dialog_message:I = 0x7f0a0708

.field public static final wakeup_setting_dialog_title:I = 0x7f0a0709

.field public static final wakeup_voice_talk:I = 0x7f0a078d

.field public static final wcis_alarm:I = 0x7f0a0566

.field public static final wcis_alarm_ex:I = 0x7f0a0567

.field public static final wcis_alarm_lookup_ex:I = 0x7f0a0568

.field public static final wcis_check_weather:I = 0x7f0a0585

.field public static final wcis_check_weather_ex:I = 0x7f0a0586

.field public static final wcis_driving_mode:I = 0x7f0a0569

.field public static final wcis_driving_mode_ex:I = 0x7f0a056a

.field public static final wcis_edit_what_you_said:I = 0x7f0a06f3

.field public static final wcis_edit_what_you_said_caption:I = 0x7f0a06f4

.field public static final wcis_find_contact:I = 0x7f0a06f1

.field public static final wcis_findcontact_ex:I = 0x7f0a0581

.field public static final wcis_general_help:I = 0x7f0a06e2

.field public static final wcis_get_an_answers:I = 0x7f0a0587

.field public static final wcis_get_an_answers_ex:I = 0x7f0a0588

.field public static final wcis_handwriting_caption:I = 0x7f0a06f2

.field public static final wcis_how_to_use:I = 0x7f0a06e3

.field public static final wcis_how_to_use_VC_caption:I = 0x7f0a06e5

.field public static final wcis_how_to_use_caption:I = 0x7f0a06e4

.field public static final wcis_info_row_more_info:I = 0x7f0a06e6

.field public static final wcis_local_listings:I = 0x7f0a059a

.field public static final wcis_local_listings_ex:I = 0x7f0a059b

.field public static final wcis_memo:I = 0x7f0a06e7

.field public static final wcis_memo_ex:I = 0x7f0a056b

.field public static final wcis_memo_in_car_ex:I = 0x7f0a056c

.field public static final wcis_music:I = 0x7f0a056d

.field public static final wcis_music_ex:I = 0x7f0a056e

.field public static final wcis_music_ex2:I = 0x7f0a056f

.field public static final wcis_nav:I = 0x7f0a06e8

.field public static final wcis_nav_ex:I = 0x7f0a0570

.field public static final wcis_news:I = 0x7f0a05aa

.field public static final wcis_news_ex:I = 0x7f0a05ab

.field public static final wcis_open_app:I = 0x7f0a0571

.field public static final wcis_open_app_ex:I = 0x7f0a0572

.field public static final wcis_recordvoice:I = 0x7f0a0573

.field public static final wcis_recordvoice_ex:I = 0x7f0a0574

.field public static final wcis_schedule:I = 0x7f0a06e9

.field public static final wcis_schedule_ex:I = 0x7f0a0575

.field public static final wcis_schedule_in_car_ex:I = 0x7f0a0577

.field public static final wcis_schedule_lookup_ex:I = 0x7f0a0576

.field public static final wcis_search_ex:I = 0x7f0a0578

.field public static final wcis_search_ex_cn:I = 0x7f0a0579

.field public static final wcis_search_speak_query:I = 0x7f0a06ea

.field public static final wcis_simple_setting_controls:I = 0x7f0a0589

.field public static final wcis_simple_setting_controls_ex:I = 0x7f0a058a

.field public static final wcis_simple_setting_controls_ex_for_chinese:I = 0x7f0a05b3

.field public static final wcis_simple_setting_controls_for_chinese:I = 0x7f0a05b2

.field public static final wcis_sms_ex:I = 0x7f0a057a

.field public static final wcis_sms_speak_text:I = 0x7f0a06eb

.field public static final wcis_social_update:I = 0x7f0a06ec

.field public static final wcis_social_update_cn:I = 0x7f0a06ed

.field public static final wcis_social_update_ex:I = 0x7f0a057b

.field public static final wcis_social_update_ex_cn:I = 0x7f0a057c

.field public static final wcis_task:I = 0x7f0a0582

.field public static final wcis_task_ex:I = 0x7f0a0583

.field public static final wcis_task_lookup_ex:I = 0x7f0a0584

.field public static final wcis_timer:I = 0x7f0a057f

.field public static final wcis_timer_ex:I = 0x7f0a0580

.field public static final wcis_voice_dial_call_contacts:I = 0x7f0a06ee

.field public static final wcis_voice_dial_contact_ex:I = 0x7f0a057d

.field public static final wcis_voice_dial_contact_ex_us:I = 0x7f0a05a7

.field public static final wcis_voice_dial_tip:I = 0x7f0a057e

.field public static final wcis_wake_up:I = 0x7f0a06ef

.field public static final wcis_wake_up_caption:I = 0x7f0a06f0

.field public static final wcis_world_clock:I = 0x7f0a05b9

.field public static final wcis_world_clock_ex:I = 0x7f0a05ba

.field public static final weather_location:I = 0x7f0a07f5

.field public static final web:I = 0x7f0a06f5

.field public static final wed1:I = 0x7f0a0734

.field public static final what_can_i_say:I = 0x7f0a06f6

.field public static final what_can_i_say_header:I = 0x7f0a03c4

.field public static final word_separators:I = 0x7f0a0503

.field public static final world_time_with_location:I = 0x7f0a0820

.field public static final you_can_say_dot_dot:I = 0x7f0a07d7

.field public static final your_destination:I = 0x7f0a0248

.field public static final zip_code:I = 0x7f0a0767


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
