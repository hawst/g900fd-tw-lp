.class Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;
.super Ljava/lang/Object;
.source "CarModeCheckReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const-wide v9, 0x401199999999999aL    # 4.4

    const/4 v8, 0x0

    .line 162
    const-string/jumbo v5, "[CarModeCheckReceiver]"

    const-string/jumbo v6, "BT/GPS/Sound Thread start"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->isAirplaneModeEnabled(Landroid/content/Context;)Z

    move-result v0

    .line 164
    .local v0, "isAirplaneMode":Z
    if-eqz v0, :cond_5

    .line 170
    const-string/jumbo v5, "[CarModeCheckReceiver]"

    const-string/jumbo v6, "CarMode is disbaled because Airplane Mode is ON"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v6, v6, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->disableCarMode(Landroid/content/Context;)V

    .line 177
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    # invokes: Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->restoreSoundAndBTState()V
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->access$0(Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;)V

    .line 202
    :goto_0
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v5

    .line 204
    const-string/jumbo v6, "PREF_USER_BT_PREVIOUS_STATE"

    .line 203
    invoke-virtual {v5, v6, v8}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v5

    .line 205
    if-eqz v5, :cond_7

    .line 206
    const-string/jumbo v5, "[CarModeCheckReceiver]"

    const-string/jumbo v6, "PREF_USER_BT_PREVIOUS_STATE true"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    .line 208
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v6

    .line 207
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->access$1(Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;Landroid/bluetooth/BluetoothAdapter;)V

    .line 209
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    # getter for: Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->access$2(Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v5

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 210
    const-string/jumbo v5, "[CarModeCheckReceiver]"

    const-string/jumbo v6, "BT enabled"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->getConnectedDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v5

    if-nez v5, :cond_6

    .line 212
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    # getter for: Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->access$2(Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v5

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    .line 214
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v5

    .line 215
    const-string/jumbo v6, "PREF_USER_BT_PREVIOUS_STATE"

    invoke-virtual {v5, v6, v8}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 217
    const-string/jumbo v5, "[CarModeCheckReceiver]"

    const-string/jumbo v6, "BT off"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    :cond_0
    :goto_1
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v5

    .line 230
    const-string/jumbo v6, "PREF_USER_GPS_PREVIOUS_STATE"

    .line 229
    invoke-virtual {v5, v6, v8}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v5

    .line 231
    if-eqz v5, :cond_8

    .line 232
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    .line 233
    const-string/jumbo v6, "location"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 232
    check-cast v2, Landroid/location/LocationManager;

    .line 235
    .local v2, "mLocationManager":Landroid/location/LocationManager;
    const-string/jumbo v5, "gps"

    invoke-virtual {v2, v5}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 237
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v5

    .line 238
    const-string/jumbo v6, "PREF_USER_GPS_PREVIOUS_STATE"

    invoke-virtual {v5, v6, v8}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 241
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    int-to-double v5, v5

    cmpl-double v5, v5, v9

    if-ltz v5, :cond_1

    .line 243
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 244
    const-string/jumbo v6, "location_mode"

    .line 242
    invoke-static {v5, v6, v8}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 246
    const-string/jumbo v5, "[CarModeCheckReceiver]"

    const-string/jumbo v6, "GPS ONLY off"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    .end local v2    # "mLocationManager":Landroid/location/LocationManager;
    :cond_1
    :goto_2
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->restoreRingerMode(Landroid/content/Context;)Z

    .line 279
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->restoreNfcMode(Landroid/content/Context;)Z

    .line 320
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v5

    .line 321
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v6, v6, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-interface {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestStopDrivingStatusMonitoring(Landroid/content/Context;)V

    .line 322
    const-string/jumbo v5, "[CarModeCheckReceiver]"

    const-string/jumbo v6, "stop driving manager"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v5

    .line 328
    const-string/jumbo v6, "PREF_CAR_MODE_CLOSED_BY_TALKBACK_ON"

    .line 327
    invoke-virtual {v5, v6, v8}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v1

    .line 330
    .local v1, "isClosedByTalkBackOn":Z
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_2

    if-eqz v1, :cond_2

    .line 331
    const-string/jumbo v5, "[CarModeCheckReceiver]"

    .line 332
    const-string/jumbo v6, "Car Mode is closed because talkback is on"

    .line 331
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v5

    .line 335
    const-string/jumbo v6, "PREF_CAR_MODE_CLOSED_BY_TALKBACK_ON"

    invoke-virtual {v5, v6, v8}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 337
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->TalkBackToastHandlerdler:Landroid/os/Handler;

    .line 338
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 342
    :cond_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 344
    :try_start_0
    const-string/jumbo v5, "e"

    const-string/jumbo v6, "[CarMode]"

    const-string/jumbo v7, "[CarModeCheckReceiver]"

    .line 345
    const-string/jumbo v8, "exitApplication"

    .line 344
    invoke-static {v5, v6, v7, v8}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v6, v6, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/music/MusicService;->terminate(Landroid/content/Context;)V

    .line 349
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->setWhenOnDestoryed()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 355
    :cond_3
    :goto_3
    const-string/jumbo v5, "[CarModeCheckReceiver]"

    const-string/jumbo v6, "BT/GPS/Sound Thread finish"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    new-instance v4, Landroid/content/Intent;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    const-class v6, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 358
    .local v4, "service":Landroid/content/Intent;
    if-eqz v4, :cond_4

    .line 359
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v4}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 362
    :cond_4
    return-void

    .line 181
    .end local v1    # "isClosedByTalkBackOn":Z
    .end local v4    # "service":Landroid/content/Intent;
    :cond_5
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    .line 182
    const-string/jumbo v6, "notification"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 181
    check-cast v3, Landroid/app/NotificationManager;

    .line 184
    .local v3, "mNotificationManager":Landroid/app/NotificationManager;
    const/16 v5, 0x1e3e

    invoke-virtual {v3, v5}, Landroid/app/NotificationManager;->cancel(I)V

    .line 186
    const-string/jumbo v5, "[CarModeCheckReceiver]"

    const-string/jumbo v6, "force cancel Notify register"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 219
    .end local v3    # "mNotificationManager":Landroid/app/NotificationManager;
    :cond_6
    const-string/jumbo v5, "[CarModeCheckReceiver]"

    const-string/jumbo v6, "BT connected"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 223
    :cond_7
    const-string/jumbo v5, "[CarModeCheckReceiver]"

    const-string/jumbo v6, "PREF_USER_BT_PREVIOUS_STATE false"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 250
    :cond_8
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v5

    .line 252
    const-string/jumbo v6, "PREF_USER_GPS_PREVIOUS_STATE_IS_POWERSAVING"

    .line 251
    invoke-virtual {v5, v6, v8}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v5

    .line 253
    if-eqz v5, :cond_1

    .line 254
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    .line 255
    const-string/jumbo v6, "location"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 254
    check-cast v2, Landroid/location/LocationManager;

    .line 257
    .restart local v2    # "mLocationManager":Landroid/location/LocationManager;
    const-string/jumbo v5, "gps"

    invoke-virtual {v2, v5}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 259
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v5

    .line 260
    const-string/jumbo v6, "PREF_USER_GPS_PREVIOUS_STATE_IS_POWERSAVING"

    invoke-virtual {v5, v6, v8}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 263
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    int-to-double v5, v5

    cmpl-double v5, v5, v9

    if-ltz v5, :cond_1

    .line 265
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 266
    const-string/jumbo v6, "location_mode"

    .line 267
    const/4 v7, 0x2

    .line 264
    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 268
    const-string/jumbo v5, "[CarModeCheckReceiver]"

    .line 269
    const-string/jumbo v6, "GPS OFF & GPS BATTERY SAVING SET"

    .line 268
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 350
    .end local v2    # "mLocationManager":Landroid/location/LocationManager;
    .restart local v1    # "isClosedByTalkBackOn":Z
    :catch_0
    move-exception v5

    goto/16 :goto_3
.end method
