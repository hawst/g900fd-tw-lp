.class Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;
.super Landroid/media/session/MediaSession$Callback;
.source "MusicRemotePlayerManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaSessionCallback"
.end annotation


# instance fields
.field private mHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;Landroid/os/Handler;)V
    .locals 1
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 654
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    invoke-direct {p0}, Landroid/media/session/MediaSession$Callback;-><init>()V

    .line 652
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;->mHandler:Landroid/os/Handler;

    .line 655
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;->mHandler:Landroid/os/Handler;

    .line 656
    return-void
.end method

.method private sendMessage(IILjava/lang/Object;)V
    .locals 2
    .param p1, "notiType"    # I
    .param p2, "arg1"    # I
    .param p3, "data"    # Ljava/lang/Object;

    .prologue
    .line 752
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 753
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 754
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 755
    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 757
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 758
    return-void
.end method


# virtual methods
.method public onCustomAction(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 674
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$1()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[audiofw] onCustomAction caslled!!! action:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 675
    const-string/jumbo v3, " extras:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "shuffle"

    invoke-virtual {p2, v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 674
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    const-string/jumbo v1, "com.samsung.android.bt.AVRCP"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 678
    const-string/jumbo v1, "shuffle"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 680
    .local v0, "value":I
    if-nez v0, :cond_1

    .line 681
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$1()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "shuffle is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 685
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicRemotePlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v1

    .line 686
    invoke-interface {v1, v5}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;->onMusicRemotePlayerShuffle(Z)V

    .line 704
    .end local v0    # "value":I
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/media/session/MediaSession$Callback;->onCustomAction(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 705
    return-void

    .line 687
    .restart local v0    # "value":I
    :cond_1
    if-ne v0, v4, :cond_2

    .line 688
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$1()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "shuffle is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 692
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicRemotePlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v1

    .line 693
    invoke-interface {v1, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;->onMusicRemotePlayerShuffle(Z)V

    goto :goto_0

    .line 695
    :cond_2
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$1()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "shuffle is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 699
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicRemotePlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v1

    .line 700
    invoke-interface {v1, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;->onMusicRemotePlayerShuffle(Z)V

    goto :goto_0
.end method

.method public onMediaButtonEvent(Landroid/content/Intent;)Z
    .locals 9
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 709
    .line 710
    const-string/jumbo v2, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/view/KeyEvent;

    .line 711
    .local v0, "event":Landroid/view/KeyEvent;
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    .line 712
    .local v1, "keycode":I
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_7

    .line 713
    const/16 v2, 0x57

    if-ne v1, v2, :cond_1

    .line 714
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$1()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "[YANG] Next Pressed"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 715
    const/4 v2, 0x7

    invoke-direct {p0, v2, v5, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;->sendMessage(IILjava/lang/Object;)V

    .line 748
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/media/session/MediaSession$Callback;->onMediaButtonEvent(Landroid/content/Intent;)Z

    move-result v2

    return v2

    .line 716
    :cond_1
    const/16 v2, 0x58

    if-ne v1, v2, :cond_2

    .line 717
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$1()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "[YANG] Previous pressed"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 718
    const/4 v2, 0x6

    invoke-direct {p0, v2, v5, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;->sendMessage(IILjava/lang/Object;)V

    goto :goto_0

    .line 719
    :cond_2
    const/16 v2, 0x4f

    if-ne v1, v2, :cond_3

    .line 720
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$1()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "[YANG] Head Set Hook pressed"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 721
    :cond_3
    const/16 v2, 0x56

    if-ne v1, v2, :cond_4

    .line 722
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$1()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "[YANG]  KEYCODE_MEDIA_STOP"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 723
    const/4 v2, 0x3

    invoke-direct {p0, v2, v5, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;->sendMessage(IILjava/lang/Object;)V

    goto :goto_0

    .line 724
    :cond_4
    const/16 v2, 0x55

    if-ne v1, v2, :cond_5

    .line 725
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$1()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "[YANG] KEYCODE_MEDIA_PLAY_PAUSE "

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    const/4 v2, 0x2

    invoke-direct {p0, v2, v5, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;->sendMessage(IILjava/lang/Object;)V

    goto :goto_0

    .line 727
    :cond_5
    const/16 v2, 0x59

    if-ne v1, v2, :cond_6

    .line 728
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$1()Ljava/lang/String;

    move-result-object v2

    .line 729
    const-string/jumbo v3, "[YANG] KeyEvent.ACTION_DOWN KEYCODE_MEDIA_REWIND"

    .line 728
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 730
    invoke-direct {p0, v7, v5, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;->sendMessage(IILjava/lang/Object;)V

    goto :goto_0

    .line 731
    :cond_6
    const/16 v2, 0x5a

    if-ne v1, v2, :cond_0

    .line 732
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$1()Ljava/lang/String;

    move-result-object v2

    .line 733
    const-string/jumbo v3, "[YANG] KeyEvent.ACTION_DOWN KEYCODE_MEDIA_FAST_FORWARD"

    .line 732
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 734
    invoke-direct {p0, v8, v5, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;->sendMessage(IILjava/lang/Object;)V

    goto :goto_0

    .line 737
    :cond_7
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v6, :cond_0

    .line 738
    const/16 v2, 0x59

    if-ne v1, v2, :cond_8

    .line 739
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$1()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "[YANG] KeyEvent.ACTION_UP KEYCODE_MEDIA_REWIND"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 740
    invoke-direct {p0, v7, v6, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_0

    .line 741
    :cond_8
    const/16 v2, 0x5a

    if-ne v1, v2, :cond_0

    .line 742
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$1()Ljava/lang/String;

    move-result-object v2

    .line 743
    const-string/jumbo v3, "[YANG] KeyEvent.ACTION_UP KEYCODE_MEDIA_FAST_FORWARD"

    .line 742
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 744
    invoke-direct {p0, v8, v6, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 660
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$1()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "[YANG] KEYCODE_MEDIA_PAUSE"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;->sendMessage(IILjava/lang/Object;)V

    .line 662
    invoke-super {p0}, Landroid/media/session/MediaSession$Callback;->onPause()V

    .line 663
    return-void
.end method

.method public onPlay()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 667
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$1()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "[YANG] KEYCODE_MEDIA_PLAY"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 668
    const/4 v0, 0x0

    invoke-direct {p0, v2, v2, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$MediaSessionCallback;->sendMessage(IILjava/lang/Object;)V

    .line 669
    invoke-super {p0}, Landroid/media/session/MediaSession$Callback;->onPlay()V

    .line 670
    return-void
.end method
