.class public Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;
.super Ljava/lang/Thread;
.source "FForwardRewindThread.java"


# instance fields
.field private final FFORWARD_REWIND_DELAY_TIME:I

.field private isFFRE:Z

.field public isRunning:Z


# direct methods
.method constructor <init>(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 4
    const/16 v0, 0x190

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->FFORWARD_REWIND_DELAY_TIME:I

    .line 5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->isRunning:Z

    .line 6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->isFFRE:Z

    .line 9
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->isFFRE:Z

    .line 10
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 16
    :cond_0
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->isRunning:Z

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isMusicSetValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 17
    const-wide/16 v1, 0x190

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    .line 18
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->isFFRE:Z

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setFForwardRewind(Z)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->isInterrupted()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 25
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 26
    :goto_0
    return-void

    .line 20
    :catch_0
    move-exception v0

    .line 21
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
