.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$13;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity;->showSuggestionList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 1440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 1464
    const-string/jumbo v0, "MessageActivity"

    .line 1465
    const-string/jumbo v1, "showSuggestionList - setAutoShrink : false n true"

    .line 1464
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1466
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 1467
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 1468
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 2
    .param p1, "arg0"    # I
    .param p2, "arg1"    # F
    .param p3, "arg2"    # I

    .prologue
    .line 1451
    if-nez p1, :cond_0

    .line 1452
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$57(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    .line 1453
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$56(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;

    move-result-object v1

    .line 1454
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;->getInitialPosition()I

    move-result v1

    .line 1453
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1455
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$55(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 1460
    :goto_0
    return-void

    .line 1457
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$55(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 1458
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$56(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 1457
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    goto :goto_0
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 1444
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$55(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 1445
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$56(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 1444
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 1447
    return-void
.end method
