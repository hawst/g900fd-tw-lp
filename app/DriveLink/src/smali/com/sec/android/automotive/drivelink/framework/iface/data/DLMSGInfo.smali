.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLMSGInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getDLMessage()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;
.end method

.method public abstract getImage()Landroid/graphics/Bitmap;
.end method

.method public abstract getMSGType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;
.end method

.method public abstract getMessage()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getPhoneNumber()Ljava/lang/String;
.end method

.method public abstract getTime()J
.end method
