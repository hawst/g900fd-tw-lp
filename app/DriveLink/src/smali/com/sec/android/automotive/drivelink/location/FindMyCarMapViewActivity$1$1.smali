.class Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1$1;
.super Ljava/lang/Object;
.source "FindMyCarMapViewActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;

    # getter for: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->access$0(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;)Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    move-result-object v0

    # invokes: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->updateConnectionState()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->access$2(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;

    # getter for: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->access$0(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;)Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mIsCarConnected:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->access$3(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 140
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;

    # getter for: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->access$0(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;)Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    move-result-object v0

    # invokes: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->recreateMapView()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->access$4(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;

    # getter for: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->access$0(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;)Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    move-result-object v0

    # invokes: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->hasChangeMyPosition()Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->access$5(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;

    # getter for: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->access$0(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;)Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    move-result-object v0

    # invokes: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->updateMyLocation()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->access$6(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V

    .line 145
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;

    # getter for: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->access$0(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;)Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    move-result-object v0

    # invokes: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->resetMyLocationMark()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->access$7(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;

    # getter for: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->access$0(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;)Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    move-result-object v0

    # invokes: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->createAllCustomMarks()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->access$8(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;

    # getter for: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->access$0(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;)Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->access$9(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;Z)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;

    # getter for: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->access$0(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;)Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1$1;->this$1:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;

    # getter for: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;->access$0(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;)Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    move-result-object v1

    # getter for: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mHideSaveBtn:Z
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->access$10(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)Z

    move-result v1

    # invokes: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->refreshBtnView(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->access$11(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;Z)V

    goto :goto_0
.end method
