.class Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$4;
.super Ljava/lang/Object;
.source "NotificationActivity.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    .line 552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 556
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "Disappear Animation End."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->removeNotification(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->access$3(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;Z)V

    .line 558
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->bEndAnimationStarted:Z

    .line 559
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->access$7(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;Landroid/view/animation/Animation;)V

    .line 560
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "Disappear Animation End..."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 567
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 572
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->bEndAnimationStarted:Z

    .line 573
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "Disappear Animation Start..."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    return-void
.end method
