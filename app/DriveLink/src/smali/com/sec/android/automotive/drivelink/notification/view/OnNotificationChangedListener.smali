.class public interface abstract Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;
.super Ljava/lang/Object;
.source "OnNotificationChangedListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;
    }
.end annotation


# virtual methods
.method public abstract onNotificationAppeared()V
.end method

.method public abstract onNotificationDisappeared()V
.end method

.method public abstract onNotificationRotating()V
.end method

.method public abstract onNotificationWillAppear()V
.end method

.method public abstract onNotificationWillDisappear()V
.end method
