.class public Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "WhatCanISayExpandableListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private listGroups:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;->context:Landroid/content/Context;

    .line 41
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;->listGroups:Ljava/util/LinkedList;

    .line 42
    return-void
.end method


# virtual methods
.method protected addGroup(ILjava/lang/String;Ljava/lang/String;Ljava/util/LinkedList;)V
    .locals 7
    .param p1, "imgResourceId"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subTitle"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 213
    .local p4, "children":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;->listGroups:Ljava/util/LinkedList;

    new-instance v0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;

    .line 214
    const/4 v5, 0x0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/util/LinkedList;Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;)V

    .line 213
    invoke-virtual {v6, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 215
    return-void
.end method

.method public getChild(II)Ljava/lang/Object;
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosititon"    # I

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;->listGroups:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;

    # invokes: Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->getChildren()Ljava/util/LinkedList;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->access$0(Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;)Ljava/util/LinkedList;

    move-result-object v0

    .line 52
    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 51
    return-object v0
.end method

.method public getChildId(II)J
    .locals 2
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 62
    int-to-long v0, p2

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 75
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;->context:Landroid/content/Context;

    .line 76
    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 75
    check-cast v1, Landroid/view/LayoutInflater;

    .line 82
    .local v1, "infalInflater":Landroid/view/LayoutInflater;
    const v3, 0x7f03005b

    .line 83
    const/4 v4, 0x0

    .line 82
    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    .line 87
    const v3, 0x7f0901f9

    invoke-virtual {p4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 86
    check-cast v2, Landroid/widget/TextView;

    .line 88
    .local v2, "txtWhatCanISayChild":Landroid/widget/TextView;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;->getChild(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 90
    .local v0, "childText":Ljava/lang/String;
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    return-object p4
.end method

.method public getChildrenCount(I)I
    .locals 1
    .param p1, "groupPosition"    # I

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;->listGroups:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;

    # invokes: Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->getChildren()Ljava/util/LinkedList;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->access$0(Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    return v0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 1
    .param p1, "groupPosition"    # I

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;->listGroups:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCount()I
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;->listGroups:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    return v0
.end method

.method public getGroupId(I)J
    .locals 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 131
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v6, 0x7f0901ec

    .line 143
    if-nez p3, :cond_0

    .line 144
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;->context:Landroid/content/Context;

    .line 145
    const-string/jumbo v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 144
    check-cast v1, Landroid/view/LayoutInflater;

    .line 147
    .local v1, "infalInflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030056

    const/4 v5, 0x0

    .line 146
    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    .line 150
    .end local v1    # "infalInflater":Landroid/view/LayoutInflater;
    :cond_0
    if-nez p1, :cond_1

    .line 151
    invoke-virtual {p3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 152
    const/16 v5, 0x8

    .line 151
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 159
    :goto_0
    const v4, 0x7f0901ee

    invoke-virtual {p3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 158
    check-cast v3, Landroid/widget/TextView;

    .line 161
    .local v3, "txtWhatCanISayGrpImgTitle":Landroid/widget/TextView;
    const v4, 0x7f0901f0

    invoke-virtual {p3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 160
    check-cast v2, Landroid/widget/TextView;

    .line 162
    .local v2, "txtWhatCanISayGrpImgSubTitle":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;

    .line 163
    .local v0, "grp":Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;
    # invokes: Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->getTitle()Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->access$1(Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    # invokes: Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->getSubTitle()Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->access$2(Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    const v4, 0x7f0901ed

    invoke-virtual {p3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 167
    # invokes: Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->getImgResourceId()I
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->access$3(Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 169
    const v4, 0x7f0901f2

    invoke-virtual {p3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 170
    if-eqz p2, :cond_2

    const v5, 0x7f02017e

    :goto_1
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 176
    return-object p3

    .line 154
    .end local v0    # "grp":Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;
    .end local v2    # "txtWhatCanISayGrpImgSubTitle":Landroid/widget/TextView;
    .end local v3    # "txtWhatCanISayGrpImgTitle":Landroid/widget/TextView;
    :cond_1
    invoke-virtual {p3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 155
    const/4 v5, 0x0

    .line 154
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 171
    .restart local v0    # "grp":Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;
    .restart local v2    # "txtWhatCanISayGrpImgSubTitle":Landroid/widget/TextView;
    .restart local v3    # "txtWhatCanISayGrpImgTitle":Landroid/widget/TextView;
    :cond_2
    const v5, 0x7f02017f

    goto :goto_1
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 196
    const/4 v0, 0x0

    return v0
.end method
