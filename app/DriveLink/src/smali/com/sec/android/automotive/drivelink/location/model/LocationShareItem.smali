.class public abstract Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;
.super Ljava/lang/Object;
.source "LocationShareItem.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildItemList(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    const/4 v6, 0x0

    .line 37
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 38
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;>;"
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 40
    .local v2, "headers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 52
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 56
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 58
    return-object v3

    .line 40
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 41
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    new-instance v5, Lcom/sec/android/automotive/drivelink/location/model/LocationShareContact;

    invoke-direct {v5, v0}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareContact;-><init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 45
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isLetter(C)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 46
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    .line 47
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 46
    invoke-static {v5}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 52
    .end local v0    # "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 53
    .local v1, "header":Ljava/lang/String;
    new-instance v5, Lcom/sec/android/automotive/drivelink/location/model/LocationShareHeader;

    invoke-direct {v5, v1}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareHeader;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public compareTo(Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;)I
    .locals 6
    .param p1, "other"    # Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;

    .prologue
    const/4 v5, 0x0

    .line 63
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 64
    const/4 v2, 0x1

    .line 105
    :goto_0
    return v2

    .line 67
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 68
    const/4 v2, -0x1

    goto :goto_0

    .line 73
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;->getType()Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    move-result-object v3

    sget-object v4, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;->CONTACT:Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    if-ne v3, v4, :cond_4

    .line 74
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;->getType()Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    move-result-object v3

    sget-object v4, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;->CONTACT:Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    if-ne v3, v4, :cond_2

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .line 77
    .local v2, "comparison":I
    goto :goto_0

    .line 79
    .end local v2    # "comparison":I
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 80
    .local v1, "charHeader":C
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 82
    .local v0, "charContact":C
    if-ne v0, v1, :cond_3

    .line 83
    const/4 v2, 0x1

    .line 84
    .restart local v2    # "comparison":I
    goto :goto_0

    .line 85
    .end local v2    # "comparison":I
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .line 88
    .restart local v2    # "comparison":I
    goto :goto_0

    .line 89
    .end local v0    # "charContact":C
    .end local v1    # "charHeader":C
    .end local v2    # "comparison":I
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;->getType()Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    move-result-object v3

    sget-object v4, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;->HEADER:Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    if-ne v3, v4, :cond_5

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .line 92
    .restart local v2    # "comparison":I
    goto :goto_0

    .line 94
    .end local v2    # "comparison":I
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 95
    .restart local v1    # "charHeader":C
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 97
    .restart local v0    # "charContact":C
    if-ne v1, v0, :cond_6

    .line 98
    const/4 v2, -0x1

    .line 99
    .restart local v2    # "comparison":I
    goto :goto_0

    .line 100
    .end local v2    # "comparison":I
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .restart local v2    # "comparison":I
    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;

    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;->compareTo(Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;)I

    move-result v0

    return v0
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getType()Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;
.end method
