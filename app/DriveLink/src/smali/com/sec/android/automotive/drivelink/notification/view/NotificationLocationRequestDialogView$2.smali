.class Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$2;
.super Ljava/lang/Object;
.source "NotificationLocationRequestDialogView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->enableTimer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;

    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 236
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 237
    .local v0, "msg":Landroid/os/Message;
    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->count:Ljava/lang/Integer;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->access$6()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->isCancel:Z
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->access$7()Z

    move-result v1

    if-nez v1, :cond_0

    .line 238
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 243
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->access$8(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 244
    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->count:Ljava/lang/Integer;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->access$6()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->access$9(Ljava/lang/Integer;)V

    .line 245
    return-void

    .line 240
    :cond_0
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    goto :goto_0
.end method
