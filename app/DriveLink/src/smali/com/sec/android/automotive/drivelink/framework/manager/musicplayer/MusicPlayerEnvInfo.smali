.class public Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;
.super Ljava/lang/Object;
.source "MusicPlayerEnvInfo.java"


# instance fields
.field private mMusicRepeatType:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

.field private mbShuffle:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->mMusicRepeatType:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    .line 7
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->mbShuffle:Z

    .line 10
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->NONE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->mMusicRepeatType:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    .line 11
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->mbShuffle:Z

    .line 12
    return-void
.end method


# virtual methods
.method public getMusicRepeatType()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->mMusicRepeatType:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    return-object v0
.end method

.method public isShuffle()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->mbShuffle:Z

    return v0
.end method

.method public setMusicRepeatType(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;)V
    .locals 0
    .param p1, "repeatType"    # Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    .prologue
    .line 15
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->mMusicRepeatType:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    .line 16
    return-void
.end method

.method public setShuffle(Z)V
    .locals 0
    .param p1, "bShuffle"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;->mbShuffle:Z

    .line 24
    return-void
.end method
