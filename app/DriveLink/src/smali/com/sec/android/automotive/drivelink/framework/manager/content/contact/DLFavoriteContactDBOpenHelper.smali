.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "DLFavoriteContactDBOpenHelper.java"


# static fields
.field private static final mTableName:Ljava/lang/String; = "TABLE_FAVORITE_CONTACT"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 12
    const-string/jumbo v0, "db_dl_contact"

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 13
    return-void
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 32
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    monitor-exit p0

    return-void

    .line 32
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 17
    const-string/jumbo v0, "CREATE TABLE IF NOT EXISTS TABLE_FAVORITE_CONTACT ( _id INTEGER PRIMARY KEY AUTOINCREMENT, contactType int, contactId int, orderValue int ) "

    .line 21
    .local v0, "query":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 22
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 26
    const-string/jumbo v0, "DROP TABLE IF EXISTS TABLE_FAVORITE_CONTACT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 27
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLFavoriteContactDBOpenHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 28
    return-void
.end method
