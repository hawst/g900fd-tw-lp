.class public final Lcom/sec/android/automotive/drivelink/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final asrServers:I = 0x7f070012

.field public static final drawer_menu_items:I = 0x7f070009

.field public static final drawer_menu_items_with_inrix:I = 0x7f07000b

.field public static final drawer_menu_items_with_inrix_obd:I = 0x7f07000c

.field public static final drawer_menu_items_with_obd:I = 0x7f07000a

.field public static final group_loc_share_action_bar:I = 0x7f070008

.field public static final group_loc_share_menu_guest:I = 0x7f070007

.field public static final group_loc_share_menu_host:I = 0x7f070006

.field public static final helloServers:I = 0x7f070014

.field public static final help_wcis_home_example_messages:I = 0x7f070011

.field public static final help_wcis_location_example_messages:I = 0x7f07000f

.field public static final help_wcis_message_example_messages:I = 0x7f07000e

.field public static final help_wcis_message_example_messages_for_kdi:I = 0x7f07001b

.field public static final help_wcis_music_example_messages:I = 0x7f070010

.field public static final help_wcis_phone_example_messages:I = 0x7f07000d

.field public static final languages_iso:I = 0x7f070019

.field public static final languages_iso_cn:I = 0x7f07001a

.field public static final languages_names:I = 0x7f070017

.field public static final languages_names_cn:I = 0x7f070018

.field public static final list_dialog_list_item:I = 0x7f070004

.field public static final list_dialog_list_item_usa:I = 0x7f070005

.field public static final lmttServers:I = 0x7f070015

.field public static final logServers:I = 0x7f070013

.field public static final monthMatchers:I = 0x7f070003

.field public static final servicesServers:I = 0x7f070016

.field public static final svox_big_numbers:I = 0x7f070002

.field public static final svox_symbols:I = 0x7f070000

.field public static final svox_timezones:I = 0x7f070001

.field public static final weather_clear:I = 0x7f07003e

.field public static final weather_clear_today:I = 0x7f07003f

.field public static final weather_cold:I = 0x7f07003a

.field public static final weather_cold_today:I = 0x7f07003b

.field public static final weather_duststorm:I = 0x7f070045

.field public static final weather_duststorm_today:I = 0x7f070046

.field public static final weather_flurries:I = 0x7f07002e

.field public static final weather_flurries_today:I = 0x7f07002f

.field public static final weather_foggy:I = 0x7f070022

.field public static final weather_foggy_today:I = 0x7f070023

.field public static final weather_haze:I = 0x7f070043

.field public static final weather_haze_today:I = 0x7f070044

.field public static final weather_hot:I = 0x7f070038

.field public static final weather_hot_today:I = 0x7f070039

.field public static final weather_ice:I = 0x7f070034

.field public static final weather_ice_today:I = 0x7f070035

.field public static final weather_mixed_rain_and_snow:I = 0x7f070036

.field public static final weather_mixed_rain_and_snow_today:I = 0x7f070037

.field public static final weather_moslty_clear:I = 0x7f070040

.field public static final weather_moslty_clear_today:I = 0x7f070041

.field public static final weather_mostly_cloudy:I = 0x7f070020

.field public static final weather_mostly_cloudy_night:I = 0x7f070042

.field public static final weather_mostly_cloudy_today:I = 0x7f070021

.field public static final weather_mostly_cloudy_with_flurries:I = 0x7f070030

.field public static final weather_mostly_cloudy_with_flurries_today:I = 0x7f070031

.field public static final weather_mostly_cloudy_with_thunder_showers:I = 0x7f07002a

.field public static final weather_mostly_cloudy_with_thunder_showers_today:I = 0x7f07002b

.field public static final weather_partly_sunny:I = 0x7f07001e

.field public static final weather_partly_sunny_today:I = 0x7f07001f

.field public static final weather_partly_sunny_with_showers:I = 0x7f070026

.field public static final weather_partly_sunny_with_showers_today:I = 0x7f070027

.field public static final weather_phenomenon:I = 0x7f070047

.field public static final weather_rainy:I = 0x7f07002c

.field public static final weather_rainy_today:I = 0x7f07002d

.field public static final weather_showers:I = 0x7f070024

.field public static final weather_showers_today:I = 0x7f070025

.field public static final weather_snow:I = 0x7f070032

.field public static final weather_snow_today:I = 0x7f070033

.field public static final weather_sunny:I = 0x7f07001c

.field public static final weather_sunny_today:I = 0x7f07001d

.field public static final weather_thunderstorms:I = 0x7f070028

.field public static final weather_thunderstorms_today:I = 0x7f070029

.field public static final weather_wind_direction:I = 0x7f070048

.field public static final weather_wind_force:I = 0x7f070049

.field public static final weather_windy:I = 0x7f07003c

.field public static final weather_windy_today:I = 0x7f07003d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
