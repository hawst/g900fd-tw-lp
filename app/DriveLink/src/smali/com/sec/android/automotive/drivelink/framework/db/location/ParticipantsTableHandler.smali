.class public Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;
.super Ljava/lang/Object;
.source "ParticipantsTableHandler.java"


# static fields
.field private static final TABLE_PLACEON_PARTICIPANTS:Ljava/lang/String; = "tb_location_participants"

.field private static final TAG:Ljava/lang/String; = "PlaceOnParticipantsTableHandler"


# instance fields
.field private mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;)V
    .locals 0
    .param p1, "handler"    # Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 28
    return-void
.end method

.method protected static getNewPlaceOnParticipantsTableHandler()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    const-string/jumbo v0, "tb_location_participants"

    return-object v0
.end method

.method private getParticipantFromCursor(Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 207
    const/4 v0, 0x0

    .line 208
    .local v0, "i":I
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;-><init>()V

    .line 209
    .local v2, "participant":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setPhoneNumber(Ljava/lang/String;)V

    .line 211
    add-int/lit8 v0, v1, 0x1

    .line 212
    .end local v1    # "i":I
    .restart local v0    # "i":I
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .restart local v1    # "i":I
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setFriendName(Ljava/lang/String;)V

    .line 214
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 213
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->getTypeFromInt(I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setStatus(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;)V

    .line 215
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .restart local v1    # "i":I
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setUrl(Ljava/lang/String;)V

    .line 216
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setIsOwner(Z)V

    .line 220
    return-object v2

    .line 216
    :cond_0
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private insertParticipant(Landroid/database/sqlite/SQLiteDatabase;ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z
    .locals 9
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "groupId"    # I
    .param p3, "participant"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 115
    const-wide/16 v1, -0x1

    .line 117
    .local v1, "result":J
    :try_start_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 118
    .local v3, "values":Landroid/content/ContentValues;
    const-string/jumbo v6, "phone_number"

    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string/jumbo v6, "group_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 120
    const-string/jumbo v6, "name"

    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getFriendName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string/jumbo v6, "status"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getStatus()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->getValue()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string/jumbo v6, "url"

    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string/jumbo v7, "isOwner"

    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getIsOwner()Z

    move-result v6

    if-nez v6, :cond_0

    move v6, v4

    :goto_0
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 125
    const-string/jumbo v6, "tb_location_participants"

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v1

    .line 132
    .end local v3    # "values":Landroid/content/ContentValues;
    :goto_1
    const-wide/16 v6, -0x1

    cmp-long v6, v1, v6

    if-nez v6, :cond_1

    :goto_2
    return v4

    .restart local v3    # "values":Landroid/content/ContentValues;
    :cond_0
    move v6, v5

    .line 123
    goto :goto_0

    .line 126
    .end local v3    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 128
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 129
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_1

    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_1
    move v4, v5

    .line 132
    goto :goto_2
.end method

.method private isParticipantInTable(Landroid/database/sqlite/SQLiteDatabase;ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "groupId"    # I
    .param p3, "participant"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    .line 94
    const/4 v2, 0x0

    .line 96
    .local v2, "result":Z
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "select * from tb_location_participants where phone_number = \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 97
    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 98
    const-string/jumbo v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " and group_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 96
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 100
    .local v3, "sql":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 101
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 102
    const/4 v2, 0x1

    .line 103
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 110
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v3    # "sql":Ljava/lang/String;
    :goto_0
    return v2

    .line 104
    :catch_0
    move-exception v1

    .line 105
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 106
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .line 107
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0
.end method

.method private updateParticipant(Landroid/database/sqlite/SQLiteDatabase;ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z
    .locals 11
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "groupId"    # I
    .param p3, "participant"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 137
    const-wide/16 v1, 0x0

    .line 139
    .local v1, "result":J
    :try_start_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 140
    .local v3, "values":Landroid/content/ContentValues;
    const-string/jumbo v8, "name"

    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getFriendName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const-string/jumbo v8, "status"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getStatus()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->getValue()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const-string/jumbo v8, "url"

    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string/jumbo v9, "isOwner"

    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getIsOwner()Z

    move-result v8

    if-nez v8, :cond_0

    move v8, v6

    :goto_0
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v3, v9, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 145
    const-string/jumbo v5, "phone_number = ? and group_id = ?"

    .line 146
    .local v5, "whereClause":Ljava/lang/String;
    const/4 v8, 0x2

    new-array v4, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v8

    const/4 v8, 0x1

    .line 147
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v8

    .line 149
    .local v4, "whereArgs":[Ljava/lang/String;
    const-string/jumbo v8, "tb_location_participants"

    invoke-virtual {p1, v8, v3, v5, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v8

    int-to-long v1, v8

    .line 157
    .end local v3    # "values":Landroid/content/ContentValues;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v5    # "whereClause":Ljava/lang/String;
    :goto_1
    const-wide/16 v8, 0x0

    cmp-long v8, v1, v8

    if-nez v8, :cond_1

    :goto_2
    return v6

    .restart local v3    # "values":Landroid/content/ContentValues;
    :cond_0
    move v8, v7

    .line 143
    goto :goto_0

    .line 151
    .end local v3    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 153
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 154
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_1

    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_1
    move v6, v7

    .line 157
    goto :goto_2
.end method


# virtual methods
.method public createPlaceOnParticipantsTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 35
    :try_start_0
    const-string/jumbo v1, "CREATE TABLE IF NOT EXISTS tb_location_participants (phone_number VARCHAR, group_id INTEGER, name VARCHAR, status INTEGER, url VARCHAR, isOwner INTEGER, PRIMARY KEY(phone_number, group_id));"

    .line 40
    .local v1, "sql":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    .end local v1    # "sql":Ljava/lang/String;
    :goto_0
    return-void

    .line 41
    :catch_0
    move-exception v0

    .line 42
    .local v0, "e":Landroid/database/SQLException;
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    goto :goto_0
.end method

.method public deleteAllParticipants(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v1, 0x0

    .line 224
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 225
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->getParticipantTrackingTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;->deleteAllTracking(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 226
    const-string/jumbo v2, "PlaceOnParticipantsTableHandler"

    const-string/jumbo v3, "Fail to remove participants tracking"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    :goto_0
    return v1

    .line 231
    :cond_0
    :try_start_0
    const-string/jumbo v2, "tb_location_participants"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    const/4 v1, 0x1

    goto :goto_0

    .line 234
    :catch_0
    move-exception v0

    .line 235
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public deleteParticipants(Landroid/database/sqlite/SQLiteDatabase;I)Z
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "groupId"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 162
    :try_start_0
    const-string/jumbo v2, "group_id = ?"

    .line 163
    .local v2, "whereClause":Ljava/lang/String;
    const/4 v5, 0x1

    new-array v1, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v5

    .line 165
    .local v1, "whereArgs":[Ljava/lang/String;
    const-string/jumbo v5, "tb_location_participants"

    invoke-virtual {p1, v5, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    .end local v1    # "whereArgs":[Ljava/lang/String;
    .end local v2    # "whereClause":Ljava/lang/String;
    :goto_0
    return v3

    .line 168
    :catch_0
    move-exception v0

    .line 169
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    move v3, v4

    .line 172
    goto :goto_0
.end method

.method public dropPlaceOnParticipantsTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 49
    :try_start_0
    const-string/jumbo v1, "DROP TABLE IF EXISTS tb_location_participants;"

    .line 50
    .local v1, "sql":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    .end local v1    # "sql":Ljava/lang/String;
    :goto_0
    return-void

    .line 51
    :catch_0
    move-exception v0

    .line 52
    .local v0, "e":Landroid/database/SQLException;
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    goto :goto_0
.end method

.method public getParticipants(Landroid/database/sqlite/SQLiteDatabase;I)Ljava/util/ArrayList;
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "groupId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 177
    if-gtz p2, :cond_0

    .line 178
    const-string/jumbo v6, "PlaceOnParticipantsTableHandler"

    const-string/jumbo v7, "group id is not valid"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    :goto_0
    return-object v3

    .line 182
    :cond_0
    const/4 v3, 0x0

    .line 184
    .local v3, "participantList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    :try_start_0
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "select * from tb_location_participants where group_id = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 185
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 184
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 187
    .local v5, "sql":Ljava/lang/String;
    const/4 v6, 0x0

    invoke-virtual {p1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 188
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 189
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 191
    .end local v3    # "participantList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    .local v4, "participantList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    :cond_1
    :try_start_1
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;->getParticipantFromCursor(Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    move-result-object v2

    .line 192
    .local v2, "participant":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    if-eqz v2, :cond_2

    .line 193
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v6

    if-nez v6, :cond_1

    move-object v3, v4

    .line 196
    .end local v2    # "participant":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .end local v4    # "participantList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    .restart local v3    # "participantList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    :cond_3
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 197
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v5    # "sql":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 198
    .local v1, "e":Ljava/lang/NullPointerException;
    :goto_1
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 199
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .line 200
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :goto_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0

    .line 199
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    .end local v3    # "participantList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    .restart local v0    # "cursor":Landroid/database/Cursor;
    .restart local v4    # "participantList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    .restart local v5    # "sql":Ljava/lang/String;
    :catch_2
    move-exception v1

    move-object v3, v4

    .end local v4    # "participantList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    .restart local v3    # "participantList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    goto :goto_2

    .line 197
    .end local v3    # "participantList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    .restart local v4    # "participantList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    :catch_3
    move-exception v1

    move-object v3, v4

    .end local v4    # "participantList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    .restart local v3    # "participantList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    goto :goto_1
.end method

.method public saveParticipant(Landroid/database/sqlite/SQLiteDatabase;ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "groupId"    # I
    .param p3, "participant"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    .line 80
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 82
    :cond_0
    const-string/jumbo v0, "PlaceOnParticipantsTableHandler"

    const-string/jumbo v1, "participant is not valid"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    const/4 v0, 0x0

    .line 89
    :goto_0
    return v0

    .line 86
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;->isParticipantInTable(Landroid/database/sqlite/SQLiteDatabase;ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 87
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;->updateParticipant(Landroid/database/sqlite/SQLiteDatabase;ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z

    move-result v0

    goto :goto_0

    .line 89
    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;->insertParticipant(Landroid/database/sqlite/SQLiteDatabase;ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z

    move-result v0

    goto :goto_0
.end method

.method public saveParticipants(Landroid/database/sqlite/SQLiteDatabase;ILjava/util/ArrayList;)Z
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "groupId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p3, "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    const/4 v1, 0x0

    .line 58
    if-nez p3, :cond_0

    .line 59
    const-string/jumbo v2, "PlaceOnParticipantsTableHandler"

    const-string/jumbo v3, "participants is null"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    :goto_0
    return v1

    .line 63
    :cond_0
    const/4 v2, -0x1

    if-ne p2, v2, :cond_1

    .line 64
    const-string/jumbo v2, "PlaceOnParticipantsTableHandler"

    const-string/jumbo v3, "groupId is not valid"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 68
    :cond_1
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 75
    const/4 v1, 0x1

    goto :goto_0

    .line 68
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 69
    .local v0, "participant":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;->saveParticipant(Landroid/database/sqlite/SQLiteDatabase;ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 70
    const-string/jumbo v2, "PlaceOnParticipantsTableHandler"

    const-string/jumbo v3, "Fail to save group participant."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
