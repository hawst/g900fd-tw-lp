.class Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$1;
.super Ljava/lang/Object;
.source "MusicRemoteController.java"

# interfaces
.implements Landroid/media/RemoteController$OnClientUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    .line 229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClientChange(Z)V
    .locals 0
    .param p1, "clearing"    # Z

    .prologue
    .line 233
    return-void
.end method

.method public onClientFolderInfoBrowsedPlayer(Ljava/lang/String;)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 283
    return-void
.end method

.method public onClientMetadataUpdate(Landroid/media/RemoteController$MetadataEditor;)V
    .locals 7
    .param p1, "metadataEditor"    # Landroid/media/RemoteController$MetadataEditor;

    .prologue
    .line 237
    const-string/jumbo v0, "RemoteMusicController"

    const-string/jumbo v4, "onClientMetadataUpdate"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mFlagForFirstPlayingChecker:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->access$0(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    const-string/jumbo v0, "RemoteMusicController"

    const-string/jumbo v4, "check intialize callback"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->access$1(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;Z)V

    .line 243
    :cond_0
    const/4 v0, 0x7

    const-string/jumbo v4, "Unknown"

    .line 242
    invoke-virtual {p1, v0, v4}, Landroid/media/RemoteController$MetadataEditor;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 245
    .local v1, "title":Ljava/lang/String;
    const/4 v0, 0x2

    const-string/jumbo v4, "Unknown"

    .line 244
    invoke-virtual {p1, v0, v4}, Landroid/media/RemoteController$MetadataEditor;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 246
    .local v2, "artist":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    .line 247
    const/16 v4, 0x9

    const-wide/16 v5, 0x0

    .line 246
    invoke-virtual {p1, v4, v5, v6}, Landroid/media/RemoteController$MetadataEditor;->getLong(IJ)J

    move-result-wide v4

    invoke-static {v0, v4, v5}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->access$2(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;J)V

    .line 249
    const/16 v0, 0x64

    const/4 v4, 0x0

    .line 248
    invoke-virtual {p1, v0, v4}, Landroid/media/RemoteController$MetadataEditor;->getBitmap(ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 251
    .local v3, "artWork":Landroid/graphics/Bitmap;
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mDLMusic:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setTitle(Ljava/lang/String;)V

    .line 252
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mDLMusic:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setArtist(Ljava/lang/String;)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mDLMusic:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setAlbumArt(Landroid/graphics/Bitmap;)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mDLMusic:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mCurrentMusicDuration:J
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;)J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setDuration(I)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mListener:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->access$4(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;)Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 257
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mListener:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->access$4(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;)Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;

    move-result-object v0

    .line 258
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mCurrentMusicDuration:J
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;)J

    move-result-wide v4

    .line 257
    invoke-interface/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;->onMetadataUpdate(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;J)V

    .line 260
    :cond_1
    return-void
.end method

.method public onClientNowPlayingContentChange()V
    .locals 0

    .prologue
    .line 289
    return-void
.end method

.method public onClientPlayItemResponse(Z)V
    .locals 0
    .param p1, "arg0"    # Z

    .prologue
    .line 295
    return-void
.end method

.method public onClientPlaybackStateUpdate(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 272
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->setMusicPlayerState(I)V
    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->access$5(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;I)V

    .line 273
    return-void
.end method

.method public onClientPlaybackStateUpdate(IJJF)V
    .locals 2
    .param p1, "state"    # I
    .param p2, "stateChangeTimeMs"    # J
    .param p4, "currentPosMs"    # J
    .param p6, "speed"    # F

    .prologue
    .line 265
    const-string/jumbo v0, "RemoteMusicController"

    const-string/jumbo v1, "onClientPlaybackStateUpdate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->setMusicPlayerState(I)V
    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->access$5(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;I)V

    .line 267
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    invoke-static {v0, p4, p5}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->access$6(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;J)V

    .line 268
    return-void
.end method

.method public onClientSessionEvent(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Landroid/os/Bundle;

    .prologue
    .line 301
    return-void
.end method

.method public onClientTransportControlUpdate(I)V
    .locals 0
    .param p1, "transportControlFlags"    # I

    .prologue
    .line 277
    return-void
.end method

.method public onClientUpdateNowPlayingEntries([J)V
    .locals 0
    .param p1, "arg0"    # [J

    .prologue
    .line 307
    return-void
.end method
