.class public Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;
.super Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;
.source "DrivingManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;
    }
.end annotation


# static fields
.field private static final ACCELEROMETER_SENSOR_UPDATE_RATE:I = 0xf4240

.field private static final AVERAGE_SPEED_COMPUTE_INETRVAL:I = 0x1

.field private static final CAR_IDLE_CHECK_DURATION:I = 0xa

.field private static final CAR_IN_MOTION_CHECK_DURATION:I = 0x1

.field private static final CAR_IN_MOTION_CHECK_THRESHOLD:F = 0.2f

.field private static final DEFAULT_MAXIMUM_SPEED_THRESHOLD:I = 0x32

.field private static final DEFAULT_MINIMUM_SPEED_THRESHOLD:I = 0x14

.field private static final DEFAULT_SPEED_DURATION_IN_SECONDS_THRESHOLD:I = 0x3

.field private static final DEFAULT_STOP_SPEED_THRESHOLD:I = 0x5

.field private static final GPS_REQUEST_DISTANCE_METER:I = 0x1

.field private static final GPS_REQUEST_INTERVAL_MS:I = 0x1f4

.field private static final LOCATION_ACCURACY_THRESHOLD:I = 0xc8

.field private static final LOCATION_TIME_IN_SECONDS_THRESHOLD:I = 0x78

.field private static final MILLISECONDS_TO_SECONDS:D = 0.001

.field private static final MPS_TO_KPH:F = 3.6f

.field private static final NANOSECONDS_TO_SECONDS:D = 1.0E-9

.field private static final SPEED_CHANGE_CHECK_INETRVAL:I = 0xa

.field private static final SPEED_SAMPLING_COUNT:I = 0x5


# instance fields
.field private mAccelerationSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;

.field private mAccelerometerSensor:Landroid/hardware/Sensor;

.field private mAccelerometerSensorEventListener:Landroid/hardware/SensorEventListener;

.field private mAverageCarSpeedSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;

.field private mAverageSpeed:F

.field private mChangeCarSpeedSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;

.field private mContext:Landroid/content/Context;

.field private mCurrentCarMotionStatus:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

.field private mCurrentCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

.field private mCurrentMagnitude:F

.field private mDrivingStatusLocationManager:Landroid/location/LocationManager;

.field private mGravityEstimate:[F

.field private mHasAccelerometerSupport:Z

.field private mHasStarted:Z

.field private mIsAccelerometerListenerEnabled:Z

.field private mIsFirstAccelerometerUpdateEvent:Z

.field private mIsFirstLocationUpdateEvent:Z

.field private mIsInitialized:Z

.field private mIsLocationUpdatesEnabled:Z

.field private mLastAverageAccelerationComputeTime:J

.field private mLastAverageSpeedComputeTime:J

.field private mLastCarMotionIdleTime:J

.field private mLastKnownLocation:Landroid/location/Location;

.field private mLastNoSpeedStatusChangeTime:J

.field private mLastRunningSpeedStatusChangeTime:J

.field private mLastSpeedStatusStopTime:J

.field private mLocationListener:Landroid/location/LocationListener;

.field private mMaximumSpeed:I

.field private mMinimumSpeed:I

.field private mRunningCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSpeedCheckDurationInSec:I


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 135
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;-><init>()V

    .line 56
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsInitialized:Z

    .line 57
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mHasStarted:Z

    .line 59
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mContext:Landroid/content/Context;

    .line 61
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mDrivingStatusLocationManager:Landroid/location/LocationManager;

    .line 62
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLocationListener:Landroid/location/LocationListener;

    .line 63
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsFirstLocationUpdateEvent:Z

    .line 64
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastKnownLocation:Landroid/location/Location;

    .line 66
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mMinimumSpeed:I

    .line 67
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mMaximumSpeed:I

    .line 68
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mSpeedCheckDurationInSec:I

    .line 70
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageCarSpeedSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;

    .line 71
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mChangeCarSpeedSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;

    .line 72
    iput v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageSpeed:F

    .line 73
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    .line 74
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mRunningCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    .line 75
    iput-wide v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastNoSpeedStatusChangeTime:J

    .line 76
    iput-wide v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastRunningSpeedStatusChangeTime:J

    .line 77
    iput-wide v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastAverageSpeedComputeTime:J

    .line 89
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mHasAccelerometerSupport:Z

    .line 90
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 91
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAccelerometerSensor:Landroid/hardware/Sensor;

    .line 92
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAccelerometerSensorEventListener:Landroid/hardware/SensorEventListener;

    .line 93
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsFirstAccelerometerUpdateEvent:Z

    .line 94
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mGravityEstimate:[F

    .line 95
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAccelerationSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;

    .line 96
    iput v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentMagnitude:F

    .line 97
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentCarMotionStatus:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    .line 98
    iput-wide v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastCarMotionIdleTime:J

    .line 99
    iput-wide v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastSpeedStatusStopTime:J

    .line 100
    iput-wide v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastAverageAccelerationComputeTime:J

    .line 101
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsAccelerometerListenerEnabled:Z

    .line 102
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsLocationUpdatesEnabled:Z

    .line 137
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;)Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsFirstLocationUpdateEvent:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;Z)V
    .locals 0

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsFirstLocationUpdateEvent:Z

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;)Landroid/location/LocationManager;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mDrivingStatusLocationManager:Landroid/location/LocationManager;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;)Landroid/hardware/SensorManager;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mSensorManager:Landroid/hardware/SensorManager;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 586
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->processLocationChangeEvent(Landroid/location/Location;)V

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;)Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsFirstAccelerometerUpdateEvent:Z

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;Z)V
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsFirstAccelerometerUpdateEvent:Z

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;Landroid/hardware/SensorEvent;)V
    .locals 0

    .prologue
    .line 986
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->processSensorChangeEvent(Landroid/hardware/SensorEvent;)V

    return-void
.end method

.method private getCarSpeed(Landroid/location/Location;)F
    .locals 8
    .param p1, "currentLocation"    # Landroid/location/Location;

    .prologue
    .line 932
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastKnownLocation:Landroid/location/Location;

    invoke-virtual {v4, p1}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v0

    .line 947
    .local v0, "distance":F
    invoke-virtual {p1}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v4

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastKnownLocation:Landroid/location/Location;

    .line 948
    invoke-virtual {v6}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v6

    .line 947
    sub-long/2addr v4, v6

    long-to-double v4, v4

    .line 948
    const-wide v6, 0x3e112e0be826d695L    # 1.0E-9

    .line 947
    mul-double v2, v4, v6

    .line 953
    .local v2, "timeDelta":D
    float-to-double v4, v0

    div-double/2addr v4, v2

    const-wide v6, 0x400cccccc0000000L    # 3.5999999046325684

    mul-double/2addr v4, v6

    double-to-float v1, v4

    .line 963
    .local v1, "speed":F
    return v1
.end method

.method private isBetterLocation(Landroid/location/Location;Landroid/location/Location;)Z
    .locals 13
    .param p1, "newLocation"    # Landroid/location/Location;
    .param p2, "lastKnown"    # Landroid/location/Location;

    .prologue
    .line 887
    if-eqz p1, :cond_a

    .line 889
    if-nez p2, :cond_0

    .line 891
    const/4 v9, 0x1

    .line 927
    :goto_0
    return v9

    .line 895
    :cond_0
    invoke-virtual {p1}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v9

    .line 896
    invoke-virtual {p2}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v11

    .line 895
    sub-long/2addr v9, v11

    long-to-double v9, v9

    .line 896
    const-wide v11, 0x3e112e0be826d695L    # 1.0E-9

    .line 895
    mul-double v7, v9, v11

    .line 897
    .local v7, "timeDelta":D
    const-wide/16 v9, 0x0

    cmpl-double v9, v7, v9

    if-lez v9, :cond_1

    const/4 v3, 0x1

    .line 898
    .local v3, "isNewer":Z
    :goto_1
    const-wide/high16 v9, 0x405e000000000000L    # 120.0

    cmpl-double v9, v7, v9

    if-lez v9, :cond_2

    const/4 v5, 0x1

    .line 899
    .local v5, "isSignificantlyNewer":Z
    :goto_2
    const-wide/high16 v9, -0x3fa2000000000000L    # -120.0

    cmpg-double v9, v7, v9

    if-gez v9, :cond_3

    const/4 v6, 0x1

    .line 901
    .local v6, "isSignificantlyOlder":Z
    :goto_3
    if-eqz v5, :cond_4

    .line 904
    const/4 v9, 0x1

    goto :goto_0

    .line 897
    .end local v3    # "isNewer":Z
    .end local v5    # "isSignificantlyNewer":Z
    .end local v6    # "isSignificantlyOlder":Z
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 898
    .restart local v3    # "isNewer":Z
    :cond_2
    const/4 v5, 0x0

    goto :goto_2

    .line 899
    .restart local v5    # "isSignificantlyNewer":Z
    :cond_3
    const/4 v6, 0x0

    goto :goto_3

    .line 905
    .restart local v6    # "isSignificantlyOlder":Z
    :cond_4
    if-eqz v6, :cond_5

    .line 908
    const/4 v9, 0x0

    goto :goto_0

    .line 912
    :cond_5
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v9

    .line 913
    invoke-virtual {p2}, Landroid/location/Location;->getAccuracy()F

    move-result v10

    .line 912
    sub-float/2addr v9, v10

    float-to-int v0, v9

    .line 914
    .local v0, "accuracyDelta":I
    if-gez v0, :cond_7

    const/4 v2, 0x1

    .line 915
    .local v2, "isMoreAccurate":Z
    :goto_4
    if-lez v0, :cond_8

    const/4 v1, 0x1

    .line 916
    .local v1, "isLessAccurate":Z
    :goto_5
    const/16 v9, 0xc8

    if-le v0, v9, :cond_9

    const/4 v4, 0x1

    .line 921
    .local v4, "isSignificantlyLessAccurate":Z
    :goto_6
    if-nez v2, :cond_6

    .line 922
    if-eqz v1, :cond_a

    if-nez v4, :cond_a

    if-eqz v3, :cond_a

    .line 923
    :cond_6
    const/4 v9, 0x1

    goto :goto_0

    .line 914
    .end local v1    # "isLessAccurate":Z
    .end local v2    # "isMoreAccurate":Z
    .end local v4    # "isSignificantlyLessAccurate":Z
    :cond_7
    const/4 v2, 0x0

    goto :goto_4

    .line 915
    .restart local v2    # "isMoreAccurate":Z
    :cond_8
    const/4 v1, 0x0

    goto :goto_5

    .line 916
    .restart local v1    # "isLessAccurate":Z
    :cond_9
    const/4 v4, 0x0

    goto :goto_6

    .line 927
    .end local v0    # "accuracyDelta":I
    .end local v1    # "isLessAccurate":Z
    .end local v2    # "isMoreAccurate":Z
    .end local v3    # "isNewer":Z
    .end local v5    # "isSignificantlyNewer":Z
    .end local v6    # "isSignificantlyOlder":Z
    .end local v7    # "timeDelta":D
    :cond_a
    const/4 v9, 0x0

    goto :goto_0
.end method

.method private processLocationChangeEvent(Landroid/location/Location;)V
    .locals 18
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 621
    if-eqz p1, :cond_0

    .line 623
    const/4 v8, 0x0

    .line 625
    .local v8, "notifySpeedChange":Z
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastKnownLocation:Landroid/location/Location;

    if-eqz v14, :cond_e

    .line 627
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->getCarSpeed(Landroid/location/Location;)F

    move-result v9

    .line 630
    .local v9, "speed":F
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsAccelerometerListenerEnabled:Z

    if-eqz v14, :cond_1

    .line 631
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mMinimumSpeed:I

    int-to-float v14, v14

    cmpl-float v14, v9, v14

    if-lez v14, :cond_1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentCarMotionStatus:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    sget-object v15, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;->CAR_MOTION_IDLE:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    if-ne v14, v15, :cond_1

    .line 883
    .end local v8    # "notifySpeedChange":Z
    .end local v9    # "speed":F
    :cond_0
    :goto_0
    return-void

    .line 637
    .restart local v8    # "notifySpeedChange":Z
    .restart local v9    # "speed":F
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageCarSpeedSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;

    invoke-virtual {v14, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->addData(F)V

    .line 638
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mChangeCarSpeedSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;

    invoke-virtual {v14, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->addData(F)V

    .line 641
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastAverageSpeedComputeTime:J

    move-wide/from16 v16, v0

    .line 640
    sub-long v14, v14, v16

    long-to-double v14, v14

    .line 642
    const-wide v16, 0x3f50624dd2f1a9fcL    # 0.001

    .line 640
    mul-double v5, v14, v16

    .line 645
    .local v5, "computeTimeDelta":D
    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    cmpl-double v14, v5, v14

    if-gtz v14, :cond_2

    .line 646
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageCarSpeedSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;

    invoke-virtual {v14}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->size()I

    move-result v14

    const/4 v15, 0x5

    if-ne v14, v15, :cond_4

    .line 648
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageCarSpeedSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;

    invoke-virtual {v14}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->getAverageSpeed()F

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageSpeed:F

    .line 653
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageCarSpeedSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;

    invoke-virtual {v14}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->clearData()V

    .line 655
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    .line 654
    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastAverageSpeedComputeTime:J

    .line 658
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageSpeed:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mMinimumSpeed:I

    int-to-float v15, v15

    .line 659
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mMaximumSpeed:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    .line 658
    invoke-static/range {v14 .. v16}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->toCarSpeedStatus(FFF)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    move-result-object v2

    .line 669
    .local v2, "carSpeedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mRunningCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    if-eq v14, v2, :cond_7

    .line 672
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mRunningCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    .line 674
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    .line 673
    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastRunningSpeedStatusChangeTime:J

    .line 677
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastNoSpeedStatusChangeTime:J

    move-wide/from16 v16, v0

    .line 676
    sub-long v14, v14, v16

    long-to-double v14, v14

    .line 678
    const-wide v16, 0x3f50624dd2f1a9fcL    # 0.001

    .line 676
    mul-double v3, v14, v16

    .line 681
    .local v3, "changeTimeDelta":D
    const-wide/high16 v14, 0x4024000000000000L    # 10.0

    cmpl-double v14, v3, v14

    if-lez v14, :cond_4

    .line 683
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mChangeCarSpeedSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;

    .line 684
    invoke-virtual {v14}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->getAverageSpeed()F

    move-result v10

    .line 687
    .local v10, "speedChangeAverage":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mChangeCarSpeedSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;

    invoke-virtual {v14}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->clearData()V

    .line 691
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mMinimumSpeed:I

    int-to-float v14, v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mMaximumSpeed:I

    int-to-float v15, v15

    .line 690
    invoke-static {v10, v14, v15}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->toCarSpeedStatus(FFF)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    move-result-object v11

    .line 703
    .local v11, "speedChangeStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    if-eq v14, v11, :cond_3

    .line 705
    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageSpeed:F

    .line 706
    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mRunningCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    .line 707
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mRunningCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    .line 713
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastRunningSpeedStatusChangeTime:J

    .line 714
    const/4 v8, 0x1

    .line 718
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    .line 717
    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastNoSpeedStatusChangeTime:J

    .line 841
    .end local v2    # "carSpeedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;
    .end local v3    # "changeTimeDelta":D
    .end local v5    # "computeTimeDelta":D
    .end local v9    # "speed":F
    .end local v10    # "speedChangeAverage":F
    .end local v11    # "speedChangeStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;
    :cond_4
    :goto_1
    if-eqz v8, :cond_6

    .line 843
    new-instance v7, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DLCarSpeedStatusImp;

    .line 844
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageSpeed:F

    .line 843
    move-object/from16 v0, p1

    invoke-direct {v7, v14, v15, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DLCarSpeedStatusImp;-><init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;FLandroid/location/Location;)V

    .line 846
    .local v7, "currentCarSpeedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v14

    if-eqz v14, :cond_5

    .line 847
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v14

    .line 848
    invoke-virtual {v14}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkDrivingStatusListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;

    move-result-object v14

    .line 847
    if-eqz v14, :cond_5

    .line 849
    const-string/jumbo v14, "DrivingManager.processLocationChangeEvent()"

    .line 850
    const-string/jumbo v15, "Notify Car Speed Status Change..."

    .line 849
    invoke-static {v14, v15}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v14

    .line 852
    invoke-virtual {v14}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkDrivingStatusListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;

    move-result-object v14

    .line 853
    invoke-interface {v14, v7}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;->onNotifyCarSpeedStatusChange(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;)V

    .line 856
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mHasAccelerometerSupport:Z

    if-eqz v14, :cond_6

    .line 858
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    sget-object v15, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;->CAR_SPEED_STATUS_STOPPED:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    if-ne v14, v15, :cond_6

    .line 859
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageSpeed:F

    const/high16 v15, 0x40a00000    # 5.0f

    cmpg-float v14, v14, v15

    if-gez v14, :cond_6

    .line 861
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsAccelerometerListenerEnabled:Z

    if-nez v14, :cond_6

    .line 862
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentMagnitude:F

    .line 863
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastSpeedStatusStopTime:J

    .line 865
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    .line 864
    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastAverageAccelerationComputeTime:J

    .line 866
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsFirstAccelerometerUpdateEvent:Z

    .line 871
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 872
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAccelerometerSensorEventListener:Landroid/hardware/SensorEventListener;

    .line 873
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAccelerometerSensor:Landroid/hardware/Sensor;

    move-object/from16 v16, v0

    .line 874
    const v17, 0xf4240

    .line 871
    invoke-virtual/range {v14 .. v17}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 875
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsAccelerometerListenerEnabled:Z

    .line 881
    .end local v7    # "currentCarSpeedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    :cond_6
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastKnownLocation:Landroid/location/Location;

    goto/16 :goto_0

    .line 727
    .restart local v2    # "carSpeedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;
    .restart local v5    # "computeTimeDelta":D
    .restart local v9    # "speed":F
    :cond_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mRunningCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    if-eq v14, v15, :cond_9

    .line 730
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastRunningSpeedStatusChangeTime:J

    move-wide/from16 v16, v0

    .line 729
    sub-long v14, v14, v16

    long-to-double v14, v14

    .line 731
    const-wide v16, 0x3f50624dd2f1a9fcL    # 0.001

    .line 729
    mul-double v12, v14, v16

    .line 732
    .local v12, "timeDelta":D
    const-string/jumbo v14, "DrivingManager.processLocationChangeEvent()"

    .line 733
    new-instance v15, Ljava/lang/StringBuilder;

    const-string/jumbo v16, "Speed Consistency Check Time: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 734
    invoke-virtual {v15, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 733
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 732
    invoke-static {v14, v15}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 735
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mSpeedCheckDurationInSec:I

    int-to-double v14, v14

    cmpl-double v14, v12, v14

    if-lez v14, :cond_8

    .line 737
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mRunningCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    .line 743
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastRunningSpeedStatusChangeTime:J

    .line 746
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    .line 745
    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastNoSpeedStatusChangeTime:J

    .line 747
    const/4 v8, 0x1

    .line 805
    .end local v12    # "timeDelta":D
    :cond_8
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mHasAccelerometerSupport:Z

    if-eqz v14, :cond_4

    .line 809
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageSpeed:F

    const/high16 v15, 0x40a00000    # 5.0f

    cmpl-float v14, v14, v15

    if-ltz v14, :cond_4

    .line 810
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentCarMotionStatus:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    sget-object v15, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;->CAR_MOTION_MOVING:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    if-ne v14, v15, :cond_4

    .line 812
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsAccelerometerListenerEnabled:Z

    if-eqz v14, :cond_4

    .line 817
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 818
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAccelerometerSensorEventListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v14, v15}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 819
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsAccelerometerListenerEnabled:Z

    goto/16 :goto_1

    .line 752
    :cond_9
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    .line 751
    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastNoSpeedStatusChangeTime:J

    .line 754
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mHasAccelerometerSupport:Z

    if-eqz v14, :cond_8

    .line 758
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    sget-object v15, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;->CAR_SPEED_STATUS_STOPPED:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    if-ne v14, v15, :cond_d

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageSpeed:F

    const/high16 v15, 0x40a00000    # 5.0f

    cmpg-float v14, v14, v15

    if-gez v14, :cond_d

    .line 759
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentCarMotionStatus:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    sget-object v15, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;->CAR_MOTION_IDLE:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    if-eq v14, v15, :cond_a

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsAccelerometerListenerEnabled:Z

    if-nez v14, :cond_d

    .line 761
    :cond_a
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastSpeedStatusStopTime:J

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_c

    .line 764
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastSpeedStatusStopTime:J

    move-wide/from16 v16, v0

    .line 763
    sub-long v14, v14, v16

    long-to-double v14, v14

    .line 765
    const-wide v16, 0x3f50624dd2f1a9fcL    # 0.001

    .line 763
    mul-double v12, v14, v16

    .line 769
    .restart local v12    # "timeDelta":D
    const-wide/high16 v14, 0x4024000000000000L    # 10.0

    cmpl-double v14, v12, v14

    if-lez v14, :cond_b

    .line 775
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mDrivingStatusLocationManager:Landroid/location/LocationManager;

    .line 776
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v14, v15}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 777
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsLocationUpdatesEnabled:Z

    .line 778
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastSpeedStatusStopTime:J

    .line 781
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsAccelerometerListenerEnabled:Z

    if-nez v14, :cond_8

    .line 782
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentMagnitude:F

    .line 784
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    .line 783
    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastAverageAccelerationComputeTime:J

    .line 785
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsFirstAccelerometerUpdateEvent:Z

    .line 787
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 789
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAccelerometerSensorEventListener:Landroid/hardware/SensorEventListener;

    .line 790
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAccelerometerSensor:Landroid/hardware/Sensor;

    move-object/from16 v16, v0

    .line 791
    const v17, 0xf4240

    .line 788
    invoke-virtual/range {v14 .. v17}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 792
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsAccelerometerListenerEnabled:Z

    goto/16 :goto_2

    .line 797
    .end local v12    # "timeDelta":D
    :cond_c
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    .line 796
    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastSpeedStatusStopTime:J

    goto/16 :goto_2

    .line 800
    :cond_d
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastSpeedStatusStopTime:J

    goto/16 :goto_2

    .line 827
    .end local v2    # "carSpeedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;
    .end local v5    # "computeTimeDelta":D
    .end local v9    # "speed":F
    :cond_e
    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getSpeed()F

    move-result v14

    const v15, 0x40666666    # 3.6f

    mul-float/2addr v14, v15

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageSpeed:F

    .line 829
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageSpeed:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mMinimumSpeed:I

    int-to-float v15, v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mMaximumSpeed:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    .line 828
    invoke-static/range {v14 .. v16}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->toCarSpeedStatus(FFF)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mRunningCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    .line 830
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mRunningCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    .line 835
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastRunningSpeedStatusChangeTime:J

    .line 836
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastNoSpeedStatusChangeTime:J

    .line 837
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastAverageSpeedComputeTime:J

    .line 838
    const/4 v8, 0x1

    goto/16 :goto_1
.end method

.method private processSensorChangeEvent(Landroid/hardware/SensorEvent;)V
    .locals 21
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 991
    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x0

    aget v15, v1, v2

    .line 992
    .local v15, "x":F
    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x1

    aget v17, v1, v2

    .line 993
    .local v17, "y":F
    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x2

    aget v19, v1, v2

    .line 996
    .local v19, "z":F
    const v8, 0x3f4ccccd    # 0.8f

    .line 998
    .local v8, "alpha":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mGravityEstimate:[F

    const/4 v2, 0x0

    const v3, 0x3f4ccccd    # 0.8f

    mul-float/2addr v3, v15

    .line 999
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mGravityEstimate:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    const v5, 0x3e4ccccc    # 0.19999999f

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    .line 998
    aput v3, v1, v2

    .line 1000
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mGravityEstimate:[F

    const/4 v2, 0x1

    const v3, 0x3f4ccccd    # 0.8f

    mul-float v3, v3, v17

    .line 1001
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mGravityEstimate:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    const v5, 0x3e4ccccc    # 0.19999999f

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    .line 1000
    aput v3, v1, v2

    .line 1002
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mGravityEstimate:[F

    const/4 v2, 0x2

    const v3, 0x3f4ccccd    # 0.8f

    mul-float v3, v3, v19

    .line 1003
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mGravityEstimate:[F

    const/4 v5, 0x2

    aget v4, v4, v5

    const v5, 0x3e4ccccc    # 0.19999999f

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    .line 1002
    aput v3, v1, v2

    .line 1005
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mGravityEstimate:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    sub-float v16, v15, v1

    .line 1006
    .local v16, "xAcceleration":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mGravityEstimate:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    sub-float v18, v17, v1

    .line 1007
    .local v18, "yAcceleration":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mGravityEstimate:[F

    const/4 v2, 0x2

    aget v1, v1, v2

    sub-float v20, v19, v1

    .line 1009
    .local v20, "zAcceleration":F
    const/4 v14, 0x0

    .line 1010
    .local v14, "magnitude":F
    mul-float v1, v16, v16

    .line 1011
    mul-float v2, v18, v18

    .line 1010
    add-float/2addr v1, v2

    .line 1012
    mul-float v2, v20, v20

    .line 1010
    add-float/2addr v1, v2

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    double-to-float v14, v1

    .line 1013
    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v14

    .line 1016
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAccelerationSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;

    invoke-virtual {v1, v14}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;->addData(F)V

    .line 1018
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastAverageAccelerationComputeTime:J

    sub-long/2addr v1, v3

    long-to-double v1, v1

    .line 1019
    const-wide v3, 0x3f50624dd2f1a9fcL    # 0.001

    .line 1018
    mul-double v12, v1, v3

    .line 1024
    .local v12, "computeTimeDelta":D
    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    cmpl-double v1, v12, v1

    if-lez v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAccelerationSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 1026
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAccelerationSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;->getAverageAcceleration()F

    move-result v14

    .line 1031
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAccelerationSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;->clearData()V

    .line 1032
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastAverageAccelerationComputeTime:J

    .line 1034
    move v9, v14

    .line 1037
    .local v9, "averageMagnitude":F
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentMagnitude:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 1038
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentMagnitude:F

    add-float/2addr v1, v14

    const/high16 v2, 0x40000000    # 2.0f

    div-float v9, v1, v2

    .line 1043
    :cond_0
    const v1, 0x3e4ccccd    # 0.2f

    cmpl-float v1, v14, v1

    if-lez v1, :cond_4

    .line 1045
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentCarMotionStatus:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;->CAR_MOTION_MOVING:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    if-ne v1, v2, :cond_3

    .line 1047
    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastCarMotionIdleTime:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    .line 1050
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastCarMotionIdleTime:J

    .line 1049
    sub-long/2addr v1, v3

    long-to-double v1, v1

    .line 1051
    const-wide v3, 0x3f50624dd2f1a9fcL    # 0.001

    .line 1049
    mul-double v10, v1, v3

    .line 1052
    .local v10, "carInMotionTimeDelta":D
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsLocationUpdatesEnabled:Z

    if-nez v1, :cond_1

    .line 1053
    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    cmpl-double v1, v10, v1

    if-lez v1, :cond_1

    .line 1057
    const-wide/16 v1, 0x0

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastCarMotionIdleTime:J

    .line 1058
    const-wide/16 v1, 0x0

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastSpeedStatusStopTime:J

    .line 1060
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mDrivingStatusLocationManager:Landroid/location/LocationManager;

    .line 1062
    const-string/jumbo v2, "gps"

    .line 1063
    const-wide/16 v3, 0x1f4

    .line 1064
    const/high16 v5, 0x3f800000    # 1.0f

    .line 1065
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLocationListener:Landroid/location/LocationListener;

    .line 1066
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v7

    .line 1061
    invoke-virtual/range {v1 .. v7}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    .line 1067
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsLocationUpdatesEnabled:Z

    .line 1075
    .end local v10    # "carInMotionTimeDelta":D
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentMagnitude:F

    .line 1115
    .end local v9    # "averageMagnitude":F
    :cond_2
    :goto_1
    return-void

    .line 1072
    .restart local v9    # "averageMagnitude":F
    :cond_3
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;->CAR_MOTION_MOVING:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentCarMotionStatus:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    .line 1073
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastCarMotionIdleTime:J

    goto :goto_0

    .line 1078
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentCarMotionStatus:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;->CAR_MOTION_MOVING:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    if-ne v1, v2, :cond_6

    .line 1079
    const v1, 0x3e4ccccd    # 0.2f

    cmpl-float v1, v9, v1

    if-lez v1, :cond_6

    .line 1081
    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastCarMotionIdleTime:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_5

    .line 1084
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastCarMotionIdleTime:J

    .line 1083
    sub-long/2addr v1, v3

    long-to-double v1, v1

    .line 1085
    const-wide v3, 0x3f50624dd2f1a9fcL    # 0.001

    .line 1083
    mul-double v10, v1, v3

    .line 1086
    .restart local v10    # "carInMotionTimeDelta":D
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsLocationUpdatesEnabled:Z

    if-nez v1, :cond_5

    .line 1087
    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    cmpl-double v1, v10, v1

    if-lez v1, :cond_5

    .line 1091
    const-wide/16 v1, 0x0

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastCarMotionIdleTime:J

    .line 1092
    const-wide/16 v1, 0x0

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastSpeedStatusStopTime:J

    .line 1094
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mDrivingStatusLocationManager:Landroid/location/LocationManager;

    .line 1096
    const-string/jumbo v2, "gps"

    .line 1097
    const-wide/16 v3, 0x1f4

    .line 1098
    const/high16 v5, 0x3f800000    # 1.0f

    .line 1099
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLocationListener:Landroid/location/LocationListener;

    .line 1100
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v7

    .line 1095
    invoke-virtual/range {v1 .. v7}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    .line 1101
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsLocationUpdatesEnabled:Z

    .line 1104
    .end local v10    # "carInMotionTimeDelta":D
    :cond_5
    move-object/from16 v0, p0

    iput v9, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentMagnitude:F

    goto :goto_1

    .line 1106
    :cond_6
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;->CAR_MOTION_IDLE:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentCarMotionStatus:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    .line 1107
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentMagnitude:F

    goto :goto_1
.end method


# virtual methods
.method public getCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    .locals 5

    .prologue
    .line 534
    const-string/jumbo v2, "DrivingManager"

    const-string/jumbo v3, "getCarSpeedStatus..."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    const/4 v0, 0x0

    .line 538
    .local v0, "currentCarSpeedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsInitialized:Z

    if-eqz v2, :cond_2

    .line 540
    monitor-enter p0

    .line 541
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastKnownLocation:Landroid/location/Location;

    if-eqz v2, :cond_1

    .line 543
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageCarSpeedSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 544
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageCarSpeedSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;

    .line 545
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->getAverageSpeed()F

    move-result v2

    .line 544
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageSpeed:F

    .line 548
    :cond_0
    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageSpeed:F

    iget v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mMinimumSpeed:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mMaximumSpeed:I

    int-to-float v4, v4

    .line 547
    invoke-static {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->toCarSpeedStatus(FFF)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mRunningCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    .line 555
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DLCarSpeedStatusImp;

    .line 556
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mRunningCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    iget v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageSpeed:F

    .line 557
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastKnownLocation:Landroid/location/Location;

    .line 555
    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DLCarSpeedStatusImp;-><init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;FLandroid/location/Location;)V

    .end local v0    # "currentCarSpeedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    .local v1, "currentCarSpeedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    move-object v0, v1

    .line 540
    .end local v1    # "currentCarSpeedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    .restart local v0    # "currentCarSpeedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 562
    :cond_2
    const-string/jumbo v2, "DrivingManager.getLocation()"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Return: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 563
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 562
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    return-object v0

    .line 540
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public getLocation()Landroid/location/Location;
    .locals 5

    .prologue
    .line 481
    const-string/jumbo v2, "DrivingManager"

    const-string/jumbo v3, "getLocation..."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    const/4 v0, 0x0

    .line 484
    .local v0, "currentLocation":Landroid/location/Location;
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsInitialized:Z

    if-eqz v2, :cond_1

    .line 488
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mDrivingStatusLocationManager:Landroid/location/LocationManager;

    .line 489
    const-string/jumbo v3, "gps"

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    .line 505
    .local v1, "newLocation":Landroid/location/Location;
    monitor-enter p0

    .line 506
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastKnownLocation:Landroid/location/Location;

    if-eqz v2, :cond_2

    .line 509
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastKnownLocation:Landroid/location/Location;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->isBetterLocation(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 512
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastKnownLocation:Landroid/location/Location;

    .line 519
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastKnownLocation:Landroid/location/Location;

    .line 505
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 528
    .end local v1    # "newLocation":Landroid/location/Location;
    :cond_1
    const-string/jumbo v2, "DrivingManager.getLocation()"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Return: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    return-object v0

    .line 523
    .restart local v1    # "newLocation":Landroid/location/Location;
    :cond_2
    move-object v0, v1

    goto :goto_0

    .line 505
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public initialize(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v3, 0x0

    const/4 v2, 0x1

    .line 141
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->initialize(Landroid/content/Context;)Z

    .line 143
    const-string/jumbo v0, "DrivingManager"

    const-string/jumbo v1, "initialize..."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsInitialized:Z

    if-nez v0, :cond_3

    .line 147
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mDrivingStatusLocationManager:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    .line 151
    const-string/jumbo v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 150
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mDrivingStatusLocationManager:Landroid/location/LocationManager;

    .line 156
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLocationListener:Landroid/location/LocationListener;

    .line 202
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mHasAccelerometerSupport:Z

    if-eqz v0, :cond_2

    .line 205
    const-string/jumbo v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    .line 204
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 206
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 207
    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    .line 206
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAccelerometerSensor:Landroid/hardware/Sensor;

    .line 208
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAccelerometerSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_1

    .line 209
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$2;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAccelerometerSensorEventListener:Landroid/hardware/SensorEventListener;

    .line 249
    :cond_1
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAccelerationSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/AccelerationSampling;

    .line 250
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mGravityEstimate:[F

    .line 251
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;->CAR_MOTION_IDLE:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentCarMotionStatus:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    .line 256
    :cond_2
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageCarSpeedSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;

    .line 257
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mChangeCarSpeedSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;

    .line 258
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;->CAR_SPEED_STATUS_STOPPED:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mCurrentCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    .line 259
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;->CAR_SPEED_STATUS_STOPPED:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mRunningCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    .line 260
    iput-wide v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastNoSpeedStatusChangeTime:J

    .line 261
    iput-wide v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastRunningSpeedStatusChangeTime:J

    .line 262
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastAverageSpeedComputeTime:J

    .line 275
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsInitialized:Z

    .line 276
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mContext:Landroid/content/Context;

    .line 279
    :cond_3
    return v2
.end method

.method public setCarSpeedThreshold(III)V
    .locals 3
    .param p1, "minSpeed"    # I
    .param p2, "maxSpeed"    # I
    .param p3, "speedCheckDurationInSec"    # I

    .prologue
    .line 570
    const-string/jumbo v0, "DrivingManager"

    const-string/jumbo v1, "setCarSpeedThreshold..."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsInitialized:Z

    if-eqz v0, :cond_0

    .line 574
    monitor-enter p0

    .line 575
    :try_start_0
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mMinimumSpeed:I

    .line 576
    iput p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mMaximumSpeed:I

    .line 577
    iput p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mSpeedCheckDurationInSec:I

    .line 578
    const-string/jumbo v0, "DrivingManager.setCarSpeedThreshold()"

    .line 579
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "New Threshold Values: ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mMinimumSpeed:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 580
    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mMaximumSpeed:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 581
    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mSpeedCheckDurationInSec:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 579
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 578
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    monitor-exit p0

    .line 584
    :cond_0
    return-void

    .line 574
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public startDrivingStatusMonitoring(Landroid/content/Context;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x1

    .line 308
    const-string/jumbo v0, "DrivingManager"

    const-string/jumbo v1, "startDrivingStatusMonitoring..."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    const/4 v7, 0x0

    .line 312
    .local v7, "result":Z
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsInitialized:Z

    if-eqz v0, :cond_2

    .line 314
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mHasStarted:Z

    if-nez v0, :cond_1

    .line 320
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLastKnownLocation:Landroid/location/Location;

    .line 321
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAverageCarSpeedSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->clearData()V

    .line 322
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mChangeCarSpeedSampling:Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->clearData()V

    .line 324
    iput-boolean v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsFirstLocationUpdateEvent:Z

    .line 326
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mDrivingStatusLocationManager:Landroid/location/LocationManager;

    .line 327
    const-string/jumbo v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 328
    const/4 v0, 0x0

    .line 408
    :goto_0
    return v0

    .line 333
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mDrivingStatusLocationManager:Landroid/location/LocationManager;

    .line 334
    const-string/jumbo v1, "gps"

    const-wide/16 v2, 0x1f4

    .line 335
    const/high16 v4, 0x3f800000    # 1.0f

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLocationListener:Landroid/location/LocationListener;

    .line 336
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    .line 333
    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    .line 340
    iput-boolean v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsLocationUpdatesEnabled:Z

    .line 400
    iput-boolean v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mHasStarted:Z

    .line 403
    :cond_1
    const/4 v7, 0x1

    .line 406
    :cond_2
    const-string/jumbo v0, "DrivingManager.startDrivingStatusMonitoring()"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Result: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 407
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 406
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v7

    .line 408
    goto :goto_0
.end method

.method public stopDrivingStatusMonitoring(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 413
    const-string/jumbo v1, "DrivingManager"

    const-string/jumbo v2, "stopDrivingStatusMonitoring..."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    const/4 v0, 0x1

    .line 416
    .local v0, "result":Z
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsInitialized:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mHasStarted:Z

    if-eqz v1, :cond_2

    .line 418
    monitor-enter p0

    .line 419
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mDrivingStatusLocationManager:Landroid/location/LocationManager;

    if-eqz v1, :cond_0

    .line 422
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mDrivingStatusLocationManager:Landroid/location/LocationManager;

    .line 423
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 424
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsLocationUpdatesEnabled:Z

    .line 427
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mHasAccelerometerSupport:Z

    if-eqz v1, :cond_1

    .line 428
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsAccelerometerListenerEnabled:Z

    if-eqz v1, :cond_1

    .line 429
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v1, :cond_1

    .line 432
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 433
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAccelerometerSensorEventListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 434
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsAccelerometerListenerEnabled:Z

    .line 418
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 470
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mHasStarted:Z

    .line 474
    :cond_2
    const-string/jumbo v1, "DrivingManager.stopDrivingStatusMonitoring()"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Result: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 475
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 474
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    return v0

    .line 418
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 285
    const-string/jumbo v0, "DrivingManager"

    const-string/jumbo v1, "terminate..."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsInitialized:Z

    if-eqz v0, :cond_1

    .line 289
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->stopDrivingStatusMonitoring(Landroid/content/Context;)Z

    .line 291
    monitor-enter p0

    .line 292
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mDrivingStatusLocationManager:Landroid/location/LocationManager;

    .line 293
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mLocationListener:Landroid/location/LocationListener;

    .line 294
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mHasAccelerometerSupport:Z

    if-eqz v0, :cond_0

    .line 295
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 296
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAccelerometerSensor:Landroid/hardware/Sensor;

    .line 297
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mAccelerometerSensorEventListener:Landroid/hardware/SensorEventListener;

    .line 291
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->mIsInitialized:Z

    .line 303
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->terminate(Landroid/content/Context;)V

    .line 304
    return-void

    .line 291
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
