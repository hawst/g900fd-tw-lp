.class public Lcom/sec/android/automotive/drivelink/util/DLToast;
.super Ljava/lang/Object;
.source "DLToast.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getView(Landroid/app/Activity;Ljava/lang/String;)Landroid/view/View;
    .locals 7
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 23
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move-object v2, v5

    .line 58
    :goto_0
    return-object v2

    .line 27
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 29
    .local v0, "app":Lcom/sec/android/automotive/drivelink/DLApplication;
    if-nez v0, :cond_2

    move-object v2, v5

    .line 30
    goto :goto_0

    .line 33
    :cond_2
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 35
    .local v1, "inflater":Landroid/view/LayoutInflater;
    if-nez v1, :cond_3

    move-object v2, v5

    .line 36
    goto :goto_0

    .line 39
    :cond_3
    if-nez p0, :cond_4

    move-object v2, v5

    .line 40
    goto :goto_0

    .line 43
    :cond_4
    const v6, 0x7f030096

    .line 44
    const v4, 0x7f090298

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 43
    invoke-virtual {v1, v6, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 46
    .local v2, "layout":Landroid/view/View;
    if-nez v2, :cond_5

    move-object v2, v5

    .line 47
    goto :goto_0

    .line 50
    :cond_5
    const v4, 0x7f09026c

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 52
    .local v3, "textView":Landroid/widget/TextView;
    if-nez v3, :cond_6

    move-object v2, v5

    .line 53
    goto :goto_0

    .line 56
    :cond_6
    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static makeText(Landroid/app/Activity;II)Landroid/widget/Toast;
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "resId"    # I
    .param p2, "duration"    # I

    .prologue
    .line 63
    if-nez p0, :cond_0

    .line 64
    const/4 v1, 0x0

    .line 69
    :goto_0
    return-object v1

    .line 67
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "text":Ljava/lang/String;
    invoke-static {p0, v0, p2}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    goto :goto_0
.end method

.method public static makeText(Landroid/app/Activity;Ljava/lang/String;I)Landroid/widget/Toast;
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "duration"    # I

    .prologue
    .line 74
    if-nez p0, :cond_0

    .line 75
    const/4 v1, 0x0

    .line 89
    :goto_0
    return-object v1

    .line 78
    :cond_0
    invoke-static {p0, p1}, Lcom/sec/android/automotive/drivelink/util/DLToast;->getView(Landroid/app/Activity;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 80
    .local v0, "layout":Landroid/view/View;
    new-instance v1, Landroid/widget/Toast;

    invoke-direct {v1, p0}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    .line 81
    .local v1, "toast":Landroid/widget/Toast;
    invoke-virtual {v1, p2}, Landroid/widget/Toast;->setDuration(I)V

    .line 83
    if-nez v0, :cond_1

    .line 84
    invoke-virtual {v1, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 88
    :cond_1
    invoke-virtual {v1, v0}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    goto :goto_0
.end method
