.class public Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;
.super Ljava/lang/Object;
.source "PlaceOn_Utils.java"


# static fields
.field private static final HOSTS:[Ljava/lang/String;

.field private static final HOST_DEV_PATH:Ljava/lang/String; = "/placeon-dev-elb03-1353224617.eu-west-1.elb.amazonaws.com/v1/placeon"

.field private static final HOST_STG_PATH:Ljava/lang/String; = "/placeon-stg-us-elb01-1932445774.us-east-1.elb.amazonaws.com/v1/placeon"

.field public static final RESPONSE_CODE:Ljava/lang/String; = "PON_200"

.field private static final SCHEMES:[Ljava/lang/String;

.field public static final SUCCESS_RESPONSE_KEY:Ljava/lang/String; = "responseMsg"

.field public static final SUCCESS_RESPONSE_MSG:Ljava/lang/String; = "Success"

.field public static final TAG_ACCEPT:Ljava/lang/String; = "/accept"

.field public static final TAG_DATE:Ljava/lang/String; = "date"

.field public static final TAG_DEVICE_ID:Ljava/lang/String; = "deviceId"

.field public static final TAG_GROUP_ID:Ljava/lang/String; = "groupId"

.field public static final TAG_LATITUDE:Ljava/lang/String; = "latitude"

.field public static final TAG_LOCATION:Ljava/lang/String; = "location"

.field public static final TAG_LOCATION_ID:Ljava/lang/String; = "locationId"

.field public static final TAG_LOCATION_LIST:Ljava/lang/String; = "locationList"

.field public static final TAG_LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final TAG_RESPONSE_CODE:Ljava/lang/String; = "responseCode"

.field public static final TAG_RESPONSE_MESSAGE:Ljava/lang/String; = "responseMsg"

.field public static final TAG_SSP_GUID:Ljava/lang/String; = "sspGuid"

.field public static final TAG_USER_ID:Ljava/lang/String; = "userId"

.field public static final TAG_USER_LOCATION_LIST:Ljava/lang/String; = "userLocationList"

.field public static final URLAcceptLocationShare:Ljava/lang/String; = "/locationShare/shareGroups/"

.field public static final URLAddGroupParticipants:Ljava/lang/String; = "/cc/locationShare/groups/%{groupId}/participants"

.field public static final URLCreateShareGroups:Ljava/lang/String; = "/cc/locationShare/groups/group"

.field public static final URLDeleteFavoriteLocation:Ljava/lang/String; = "/cc/profile/me/favoriteLocation/%{locationId}"

.field public static final URLFavoriteLocation:Ljava/lang/String; = "/cc/profile/me/favoriteLocation/%{locationId}"

.field public static final URLGetFavoriteLocation:Ljava/lang/String; = "/cc/profile/me/favoriteLocation"

.field public static final URLGetGroupLocation:Ljava/lang/String; = "/locationShare/groups/%{groupId}/location"

.field public static final URLGetGroupLocations:Ljava/lang/String; = "/locationShare/groups/%{groupId}/locations"

.field public static final URLGetMyProfile:Ljava/lang/String; = "/cc/profile/me"

.field public static final URLGetProfileByPhoneNumber:Ljava/lang/String; = "/cc/profile/me/profile?phoneNumber={%phoneNumber}"

.field public static final URLGetUSerLocation:Ljava/lang/String; = "/locationShare/groups/%{groupId}/users/%{userId}/location"

.field public static final URLGetUSerLocations:Ljava/lang/String; = "/locationShare/groups/%{groupId}/users/%{userId}/locations"

.field public static final URLGetUserProfile:Ljava/lang/String; = "/cc/profile/%{userId}"

.field public static final URLLocationShareGroups:Ljava/lang/String; = "/cc/locationShare/groups/%{groupId}"

.field public static final URLLocationShareRequest:Ljava/lang/String; = "/locationShare/request"

.field public static final URLProfileLocation:Ljava/lang/String; = "/cc/profile/me/location"

.field public static final URLSendMessage:Ljava/lang/String; = "/cc/test/message"

.field public static final URLSetUserLocation:Ljava/lang/String; = "/locationShare/users/my/location"

.field public static final URLShareMyLocation:Ljava/lang/String; = "/locationShare/share"

.field public static final URLUpdateFavoriteLocation:Ljava/lang/String; = "/cc/profile/me/favoriteLocation"

.field public static final URLUpdateParticipantStatus:Ljava/lang/String; = "/cc/locationShare/groups/%{groupId}/participants"

.field public static final URLUpdateProfile:Ljava/lang/String; = "/cc/profile/me"

.field public static final URLUpdateShareGroup:Ljava/lang/String; = "/cc/locationShare/groups/{%groupId}"

.field public static final URLUserAuthentication:Ljava/lang/String; = "/user/login?username={%username}"


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 62
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "http"

    aput-object v1, v0, v2

    const-string/jumbo v1, "https"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->SCHEMES:[Ljava/lang/String;

    .line 63
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "/placeon-dev-elb03-1353224617.eu-west-1.elb.amazonaws.com/v1/placeon"

    aput-object v1, v0, v2

    const-string/jumbo v1, "/placeon-stg-us-elb01-1932445774.us-east-1.elb.amazonaws.com/v1/placeon"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->HOSTS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getHostUrl()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->SCHEMES:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, ":/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->HOSTS:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isJSONArray(Lorg/json/JSONArray;)Z
    .locals 1
    .param p0, "jsArray"    # Lorg/json/JSONArray;

    .prologue
    .line 72
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 73
    const/4 v0, 0x1

    .line 75
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isString(Ljava/lang/String;)Z
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 80
    :cond_0
    const/4 v0, 0x1

    .line 82
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSuccessResponse(Lorg/json/JSONObject;)Z
    .locals 4
    .param p0, "response"    # Lorg/json/JSONObject;

    .prologue
    const/4 v1, 0x0

    .line 114
    if-eqz p0, :cond_0

    .line 115
    :try_start_0
    const-string/jumbo v2, "responseMsg"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 116
    const-string/jumbo v2, "Success"

    .line 117
    const-string/jumbo v3, "responseMsg"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 116
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 117
    if-eqz v2, :cond_0

    .line 114
    const/4 v1, 0x1

    .line 122
    :cond_0
    :goto_0
    return v1

    .line 118
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public static isValidDuration(I)Z
    .locals 1
    .param p0, "duration"    # I

    .prologue
    .line 86
    if-lez p0, :cond_0

    .line 87
    const/4 v0, 0x1

    .line 89
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isValidLatitude(D)Z
    .locals 2
    .param p0, "latitude"    # D

    .prologue
    .line 93
    const-wide v0, -0x3fa9800000000000L    # -90.0

    cmpg-double v0, p0, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x4056800000000000L    # 90.0

    cmpl-double v0, p0, v0

    if-lez v0, :cond_1

    .line 94
    :cond_0
    const/4 v0, 0x0

    .line 96
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isValidLongitude(D)Z
    .locals 2
    .param p0, "longitude"    # D

    .prologue
    .line 100
    const-wide v0, -0x3f99800000000000L    # -180.0

    cmpg-double v0, p0, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x4066800000000000L    # 180.0

    cmpl-double v0, p0, v0

    if-lez v0, :cond_1

    .line 101
    :cond_0
    const/4 v0, 0x0

    .line 103
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isValidUser(Lorg/json/JSONObject;)Z
    .locals 1
    .param p0, "userProfile"    # Lorg/json/JSONObject;

    .prologue
    .line 107
    const-string/jumbo v0, "sspGuid"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "email"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    const-string/jumbo v0, "accessToken"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "pushId"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    const-string/jumbo v0, "model"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
