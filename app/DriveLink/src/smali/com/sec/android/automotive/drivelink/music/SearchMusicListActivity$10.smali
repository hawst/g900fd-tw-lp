.class Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$10;
.super Ljava/lang/Object;
.source "SearchMusicListActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    .line 485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 489
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->imm:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$5(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$6(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 490
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$4(Z)V

    .line 492
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$12(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 493
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$10(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    .line 492
    sub-int/2addr v1, v2

    .line 493
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 492
    add-int v0, v1, v2

    .line 494
    .local v0, "currentItem":I
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$12(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 495
    return-void
.end method
