.class public Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;
.source "MyPlaceMapActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field private btnSave:Landroid/widget/Button;

.field private txtSearch:Landroid/widget/AutoCompleteTextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;)Landroid/widget/AutoCompleteTextView;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->txtSearch:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method


# virtual methods
.method protected getLayoutId()I
    .locals 1

    .prologue
    .line 144
    const v0, 0x7f030020

    return v0
.end method

.method protected getLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    const v2, 0x7f090098

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 41
    new-instance v3, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$1;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;)V

    .line 40
    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    const v2, 0x7f0900e6

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->btnSave:Landroid/widget/Button;

    .line 50
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->btnSave:Landroid/widget/Button;

    new-instance v3, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$2;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;

    .line 60
    const v2, 0x7f030091

    .line 59
    invoke-direct {v1, p0, v2}, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;-><init>(Landroid/content/Context;I)V

    .line 61
    .local v1, "placesAdapter":Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;
    const v2, 0x7f0900e4

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/AutoCompleteTextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->txtSearch:Landroid/widget/AutoCompleteTextView;

    .line 62
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->txtSearch:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 63
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->txtSearch:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2, p0}, Landroid/widget/AutoCompleteTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 65
    const v2, 0x7f090039

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 66
    .local v0, "ivClearText":Landroid/widget/ImageView;
    new-instance v2, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->txtSearch:Landroid/widget/AutoCompleteTextView;

    new-instance v3, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$4;

    invoke-direct {v3, p0, v1}, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;)V

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 85
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->txtSearch:Landroid/widget/AutoCompleteTextView;

    new-instance v3, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$5;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;Landroid/widget/ImageView;)V

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 110
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->setMarkerDraggable(Z)V

    .line 111
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v6, 0x0

    .line 123
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 125
    .local v3, "str":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;

    .line 126
    .local v0, "adapter":Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;
    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->getReference(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 128
    .local v2, "reference":Ljava/lang/String;
    const-string/jumbo v4, "input_method"

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 129
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->txtSearch:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v4}, Landroid/widget/AutoCompleteTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    invoke-virtual {v1, v4, v6}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 132
    new-instance v4, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceDetailTask;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceDetailTask;-><init>(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)V

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    aput-object v2, v5, v6

    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceDetailTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 134
    return-void
.end method
