.class public Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
.super Ljava/lang/Object;
.source "FlowParams.java"


# static fields
.field public static final CONTINUE_REQUEST_SHARE_LOCATION:Ljava/lang/String; = "CONTINUE_REQUEST_SHARE_LOCATION"

.field public static final MUSIC_PLAYER_NEXT:I = 0x3

.field public static final MUSIC_PLAYER_PAUSE:I = 0x1

.field public static final MUSIC_PLAYER_PLAY:I = 0x0

.field public static final MUSIC_PLAYER_PREV:I = 0x2


# instance fields
.field public extras:Landroid/os/Bundle;

.field public isVideoCall:Z

.field public mAsrExtString:Ljava/lang/String;

.field public mBitmap:Landroid/graphics/Bitmap;

.field public mContactChoices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field

.field public mContactMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field public mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

.field public mIsFromMsgReader:Z

.field public mIsShareMyLocation:Z

.field public mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

.field public mLocationName:Ljava/lang/String;

.field public mMessageName:Ljava/lang/String;

.field public mMessageSendExcute:Z

.field public mMessageText:Ljava/lang/String;

.field public mPhoneNum:Ljava/lang/String;

.field public mPrePromptString:Ljava/lang/String;

.field public mPromptString:Ljava/lang/String;

.field public mSearchListType:I

.field public mSearchedInobxList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public mSearchedText:Ljava/lang/String;

.field public mSuggestionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mSuggestionSelected:I

.field public mTimestamp:Ljava/util/Date;

.field public mTimestampEnd:Ljava/util/Date;

.field public mUrl:Ljava/lang/String;

.field public mUserCallLog:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

.field public mUserIndex:I

.field public mUserPhoneTypeIndex:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPrePromptString:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 26
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mAsrExtString:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageText:Ljava/lang/String;

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageName:Ljava/lang/String;

    .line 30
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageSendExcute:Z

    .line 33
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactChoices:Ljava/util/List;

    .line 34
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 35
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMap:Ljava/util/Map;

    .line 36
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPhoneNum:Ljava/lang/String;

    .line 38
    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    .line 39
    iput v1, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserIndex:I

    .line 41
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mIsFromMsgReader:Z

    .line 43
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserCallLog:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .line 45
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->isVideoCall:Z

    .line 46
    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchListType:I

    .line 47
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchedText:Ljava/lang/String;

    .line 50
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 51
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mLocationName:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mTimestamp:Ljava/util/Date;

    .line 53
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mTimestampEnd:Ljava/util/Date;

    .line 54
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mBitmap:Landroid/graphics/Bitmap;

    .line 55
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mIsShareMyLocation:Z

    .line 56
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUrl:Ljava/lang/String;

    .line 58
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    .line 59
    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionSelected:I

    .line 61
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchedInobxList:Ljava/util/ArrayList;

    .line 64
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->extras:Landroid/os/Bundle;

    .line 16
    return-void
.end method
