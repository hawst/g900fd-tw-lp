.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageField;
.super Ljava/lang/Object;
.source "MessageField.java"


# static fields
.field static final ADDRESS:Ljava/lang/String; = "address"

.field static final BODY:Ljava/lang/String; = "body"

.field static final DATE:Ljava/lang/String; = "date"

.field static final HIDDEN:Ljava/lang/String; = "hidden"

.field static final ID:Ljava/lang/String; = "_id"

.field static final MMS_DATA:Ljava/lang/String; = "_data"

.field static final MMS_DATA_TEXT:Ljava/lang/String; = "text"

.field static final MMS_SUBJECT:Ljava/lang/String; = "sub"

.field static final MMS_SUBJECT_CS:Ljava/lang/String; = "sub_cs"

.field static final MSG_TYPE:Ljava/lang/String; = "message_type"

.field static final M_TYPE:Ljava/lang/String; = "m_type"

.field static final PERSON:Ljava/lang/String; = "person"

.field static final READ:Ljava/lang/String; = "read"

.field static final THREAD_ID:Ljava/lang/String; = "thread_id"

.field static final TYPE:Ljava/lang/String; = "type"

.field static final UNREAD_COUNT:Ljava/lang/String; = "unread_count"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
