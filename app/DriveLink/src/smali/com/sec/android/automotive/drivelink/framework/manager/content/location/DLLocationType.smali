.class public final enum Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;
.super Ljava/lang/Enum;
.source "DLLocationType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

.field public static final enum FRIENDS_SHARED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

.field public static final enum GROUP_SHARING:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

.field public static final enum MY_PLACE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

.field public static final enum SCHEDULE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

.field public static final enum SEARCHED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

.field public static final enum UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    const-string/jumbo v1, "UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    const-string/jumbo v1, "SCHEDULE"

    invoke-direct {v0, v1, v5, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->SCHEDULE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    const-string/jumbo v1, "MY_PLACE"

    invoke-direct {v0, v1, v6, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->MY_PLACE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    const-string/jumbo v1, "FRIENDS_SHARED"

    invoke-direct {v0, v1, v7, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->FRIENDS_SHARED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    const-string/jumbo v1, "GROUP_SHARING"

    invoke-direct {v0, v1, v8, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->GROUP_SHARING:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    const-string/jumbo v1, "SEARCHED"

    const/4 v2, 0x5

    .line 5
    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->SEARCHED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    .line 3
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->SCHEDULE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->MY_PLACE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->FRIENDS_SHARED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->GROUP_SHARING:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->SEARCHED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 9
    iput p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->value:I

    .line 10
    return-void
.end method

.method public static getTypeFromInt(I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 17
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->SCHEDULE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    iget v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->value:I

    if-ne v0, p0, :cond_0

    .line 18
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->SCHEDULE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    .line 27
    :goto_0
    return-object v0

    .line 19
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->MY_PLACE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    iget v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->value:I

    if-ne v0, p0, :cond_1

    .line 20
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->MY_PLACE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    goto :goto_0

    .line 21
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->FRIENDS_SHARED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    iget v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->value:I

    if-ne v0, p0, :cond_2

    .line 22
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->FRIENDS_SHARED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    goto :goto_0

    .line 23
    :cond_2
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->GROUP_SHARING:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    iget v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->value:I

    if-ne v0, p0, :cond_3

    .line 24
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->GROUP_SHARING:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    goto :goto_0

    .line 25
    :cond_3
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->SEARCHED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    iget v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->value:I

    if-ne v0, p0, :cond_4

    .line 26
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->SEARCHED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    goto :goto_0

    .line 27
    :cond_4
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->value:I

    return v0
.end method
