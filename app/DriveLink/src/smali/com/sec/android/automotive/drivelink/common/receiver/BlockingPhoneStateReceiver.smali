.class public Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BlockingPhoneStateReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static VIBRATE_WHEN_RINGING:I

.field private static mPrevPhoneState:I


# instance fields
.field DuringCallToastHandlerdler:Landroid/os/Handler;

.field private isCheckDrivingMode:I

.field mContext:Landroid/content/Context;

.field private mNotiManager:Landroid/app/NotificationManager;

.field private mNotification:Landroid/app/Notification;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    const-class v0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;

    .line 30
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 29
    sput-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->TAG:Ljava/lang/String;

    .line 36
    sput v1, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->VIBRATE_WHEN_RINGING:I

    .line 37
    sput v1, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mPrevPhoneState:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 32
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->DuringCallToastHandlerdler:Landroid/os/Handler;

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->isCheckDrivingMode:I

    .line 27
    return-void
.end method

.method private sendEndCallTaskManger(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 140
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 141
    .local v0, "callEndIntent":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.automotive.drivelink.ACTION_TASK_CALL_END"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    const-string/jumbo v1, "com.sec.android.automotive.drivelink"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 143
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 144
    return-void
.end method

.method private updateNotification(Z)V
    .locals 10
    .param p1, "blockLaunchingCarMode"    # Z

    .prologue
    const v9, 0x7f0201e8

    const/16 v8, 0x1e3e

    const/4 v7, 0x1

    const v6, 0x7f0a0265

    const/4 v5, 0x0

    .line 86
    if-eqz p1, :cond_0

    .line 87
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "blockLaunchingCarMode:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 89
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.block_launch_car_mode_in_call"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mContext:Landroid/content/Context;

    const-class v3, Lcom/sec/android/automotive/drivelink/common/receiver/DuringCallToastReceiver;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 91
    const-string/jumbo v2, "com.sec.android.automotive.drivelink"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mContext:Landroid/content/Context;

    invoke-static {v2, v5, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 95
    .local v1, "pintent":Landroid/app/PendingIntent;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mContext:Landroid/content/Context;

    .line 96
    const-string/jumbo v3, "notification"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 95
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mNotiManager:Landroid/app/NotificationManager;

    .line 98
    new-instance v2, Landroid/app/Notification$Builder;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 100
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0264

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 99
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 102
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 101
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 103
    invoke-virtual {v2, v9}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 105
    new-instance v3, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v3}, Landroid/app/Notification$BigTextStyle;-><init>()V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mContext:Landroid/content/Context;

    .line 106
    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 105
    invoke-virtual {v3, v4}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v3

    .line 104
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 107
    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    .line 98
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mNotification:Landroid/app/Notification;

    .line 109
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mNotiManager:Landroid/app/NotificationManager;

    .line 110
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mNotification:Landroid/app/Notification;

    invoke-virtual {v2, v8, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 137
    :goto_0
    return-void

    .line 112
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "pintent":Landroid/app/PendingIntent;
    :cond_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "blockLaunchingCarMode:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mContext:Landroid/content/Context;

    const-class v3, Lcom/sec/android/automotive/drivelink/firstaccess/OngoingNotiActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 114
    .restart local v0    # "intent":Landroid/content/Intent;
    const/high16 v2, 0x30000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 117
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mContext:Landroid/content/Context;

    .line 118
    const-string/jumbo v3, "notification"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 117
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mNotiManager:Landroid/app/NotificationManager;

    .line 120
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mContext:Landroid/content/Context;

    invoke-static {v2, v5, v0, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 123
    .restart local v1    # "pintent":Landroid/app/PendingIntent;
    new-instance v2, Landroid/app/Notification$Builder;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 125
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0264

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 124
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 127
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 126
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 128
    invoke-virtual {v2, v9}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 130
    new-instance v3, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v3}, Landroid/app/Notification$BigTextStyle;-><init>()V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mContext:Landroid/content/Context;

    .line 131
    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 130
    invoke-virtual {v3, v4}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v3

    .line 129
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 132
    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    .line 123
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mNotification:Landroid/app/Notification;

    .line 134
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mNotiManager:Landroid/app/NotificationManager;

    .line 135
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mNotification:Landroid/app/Notification;

    invoke-virtual {v2, v8, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 41
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mContext:Landroid/content/Context;

    .line 43
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onReceive"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "car_mode_on"

    .line 45
    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->isCheckDrivingMode:I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    :goto_0
    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->isCheckDrivingMode:I

    if-ne v2, v5, :cond_1

    .line 53
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "android.intent.action.PHONE_STATE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 54
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "onReceive action:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mContext:Landroid/content/Context;

    .line 56
    const-string/jumbo v3, "phone"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 55
    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 58
    .local v1, "telephony":Landroid/telephony/TelephonyManager;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 59
    const-string/jumbo v3, "vibrate_when_ringing"

    .line 58
    invoke-static {v2, v3, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 61
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v2

    if-nez v2, :cond_2

    .line 62
    invoke-direct {p0, v6}, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->updateNotification(Z)V

    .line 65
    sget v2, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mPrevPhoneState:I

    if-eqz v2, :cond_0

    .line 66
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->sendEndCallTaskManger(Landroid/content/Context;)V

    .line 69
    :cond_0
    sput v6, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mPrevPhoneState:I

    .line 82
    .end local v1    # "telephony":Landroid/telephony/TelephonyManager;
    :cond_1
    :goto_1
    return-void

    .line 47
    :catch_0
    move-exception v0

    .line 49
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 70
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    .restart local v1    # "telephony":Landroid/telephony/TelephonyManager;
    :cond_2
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v2

    if-ne v2, v7, :cond_3

    .line 71
    sput v7, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mPrevPhoneState:I

    .line 72
    invoke-direct {p0, v5}, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->updateNotification(Z)V

    goto :goto_1

    .line 73
    :cond_3
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v2

    if-ne v2, v5, :cond_1

    .line 74
    sput v5, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->mPrevPhoneState:I

    .line 75
    invoke-direct {p0, v5}, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->updateNotification(Z)V

    goto :goto_1
.end method
