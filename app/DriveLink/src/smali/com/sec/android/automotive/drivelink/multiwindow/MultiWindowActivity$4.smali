.class Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$4;
.super Ljava/lang/Object;
.source "MultiWindowActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    .line 342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 345
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$15(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$15(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v0

    if-nez v0, :cond_0

    .line 347
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "MulitWindow:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$15(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 348
    const-string/jumbo v2, " Multi Window will be finished."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 347
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 350
    const/4 v1, 0x0

    .line 349
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->setMultiWindowState(Landroid/content/Context;I)V

    .line 351
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->finish()V

    .line 353
    :cond_0
    return-void
.end method
