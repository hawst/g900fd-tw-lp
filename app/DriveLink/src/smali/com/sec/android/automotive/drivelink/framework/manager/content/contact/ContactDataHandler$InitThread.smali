.class Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;
.super Ljava/lang/Thread;
.source "ContactDataHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InitThread"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private m_bStopped:Z

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;Ljava/lang/String;Landroid/content/Context;)V
    .locals 1
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 1644
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    .line 1645
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 1646
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;->mContext:Landroid/content/Context;

    .line 1647
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;->m_bStopped:Z

    .line 1648
    return-void
.end method

.method private isStopped()Z
    .locals 1

    .prologue
    .line 1655
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;->m_bStopped:Z

    return v0
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 1659
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;->isStopped()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1660
    const-string/jumbo v4, "CDH"

    const-string/jumbo v5, "initiazlieCaches : before sync"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1661
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->mContactCacheUpdate:Ljava/lang/Object;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 1662
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;->isStopped()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1663
    const-string/jumbo v4, "CDH"

    const-string/jumbo v6, "initiazlieCaches : after sync"

    invoke-static {v4, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1664
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1665
    .local v2, "startTime":J
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;->mContext:Landroid/content/Context;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->initializeCaches(Landroid/content/Context;)V
    invoke-static {v4, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;Landroid/content/Context;)V

    .line 1666
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1667
    .local v0, "endTime":J
    const-string/jumbo v4, "CDH"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "ContactDataHandler initiazlieCaches time : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v7, v0, v2

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1661
    .end local v0    # "endTime":J
    .end local v2    # "startTime":J
    :cond_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1670
    const-string/jumbo v4, "CDH"

    const-string/jumbo v5, "initiazlieCaches : end sync"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1673
    :cond_1
    return-void

    .line 1661
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method public stopThread()V
    .locals 1

    .prologue
    .line 1651
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$InitThread;->m_bStopped:Z

    .line 1652
    return-void
.end method
