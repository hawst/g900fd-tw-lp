.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLMessage.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getAttachmentInfo()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessageAttachment;
.end method

.method public abstract getMsgBody()Ljava/lang/String;
.end method

.method public abstract getMsgType()I
.end method

.method public abstract getPhoneNumber()Ljava/lang/String;
.end method

.method public abstract getReadStatus()I
.end method

.method public abstract getReceivedTime()J
.end method

.method public abstract setMsgBody(Ljava/lang/String;)V
.end method

.method public abstract setPhoneNumber(Ljava/lang/String;)V
.end method
