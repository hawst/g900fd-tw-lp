.class public Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;
.super Ljava/lang/Object;
.source "PathInfo.java"


# instance fields
.field public final ID:I

.field public final PAINT:Landroid/graphics/Paint;

.field public final PATH:Landroid/graphics/Path;


# direct methods
.method public constructor <init>(Landroid/graphics/Path;Landroid/graphics/Paint;I)V
    .locals 0
    .param p1, "path"    # Landroid/graphics/Path;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .param p3, "id"    # I

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;->PATH:Landroid/graphics/Path;

    .line 14
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;->PAINT:Landroid/graphics/Paint;

    .line 15
    iput p3, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;->ID:I

    .line 16
    return-void
.end method
