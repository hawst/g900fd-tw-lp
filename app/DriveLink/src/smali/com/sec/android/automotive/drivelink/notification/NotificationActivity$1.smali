.class Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "NotificationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    .line 45
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 49
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "mLcdOffReceiver onReceive."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->pauseNotiFlow()V

    .line 54
    :cond_0
    return-void
.end method
