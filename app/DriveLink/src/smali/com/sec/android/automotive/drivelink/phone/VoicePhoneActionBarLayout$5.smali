.class Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$5;
.super Ljava/lang/Object;
.source "VoicePhoneActionBarLayout.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->runTTSBarAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    .line 623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3
    .param p1, "arg0"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 627
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSBar:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->access$16(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)Landroid/widget/ImageView;

    move-result-object v0

    .line 628
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->mTTSBar:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->access$16(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getX()F

    move-result v2

    .line 627
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->getCurrentTTSBarImage(Landroid/content/res/Resources;F)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 629
    return-void
.end method
