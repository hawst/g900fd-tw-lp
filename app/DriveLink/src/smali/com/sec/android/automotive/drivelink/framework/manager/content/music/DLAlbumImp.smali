.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;
.source "DLAlbumImp.java"


# instance fields
.field private mAlbumId:I

.field private mArtist:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "albumId"    # I
    .param p2, "album"    # Ljava/lang/String;
    .param p3, "artist"    # Ljava/lang/String;
    .param p4, "songCount"    # I

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;-><init>()V

    .line 6
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;->mAlbumId:I

    .line 7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;->mArtist:Ljava/lang/String;

    .line 10
    invoke-virtual {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;->setName(Ljava/lang/String;)V

    .line 11
    invoke-virtual {p0, p4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;->setSongCount(I)V

    .line 13
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;->mAlbumId:I

    .line 14
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;->mArtist:Ljava/lang/String;

    .line 15
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "album"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;-><init>()V

    .line 6
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;->mAlbumId:I

    .line 7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;->mArtist:Ljava/lang/String;

    .line 18
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;->setName(Ljava/lang/String;)V

    .line 19
    return-void
.end method


# virtual methods
.method public getAlbumId()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;->mAlbumId:I

    return v0
.end method

.method public getArtist()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;->mArtist:Ljava/lang/String;

    return-object v0
.end method
