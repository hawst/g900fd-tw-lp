.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;
.source "DLPlaylistImp.java"


# static fields
.field public static final QUICK_LIST:Ljava/lang/String; = "Quick list"


# instance fields
.field mId:I


# direct methods
.method public constructor <init>(ILjava/lang/String;I)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "songCount"    # I

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;-><init>()V

    .line 10
    invoke-virtual {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;->setName(Ljava/lang/String;)V

    .line 11
    invoke-virtual {p0, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;->setSongCount(I)V

    .line 13
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;->mId:I

    .line 14
    return-void
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;->mId:I

    return v0
.end method
