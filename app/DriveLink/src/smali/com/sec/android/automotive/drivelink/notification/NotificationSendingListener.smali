.class public Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;
.super Ljava/lang/Object;
.source "NotificationSendingListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener$OnMessageSendingNotiListener;
    }
.end annotation


# static fields
.field public static final MESSAGE_STATE_NONE:I = 0x0

.field public static final MESSAGE_STATE_SENDING:I = 0x1

.field public static final MESSAGE_STATE_SENDING_FAILED:I = 0x2

.field public static final MESSAGE_STATE_SENDING_SUCCESS:I = 0x3

.field private static volatile _instance:Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;


# instance fields
.field mListener:Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener$OnMessageSendingNotiListener;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method public static getInstance()Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;
    .locals 2

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;

    if-nez v0, :cond_1

    .line 20
    const-class v1, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;

    monitor-enter v1

    .line 21
    :try_start_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;

    if-nez v0, :cond_0

    .line 22
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static getListener()Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener$OnMessageSendingNotiListener;
    .locals 2

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;

    if-nez v0, :cond_1

    .line 32
    const-class v1, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;

    monitor-enter v1

    .line 33
    :try_start_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;

    .line 32
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 39
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;->mListener:Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener$OnMessageSendingNotiListener;

    return-object v0

    .line 32
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public registerListener(Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener$OnMessageSendingNotiListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener$OnMessageSendingNotiListener;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;->mListener:Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener$OnMessageSendingNotiListener;

    .line 44
    return-void
.end method

.method public unregisterListener()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;->mListener:Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener$OnMessageSendingNotiListener;

    .line 48
    return-void
.end method
