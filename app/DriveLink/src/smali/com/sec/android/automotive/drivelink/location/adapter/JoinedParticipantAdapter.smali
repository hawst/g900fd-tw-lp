.class public Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter;
.super Landroid/widget/ArrayAdapter;
.source "JoinedParticipantAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;",
        ">;"
    }
.end annotation


# instance fields
.field private imgs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field private final inflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p2, "objects":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter;->imgs:Ljava/util/Map;

    .line 31
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 32
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v8, 0x0

    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 43
    if-nez p2, :cond_2

    .line 44
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 45
    const v7, 0x7f030059

    .line 44
    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 46
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;

    invoke-direct {v0, v8}, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;-><init>(Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;)V

    .line 48
    .local v0, "holder":Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;
    const v6, 0x7f0901f4

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 47
    invoke-static {v0, v6}, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->access$1(Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;Landroid/widget/ImageView;)V

    .line 50
    const v6, 0x7f0901f6

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 49
    invoke-static {v0, v6}, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->access$2(Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;Landroid/widget/TextView;)V

    .line 52
    const v6, 0x7f0901f5

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 51
    invoke-static {v0, v6}, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->access$3(Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;Landroid/widget/TextView;)V

    .line 53
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 58
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;

    .line 60
    .local v2, "participant":Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getName()Ljava/lang/String;

    move-result-object v3

    .line 61
    .local v3, "txt":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_1

    .line 62
    :cond_0
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getPhone()Ljava/lang/String;

    move-result-object v3

    .line 65
    :cond_1
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 67
    # getter for: Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->ivUser:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->access$4(Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 68
    # getter for: Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->tvUserLetter:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->access$5(Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 70
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter;->imgs:Ljava/util/Map;

    invoke-interface {v6, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/SoftReference;

    .line 71
    .local v1, "img":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Landroid/graphics/Bitmap;>;"
    const/4 v4, 0x0

    .line 72
    .local v4, "userBitmap":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_3

    .line 73
    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "userBitmap":Landroid/graphics/Bitmap;
    check-cast v4, Landroid/graphics/Bitmap;

    .line 74
    .restart local v4    # "userBitmap":Landroid/graphics/Bitmap;
    # getter for: Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->ivUser:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->access$4(Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 96
    :goto_1
    # getter for: Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->tvName:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->access$6(Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    return-object p2

    .line 55
    .end local v0    # "holder":Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;
    .end local v1    # "img":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Landroid/graphics/Bitmap;>;"
    .end local v2    # "participant":Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;
    .end local v3    # "txt":Ljava/lang/String;
    .end local v4    # "userBitmap":Landroid/graphics/Bitmap;
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;

    .restart local v0    # "holder":Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;
    goto :goto_0

    .line 78
    .restart local v1    # "img":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Landroid/graphics/Bitmap;>;"
    .restart local v2    # "participant":Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;
    .restart local v3    # "txt":Ljava/lang/String;
    .restart local v4    # "userBitmap":Landroid/graphics/Bitmap;
    :cond_3
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v6

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getDlContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v8

    .line 78
    invoke-interface {v6, v7, v8}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 80
    .local v5, "userBitmapTmp":Landroid/graphics/Bitmap;
    if-eqz v5, :cond_4

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 82
    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 83
    const v7, 0x7f0201a2

    .line 81
    invoke-static {v6, v5, v7}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 84
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 85
    # getter for: Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->ivUser:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->access$4(Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 86
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter;->imgs:Ljava/util/Map;

    invoke-interface {v6, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter;->imgs:Ljava/util/Map;

    .line 88
    new-instance v7, Ljava/lang/ref/SoftReference;

    invoke-direct {v7, v4}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    .line 87
    invoke-interface {v6, v2, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 90
    :cond_4
    # getter for: Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->ivUser:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->access$4(Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 91
    # getter for: Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->tvUserLetter:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->access$5(Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 92
    # getter for: Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->tvUserLetter:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->access$5(Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v3, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
