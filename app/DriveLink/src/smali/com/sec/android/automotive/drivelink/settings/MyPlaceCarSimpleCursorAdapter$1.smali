.class Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$1;
.super Ljava/lang/Object;
.source "MyPlaceCarSimpleCursorAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

.field private final synthetic val$context:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$1;->val$context:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 104
    .line 105
    const v5, 0x1020010

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 104
    check-cast v4, Landroid/widget/TextView;

    .line 107
    .local v4, "tvSummary":Landroid/widget/TextView;
    const v5, 0x7f09024e

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 106
    check-cast v3, Landroid/widget/TextView;

    .line 108
    .local v3, "tvSettingsCarMacAddress":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$1;->val$context:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    .line 109
    const v6, 0x7f0a03e6

    .line 108
    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 110
    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 109
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 111
    .local v1, "isConnected":Z
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$1;->val$context:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    .line 112
    const v6, 0x7f0a03ed

    .line 111
    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 113
    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 112
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 114
    .local v2, "isNotConnected":Z
    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    .line 115
    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 116
    .local v0, "btAddress":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 117
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    invoke-virtual {v5, v0, p1, v4}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->disconnect(Ljava/lang/String;Landroid/view/View;Landroid/widget/TextView;)V

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    if-eqz v2, :cond_0

    .line 120
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    invoke-virtual {v5, v0, p1, v4}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->connect(Ljava/lang/String;Landroid/view/View;Landroid/widget/TextView;)V

    goto :goto_0
.end method
