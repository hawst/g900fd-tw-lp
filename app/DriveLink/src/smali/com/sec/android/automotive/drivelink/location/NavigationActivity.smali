.class public Lcom/sec/android/automotive/drivelink/location/NavigationActivity;
.super Ljava/lang/Object;
.source "NavigationActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;


# static fields
.field private static mNavigationActivity:Lcom/sec/android/automotive/drivelink/location/NavigationActivity;

.field private static mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mAddress:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mLat:D

.field private mLng:D

.field private mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

.field private mPlaceName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    sput-object v0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mNavigationActivity:Lcom/sec/android/automotive/drivelink/location/NavigationActivity;

    .line 25
    sput-object v0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string/jumbo v0, "NavigationActivity"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->TAG:Ljava/lang/String;

    .line 28
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mLat:D

    .line 29
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mLng:D

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mContext:Landroid/content/Context;

    .line 39
    return-void
.end method

.method public static getInstance()Lcom/sec/android/automotive/drivelink/location/NavigationActivity;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mNavigationActivity:Lcom/sec/android/automotive/drivelink/location/NavigationActivity;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mNavigationActivity:Lcom/sec/android/automotive/drivelink/location/NavigationActivity;

    .line 35
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mNavigationActivity:Lcom/sec/android/automotive/drivelink/location/NavigationActivity;

    return-object v0
.end method

.method private getRequestLocation()Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;
    .locals 1

    .prologue
    .line 98
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    if-eqz v0, :cond_0

    .line 99
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    .line 103
    :goto_0
    return-object v0

    .line 101
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    .line 103
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    goto :goto_0
.end method

.method private getTTSParameter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mAddress:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mAddress:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mAddress:Ljava/lang/String;

    .line 155
    :goto_0
    return-object v0

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mPlaceName:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mPlaceName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 152
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mPlaceName:Ljava/lang/String;

    goto :goto_0

    .line 155
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private startMap(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->startMap(Landroid/content/Context;)V

    .line 168
    return-void
.end method

.method private startNavigation(DD)V
    .locals 7
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->startNavigation(Landroid/content/Context;DDLjava/lang/String;)V

    .line 160
    return-void
.end method

.method private startNavigation(Ljava/lang/String;)V
    .locals 7
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, 0x0

    .line 163
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->getTTSParameter()Ljava/lang/String;

    move-result-object v6

    move-wide v4, v2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->startNavigation(Landroid/content/Context;DDLjava/lang/String;)V

    .line 164
    return-void
.end method

.method private startSequence()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 122
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mLat:D

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mLng:D

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    .line 123
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mLat:D

    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mLng:D

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->startNavigation(DD)V

    .line 129
    :goto_0
    return-void

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mAddress:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mAddress:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 125
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mAddress:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->startNavigation(Ljava/lang/String;)V

    goto :goto_0

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mPlaceName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->startNavigation(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected isFlowManagerIntent(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v0, 0x0

    .line 107
    if-eqz p1, :cond_0

    .line 108
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 109
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 110
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.ACTION_DL_DM_FLOW_CHANGED"

    .line 109
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 110
    if-eqz v1, :cond_0

    .line 111
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 112
    const-string/jumbo v2, "EXTRA_FROM_FLOW_MGR"

    .line 111
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 112
    if-eqz v1, :cond_0

    .line 113
    const/4 v0, 0x1

    .line 116
    :cond_0
    return v0
.end method

.method public reponse(Landroid/location/Location;Z)V
    .locals 5
    .param p1, "location"    # Landroid/location/Location;
    .param p2, "timeout"    # Z

    .prologue
    .line 133
    const-string/jumbo v0, "NavigationActivity"

    const-string/jumbo v1, "onResponseRequestGeoLocation"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    if-nez p2, :cond_0

    if-eqz p1, :cond_0

    .line 136
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    .line 137
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    .line 136
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->setOrigin(DD)V

    .line 138
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->startSequence()V

    .line 144
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->startMap(Landroid/content/Context;)V

    .line 141
    const-string/jumbo v0, "NavigationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Timeout: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    const-string/jumbo v0, "NavigationActivity"

    const-string/jumbo v1, "Fail to get current location."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected requestGeolocation()Z
    .locals 2

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->getRequestLocation()Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->execute(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method protected requestGeolocation(J)Z
    .locals 2
    .param p1, "timeout"    # J

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->getRequestLocation()Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->execute(Landroid/content/Context;J)Z

    move-result v0

    return v0
.end method

.method public startNavigation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 42
    const-string/jumbo v0, "NavigationActivity"

    const-string/jumbo v1, "Navigation activity started"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    if-nez p1, :cond_1

    .line 45
    const-string/jumbo v0, "NavigationActivity"

    const-string/jumbo v1, "Invalid context to navigation."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 49
    :cond_1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mContext:Landroid/content/Context;

    .line 50
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/map/MapFactory;->newNavigationMap()Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    .line 52
    if-nez p2, :cond_2

    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->startMap(Landroid/content/Context;)V

    goto :goto_0

    .line 58
    :cond_2
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mPlaceName:Ljava/lang/String;

    .line 59
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mAddress:Ljava/lang/String;

    .line 60
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mLat:D

    .line 61
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mLng:D

    .line 66
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->isOriginNeeded()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 67
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->getRequestLocation()Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->setResponseListener(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;)V

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->requestGeolocation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    const-string/jumbo v0, "NavigationActivity"

    const-string/jumbo v1, "GPS and network providers are not enabled."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 72
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->startSequence()V

    goto :goto_0
.end method
