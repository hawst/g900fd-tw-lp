.class public Lcom/sec/android/automotive/drivelink/DLApplication;
.super Landroid/app/Application;
.source "DLApplication.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/DLApplication$ObserverContacts;
    }
.end annotation


# static fields
.field public static final BAIDU_API_KEY:Ljava/lang/String; = "nuWG7grx7kfK5HgMOneNEPXi"

.field public static DL_DEMO:Z = false

.field private static final TAG:Ljava/lang/String; = "DLApplication"

.field private static instance:Lcom/sec/android/automotive/drivelink/DLApplication;

.field public static mIsThereDialogActive:Z

.field public static startTime:J


# instance fields
.field private contactsChanged:Z

.field private mCurrentActivity:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

.field private mIsInitialized:Z

.field private mToolboxEnabled:Z

.field private final observerContacts:Lcom/sec/android/automotive/drivelink/DLApplication$ObserverContacts;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    sput-boolean v1, Lcom/sec/android/automotive/drivelink/DLApplication;->DL_DEMO:Z

    .line 40
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/DLApplication;->instance:Lcom/sec/android/automotive/drivelink/DLApplication;

    .line 46
    sput-boolean v1, Lcom/sec/android/automotive/drivelink/DLApplication;->mIsThereDialogActive:Z

    .line 58
    new-instance v0, Lcom/nuance/sample/coreaccess/SampleAppValues;

    new-instance v1, Lcom/sec/android/automotive/drivelink/DLApplication;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;-><init>()V

    invoke-direct {v0, v1}, Lcom/nuance/sample/coreaccess/SampleAppValues;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->setInterface(Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;)V

    .line 59
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 41
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->mIsInitialized:Z

    .line 49
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->contactsChanged:Z

    .line 50
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->mToolboxEnabled:Z

    .line 54
    new-instance v0, Lcom/sec/android/automotive/drivelink/DLApplication$ObserverContacts;

    .line 55
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/automotive/drivelink/DLApplication$ObserverContacts;-><init>(Lcom/sec/android/automotive/drivelink/DLApplication;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->observerContacts:Lcom/sec/android/automotive/drivelink/DLApplication$ObserverContacts;

    .line 265
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->mCurrentActivity:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    .line 36
    return-void
.end method

.method public static getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;
    .locals 1

    .prologue
    .line 250
    sget-object v0, Lcom/sec/android/automotive/drivelink/DLApplication;->instance:Lcom/sec/android/automotive/drivelink/DLApplication;

    return-object v0
.end method

.method private insertNavigationAvailable()V
    .locals 5

    .prologue
    .line 278
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 285
    .local v0, "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    const-string/jumbo v2, ""

    const-string/jumbo v3, ""

    invoke-interface {v0, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestMapAvailableByCountryAndSaleCode(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 288
    .local v1, "mListNavitationAvailable":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    .line 289
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 293
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "SKT"

    .line 294
    const-string/jumbo v4, "com.skt.skaf.l001mtm091"

    .line 293
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 295
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "SKT"

    .line 296
    const-string/jumbo v4, "com.google.android.apps.maps"

    .line 295
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 298
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "SKC"

    .line 299
    const-string/jumbo v4, "com.skt.skaf.l001mtm091"

    .line 298
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 300
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "SKC"

    .line 301
    const-string/jumbo v4, "com.google.android.apps.maps"

    .line 300
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 303
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "SKO"

    .line 304
    const-string/jumbo v4, "com.skt.skaf.l001mtm091"

    .line 303
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 305
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "SKO"

    .line 306
    const-string/jumbo v4, "com.google.android.apps.maps"

    .line 305
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 308
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "KTT"

    .line 309
    const-string/jumbo v4, "kt.navi"

    .line 308
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 312
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "KTT"

    .line 313
    const-string/jumbo v4, "com.google.android.apps.maps"

    .line 312
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 315
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "KTC"

    .line 316
    const-string/jumbo v4, "kt.navi"

    .line 315
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 319
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "KTC"

    .line 320
    const-string/jumbo v4, "com.google.android.apps.maps"

    .line 319
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 322
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "KTO"

    .line 323
    const-string/jumbo v4, "kt.navi"

    .line 322
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 326
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "KTO"

    .line 327
    const-string/jumbo v4, "com.google.android.apps.maps"

    .line 326
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 329
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "LGT"

    .line 330
    const-string/jumbo v4, "com.mnsoft.lgunavi"

    .line 329
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 333
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "LGT"

    .line 334
    const-string/jumbo v4, "com.google.android.apps.maps"

    .line 333
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 336
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "LUC"

    .line 337
    const-string/jumbo v4, "com.mnsoft.lgunavi"

    .line 336
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 340
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "LUC"

    .line 341
    const-string/jumbo v4, "com.google.android.apps.maps"

    .line 340
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 343
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "LUO"

    .line 344
    const-string/jumbo v4, "com.mnsoft.lgunavi"

    .line 343
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 347
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "LUO"

    .line 348
    const-string/jumbo v4, "com.google.android.apps.maps"

    .line 347
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 350
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "KOO"

    .line 351
    const-string/jumbo v4, "kt.navi"

    .line 350
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 352
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "KOO"

    .line 353
    const-string/jumbo v4, "com.skt.skaf.l001mtm091"

    .line 352
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 354
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "KOO"

    .line 355
    const-string/jumbo v4, "com.mnsoft.lgunavi"

    .line 354
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 356
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "KOO"

    .line 357
    const-string/jumbo v4, "com.google.android.apps.maps"

    .line 356
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 359
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "ANY"

    .line 360
    const-string/jumbo v4, "kt.navi"

    .line 359
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 361
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "ANY"

    .line 362
    const-string/jumbo v4, "com.skt.skaf.l001mtm091"

    .line 361
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 363
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "ANY"

    .line 364
    const-string/jumbo v4, "com.mnsoft.lgunavi"

    .line 363
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 365
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "ANY"

    .line 366
    const-string/jumbo v4, "com.google.android.apps.maps"

    .line 365
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 368
    const-string/jumbo v2, "KR"

    const-string/jumbo v3, "OTHERS"

    .line 369
    const-string/jumbo v4, "com.google.android.apps.maps"

    .line 368
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 383
    const-string/jumbo v2, "CN"

    const-string/jumbo v3, "OTHERS"

    .line 384
    const-string/jumbo v4, "com.baidu.BaiduMap"

    .line 383
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 386
    const-string/jumbo v2, "OTHERS"

    const-string/jumbo v3, "OTHERS"

    .line 387
    const-string/jumbo v4, "com.google.android.apps.maps"

    .line 386
    invoke-interface {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 401
    :cond_0
    return-void
.end method

.method public static isAppInitialized()Z
    .locals 1

    .prologue
    .line 254
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    const/4 v0, 0x1

    .line 257
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isInitialized()Z
    .locals 1

    .prologue
    .line 404
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->mIsInitialized:Z

    return v0
.end method


# virtual methods
.method public getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->mCurrentActivity:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    return-object v0
.end method

.method public getMainActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 262
    const-class v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    return-object v0
.end method

.method public googlePlayServicesAvailable()V
    .locals 4

    .prologue
    .line 408
    invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    .line 410
    .local v0, "code":I
    packed-switch v0, :pswitch_data_0

    .line 436
    :pswitch_0
    const-string/jumbo v1, "DLApplication"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "GooglePlayServices: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    :goto_0
    return-void

    .line 412
    :pswitch_1
    const-string/jumbo v1, "DLApplication"

    const-string/jumbo v2, "GooglePlayServices SUCCESS."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 416
    :pswitch_2
    const-string/jumbo v1, "DLApplication"

    const-string/jumbo v2, "GooglePlayServices SERVICE_MISSING."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 420
    :pswitch_3
    const-string/jumbo v1, "DLApplication"

    const-string/jumbo v2, "GooglePlayServices SERVICE_VERSION_UPDATE_REQUIRED."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 424
    :pswitch_4
    const-string/jumbo v1, "DLApplication"

    const-string/jumbo v2, "GooglePlayServices SERVICE_DISABLED."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 428
    :pswitch_5
    const-string/jumbo v1, "DLApplication"

    const-string/jumbo v2, "GooglePlayServices SERVICE_INVALID."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 410
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public declared-synchronized init()V
    .locals 5

    .prologue
    .line 122
    monitor-enter p0

    :try_start_0
    const-string/jumbo v2, "DLApplication"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Init Called!:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->mIsInitialized:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->mIsInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    .line 184
    :goto_0
    monitor-exit p0

    return-void

    .line 139
    :cond_0
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->mIsInitialized:Z

    .line 140
    const-string/jumbo v2, "DLApplication"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Init Started!:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->mIsInitialized:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    sput-wide v2, Lcom/sec/android/automotive/drivelink/DLApplication;->startTime:J

    .line 145
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v2

    .line 146
    const/4 v3, 0x0

    .line 145
    invoke-interface {v2, p0, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->initialize(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;)Z

    .line 148
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->getCallEndCheckSingleton()Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->initialize()V

    .line 151
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateAll()V

    .line 153
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/DLApplication;->insertNavigationAvailable()V

    .line 154
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/service/SetupPreferenceNavigation;->setUpNaviPrefSettings()V

    .line 157
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 156
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v2

    .line 158
    const-string/jumbo v3, "PREF_SETTINGS_MY_NAVIGATION_PACKAGE"

    .line 159
    const-string/jumbo v4, "com.google.android.apps.maps"

    .line 157
    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 160
    .local v1, "naviPackageName":Ljava/lang/String;
    const-string/jumbo v2, "LogNotNull"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Package name is: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    new-instance v0, Landroid/content/Intent;

    .line 163
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.NAVIGATION_PACKAGE_NAME"

    .line 162
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 164
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "com.android.phone"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 165
    const-string/jumbo v2, "INTENT_KEY_NAVIGATION_PACKAGE_NAME"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 170
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->sendBroadcast(Landroid/content/Intent;)V

    .line 183
    const-string/jumbo v2, "DLApplication"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Init End!:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->mIsInitialized:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 122
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "naviPackageName":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public initBaiduMapsEngine(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 98
    return-void
.end method

.method public isContactsChanged()Z
    .locals 1

    .prologue
    .line 443
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->contactsChanged:Z

    return v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    .line 63
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 64
    const-string/jumbo v1, "DLApplication"

    const-string/jumbo v2, "onCreate: Applicatino Started!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    sput-object p0, Lcom/sec/android/automotive/drivelink/DLApplication;->instance:Lcom/sec/android/automotive/drivelink/DLApplication;

    .line 67
    const-string/jumbo v1, "ro.csc.countryiso_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, "country":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 69
    const-string/jumbo v1, "CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 72
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->init()Z

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/DLApplication;->init()V

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/DLApplication;->googlePlayServicesAvailable()V

    .line 77
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 78
    const-string/jumbo v2, "PREF_CAR_APP_RUNNING"

    const/4 v3, 0x0

    .line 77
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 82
    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    .line 83
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->observerContacts:Lcom/sec/android/automotive/drivelink/DLApplication$ObserverContacts;

    .line 81
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 85
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->getInstance()Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->registerReceiver(Landroid/content/Context;)V

    .line 87
    const-string/jumbo v1, "DLApplication"

    const-string/jumbo v2, "PREF_CAR_APP_RUNNING is set to false"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    return-void
.end method

.method public setContactsChanged(Z)V
    .locals 0
    .param p1, "contactsChanged"    # Z

    .prologue
    .line 447
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->contactsChanged:Z

    .line 448
    return-void
.end method

.method public setCurrentActivity(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;)V
    .locals 0
    .param p1, "mCurrentActivity"    # Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    .prologue
    .line 272
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->mCurrentActivity:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    .line 273
    return-void
.end method

.method public declared-synchronized terminate()V
    .locals 5

    .prologue
    .line 187
    monitor-enter p0

    :try_start_0
    const-string/jumbo v2, "DLApplication"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Terminate Called:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->mIsInitialized:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->mIsInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    .line 247
    :goto_0
    monitor-exit p0

    return-void

    .line 191
    :cond_0
    :try_start_1
    const-string/jumbo v2, "DLApplication"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Terminate Started:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->mIsInitialized:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->mIsInitialized:Z

    .line 208
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateStopDrivingStatus()V

    .line 210
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->destroy()V

    .line 215
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->getInstance()Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->unregisterReceiver(Landroid/content/Context;)V

    .line 217
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->delCallEndCheckSingleton()V

    .line 219
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 220
    .local v1, "service":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->stopService(Landroid/content/Intent;)Z

    .line 223
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->releaseInstance()V

    .line 224
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v2

    .line 225
    const/4 v3, 0x0

    .line 224
    invoke-interface {v2, p0, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->terminate(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;)V

    .line 226
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->delDriveLinkServiceInterface()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 231
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "car_mode_on"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    .line 230
    if-ne v2, v3, :cond_1

    .line 232
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 233
    const-string/jumbo v3, "car_mode_on"

    const/4 v4, 0x0

    .line 232
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 234
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 235
    const-string/jumbo v3, "car_mode_blocking_system_key"

    const/4 v4, 0x0

    .line 234
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 237
    :cond_1
    const-string/jumbo v2, "DLApplication"

    const-string/jumbo v3, "change to car_mode_off"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 244
    :goto_1
    :try_start_3
    invoke-static {}, Lcom/vlingo/core/internal/audio/RemoteControlManager;->getInstance()Lcom/vlingo/core/internal/audio/RemoteControlManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/audio/RemoteControlManager;->destroy()V

    .line 246
    const-string/jumbo v2, "DLApplication"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Terminate End:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/DLApplication;->mIsInitialized:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 187
    .end local v1    # "service":Landroid/content/Intent;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 238
    .restart local v1    # "service":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 240
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    :try_start_4
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 241
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :catch_1
    move-exception v2

    goto :goto_1
.end method
