.class public Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;
.source "LocationShareActivity.java"


# static fields
.field public static final DLCONTACT:Ljava/lang/String; = "dlcontact"

.field public static final LOGS_MODE:I = 0x20

.field public static final MAX_LOGS:I = 0xf

.field public static final MAX_SUGGESTIONS:I = 0xc

.field public static final NUMBER_MODE:I = 0x80

.field public static final PHONE_MODE:Ljava/lang/String; = "PHONE_MODE"

.field public static final PHONE_NUMBER:Ljava/lang/String; = "phoneNumber"

.field public static final SEARCH_MODE:I = 0x40

.field public static final SIP_SEARCH_MODE:Ljava/lang/String; = "SIP_SEARCH_MODE"

.field public static final SIP_SEARCH_TEXT:Ljava/lang/String; = "SIP_SEARCH_TEXT"

.field public static final SUGGESTIONS_MODE:I = 0x10

.field private static final WAS_SHOWING_ALLOW_DIALOG:Ljava/lang/String; = "wasShowingDialog"


# instance fields
.field private allowDialog:Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;

.field private isLoaded:Z

.field private mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

.field private mAllListLayout:Landroid/widget/LinearLayout;

.field private mBackLayout:Landroid/widget/RelativeLayout;

.field private mBtnBack:Landroid/widget/ImageButton;

.field private mContactList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation
.end field

.field private mEmptyListLayout:Landroid/view/View;

.field private mOnUpdateLogListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;

.field private mOnUpdateSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;

.field private mPageNavigationLayout:Landroid/widget/RelativeLayout;

.field private mPhoneNumber:Ljava/lang/String;

.field private mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

.field private mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

.field private mRecommendationViewPager:Landroid/support/v4/view/ViewPager;

.field private mSIPState:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

.field protected mSearchMode:Z

.field mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

.field protected mSearchedText:Ljava/lang/String;

.field private mTextfieldLayout:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;-><init>()V

    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->isLoaded:Z

    .line 107
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    .line 61
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    return-void
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Z
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->isLoaded:Z

    return v0
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;Z)V
    .locals 0

    .prologue
    .line 91
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->isLoaded:Z

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->finish()V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mTextfieldLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 320
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->searchContacts(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mContactList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V
    .locals 0

    .prologue
    .line 380
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->updateContacts()V

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V
    .locals 0

    .prologue
    .line 477
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->setSuggetionListFlowParam()V

    return-void
.end method

.method static synthetic access$7()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;I)V
    .locals 0

    .prologue
    .line 521
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->startConfirm(I)V

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->allowDialog:Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;

    return-object v0
.end method

.method private applyFilter(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 459
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isSearchMode()Z

    move-result v2

    if-nez v2, :cond_0

    .line 475
    :goto_0
    return-void

    .line 462
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 463
    .local v1, "toRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 473
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 463
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 465
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    .line 466
    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 468
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getTextFromEditTextSearch()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 469
    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    .line 467
    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    .line 469
    if-nez v3, :cond_1

    .line 470
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private bindViews()V
    .locals 2

    .prologue
    .line 161
    const v0, 0x7f09009e

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;

    .line 162
    const v0, 0x7f09008a

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 163
    const v0, 0x7f09009c

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mAllListLayout:Landroid/widget/LinearLayout;

    .line 164
    const v0, 0x7f09009f

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mEmptyListLayout:Landroid/view/View;

    .line 165
    const v0, 0x7f09009d

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mBackLayout:Landroid/widget/RelativeLayout;

    .line 166
    const v0, 0x7f09009b

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 167
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const v1, 0x7f090331

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mBtnBack:Landroid/widget/ImageButton;

    .line 168
    return-void
.end method

.method private configureVoiceActionBar()V
    .locals 3

    .prologue
    const v2, 0x7f0a020f

    .line 235
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const v1, 0x7f0a02b3

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setTitleBar(I)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setBtnSearchVisible()V

    .line 237
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setVoiceLayoutOnClickListener(I)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setSayListeningText(I)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setSayText(I)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 241
    const v1, 0x7f0a05eb

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setVoiceLayoutOnClickListener(I)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setContactsHint()V

    .line 243
    return-void
.end method

.method private getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 568
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$15;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$15;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V

    return-object v0
.end method

.method private getListItemOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 507
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$14;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$14;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V

    return-object v0
.end method

.method private getLocationSipStateListener()Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$OnLocationSipStateListener;
    .locals 1

    .prologue
    .line 201
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V

    return-object v0
.end method

.method private getMicStateChangeListener()Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;
    .locals 1

    .prologue
    .line 581
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$16;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$16;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V

    return-object v0
.end method

.method private getUpdateLogListListener()Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;
    .locals 1

    .prologue
    .line 312
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$9;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$9;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V

    return-object v0
.end method

.method private getUpdateSuggestionContactListener()Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;
    .locals 1

    .prologue
    .line 300
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$8;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$8;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V

    return-object v0
.end method

.method private initSearchTextField()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 172
    const v1, 0x7f090333

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 171
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mTextfieldLayout:Landroid/widget/RelativeLayout;

    .line 173
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 174
    const v1, 0x7f09034a

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    .line 173
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    .line 178
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setOnSearchBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    .line 189
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->getLocationSipStateListener()Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$OnLocationSipStateListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->setOnLocationSipStateListener(Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$OnLocationSipStateListener;)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mBtnBack:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    return-void
.end method

.method private loadAllContacts()V
    .locals 0

    .prologue
    .line 376
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->updateContacts()V

    .line 377
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->setSuggetionListFlowParam()V

    .line 378
    return-void
.end method

.method private prepareAllowShareDialog()V
    .locals 4

    .prologue
    .line 353
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->allowDialog:Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;->show()V

    .line 354
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->allowDialog:Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;

    const v3, 0x7f09020d

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 355
    .local v0, "btCancel":Landroid/widget/Button;
    new-instance v2, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$11;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$11;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 361
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->allowDialog:Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;

    const v3, 0x7f09020e

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 362
    .local v1, "btOk":Landroid/widget/Button;
    new-instance v2, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$12;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$12;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 368
    return-void
.end method

.method private searchContacts(Ljava/lang/String;)V
    .locals 2
    .param p1, "searchCriteria"    # Ljava/lang/String;

    .prologue
    .line 322
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 325
    .local v0, "dlServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$10;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$10;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkContactListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;)V

    .line 347
    invoke-interface {v0, p0, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestSearchedContactList(Landroid/content/Context;Ljava/lang/String;)V

    .line 349
    return-void
.end method

.method private setInitUI(Landroid/content/Intent;)V
    .locals 14
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v13, 0x0

    .line 722
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.sec.android.automotive.drivelink.ACTION_DL_DM_FLOW_CHANGED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 839
    :cond_0
    :goto_0
    return-void

    .line 729
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "EXTRA_FROM_FLOW_MGR"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 740
    const/4 v11, 0x0

    .line 741
    .local v11, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "EXTRA_FLOW_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 742
    .local v10, "flowID":Ljava/lang/String;
    invoke-static {v10}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v11

    .line 743
    sget-object v1, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "flow ID :: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 774
    .local v9, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    const-string/jumbo v1, "DM_LOCATION_CONTACT_SHARE"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 775
    iget-object v8, v11, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactChoices:Ljava/util/List;

    .line 776
    .local v8, "contactChoiceList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    if-eqz v8, :cond_2

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 777
    :cond_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Problem with contacts return on voice search"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 780
    :cond_3
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_6

    .line 797
    .end local v8    # "contactChoiceList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_4
    const-string/jumbo v1, "DM_LOCATION_CONTACT_SHARE_SEARCH_LIST"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 805
    iget-object v8, v11, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactChoices:Ljava/util/List;

    .line 806
    .restart local v8    # "contactChoiceList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    if-eqz v8, :cond_5

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 807
    :cond_5
    sget-object v1, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Problem with contacts return on voice search"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 780
    :cond_6
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 782
    .local v7, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 781
    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 783
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 784
    iget-wide v2, v7, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    .line 783
    invoke-virtual {v1, p0, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getPhoneNumberList(Landroid/content/Context;J)Ljava/util/ArrayList;

    move-result-object v6

    .line 786
    .local v6, "phoneNumber":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 787
    iget-wide v1, v7, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    iget-object v3, v7, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 788
    invoke-virtual {v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v5

    .line 786
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 790
    .local v0, "dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 791
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->startConfirm(I)V

    goto :goto_1

    .line 811
    .end local v0    # "dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v6    # "phoneNumber":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    .end local v7    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_7
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_9

    .line 823
    iput-object v9, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mContactList:Ljava/util/ArrayList;

    .line 824
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->updateContacts()V

    .line 827
    .end local v8    # "contactChoiceList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_8
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_a

    .line 828
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1, v13}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setResutlText(I)V

    .line 829
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mEmptyListLayout:Landroid/view/View;

    invoke-virtual {v1, v13}, Landroid/view/View;->setVisibility(I)V

    .line 830
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 831
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 832
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 833
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->invalidate()V

    goto/16 :goto_0

    .line 811
    .restart local v8    # "contactChoiceList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_9
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 813
    .restart local v7    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 812
    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 814
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 815
    iget-wide v2, v7, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    .line 814
    invoke-virtual {v1, p0, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getPhoneNumberList(Landroid/content/Context;J)Ljava/util/ArrayList;

    move-result-object v6

    .line 817
    .restart local v6    # "phoneNumber":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 818
    iget-wide v1, v7, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    iget-object v3, v7, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 819
    invoke-virtual {v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v5

    .line 817
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 821
    .restart local v0    # "dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 836
    .end local v0    # "dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v6    # "phoneNumber":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    .end local v7    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v8    # "contactChoiceList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_a
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    .line 837
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mOnUpdateSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;

    .line 836
    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->setOnUpdateSugessionListListener(Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;)V

    goto/16 :goto_0
.end method

.method private setSuggetionListFlowParam()V
    .locals 5

    .prologue
    .line 478
    const/4 v1, 0x0

    .line 480
    .local v1, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v2, "DM_SMS_CONTACT"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v1

    .line 482
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mContactList:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 493
    :cond_0
    return-void

    .line 486
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 487
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    .line 489
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 490
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    iget-object v3, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private setupListeners()V
    .locals 2

    .prologue
    .line 246
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->getUpdateLogListListener()Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mOnUpdateLogListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;

    .line 247
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->getUpdateSuggestionContactListener()Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mOnUpdateSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;

    .line 249
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    .line 250
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mOnUpdateLogListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;

    .line 249
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->setOnUpdateLogListListener(Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;)V

    .line 251
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    .line 252
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mOnUpdateSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;

    .line 251
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->setOnUpdateSugessionListListener(Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mBtnBack:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setISearchListener(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$ISearchListener;)V

    .line 279
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$7;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 297
    return-void
.end method

.method private startConfirm(I)V
    .locals 8
    .param p1, "currentItem"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 522
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v4

    if-nez v4, :cond_1

    .line 524
    const v4, 0x7f0a0321

    .line 523
    invoke-static {p0, v4, v7}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;II)Landroid/widget/Toast;

    move-result-object v2

    .line 526
    .local v2, "toast":Landroid/widget/Toast;
    if-eqz v2, :cond_0

    .line 527
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 565
    .end local v2    # "toast":Landroid/widget/Toast;
    :cond_0
    :goto_0
    return-void

    .line 533
    :cond_1
    if-gez p1, :cond_3

    .line 534
    const/4 p1, 0x0

    .line 538
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 542
    .local v3, "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-eqz v3, :cond_0

    .line 545
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 546
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_4

    .line 547
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mPhoneNumber:Ljava/lang/String;

    .line 551
    :goto_2
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 553
    .local v0, "params":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const/4 v4, 0x3

    iput v4, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserIndex:I

    .line 554
    invoke-static {v3}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v4

    iput-object v4, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 555
    iput-boolean v7, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mIsShareMyLocation:Z

    .line 556
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 557
    const v5, 0x7f02024e

    .line 556
    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mBitmap:Landroid/graphics/Bitmap;

    .line 559
    const-string/jumbo v4, "DM_LOCATION_SHARE_CONFIRM"

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 558
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->getPrompt(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 561
    .local v1, "prompt":Ljava/lang/String;
    new-array v5, v7, [Ljava/lang/Object;

    .line 562
    if-nez v3, :cond_5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :goto_3
    aput-object v4, v5, v6

    .line 561
    invoke-static {v1, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 564
    const-string/jumbo v4, "DM_LOCATION_SHARE_CONFIRM"

    .line 563
    invoke-static {v4, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0

    .line 535
    .end local v0    # "params":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v1    # "prompt":Ljava/lang/String;
    .end local v3    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt p1, v4, :cond_2

    .line 536
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 p1, v4, -0x1

    goto :goto_1

    .line 549
    .restart local v3    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :cond_4
    const-string/jumbo v4, ""

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mPhoneNumber:Ljava/lang/String;

    goto :goto_2

    .line 562
    .restart local v0    # "params":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .restart local v1    # "prompt":Ljava/lang/String;
    :cond_5
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    goto :goto_3
.end method

.method private updateContacts()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 381
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mContactList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 382
    :cond_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v3, v7}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setResutlText(I)V

    .line 383
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mEmptyListLayout:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 384
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 385
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v3

    if-lez v3, :cond_1

    .line 386
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 387
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->invalidate()V

    .line 456
    :cond_1
    :goto_0
    return-void

    .line 391
    :cond_2
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mContactList:Ljava/util/ArrayList;

    invoke-static {v3}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContactMatchList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 393
    .local v0, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 394
    .local v2, "size":I
    const/16 v3, 0x28

    if-le v2, v3, :cond_3

    .line 395
    const/16 v2, 0x28

    .line 401
    :cond_3
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mEmptyListLayout:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 402
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, v7}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 405
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mContactList:Ljava/util/ArrayList;

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->adjustToMaxSizeSuggestionList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    .line 404
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mContactList:Ljava/util/ArrayList;

    .line 406
    new-instance v3, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    .line 407
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mContactList:Ljava/util/ArrayList;

    .line 408
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->getListItemOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V

    .line 406
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    .line 409
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 410
    new-instance v3, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    .line 411
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;-><init>(Landroid/content/Context;)V

    .line 410
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    .line 412
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 413
    const/4 v3, -0x1

    const/4 v4, -0x2

    .line 412
    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 414
    .local v1, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v3, 0xd

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 415
    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 416
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v3, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 417
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setGravity(I)V

    .line 418
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 419
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->notifyDataSetChanged()V

    .line 420
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    .line 421
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->getPageCount()I

    move-result v4

    .line 422
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v5

    .line 420
    invoke-virtual {v3, v4, v7, v5}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setDotIndicator(IILandroid/view/View$OnClickListener;)V

    .line 423
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;

    .line 424
    new-instance v4, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$13;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$13;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public finish()V
    .locals 2

    .prologue
    .line 668
    # invokes: Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->finish()V
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$15(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V

    .line 669
    const v0, 0x7f040025

    const v1, 0x7f040001

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->overridePendingTransition(II)V

    .line 673
    return-void
.end method

.method protected getLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    .locals 1

    .prologue
    .line 646
    const/4 v0, 0x0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 843
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isSearchMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 847
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->showMic()V

    .line 848
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->updateMicState(Lcom/nuance/sample/MicState;)V

    .line 849
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->loadAllContacts()V

    .line 855
    :goto_0
    return-void

    .line 853
    :cond_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 111
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onCreate(Landroid/os/Bundle;)V

    .line 112
    const v2, 0x7f030016

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->setContentView(I)V

    .line 114
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->bindViews()V

    .line 115
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->configureVoiceActionBar()V

    .line 116
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->setupListeners()V

    .line 118
    new-instance v2, Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->allowDialog:Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;

    .line 120
    if-nez p1, :cond_1

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mSearchMode:Z

    .line 122
    if-nez p1, :cond_2

    move-object v2, v4

    :goto_1
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mSearchedText:Ljava/lang/String;

    .line 125
    if-eqz p1, :cond_0

    .line 126
    const-string/jumbo v2, "search_mode"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 127
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->showSearch()V

    .line 128
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 129
    const-string/jumbo v4, "search_text"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 128
    invoke-virtual {v2, v4}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setSearchText(Ljava/lang/String;)V

    .line 132
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->initSearchTextField()V

    .line 134
    if-eqz p1, :cond_3

    .line 135
    const-string/jumbo v2, "search_mode"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 136
    const-string/jumbo v2, "search_text"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->searchContacts(Ljava/lang/String;)V

    .line 143
    :goto_2
    :try_start_0
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v2, v3}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    :goto_3
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updatePhoneSuggestionList()V

    .line 157
    const/high16 v2, 0x7f040000

    const v3, 0x7f040025

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->overridePendingTransition(II)V

    .line 158
    return-void

    .line 121
    :cond_1
    const-string/jumbo v2, "SIP_SEARCH_MODE"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    goto :goto_0

    .line 123
    :cond_2
    const-string/jumbo v2, "SIP_SEARCH_TEXT"

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 138
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->loadAllContacts()V

    goto :goto_2

    .line 144
    :catch_0
    move-exception v0

    .line 145
    .local v0, "e":Ljava/lang/Exception;
    const v2, 0x7f0a0296

    .line 146
    const/4 v3, 0x1

    .line 145
    invoke-static {p0, v2, v3}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;II)Landroid/widget/Toast;

    move-result-object v1

    .line 148
    .local v1, "toast":Landroid/widget/Toast;
    if-eqz v1, :cond_4

    .line 149
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 152
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 606
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onDestroy()V

    .line 607
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 608
    return-void
.end method

.method protected onFlowListSelected(Ljava/lang/String;IZ)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;
    .param p2, "index"    # I
    .param p3, "isOrdinal"    # Z

    .prologue
    .line 499
    if-eqz p3, :cond_0

    .line 500
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    add-int/2addr p2, v0

    .line 503
    :cond_0
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->startConfirm(I)V

    .line 504
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 715
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 716
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 717
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->setInitUI(Landroid/content/Intent;)V

    .line 719
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 612
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onPause()V

    .line 614
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 632
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 633
    const-string/jumbo v0, "wasShowingDialog"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 634
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->prepareAllowShareDialog()V

    .line 636
    :cond_0
    const-string/jumbo v0, "search_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 637
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->showSearch()V

    .line 638
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 639
    const-string/jumbo v1, "search_text"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 638
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setSearchText(Ljava/lang/String;)V

    .line 641
    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 698
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onResume()V

    .line 699
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isSearchMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 700
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->stopDmFlow()V

    .line 701
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelDialog()V

    .line 702
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->showSearch()V

    .line 704
    :try_start_0
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1, v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 712
    :cond_0
    :goto_0
    return-void

    .line 705
    :catch_0
    move-exception v0

    .line 706
    .local v0, "e":Ljava/lang/Exception;
    const v1, 0x7f0a0296

    .line 707
    const/4 v2, 0x1

    .line 706
    invoke-static {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;II)Landroid/widget/Toast;

    move-result-object v1

    .line 707
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 618
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 619
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isSearchMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 620
    const-string/jumbo v0, ""

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getSearchText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 621
    const-string/jumbo v0, "search_mode"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 622
    const-string/jumbo v0, "search_text"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getSearchText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    :cond_0
    const-string/jumbo v0, "wasShowingDialog"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->allowDialog:Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;->isShowing()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 625
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->allowDialog:Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 626
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->allowDialog:Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;->dismiss()V

    .line 628
    :cond_1
    return-void
.end method

.method protected setDayMode()V
    .locals 3

    .prologue
    const v2, 0x7f08002a

    .line 884
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->setDayMode()V

    .line 886
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    if-eqz v0, :cond_0

    .line 887
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setDayMode(Z)V

    .line 890
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->setDayMode()V

    .line 892
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mBackLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 893
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mBackLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 896
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mEmptyListLayout:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 897
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mEmptyListLayout:Landroid/view/View;

    .line 898
    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 901
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    if-eqz v0, :cond_3

    .line 902
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->notifyDataSetChanged()V

    .line 903
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->updateContacts()V

    .line 905
    :cond_3
    return-void
.end method

.method protected setNightMode()V
    .locals 3

    .prologue
    const v2, 0x7f08002e

    .line 859
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->setNightMode()V

    .line 861
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    if-eqz v0, :cond_0

    .line 862
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setNightMode(Z)V

    .line 865
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->setNightMode()V

    .line 867
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mBackLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 868
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mBackLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 871
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mEmptyListLayout:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 872
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mEmptyListLayout:Landroid/view/View;

    .line 873
    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 876
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    if-eqz v0, :cond_3

    .line 877
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->notifyDataSetChanged()V

    .line 878
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->updateContacts()V

    .line 880
    :cond_3
    return-void
.end method
