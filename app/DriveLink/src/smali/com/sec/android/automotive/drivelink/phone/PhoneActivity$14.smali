.class Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$14;
.super Ljava/lang/Object;
.source "PhoneActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->showSearchResult(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    .line 1548
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 1571
    const-string/jumbo v0, "PhoneActivity"

    const-string/jumbo v1, "showSearchResult - setAutoShrink : false n true"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1572
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$24(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V

    .line 1573
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$24(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V

    .line 1574
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 2
    .param p1, "arg0"    # I
    .param p2, "arg1"    # F
    .param p3, "arg2"    # I

    .prologue
    .line 1559
    if-nez p1, :cond_0

    .line 1560
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$36(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$35(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    move-result-object v1

    .line 1561
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->getInitialPosition()I

    move-result v1

    .line 1560
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1562
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$34(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 1567
    :goto_0
    return-void

    .line 1564
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$34(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 1565
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$35(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 1564
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    goto :goto_0
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 1552
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$34(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 1553
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$35(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 1552
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 1555
    return-void
.end method
