.class Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$2;
.super Ljava/lang/Object;
.source "NotificationCenter.java"

# interfaces
.implements Lcom/nuance/drivelink/DLUiUpdater;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    .line 1092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public displayError(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 1097
    return-void
.end method

.method public displaySystemTurn(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 1109
    return-void
.end method

.method public displayUserTurn(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 1103
    return-void
.end method

.method public displayWidgetContent(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 1115
    return-void
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 1

    .prologue
    .line 1165
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMicState()Lcom/nuance/sample/MicState;
    .locals 1

    .prologue
    .line 1141
    const/4 v0, 0x0

    return-object v0
.end method

.method public handleUserCancel()V
    .locals 0

    .prologue
    .line 1160
    return-void
.end method

.method public onClickable(Z)V
    .locals 0
    .param p1, "isClickable"    # Z

    .prologue
    .line 1172
    return-void
.end method

.method public onDisplayMic(Z)V
    .locals 0
    .param p1, "isMicDisplayed"    # Z

    .prologue
    .line 1184
    return-void
.end method

.method public onPhraseSpotterStateChanged(Z)V
    .locals 0
    .param p1, "isSpotting"    # Z

    .prologue
    .line 1178
    return-void
.end method

.method public setOnMicStateChangeListener(Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .prologue
    .line 1148
    return-void
.end method

.method public updateMicRMSChange(I)V
    .locals 0
    .param p1, "rmsValue"    # I

    .prologue
    .line 1154
    return-void
.end method

.method public updateMicState(Lcom/nuance/sample/MicState;)V
    .locals 4
    .param p1, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/4 v3, 0x0

    .line 1119
    const-string/jumbo v0, "UiUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1120
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->access$1(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;Lcom/nuance/sample/MicState;)V

    .line 1122
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mMicState:Lcom/nuance/sample/MicState;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->access$2(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;)Lcom/nuance/sample/MicState;

    move-result-object v0

    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasPending:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->access$3(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1123
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "Notify Pending Item with Delay"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1124
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    invoke-static {v0, v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->access$4(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;Z)V

    .line 1125
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotifyHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->access$5(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1126
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotifyHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->access$5(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;)Landroid/os/Handler;

    move-result-object v0

    .line 1127
    const-wide/16 v1, 0x12c

    .line 1126
    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1136
    :cond_0
    return-void
.end method
