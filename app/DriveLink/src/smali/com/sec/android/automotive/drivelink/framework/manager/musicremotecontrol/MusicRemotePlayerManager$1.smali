.class Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$1;
.super Landroid/os/Handler;
.source "MusicRemotePlayerManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    .line 582
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 585
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 586
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicRemotePlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v1

    if-nez v1, :cond_1

    .line 648
    :cond_0
    :goto_0
    return-void

    .line 590
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 592
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicRemotePlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v1

    .line 593
    invoke-interface {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;->onMusicRemotePlayerPlay()V

    goto :goto_0

    .line 597
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicRemotePlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v1

    .line 598
    invoke-interface {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;->onMusicRemotePlayerPause()V

    goto :goto_0

    .line 602
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicRemotePlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v1

    .line 603
    invoke-interface {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;->onMusicRemotePlayerPlayPause()V

    goto :goto_0

    .line 607
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicRemotePlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v1

    .line 608
    invoke-interface {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;->onMusicRemotePlayerPrevious()V

    goto :goto_0

    .line 613
    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicRemotePlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v1

    .line 614
    invoke-interface {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;->onMusicRemotePlayerNext()V

    goto :goto_0

    .line 618
    :pswitch_5
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-nez v1, :cond_2

    .line 619
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicRemotePlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v1

    .line 620
    invoke-interface {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;->onMusicRemotePlayerRewind(Z)V

    goto :goto_0

    .line 622
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicRemotePlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v1

    .line 623
    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;->onMusicRemotePlayerRewind(Z)V

    goto :goto_0

    .line 628
    :pswitch_6
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-nez v1, :cond_3

    .line 629
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicRemotePlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v1

    .line 630
    invoke-interface {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;->onMusicRemotePlayerFForward(Z)V

    goto :goto_0

    .line 632
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicRemotePlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v1

    .line 633
    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;->onMusicRemotePlayerFForward(Z)V

    goto/16 :goto_0

    .line 638
    :pswitch_7
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicRemotePlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v1

    .line 639
    invoke-interface {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;->onMusicRemotePlayerStop()V

    goto/16 :goto_0

    .line 643
    :pswitch_8
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 644
    .local v0, "newPositionMs":I
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicRemotePlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v1

    .line 645
    invoke-interface {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;->onMusicRemotePositionUpdate(I)V

    goto/16 :goto_0

    .line 590
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_7
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_8
    .end packed-switch
.end method
