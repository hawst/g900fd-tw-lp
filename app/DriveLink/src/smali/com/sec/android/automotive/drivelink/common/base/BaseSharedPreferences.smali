.class public Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
.super Ljava/lang/Object;
.source "BaseSharedPreferences.java"


# static fields
.field public static final DB_KEY_DRIVELINK_REJECTMESSAGE_BODY:Ljava/lang/String; = "drivelink_rejectmessage_body"

.field public static final DB_KEY_DRIVELINK_REJECTMESSAGE_ON:Ljava/lang/String; = "drivelink_rejectmessage_on"

.field public static final PREF_ACCEPT_DISCLAIMER_PLACE_ON:Ljava/lang/String; = "PREF_ACCEPT_DISCLAIMER_PLACE_ON"

.field public static final PREF_AUTO_LAUNCH_DURING_CALL:Ljava/lang/String; = "PREF_AUTO_LAUNCH_DURING_CALL"

.field public static final PREF_CAR_APP_RUNNING:Ljava/lang/String; = "PREF_CAR_APP_RUNNING"

.field public static final PREF_CAR_MODE_CLOSED_BY_TALKBACK_ON:Ljava/lang/String; = "PREF_CAR_MODE_CLOSED_BY_TALKBACK_ON"

.field public static final PREF_LAST_CONNECTED_REGISTERED_CAR:Ljava/lang/String; = "PREF_LAST_CONNECTED_REGISTERED_CAR"

.field public static final PREF_LOCATION_SHARE_REQUEST_WAITING:Ljava/lang/String; = "PREF_LOCATION_SHARE_REQUEST_WAITING"

.field private static final PREF_NAME:Ljava/lang/String; = "com.sec.android.automotive.drivelink"

.field public static final PREF_SETTINGS_ACCOUNT_EMAIL:Ljava/lang/String; = "PREF_SETTINGS_ACCOUNT_EMAIL"

.field public static final PREF_SETTINGS_COMMUTE_NOTIFICATION:Ljava/lang/String; = "PREF_SETTINGS_COMMUTE_NOTIFICATION"

.field public static final PREF_SETTINGS_COMMUTE_NOTIFICATION_FRIDAY:Ljava/lang/String; = "PREF_SETTINGS_COMMUTE_NOTIFICATION_FRIDAY"

.field public static final PREF_SETTINGS_COMMUTE_NOTIFICATION_FROMHOME:Ljava/lang/String; = "PREF_SETTINGS_COMMUTE_NOTIFICATION_FROMHOME"

.field public static final PREF_SETTINGS_COMMUTE_NOTIFICATION_FROMWORK:Ljava/lang/String; = "PREF_SETTINGS_COMMUTE_NOTIFICATION_FROMWORK"

.field public static final PREF_SETTINGS_COMMUTE_NOTIFICATION_MONDAY:Ljava/lang/String; = "PREF_SETTINGS_COMMUTE_NOTIFICATION_MONDAY"

.field public static final PREF_SETTINGS_COMMUTE_NOTIFICATION_SATURDAY:Ljava/lang/String; = "PREF_SETTINGS_COMMUTE_NOTIFICATION_SATURDAY"

.field public static final PREF_SETTINGS_COMMUTE_NOTIFICATION_SUNDAY:Ljava/lang/String; = "PREF_SETTINGS_COMMUTE_NOTIFICATION_SUNDAY"

.field public static final PREF_SETTINGS_COMMUTE_NOTIFICATION_THURSDAY:Ljava/lang/String; = "PREF_SETTINGS_COMMUTE_NOTIFICATION_THURSDAY"

.field public static final PREF_SETTINGS_COMMUTE_NOTIFICATION_TOHOME:Ljava/lang/String; = "PREF_SETTINGS_COMMUTE_NOTIFICATION_TOHOME"

.field public static final PREF_SETTINGS_COMMUTE_NOTIFICATION_TOWORK:Ljava/lang/String; = "PREF_SETTINGS_COMMUTE_NOTIFICATION_TOWORK"

.field public static final PREF_SETTINGS_COMMUTE_NOTIFICATION_TUESDAY:Ljava/lang/String; = "PREF_SETTINGS_COMMUTE_NOTIFICATION_TUESDAY"

.field public static final PREF_SETTINGS_COMMUTE_NOTIFICATION_WEDNESDAY:Ljava/lang/String; = "PREF_SETTINGS_COMMUTE_NOTIFICATION_WEDNESDAY"

.field public static final PREF_SETTINGS_GAS_STATION_FUEL_TYPE:Ljava/lang/String; = "PREF_SETTINGS_GAS_STATION_FUEL_TYPE"

.field public static final PREF_SETTINGS_GAS_STATION_ORDER:Ljava/lang/String; = "PREF_SETTINGS_GAS_STATION_ORDER"

.field public static final PREF_SETTINGS_GENERAL_REJECT_MESSAGE_DEFAULT:Ljava/lang/String; = "PREF_SETTINGS_GENERAL_REJECT_MESSAGE_DEFAULT"

.field public static final PREF_SETTINGS_HOME:Ljava/lang/String; = "PREF_SETTINGS_HOME"

.field public static final PREF_SETTINGS_MY_CAR_AUTO_LAUNCH:Ljava/lang/String; = "PREF_SETTINGS_MY_CAR_AUTO_LAUNCH"

.field public static final PREF_SETTINGS_MY_NAVIGATION:Ljava/lang/String; = "PREF_SETTINGS_MY_NAVIGATION"

.field public static final PREF_SETTINGS_MY_NAVIGATION_PACKAGE:Ljava/lang/String; = "PREF_SETTINGS_MY_NAVIGATION_PACKAGE"

.field public static final PREF_SETTINGS_OBD_DEVICE_ADDRESS:Ljava/lang/String; = "PREF_SETTINGS_OBD_DEVICE_ADDRESS"

.field public static final PREF_SETTINGS_OBD_DEVICE_NAME:Ljava/lang/String; = "PREF_SETTINGS_OBD_DEVICE_NAME"

.field public static final PREF_SETTINGS_OFFICE:Ljava/lang/String; = "PREF_SETTINGS_OFFICE"

.field public static final PREF_SETTINGS_PARKING_LOT_ORDER:Ljava/lang/String; = "PREF_SETTINGS_PARKING_LOT_ORDER"

.field public static final PREF_SETTINGS_READOUT_NOTIFICATION:Ljava/lang/String; = "PREF_SETTINGS_READOUT_NOTIFICATION"

.field public static final PREF_SETTINGS_READOUT_NOTIFICATION_ALARM:Ljava/lang/String; = "PREF_SETTINGS_READOUT_NOTIFICATION_ALARM"

.field public static final PREF_SETTINGS_READOUT_NOTIFICATION_BT_REQUEST:Ljava/lang/String; = "PREF_SETTINGS_READOUT_NOTIFICATION_BT_REQUEST"

.field public static final PREF_SETTINGS_READOUT_NOTIFICATION_INCOMING_CALL:Ljava/lang/String; = "PREF_SETTINGS_READOUT_NOTIFICATION_INCOMING_CALL"

.field public static final PREF_SETTINGS_READOUT_NOTIFICATION_LOCATION_SHARE:Ljava/lang/String; = "PREF_SETTINGS_READOUT_NOTIFICATION_LOCATION_SHARE"

.field public static final PREF_SETTINGS_READOUT_NOTIFICATION_RECEIVE_MESSAGE:Ljava/lang/String; = "PREF_SETTINGS_READOUT_NOTIFICATION_RECEIVE_MESSAGE"

.field public static final PREF_SETTINGS_READOUT_SPEED:Ljava/lang/String; = "PREF_SETTINGS_READOUT_SPEED"

.field public static final PREF_SETTINGS_SET_FAVORITE_LOCATION:Ljava/lang/String; = "PREF_SETTINGS_SET_FAVORITE_LOCATION"

.field public static final PREF_SETTINGS_SUGGEST_CONTACTS:Ljava/lang/String; = "PREF_SETTINGS_SUGGEST_CONTACTS"

.field public static final PREF_USER_BASIC_SETTING_RUN_ONCE:Ljava/lang/String; = "PREF_USER_BASIC_SETTING_RUN_ONCE"

.field public static final PREF_USER_BT_PREVIOUS_STATE:Ljava/lang/String; = "PREF_USER_BT_PREVIOUS_STATE"

.field public static final PREF_USER_CAR_APP_WARNING_AGREEMENT:Ljava/lang/String; = "PREF_USER_CAR_APP_WARNING_AGREEMENT"

.field public static final PREF_USER_GPS_PREVIOUS_STATE:Ljava/lang/String; = "PREF_USER_GPS_PREVIOUS_STATE"

.field public static final PREF_USER_GPS_PREVIOUS_STATE_IS_POWERSAVING:Ljava/lang/String; = "PREF_USER_GPS_PREVIOUS_STATE_IS_POWERSAVING"

.field public static final PREF_USER_REGULATION_AGREEMENT:Ljava/lang/String; = "PREF_USER_REGULATION_AGREEMENT"

.field public static final PREF_USER_SOUND_MODE_PREVIOUS_STATE:Ljava/lang/String; = "PREF_USER_SOUND_MODE_PREVIOUS_STATE"

.field public static final PREF_USER_TOURINGGUIDE_DONE:Ljava/lang/String; = "PREF_USER_TOURINGGUIDE_DONE"

.field public static final PREF_USER_WELCOME:Ljava/lang/String; = "PREF_USER_WELCOME"

.field public static final S_VOICE_SETTINGS_LANGUAGE:Ljava/lang/String; = "carmode_language"

.field private static mInstance:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mContext:Landroid/content/Context;

    .line 74
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    .locals 2
    .param p0, "c"    # Landroid/content/Context;

    .prologue
    .line 77
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mInstance:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    if-nez v0, :cond_0

    .line 78
    const-class v1, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    monitor-enter v1

    .line 79
    :try_start_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mInstance:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    .line 78
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mInstance:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    return-object v0

    .line 78
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public getValue(Ljava/lang/String;I)I
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "dftValue"    # I

    .prologue
    .line 132
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "com.sec.android.automotive.drivelink"

    .line 133
    const/4 v4, 0x0

    .line 132
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 135
    .local v1, "pref":Landroid/content/SharedPreferences;
    :try_start_0
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 137
    .end local p2    # "dftValue":I
    :goto_0
    return p2

    .line 136
    .restart local p2    # "dftValue":I
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "dftValue"    # Ljava/lang/String;

    .prologue
    .line 122
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "com.sec.android.automotive.drivelink"

    .line 123
    const/4 v4, 0x0

    .line 122
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 125
    .local v1, "pref":Landroid/content/SharedPreferences;
    :try_start_0
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    .line 127
    .end local p2    # "dftValue":Ljava/lang/String;
    :goto_0
    return-object p2

    .line 126
    .restart local p2    # "dftValue":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getValue(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    .local p2, "dftValue":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "com.sec.android.automotive.drivelink"

    .line 153
    const/4 v4, 0x0

    .line 152
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 155
    .local v1, "pref":Landroid/content/SharedPreferences;
    :try_start_0
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    .line 157
    .end local p2    # "dftValue":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :goto_0
    return-object p2

    .line 156
    .restart local p2    # "dftValue":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 157
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getValue(Ljava/lang/String;Z)Z
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "dftValue"    # Z

    .prologue
    .line 142
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "com.sec.android.automotive.drivelink"

    .line 143
    const/4 v4, 0x0

    .line 142
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 145
    .local v1, "pref":Landroid/content/SharedPreferences;
    :try_start_0
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 147
    .end local p2    # "dftValue":Z
    :goto_0
    return p2

    .line 146
    .restart local p2    # "dftValue":Z
    :catch_0
    move-exception v0

    .line 147
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public put(Ljava/lang/String;I)V
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 104
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "com.sec.android.automotive.drivelink"

    .line 105
    const/4 v4, 0x0

    .line 104
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 106
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 108
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 109
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 110
    return-void
.end method

.method public put(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 86
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "com.sec.android.automotive.drivelink"

    .line 87
    const/4 v4, 0x0

    .line 86
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 88
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 90
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 91
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 92
    return-void
.end method

.method public put(Ljava/lang/String;Ljava/util/Set;)V
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 113
    .local p2, "value":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "com.sec.android.automotive.drivelink"

    .line 114
    const/4 v4, 0x0

    .line 113
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 115
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 117
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 118
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 119
    return-void
.end method

.method public put(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 95
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "com.sec.android.automotive.drivelink"

    .line 96
    const/4 v4, 0x0

    .line 95
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 97
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 99
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 100
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 101
    return-void
.end method

.method public registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 4
    .param p1, "listener"    # Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .prologue
    .line 163
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "com.sec.android.automotive.drivelink"

    .line 164
    const/4 v3, 0x0

    .line 163
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 165
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 166
    return-void
.end method

.method public removeKey(Ljava/lang/String;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 193
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "com.sec.android.automotive.drivelink"

    .line 194
    const/4 v3, 0x0

    .line 193
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 195
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 196
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 198
    :cond_0
    return-void
.end method

.method public setDefaultValue(Ljava/lang/String;I)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "dftValue"    # I

    .prologue
    .line 177
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "com.sec.android.automotive.drivelink"

    .line 178
    const/4 v3, 0x0

    .line 177
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 179
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 180
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mInstance:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;I)V

    .line 182
    :cond_0
    return-void
.end method

.method public setDefaultValue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "dftValue"    # Ljava/lang/String;

    .prologue
    .line 169
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "com.sec.android.automotive.drivelink"

    .line 170
    const/4 v3, 0x0

    .line 169
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 171
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 172
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mInstance:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :cond_0
    return-void
.end method

.method public setDefaultValue(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "dftValue"    # Z

    .prologue
    .line 185
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "com.sec.android.automotive.drivelink"

    .line 186
    const/4 v3, 0x0

    .line 185
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 187
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 188
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->mInstance:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 190
    :cond_0
    return-void
.end method
