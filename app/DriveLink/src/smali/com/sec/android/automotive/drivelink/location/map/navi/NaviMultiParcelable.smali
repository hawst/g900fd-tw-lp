.class public Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;
.super Ljava/lang/Object;
.source "NaviMultiParcelable.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;",
            ">;"
        }
    .end annotation
.end field

.field public static final MULTI_NAVI_MODE_START_MAP:I = 0x0

.field public static final MULTI_NAVI_MODE_START_NAVIGATION:I = 0x1

.field public static mOriginLat:D

.field public static mOriginLng:D


# instance fields
.field public className:Ljava/lang/String;

.field public isStartMap:I

.field public mLat:D

.field public mLng:D

.field public mTts:Ljava/lang/String;

.field public packageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 20
    sput-wide v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mOriginLat:D

    .line 21
    sput-wide v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mOriginLng:D

    .line 77
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable$1;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable$1;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 87
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;DDLjava/lang/String;)V
    .locals 2
    .param p1, "ct"    # Landroid/content/Context;
    .param p2, "lat"    # D
    .param p4, "lng"    # D
    .param p6, "tts"    # Ljava/lang/String;

    .prologue
    const-wide/16 v0, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLat:D

    .line 12
    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLng:D

    .line 13
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mTts:Ljava/lang/String;

    .line 14
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->packageName:Ljava/lang/String;

    .line 15
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->className:Ljava/lang/String;

    .line 35
    iput-wide p2, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLat:D

    .line 36
    iput-wide p4, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLng:D

    .line 37
    iput-object p6, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mTts:Ljava/lang/String;

    .line 38
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->packageName:Ljava/lang/String;

    .line 39
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->className:Ljava/lang/String;

    .line 40
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->isStartMap:I

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "ct"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "className"    # Ljava/lang/String;

    .prologue
    const-wide/16 v1, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLat:D

    .line 12
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLng:D

    .line 13
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mTts:Ljava/lang/String;

    .line 14
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->packageName:Ljava/lang/String;

    .line 15
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->className:Ljava/lang/String;

    .line 25
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLat:D

    .line 26
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLng:D

    .line 27
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mTts:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->packageName:Ljava/lang/String;

    .line 29
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->className:Ljava/lang/String;

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->isStartMap:I

    .line 31
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const-wide/16 v0, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLat:D

    .line 12
    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLng:D

    .line 13
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mTts:Ljava/lang/String;

    .line 14
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->packageName:Ljava/lang/String;

    .line 15
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->className:Ljava/lang/String;

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLat:D

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLng:D

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mTts:Ljava/lang/String;

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->packageName:Ljava/lang/String;

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->className:Ljava/lang/String;

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->isStartMap:I

    .line 69
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public setOrigin(DD)V
    .locals 3
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 72
    const-string/jumbo v0, "DEBUG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "lat: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", lng: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    sput-wide p1, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mOriginLat:D

    .line 74
    sput-wide p3, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mOriginLng:D

    .line 75
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLat:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 52
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLng:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 53
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mTts:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->className:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 56
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->isStartMap:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    return-void
.end method
