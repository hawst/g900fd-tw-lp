.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$17;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getPhoneTypeItemOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 1823
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1827
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->hideInputMethod()V
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$61(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    .line 1828
    const/4 v9, 0x0

    .line 1830
    .local v9, "currentItem":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$15(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    .line 1831
    mul-int/lit8 v2, v2, 0x4

    .line 1830
    add-int/2addr v1, v2

    add-int/lit8 v9, v1, -0x1

    .line 1832
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberContactId:J
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$64(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)J

    move-result-wide v1

    .line 1833
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberUserDisplayName:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$65(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1834
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$10(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;

    move-result-object v6

    .line 1832
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1837
    .local v0, "dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v10

    .line 1838
    .local v10, "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    check-cast v10, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 1839
    .end local v10    # "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-virtual {v10}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 1840
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 1841
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v3

    .line 1839
    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getPhoneNumberList(Landroid/content/Context;J)Ljava/util/ArrayList;

    move-result-object v11

    .line 1843
    .local v11, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    invoke-static {v0, v11}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;Ljava/util/ArrayList;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v8

    .line 1846
    .local v8, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v7}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 1847
    .local v7, "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput-object v8, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 1848
    iput v9, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    .line 1851
    iget-object v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v1, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1853
    iget-object v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v1, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isJapaneseString(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1854
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1855
    const v2, 0x7f0a014a

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 1856
    iget-object v5, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v5, v5, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    aput-object v5, v3, v4

    .line 1855
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1854
    iput-object v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 1863
    :goto_0
    const-string/jumbo v1, "DM_SMS_COMPOSE"

    invoke-static {v1, v7}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 1877
    return-void

    .line 1858
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1859
    const v2, 0x7f0a014a

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 1860
    iget-object v5, v8, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    aput-object v5, v3, v4

    .line 1859
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1858
    iput-object v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    goto :goto_0
.end method
