.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessageAttachment;
.source "DLMessageAttachmentImpl.java"


# instance fields
.field private mAudioCount:I

.field private mCalendarCount:I

.field private mContactCount:I

.field private mImageCount:I

.field private mIsText:Z

.field private mText:Ljava/lang/String;

.field private mVideoCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessageAttachment;-><init>()V

    .line 6
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mAudioCount:I

    .line 7
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mVideoCount:I

    .line 8
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mImageCount:I

    .line 9
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mContactCount:I

    .line 10
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mCalendarCount:I

    .line 11
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mIsText:Z

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mText:Ljava/lang/String;

    .line 16
    return-void
.end method

.method public constructor <init>(IIIIIZLjava/lang/String;)V
    .locals 1
    .param p1, "audioCount"    # I
    .param p2, "videoCount"    # I
    .param p3, "imageCount"    # I
    .param p4, "contactCount"    # I
    .param p5, "calendarCount"    # I
    .param p6, "isText"    # Z
    .param p7, "text"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessageAttachment;-><init>()V

    .line 6
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mAudioCount:I

    .line 7
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mVideoCount:I

    .line 8
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mImageCount:I

    .line 9
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mContactCount:I

    .line 10
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mCalendarCount:I

    .line 11
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mIsText:Z

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mText:Ljava/lang/String;

    .line 22
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mAudioCount:I

    .line 23
    iput p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mVideoCount:I

    .line 24
    iput p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mImageCount:I

    .line 25
    iput p4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mContactCount:I

    .line 26
    iput p5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mCalendarCount:I

    .line 27
    iput-boolean p6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mIsText:Z

    .line 28
    iput-object p7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mText:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public IsText()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mIsText:Z

    return v0
.end method

.method public getAudioCount()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mAudioCount:I

    return v0
.end method

.method public getCalendarCount()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mCalendarCount:I

    return v0
.end method

.method public getContactCount()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mContactCount:I

    return v0
.end method

.method public getImageCount()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mImageCount:I

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mText:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoCount()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mVideoCount:I

    return v0
.end method

.method public setAudioCount(I)V
    .locals 0
    .param p1, "audioCount"    # I

    .prologue
    .line 68
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mAudioCount:I

    .line 69
    return-void
.end method

.method public setCalendarCount(I)V
    .locals 0
    .param p1, "calendarCount"    # I

    .prologue
    .line 84
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mCalendarCount:I

    .line 85
    return-void
.end method

.method public setContactCount(I)V
    .locals 0
    .param p1, "contactCount"    # I

    .prologue
    .line 80
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mContactCount:I

    .line 81
    return-void
.end method

.method public setImageCount(I)V
    .locals 0
    .param p1, "imageCount"    # I

    .prologue
    .line 76
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mImageCount:I

    .line 77
    return-void
.end method

.method public setIsText(Z)V
    .locals 0
    .param p1, "isText"    # Z

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mIsText:Z

    .line 89
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mText:Ljava/lang/String;

    .line 93
    return-void
.end method

.method public setVideoCount(I)V
    .locals 0
    .param p1, "videoCount"    # I

    .prologue
    .line 72
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->mVideoCount:I

    .line 73
    return-void
.end method
