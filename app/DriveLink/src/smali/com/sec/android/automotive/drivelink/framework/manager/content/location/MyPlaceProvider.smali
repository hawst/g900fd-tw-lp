.class public abstract Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;
.super Ljava/lang/Object;
.source "MyPlaceProvider.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static printPlaces(Landroid/database/Cursor;)V
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 190
    if-nez p0, :cond_1

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 198
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "============="

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {p0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v2

    if-lt v1, v2, :cond_3

    .line 212
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "============="

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    .line 203
    :cond_3
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getType(I)I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    .line 202
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 207
    :cond_4
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v0

    .line 208
    .local v0, "column":Ljava/lang/String;
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    goto :goto_2
.end method


# virtual methods
.method protected abstract getLatLngDegree(D)D
.end method

.method public getMyPlaces(Landroid/content/Context;)Ljava/util/List;
    .locals 28
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->getProviderUri()Ljava/lang/String;

    move-result-object v25

    .line 36
    .local v25, "providerUri":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->getProjection()[Ljava/lang/String;

    move-result-object v4

    .line 40
    .local v4, "projection":[Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 41
    invoke-static/range {v25 .. v25}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 40
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 43
    .local v10, "cursor":Landroid/database/Cursor;
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 44
    .local v23, "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;>;"
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "+++Begin print all data in content provider+++"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    invoke-static {v10}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->printPlaces(Landroid/database/Cursor;)V

    .line 46
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "---End print all data in content provider---"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    if-nez v10, :cond_1

    .line 181
    if-eqz v10, :cond_0

    .line 182
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 186
    :cond_0
    :goto_0
    return-object v23

    .line 53
    :cond_1
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_2

    .line 54
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    if-eqz v10, :cond_0

    .line 182
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 59
    :cond_2
    :try_start_1
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "=============="

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->getNameFieldMyPlaceType()Ljava/lang/String;

    move-result-object v27

    .line 61
    .local v27, "typeColumnName":Ljava/lang/String;
    move-object/from16 v0, v27

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    .line 68
    .local v26, "type":I
    const/4 v2, 0x1

    move/from16 v0, v26

    if-eq v0, v2, :cond_3

    .line 69
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "type != 1, not a map"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :goto_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    .line 181
    if-eqz v10, :cond_0

    .line 182
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 73
    :cond_3
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->getNameFieldMyPlaceId()Ljava/lang/String;

    move-result-object v13

    .line 74
    .local v13, "idColumnName":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->getNameFieldMyPlaceAddress()Ljava/lang/String;

    move-result-object v9

    .line 75
    .local v9, "addressColumnName":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->getNameFieldMyPlaceLatitude()Ljava/lang/String;

    move-result-object v16

    .line 76
    .local v16, "latitudeColumnName":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->getNameFieldMyPlaceLongitude()Ljava/lang/String;

    move-result-object v20

    .line 77
    .local v20, "longitudeColumnName":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->getNameFieldMyPlaceProfile()Ljava/lang/String;

    move-result-object v24

    .line 83
    .local v24, "profileColumnName":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 85
    move-object/from16 v0, v16

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 84
    invoke-interface {v10, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    .line 85
    const-wide/16 v5, 0x0

    .line 84
    cmpl-double v2, v2, v5

    if-nez v2, :cond_5

    .line 86
    :cond_4
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "latitude is not valid"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 178
    .end local v9    # "addressColumnName":Ljava/lang/String;
    .end local v13    # "idColumnName":Ljava/lang/String;
    .end local v16    # "latitudeColumnName":Ljava/lang/String;
    .end local v20    # "longitudeColumnName":Ljava/lang/String;
    .end local v24    # "profileColumnName":Ljava/lang/String;
    .end local v26    # "type":I
    .end local v27    # "typeColumnName":Ljava/lang/String;
    :catch_0
    move-exception v11

    .line 179
    .local v11, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 181
    if-eqz v10, :cond_0

    .line 182
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 90
    .end local v11    # "e":Ljava/lang/Exception;
    .restart local v9    # "addressColumnName":Ljava/lang/String;
    .restart local v13    # "idColumnName":Ljava/lang/String;
    .restart local v16    # "latitudeColumnName":Ljava/lang/String;
    .restart local v20    # "longitudeColumnName":Ljava/lang/String;
    .restart local v24    # "profileColumnName":Ljava/lang/String;
    .restart local v26    # "type":I
    .restart local v27    # "typeColumnName":Ljava/lang/String;
    :cond_5
    const/4 v12, -0x1

    .line 91
    .local v12, "id":I
    const/4 v8, 0x0

    .line 92
    .local v8, "address":Ljava/lang/String;
    const-wide v14, 0x40c3878000000000L    # 9999.0

    .line 93
    .local v14, "latitude":D
    const-wide v18, 0x40c3878000000000L    # 9999.0

    .line 94
    .local v18, "longitude":D
    const/16 v21, 0x0

    .line 96
    .local v21, "name":Ljava/lang/String;
    :try_start_4
    new-instance v17, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    invoke-direct/range {v17 .. v17}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;-><init>()V

    .line 97
    .local v17, "location":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;
    new-instance v22, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;

    invoke-direct/range {v22 .. v22}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;-><init>()V

    .line 99
    .local v22, "place":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;
    invoke-interface {v10, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_6

    .line 100
    invoke-interface {v10, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 104
    :cond_6
    invoke-interface {v10, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_7

    .line 106
    invoke-interface {v10, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 105
    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 110
    :cond_7
    move-object/from16 v0, v24

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_b

    .line 112
    move-object/from16 v0, v24

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 111
    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 135
    if-nez v12, :cond_8

    .line 136
    const-string/jumbo v2, "location_home"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->setPlaceName(Ljava/lang/String;)V

    .line 139
    :cond_8
    const/4 v2, 0x1

    if-ne v12, v2, :cond_9

    .line 140
    const-string/jumbo v2, "location_office"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->setPlaceName(Ljava/lang/String;)V

    .line 143
    :cond_9
    const/4 v2, 0x1

    if-le v12, v2, :cond_a

    .line 144
    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->setPlaceName(Ljava/lang/String;)V

    .line 147
    :cond_a
    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLocationName(Ljava/lang/String;)V

    .line 151
    :cond_b
    move-object/from16 v0, v16

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_c

    .line 153
    move-object/from16 v0, v16

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 152
    invoke-interface {v10, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->getLatLngDegree(D)D

    move-result-wide v14

    .line 159
    :cond_c
    move-object/from16 v0, v20

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_d

    .line 161
    move-object/from16 v0, v20

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 160
    invoke-interface {v10, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->getLatLngDegree(D)D

    move-result-wide v18

    .line 167
    :cond_d
    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLocationAddress(Ljava/lang/String;)V

    .line 168
    move-object/from16 v0, v17

    invoke-virtual {v0, v14, v15}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLatitude(D)V

    .line 169
    invoke-virtual/range {v17 .. v19}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLongitude(D)V

    .line 170
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->MY_PLACE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLocationType(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;)V

    .line 172
    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->setPlaceAddress(Ljava/lang/String;)V

    .line 173
    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->setLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 174
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 180
    .end local v8    # "address":Ljava/lang/String;
    .end local v9    # "addressColumnName":Ljava/lang/String;
    .end local v12    # "id":I
    .end local v13    # "idColumnName":Ljava/lang/String;
    .end local v14    # "latitude":D
    .end local v16    # "latitudeColumnName":Ljava/lang/String;
    .end local v17    # "location":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;
    .end local v18    # "longitude":D
    .end local v20    # "longitudeColumnName":Ljava/lang/String;
    .end local v21    # "name":Ljava/lang/String;
    .end local v22    # "place":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;
    .end local v24    # "profileColumnName":Ljava/lang/String;
    .end local v26    # "type":I
    .end local v27    # "typeColumnName":Ljava/lang/String;
    :catchall_0
    move-exception v2

    .line 181
    if-eqz v10, :cond_e

    .line 182
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 184
    :cond_e
    throw v2
.end method

.method protected abstract getNameFieldMyPlaceAddress()Ljava/lang/String;
.end method

.method protected abstract getNameFieldMyPlaceId()Ljava/lang/String;
.end method

.method protected abstract getNameFieldMyPlaceLatitude()Ljava/lang/String;
.end method

.method protected abstract getNameFieldMyPlaceLongitude()Ljava/lang/String;
.end method

.method protected abstract getNameFieldMyPlaceProfile()Ljava/lang/String;
.end method

.method protected abstract getNameFieldMyPlaceType()Ljava/lang/String;
.end method

.method protected abstract getProjection()[Ljava/lang/String;
.end method

.method protected abstract getProviderUri()Ljava/lang/String;
.end method
