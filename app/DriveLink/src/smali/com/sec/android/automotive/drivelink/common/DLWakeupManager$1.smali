.class Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$1;
.super Ljava/lang/Object;
.source "DLWakeupManager.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 4
    .param p1, "focusChange"    # I

    .prologue
    const/4 v3, 0x0

    .line 164
    # getter for: Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "mChipWakeupFocusListener : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    packed-switch p1, :pswitch_data_0

    .line 190
    :goto_0
    :pswitch_0
    return-void

    .line 169
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mAudioFocusLossHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->access$1(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 181
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getWakeupState()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "BG_WAKEUP_NONE"

    if-ne v0, v1, :cond_0

    .line 183
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->access$2(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;)Landroid/content/Context;

    move-result-object v1

    # invokes: Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->startChipWakeupBargeIn(Landroid/content/Context;)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->access$3(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;Landroid/content/Context;)V

    .line 187
    :cond_0
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mAudioFocusLossHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->access$1(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 166
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
