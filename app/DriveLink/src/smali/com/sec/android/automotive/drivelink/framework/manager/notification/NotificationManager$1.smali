.class Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager$1;
.super Landroid/os/Handler;
.source "NotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    .line 398
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 401
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 479
    :goto_0
    :pswitch_0
    return-void

    .line 403
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;->STATE_IDLE:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    .line 404
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;

    .line 403
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->NotifyInCallState(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;)V

    goto :goto_0

    .line 408
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;->STATE_RINGING:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    .line 409
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;

    .line 408
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->NotifyInCallState(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;)V

    goto :goto_0

    .line 413
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;->STATE_OFFHOOK:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    .line 414
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;

    .line 413
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->NotifyInCallState(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;)V

    goto :goto_0

    .line 418
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->NotifyOutCallState()V

    goto :goto_0

    .line 422
    :pswitch_5
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->NotifyMSGSMS(Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;)V

    goto :goto_0

    .line 426
    :pswitch_6
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->NotifyMSGMMS(Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;)V

    goto :goto_0

    .line 430
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->NotifyBattery(I)V

    goto :goto_0

    .line 434
    :pswitch_8
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->NotifyAlarm(Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;)V

    goto :goto_0

    .line 438
    :pswitch_9
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->NotifySchedule(I)V

    goto :goto_0

    .line 442
    :pswitch_a
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->NotifyLocation(Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;)V

    goto :goto_0

    .line 446
    :pswitch_b
    const-string/jumbo v0, "NotificationManager"

    const-string/jumbo v1, "ACTION_AUDIO_BECOMING_NOISY"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicRemotePlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v0

    .line 448
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;->onMusicRemotePlayerPause()V

    goto :goto_0

    .line 452
    :pswitch_c
    const-string/jumbo v0, "NotificationManager"

    const-string/jumbo v1, "ACTION_MEDIA_MOUNTED"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMusicPlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->handleMusicListRenew()V

    goto/16 :goto_0

    .line 457
    :pswitch_d
    const-string/jumbo v0, "NotificationManager"

    const-string/jumbo v1, "ACTION_MEDIA_UNMOUNTED"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 461
    :pswitch_e
    const-string/jumbo v0, "NotificationManager"

    const-string/jumbo v1, "ACTION_MEDIA_EJECT"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 465
    :pswitch_f
    const-string/jumbo v0, "NotificationManager"

    const-string/jumbo v1, "ACTION_MEDIA_BAD_REMOVAL"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMusicPlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->handleSDCardEject()V

    goto/16 :goto_0

    .line 470
    :pswitch_10
    const-string/jumbo v0, "NotificationManager"

    const-string/jumbo v1, "ACTION_PRIVATE_MODE_OFF"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMusicPlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->handleMediaEject()V

    goto/16 :goto_0

    .line 475
    :pswitch_11
    const-string/jumbo v0, "NotificationManager"

    const-string/jumbo v1, "ACTION_PRIVATE_MODE_ON"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMusicPlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->handleMusicListRenew()V

    goto/16 :goto_0

    .line 401
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_4
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_11
        :pswitch_10
    .end packed-switch
.end method
