.class public Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLContactPlaceOn;
.super Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
.source "DLContactPlaceOn.java"


# static fields
.field private static final serialVersionUID:J = -0x7ab18a8dc06a358dL


# instance fields
.field private mGroupId:Ljava/lang/String;

.field private mShareToken:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>()V

    .line 7
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLContactPlaceOn;->mGroupId:Ljava/lang/String;

    .line 8
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLContactPlaceOn;->mShareToken:Ljava/lang/String;

    .line 5
    return-void
.end method


# virtual methods
.method public getGroupId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLContactPlaceOn;->mGroupId:Ljava/lang/String;

    return-object v0
.end method

.method public getShareToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLContactPlaceOn;->mShareToken:Ljava/lang/String;

    return-object v0
.end method
