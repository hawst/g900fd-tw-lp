.class public Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
.super Ljava/lang/Object;
.source "UpdateData.java"


# instance fields
.field private mAppId:Ljava/lang/String;

.field private mDownloadURL:Ljava/lang/String;

.field private mIsNoProxy:Z

.field private mResultCode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->mIsNoProxy:Z

    .line 11
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "downloadURL"    # Ljava/lang/String;
    .param p2, "resultCode"    # Ljava/lang/String;
    .param p3, "appId"    # Ljava/lang/String;
    .param p4, "noProxy"    # Z

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->mIsNoProxy:Z

    .line 16
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->mDownloadURL:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->mResultCode:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->mAppId:Ljava/lang/String;

    .line 19
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->mIsNoProxy:Z

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->mIsNoProxy:Z

    .line 20
    return-void
.end method


# virtual methods
.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->mAppId:Ljava/lang/String;

    return-object v0
.end method

.method public getDownloadURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->mDownloadURL:Ljava/lang/String;

    return-object v0
.end method

.method public getIsNoProxy()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->mIsNoProxy:Z

    return v0
.end method

.method public getResultCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->mResultCode:Ljava/lang/String;

    return-object v0
.end method

.method public setAppId(Ljava/lang/String;)V
    .locals 0
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->mAppId:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public setDownloadURL(Ljava/lang/String;)V
    .locals 0
    .param p1, "downloadURL"    # Ljava/lang/String;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->mDownloadURL:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public setResultCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "resultCode"    # Ljava/lang/String;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->mResultCode:Ljava/lang/String;

    .line 28
    return-void
.end method
