.class public Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;
.source "DLIncidentInfoImp.java"


# static fields
.field private static final serialVersionUID:J = -0x2b0f1d4dbcf82b64L


# instance fields
.field private delayImpactDistance:D

.field private delayImpactFreeFlowMinutes:D

.field private delayImpactTypicalMinutes:D

.field private distance:D

.field private eventCode:Ljava/lang/Integer;

.field private fullDescription:Ljava/lang/String;

.field private latitude:D

.field private longitude:D

.field private severity:I

.field private type:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;-><init>()V

    .line 21
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->setDelayImpactDistance(D)V

    .line 22
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->setType(I)V

    .line 23
    return-void
.end method


# virtual methods
.method public getDelayImpactDistance()D
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->delayImpactDistance:D

    return-wide v0
.end method

.method public getDelayImpactFreeFlowMinutes()D
    .locals 2

    .prologue
    .line 67
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->delayImpactFreeFlowMinutes:D

    return-wide v0
.end method

.method public getDelayImpactTypicalMinutes()D
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->delayImpactTypicalMinutes:D

    return-wide v0
.end method

.method public getDistance()D
    .locals 2

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->distance:D

    return-wide v0
.end method

.method public getEventCode()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->eventCode:Ljava/lang/Integer;

    return-object v0
.end method

.method public getFullDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->fullDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->longitude:D

    return-wide v0
.end method

.method public getSeverity()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->severity:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->type:I

    return v0
.end method

.method public setDelayImpactDistance(D)V
    .locals 0
    .param p1, "delayImpactDistance"    # D

    .prologue
    .line 88
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->delayImpactDistance:D

    .line 89
    return-void
.end method

.method public setDelayImpactFreeFlowMinutes(D)V
    .locals 0
    .param p1, "delayImpactFreeFlowMinutes"    # D

    .prologue
    .line 108
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->delayImpactFreeFlowMinutes:D

    .line 109
    return-void
.end method

.method public setDelayImpactTypicalMinutes(D)V
    .locals 0
    .param p1, "delayImpactTypicalMinutes"    # D

    .prologue
    .line 112
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->delayImpactTypicalMinutes:D

    .line 113
    return-void
.end method

.method public setDistance(D)V
    .locals 0
    .param p1, "distance"    # D

    .prologue
    .line 84
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->distance:D

    .line 85
    return-void
.end method

.method public setEventCode(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "eventCode"    # Ljava/lang/Integer;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->eventCode:Ljava/lang/Integer;

    .line 101
    return-void
.end method

.method public setFullDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "fullDescription"    # Ljava/lang/String;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->fullDescription:Ljava/lang/String;

    .line 81
    return-void
.end method

.method public setLatitude(D)V
    .locals 0
    .param p1, "latitude"    # D

    .prologue
    .line 92
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->latitude:D

    .line 93
    return-void
.end method

.method public setLongitude(D)V
    .locals 0
    .param p1, "longitude"    # D

    .prologue
    .line 96
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->longitude:D

    .line 97
    return-void
.end method

.method public setSeverity(I)V
    .locals 0
    .param p1, "severity"    # I

    .prologue
    .line 104
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->severity:I

    .line 105
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 76
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLIncidentInfoImp;->type:I

    .line 77
    return-void
.end method
