.class Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$1;
.super Landroid/os/Handler;
.source "NotificationLocationRequestDialogView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;

    .line 196
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 199
    iget v1, p1, Landroid/os/Message;->what:I

    if-nez v1, :cond_0

    .line 200
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;-><init>()V

    .line 201
    .local v0, "msgInfo":Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mName:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->access$0(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->setName(Ljava/lang/String;)V

    .line 202
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNumber:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->access$1(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->setPhoneNumber(Ljava/lang/String;)V

    .line 203
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v1

    .line 204
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mUrl:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->access$2(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->onNotifyLocationRequest(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Ljava/lang/String;)V

    .line 212
    .end local v0    # "msgInfo":Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;
    :goto_0
    return-void

    .line 207
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v1

    .line 208
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    .line 207
    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->removeNotification(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    .line 209
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->disableTimer()V
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->access$3(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)V

    .line 210
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->access$4(Z)V

    .line 211
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->access$5(Z)V

    goto :goto_0
.end method
