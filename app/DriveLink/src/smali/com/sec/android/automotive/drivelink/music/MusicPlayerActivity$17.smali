.class Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$17;
.super Ljava/lang/Object;
.source "MusicPlayerActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->initSearchTextField()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    .line 2058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "arg0"    # Landroid/text/Editable;

    .prologue
    .line 2063
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 2069
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2074
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 2075
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$33(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2076
    .local v0, "keyword":Ljava/lang/String;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$33(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 2077
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchClearBtn:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$40(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2078
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchIconText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$41(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2084
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$42(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Ljava/lang/String;)V

    .line 2085
    return-void

    .line 2080
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchClearBtn:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$40(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2081
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchIconText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$41(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
