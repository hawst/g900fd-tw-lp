.class public Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForMessage;
.super Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;
.source "RecommendationManagerForMessage.java"


# static fields
.field private static final WEIGHT_MESSAGE_CONTACT_COUNT_ON_CALLLOG:I = 0x2

.field private static final WEIGHT_MESSAGE_CONTACT_COUNT_ON_DRIVELINK:I = 0x6

.field private static final WEIGHT_MESSAGE_CONTACT_IN_SCHEDULE:I = 0x8

.field private static final WEIGHT_MESSAGE_IS_FAVORITE:I = 0x4

.field private static final WEIGHT_MESSAGE_LOCATION_SHARE:I = 0xa

.field private static final WEIGHT_MESSAGE_MISSED_CALL:I = 0xb


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;-><init>()V

    return-void
.end method


# virtual methods
.method public getRecommendedList(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x6

    const/16 v7, 0xa

    .line 25
    const-string/jumbo v4, "M"

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForMessage;->setLogTag(Ljava/lang/String;)V

    .line 28
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 29
    .local v2, "targetTime":Ljava/util/Calendar;
    const/16 v4, -0x960

    invoke-virtual {v2, v7, v4}, Ljava/util/Calendar;->add(II)V

    .line 31
    const-wide/16 v4, -0x1

    const/16 v6, 0xb

    invoke-virtual {p0, p1, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForMessage;->checkMissedCallList(Landroid/content/Context;JI)V

    .line 34
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForMessage;->checkFavoriteContactist(Landroid/content/Context;I)V

    .line 39
    const/16 v4, 0x3e9

    const/16 v5, 0x41a0

    .line 40
    const/4 v6, 0x2

    .line 39
    invoke-virtual {p0, p1, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForMessage;->checkCallLogList(Landroid/content/Context;III)V

    .line 43
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 44
    .local v0, "fromTime":Ljava/util/Calendar;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 45
    .local v3, "toTime":Ljava/util/Calendar;
    const/16 v4, 0x18

    invoke-virtual {v3, v7, v4}, Ljava/util/Calendar;->set(II)V

    .line 46
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    .line 47
    const/16 v6, 0x8

    .line 46
    invoke-virtual {p0, p1, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForMessage;->checkScheduleList(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;I)V

    .line 50
    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->LOG_TYPE_SMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->ordinal()I

    move-result v1

    .line 51
    .local v1, "logType":I
    invoke-virtual {p0, p1, v1, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForMessage;->checkDLCallLogList(Landroid/content/Context;II)V

    .line 54
    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->LOG_TYPE_MMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->ordinal()I

    move-result v1

    .line 55
    invoke-virtual {p0, p1, v1, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForMessage;->checkDLCallLogList(Landroid/content/Context;II)V

    .line 60
    const/16 v4, 0xa

    :try_start_0
    invoke-virtual {p0, p1, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForMessage;->checkLocationSharingList(Landroid/content/Context;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForMessage;->getContactListFromHashMap(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v4

    return-object v4

    .line 61
    :catch_0
    move-exception v4

    goto :goto_0
.end method
