.class public Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;
.source "LocationRequestConfirmActivity.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I


# instance fields
.field private mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

.field private mBtnCancel:Landroid/widget/Button;

.field private mBtnShare:Landroid/widget/Button;

.field private final mNotifiyNewMicState:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;

.field private mPhoneNumber:Ljava/lang/String;

.field private mText:Landroid/widget/TextView;

.field private user:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->user:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 181
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mNotifiyNewMicState:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;

    .line 26
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;Lcom/nuance/sample/MicState;)V
    .locals 0

    .prologue
    .line 168
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->handleMicStateChanged(Lcom/nuance/sample/MicState;)V

    return-void
.end method

.method private gotoHome()V
    .locals 3

    .prologue
    .line 159
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 160
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "fromRequest"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 161
    const-string/jumbo v1, "dlcontact"

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->user:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 162
    const-string/jumbo v1, "phoneNumber"

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 163
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 164
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->startActivity(Landroid/content/Intent;)V

    .line 165
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->finish()V

    .line 166
    return-void
.end method

.method private handleMicStateChanged(Lcom/nuance/sample/MicState;)V
    .locals 5
    .param p1, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 169
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 179
    :goto_0
    return-void

    .line 171
    :pswitch_0
    new-array v0, v4, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mBtnShare:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mBtnCancel:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->setButtonQuoted(Z[Landroid/widget/Button;)V

    .line 172
    new-array v0, v4, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mBtnShare:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mBtnCancel:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->setButtonQuoted(Z[Landroid/widget/Button;)V

    goto :goto_0

    .line 176
    :pswitch_1
    new-array v0, v4, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mBtnShare:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mBtnCancel:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->setButtonQuoted(Z[Landroid/widget/Button;)V

    goto :goto_0

    .line 169
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected getLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    const v2, 0x7f0a0249

    .line 37
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v0, 0x7f030014

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->setContentView(I)V

    .line 41
    const v0, 0x7f090095

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mText:Landroid/widget/TextView;

    .line 43
    const v0, 0x7f090098

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mBtnCancel:Landroid/widget/Button;

    .line 44
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mBtnCancel:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    const v0, 0x7f0900a2

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mBtnShare:Landroid/widget/Button;

    .line 52
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mBtnShare:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    const v0, 0x7f090093

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 61
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const v1, 0x7f0a0210

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setTitleBar(I)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setSayText(I)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setSayListeningText(I)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 75
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mNotifiyNewMicState:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setNotifiyNewMicState(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;)V

    .line 76
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onDestroy()V

    .line 146
    :goto_0
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1, v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 147
    return-void

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mFlowID:Ljava/lang/String;

    .line 142
    .local v0, "bkp":Ljava/lang/String;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mFlowID:Ljava/lang/String;

    .line 143
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onDestroy()V

    .line 144
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mFlowID:Ljava/lang/String;

    goto :goto_0
.end method

.method public onFlowCommandCancel(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->finish()V

    .line 90
    return-void
.end method

.method public onFlowCommandExcute(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->sendRequest()V

    .line 85
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 10
    .param p1, "hasFocus"    # Z

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 95
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onWindowFocusChanged(Z)V

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->isFlowManagerIntent(Landroid/content/Intent;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 99
    const-string/jumbo v6, "EXTRA_FLOW_ID"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 101
    .local v1, "flowID":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v2

    .line 103
    .local v2, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v2, :cond_0

    .line 104
    iget-object v5, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-static {v5}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getDlContact(Lcom/vlingo/core/internal/contacts/ContactMatch;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->user:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 105
    iget-object v5, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPhoneNum:Ljava/lang/String;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mPhoneNumber:Ljava/lang/String;

    .line 115
    .end local v1    # "flowID":Ljava/lang/String;
    .end local v2    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_0
    :goto_0
    const-string/jumbo v3, ""

    .line 116
    .local v3, "msg":Ljava/lang/String;
    const-string/jumbo v0, ""

    .line 118
    .local v0, "displayName":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->user:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->user:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 119
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->user:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 124
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0214

    new-array v7, v8, [Ljava/lang/Object;

    .line 125
    aput-object v0, v7, v9

    .line 124
    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 127
    invoke-static {v3, v0, v8}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->getHighlightedText(Ljava/lang/String;Ljava/lang/String;I)Landroid/text/SpannableString;

    move-result-object v4

    .line 130
    .local v4, "msgSpannable":Landroid/text/SpannableString;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 131
    const v7, 0x7f0a0213

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v0, v8, v9

    .line 130
    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setVoiceLayoutOnClickListener(Ljava/lang/String;)V

    .line 132
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mText:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    return-void

    .line 109
    .end local v0    # "displayName":Ljava/lang/String;
    .end local v3    # "msg":Ljava/lang/String;
    .end local v4    # "msgSpannable":Landroid/text/SpannableString;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 110
    const-string/jumbo v6, "dlcontact"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 109
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->user:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 112
    const-string/jumbo v6, "phoneNumber"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 111
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mPhoneNumber:Ljava/lang/String;

    goto :goto_0

    .line 121
    .restart local v0    # "displayName":Ljava/lang/String;
    .restart local v3    # "msg":Ljava/lang/String;
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->mPhoneNumber:Ljava/lang/String;

    goto :goto_1
.end method

.method sendLocationShareRequestMessage()V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->gotoHome()V

    .line 151
    return-void
.end method

.method protected sendRequest()V
    .locals 0

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;->sendLocationShareRequestMessage()V

    .line 80
    return-void
.end method
