.class Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$6;
.super Ljava/lang/Object;
.source "VoiceLocationActionBarLayout.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->startScaleAnimation(FJLandroid/view/animation/Interpolator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$6;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 722
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 726
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$6;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isRestore:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$11(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 727
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$6;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$12(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;Z)V

    .line 728
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$6;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const/high16 v1, 0x3f800000    # 1.0f

    const-wide/16 v2, 0x64

    # getter for: Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$13()Landroid/view/animation/Interpolator;

    move-result-object v4

    # invokes: Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->startScaleAnimation(FJLandroid/view/animation/Interpolator;)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$14(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;FJLandroid/view/animation/Interpolator;)V

    .line 732
    :goto_0
    return-void

    .line 730
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$6;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$12(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;Z)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 737
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 742
    return-void
.end method
