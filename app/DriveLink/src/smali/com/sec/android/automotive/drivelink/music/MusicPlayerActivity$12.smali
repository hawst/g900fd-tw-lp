.class Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$12;
.super Ljava/lang/Object;
.source "MusicPlayerActivity.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->initView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    .line 808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 821
    if-eqz p3, :cond_0

    .line 823
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    invoke-static {v0, p2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$37(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;I)V

    .line 824
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicVolume(I)V

    .line 827
    const-string/jumbo v0, "JINSEIL"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "JINSEIL onProgressChanged volume "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 828
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setVolumeUI()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$27(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    .line 830
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    const/4 v1, 0x0

    .line 815
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundViewAutoDisappear(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$26(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 816
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 817
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 811
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundViewAutoDisappear(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$26(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 812
    return-void
.end method
