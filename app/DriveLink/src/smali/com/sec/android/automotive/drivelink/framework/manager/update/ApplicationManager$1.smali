.class Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$1;
.super Landroid/os/Handler;
.source "ApplicationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    .line 441
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 443
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onInstalledPackaged.packageInstalled"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->pkgname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 444
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    iget v2, v2, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->returncode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 443
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->onInstalledPackaged:Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageInstalled;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageInstalled;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->pkgname:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    iget v2, v2, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->returncode:I

    invoke-interface {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageInstalled;->packageInstalled(Ljava/lang/String;I)V

    .line 446
    return-void
.end method
