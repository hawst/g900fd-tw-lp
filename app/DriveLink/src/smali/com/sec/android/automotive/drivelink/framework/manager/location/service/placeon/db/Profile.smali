.class public Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;
.super Ljava/lang/Object;
.source "Profile.java"


# instance fields
.field private mBirthday:Ljava/lang/String;

.field private mCountryCode:Ljava/lang/String;

.field private mEmail:Ljava/lang/String;

.field private mExpiredTime:Ljava/lang/String;

.field private mGender:Ljava/lang/String;

.field private mGroupId:I

.field private mModel:Ljava/lang/String;

.field private mPhoneNumber:Ljava/lang/String;

.field private mPushId:Ljava/lang/String;

.field private mSspGuid:Ljava/lang/String;

.field private mUserId:I

.field private mUserImgUrl:Ljava/lang/String;

.field private mUserName:Ljava/lang/String;


# direct methods
.method public constructor <init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "userId"    # I
    .param p2, "groupId"    # I
    .param p3, "sspGuid"    # Ljava/lang/String;
    .param p4, "userName"    # Ljava/lang/String;
    .param p5, "phoneNumber"    # Ljava/lang/String;
    .param p6, "gender"    # Ljava/lang/String;
    .param p7, "birthday"    # Ljava/lang/String;
    .param p8, "email"    # Ljava/lang/String;
    .param p9, "countryCode"    # Ljava/lang/String;
    .param p10, "expiredTime"    # Ljava/lang/String;
    .param p11, "pushId"    # Ljava/lang/String;
    .param p12, "model"    # Ljava/lang/String;
    .param p13, "userImgUrl"    # Ljava/lang/String;

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mUserId:I

    .line 5
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mGroupId:I

    .line 6
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mSspGuid:Ljava/lang/String;

    .line 7
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mUserName:Ljava/lang/String;

    .line 8
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mPhoneNumber:Ljava/lang/String;

    .line 9
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mGender:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mBirthday:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mEmail:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mCountryCode:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mExpiredTime:Ljava/lang/String;

    .line 14
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mPushId:Ljava/lang/String;

    .line 15
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mModel:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mUserImgUrl:Ljava/lang/String;

    .line 22
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mUserId:I

    .line 23
    iput p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mGroupId:I

    .line 24
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mSspGuid:Ljava/lang/String;

    .line 25
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mUserName:Ljava/lang/String;

    .line 26
    iput-object p5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mPhoneNumber:Ljava/lang/String;

    .line 27
    iput-object p6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mGender:Ljava/lang/String;

    .line 28
    iput-object p7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mBirthday:Ljava/lang/String;

    .line 29
    iput-object p8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mEmail:Ljava/lang/String;

    .line 30
    iput-object p9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mCountryCode:Ljava/lang/String;

    .line 31
    iput-object p10, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mExpiredTime:Ljava/lang/String;

    .line 32
    iput-object p11, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mPushId:Ljava/lang/String;

    .line 33
    iput-object p12, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mModel:Ljava/lang/String;

    .line 34
    iput-object p13, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mUserImgUrl:Ljava/lang/String;

    .line 35
    return-void
.end method


# virtual methods
.method public getBirthday()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mBirthday:Ljava/lang/String;

    return-object v0
.end method

.method public getCountryCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mCountryCode:Ljava/lang/String;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mEmail:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiredTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mExpiredTime:Ljava/lang/String;

    return-object v0
.end method

.method public getGender()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mGender:Ljava/lang/String;

    return-object v0
.end method

.method public getGroupId()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mGroupId:I

    return v0
.end method

.method public getModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mModel:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getPushId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mPushId:Ljava/lang/String;

    return-object v0
.end method

.method public getSspGuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mSspGuid:Ljava/lang/String;

    return-object v0
.end method

.method public getUserId()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mUserId:I

    return v0
.end method

.method public getUserImgUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mUserImgUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mUserName:Ljava/lang/String;

    return-object v0
.end method

.method public setBirthday(Ljava/lang/String;)V
    .locals 0
    .param p1, "mBirthday"    # Ljava/lang/String;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mBirthday:Ljava/lang/String;

    .line 91
    return-void
.end method

.method public setCountryCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "mCountryCode"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mCountryCode:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0
    .param p1, "mEmail"    # Ljava/lang/String;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mEmail:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public setExpiredTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "mExpiredTime"    # Ljava/lang/String;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mExpiredTime:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public setGender(Ljava/lang/String;)V
    .locals 0
    .param p1, "mGender"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mGender:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public setGroupId(I)V
    .locals 0
    .param p1, "mGroupId"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mGroupId:I

    .line 51
    return-void
.end method

.method public setModel(Ljava/lang/String;)V
    .locals 0
    .param p1, "mModel"    # Ljava/lang/String;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mModel:Ljava/lang/String;

    .line 131
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPhoneNumber"    # Ljava/lang/String;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mPhoneNumber:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public setPushId(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPushId"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mPushId:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public setSspGuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "mSspGuid"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mSspGuid:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setUserId(I)V
    .locals 0
    .param p1, "mUserId"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mUserId:I

    .line 43
    return-void
.end method

.method public setUserImgUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "mUserImgUrl"    # Ljava/lang/String;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mUserImgUrl:Ljava/lang/String;

    .line 139
    return-void
.end method

.method public setUserName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mUserName"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Profile;->mUserName:Ljava/lang/String;

    .line 67
    return-void
.end method
