.class public interface abstract Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;
.super Ljava/lang/Object;
.source "DriveLinkServiceInterface.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnDriveLinkMusicRemotePlayerListener"
.end annotation


# virtual methods
.method public abstract onMusicRemotePlayerErrorStop()V
.end method

.method public abstract onMusicRemotePlayerFForward(Z)V
.end method

.method public abstract onMusicRemotePlayerNext()V
.end method

.method public abstract onMusicRemotePlayerPause()V
.end method

.method public abstract onMusicRemotePlayerPlay()V
.end method

.method public abstract onMusicRemotePlayerPlayPause()V
.end method

.method public abstract onMusicRemotePlayerPrevious()V
.end method

.method public abstract onMusicRemotePlayerRewind(Z)V
.end method

.method public abstract onMusicRemotePlayerShuffle(Z)V
.end method

.method public abstract onMusicRemotePlayerStop()V
.end method

.method public abstract onMusicRemotePositionUpdate(I)V
.end method
