.class public Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;
.super Ljava/lang/Object;
.source "NotificationCenter.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;
.implements Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener$OnHomeNotiListener;
.implements Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener$OnMessageSendingNotiListener;
.implements Lcom/sec/android/automotive/drivelink/notification/OnMessageSharingNotiListener;
.implements Lcom/sec/android/automotive/drivelink/notification/OnSendInvitationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;
    }
.end annotation


# static fields
.field private static final NOTIFY_PENDING_NOTIFICATION:I = 0x0

.field public static NOTI_PRIORITY_ALARM:I = 0x0

.field public static NOTI_PRIORITY_BT_REQ:I = 0x0

.field public static NOTI_PRIORITY_CALL_DIALING:I = 0x0

.field public static NOTI_PRIORITY_CALL_ENDCALL:I = 0x0

.field public static NOTI_PRIORITY_CALL_INCALL:I = 0x0

.field public static NOTI_PRIORITY_CALL_INCOMING:I = 0x0

.field public static NOTI_PRIORITY_CALL_PREPARE:I = 0x0

.field public static NOTI_PRIORITY_COMMUTE:I = 0x0

.field public static NOTI_PRIORITY_HOME:I = 0x0

.field private static NOTI_PRIORITY_LEVEL_01:I = 0x0

.field private static NOTI_PRIORITY_LEVEL_02:I = 0x0

.field private static NOTI_PRIORITY_LEVEL_03:I = 0x0

.field private static NOTI_PRIORITY_LEVEL_04:I = 0x0

.field private static NOTI_PRIORITY_LEVEL_05:I = 0x0

.field private static NOTI_PRIORITY_LEVEL_06:I = 0x0

.field private static NOTI_PRIORITY_LEVEL_07:I = 0x0

.field private static NOTI_PRIORITY_LEVEL_08:I = 0x0

.field private static NOTI_PRIORITY_LEVEL_09:I = 0x0

.field private static NOTI_PRIORITY_LEVEL_10:I = 0x0

.field public static NOTI_PRIORITY_LOC_GROUP_SHARE_REQ:I = 0x0

.field public static NOTI_PRIORITY_LOC_REQUEST:I = 0x0

.field public static NOTI_PRIORITY_LOC_REQUEST_WAITING:I = 0x0

.field public static NOTI_PRIORITY_LOC_SHARE_CHANGED:I = 0x0

.field public static NOTI_PRIORITY_LOC_SHARE_CHANGED_HOST:I = 0x0

.field public static NOTI_PRIORITY_LOC_SHARE_CONFIRM:I = 0x0

.field public static NOTI_PRIORITY_LOC_SHARE_ENDED:I = 0x0

.field public static NOTI_PRIORITY_LOC_SHARE_EXTEND:I = 0x0

.field public static NOTI_PRIORITY_LOC_SHARE_REQ:I = 0x0

.field public static NOTI_PRIORITY_LOC_SHARE_SENT:I = 0x0

.field public static NOTI_PRIORITY_LOW_BATTERY:I = 0x0

.field public static NOTI_PRIORITY_MAX:I = 0x0

.field public static NOTI_PRIORITY_MESSAGE:I = 0x0

.field public static NOTI_PRIORITY_OTHER:I = 0x0

.field public static NOTI_PRIORITY_SCHEDULE:I = 0x0

.field public static NOTI_PRIORITY_SMART_ALERT:I = 0x0

.field public static NOTI_PRIORITY_STATUS_DIALOG:I = 0x0

.field public static NOTI_PRIORITY_STATUS_STANDBY:I = 0x0

.field public static final NOTI_TYPE_ALARM:I = 0x2

.field public static final NOTI_TYPE_BT_REQ:I = 0x9

.field public static final NOTI_TYPE_CALL:I = 0x1

.field public static final NOTI_TYPE_COMMUTE:I = 0x4

.field public static final NOTI_TYPE_GENERAL_MESSAGE:I = 0x1a

.field public static final NOTI_TYPE_HOME:I = 0xe

.field public static final NOTI_TYPE_LOC_GROUP_REQUEST_IGNORED:I = 0x19

.field public static final NOTI_TYPE_LOC_GROUP_SHARE_REQ:I = 0x10

.field public static final NOTI_TYPE_LOC_GROUP_SHARE_STOP:I = 0x12

.field private static final NOTI_TYPE_LOC_HAS_NO_SAMSUNG_ACCOUNT:I = 0x1b

.field public static final NOTI_TYPE_LOC_REQUEST:I = 0x13

.field public static final NOTI_TYPE_LOC_REQUEST_FAILED:I = 0x1c

.field public static final NOTI_TYPE_LOC_REQUEST_WAITING:I = 0x14

.field public static final NOTI_TYPE_LOC_SHARE_CHANGED:I = 0x11

.field public static final NOTI_TYPE_LOC_SHARE_CHANGED_HOST:I = 0x15

.field public static final NOTI_TYPE_LOC_SHARE_CONFIRM:I = 0x8

.field public static final NOTI_TYPE_LOC_SHARE_ENDED:I = 0x18

.field public static final NOTI_TYPE_LOC_SHARE_EXTEND:I = 0x17

.field public static final NOTI_TYPE_LOC_SHARE_GROUP:I = 0xd

.field public static final NOTI_TYPE_LOC_SHARE_REQ:I = 0x7

.field public static final NOTI_TYPE_LOC_SHARE_SENT:I = 0x16

.field public static final NOTI_TYPE_LOW_BATTERY:I = 0xa

.field public static final NOTI_TYPE_MESSAGE:I = 0x6

.field public static final NOTI_TYPE_MESSAGE_SENDING:I = 0xb

.field public static final NOTI_TYPE_MESSAGE_SHARING:I = 0xc

.field public static final NOTI_TYPE_MUSIC:I = 0xf

.field public static final NOTI_TYPE_NONE:I = 0x0

.field public static final NOTI_TYPE_SCHEDULE:I = 0x5

.field public static final NOTI_TYPE_SMART_ALERT:I = 0x3

.field private static final TAG:Ljava/lang/String; = "[NotificationCenter]"

.field private static volatile _instance:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;


# instance fields
.field private hasPending:Z

.field private mContext:Landroid/content/Context;

.field private mMicState:Lcom/nuance/sample/MicState;

.field private mNotiChangeListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

.field private mNotiItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;",
            ">;"
        }
    .end annotation
.end field

.field private mNotiMapChangeListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

.field private mNotifyHandler:Landroid/os/Handler;

.field private mPendingReqCount:I

.field mVoiceUiUpdate:Lcom/nuance/drivelink/DLUiUpdater;

.field private multiMode:Z

.field private prepareNotiActivity:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    .line 95
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_01:I

    .line 96
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_02:I

    .line 97
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_03:I

    .line 98
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_04:I

    .line 99
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_05:I

    .line 100
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_06:I

    .line 101
    const/4 v0, 0x7

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_07:I

    .line 102
    const/16 v0, 0x8

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_08:I

    .line 103
    const/16 v0, 0x9

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_09:I

    .line 104
    const/16 v0, 0xa

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_10:I

    .line 106
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_01:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_CALL_INCOMING:I

    .line 107
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_01:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_HOME:I

    .line 108
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_02:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_CALL_PREPARE:I

    .line 109
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_02:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_CALL_DIALING:I

    .line 110
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_02:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_CALL_INCALL:I

    .line 111
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_02:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_CALL_ENDCALL:I

    .line 113
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_03:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_ALARM:I

    .line 114
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_03:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_SCHEDULE:I

    .line 116
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_04:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_STATUS_DIALOG:I

    .line 118
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_05:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_SMART_ALERT:I

    .line 119
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_05:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_COMMUTE:I

    .line 121
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_06:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_MESSAGE:I

    .line 122
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_06:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_SHARE_REQ:I

    .line 123
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_06:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_GROUP_SHARE_REQ:I

    .line 124
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_06:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_SHARE_CONFIRM:I

    .line 125
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_06:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_SHARE_CHANGED:I

    .line 126
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_06:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_REQUEST:I

    .line 127
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_06:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_REQUEST_WAITING:I

    .line 128
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_06:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_SHARE_CHANGED_HOST:I

    .line 129
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_06:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_SHARE_SENT:I

    .line 130
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_06:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_SHARE_EXTEND:I

    .line 131
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_06:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_SHARE_ENDED:I

    .line 133
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_07:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_BT_REQ:I

    .line 135
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_09:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOW_BATTERY:I

    .line 137
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_10:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_STATUS_STANDBY:I

    .line 138
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_10:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_OTHER:I

    .line 139
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LEVEL_10:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_MAX:I

    .line 1074
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    .line 184
    sget-object v0, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mMicState:Lcom/nuance/sample/MicState;

    .line 185
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasPending:Z

    .line 192
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 299
    iput v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mPendingReqCount:I

    .line 911
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->prepareNotiActivity:Z

    .line 1076
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotifyHandler:Landroid/os/Handler;

    .line 1092
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$2;-><init>(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mVoiceUiUpdate:Lcom/nuance/drivelink/DLUiUpdater;

    .line 260
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 261
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    .line 263
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 262
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    .line 264
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 265
    invoke-interface {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkNotificationListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;)V

    .line 267
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 268
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->registerNotification(Landroid/content/Context;ZZZZZ)V

    .line 270
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;->getInstance()Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;->registerListener(Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener$OnMessageSendingNotiListener;)V

    .line 271
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;->getInstance()Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;->registerListener(Lcom/sec/android/automotive/drivelink/notification/OnMessageSharingNotiListener;)V

    .line 272
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;->getInstance()Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;->registerListener(Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener$OnHomeNotiListener;)V

    .line 273
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mVoiceUiUpdate:Lcom/nuance/drivelink/DLUiUpdater;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 274
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;Z)Z
    .locals 1

    .prologue
    .line 779
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->notifyNewNotification(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;Lcom/nuance/sample/MicState;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mMicState:Lcom/nuance/sample/MicState;

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;)Lcom/nuance/sample/MicState;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mMicState:Lcom/nuance/sample/MicState;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;)Z
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasPending:Z

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;Z)V
    .locals 0

    .prologue
    .line 185
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasPending:Z

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 1076
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotifyHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private addNotification(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Z
    .locals 9
    .param p1, "notiItem"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .prologue
    .line 720
    const-string/jumbo v6, "[NotificationCenter]"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "addNotification ++:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    const/4 v0, 0x0

    .line 723
    .local v0, "bTopNotiChanged":Z
    if-nez p1, :cond_0

    move v1, v0

    .line 754
    .end local v0    # "bTopNotiChanged":Z
    .local v1, "bTopNotiChanged":I
    :goto_0
    return v1

    .line 727
    .end local v1    # "bTopNotiChanged":I
    .restart local v0    # "bTopNotiChanged":Z
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getTopNotificationItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v5

    .line 729
    .local v5, "oldTop":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    monitor-enter v7

    .line 730
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v2, v6, -0x1

    .line 731
    .local v2, "index":I
    :goto_1
    if-gez v2, :cond_3

    .line 737
    :cond_1
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    add-int/lit8 v8, v2, 0x1

    invoke-interface {v6, v8, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 729
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 740
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getTopNotificationItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v4

    .line 742
    .local v4, "newTop":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    invoke-direct {p0, v5, v4}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->checkChangeTopItems(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 743
    const/4 v0, 0x1

    .line 753
    :cond_2
    const-string/jumbo v6, "[NotificationCenter]"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "addNotification --:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    .line 754
    .restart local v1    # "bTopNotiChanged":I
    goto :goto_0

    .line 732
    .end local v1    # "bTopNotiChanged":I
    .end local v4    # "newTop":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    :cond_3
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .line 733
    .local v3, "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getPriority()I

    move-result v6

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getPriority()I

    move-result v8

    if-le v6, v8, :cond_1

    .line 731
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 729
    .end local v2    # "index":I
    .end local v3    # "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6
.end method

.method private checkChangeTopItems(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Z
    .locals 2
    .param p1, "oldTop"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p2, "newTop"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .prologue
    .line 529
    const/4 v0, 0x0

    .line 530
    .local v0, "bChanged":Z
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 531
    const/4 v0, 0x0

    .line 540
    :cond_0
    :goto_0
    return v0

    .line 532
    :cond_1
    if-nez p1, :cond_2

    if-eqz p2, :cond_2

    .line 533
    const/4 v0, 0x1

    .line 534
    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    if-nez p2, :cond_3

    .line 535
    const/4 v0, 0x0

    .line 536
    goto :goto_0

    :cond_3
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 537
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private createNotificationItem(IILjava/lang/Object;)Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .locals 3
    .param p1, "type"    # I
    .param p2, "priority"    # I
    .param p3, "obj"    # Ljava/lang/Object;

    .prologue
    .line 566
    const/4 v0, 0x0

    .line 567
    .local v0, "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    move-object v1, p3

    .line 569
    .local v1, "notiInfo":Ljava/lang/Object;
    const/4 v2, 0x6

    if-ne p1, v2, :cond_1

    .line 570
    const-class v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    invoke-virtual {v2, p3}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 571
    sget p2, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_MESSAGE:I

    .line 573
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;

    .line 574
    .end local v0    # "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 573
    invoke-direct {v0, p1, p2, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;-><init>(IILjava/lang/Object;Landroid/content/Context;)V

    .line 580
    .restart local v0    # "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    :cond_0
    :goto_0
    return-object v0

    .line 577
    :cond_1
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .end local v0    # "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;-><init>(IILjava/lang/Object;Landroid/content/Context;)V

    .restart local v0    # "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    goto :goto_0
.end method

.method private createNotificationView(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    .locals 4
    .param p1, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .prologue
    const/4 v3, 0x0

    .line 592
    if-nez p1, :cond_0

    .line 593
    const/4 v0, 0x0

    .line 710
    :goto_0
    :pswitch_0
    return-object v0

    .line 596
    :cond_0
    const/4 v0, 0x0

    .line 597
    .local v0, "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_1
    goto :goto_0

    .line 599
    :pswitch_2
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 600
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto :goto_0

    .line 602
    :pswitch_3
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 603
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 602
    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 604
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto :goto_0

    .line 606
    :pswitch_4
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 607
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 606
    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 608
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto :goto_0

    .line 610
    :pswitch_5
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 611
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 610
    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 612
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto :goto_0

    .line 614
    :pswitch_6
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 615
    const-string/jumbo v2, "PREF_LOCATION_SHARE_REQUEST_WAITING"

    .line 614
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v1

    .line 616
    if-nez v1, :cond_1

    .line 617
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 618
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 617
    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareDialogView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 619
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto :goto_0

    .line 621
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 622
    const-string/jumbo v2, "PREF_LOCATION_SHARE_REQUEST_WAITING"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 624
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->removeNotification(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    goto :goto_0

    .line 630
    :pswitch_7
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreInvitationDialogView;

    .line 631
    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 630
    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreInvitationDialogView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 632
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto :goto_0

    .line 634
    :pswitch_8
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 635
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 634
    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 636
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto :goto_0

    .line 638
    :pswitch_9
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 639
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 638
    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 640
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto :goto_0

    .line 642
    :pswitch_a
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 643
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 642
    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 644
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto/16 :goto_0

    .line 646
    :pswitch_b
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 647
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 646
    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 648
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto/16 :goto_0

    .line 650
    :pswitch_c
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 651
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 650
    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 652
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto/16 :goto_0

    .line 654
    :pswitch_d
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 655
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 654
    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 656
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto/16 :goto_0

    .line 658
    :pswitch_e
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 659
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 658
    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSharingDialogView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 660
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto/16 :goto_0

    .line 662
    :pswitch_f
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 663
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 662
    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 664
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto/16 :goto_0

    .line 666
    :pswitch_10
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 667
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto/16 :goto_0

    .line 669
    :pswitch_11
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 670
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 669
    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 671
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto/16 :goto_0

    .line 673
    :pswitch_12
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 675
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto/16 :goto_0

    .line 679
    :pswitch_13
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 680
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 679
    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 681
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto/16 :goto_0

    .line 683
    :pswitch_14
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 684
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto/16 :goto_0

    .line 686
    :pswitch_15
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationExpired;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 687
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto/16 :goto_0

    .line 689
    :pswitch_16
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 690
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 689
    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 691
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto/16 :goto_0

    .line 693
    :pswitch_17
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGeneralMessage;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 694
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto/16 :goto_0

    .line 696
    :pswitch_18
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 697
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 696
    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 698
    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto/16 :goto_0

    .line 700
    :pswitch_19
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView;

    .end local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 701
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 700
    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .restart local v0    # "layout":Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    goto/16 :goto_0

    .line 597
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_12
        :pswitch_0
        :pswitch_11
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_7
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch
.end method

.method public static destroyInstance()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 243
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "Notifcation Center Destroyed."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    if-eqz v0, :cond_0

    .line 245
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 246
    invoke-interface {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkNotificationListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;)V

    .line 247
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 249
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 248
    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->unregisterNotification(Landroid/content/Context;)V

    .line 250
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;->getInstance()Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;->unregisterListener()V

    .line 251
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;->getInstance()Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;->unregisterListener()V

    .line 252
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;->getInstance()Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;->unregisterListener()V

    .line 253
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    .line 254
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mVoiceUiUpdate:Lcom/nuance/drivelink/DLUiUpdater;

    .line 253
    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 256
    :cond_0
    sput-object v2, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    .line 257
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 214
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    if-nez v0, :cond_1

    if-eqz p0, :cond_1

    .line 215
    const-class v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    monitor-enter v1

    .line 216
    :try_start_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    if-nez v0, :cond_0

    if-eqz p0, :cond_0

    .line 217
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    .line 218
    const-string/jumbo v0, "[NotificationCenter]"

    .line 219
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Notifcation Center Created.:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 218
    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    if-nez v0, :cond_2

    .line 225
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "instance of NotificationCenter is null!!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    :cond_2
    const-class v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 229
    const-class v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 230
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->destroyInstance()V

    .line 231
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    .line 232
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    iput-object p0, v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 233
    const-string/jumbo v0, "[NotificationCenter]"

    .line 234
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Notifcation Center Context Reassigned.:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 235
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " => "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 236
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 234
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 233
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    :cond_3
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    return-object v0

    .line 215
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static hasInstance()Z
    .locals 1

    .prologue
    .line 210
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hasSameNotification(I)Z
    .locals 5
    .param p1, "notiType"    # I

    .prologue
    .line 343
    const/4 v0, 0x0

    .line 344
    .local v0, "hasSameType":Z
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    monitor-enter v4

    .line 345
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_0

    .line 344
    :goto_1
    monitor-exit v4

    .line 353
    return v0

    .line 346
    :cond_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .line 347
    .local v2, "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v3

    if-ne v3, p1, :cond_1

    .line 348
    const/4 v0, 0x1

    .line 349
    goto :goto_1

    .line 345
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 344
    .end local v2    # "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private isAppForeground()Z
    .locals 17

    .prologue
    .line 979
    const/4 v6, 0x0

    .line 980
    .local v6, "isForeground":Z
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    .line 981
    const-string/jumbo v14, "activity"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 980
    check-cast v1, Landroid/app/ActivityManager;

    .line 983
    .local v1, "am":Landroid/app/ActivityManager;
    const/4 v13, 0x5

    invoke-virtual {v1, v13}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v12

    .line 984
    .local v12, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v12, :cond_0

    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_0

    .line 985
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 986
    .local v9, "pkgNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 987
    .local v4, "count":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v13

    iget-object v2, v13, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 988
    .local v2, "appPkgName":Ljava/lang/String;
    const-string/jumbo v3, "com.android.incallui"

    .line 989
    .local v3, "callPkgName":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/map/MapFactory;->newNavigationMap()Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    move-result-object v8

    .line 990
    .local v8, "navigation":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->getNaviPackageName()Ljava/lang/String;

    move-result-object v7

    .line 1000
    .local v7, "naviPkgName":Ljava/lang/String;
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_1

    .line 1014
    :goto_1
    if-nez v6, :cond_0

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lez v13, :cond_0

    .line 1016
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->isMultiMode()Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isMultiWindowVisible()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 1032
    const-string/jumbo v13, "[NotificationCenter]"

    .line 1033
    const-string/jumbo v14, "carmode is in multiwindow mode and visible. app is foreground !!"

    .line 1032
    invoke-static {v13, v14}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1034
    const/4 v6, 0x1

    .line 1071
    .end local v2    # "appPkgName":Ljava/lang/String;
    .end local v3    # "callPkgName":Ljava/lang/String;
    .end local v4    # "count":I
    .end local v7    # "naviPkgName":Ljava/lang/String;
    .end local v8    # "navigation":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    .end local v9    # "pkgNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    :goto_2
    return v6

    .line 1000
    .restart local v2    # "appPkgName":Ljava/lang/String;
    .restart local v3    # "callPkgName":Ljava/lang/String;
    .restart local v4    # "count":I
    .restart local v7    # "naviPkgName":Ljava/lang/String;
    .restart local v8    # "navigation":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    .restart local v9    # "pkgNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1001
    .local v11, "task":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v14, v11, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v14}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    .line 1002
    .local v10, "pkgname":Ljava/lang/String;
    const-string/jumbo v14, "[NotificationCenter]"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string/jumbo v16, "["

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, "]PackageName:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1003
    if-nez v4, :cond_2

    .line 1004
    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1005
    const-string/jumbo v13, "[NotificationCenter]"

    const-string/jumbo v14, "1st app is carmode. app is foreground !!"

    invoke-static {v13, v14}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1006
    const/4 v6, 0x1

    .line 1007
    goto :goto_1

    .line 1010
    :cond_2
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1011
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1036
    .end local v10    # "pkgname":Ljava/lang/String;
    .end local v11    # "task":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_3
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v13

    const/4 v14, 0x2

    if-lt v13, v14, :cond_4

    .line 1037
    const/4 v13, 0x0

    invoke-virtual {v9, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-virtual {v13, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 1038
    const/4 v13, 0x1

    invoke-virtual {v9, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-virtual {v13, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 1041
    const-string/jumbo v13, "[NotificationCenter]"

    .line 1042
    const-string/jumbo v14, "1st:call, 2nd carmode. call in carmod. app is foreground !!"

    .line 1041
    invoke-static {v13, v14}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1043
    const/4 v6, 0x1

    .line 1044
    goto :goto_2

    :cond_4
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isMultiWindowRunning()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 1045
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v13

    const/4 v14, 0x3

    if-lt v13, v14, :cond_5

    .line 1046
    const/4 v13, 0x0

    invoke-virtual {v9, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-virtual {v13, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 1047
    const/4 v13, 0x1

    invoke-virtual {v9, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-virtual {v13, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 1048
    const/4 v13, 0x2

    invoke-virtual {v9, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-virtual {v13, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 1050
    const-string/jumbo v13, "[NotificationCenter]"

    .line 1051
    const-string/jumbo v14, "call in multi-carmode (checked by pkg order). app is foreground !!"

    .line 1050
    invoke-static {v13, v14}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1052
    const/4 v6, 0x1

    .line 1053
    goto/16 :goto_2

    :cond_5
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isMultiWindowRunning()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1054
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1059
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v5

    .line 1060
    .local v5, "index":I
    if-eqz v5, :cond_6

    const/4 v13, 0x1

    if-ne v5, v13, :cond_0

    .line 1061
    :cond_6
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v13

    add-int/lit8 v14, v5, 0x1

    if-le v13, v14, :cond_0

    .line 1062
    add-int/lit8 v13, v5, 0x1

    invoke-virtual {v9, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-virtual {v13, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1063
    const-string/jumbo v13, "[NotificationCenter]"

    .line 1064
    const-string/jumbo v14, "call in multi-carmode (checked by call & carmode order). app is foreground !!"

    .line 1063
    invoke-static {v13, v14}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1065
    const/4 v6, 0x1

    goto/16 :goto_2
.end method

.method private makeNotification(IILjava/lang/Object;)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "priority"    # I
    .param p3, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    .line 544
    const-string/jumbo v1, "[NotificationCenter]"

    const-string/jumbo v2, "makeNotification"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->createNotificationItem(IILjava/lang/Object;)Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    .line 546
    .local v0, "noti":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    if-nez v0, :cond_1

    .line 561
    :cond_0
    :goto_0
    return-void

    .line 550
    :cond_1
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->addNotification(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 551
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->notifyNewNotification(Z)Z

    goto :goto_0

    .line 554
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasNotification()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiChangeListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    if-nez v1, :cond_0

    .line 555
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiMapChangeListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    if-nez v1, :cond_0

    .line 556
    const-string/jumbo v1, "[NotificationCenter]"

    .line 557
    const-string/jumbo v2, "[WARNING!] Notifications are not showed up. notify now!"

    .line 556
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->notifyNewNotification(Z)Z

    goto :goto_0
.end method

.method private notifyNewNotification(Z)Z
    .locals 8
    .param p1, "animation"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 780
    const/4 v0, 0x0

    .line 782
    .local v0, "bNotify":Z
    const-string/jumbo v4, "[NotificationCenter]"

    const-string/jumbo v5, "notifyNewNotification!"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 783
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasNotification()Z

    move-result v4

    if-nez v4, :cond_0

    .line 784
    const-string/jumbo v4, "[NotificationCenter]"

    const-string/jumbo v5, "There is no notification."

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    .line 908
    .end local v0    # "bNotify":Z
    .local v1, "bNotify":I
    :goto_0
    return v1

    .line 788
    .end local v1    # "bNotify":I
    .restart local v0    # "bNotify":Z
    :cond_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotifyHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 789
    const-string/jumbo v4, "[NotificationCenter]"

    const-string/jumbo v5, "remove NOTIFY_PENDING_NOTIFICATION"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotifyHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 794
    :cond_1
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiMapChangeListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    if-eqz v4, :cond_2

    .line 795
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiMapChangeListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    invoke-interface {v4, v7}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;->onNewNotification(Z)V

    .line 800
    :cond_2
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 801
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v4

    .line 802
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mVoiceUiUpdate:Lcom/nuance/drivelink/DLUiUpdater;

    .line 801
    invoke-virtual {v4, v5}, Lcom/nuance/drivelink/DLAppUiUpdater;->isAddedVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)Z

    move-result v4

    .line 802
    if-nez v4, :cond_3

    .line 803
    const-string/jumbo v4, "[NotificationCenter]"

    const-string/jumbo v5, "Voice Ui Updater removed. add again!."

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 804
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasPending:Z

    .line 805
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mVoiceUiUpdate:Lcom/nuance/drivelink/DLUiUpdater;

    invoke-virtual {v4, v5}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 827
    :cond_3
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiChangeListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    if-eqz v4, :cond_4

    .line 828
    const-string/jumbo v4, "[NotificationCenter]"

    const-string/jumbo v5, "Send onNewNotification"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 829
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiChangeListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    invoke-interface {v4, v7}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;->onNewNotification(Z)V

    .line 830
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 908
    .restart local v1    # "bNotify":I
    goto :goto_0

    .line 836
    .end local v1    # "bNotify":I
    :cond_4
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mMicState:Lcom/nuance/sample/MicState;

    sget-object v5, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    if-eq v4, v5, :cond_5

    .line 841
    const-string/jumbo v4, "[NotificationCenter]"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Mic State:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 842
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mMicState:Lcom/nuance/sample/MicState;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 843
    const-string/jumbo v6, ", isInDmFlowChanging:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 844
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->isInDmFlowChanging()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 845
    const-string/jumbo v6, ", isIdle:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 846
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v6

    invoke-interface {v6}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->isIdle()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 847
    const-string/jumbo v6, ", isAboutToStartUserFlowWithMic:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 848
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v6

    .line 849
    invoke-interface {v6}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->isAboutToStartUserFlowWithMic()Z

    move-result v6

    .line 848
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 841
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 850
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getTopNotificationItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    .line 851
    .local v3, "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    if-eqz v3, :cond_6

    .line 852
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getPriority()I

    move-result v4

    sget v5, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_STATUS_DIALOG:I

    if-le v4, v5, :cond_6

    .line 853
    const-string/jumbo v4, "[NotificationCenter]"

    .line 854
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Pending Notification, Type:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 853
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 855
    iput-boolean v7, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasPending:Z

    .line 856
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    move v1, v0

    .line 857
    .restart local v1    # "bNotify":I
    goto/16 :goto_0

    .line 859
    .end local v1    # "bNotify":I
    .end local v3    # "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasPendingReq()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 860
    const-string/jumbo v4, "JINSEIL"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "JINSEIL "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasPendingReq()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 861
    const-string/jumbo v4, "[NotificationCenter]"

    const-string/jumbo v5, "Has Pending Request. pending it."

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    .line 862
    .restart local v1    # "bNotify":I
    goto/16 :goto_0

    .line 868
    .end local v1    # "bNotify":I
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getTopNotificationItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    .line 869
    .restart local v3    # "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    if-eqz v3, :cond_7

    .line 871
    const-string/jumbo v4, "JINSEIL"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "JINSEIL "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 873
    :cond_7
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v4

    const/16 v5, 0xe

    if-eq v4, v5, :cond_8

    .line 874
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v4

    const/16 v5, 0xf

    if-ne v4, v5, :cond_9

    .line 875
    :cond_8
    const-string/jumbo v4, "JINSEIL"

    const-string/jumbo v5, "JINSEIL remove noti"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 876
    const-string/jumbo v4, "[NotificationCenter]"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Remvoe noti:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 877
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->removeNotification(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    .line 878
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasNotification()Z

    move-result v4

    if-nez v4, :cond_9

    .line 879
    const-string/jumbo v4, "[NotificationCenter]"

    const-string/jumbo v5, "Notifification is empty."

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    .line 880
    .restart local v1    # "bNotify":I
    goto/16 :goto_0

    .line 884
    .end local v1    # "bNotify":I
    :cond_9
    sget-object v4, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNoti:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    if-eqz v4, :cond_a

    .line 886
    sget-object v4, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNoti:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->finish()V

    .line 892
    :cond_a
    const-string/jumbo v4, "JINSEIL"

    const-string/jumbo v5, "JINSEIL startInternalActivity!"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 893
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    const-class v5, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 894
    .local v2, "intent":Landroid/content/Intent;
    const-class v4, Landroid/app/Activity;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 896
    const-string/jumbo v4, "[NotificationCenter]"

    .line 897
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "[Warning]adding New Task Flag!!!:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 898
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 897
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 896
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 899
    const/high16 v4, 0x10000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 901
    :cond_b
    const/high16 v4, 0x24000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 903
    const-string/jumbo v4, "[NotificationCenter]"

    const-string/jumbo v5, "start NotificationActivity"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 904
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    invoke-static {v4, v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 905
    const/4 v0, 0x1

    goto/16 :goto_1
.end method

.method private removeNotification(I)V
    .locals 5
    .param p1, "notiType"    # I

    .prologue
    .line 758
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 759
    const-string/jumbo v2, "[NotificationCenter]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "removeNotification ++:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    monitor-enter v3

    .line 761
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_2

    .line 760
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 772
    const-string/jumbo v2, "[NotificationCenter]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "removeNotification --:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 774
    .end local v0    # "index":I
    :cond_1
    return-void

    .line 762
    .restart local v0    # "index":I
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .line 763
    .local v1, "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v2

    if-ne v2, p1, :cond_3

    .line 764
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 765
    add-int/lit8 v0, v0, -0x1

    .line 766
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 761
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 760
    .end local v1    # "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private setOnNotificationChangeListener(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    .prologue
    .line 508
    const-string/jumbo v0, "[NotificationCenter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setOnNotificationChangeListener: Old:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 509
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiChangeListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", New:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 508
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiChangeListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    .line 511
    return-void
.end method


# virtual methods
.method public OnHomeState(I)V
    .locals 4
    .param p1, "isSend"    # I

    .prologue
    const/16 v3, 0xe

    .line 1542
    const-string/jumbo v0, "[NotificationCenter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "OnHomeState:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1543
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasSameNotification(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1544
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_HOME:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v3, v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1547
    :cond_0
    return-void
.end method

.method public OnMessageSendingState(Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;)V
    .locals 6
    .param p1, "sendInfo"    # Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;

    .prologue
    const/16 v5, 0xb

    const/4 v4, 0x1

    .line 1285
    if-nez p1, :cond_0

    .line 1286
    const-string/jumbo v1, "[NotificationCenter]"

    const-string/jumbo v2, "OnMessageSendingState: sendInfo is null!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1298
    :goto_0
    return-void

    .line 1289
    :cond_0
    const-string/jumbo v1, "[NotificationCenter]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "OnMessageSendingState:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;->getSendingType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1290
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;->getSendingType()I

    move-result v1

    if-ne v1, v4, :cond_1

    .line 1292
    sget v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_MESSAGE:I

    .line 1291
    invoke-direct {p0, v5, v1, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->createNotificationItem(IILjava/lang/Object;)Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    .line 1293
    .local v0, "noti":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    invoke-virtual {p0, v0, v4}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->addNotificationTop(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)Z

    goto :goto_0

    .line 1295
    .end local v0    # "noti":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    :cond_1
    sget v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_MESSAGE:I

    invoke-direct {p0, v5, v1, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    goto :goto_0
.end method

.method public addNotificationFront(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)Z
    .locals 8
    .param p1, "newItem"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p2, "backItem"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "animation"    # Z

    .prologue
    .line 423
    const/4 v0, 0x0

    .line 425
    .local v0, "bAdded":Z
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move v1, v0

    .line 449
    .end local v0    # "bAdded":Z
    .local v1, "bAdded":I
    :goto_0
    return v1

    .line 429
    .end local v1    # "bAdded":I
    .restart local v0    # "bAdded":Z
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getTopNotificationItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v5

    .line 431
    .local v5, "oldTop":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    monitor-enter v7

    .line 432
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_1
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lt v2, v6, :cond_3

    .line 431
    :goto_2
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 443
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getTopNotificationItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v4

    .line 445
    .local v4, "newTop":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    invoke-direct {p0, v5, v4}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->checkChangeTopItems(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 446
    invoke-direct {p0, p3}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->notifyNewNotification(Z)Z

    :cond_2
    move v1, v0

    .line 449
    .restart local v1    # "bAdded":I
    goto :goto_0

    .line 433
    .end local v1    # "bAdded":I
    .end local v4    # "newTop":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    :cond_3
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .line 435
    .local v3, "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    invoke-virtual {v3, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 436
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v6, v2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 437
    const/4 v0, 0x1

    .line 438
    goto :goto_2

    .line 432
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 431
    .end local v3    # "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6
.end method

.method public addNotificationTop(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)Z
    .locals 7
    .param p1, "newItem"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p2, "animation"    # Z

    .prologue
    .line 391
    const/4 v0, 0x0

    .line 393
    .local v0, "bAdded":Z
    if-nez p1, :cond_0

    move v1, v0

    .line 417
    .end local v0    # "bAdded":Z
    .local v1, "bAdded":I
    :goto_0
    return v1

    .line 397
    .end local v1    # "bAdded":I
    .restart local v0    # "bAdded":Z
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getTopNotificationItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    .line 399
    .local v3, "oldTop":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    monitor-enter v5

    .line 401
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 402
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 403
    const/4 v0, 0x1

    .line 399
    :goto_1
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 411
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getTopNotificationItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    .line 413
    .local v2, "newTop":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    invoke-direct {p0, v3, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->checkChangeTopItems(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 414
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->notifyNewNotification(Z)Z

    :cond_1
    move v1, v0

    .line 417
    .restart local v1    # "bAdded":I
    goto :goto_0

    .line 405
    .end local v1    # "bAdded":I
    .end local v2    # "newTop":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    :cond_2
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v4, v6, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 406
    const/4 v0, 0x1

    goto :goto_1

    .line 399
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method public addPendingReq()V
    .locals 3

    .prologue
    .line 302
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mPendingReqCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mPendingReqCount:I

    .line 303
    const-string/jumbo v0, "[NotificationCenter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "addPendingReq: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mPendingReqCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    return-void
.end method

.method public changeNotification(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 6
    .param p1, "newItem"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p2, "oldItem"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "animation"    # Z

    .prologue
    .line 455
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 478
    :cond_0
    :goto_0
    return-void

    .line 459
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getTopNotificationItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    .line 461
    .local v3, "oldTop":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    monitor-enter v5

    .line 462
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lt v0, v4, :cond_2

    .line 461
    :goto_2
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 472
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getTopNotificationItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    .line 474
    .local v2, "newTop":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    invoke-direct {p0, v3, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->checkChangeTopItems(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 475
    invoke-direct {p0, p3}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->notifyNewNotification(Z)Z

    goto :goto_0

    .line 463
    .end local v2    # "newTop":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    :cond_2
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .line 465
    .local v1, "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    invoke-virtual {v1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 466
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v4, v0, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 461
    .end local v1    # "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 462
    .restart local v1    # "item":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getTopNotificationItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .locals 2

    .prologue
    .line 520
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521
    const/4 v0, 0x0

    .line 523
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    goto :goto_0
.end method

.method public getTopNotificationView()Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    .locals 2

    .prologue
    .line 357
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    const/4 v0, 0x0

    .line 360
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->createNotificationView(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    move-result-object v0

    goto :goto_0
.end method

.method public hasNotification()Z
    .locals 3

    .prologue
    .line 334
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 335
    const-string/jumbo v0, "[NotificationCenter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "hasNotification: size:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 336
    const-string/jumbo v2, ", isEmpty:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 335
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hasPendingReq()Z
    .locals 3

    .prologue
    .line 328
    const-string/jumbo v0, "[NotificationCenter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "hasPendingReq: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mPendingReqCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mPendingReqCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMultiMode()Z
    .locals 3

    .prologue
    .line 280
    const-string/jumbo v0, "[NotificationCenter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "isMultiMode:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    return v0
.end method

.method public onMessageSharingState(I)V
    .locals 3
    .param p1, "isSend"    # I

    .prologue
    .line 1302
    const-string/jumbo v0, "[NotificationCenter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onMessageSharingState:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1303
    const/16 v0, 0xc

    sget v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_MESSAGE:I

    .line 1304
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1303
    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1305
    return-void
.end method

.method public onNotifyAlarm(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;)V
    .locals 2
    .param p1, "alarmInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;

    .prologue
    .line 1241
    if-nez p1, :cond_0

    .line 1242
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "onNotifyAlarm: alarmInfo is null!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1249
    :goto_0
    return-void

    .line 1248
    :cond_0
    const/4 v0, 0x2

    sget v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_ALARM:I

    invoke-direct {p0, v0, v1, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    goto :goto_0
.end method

.method public onNotifyBTPairingRequest(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;)V
    .locals 2
    .param p1, "btInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;

    .prologue
    .line 1309
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "onNotifyBTPairingRequest"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1310
    if-nez p1, :cond_0

    .line 1321
    :cond_0
    return-void
.end method

.method public onNotifyCallState(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo;)V
    .locals 2
    .param p1, "callInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo;

    .prologue
    .line 1195
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "onNotifyCallState:"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1196
    return-void
.end method

.method public onNotifyLocationFailedRequestSent(I)V
    .locals 3
    .param p1, "msg"    # I

    .prologue
    const/16 v2, 0x1c

    const/4 v1, 0x0

    .line 1609
    .line 1610
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;

    .line 1611
    invoke-direct {v0, p1, v1, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;-><init>(III)V

    .line 1609
    invoke-direct {p0, v2, v2, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1612
    return-void
.end method

.method public onNotifyLocationGroupDestinatiON_CHANGEdSent()V
    .locals 3

    .prologue
    .line 1475
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "onNotifyLocationGroupDestinatiON_CHANGEdSent:"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1476
    const/16 v0, 0x15

    .line 1477
    sget v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_SHARE_CHANGED_HOST:I

    const/4 v2, 0x0

    .line 1476
    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1478
    return-void
.end method

.method public onNotifyLocationGroupDestinationChanged(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 5
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;
    .param p2, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 1454
    const-string/jumbo v3, "[NotificationCenter]"

    const-string/jumbo v4, "onNotifyGroupDestinationChanged:"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1455
    const-string/jumbo v1, ""

    .line 1456
    .local v1, "name":Ljava/lang/String;
    const-string/jumbo v2, ""

    .line 1458
    .local v2, "phoneNumber":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 1459
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1460
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    .line 1463
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1464
    .local v0, "locInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1465
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getNumber()Ljava/lang/String;

    move-result-object v4

    .line 1464
    invoke-static {v3, v4, p2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->setContent(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 1466
    const/16 v3, 0x11

    .line 1467
    sget v4, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_SHARE_CHANGED:I

    .line 1466
    invoke-direct {p0, v3, v4, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1468
    return-void
.end method

.method public onNotifyLocationGroupRequestIgnored()V
    .locals 4

    .prologue
    const v3, 0x7f0a0392

    .line 1588
    const-string/jumbo v1, "[NotificationCenter]"

    const-string/jumbo v2, "onNotifyLocationGroupRequestIgnored:"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1589
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;

    .line 1591
    const/4 v1, 0x0

    .line 1589
    invoke-direct {v0, v3, v3, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;-><init>(III)V

    .line 1592
    .local v0, "notiInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;
    const/16 v1, 0x1a

    sget v2, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_MESSAGE:I

    invoke-direct {p0, v1, v2, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1594
    return-void
.end method

.method public onNotifyLocationGroupShareRequest(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 3
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;
    .param p2, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 1430
    const-string/jumbo v1, "[NotificationCenter]"

    const-string/jumbo v2, "onNotifyLocationGroupShare:"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1431
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1432
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    .line 1431
    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1438
    .local v0, "locInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->hasSamsungAccount(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1441
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getNumber()Ljava/lang/String;

    move-result-object v2

    .line 1440
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreDialogView;->setContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 1443
    const/16 v1, 0x10

    .line 1444
    sget v2, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_GROUP_SHARE_REQ:I

    .line 1443
    invoke-direct {p0, v1, v2, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1449
    :goto_0
    return-void

    .line 1446
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->onNotifyLocationHasNoSamsungAccount(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;)V

    goto :goto_0
.end method

.method public onNotifyLocationGroupShareRequestIgnored()V
    .locals 3

    .prologue
    .line 1483
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "onNotifyLocationGroupShareRequestIgnored:"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1484
    const/16 v0, 0x19

    .line 1485
    sget v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_MESSAGE:I

    const/4 v2, 0x0

    .line 1484
    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1486
    return-void
.end method

.method public onNotifyLocationGroupSharingExpired()V
    .locals 3

    .prologue
    .line 1490
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "onNotifyLocationGroupSharingExpired:"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1491
    const/16 v0, 0x17

    .line 1492
    sget v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_SHARE_EXTEND:I

    const/4 v2, 0x0

    .line 1491
    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1493
    return-void
.end method

.method public onNotifyLocationGroupSharingRequestSent()V
    .locals 2

    .prologue
    .line 1497
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "onNotifyLocationGroupSharingRequestSent:"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1498
    return-void
.end method

.method public onNotifyLocationGroupSharingStopped(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0x12

    .line 1560
    const-string/jumbo v0, "[NotificationCenter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onNotifyLocationGroupSharingStopped:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1561
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasSameNotification(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1563
    sget v0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_MESSAGE:I

    .line 1562
    invoke-direct {p0, v3, v0, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1565
    :cond_0
    return-void
.end method

.method public onNotifyLocationHasNoSamsungAccount(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;)V
    .locals 4
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    .prologue
    const/16 v3, 0x1b

    .line 1598
    const-string/jumbo v1, "DEBUG"

    const-string/jumbo v2, "onNotifyLocationHasNoSamsungAccount"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1600
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1601
    .local v0, "locInfoAccount":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHasNotSamsungAccountView;->setContent(Ljava/lang/String;)V

    .line 1602
    invoke-direct {p0, v3, v3, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1605
    return-void
.end method

.method public onNotifyLocationRequest(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Ljava/lang/String;)V
    .locals 3
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 1379
    const-string/jumbo v1, "[NotificationCenter]"

    const-string/jumbo v2, "onNotifyRequest:"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1381
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->hasSamsungAccount(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1382
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1383
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    .line 1382
    invoke-direct {v0, v1, v2, p2}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1384
    .local v0, "locInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;
    const/16 v1, 0x8

    .line 1385
    sget v2, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_SHARE_CONFIRM:I

    .line 1384
    invoke-direct {p0, v1, v2, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1389
    .end local v0    # "locInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;
    :goto_0
    return-void

    .line 1387
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->onNotifyLocationHasNoSamsungAccount(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;)V

    goto :goto_0
.end method

.method public onNotifyLocationRequestIgnored()V
    .locals 4

    .prologue
    const v3, 0x7f0a02df

    .line 1503
    const-string/jumbo v1, "[NotificationCenter]"

    const-string/jumbo v2, "onNotifyLocationRequestIgnored:"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1505
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;

    .line 1507
    const/4 v1, 0x3

    .line 1505
    invoke-direct {v0, v3, v3, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;-><init>(III)V

    .line 1509
    .local v0, "notiInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;
    const/16 v1, 0x1a

    sget v2, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_MESSAGE:I

    invoke-direct {p0, v1, v2, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1511
    return-void
.end method

.method public onNotifyLocationRequestSent(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
    .locals 2
    .param p1, "dlContact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 1569
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "onNotifyLocationRequestSent:"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1570
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->setContact(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    .line 1571
    const/16 v0, 0x13

    sget v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_REQUEST:I

    invoke-direct {p0, v0, v1, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1573
    return-void
.end method

.method public onNotifyLocationRequestWaiting(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
    .locals 3
    .param p1, "dlContact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 1577
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "onNotifyLocationRequestWaiting:"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1578
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 1579
    const-string/jumbo v1, "PREF_LOCATION_SHARE_REQUEST_WAITING"

    .line 1580
    const/4 v2, 0x1

    .line 1579
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 1581
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->setContact(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    .line 1582
    const/16 v0, 0x14

    .line 1583
    sget v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_REQUEST_WAITING:I

    .line 1582
    invoke-direct {p0, v0, v1, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1584
    return-void
.end method

.method public onNotifyLocationShare(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Ljava/lang/String;)V
    .locals 5
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 1394
    const-string/jumbo v2, "[NotificationCenter]"

    const-string/jumbo v3, "onNotifyShare:"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1396
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getTopNotificationItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v1

    .line 1398
    .local v1, "top":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v2

    const/16 v3, 0x14

    if-ne v2, v3, :cond_0

    .line 1399
    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->removeNotification(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    .line 1402
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v2

    .line 1403
    const-string/jumbo v3, "PREF_LOCATION_SHARE_REQUEST_WAITING"

    .line 1404
    const/4 v4, 0x0

    .line 1402
    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v2

    .line 1404
    if-eqz v2, :cond_1

    .line 1405
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->getInstance()Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mContext:Landroid/content/Context;

    .line 1406
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getPhoneNumber()Ljava/lang/String;

    move-result-object v4

    .line 1405
    invoke-virtual {v2, v3, v4, p2}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->acceptLocationShared(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1413
    :goto_0
    return-void

    .line 1408
    :cond_1
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getName()Ljava/lang/String;

    move-result-object v2

    .line 1409
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    .line 1408
    invoke-direct {v0, v2, v3, p2}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1410
    .local v0, "locInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;
    const/4 v2, 0x7

    .line 1411
    sget v3, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_SHARE_REQ:I

    .line 1410
    invoke-direct {p0, v2, v3, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    goto :goto_0
.end method

.method public onNotifyLocationShareEnded()V
    .locals 3

    .prologue
    .line 1534
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "onNotifyLocationShareEnded:"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1535
    const/16 v0, 0x18

    .line 1536
    sget v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_SHARE_ENDED:I

    const/4 v2, 0x0

    .line 1535
    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1537
    return-void
.end method

.method public onNotifyLocationShareSent(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1527
    const-string/jumbo v0, "[NotificationCenter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onNotifyLocationShareSent:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1528
    const/16 v0, 0x16

    .line 1529
    sget v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_LOC_SHARE_SENT:I

    .line 1528
    invoke-direct {p0, v0, v1, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1530
    return-void
.end method

.method public onNotifyLocationSharedIgnored()V
    .locals 4

    .prologue
    const v3, 0x7f0a02e0

    .line 1515
    const-string/jumbo v1, "[NotificationCenter]"

    const-string/jumbo v2, "onNotifyLocationSharedIgnored:"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1517
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;

    .line 1519
    const/4 v1, 0x3

    .line 1517
    invoke-direct {v0, v3, v3, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;-><init>(III)V

    .line 1521
    .local v0, "notiInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;
    const/16 v1, 0x1a

    sget v2, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_MESSAGE:I

    invoke-direct {p0, v1, v2, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1523
    return-void
.end method

.method public onNotifyLowBattery(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 1231
    const-string/jumbo v0, "[NotificationCenter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onNotifyLowBattery: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1237
    return-void
.end method

.method public onNotifyMirrorLinkSetup()V
    .locals 2

    .prologue
    .line 1418
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "onNotifyMirrorLinkSetup:"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1419
    return-void
.end method

.method public onNotifyMirrorLinkShutDown()V
    .locals 2

    .prologue
    .line 1424
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "onNotifyMirrorLinkShutDown:"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1425
    return-void
.end method

.method public onNotifyMusic()V
    .locals 3

    .prologue
    .line 1552
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "onNotifyMusic "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1554
    const/16 v0, 0xf

    sget v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_OTHER:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1555
    return-void
.end method

.method public onNotifyOutgoingCall()V
    .locals 2

    .prologue
    .line 1200
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "onNotifyOutgoingCall:"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1201
    return-void
.end method

.method public onNotifyReceivedMSG(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;)V
    .locals 2
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    .prologue
    .line 1205
    if-eqz p1, :cond_0

    .line 1206
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getMSGType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    if-ne v0, v1, :cond_1

    .line 1207
    :cond_0
    const-string/jumbo v0, "[NotificationCenter]"

    .line 1208
    const-string/jumbo v1, "onNotifyReceivedMSG: msgInfo is null! or MSG Type is Unknown"

    .line 1207
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1227
    :goto_0
    return-void

    .line 1211
    :cond_1
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "onNotifyReceivedMSG:"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1214
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateInboxList()V

    .line 1215
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateUnreadMessageCount()V

    .line 1218
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getImage()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1219
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "msgInfo.getImage() = null"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1221
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->isAppForeground()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1222
    const/4 v0, 0x6

    sget v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_MESSAGE:I

    invoke-direct {p0, v0, v1, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    goto :goto_0

    .line 1224
    :cond_3
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "App is background. Received Message skipped."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onNotifySchedule(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;)V
    .locals 6
    .param p1, "scheduleInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;

    .prologue
    .line 1253
    if-nez p1, :cond_0

    .line 1254
    const-string/jumbo v2, "[NotificationCenter]"

    const-string/jumbo v3, "onNotifySchedule: scheduleInfo is null!"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1280
    :goto_0
    return-void

    .line 1262
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getStartDate()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1263
    const-string/jumbo v2, "[NotificationCenter]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "StartDate: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getStartDate()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getAttendeesList()Ljava/util/ArrayList;

    move-result-object v1

    .line 1266
    .local v1, "attendees":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1267
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1271
    :cond_2
    const-string/jumbo v2, "[NotificationCenter]"

    const-string/jumbo v3, "+++++++++++++++++++++++++++++++++++++"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1274
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->isAppForeground()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1275
    const/4 v2, 0x5

    sget v3, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_SCHEDULE:I

    invoke-direct {p0, v2, v3, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    goto :goto_0

    .line 1267
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1268
    .local v0, "attendee":Ljava/lang/Integer;
    const-string/jumbo v3, "[NotificationCenter]"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "attendee:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1278
    .end local v0    # "attendee":Ljava/lang/Integer;
    :cond_4
    const-string/jumbo v2, "[NotificationCenter]"

    const-string/jumbo v3, "App is background. Received Schedule skipped."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onSendInvitation(I)V
    .locals 3
    .param p1, "total"    # I

    .prologue
    .line 1358
    const-string/jumbo v0, "[NotificationCenter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onSendInvitation:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1359
    const/16 v0, 0xd

    sget v1, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->NOTI_PRIORITY_MESSAGE:I

    .line 1360
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1359
    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->makeNotification(IILjava/lang/Object;)V

    .line 1362
    return-void
.end method

.method public onSendInvitationFinished()V
    .locals 2

    .prologue
    .line 1373
    const-string/jumbo v0, "[NotificationCenter]"

    const-string/jumbo v1, "onSendInvitationFinished:"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1374
    return-void
.end method

.method public registerOnNotificationChangeListener(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    .prologue
    .line 482
    if-eqz p1, :cond_0

    .line 483
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->setOnNotificationChangeListener(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;)V

    .line 488
    :goto_0
    return-void

    .line 485
    :cond_0
    const-string/jumbo v0, "[NotificationCenter]"

    .line 486
    const-string/jumbo v1, "NULL listener can\'t register. use unregister method instead of it."

    .line 485
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeNotification(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V
    .locals 4
    .param p1, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .prologue
    .line 365
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getTopNotificationItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    .line 367
    .local v0, "topItem":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    if-nez v0, :cond_1

    .line 387
    :cond_0
    :goto_0
    return-void

    .line 370
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 371
    if-eqz v0, :cond_2

    .line 372
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v1

    const/4 v2, 0x6

    if-ne v1, v2, :cond_2

    .line 373
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    .line 374
    const/4 v2, 0x1

    .line 373
    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->onPlaybackRequest(I)V

    .line 376
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 377
    const-string/jumbo v1, "[NotificationCenter]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "removeNotification ++:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    monitor-enter v2

    .line 379
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 380
    const-string/jumbo v1, "[NotificationCenter]"

    const-string/jumbo v3, "Success to remove noti item."

    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    :goto_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385
    const-string/jumbo v1, "[NotificationCenter]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "removeNotification --:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 382
    :cond_3
    :try_start_1
    const-string/jumbo v1, "[NotificationCenter]"

    const-string/jumbo v3, "Fail to remove noti item."

    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 378
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public removePendingReq()Z
    .locals 4

    .prologue
    .line 307
    const/4 v0, 0x0

    .line 308
    .local v0, "bNotify":Z
    iget v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mPendingReqCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mPendingReqCount:I

    .line 309
    const-string/jumbo v1, "[NotificationCenter]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "removePendingReq: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mPendingReqCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    iget v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mPendingReqCount:I

    if-gez v1, :cond_0

    .line 311
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mPendingReqCount:I

    .line 314
    :cond_0
    iget v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mPendingReqCount:I

    if-nez v1, :cond_1

    .line 315
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->notifyNewNotification(Z)Z

    move-result v0

    .line 318
    :cond_1
    return v0
.end method

.method public resetIsPrepareNotiActivity()V
    .locals 1

    .prologue
    .line 914
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->prepareNotiActivity:Z

    .line 915
    return-void
.end method

.method public resetPendingReq()Z
    .locals 3

    .prologue
    .line 322
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mPendingReqCount:I

    .line 323
    const-string/jumbo v0, "[NotificationCenter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "resetPendingReq: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mPendingReqCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->notifyNewNotification(Z)Z

    move-result v0

    return v0
.end method

.method public setMultiMode(Z)V
    .locals 4
    .param p1, "multiMode"    # Z

    .prologue
    .line 289
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 290
    .local v0, "oldMultiMode":Z
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    .line 291
    const-string/jumbo v1, "[NotificationCenter]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "setMultiMode: old:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", new:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    if-eq v0, v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->multiMode:Z

    if-nez v1, :cond_0

    .line 294
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasNotification()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 295
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->notifyNewNotification(Z)Z

    .line 297
    :cond_0
    return-void
.end method

.method public setOnNotificationMapChangeListener(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    .prologue
    .line 516
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiMapChangeListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    .line 517
    return-void
.end method

.method public unregisterOnNotificationChangeListener(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    .prologue
    .line 492
    if-eqz p1, :cond_1

    .line 493
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiChangeListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    if-eqz v0, :cond_0

    .line 494
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiChangeListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 495
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->setOnNotificationChangeListener(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;)V

    .line 504
    :goto_0
    return-void

    .line 497
    :cond_0
    const-string/jumbo v0, "[NotificationCenter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "another listener is already registered. now:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 498
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->mNotiChangeListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 497
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 501
    :cond_1
    const-string/jumbo v0, "[NotificationCenter]"

    .line 502
    const-string/jumbo v1, "NULL listener can\'t unregister. use listener that originally unregistered."

    .line 501
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
