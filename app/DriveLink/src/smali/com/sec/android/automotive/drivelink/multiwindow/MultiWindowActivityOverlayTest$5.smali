.class Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$5;
.super Ljava/lang/Object;
.source "MultiWindowActivityOverlayTest.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$5;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;

    .line 946
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 951
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$5;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;

    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->access$4(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    move-result-object v3

    if-nez v3, :cond_1

    .line 970
    :cond_0
    :goto_0
    return v2

    .line 954
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 957
    new-instance v1, Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$5;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;

    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->access$4(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$5;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;

    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->access$4(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getTop()I

    move-result v4

    .line 958
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$5;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;

    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->access$4(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getRight()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$5;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;

    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->access$4(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getBottom()I

    move-result v6

    .line 957
    invoke-direct {v1, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 959
    .local v1, "r":Landroid/graphics/Rect;
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    .line 960
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    .line 959
    invoke-virtual {v1, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    .line 962
    .local v0, "bTouchInside":Z
    if-nez v0, :cond_2

    .line 963
    const-string/jumbo v3, "[MultiWindowActivity]"

    const-string/jumbo v4, "Touched outside noti."

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 966
    :cond_2
    const-string/jumbo v2, "[MultiWindowActivity]"

    const-string/jumbo v3, "Touched inside noti."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 967
    const/4 v2, 0x1

    goto :goto_0
.end method
