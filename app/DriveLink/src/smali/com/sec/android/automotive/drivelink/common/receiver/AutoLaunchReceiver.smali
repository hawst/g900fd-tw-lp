.class public Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AutoLaunchReceiver.java"


# static fields
.field private static final CALL_STATE_OFFHOOK:I = 0x2

.field private static final TAG:Ljava/lang/String;

.field private static final mBits:I = 0x4

.field private static mDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

.field private static mDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

.field private static sStaticCounter:I


# instance fields
.field private mDeviceIsRegistered:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    .line 48
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->sStaticCounter:I

    .line 51
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private autoLaunch(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "connectedDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/4 v4, 0x1

    .line 300
    const-string/jumbo v2, "phone"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    .line 299
    invoke-static {v2}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    .line 302
    .local v1, "mTelephonyService":Lcom/android/internal/telephony/ITelephony;
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->isCarAppRunning(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 303
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "Car Mode not yet running."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v2

    .line 305
    const-string/jumbo v3, "PREF_SETTINGS_MY_CAR_AUTO_LAUNCH"

    .line 304
    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v2

    .line 306
    if-eqz v2, :cond_2

    .line 307
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "Auto Launch is checked."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceIsRegistered:Z

    if-eqz v2, :cond_1

    .line 309
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 310
    const-string/jumbo v4, " registered."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 309
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    .line 312
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 311
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 313
    const-string/jumbo v2, "CM24"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 315
    if-eqz v1, :cond_0

    .line 316
    :try_start_0
    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->getCallState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 317
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    .line 318
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Car Mode Auto Launched by "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 319
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 320
    const-string/jumbo v4, " after end call"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 318
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 317
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v2

    .line 323
    const-string/jumbo v3, "PREF_AUTO_LAUNCH_DURING_CALL"

    .line 324
    const/4 v4, 0x1

    .line 323
    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 325
    const/4 v2, 0x1

    invoke-static {p1, v2}, Lcom/sec/android/automotive/drivelink/common/receiver/EndCallReceiver;->enable(Landroid/content/Context;Z)V

    .line 411
    :goto_0
    return-void

    .line 327
    :cond_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    .line 328
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Car Mode Auto Launched by "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 329
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 328
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 327
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->launchApplication(Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 332
    :catch_0
    move-exception v0

    .line 334
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 337
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 338
    const-string/jumbo v4, " not registered."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 337
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 341
    :cond_2
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "Auto Launch is not checked."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 409
    :cond_3
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "Car Mode already running."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getConnectedDevice()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    if-nez v0, :cond_0

    .line 55
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    .line 57
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    goto :goto_0
.end method

.method public static getConnectedDevice2()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    if-nez v0, :cond_0

    .line 62
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    .line 64
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    goto :goto_0
.end method

.method public static getConnectedDeviceA2DP()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method public static getConnectedDeviceHeadset()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method private handleConnect(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "connectedDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 182
    if-nez p2, :cond_0

    .line 183
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "null device connected."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    :goto_0
    return-void

    .line 187
    :cond_0
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v1

    const/16 v2, 0xc

    if-ne v1, v2, :cond_4

    .line 188
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    .line 189
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "connected device "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " bonded"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 188
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    invoke-static {p1, p2}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->getDeviceRegisteredId(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    .line 192
    .local v0, "deviceRegisteredId":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceIsRegistered:Z

    .line 193
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    .line 194
    new-instance v3, Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceIsRegistered:Z

    if-eqz v1, :cond_3

    const-string/jumbo v1, "Registered device "

    .line 195
    :goto_2
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 196
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " connected"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 194
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 193
    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceIsRegistered:Z

    if-eqz v1, :cond_1

    .line 201
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 202
    const-string/jumbo v2, "PREF_LAST_CONNECTED_REGISTERED_CAR"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;I)V

    .line 205
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->autoLaunch(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_0

    .line 192
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 195
    :cond_3
    const-string/jumbo v1, "Unregistered device "

    goto :goto_2

    .line 207
    .end local v0    # "deviceRegisteredId":I
    :cond_4
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    .line 208
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "connected device "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 209
    const-string/jumbo v3, " not bonded"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 208
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 207
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private handleDisconnect(Landroid/content/Context;Landroid/content/Intent;Z)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "isHeadset"    # Z

    .prologue
    const/4 v4, 0x0

    .line 215
    .line 216
    const-string/jumbo v1, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 217
    .local v0, "disconnectedDevice":Landroid/bluetooth/BluetoothDevice;
    if-nez v0, :cond_1

    .line 218
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "null device disconnected."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    if-eqz p3, :cond_5

    .line 222
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_4

    .line 223
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 224
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    .line 223
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 224
    if-eqz v1, :cond_4

    .line 225
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 226
    const-string/jumbo v3, " headset disconnected"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 225
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    sput-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    .line 229
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_3

    .line 230
    :cond_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    if-nez v1, :cond_0

    .line 231
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 232
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    .line 233
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    .line 231
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 233
    if-nez v1, :cond_0

    .line 234
    :cond_3
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 235
    const-string/jumbo v3, " disconnected"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 234
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->showDisconnectToast(Landroid/content/Context;)V

    goto :goto_0

    .line 244
    :cond_4
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_0

    .line 245
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 246
    const-string/jumbo v3, " headset disconnected but "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 247
    sget-object v3, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 248
    const-string/jumbo v3, " headset still connected"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 245
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 251
    :cond_5
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_8

    .line 252
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 253
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    .line 252
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 253
    if-eqz v1, :cond_8

    .line 254
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 255
    const-string/jumbo v3, " a2dp disconnected"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 254
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    sput-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    .line 258
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    if-nez v1, :cond_6

    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_7

    .line 259
    :cond_6
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    if-nez v1, :cond_0

    .line 260
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 261
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    .line 262
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    .line 260
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 262
    if-nez v1, :cond_0

    .line 263
    :cond_7
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 264
    const-string/jumbo v3, " disconnected"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 263
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->showDisconnectToast(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 273
    :cond_8
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_0

    .line 274
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 275
    const-string/jumbo v3, " a2dp disconnected but "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 276
    sget-object v3, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 277
    const-string/jumbo v3, " a2dp still connected"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 274
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method protected static isCarAppRunning(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 437
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 438
    const-string/jumbo v3, "car_mode_on"

    .line 437
    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_0

    .line 446
    :goto_0
    return v1

    .line 441
    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    .line 443
    :catch_0
    move-exception v0

    .line 444
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    .line 445
    const-string/jumbo v3, "isCarAppRunning SettingNotFoundException"

    .line 444
    invoke-static {v2, v3, v0}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 446
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v2

    .line 447
    const-string/jumbo v3, "PREF_CAR_APP_RUNNING"

    .line 446
    invoke-virtual {v2, v3, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_0
.end method

.method protected static launchApplication(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 423
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->isCarAppRunning(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 424
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "car_mode_on"

    .line 425
    const/4 v2, 0x1

    .line 424
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 427
    :cond_0
    return-void
.end method

.method private static showDisconnectToast(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 284
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 285
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->isCarAppRunning(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 286
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 288
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    .line 289
    const v2, 0x7f0a0269

    const/4 v3, 0x0

    .line 287
    invoke-static {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;II)Landroid/widget/Toast;

    move-result-object v0

    .line 290
    .local v0, "btDisconnectToast":Landroid/widget/Toast;
    if-eqz v0, :cond_0

    .line 291
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 294
    .end local v0    # "btDisconnectToast":Landroid/widget/Toast;
    :cond_0
    return-void
.end method


# virtual methods
.method public getUniqueId()J
    .locals 4

    .prologue
    .line 452
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const/4 v2, 0x4

    shl-long/2addr v0, v2

    .line 453
    sget v2, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->sStaticCounter:I

    add-int/lit8 v3, v2, 0x1

    sput v3, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->sStaticCounter:I

    and-int/lit8 v2, v2, 0x2

    xor-int/lit8 v2, v2, 0x3

    int-to-long v2, v2

    .line 452
    or-long/2addr v0, v2

    return-wide v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    const/4 v10, -0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/high16 v9, -0x80000000

    .line 79
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    const-string/jumbo v4, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 85
    const-string/jumbo v4, "android.bluetooth.profile.extra.STATE"

    invoke-virtual {p2, v4, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 88
    .local v3, "state":I
    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 101
    :pswitch_0
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    .line 102
    const-string/jumbo v5, "BluetoothHeadset STATE_DISCONNECTED"

    .line 101
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    invoke-direct {p0, p1, p2, v6}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->handleDisconnect(Landroid/content/Context;Landroid/content/Intent;Z)V

    goto :goto_0

    .line 90
    :pswitch_1
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    .line 91
    const-string/jumbo v5, "BluetoothHeadset STATE_CONNECTED"

    .line 90
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    const-string/jumbo v4, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/bluetooth/BluetoothDevice;

    .line 92
    sput-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    .line 94
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    invoke-direct {p0, p1, v4}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->handleConnect(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_0

    .line 97
    :pswitch_2
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    .line 98
    const-string/jumbo v5, "BluetoothHeadset STATE_CONNECTING"

    .line 97
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 106
    :pswitch_3
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    .line 107
    const-string/jumbo v5, "BluetoothHeadset STATE_DISCONNECTING"

    .line 106
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 110
    .end local v3    # "state":I
    :cond_2
    const-string/jumbo v4, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 111
    const-string/jumbo v4, "android.bluetooth.profile.extra.STATE"

    invoke-virtual {p2, v4, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 114
    .restart local v3    # "state":I
    packed-switch v3, :pswitch_data_1

    goto :goto_0

    .line 126
    :pswitch_4
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    .line 127
    const-string/jumbo v5, "BluetoothA2dp STATE_DISCONNECTED"

    .line 126
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    invoke-direct {p0, p1, p2, v7}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->handleDisconnect(Landroid/content/Context;Landroid/content/Intent;Z)V

    goto :goto_0

    .line 116
    :pswitch_5
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "BluetoothA2dp STATE_CONNECTED"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    const-string/jumbo v4, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/bluetooth/BluetoothDevice;

    .line 118
    sput-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    .line 120
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    invoke-direct {p0, p1, v4}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->handleConnect(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)V

    goto/16 :goto_0

    .line 123
    :pswitch_6
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "BluetoothA2dp STATE_CONNECTING"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 131
    :pswitch_7
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    .line 132
    const-string/jumbo v5, "BluetoothA2dp STATE_DISCONNECTING"

    .line 131
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 135
    .end local v3    # "state":I
    :cond_3
    const-string/jumbo v4, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 136
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "ACTION_ACL_CONNECTED"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 137
    :cond_4
    const-string/jumbo v4, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 138
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "ACTION_ACL_DISCONNECTED"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 139
    :cond_5
    const-string/jumbo v4, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 140
    const/16 v4, 0xc

    .line 141
    const-string/jumbo v8, "android.bluetooth.device.extra.BOND_STATE"

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 140
    if-ne v4, v8, :cond_c

    .line 144
    const-string/jumbo v4, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 145
    .local v1, "device":Landroid/bluetooth/BluetoothDevice;
    if-nez v1, :cond_6

    .line 146
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "null device bonded."

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 150
    :cond_6
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v9, " bonded."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v8

    .line 152
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    if-nez v4, :cond_7

    move-object v4, v5

    .line 151
    :goto_1
    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 153
    if-eqz v4, :cond_9

    .line 156
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    .line 155
    invoke-static {p1, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->getDeviceRegisteredId(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)I

    move-result v2

    .line 157
    .local v2, "deviceRegisteredId":I
    if-eq v2, v10, :cond_8

    move v4, v6

    :goto_2
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceIsRegistered:Z

    .line 158
    invoke-direct {p0, p1, v1}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->autoLaunch(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)V

    goto/16 :goto_0

    .line 153
    .end local v2    # "deviceRegisteredId":I
    :cond_7
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .restart local v2    # "deviceRegisteredId":I
    :cond_8
    move v4, v7

    .line 157
    goto :goto_2

    .line 159
    .end local v2    # "deviceRegisteredId":I
    :cond_9
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    .line 160
    sget-object v8, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    if-nez v8, :cond_a

    .line 159
    :goto_3
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 161
    if-eqz v4, :cond_0

    .line 164
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    .line 163
    invoke-static {p1, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->getDeviceRegisteredId(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)I

    move-result v2

    .line 165
    .restart local v2    # "deviceRegisteredId":I
    if-eq v2, v10, :cond_b

    :goto_4
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceIsRegistered:Z

    .line 166
    invoke-direct {p0, p1, v1}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->autoLaunch(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)V

    goto/16 :goto_0

    .line 161
    .end local v2    # "deviceRegisteredId":I
    :cond_a
    sget-object v5, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->mDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    .restart local v2    # "deviceRegisteredId":I
    :cond_b
    move v6, v7

    .line 165
    goto :goto_4

    .line 169
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v2    # "deviceRegisteredId":I
    :cond_c
    const-string/jumbo v4, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 171
    const-string/jumbo v4, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 173
    .restart local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    const-string/jumbo v4, "android.bluetooth.device.extra.BOND_STATE"

    .line 172
    invoke-virtual {p2, v4, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 175
    .restart local v3    # "state":I
    sget-object v5, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "ACTION_BOND_STATE_CHANGED "

    invoke-direct {v6, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 176
    if-nez v1, :cond_d

    const-string/jumbo v4, "null device"

    :goto_5
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 177
    const-string/jumbo v6, " state is "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 175
    invoke-static {v5, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 176
    :cond_d
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    goto :goto_5

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch

    .line 114
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method
