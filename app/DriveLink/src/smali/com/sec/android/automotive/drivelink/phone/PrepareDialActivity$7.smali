.class Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$7;
.super Ljava/lang/Object;
.source "PrepareDialActivity.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    .line 982
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 2
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "arg1"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 1013
    const-string/jumbo v0, "PrepareDial"

    const-string/jumbo v1, "TTSAnyway : onRequestCancelled  startBargeInAndTimer()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1014
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1015
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->startBargeInAndTimer()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)V

    .line 1016
    :cond_0
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 2
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 1005
    const-string/jumbo v0, "PrepareDial"

    const-string/jumbo v1, "TTSAnyway : onRequestDidPlay  startBargeInAndTimer()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1006
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1007
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->startBargeInAndTimer()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)V

    .line 1008
    :cond_0
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 2
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "arg1"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 998
    const-string/jumbo v0, "PrepareDial"

    const-string/jumbo v1, "TTSAnyway : onRequestIgnored  startBargeInAndTimer()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 999
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1000
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->startBargeInAndTimer()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)V

    .line 1001
    :cond_0
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 2
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 985
    const-string/jumbo v0, "PrepareDial"

    const-string/jumbo v1, "TTSAnyway : onRequestWillPlay"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 986
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$0(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    move-result-object v0

    if-nez v0, :cond_1

    .line 993
    :cond_0
    :goto_0
    return-void

    .line 989
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$0(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->getState()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$1(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;I)V

    .line 990
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mState:I
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$2(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 991
    const-string/jumbo v0, "PrepareDial"

    const-string/jumbo v1, "DEBUG TTSAnyway : onRequestWillPlay"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
