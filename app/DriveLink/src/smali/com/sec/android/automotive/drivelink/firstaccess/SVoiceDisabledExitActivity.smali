.class public Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "SVoiceDisabledExitActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity;

    .line 18
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 17
    sput-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity;->TAG:Ljava/lang/String;

    .line 18
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity;->closeApplication()V

    return-void
.end method

.method private closeApplication()V
    .locals 4

    .prologue
    .line 65
    sget-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "S-Voice Provider is disalbled. app close"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "car_mode_on"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "car_mode_on"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity;->finish()V

    .line 74
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->terminate()V

    .line 75
    return-void

    .line 70
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 59
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onBackPressed()V

    .line 61
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity;->closeApplication()V

    .line 62
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 22
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    sget-object v2, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onCreate"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    const v2, 0x7f030038

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity;->setContentView(I)V

    .line 27
    new-instance v1, Landroid/app/AlertDialog$Builder;

    .line 28
    const/4 v2, 0x4

    .line 27
    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 30
    .local v1, "dialog":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0a04ab

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 32
    const v2, 0x7f0a0254

    .line 33
    new-instance v3, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity$1;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity;)V

    .line 32
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 40
    new-instance v2, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 51
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 52
    .local v0, "alert":Landroid/app/AlertDialog;
    const v2, 0x7f0a001a

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 53
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 55
    return-void
.end method
