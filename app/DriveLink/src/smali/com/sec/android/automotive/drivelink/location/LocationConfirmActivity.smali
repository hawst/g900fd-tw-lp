.class public Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;
.source "LocationConfirmActivity.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I = null

.field public static final ADDRESS:Ljava/lang/String; = "address"


# instance fields
.field private isRout:Z

.field private mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

.field private mAddress:Ljava/lang/String;

.field private mBtnCancel:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

.field private mBtnRoute:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

.field private final mNotifiyNewMicState:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;

.field private mText:Landroid/widget/TextView;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 43
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->isRout:Z

    .line 277
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mNotifiyNewMicState:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;

    .line 43
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;Lcom/nuance/sample/MicState;)V
    .locals 0

    .prologue
    .line 264
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->handleMicStateChanged(Lcom/nuance/sample/MicState;)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 303
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->hasGoogleAccount(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;)Z
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->IsOffline()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;)Z
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->isProviderEnabled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mAddress:Ljava/lang/String;

    return-object v0
.end method

.method private handleMicStateChanged(Lcom/nuance/sample/MicState;)V
    .locals 2
    .param p1, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    .line 266
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 275
    :goto_0
    return-void

    .line 268
    :pswitch_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->quotesInButton(Z)V

    goto :goto_0

    .line 272
    :pswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->quotesInButton(Z)V

    goto :goto_0

    .line 266
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private hasGoogleAccount(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 304
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 305
    .local v1, "accMan":Landroid/accounts/AccountManager;
    const-string/jumbo v3, "com.google"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 306
    .local v0, "accArray":[Landroid/accounts/Account;
    array-length v3, v0

    if-lt v3, v2, :cond_0

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isGoogleMapsNavigation()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 236
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 235
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v2

    .line 237
    const-string/jumbo v3, "PREF_SETTINGS_MY_NAVIGATION"

    .line 236
    invoke-virtual {v2, v3, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v0

    .line 239
    .local v0, "mapID":I
    sget v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_GOOGLE_MAPS:I

    if-ne v0, v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method


# virtual methods
.method protected getLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    .locals 1

    .prologue
    .line 225
    const/4 v0, 0x0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 293
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onBackPressed()V

    .line 295
    const-string/jumbo v1, "VAC_CLIENT_DM"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 296
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "DM_LOCATION_NAV_SEARCH_CONFIRM"

    if-ne v1, v2, :cond_0

    .line 297
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 298
    .local v0, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v1, "DM_LOCATION"

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 301
    .end local v0    # "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    const v7, 0x7f0a0249

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 57
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    if-nez p1, :cond_6

    .line 61
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->isFlowManagerIntent(Landroid/content/Intent;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 63
    const-string/jumbo v6, "EXTRA_FLOW_ID"

    .line 62
    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "flowID":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v1

    .line 68
    .local v1, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v1, :cond_0

    .line 69
    iget-object v5, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mLocationName:Ljava/lang/String;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mAddress:Ljava/lang/String;

    .line 79
    .end local v0    # "flowID":Ljava/lang/String;
    .end local v1    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mAddress:Ljava/lang/String;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mAddress:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 81
    :cond_1
    const v5, 0x7f0a0330

    .line 80
    invoke-static {p0, v5, v10}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;II)Landroid/widget/Toast;

    move-result-object v4

    .line 83
    .local v4, "toast":Landroid/widget/Toast;
    if-eqz v4, :cond_2

    .line 84
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 87
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->finish()V

    .line 90
    .end local v4    # "toast":Landroid/widget/Toast;
    :cond_3
    const v5, 0x7f030012

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->setContentView(I)V

    .line 92
    const v5, 0x7f090093

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 94
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v5, v7}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setSayText(I)V

    .line 95
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v5, v7}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setSayListeningText(I)V

    .line 97
    const v5, 0x7f090095

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mText:Landroid/widget/TextView;

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 99
    const v6, 0x7f0a02de

    new-array v7, v9, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mAddress:Ljava/lang/String;

    aput-object v8, v7, v10

    .line 98
    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 101
    .local v2, "msg":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mAddress:Ljava/lang/String;

    .line 100
    invoke-static {v2, v5, v9}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->getHighlightedText(Ljava/lang/String;Ljava/lang/String;I)Landroid/text/SpannableString;

    move-result-object v3

    .line 102
    .local v3, "msgSpannable":Landroid/text/SpannableString;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mText:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    const v5, 0x7f090098

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mBtnCancel:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 111
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mBtnCancel:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    new-instance v6, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$2;

    invoke-direct {v6, p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;)V

    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    const v5, 0x7f090099

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mBtnRoute:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 128
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mBtnRoute:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    new-instance v6, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$3;

    invoke-direct {v6, p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;)V

    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    new-instance v6, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$4;

    invoke-direct {v6, p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;)V

    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v5, v6}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 175
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mNotifiyNewMicState:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;

    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setNotifiyNewMicState(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;)V

    .line 177
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v5

    invoke-virtual {v5}, Lcom/nuance/drivelink/DLAppUiUpdater;->getMicState()Lcom/nuance/sample/MicState;

    move-result-object v5

    sget-object v6, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    if-ne v5, v6, :cond_4

    .line 178
    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->quotesInButton(Z)V

    .line 179
    :cond_4
    return-void

    .line 73
    .end local v2    # "msg":Ljava/lang/String;
    .end local v3    # "msgSpannable":Landroid/text/SpannableString;
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string/jumbo v6, "address"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mAddress:Ljava/lang/String;

    goto/16 :goto_0

    .line 76
    :cond_6
    const-string/jumbo v5, "address"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mAddress:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 212
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onDestroy()V

    .line 220
    :goto_0
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1, v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 221
    return-void

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mFlowID:Ljava/lang/String;

    .line 216
    .local v0, "bkp":Ljava/lang/String;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mFlowID:Ljava/lang/String;

    .line 217
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onDestroy()V

    .line 218
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mFlowID:Ljava/lang/String;

    goto :goto_0
.end method

.method public onFlowCommandCancel(Ljava/lang/String;)V
    .locals 3
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 329
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onFlowCommandCancel(Ljava/lang/String;)V

    .line 331
    const-string/jumbo v1, "VAC_CLIENT_DM"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 332
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "DM_LOCATION_NAV_SEARCH_CONFIRM"

    if-ne v1, v2, :cond_0

    .line 333
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 334
    .local v0, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v1, "DM_LOCATION"

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 337
    .end local v0    # "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_0
    return-void
.end method

.method public onFlowCommandNo(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 261
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->finish()V

    .line 262
    return-void
.end method

.method public onFlowCommandRoute(Ljava/lang/String;)V
    .locals 2
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->isProviderEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 184
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->isGoogleMapsNavigation()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 186
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v0

    .line 185
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->hasGoogleAccount(Landroid/content/Context;)Z

    move-result v0

    .line 186
    if-eqz v0, :cond_1

    .line 188
    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mAddress:Ljava/lang/String;

    .line 187
    invoke-static {p0, v0, v1}, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->startNavigation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;Ljava/lang/String;)V

    .line 195
    :goto_0
    return-void

    .line 190
    :cond_1
    const v0, 0x7f0a01fb

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->showToast(I)V

    goto :goto_0

    .line 193
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->getEnableGPSPDialog()V

    goto :goto_0
.end method

.method public onFlowCommandRouteNop(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->onStartResume()V

    .line 200
    return-void
.end method

.method public onFlowCommandYes(Ljava/lang/String;)V
    .locals 2
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 245
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->isProviderEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 246
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->isGoogleMapsNavigation()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 248
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v0

    .line 247
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->hasGoogleAccount(Landroid/content/Context;)Z

    move-result v0

    .line 248
    if-eqz v0, :cond_1

    .line 250
    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mAddress:Ljava/lang/String;

    .line 249
    invoke-static {p0, v0, v1}, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->startNavigation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;Ljava/lang/String;)V

    .line 257
    :goto_0
    return-void

    .line 252
    :cond_1
    const v0, 0x7f0a01fb

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->showToast(I)V

    goto :goto_0

    .line 255
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->getEnableGPSPDialog()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 286
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onResume()V

    .line 289
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 230
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 231
    const-string/jumbo v0, "address"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mAddress:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    return-void
.end method

.method public quotesInButton(Z)V
    .locals 3
    .param p1, "isVisable"    # Z

    .prologue
    const v2, 0x7f0c00bc

    const v1, 0x7f0c00bb

    .line 340
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mBtnCancel:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setMarkShow(Z)V

    .line 341
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mBtnRoute:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setMarkShow(Z)V

    .line 343
    if-eqz p1, :cond_0

    .line 344
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mBtnCancel:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 345
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setButtonStyleOn(I)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mBtnRoute:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 347
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setButtonStyleOn(I)V

    .line 352
    :goto_0
    return-void

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mBtnCancel:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setButtonStyleOff(I)V

    .line 350
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mBtnRoute:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setButtonStyleOff(I)V

    goto :goto_0
.end method

.method protected setDayMode()V
    .locals 2

    .prologue
    .line 311
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->setDayMode()V

    .line 313
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    if-eqz v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setDayMode(Z)V

    .line 316
    :cond_0
    return-void
.end method

.method protected setNightMode()V
    .locals 2

    .prologue
    .line 320
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->setNightMode()V

    .line 322
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setNightMode(Z)V

    .line 325
    :cond_0
    return-void
.end method
