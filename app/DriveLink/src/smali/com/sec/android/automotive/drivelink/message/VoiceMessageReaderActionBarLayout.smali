.class public Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;
.super Landroid/widget/RelativeLayout;
.source "VoiceMessageReaderActionBarLayout.java"

# interfaces
.implements Lcom/nuance/drivelink/DLUiUpdater;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;,
        Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I


# instance fields
.field private mBackBtn:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private mCurrentMicState:Lcom/nuance/sample/MicState;

.field private mCurrentMode:I

.field private mIdleNum:I

.field private mIsPhraseSotting:Z

.field private mIsStartQEnded:Z

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mMicBtn:Landroid/widget/ImageView;

.field private mMicBtnEffect:Landroid/widget/ImageView;

.field private mOn2DepthBtnClickListener:Landroid/view/View$OnClickListener;

.field private mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

.field mReaderMicStateListener:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;

.field private mSearchBtn:Landroid/widget/ImageButton;

.field private mStartQImage:Landroid/widget/ImageView;

.field private mStartQImageEndRunnable:Ljava/lang/Runnable;

.field private mStartQImageHandler:Landroid/os/Handler;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 20
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 26
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mIsPhraseSotting:Z

    .line 45
    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mIdleNum:I

    .line 58
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->init(Landroid/content/Context;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mIsPhraseSotting:Z

    .line 45
    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mIdleNum:I

    .line 63
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->init(Landroid/content/Context;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 68
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mIsPhraseSotting:Z

    .line 45
    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mIdleNum:I

    .line 69
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->init(Landroid/content/Context;)V

    .line 70
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mContext:Landroid/content/Context;

    .line 74
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;->isListening()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mIsPhraseSotting:Z

    .line 75
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 77
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f0300d9

    .line 76
    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 79
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 80
    const v2, 0x7f090368

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 79
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    .line 121
    return-void
.end method


# virtual methods
.method public displayError(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 166
    return-void
.end method

.method public displaySystemTurn(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 186
    return-void
.end method

.method public displayUserTurn(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 172
    return-void
.end method

.method public displayWidgetContent(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 192
    return-void
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMicState()Lcom/nuance/sample/MicState;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    return-object v0
.end method

.method public handleUserCancel()V
    .locals 0

    .prologue
    .line 257
    return-void
.end method

.method public onClickable(Z)V
    .locals 0
    .param p1, "isClickable"    # Z

    .prologue
    .line 238
    return-void
.end method

.method public onDisplayMic(Z)V
    .locals 0
    .param p1, "isMicDisplayed"    # Z

    .prologue
    .line 284
    return-void
.end method

.method public onPhraseSpotterStateChanged(Z)V
    .locals 3
    .param p1, "isSpotting"    # Z

    .prologue
    .line 267
    const-string/jumbo v0, "UiUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " - PhraseSpotting : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 268
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 267
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mIsPhraseSotting:Z

    .line 278
    return-void
.end method

.method public registerStateListener(Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mReaderMicStateListener:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;

    .line 125
    return-void
.end method

.method public setCurrentMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 151
    return-void
.end method

.method public setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 246
    return-void
.end method

.method public setOnMicStateChangeListener(Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .prologue
    .line 232
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .line 233
    return-void
.end method

.method public updateMicRMSChange(I)V
    .locals 0
    .param p1, "rmsValue"    # I

    .prologue
    .line 251
    return-void
.end method

.method public updateMicState(Lcom/nuance/sample/MicState;)V
    .locals 3
    .param p1, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    .line 196
    const-string/jumbo v0, "UiUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    .line 199
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    if-nez v0, :cond_1

    .line 200
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 202
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mReaderMicStateListener:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mReaderMicStateListener:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;

    .line 204
    sget-object v1, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;->QUOTES_NONE:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;->OnStateChangeed(Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;)V

    goto :goto_0

    .line 209
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mReaderMicStateListener:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mReaderMicStateListener:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;

    .line 211
    sget-object v1, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;->QUOTES_ENABLE:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;->OnStateChangeed(Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;)V

    goto :goto_0

    .line 215
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mReaderMicStateListener:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mReaderMicStateListener:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;

    .line 217
    sget-object v1, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;->QUOTES_DISABLE:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;->OnStateChangeed(Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;)V

    goto :goto_0

    .line 221
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    invoke-interface {v0, p0, p1}, Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;->onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V

    goto :goto_0

    .line 200
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
