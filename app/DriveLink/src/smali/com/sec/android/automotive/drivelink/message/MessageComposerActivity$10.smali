.class Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;
.super Ljava/lang/Object;
.source "MessageComposerActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$OnComposerSipStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getComposerSipStateListener()Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$OnComposerSipStateListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$message$MessageComposerTextView$MessageComposerSIPState:[I


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$message$MessageComposerTextView$MessageComposerSIPState()[I
    .locals 3

    .prologue
    .line 846
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$message$MessageComposerTextView$MessageComposerSIPState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->values()[Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_CLOSE:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_SHOW:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$message$MessageComposerTextView$MessageComposerSIPState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    .line 846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnAbnormalSIP(Z)V
    .locals 4
    .param p1, "state"    # Z

    .prologue
    .line 928
    const/4 v0, 0x3

    const-string/jumbo v1, "OnAbnormalSIP"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "abnormalSIP : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$9(ILjava/lang/String;Ljava/lang/String;)V

    .line 930
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$20(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;Z)V

    .line 931
    return-void
.end method

.method public OnStateChanged(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;)V
    .locals 8
    .param p1, "state"    # Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x3

    const/4 v5, 0x0

    .line 851
    const-string/jumbo v2, "getComposerSipStateListener"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "OnStateChanged = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 852
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 851
    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V
    invoke-static {v6, v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$9(ILjava/lang/String;Ljava/lang/String;)V

    .line 853
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    invoke-static {v2, p1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$13(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;)V

    .line 855
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$message$MessageComposerTextView$MessageComposerSIPState()[I

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 923
    :goto_0
    return-void

    .line 858
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setUnknownSIP(Z)V

    .line 859
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mStartCancelBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setVisibility(I)V

    .line 860
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteCancellBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$14(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setVisibility(I)V

    .line 861
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteSendBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$15(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setVisibility(I)V

    .line 862
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteReplaceBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$16(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setVisibility(I)V

    goto :goto_0

    .line 866
    :pswitch_1
    const-string/jumbo v2, "getComposer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, " mDensity : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mDensity:F
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$17(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 867
    const-string/jumbo v4, " mOrientation : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mOrientation:I
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 866
    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V
    invoke-static {v6, v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$9(ILjava/lang/String;Ljava/lang/String;)V

    .line 868
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mOrientation:I
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)I

    move-result v3

    .line 869
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mDensity:F
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$17(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)F

    move-result v4

    .line 868
    invoke-virtual {v2, v3, p1, v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setContentAreaHeight(ILcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;F)V

    .line 870
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->hideVoiceLayout()V

    .line 871
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mStartCancelBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setVisibility(I)V

    .line 872
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteCancellBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$14(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setVisibility(I)V

    .line 873
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteSendBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$15(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setVisibility(I)V

    .line 874
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteReplaceBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$16(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 881
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setUnknownSIP(Z)V

    .line 882
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setSharedPreferences(Z)V
    invoke-static {v2, v5}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$19(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;Z)V

    .line 883
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getText()Ljava/lang/String;

    move-result-object v1

    .line 885
    .local v1, "strText":Ljava/lang/String;
    const-string/jumbo v2, "getComposer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, " mDensity : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mDensity:F
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$17(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 886
    const-string/jumbo v4, " mOrientation : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mOrientation:I
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 885
    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V
    invoke-static {v6, v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$9(ILjava/lang/String;Ljava/lang/String;)V

    .line 887
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mOrientation:I
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)I

    move-result v3

    .line 888
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mDensity:F
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$17(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)F

    move-result v4

    .line 887
    invoke-virtual {v2, v3, p1, v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setContentAreaHeight(ILcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;F)V

    .line 891
    const-string/jumbo v2, "getComposerSipStateListener"

    .line 892
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "OnStateChanged text = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 891
    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V
    invoke-static {v6, v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$9(ILjava/lang/String;Ljava/lang/String;)V

    .line 893
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 894
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->CompleteMessageFlow(Ljava/lang/String;)V

    .line 895
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    .line 896
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 895
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 897
    const-string/jumbo v2, "CM09"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 909
    :goto_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->showVoiceLayout()V

    .line 910
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v2

    .line 911
    const-wide/16 v3, 0x258

    invoke-virtual {v2, v3, v4}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    .line 913
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mStartCancelBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setVisibility(I)V

    .line 914
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteCancellBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$14(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setVisibility(I)V

    .line 915
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteSendBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$15(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setVisibility(I)V

    .line 916
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteReplaceBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$16(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 902
    :cond_0
    const-string/jumbo v2, "getComposerSipStateListener"

    .line 903
    const-string/jumbo v3, "OnStateChanged text = null "

    .line 902
    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V
    invoke-static {v6, v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$9(ILjava/lang/String;Ljava/lang/String;)V

    .line 905
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 907
    .local v0, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v2, "DM_SMS_COMPOSING"

    .line 906
    invoke-static {v2, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_1

    .line 855
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
