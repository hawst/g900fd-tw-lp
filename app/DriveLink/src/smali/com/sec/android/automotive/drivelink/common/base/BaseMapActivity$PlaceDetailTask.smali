.class public Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceDetailTask;
.super Landroid/os/AsyncTask;
.source "BaseMapActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PlaceDetailTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)V
    .locals 0

    .prologue
    .line 894
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceDetailTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    .locals 3
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 903
    const/4 v0, 0x0

    .line 905
    .local v0, "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    if-eqz p1, :cond_0

    array-length v1, p1

    if-nez v1, :cond_1

    .line 906
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceDetailTask;->cancel(Z)Z

    .line 910
    :goto_0
    return-object v0

    .line 908
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceDetailTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->placeService:Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$0(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;->details(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceDetailTask;->doInBackground([Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;)V
    .locals 5
    .param p1, "result"    # Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;

    .prologue
    .line 920
    if-eqz p1, :cond_1

    .line 922
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceDetailTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBalloonMarker:Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$1(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 923
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceDetailTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBalloonMarker:Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$1(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->removeMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V

    .line 924
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceDetailTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$2(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V

    .line 927
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->getLat()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->getLng()D

    move-result-wide v3

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setLocationPoint(DD)V

    .line 928
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    const v1, 0x3f19999a    # 0.6f

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setZoomLevel(F)V

    .line 929
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceDetailTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    sget-object v1, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->getLocationPoint()Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$3(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;)V

    .line 930
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceDetailTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->getLat()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->getLng()D

    move-result-wide v3

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->createMarks(DD)V

    .line 932
    :cond_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;

    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceDetailTask;->onPostExecute(Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;)V

    return-void
.end method
