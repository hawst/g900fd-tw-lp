.class Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$14;
.super Ljava/lang/Object;
.source "MusicPlayerActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->initView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    .line 859
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 862
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V
    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 863
    if-eq p2, v1, :cond_0

    .line 864
    const/4 v2, 0x2

    if-eq p2, v2, :cond_0

    .line 865
    const/16 v2, 0x75

    if-eq p2, v2, :cond_0

    .line 866
    const/16 v2, 0x76

    if-eq p2, v2, :cond_0

    .line 867
    const/16 v2, 0xfb

    if-eq p2, v2, :cond_0

    .line 868
    const/16 v2, 0xfc

    if-ne p2, v2, :cond_2

    :cond_0
    move v0, v1

    .line 882
    :cond_1
    :goto_0
    return v0

    .line 871
    :cond_2
    const/16 v1, 0x15

    if-eq p2, v1, :cond_3

    .line 872
    const/16 v1, 0x16

    if-ne p2, v1, :cond_4

    .line 873
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setProgressStopWithStarted(Z)V
    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$15(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 874
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mBTKProc:Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$38(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDurationBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$39(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    move-result-object v2

    invoke-virtual {v1, v2, p2, p3}, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;->proc(Landroid/widget/SeekBar;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 875
    .local v0, "ret":Z
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 876
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    .line 877
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v2

    .line 876
    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setProgressStopWithStarted(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$15(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    goto :goto_0

    .line 880
    .end local v0    # "ret":Z
    :cond_4
    const-string/jumbo v1, "Music Seekbar"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "keycode event not consumed - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 881
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 880
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
