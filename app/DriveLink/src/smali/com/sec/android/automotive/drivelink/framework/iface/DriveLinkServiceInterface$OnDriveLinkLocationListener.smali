.class public interface abstract Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
.super Ljava/lang/Object;
.source "DriveLinkServiceInterface.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnDriveLinkLocationListener"
.end annotation


# virtual methods
.method public abstract onNotifySmartAlert(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;Ljava/lang/String;)V
.end method

.method public abstract onResponseCreateUserProfile(Z)V
.end method

.method public abstract onResponseRemoveGroupShared(Z)V
.end method

.method public abstract onResponseRequestAcceptLocationShared(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V
.end method

.method public abstract onResponseRequestAcceptShareMyLocation(Z)V
.end method

.method public abstract onResponseRequestAddFriendsToGroup(ZLjava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onResponseRequestAllParticipantTracking(Ljava/util/HashMap;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;>;)V"
        }
    .end annotation
.end method

.method public abstract onResponseRequestChangeGroupDestination(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
.end method

.method public abstract onResponseRequestCreateGroupShare(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
.end method

.method public abstract onResponseRequestFriendLocationShare(Ljava/lang/Exception;Z)V
.end method

.method public abstract onResponseRequestGroupSharedUpdateInfo(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
.end method

.method public abstract onResponseRequestQuitFromGroup(Z)V
.end method

.method public abstract onResponseRequestRecommendedLocationList(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onResponseRequestReinviteFriendToGroup(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V
.end method

.method public abstract onResponseRequestRestartGroupShared(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
.end method

.method public abstract onResponseRequestSetMyLocation(Z)V
.end method

.method public abstract onResponseRequestShareMyLocation(Z)V
.end method

.method public abstract onResponseRequestUpdateParticipantStatus(Z)V
.end method
