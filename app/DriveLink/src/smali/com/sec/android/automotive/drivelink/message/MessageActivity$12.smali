.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$12;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity;->initSearchTextField()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 752
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 753
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$47(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 755
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$47(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 757
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$47(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    .line 758
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$47(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 760
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$47(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 761
    return-void
.end method
