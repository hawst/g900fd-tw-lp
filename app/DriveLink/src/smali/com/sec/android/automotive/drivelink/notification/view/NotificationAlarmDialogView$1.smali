.class Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;
.super Ljava/lang/Object;
.source "NotificationAlarmDialogView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;

    .line 215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 221
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mButton1:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 222
    const-string/jumbo v1, "[NotificationAlarmView]"

    const-string/jumbo v2, "Stop Button Clicked."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->hasNotiItemInfo()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 224
    const-class v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    .line 225
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v2

    .line 224
    invoke-virtual {v1, v2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    .line 225
    if-eqz v1, :cond_0

    .line 226
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v1

    .line 227
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    .line 226
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;

    .line 228
    .local v0, "alarmInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;->getAlarmId()I

    move-result v2

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->alarmStop(IZ)V
    invoke-static {v1, v2, v4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->access$0(Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;IZ)V

    .line 229
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;

    iput-boolean v4, v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->bButtonClicked:Z

    .line 231
    .end local v0    # "alarmInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->dismiss()V

    .line 249
    :goto_0
    return-void

    .line 232
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mButton2:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 233
    const-string/jumbo v1, "[NotificationAlarmView]"

    const-string/jumbo v2, "Snooze Button Clicked."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->hasNotiItemInfo()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 235
    const-class v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    .line 236
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v2

    .line 235
    invoke-virtual {v1, v2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    .line 236
    if-eqz v1, :cond_2

    .line 237
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v1

    .line 238
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    .line 237
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;

    .line 239
    .restart local v0    # "alarmInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;->getSNZActive()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 240
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;->getAlarmId()I

    move-result v2

    const/4 v3, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->alarmStop(IZ)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->access$0(Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;IZ)V

    .line 241
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;

    iput-boolean v4, v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->bButtonClicked:Z

    .line 244
    .end local v0    # "alarmInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->dismiss()V

    goto :goto_0

    .line 246
    :cond_3
    const-string/jumbo v1, "[NotificationAlarmView]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Click Event from Unhandled View:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
