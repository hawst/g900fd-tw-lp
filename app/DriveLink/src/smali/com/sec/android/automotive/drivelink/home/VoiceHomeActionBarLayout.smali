.class public Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;
.super Landroid/widget/RelativeLayout;
.source "VoiceHomeActionBarLayout.java"

# interfaces
.implements Lcom/nuance/drivelink/DLUiUpdater;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$SineEaseOut;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I

.field private static final SINE_OUT:Landroid/view/animation/Interpolator;


# instance fields
.field private isRestore:Z

.field private mAnimator:Landroid/view/ViewPropertyAnimator;

.field private mBgView:Landroid/widget/ImageView;

.field private mCurrentMicState:Lcom/nuance/sample/MicState;

.field private mErrorCharSequence:Ljava/lang/CharSequence;

.field private mHiText:Landroid/widget/TextView;

.field private mIsErrorState:Z

.field private mIsMicDisplayed:Z

.field private mIsPhraseSotting:Z

.field private mIsStartQEnded:Z

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mListeningText:Landroid/widget/TextView;

.field private mListeningTextLayout:Landroid/widget/LinearLayout;

.field private mLocale:Ljava/util/Locale;

.field private mMicBtn:Landroid/widget/ImageView;

.field private mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

.field private mMicLayout:Landroid/widget/RelativeLayout;

.field private mMicProcessBg:Landroid/widget/ImageView;

.field private mMicStartQBg:Landroid/widget/ImageView;

.field private mMoreBtn:Landroid/widget/ImageButton;

.field private mNoNetworkLayout:Landroid/widget/LinearLayout;

.field private mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

.field private mPhraseSpotterHandler:Landroid/os/Handler;

.field private mProcessText:Landroid/widget/TextView;

.field private mQuoteEnd:Landroid/widget/ImageView;

.field private mQuoteStart:Landroid/widget/ImageView;

.field private mRatio:F

.field private mSampleCommandList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSayText:Landroid/widget/TextView;

.field private mStandByLayout:Landroid/widget/LinearLayout;

.field private mStartQAni:Landroid/view/animation/Animation;

.field private mSubQuoteEnd:Landroid/widget/ImageView;

.field private mSubQuoteStart:Landroid/widget/ImageView;

.field private mTTSText:Landroid/widget/TextView;

.field private mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 407
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$SineEaseOut;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$SineEaseOut;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 49
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsPhraseSotting:Z

    .line 409
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mRatio:F

    .line 411
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->isRestore:Z

    .line 561
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    .line 90
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->init(Landroid/content/Context;)V

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsPhraseSotting:Z

    .line 409
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mRatio:F

    .line 411
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->isRestore:Z

    .line 561
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    .line 96
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->init(Landroid/content/Context;)V

    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 101
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsPhraseSotting:Z

    .line 409
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mRatio:F

    .line 411
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->isRestore:Z

    .line 561
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    .line 103
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->init(Landroid/content/Context;)V

    .line 104
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsMicDisplayed:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$10()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 407
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;FJLandroid/view/animation/Interpolator;)V
    .locals 0

    .prologue
    .line 413
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->startScaleAnimation(FJLandroid/view/animation/Interpolator;)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;)Lcom/sec/android/automotive/drivelink/common/view/ListeningView;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsStartQEnded:Z

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;)Z
    .locals 1

    .prologue
    .line 411
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->isRestore:Z

    return v0
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 411
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->isRestore:Z

    return-void
.end method

.method private getSampleCommand()Ljava/lang/String;
    .locals 5

    .prologue
    .line 580
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mSampleCommandList:Ljava/util/ArrayList;

    .line 581
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    int-to-long v3, v3

    .line 580
    rem-long/2addr v1, v3

    long-to-int v0, v1

    .line 582
    .local v0, "randomIndex":I
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mSampleCommandList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    return-object v1
.end method

.method private init(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 107
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 108
    .local v0, "inflater":Landroid/view/LayoutInflater;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;->isListening()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsPhraseSotting:Z

    .line 111
    const v1, 0x7f0300d4

    .line 110
    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 112
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090315

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mBgView:Landroid/widget/ImageView;

    .line 115
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f09032a

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMoreBtn:Landroid/widget/ImageButton;

    .line 119
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 120
    const v2, 0x7f090317

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 119
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    .line 121
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 122
    const v2, 0x7f09031c

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 121
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    .line 123
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f09031e

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTTSText:Landroid/widget/TextView;

    .line 124
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 125
    const v2, 0x7f09031f

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 124
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    .line 126
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 127
    const v2, 0x7f090325

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 126
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mListeningTextLayout:Landroid/widget/LinearLayout;

    .line 128
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090329

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mProcessText:Landroid/widget/TextView;

    .line 130
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090323

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    .line 131
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090324

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    .line 132
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090320

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    .line 133
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 134
    const v2, 0x7f090321

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 133
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    .line 135
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 136
    const v2, 0x7f090322

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 135
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    .line 139
    const v1, 0x7f04001e

    .line 138
    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    .line 140
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    new-instance v2, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$2;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$2;-><init>(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 158
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/nuance/sample/OnClickMicListenerImpl;

    .line 159
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    invoke-direct {v2, v3, v4}, Lcom/nuance/sample/OnClickMicListenerImpl;-><init>(Lcom/nuance/sample/MicStateMaster;Landroid/view/View;)V

    .line 158
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090318

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mSayText:Landroid/widget/TextView;

    .line 162
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f09031a

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mHiText:Landroid/widget/TextView;

    .line 163
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 164
    const v2, 0x7f090327

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 163
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mListeningText:Landroid/widget/TextView;

    .line 165
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090319

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mQuoteStart:Landroid/widget/ImageView;

    .line 166
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f09031b

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mQuoteEnd:Landroid/widget/ImageView;

    .line 167
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 168
    const v2, 0x7f090326

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 167
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mSubQuoteStart:Landroid/widget/ImageView;

    .line 169
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 170
    const v2, 0x7f090328

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 169
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mSubQuoteEnd:Landroid/widget/ImageView;

    .line 176
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mHiText:Landroid/widget/TextView;

    .line 177
    const-string/jumbo v2, "/system/fonts/Cooljazz.ttf"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->createTypefaceFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    .line 176
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 179
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mSampleCommandList:Ljava/util/ArrayList;

    .line 181
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->setHasInternetLayout(Z)V

    .line 191
    return-void
.end method

.method private startScaleAnimation(FJLandroid/view/animation/Interpolator;)V
    .locals 2
    .param p1, "ratio"    # F
    .param p2, "duration"    # J
    .param p4, "interpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 416
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    if-nez v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setScaleX(F)V

    .line 418
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setScaleY(F)V

    .line 420
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    .line 421
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$3;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$3;-><init>(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 449
    :cond_0
    iget v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mRatio:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 457
    :goto_0
    return-void

    .line 452
    :cond_1
    iput p1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mRatio:F

    .line 454
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 455
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 456
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method


# virtual methods
.method public displayError(Ljava/lang/CharSequence;)V
    .locals 4
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 212
    const-string/jumbo v0, "VoiceHomeActionBar"

    const-string/jumbo v1, "-----------> displayError"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 215
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mErrorCharSequence:Ljava/lang/CharSequence;

    .line 218
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsErrorState:Z

    .line 219
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsStartQEnded:Z

    .line 220
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 221
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 222
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mListeningTextLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 226
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 229
    return-void
.end method

.method public displaySystemTurn(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 250
    return-void
.end method

.method public displayUserTurn(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 235
    return-void
.end method

.method public displayWidgetContent(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 256
    return-void
.end method

.method protected getErrorCharSequence()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mErrorCharSequence:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 1

    .prologue
    .line 487
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMicState()Lcom/nuance/sample/MicState;
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    return-object v0
.end method

.method public handleUserCancel()V
    .locals 0

    .prologue
    .line 482
    return-void
.end method

.method protected isErrorState()Z
    .locals 1

    .prologue
    .line 375
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsErrorState:Z

    return v0
.end method

.method public onClickable(Z)V
    .locals 1
    .param p1, "isClickable"    # Z

    .prologue
    .line 367
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 368
    return-void
.end method

.method public onDisplayMic(Z)V
    .locals 5
    .param p1, "isMicDisplayed"    # Z

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 650
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsMicDisplayed:Z

    .line 655
    const-string/jumbo v0, "VoiceHomeActionBarLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "JINSEIL onDisplayMic "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 657
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v0

    if-nez v0, :cond_1

    .line 658
    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->setHasInternetLayout(Z)V

    .line 699
    :cond_0
    :goto_0
    return-void

    .line 663
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    if-ne v0, v1, :cond_0

    .line 664
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsMicDisplayed:Z

    if-eqz v0, :cond_2

    .line 665
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsErrorState:Z

    .line 666
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsStartQEnded:Z

    .line 667
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 668
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 670
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 671
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 672
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 673
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mListeningTextLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 674
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 675
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 677
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 678
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 679
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 680
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 681
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 682
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02038c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 684
    :cond_2
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsErrorState:Z

    .line 685
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsStartQEnded:Z

    .line 686
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 687
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 689
    const-string/jumbo v0, "voicehome"

    const-string/jumbo v1, "------------> visibility visible 2"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 690
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 691
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 692
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 693
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 694
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mListeningTextLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 695
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 696
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public onPhraseSpotterStateChanged(Z)V
    .locals 3
    .param p1, "isSpotting"    # Z

    .prologue
    .line 549
    const-string/jumbo v0, "UiUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " - PhraseSpotting : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 550
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 549
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsPhraseSotting:Z

    .line 559
    return-void
.end method

.method protected setDayMode()V
    .locals 3

    .prologue
    .line 534
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mBgView:Landroid/widget/ImageView;

    const v1, 0x7f020001

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 535
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mSayText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080031

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 536
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mHiText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080033

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 537
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 538
    const v2, 0x7f08004b

    .line 537
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 539
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 540
    const v2, 0x7f080037

    .line 539
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 541
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mQuoteStart:Landroid/widget/ImageView;

    const v1, 0x7f02035b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 542
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mQuoteEnd:Landroid/widget/ImageView;

    const v1, 0x7f020359

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 543
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mSubQuoteStart:Landroid/widget/ImageView;

    const v1, 0x7f020361

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 544
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mSubQuoteEnd:Landroid/widget/ImageView;

    const v1, 0x7f02035f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 545
    return-void
.end method

.method public setHasInternetLayout(Z)V
    .locals 5
    .param p1, "hasInternet"    # Z

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 195
    const-string/jumbo v0, "VoiceHomeActionBarLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "JINSEIL hasInternet="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    if-eqz p1, :cond_0

    .line 198
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 199
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 207
    :goto_0
    return-void

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 205
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected setMicDisplayed(Z)V
    .locals 3
    .param p1, "isMicDisplayed"    # Z

    .prologue
    .line 702
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsMicDisplayed:Z

    .line 703
    const-string/jumbo v0, "VoiceHomeActionBarLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "JINSEIL setMicDisplayed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    return-void
.end method

.method protected setNightMode()V
    .locals 3

    .prologue
    .line 518
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mBgView:Landroid/widget/ImageView;

    const v1, 0x7f08002e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 519
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mSayText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 520
    const v2, 0x7f080032

    .line 519
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 521
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mHiText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 522
    const v2, 0x7f080034

    .line 521
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 523
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 524
    const v2, 0x7f08004c

    .line 523
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 525
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 526
    const v2, 0x7f080038

    .line 525
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 527
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mQuoteStart:Landroid/widget/ImageView;

    const v1, 0x7f02035c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 528
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mQuoteEnd:Landroid/widget/ImageView;

    const v1, 0x7f02035a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 529
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mSubQuoteStart:Landroid/widget/ImageView;

    const v1, 0x7f020362

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 530
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mSubQuoteEnd:Landroid/widget/ImageView;

    const v1, 0x7f020360

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 531
    return-void
.end method

.method public setOnMicStateChangeListener(Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .prologue
    .line 362
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .line 363
    return-void
.end method

.method public setOnMoreBtnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 371
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMoreBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 372
    return-void
.end method

.method public updateMicRMSChange(I)V
    .locals 1
    .param p1, "rmsValue"    # I

    .prologue
    .line 387
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsStartQEnded:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    if-eqz v0, :cond_0

    .line 388
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->update(I)V

    .line 390
    :cond_0
    return-void
.end method

.method public updateMicState(Lcom/nuance/sample/MicState;)V
    .locals 5
    .param p1, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 260
    const-string/jumbo v0, "UiUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    .line 263
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    if-nez v0, :cond_2

    .line 264
    const-string/jumbo v0, "VoiceHomeActionBarLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "JINSEIL updateMicState "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsMicDisplayed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 353
    :goto_0
    return-void

    .line 267
    :pswitch_0
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsErrorState:Z

    .line 268
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsStartQEnded:Z

    .line 269
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 270
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 279
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 281
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsMicDisplayed:Z

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 283
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 285
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 286
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02038c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 303
    :goto_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mListeningTextLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 305
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 306
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    goto :goto_0

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 293
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x2bc

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 298
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 299
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1

    .line 310
    :pswitch_1
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsErrorState:Z

    .line 311
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 312
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->getSampleCommand()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 315
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 317
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mListeningTextLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 320
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 321
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 322
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 323
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 324
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 326
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020385

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 327
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020388

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 333
    :pswitch_2
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsErrorState:Z

    .line 334
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsStartQEnded:Z

    .line 335
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 336
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 337
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mListeningTextLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 338
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 339
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 341
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 342
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 343
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 345
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->play(I)V

    .line 347
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 351
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    invoke-interface {v0, p0, p1}, Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;->onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V

    goto/16 :goto_0

    .line 265
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected updateSampleCommandList()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 586
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v6

    invoke-virtual {v6}, Lcom/nuance/drivelink/DLAppUiUpdater;->getVoiceLocale()Ljava/util/Locale;

    move-result-object v3

    .line 588
    .local v3, "locale":Ljava/util/Locale;
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLocale:Ljava/util/Locale;

    if-nez v6, :cond_5

    const/4 v1, 0x0

    .line 589
    .local v1, "language":Ljava/lang/String;
    :goto_0
    if-nez v1, :cond_0

    .line 591
    const-string/jumbo v1, ""

    .line 594
    :cond_0
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLocale:Ljava/util/Locale;

    if-eqz v6, :cond_1

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 595
    :cond_1
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLocale:Ljava/util/Locale;

    .line 596
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mSampleCommandList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 598
    const/16 v2, 0x16

    .line 600
    .local v2, "length":I
    sget-object v6, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 601
    const/16 v2, 0xf

    .line 608
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 609
    const v7, 0x7f07000d

    .line 608
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    array-length v8, v7

    move v6, v5

    :goto_2
    if-lt v6, v8, :cond_8

    .line 615
    const-string/jumbo v6, "ro.csc.sales_code"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 616
    .local v4, "salesCode":Ljava/lang/String;
    const-string/jumbo v6, "KDI"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 617
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 618
    const v7, 0x7f07001b

    .line 617
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    array-length v8, v7

    move v6, v5

    :goto_3
    if-lt v6, v8, :cond_a

    .line 632
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 633
    const v7, 0x7f07000f

    .line 632
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    array-length v8, v7

    move v6, v5

    :goto_4
    if-lt v6, v8, :cond_e

    .line 639
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 640
    const v7, 0x7f070010

    .line 639
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    :goto_5
    if-lt v5, v7, :cond_10

    .line 646
    .end local v2    # "length":I
    .end local v4    # "salesCode":Ljava/lang/String;
    :cond_4
    return-void

    .line 588
    .end local v1    # "language":Ljava/lang/String;
    :cond_5
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLocale:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 602
    .restart local v1    # "language":Ljava/lang/String;
    .restart local v2    # "length":I
    :cond_6
    sget-object v6, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 603
    const/16 v2, 0xc

    .line 604
    goto :goto_1

    :cond_7
    const-string/jumbo v6, "ru"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 605
    const/16 v2, 0x14

    goto :goto_1

    .line 608
    :cond_8
    aget-object v0, v7, v6

    .line 610
    .local v0, "command":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v9, v2, :cond_9

    .line 611
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mSampleCommandList:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 608
    :cond_9
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 617
    .end local v0    # "command":Ljava/lang/String;
    .restart local v4    # "salesCode":Ljava/lang/String;
    :cond_a
    aget-object v0, v7, v6

    .line 619
    .restart local v0    # "command":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v9, v2, :cond_b

    .line 620
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mSampleCommandList:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 617
    :cond_b
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 624
    .end local v0    # "command":Ljava/lang/String;
    :cond_c
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 625
    const v7, 0x7f07000e

    .line 624
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    array-length v8, v7

    move v6, v5

    :goto_6
    if-ge v6, v8, :cond_3

    aget-object v0, v7, v6

    .line 626
    .restart local v0    # "command":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v9, v2, :cond_d

    .line 627
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mSampleCommandList:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 624
    :cond_d
    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    .line 632
    .end local v0    # "command":Ljava/lang/String;
    :cond_e
    aget-object v0, v7, v6

    .line 634
    .restart local v0    # "command":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v9, v2, :cond_f

    .line 635
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mSampleCommandList:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 632
    :cond_f
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_4

    .line 639
    .end local v0    # "command":Ljava/lang/String;
    :cond_10
    aget-object v0, v6, v5

    .line 641
    .restart local v0    # "command":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v8, v2, :cond_11

    .line 642
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mSampleCommandList:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 639
    :cond_11
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_5
.end method
