.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestLocationRoute;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestLocationRoute.java"


# instance fields
.field private endLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

.field private numAlternates:I

.field private startLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

.field private tolerance:I

.field private wayLocations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V
    .locals 0
    .param p1, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 19
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestLocationRoute;->wayLocations:Ljava/util/List;

    .line 54
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestLocationRoute;->startLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 55
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestLocationRoute;->endLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 56
    return-void
.end method

.method protected process()V
    .locals 0

    .prologue
    .line 25
    return-void
.end method

.method protected response()V
    .locals 0

    .prologue
    .line 29
    return-void
.end method

.method public setEndLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 0
    .param p1, "endLocation"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestLocationRoute;->endLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 37
    return-void
.end method

.method public setNumAlternates(I)V
    .locals 0
    .param p1, "numAlternates"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestLocationRoute;->numAlternates:I

    .line 41
    return-void
.end method

.method public setStartLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 0
    .param p1, "startLocation"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestLocationRoute;->startLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 33
    return-void
.end method

.method public setTolerance(I)V
    .locals 0
    .param p1, "tolerance"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestLocationRoute;->tolerance:I

    .line 45
    return-void
.end method

.method public setWayLocations(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "wayLocations":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestLocationRoute;->wayLocations:Ljava/util/List;

    .line 49
    return-void
.end method
