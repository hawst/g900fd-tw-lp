.class public Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
.super Ljava/lang/Object;
.source "LocationMarkerItem.java"


# static fields
.field private static SEQUENCE:I


# instance fields
.field private final id:I

.field private labelDescription:Ljava/lang/String;

.field private mAnchor:Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;

.field private mIcon:Landroid/graphics/Bitmap;

.field private mPosition:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

.field private mResourceId:I

.field private snippet:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->SEQUENCE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mResourceId:I

    .line 21
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;

    invoke-direct {v0, v1, v1}, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mAnchor:Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;

    .line 22
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->snippet:Ljava/lang/String;

    .line 23
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->labelDescription:Ljava/lang/String;

    .line 28
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->generateId()I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->id:I

    .line 29
    return-void
.end method

.method public constructor <init>(DD)V
    .locals 2
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mResourceId:I

    .line 21
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;

    invoke-direct {v0, v1, v1}, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mAnchor:Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;

    .line 22
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->snippet:Ljava/lang/String;

    .line 23
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->labelDescription:Ljava/lang/String;

    .line 32
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->generateId()I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->id:I

    .line 33
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;-><init>(DD)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mPosition:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .line 34
    return-void
.end method

.method public constructor <init>(DDI)V
    .locals 2
    .param p1, "lat"    # D
    .param p3, "lng"    # D
    .param p5, "resourceId"    # I

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mResourceId:I

    .line 21
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;

    invoke-direct {v0, v1, v1}, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mAnchor:Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;

    .line 22
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->snippet:Ljava/lang/String;

    .line 23
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->labelDescription:Ljava/lang/String;

    .line 37
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->generateId()I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->id:I

    .line 38
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;-><init>(DD)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mPosition:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .line 39
    iput p5, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mResourceId:I

    .line 40
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;I)V
    .locals 2
    .param p1, "position"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    .param p2, "resourceId"    # I

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mResourceId:I

    .line 21
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;

    invoke-direct {v0, v1, v1}, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mAnchor:Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;

    .line 22
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->snippet:Ljava/lang/String;

    .line 23
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->labelDescription:Ljava/lang/String;

    .line 43
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->generateId()I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->id:I

    .line 44
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mPosition:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .line 45
    iput p2, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mResourceId:I

    .line 46
    return-void
.end method

.method private generateId()I
    .locals 2

    .prologue
    .line 152
    sget v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->SEQUENCE:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->SEQUENCE:I

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 165
    if-nez p1, :cond_1

    .line 171
    :cond_0
    :goto_0
    return v1

    .line 167
    :cond_1
    instance-of v2, p1, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 170
    check-cast v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .line 171
    .local v0, "other":Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    iget v2, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->id:I

    iget v3, v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->id:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getAnchor()Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mAnchor:Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;

    return-object v0
.end method

.method public getIcon()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mIcon:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->id:I

    return v0
.end method

.method public getLabelDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->labelDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mPosition:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mPosition:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public getPosition()Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mPosition:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    return-object v0
.end method

.method public getResourceId()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mResourceId:I

    return v0
.end method

.method public getSnippet()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->snippet:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->id:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    return v0
.end method

.method public setAnchor(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 131
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mAnchor:Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;

    .line 132
    return-void
.end method

.method public setIcon(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mIcon:Landroid/graphics/Bitmap;

    .line 115
    return-void
.end method

.method public setLabelDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "labelDescription"    # Ljava/lang/String;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->labelDescription:Ljava/lang/String;

    .line 161
    return-void
.end method

.method public setPosition(Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;)V
    .locals 0
    .param p1, "position"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mPosition:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .line 67
    return-void
.end method

.method public setResourceId(I)V
    .locals 0
    .param p1, "resourceId"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->mResourceId:I

    .line 98
    return-void
.end method

.method public setSnippet(Ljava/lang/String;)V
    .locals 0
    .param p1, "snippet"    # Ljava/lang/String;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->snippet:Ljava/lang/String;

    .line 149
    return-void
.end method
