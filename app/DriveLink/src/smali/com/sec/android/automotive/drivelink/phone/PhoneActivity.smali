.class public Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;
.source "PhoneActivity.java"


# static fields
.field public static final CONTACT_CHOICE:Ljava/lang/String; = "CONTACT_CHOICE"

.field public static final DELAY_TIME_FOR_AUTO_SHRINK:J = 0x2710L

.field private static final ERROR_STRING:Ljava/lang/String; = "ERROR_STRING"

.field public static final INTENT_NAME_CONTACT_ID:Ljava/lang/String; = "contactId"

.field public static final INTENT_NAME_USER_BMP:Ljava/lang/String; = "userbitmap"

.field public static final INTENT_NAME_USER_DISPLAY_NAME:Ljava/lang/String; = "userdisplayname"

.field public static final INTENT_NAME_USER_INDEX:Ljava/lang/String; = "userindex"

.field public static final INTENT_NAME_USER_PHONE_NUM:Ljava/lang/String; = "userphonenum"

.field protected static final INTENT_NAME_USER_PHONE_NUM_SIZE:Ljava/lang/String; = "userphonenumsize"

.field public static final INTENT_NAME_USER_PHONE_TYPE:Ljava/lang/String; = "userphonetype"

.field private static final IS_ERROR_STATE:Ljava/lang/String; = "IS_ERROR_STATE"

.field private static final IS_TTS_STATE:Ljava/lang/String; = "IS_TTS_STATE"

.field public static final LOGS_MODE:I = 0x20

.field public static final MSG_AUTO_SHRINK:I = 0x0

.field public static final MULTI_SEARCH_MODE:I = 0x42

.field public static final NO_RESULT_SEARCH_MODE:I = 0x43

.field public static final NUMBER_MODE:I = 0x80

.field public static final PHONE_MODE:Ljava/lang/String; = "PHONE_MODE"

.field public static final PHONE_PREMODE:Ljava/lang/String; = "PHONE_PREMODE"

.field public static final SEARCH_MODE:I = 0x40

.field public static final SINGLE_SEARCH_MODE:I = 0x41

.field public static final SIP_OPEN_MODE:Ljava/lang/String; = "SIP_OPEN_MODE"

.field public static final SIP_SEARCH_MODE:Ljava/lang/String; = "SIP_SEARCH_MODE"

.field public static final SIP_SEARCH_RESULT:Ljava/lang/String; = "SIP_SEARCH_RESULT"

.field public static final SIP_SEARCH_RESULT_TEXT:Ljava/lang/String; = "SIP_SEARCH_RESULT_TEXT"

.field public static final SIP_SEARCH_TEXT:Ljava/lang/String; = "SIP_SEARCH_TEXT"

.field public static final SUGGESTIONS_MODE:I = 0x10

.field public static final TAG:Ljava/lang/String; = "PhoneActivity"


# instance fields
.field private isLogClicked:Z

.field private isVideoCallRequested:Z

.field private mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

.field private mContactChoices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field

.field private mContactList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation
.end field

.field private mContactMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field private mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

.field private mCurrentMode:I

.field mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

.field private mDrivingMode:Z

.field private mErrorStr:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mIsError:Z

.field private mListLayout:Landroid/widget/LinearLayout;

.field private mListTitle:Landroid/widget/TextView;

.field private mLogTTS:Z

.field private mLogsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;",
            ">;"
        }
    .end annotation
.end field

.field private mNightMode:Z

.field private mNoListLayout:Landroid/widget/LinearLayout;

.field private mNolistText:Landroid/widget/TextView;

.field private mNumberContactId:J

.field private mNumberDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

.field private mNumberIsLoaded:Z

.field private mNumberListLayout:Landroid/widget/LinearLayout;

.field private mNumberPageAdapter:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;

.field private mNumberPageNavigationLayout:Landroid/widget/RelativeLayout;

.field private mNumberUserContactImage:Landroid/graphics/Bitmap;

.field private mNumberUserDisplayName:Ljava/lang/String;

.field private mNumberUserIndex:I

.field private mNumberUserPhoneNumbers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;"
        }
    .end annotation
.end field

.field private mNumberViewPager:Landroid/support/v4/view/ViewPager;

.field mOnUpdateLogListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;

.field mOnUpdateSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;

.field private mPrevStateOfSearchMode:I

.field private mPreviousMode:I

.field private mSIPState:Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;

.field private mSearchBtn:Landroid/widget/ImageButton;

.field private mSearchClearBtn:Landroid/widget/ImageButton;

.field private mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

.field private mSearchListLayout:Landroid/widget/LinearLayout;

.field private mSearchMode:Z

.field private mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

.field private mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;

.field private mSearchResultText:Landroid/widget/TextView;

.field private mSearchText:Landroid/widget/EditText;

.field private mSearchViewPager:Landroid/support/v4/view/ViewPager;

.field private mSearchedResult:Ljava/lang/String;

.field private mSearchedResultTitle:Ljava/lang/String;

.field private mSearchedText:Ljava/lang/String;

.field private mSipOpenMode:Z

.field private mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

.field private mSuggestionsListLayout:Landroid/widget/LinearLayout;

.field private mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;

.field private mSuggestionsPageNavigationLayout:Landroid/widget/RelativeLayout;

.field private mSuggestionsViewPager:Landroid/support/v4/view/ViewPager;

.field private mTextfieldLayout:Landroid/widget/FrameLayout;

.field private mToDial:Z

.field private mTvSearchContact:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 85
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;-><init>()V

    .line 172
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberIsLoaded:Z

    .line 178
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mLogTTS:Z

    .line 179
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->isLogClicked:Z

    .line 180
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->isVideoCallRequested:Z

    .line 194
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSipOpenMode:Z

    .line 195
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchMode:Z

    .line 196
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedText:Ljava/lang/String;

    .line 197
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedResult:Ljava/lang/String;

    .line 198
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedResultTitle:Ljava/lang/String;

    .line 199
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mToDial:Z

    .line 201
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mIsError:Z

    .line 202
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mErrorStr:Ljava/lang/String;

    .line 204
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mDrivingMode:Z

    .line 205
    sget-object v0, Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;

    .line 207
    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mPrevStateOfSearchMode:I

    .line 209
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNightMode:Z

    .line 437
    new-instance v0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mHandler:Landroid/os/Handler;

    .line 1247
    new-instance v0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mOnUpdateSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;

    .line 1268
    new-instance v0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mOnUpdateLogListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;

    .line 2569
    new-instance v0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    .line 85
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V
    .locals 0

    .prologue
    .line 178
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mLogTTS:Z

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V
    .locals 0

    .prologue
    .line 129
    iput p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mPreviousMode:I

    return-void
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mPreviousMode:I

    return v0
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V
    .locals 0

    .prologue
    .line 128
    iput p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V
    .locals 0

    .prologue
    .line 966
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setCurrentMode(I)V

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    return-object v0
.end method

.method static synthetic access$16(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V
    .locals 0

    .prologue
    .line 179
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->isLogClicked:Z

    return-void
.end method

.method static synthetic access$17(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V
    .locals 0

    .prologue
    .line 204
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mDrivingMode:Z

    return-void
.end method

.method static synthetic access$18(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$19(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V
    .locals 0

    .prologue
    .line 195
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchMode:Z

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V
    .locals 0

    .prologue
    .line 194
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSipOpenMode:Z

    return-void
.end method

.method static synthetic access$21(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedText:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$22(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedResult:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$24(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V
    .locals 0

    .prologue
    .line 421
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setAutoShrink(Z)V

    return-void
.end method

.method static synthetic access$25(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V
    .locals 0

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->restorePrevFlow()V

    return-void
.end method

.method static synthetic access$26(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V
    .locals 0

    .prologue
    .line 399
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->handleShrinkMessageOnMicState(I)V

    return-void
.end method

.method static synthetic access$27(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;)V
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;

    return-void
.end method

.method static synthetic access$28(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchClearBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$29(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mTvSearchContact:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V
    .locals 0

    .prologue
    .line 1332
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->showSuggestionList()V

    return-void
.end method

.method static synthetic access$30(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    return-void
.end method

.method static synthetic access$31(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    return-object v0
.end method

.method static synthetic access$32(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;

    return-object v0
.end method

.method static synthetic access$33(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$34(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    return-object v0
.end method

.method static synthetic access$35(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    return-object v0
.end method

.method static synthetic access$36(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$37(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    return-object v0
.end method

.method static synthetic access$38(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberPageAdapter:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;

    return-object v0
.end method

.method static synthetic access$39(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Z
    .locals 1

    .prologue
    .line 172
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberIsLoaded:Z

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    return v0
.end method

.method static synthetic access$40(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V
    .locals 0

    .prologue
    .line 172
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberIsLoaded:Z

    return-void
.end method

.method static synthetic access$41(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$42(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)J
    .locals 2

    .prologue
    .line 167
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberContactId:J

    return-wide v0
.end method

.method static synthetic access$43(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$44(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$45(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserIndex:I

    return v0
.end method

.method static synthetic access$46(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;ILcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;I)V
    .locals 0

    .prologue
    .line 2027
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->startPrepareDialActivity(ILcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;I)V

    return-void
.end method

.method static synthetic access$47(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)I
    .locals 1

    .prologue
    .line 1685
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getMainPhoneNumber(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)I

    move-result v0

    return v0
.end method

.method static synthetic access$48(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Ljava/util/List;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$49(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Lcom/vlingo/core/internal/contacts/ContactMatch;I)I
    .locals 1

    .prologue
    .line 1805
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getTypePhoneNumber(Lcom/vlingo/core/internal/contacts/ContactMatch;I)I

    move-result v0

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V
    .locals 0

    .prologue
    .line 1093
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setSuggetionListFlowParam(I)V

    return-void
.end method

.method static synthetic access$50(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Lcom/vlingo/core/internal/contacts/ContactMatch;)I
    .locals 1

    .prologue
    .line 1732
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getMainPhoneNumber(Lcom/vlingo/core/internal/contacts/ContactMatch;)I

    move-result v0

    return v0
.end method

.method static synthetic access$51(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;ILcom/vlingo/core/internal/contacts/ContactMatch;I)V
    .locals 0

    .prologue
    .line 1983
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->startPrepareDialActivity(ILcom/vlingo/core/internal/contacts/ContactMatch;I)V

    return-void
.end method

.method static synthetic access$52(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->finish()V

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mLogsList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Z
    .locals 1

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->isLogClicked:Z

    return v0
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mLogsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Z
    .locals 1

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mLogTTS:Z

    return v0
.end method

.method private get2DepthBtnOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2126
    new-instance v0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$19;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$19;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    return-object v0
.end method

.method private getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2092
    new-instance v0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$18;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$18;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    return-object v0
.end method

.method private getListItemOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1832
    new-instance v0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$17;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$17;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    return-object v0
.end method

.method private getMainPhoneNumber(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)I
    .locals 7
    .param p1, "user"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 1686
    if-nez p1, :cond_0

    .line 1687
    const/4 v3, 0x0

    .line 1729
    :goto_0
    return v3

    .line 1691
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 1693
    .local v1, "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v1, v4, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getPhoneNumberList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1695
    .local v0, "dp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    const/4 v2, 0x0

    .line 1696
    .local v2, "i":I
    const/4 v3, 0x0

    .line 1698
    .local v3, "phoneType":I
    if-nez v0, :cond_1

    .line 1699
    const-string/jumbo v4, "PhoneActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "dp == null , phoneType index : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1703
    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_3

    .line 1714
    :goto_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v2, v4, :cond_2

    if-nez v3, :cond_2

    .line 1715
    const/4 v2, 0x0

    :goto_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_5

    .line 1727
    :cond_2
    :goto_4
    const-string/jumbo v4, "PhoneActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "phoneType index : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1704
    :cond_3
    const-string/jumbo v5, "PhoneActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "["

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "] "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "is Main? : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1705
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->isMainPhoneNumber()Z

    move-result v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1704
    invoke-static {v5, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1706
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->isMainPhoneNumber()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1707
    const-string/jumbo v4, "PhoneActivity"

    const-string/jumbo v5, "is MainPhoneNumber : true"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1708
    move v3, v2

    .line 1710
    goto :goto_2

    .line 1703
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1716
    :cond_5
    const-string/jumbo v5, "PhoneActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "["

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "] "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "type : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1717
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneType()I

    move-result v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1716
    invoke-static {v5, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1718
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneType()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_6

    .line 1719
    const-string/jumbo v4, "PhoneActivity"

    const-string/jumbo v5, "is Mobile : true"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1720
    move v3, v2

    .line 1722
    goto/16 :goto_4

    .line 1715
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3
.end method

.method private getMainPhoneNumber(Lcom/vlingo/core/internal/contacts/ContactMatch;)I
    .locals 6
    .param p1, "user"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 1733
    if-nez p1, :cond_0

    .line 1734
    const/4 v2, 0x0

    .line 1774
    :goto_0
    return v2

    .line 1745
    :cond_0
    const/4 v1, 0x0

    .line 1746
    .local v1, "i":I
    const/4 v2, 0x0

    .line 1748
    .local v2, "phoneType":I
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v0

    .line 1750
    .local v0, "cl":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/4 v1, 0x0

    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_2

    .line 1760
    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ne v1, v3, :cond_1

    if-nez v2, :cond_1

    .line 1761
    const/4 v1, 0x0

    :goto_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_4

    .line 1772
    :cond_1
    :goto_4
    const-string/jumbo v3, "PhoneActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "phoneType index : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1751
    :cond_2
    const-string/jumbo v4, "PhoneActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "["

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "] "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "is Main? : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactData;

    iget v3, v3, Lcom/vlingo/core/internal/contacts/ContactData;->isDefault:I

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1752
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactData;

    iget v3, v3, Lcom/vlingo/core/internal/contacts/ContactData;->isDefault:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 1753
    const-string/jumbo v3, "PhoneActivity"

    const-string/jumbo v4, "is MainPhoneNumber : true"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1754
    move v2, v1

    .line 1756
    goto :goto_2

    .line 1750
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1762
    :cond_4
    const-string/jumbo v4, "PhoneActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "["

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "] "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "type : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactData;->getType()I

    move-result v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1763
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactData;->getType()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    .line 1764
    const-string/jumbo v3, "PhoneActivity"

    const-string/jumbo v4, "is Mobile : true"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1765
    move v2, v1

    .line 1767
    goto/16 :goto_4

    .line 1761
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_3
.end method

.method private getNumberListItemOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1662
    new-instance v0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$16;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$16;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    return-object v0
.end method

.method private getPhoneSipStateListener()Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$OnPhoneSipStateListener;
    .locals 1

    .prologue
    .line 468
    new-instance v0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    return-object v0
.end method

.method private getTypePhoneNumber(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;I)I
    .locals 7
    .param p1, "user"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .param p2, "index"    # I

    .prologue
    .line 1778
    if-nez p1, :cond_0

    .line 1779
    const/4 v3, 0x0

    .line 1802
    :goto_0
    return v3

    .line 1781
    :cond_0
    const/4 v2, 0x0

    .line 1782
    .local v2, "i":I
    const/4 v3, 0x0

    .line 1785
    .local v3, "phoneType":I
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 1787
    .local v1, "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v1, v4, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getPhoneNumberList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1789
    .local v0, "dp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    const-string/jumbo v4, "PhoneActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "[getTypePhoneNumber] index : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1790
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_1

    .line 1800
    :goto_2
    const-string/jumbo v4, "PhoneActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "phoneType index : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1791
    :cond_1
    const-string/jumbo v5, "PhoneActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "["

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "] "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "type : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneType()I

    move-result v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1792
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneType()I

    move-result v4

    if-ne v4, p2, :cond_2

    .line 1793
    const-string/jumbo v4, "PhoneActivity"

    const-string/jumbo v5, "is type : true"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1794
    move v3, v2

    .line 1796
    goto :goto_2

    .line 1790
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private getTypePhoneNumber(Lcom/vlingo/core/internal/contacts/ContactMatch;I)I
    .locals 6
    .param p1, "user"    # Lcom/vlingo/core/internal/contacts/ContactMatch;
    .param p2, "index"    # I

    .prologue
    .line 1806
    if-nez p1, :cond_0

    .line 1807
    const/4 v2, 0x0

    .line 1828
    :goto_0
    return v2

    .line 1809
    :cond_0
    const/4 v1, 0x0

    .line 1810
    .local v1, "i":I
    const/4 v2, 0x0

    .line 1812
    .local v2, "phoneType":I
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v0

    .line 1814
    .local v0, "cl":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const-string/jumbo v3, "PhoneActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "[getTypePhoneNumber] index : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1816
    const/4 v1, 0x0

    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 1826
    :goto_2
    const-string/jumbo v3, "PhoneActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "phoneType index : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1817
    :cond_1
    const-string/jumbo v4, "PhoneActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "["

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "] "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "type : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactData;->getType()I

    move-result v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1818
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactData;->getType()I

    move-result v3

    if-ne v3, p2, :cond_2

    .line 1819
    const-string/jumbo v3, "PhoneActivity"

    const-string/jumbo v4, "is type : true"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1820
    move v2, v1

    .line 1822
    goto :goto_2

    .line 1816
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private handleShrinkMessageOnMicState(I)V
    .locals 7
    .param p1, "stateValue"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 400
    const-string/jumbo v3, "activity"

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 401
    .local v1, "am":Landroid/app/ActivityManager;
    invoke-virtual {v1, v6}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    .line 402
    .local v0, "Info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v2, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 404
    .local v2, "topActivity":Landroid/content/ComponentName;
    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    .line 405
    const-string/jumbo v4, "com.sec.android.automotive.drivelink.phone.PhoneActivity"

    .line 404
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 405
    if-nez v3, :cond_0

    .line 406
    const-string/jumbo v3, "PhoneActivity"

    const-string/jumbo v4, "handleShrinkMessageOnMicState - not PhoneActivity"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    :goto_0
    return-void

    .line 409
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 415
    const-string/jumbo v3, "PhoneActivity"

    const-string/jumbo v4, "handleShrinkMessageOnMicState - default"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    invoke-direct {p0, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setAutoShrink(Z)V

    goto :goto_0

    .line 411
    :pswitch_0
    const-string/jumbo v3, "PhoneActivity"

    const-string/jumbo v4, "handleShrinkMessageOnMicState - MIC_STATE_IDLE"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    invoke-direct {p0, v6}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setAutoShrink(Z)V

    goto :goto_0

    .line 409
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private initNumberLayout()V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v4, 0x0

    .line 1588
    iget v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    const/16 v2, 0x80

    if-ne v1, v2, :cond_0

    .line 1589
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1590
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1592
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1593
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1596
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberPageAdapter:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;

    if-nez v1, :cond_1

    .line 1597
    new-instance v1, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;

    .line 1598
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getNumberListItemOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V

    .line 1597
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberPageAdapter:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;

    .line 1599
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberPageAdapter:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 1608
    :goto_0
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    .line 1609
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    .line 1610
    const/4 v2, -0x2

    .line 1609
    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1611
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1612
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1613
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1614
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setGravity(I)V

    .line 1615
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberPageNavigationLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1616
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberPageAdapter:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->getPageCount()I

    move-result v2

    .line 1617
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v3

    .line 1616
    invoke-virtual {v1, v2, v4, v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setDotIndicator(IILandroid/view/View$OnClickListener;)V

    .line 1618
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberViewPager:Landroid/support/v4/view/ViewPager;

    new-instance v2, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$15;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$15;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 1650
    return-void

    .line 1601
    .end local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberPageNavigationLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 1602
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberPageAdapter:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->setDLPhoneNumbers(Ljava/util/ArrayList;)V

    .line 1603
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberPageAdapter:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->notifyDataSetChanged()V

    .line 1604
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberPageAdapter:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;

    .line 1605
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->getInitialPosition()I

    move-result v2

    .line 1604
    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method

.method private initSearchTextField()V
    .locals 2

    .prologue
    .line 540
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    .line 541
    const v1, 0x7f090393

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 540
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchBtn:Landroid/widget/ImageButton;

    .line 542
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    .line 543
    const v1, 0x7f090394

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 542
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;

    .line 544
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    .line 545
    const v1, 0x7f090396

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 544
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;

    .line 546
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    .line 547
    const v1, 0x7f090397

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 546
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchClearBtn:Landroid/widget/ImageButton;

    .line 548
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    .line 549
    const v1, 0x7f090398

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 548
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;

    .line 550
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    .line 551
    const v1, 0x7f090395

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 550
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mTvSearchContact:Landroid/widget/TextView;

    .line 553
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;

    check-cast v0, Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP;

    .line 554
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getPhoneSipStateListener()Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$OnPhoneSipStateListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP;->setOnPhoneSipStateListener(Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$OnPhoneSipStateListener;)V

    .line 555
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;

    const-string/jumbo v1, "disableEmoticonInput=true;"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 557
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchBtn:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 607
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$9;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 699
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$10;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$10;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 709
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 887
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchClearBtn:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$12;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$12;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 904
    return-void
.end method

.method private setAutoShrink(Z)V
    .locals 4
    .param p1, "isOn"    # Z

    .prologue
    const/4 v3, 0x0

    .line 422
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 435
    :goto_0
    return-void

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 427
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchMode:Z

    if-nez v0, :cond_1

    .line 428
    const-string/jumbo v0, "PhoneActivity"

    const-string/jumbo v1, "MSG_AUTO_SHRINK - start on playing"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mHandler:Landroid/os/Handler;

    .line 430
    const-wide/16 v1, 0x2710

    .line 429
    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 432
    :cond_1
    const-string/jumbo v0, "PhoneActivity"

    const-string/jumbo v1, "MSG_AUTO_SHRINK - removing"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method private setCurrentMode(I)V
    .locals 13
    .param p1, "mode"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v12, 0x0

    const/16 v4, 0x8

    .line 968
    iput p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    .line 969
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    invoke-virtual {v1, p1}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->setCurrentMode(I)V

    .line 971
    sparse-switch p1, :sswitch_data_0

    .line 1090
    :goto_0
    iget v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setSuggetionListFlowParam(I)V

    .line 1091
    :cond_0
    return-void

    .line 973
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mListTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 974
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->setPhoneSuggestionListUpdateListener()V

    .line 976
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    check-cast v8, Lcom/sec/android/automotive/drivelink/DLApplication;

    .line 977
    .local v8, "dlApp":Lcom/sec/android/automotive/drivelink/DLApplication;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactList:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/DLApplication;->isContactsChanged()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 978
    :cond_1
    invoke-virtual {v8, v12}, Lcom/sec/android/automotive/drivelink/DLApplication;->setContactsChanged(Z)V

    .line 979
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updatePhoneSuggestionList()V

    goto :goto_0

    .line 981
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->showSuggestionList()V

    goto :goto_0

    .line 985
    .end local v8    # "dlApp":Lcom/sec/android/automotive/drivelink/DLApplication;
    :sswitch_1
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->isLogClicked:Z

    .line 986
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mListTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 987
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateLogList()V

    goto :goto_0

    .line 991
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mListTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 999
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    .line 1000
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1001
    :cond_3
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-wide v1, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    .line 1002
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v3, v3, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 1003
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v5

    .line 1004
    const/4 v6, 0x0

    .line 1001
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1006
    .local v0, "d":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 1005
    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 1006
    invoke-virtual {v1, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 1015
    .local v7, "bitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-direct {p0, v12, v1, v12}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->startPrepareDialActivity(ILcom/vlingo/core/internal/contacts/ContactMatch;I)V

    goto :goto_0

    .line 1018
    .end local v0    # "d":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v7    # "bitmap":Landroid/graphics/Bitmap;
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mListTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1020
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;

    if-eqz v1, :cond_4

    .line 1021
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;

    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1029
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getDLLastConfigInstance()Ljava/util/HashMap;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1030
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getDLLastConfigInstance()Ljava/util/HashMap;

    move-result-object v1

    const-string/jumbo v2, "mContactChoices"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    .line 1031
    .local v11, "obj":Ljava/lang/Object;
    if-eqz v11, :cond_5

    .line 1032
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mIsRotation:Z

    if-eqz v1, :cond_5

    .line 1033
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getDLLastConfigInstance()Ljava/util/HashMap;

    move-result-object v1

    .line 1034
    const-string/jumbo v2, "mContactChoices"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 1033
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    .line 1038
    .end local v11    # "obj":Ljava/lang/Object;
    :cond_5
    const-string/jumbo v1, "PhoneActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "msearched text : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedResult:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1039
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->showSearchResult(Ljava/util/List;)V

    goto/16 :goto_0

    .line 1043
    :sswitch_4
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mListTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1044
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;

    if-eqz v1, :cond_6

    .line 1045
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1046
    :cond_6
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNolistText:Landroid/widget/TextView;

    const v2, 0x7f0a035a

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1047
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v12}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1048
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1049
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1051
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 1057
    :sswitch_5
    const-string/jumbo v1, "DM_DIAL_TYPE"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v10

    .line 1059
    .local v10, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v10, :cond_0

    iget-object v1, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-eqz v1, :cond_0

    .line 1061
    iget-object v1, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-static {v1}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getDlContact(Lcom/vlingo/core/internal/contacts/ContactMatch;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v9

    .line 1063
    .local v9, "dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    iget-object v1, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-nez v1, :cond_7

    .line 1064
    const-string/jumbo v1, "PhoneActivity"

    const-string/jumbo v2, "ContactMatch is null"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1067
    :cond_7
    iget-object v1, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-wide v1, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberContactId:J

    .line 1068
    iget v1, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserIndex:I

    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserIndex:I

    .line 1069
    iget-object v1, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v1, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserDisplayName:Ljava/lang/String;

    .line 1071
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    invoke-interface {v1, p0, v9}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1070
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserContactImage:Landroid/graphics/Bitmap;

    .line 1075
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 1074
    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 1076
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 1077
    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberContactId:J

    .line 1076
    invoke-virtual {v1, p0, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getPhoneNumberList(Landroid/content/Context;J)Ljava/util/ArrayList;

    move-result-object v1

    .line 1074
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    .line 1081
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;

    if-eqz v1, :cond_8

    .line 1082
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1083
    :cond_8
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mListTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1084
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mListTitle:Landroid/widget/TextView;

    const v2, 0x7f0a035b

    new-array v3, v5, [Ljava/lang/Object;

    .line 1085
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserDisplayName:Ljava/lang/String;

    aput-object v4, v3, v12

    .line 1084
    invoke-virtual {p0, v2, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1086
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->initNumberLayout()V

    goto/16 :goto_0

    .line 971
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x20 -> :sswitch_1
        0x41 -> :sswitch_2
        0x42 -> :sswitch_3
        0x43 -> :sswitch_4
        0x80 -> :sswitch_5
    .end sparse-switch
.end method

.method private setInitUI(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v5, 0x42

    .line 907
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.sec.android.automotive.drivelink.ACTION_DL_DM_FLOW_CHANGED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 911
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "EXTRA_FROM_FLOW_MGR"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 919
    const/4 v1, 0x0

    .line 920
    .local v1, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 921
    const-string/jumbo v3, "EXTRA_FLOW_ID"

    .line 920
    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 922
    .local v0, "flowID":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v1

    .line 923
    const-string/jumbo v2, "PhoneActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "flow ID :: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 925
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->isVideoCallRequested:Z

    .line 927
    const-string/jumbo v2, "DM_DIAL_TYPE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 932
    if-eqz v1, :cond_0

    .line 935
    iget v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    if-eq v2, v5, :cond_0

    .line 936
    iget v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    const/16 v3, 0x43

    if-eq v2, v3, :cond_0

    .line 937
    iget v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    iput v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mPreviousMode:I

    .line 938
    const/16 v2, 0x80

    iput v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    .line 939
    iget-object v2, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 940
    iget-object v2, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMap:Ljava/util/Map;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactMap:Ljava/util/Map;

    .line 955
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 956
    const-string/jumbo v2, "PhoneActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "isVideoCallRequested : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->isVideoCallRequested:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 957
    const-string/jumbo v2, "PhoneActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "is Video Call : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->isVideoCall:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 958
    const-string/jumbo v2, "PhoneActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "search Text : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchedText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 959
    const-string/jumbo v2, "PhoneActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "search Text : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedResult:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 960
    iget-boolean v2, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->isVideoCall:Z

    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->isVideoCallRequested:Z

    .line 964
    .end local v0    # "flowID":Ljava/lang/String;
    .end local v1    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_1
    return-void

    .line 944
    .restart local v0    # "flowID":Ljava/lang/String;
    .restart local v1    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_2
    const-string/jumbo v2, "DM_DIAL_CONTACT_SEARCH_LIST"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 949
    iget v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    iput v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mPreviousMode:I

    .line 950
    iput v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    .line 951
    iget-object v2, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactChoices:Ljava/util/List;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    .line 952
    iget-object v2, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchedText:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedResult:Ljava/lang/String;

    goto :goto_0
.end method

.method private setSuggetionListFlowParam(I)V
    .locals 6
    .param p1, "mode"    # I

    .prologue
    .line 1094
    const/4 v2, 0x0

    .line 1096
    .local v2, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    sparse-switch p1, :sswitch_data_0

    .line 1144
    :cond_0
    return-void

    .line 1099
    :sswitch_0
    const-string/jumbo v3, "DM_DIAL_CONTACT"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v2

    .line 1101
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 1102
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 1103
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    .line 1105
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 1106
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    iget-object v4, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1113
    .end local v0    # "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :sswitch_1
    const-string/jumbo v3, "DM_DIAL_CONTACT_SEARCH_LIST"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v2

    .line 1115
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 1116
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 1117
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    .line 1119
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 1120
    .local v0, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget-object v4, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    iget-object v5, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1127
    .end local v0    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :sswitch_2
    const-string/jumbo v3, "DM_DIAL_TYPE"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v2

    .line 1129
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 1130
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 1131
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    .line 1133
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    .line 1134
    .local v1, "dp":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    iget-object v4, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1096
    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x42 -> :sswitch_1
        0x80 -> :sswitch_2
    .end sparse-switch
.end method

.method private showLogList()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1410
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mLogsList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mLogsList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 1411
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1413
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1414
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1415
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1472
    :cond_1
    :goto_0
    return-void

    .line 1419
    :cond_2
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    .line 1422
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1423
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1424
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1425
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private showSuggestionList()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v3, 0x8

    .line 1333
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 1334
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNolistText:Landroid/widget/TextView;

    const v2, 0x7f0a0359

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1335
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1336
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1337
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1339
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1407
    :goto_0
    return-void

    .line 1343
    :cond_1
    iget v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    const/16 v2, 0x10

    if-ne v1, v2, :cond_2

    .line 1345
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1346
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1347
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1349
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1352
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;

    if-nez v1, :cond_3

    .line 1353
    new-instance v1, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;

    .line 1354
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactList:Ljava/util/ArrayList;

    .line 1355
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getListItemOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V

    .line 1353
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;

    .line 1356
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNightMode:Z

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->setNightMode(Z)V

    .line 1357
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 1365
    :goto_1
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    .line 1366
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    .line 1367
    const/4 v2, -0x2

    .line 1366
    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1368
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1369
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1370
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1371
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setGravity(I)V

    .line 1372
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageNavigationLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1373
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    .line 1374
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->getPageCount()I

    move-result v2

    .line 1375
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v3

    .line 1373
    invoke-virtual {v1, v2, v5, v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setDotIndicator(IILandroid/view/View$OnClickListener;)V

    .line 1376
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsViewPager:Landroid/support/v4/view/ViewPager;

    .line 1377
    new-instance v2, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$13;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$13;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    goto/16 :goto_0

    .line 1359
    .end local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageNavigationLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 1360
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->setSuggestionsList(Ljava/util/ArrayList;)V

    .line 1361
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->notifyDataSetChanged()V

    .line 1362
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;

    .line 1363
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->getInitialPosition()I

    move-result v2

    .line 1362
    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_1
.end method

.method private startPrepareDialActivity(IJLjava/lang/String;ILjava/lang/String;Landroid/graphics/Bitmap;I)V
    .locals 6
    .param p1, "userindex"    # I
    .param p2, "contactId"    # J
    .param p4, "userDisplayName"    # Ljava/lang/String;
    .param p5, "phoneType"    # I
    .param p6, "phoneNumber"    # Ljava/lang/String;
    .param p7, "userBmp"    # Landroid/graphics/Bitmap;
    .param p8, "size"    # I

    .prologue
    const/4 v5, 0x2

    .line 1937
    const-string/jumbo v3, "phone"

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 1938
    .local v2, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getServiceState()I

    move-result v1

    .line 1941
    .local v1, "serviceState":I
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1942
    const-string/jumbo v3, "PhoneActivity"

    .line 1943
    const-string/jumbo v4, "MUSICService PhoneActivity called PLAYBACK_REQUEST_PHONE_PAUSE"

    .line 1942
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1944
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    .line 1945
    invoke-virtual {v3, v5}, Lcom/sec/android/automotive/drivelink/music/MusicService;->onPlaybackRequest(I)V

    .line 1961
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 1962
    const-class v4, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    .line 1961
    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1964
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v3, "userindex"

    invoke-virtual {v0, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1965
    const-string/jumbo v3, "contactId"

    invoke-virtual {v0, v3, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1966
    const-string/jumbo v3, "userdisplayname"

    invoke-virtual {v0, v3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1967
    const-string/jumbo v3, "userphonetype"

    invoke-virtual {v0, v3, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1968
    const-string/jumbo v3, "userphonenum"

    invoke-virtual {v0, v3, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1969
    const-string/jumbo v3, "userbitmap"

    invoke-virtual {v0, v3, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1970
    const-string/jumbo v3, "userphonenumsize"

    invoke-virtual {v0, v3, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1973
    invoke-static {p0, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 1975
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->stopDmFlow()V

    .line 1977
    if-ge p8, v5, :cond_1

    .line 1978
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->finish()V

    .line 1980
    :cond_1
    return-void
.end method

.method private startPrepareDialActivity(ILcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;I)V
    .locals 8
    .param p1, "userindex"    # I
    .param p2, "dlContact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .param p3, "phoneTypeIndex"    # I

    .prologue
    .line 2029
    const-string/jumbo v5, "phone"

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    .line 2030
    .local v4, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getServiceState()I

    move-result v3

    .line 2033
    .local v3, "serviceState":I
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 2034
    const-string/jumbo v5, "PhoneActivity"

    .line 2035
    const-string/jumbo v6, "MUSICService PhoneActivity called PLAYBACK_REQUEST_PHONE_PAUSE"

    .line 2034
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2036
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v5

    .line 2038
    const/4 v6, 0x2

    .line 2037
    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/music/MusicService;->onPlaybackRequest(I)V

    .line 2053
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 2054
    .local v1, "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 2055
    .end local v1    # "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v5

    .line 2056
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v6

    .line 2055
    invoke-virtual {v5, p0, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getPhoneNumberList(Landroid/content/Context;J)Ljava/util/ArrayList;

    move-result-object v2

    .line 2058
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    invoke-static {p2, v2}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;Ljava/util/ArrayList;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v0

    .line 2060
    .local v0, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-direct {p0, p1, v0, p3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->startPrepareDialActivity(ILcom/vlingo/core/internal/contacts/ContactMatch;I)V

    .line 2062
    return-void
.end method

.method private startPrepareDialActivity(ILcom/vlingo/core/internal/contacts/ContactMatch;I)V
    .locals 7
    .param p1, "userindex"    # I
    .param p2, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;
    .param p3, "phoneTypeIndex"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1985
    const-string/jumbo v3, "phone"

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 1986
    .local v2, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getServiceState()I

    move-result v1

    .line 1989
    .local v1, "serviceState":I
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1990
    const-string/jumbo v3, "PhoneActivity"

    .line 1991
    const-string/jumbo v4, "MUSICService PhoneActivity called PLAYBACK_REQUEST_PHONE_PAUSE"

    .line 1990
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1992
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    .line 1994
    const/4 v4, 0x2

    .line 1993
    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->onPlaybackRequest(I)V

    .line 2008
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 2010
    .local v0, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput p1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserIndex:I

    .line 2011
    iput-object p2, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 2012
    iput p3, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    .line 2014
    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->isVideoCallRequested:Z

    if-eqz v3, :cond_1

    .line 2015
    const-string/jumbo v3, "PhoneActivity"

    const-string/jumbo v4, "video Call is true"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2016
    iput-boolean v5, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->isVideoCall:Z

    .line 2017
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 2018
    const v4, 0x7f0a013c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    .line 2019
    iget-object v5, p2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    aput-object v5, v4, v6

    .line 2017
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 2020
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->isVideoCallRequested:Z

    .line 2023
    :cond_1
    const-string/jumbo v3, "DM_DIAL"

    invoke-static {v3, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 2025
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 2

    .prologue
    .line 2369
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mToDial:Z

    if-eqz v0, :cond_0

    .line 2370
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->finish()V

    .line 2390
    :cond_0
    # invokes: Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->finish()V
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$52(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    .line 2391
    const v0, 0x7f040025

    const v1, 0x7f040001

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->overridePendingTransition(II)V

    .line 2396
    return-void
.end method

.method public onBackPressed()V
    .locals 5

    .prologue
    const/16 v4, 0x10

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2158
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2162
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2163
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2164
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->showVoiceLayout()V

    .line 2166
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mPreviousMode:I

    .line 2167
    iput v4, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    .line 2168
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setCurrentMode(I)V

    .line 2170
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchMode:Z

    .line 2171
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSipOpenMode:Z

    .line 2172
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedText:Ljava/lang/String;

    .line 2173
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedResult:Ljava/lang/String;

    .line 2175
    const-string/jumbo v0, "PhoneActivity"

    const-string/jumbo v1, "onBackPressed - setAutoShrink : true"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2176
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setAutoShrink(Z)V

    .line 2193
    :goto_0
    return-void

    .line 2181
    :cond_0
    const-string/jumbo v0, "PhoneActivity"

    const-string/jumbo v1, "onBackPressed - setAutoShrink : false"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2182
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setAutoShrink(Z)V

    .line 2184
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    .line 2185
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mPreviousMode:I

    .line 2186
    iput v4, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    .line 2187
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setCurrentMode(I)V

    goto :goto_0

    .line 2189
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->restorePrevFlow()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v10, 0x0

    const/4 v8, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    .line 213
    const-string/jumbo v5, "PhoneActivity"

    const-string/jumbo v9, "onCreate"

    invoke-static {v5, v9}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 216
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 218
    .local v3, "startTime":J
    const v5, 0x7f030023

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setContentView(I)V

    .line 219
    const v5, 0x7f0a024f

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setTitle(I)V

    .line 220
    const v5, 0x7f0900ea

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    .line 221
    const v5, 0x7f0900eb

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mListLayout:Landroid/widget/LinearLayout;

    .line 222
    const v5, 0x7f0900ec

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mListTitle:Landroid/widget/TextView;

    .line 223
    const v5, 0x7f09009f

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    .line 224
    const v5, 0x7f0900ae

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNolistText:Landroid/widget/TextView;

    .line 225
    const v5, 0x7f09009d

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;

    .line 227
    const v5, 0x7f0900ed

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchListLayout:Landroid/widget/LinearLayout;

    .line 228
    const v5, 0x7f0900f0

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberListLayout:Landroid/widget/LinearLayout;

    .line 229
    const v5, 0x7f09009e

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/support/v4/view/ViewPager;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsViewPager:Landroid/support/v4/view/ViewPager;

    .line 231
    const v5, 0x7f0900ee

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/support/v4/view/ViewPager;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;

    .line 232
    const v5, 0x7f0900f1

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/support/v4/view/ViewPager;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberViewPager:Landroid/support/v4/view/ViewPager;

    .line 233
    const v5, 0x7f0900af

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 236
    const v5, 0x7f0900ef

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 237
    const v5, 0x7f0900f2

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 240
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getPhoneSuggestionList()Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactList:Ljava/util/ArrayList;

    .line 241
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getLogList()Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mLogsList:Ljava/util/ArrayList;

    .line 243
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v5

    .line 244
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mOnUpdateLogListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;

    .line 243
    invoke-virtual {v5, v9}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->setOnUpdateLogListListener(Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;)V

    .line 245
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v5

    .line 246
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mOnUpdateSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;

    .line 245
    invoke-virtual {v5, v9}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->setOnUpdateSugessionListListener(Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;)V

    .line 248
    if-nez p1, :cond_5

    move v5, v6

    :goto_0
    iput v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mPreviousMode:I

    .line 250
    if-nez p1, :cond_6

    :goto_1
    iput v6, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    .line 252
    if-nez p1, :cond_7

    move-object v5, v7

    :goto_2
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    .line 257
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string/jumbo v6, "contactId"

    invoke-virtual {v5, v6, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v5

    cmp-long v5, v5, v10

    if-eqz v5, :cond_0

    .line 258
    iget v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    iput v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mPreviousMode:I

    .line 259
    const/16 v5, 0x80

    iput v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    .line 262
    :cond_0
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    new-instance v6, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;

    invoke-direct {v6, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 321
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    .line 322
    new-instance v6, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$6;

    invoke-direct {v6, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->setOnVoicePhoneActionBarListener(Lcom/sec/android/automotive/drivelink/phone/OnVoicePhoneActionBarListener;)V

    .line 335
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->get2DepthBtnOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->setOn2DepthBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 336
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 337
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    invoke-virtual {v5, v6}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 339
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;

    move-result-object v5

    .line 340
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    .line 339
    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->addDrivingChangeListener(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;)V

    .line 342
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 343
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 344
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setInitUI(Landroid/content/Intent;)V

    .line 350
    :cond_2
    if-nez p1, :cond_8

    move v5, v8

    :goto_3
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchMode:Z

    .line 352
    if-nez p1, :cond_9

    move v5, v8

    :goto_4
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSipOpenMode:Z

    .line 354
    if-nez p1, :cond_a

    move-object v5, v7

    :goto_5
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedText:Ljava/lang/String;

    .line 356
    if-nez p1, :cond_b

    move-object v5, v7

    :goto_6
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedResult:Ljava/lang/String;

    .line 358
    if-nez p1, :cond_c

    :goto_7
    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedResultTitle:Ljava/lang/String;

    .line 361
    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchMode:Z

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedResult:Ljava/lang/String;

    if-nez v5, :cond_3

    .line 363
    const-string/jumbo v5, "DM_DIAL_CONTACT_SEARCH_LIST"

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v2

    .line 365
    .local v2, "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v2, :cond_3

    .line 366
    const-string/jumbo v5, "PhoneActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "onCreate - searchedText : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchedText:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    iget-object v5, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchedText:Ljava/lang/String;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedResult:Ljava/lang/String;

    .line 371
    .end local v2    # "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->initSearchTextField()V

    .line 373
    const/high16 v5, 0x7f040000

    const v6, 0x7f040025

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->overridePendingTransition(II)V

    .line 377
    if-eqz p1, :cond_d

    .line 378
    const-string/jumbo v5, "IS_ERROR_STATE"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 380
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mIsError:Z

    .line 382
    const-string/jumbo v5, "ERROR_STRING"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 381
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mErrorStr:Ljava/lang/String;

    .line 387
    :goto_8
    if-eqz p1, :cond_4

    .line 388
    const-string/jumbo v5, "IS_TTS_STATE"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 389
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->setTTSState()V

    .line 392
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 394
    .local v0, "endTime":J
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->moveoutOverlayService()V

    .line 396
    const-string/jumbo v5, "PhoneActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "[snowdeer] onCreate time : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v7, v0, v3

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    return-void

    .line 249
    .end local v0    # "endTime":J
    :cond_5
    const-string/jumbo v5, "PHONE_PREMODE"

    invoke-virtual {p1, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    goto/16 :goto_0

    .line 251
    :cond_6
    const-string/jumbo v5, "PHONE_MODE"

    invoke-virtual {p1, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    goto/16 :goto_1

    .line 254
    :cond_7
    const-string/jumbo v5, "CONTACT_CHOICE"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    .line 253
    check-cast v5, Ljava/util/List;

    goto/16 :goto_2

    .line 351
    :cond_8
    const-string/jumbo v5, "SIP_SEARCH_MODE"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    goto/16 :goto_3

    .line 353
    :cond_9
    const-string/jumbo v5, "SIP_OPEN_MODE"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    goto/16 :goto_4

    .line 355
    :cond_a
    const-string/jumbo v5, "SIP_SEARCH_TEXT"

    invoke-virtual {p1, v5, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_5

    .line 357
    :cond_b
    const-string/jumbo v5, "SIP_SEARCH_RESULT"

    invoke-virtual {p1, v5, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_6

    .line 359
    :cond_c
    const-string/jumbo v5, "SIP_SEARCH_RESULT_TEXT"

    invoke-virtual {p1, v5, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_7

    .line 384
    :cond_d
    iput-boolean v8, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mIsError:Z

    goto :goto_8
.end method

.method protected onDLRetainConfigInstance(Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2149
    .local p1, "backupMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    const/16 v1, 0x41

    if-ne v0, v1, :cond_1

    .line 2150
    const-string/jumbo v0, "mContactMap"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactMap:Ljava/util/Map;

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2154
    :cond_0
    :goto_0
    return-void

    .line 2151
    :cond_1
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    const/16 v1, 0x42

    if-ne v0, v1, :cond_0

    .line 2152
    const-string/jumbo v0, "mContactChoices"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 2344
    const-string/jumbo v0, "PhoneActivity"

    const-string/jumbo v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2345
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onDestroy()V

    .line 2346
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    if-eqz v0, :cond_0

    .line 2347
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->stopTTSBarAnimation()V

    .line 2350
    :cond_0
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 2351
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;

    move-result-object v0

    .line 2352
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->removeDrivingChangeListener(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;)V

    .line 2355
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->setContactMatches(Ljava/util/List;)V

    .line 2356
    return-void
.end method

.method public onFlowCommandCancel(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 2566
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->restorePrevFlow()V

    .line 2567
    return-void
.end method

.method protected onFlowListSelected(Ljava/lang/String;IZ)V
    .locals 12
    .param p1, "flowID"    # Ljava/lang/String;
    .param p2, "index"    # I
    .param p3, "isOrdinal"    # Z

    .prologue
    const/16 v5, 0x42

    const/16 v4, 0x10

    const/4 v6, 0x0

    .line 1150
    if-eqz p3, :cond_2

    .line 1151
    const/4 v1, 0x4

    if-ge p2, v1, :cond_0

    .line 1152
    iget v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    if-ne v1, v4, :cond_1

    .line 1154
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1155
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr p2, v1

    .line 1157
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt p2, v1, :cond_2

    .line 1245
    :cond_0
    :goto_0
    return-void

    .line 1162
    :cond_1
    iget v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    const/16 v3, 0x80

    if-ne v1, v3, :cond_5

    .line 1163
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 1164
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1165
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr p2, v1

    .line 1167
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p2, v1, :cond_0

    .line 1188
    :cond_2
    iget v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 1191
    :sswitch_0
    const/4 v0, 0x0

    .line 1192
    .local v0, "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    const/4 v10, 0x0

    .line 1194
    .local v10, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    if-ne v1, v4, :cond_6

    .line 1195
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    .line 1196
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1195
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 1197
    const-string/jumbo v1, "CM07"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 1199
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 1200
    .restart local v0    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-static {v0}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v10

    .line 1210
    :cond_3
    :goto_1
    new-instance v9, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v9}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 1211
    .local v9, "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput-object v10, v9, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 1212
    iput p2, v9, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserIndex:I

    .line 1215
    const-string/jumbo v1, "DM_DIAL_CONTACT_SEARCH_LIST"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v11

    .line 1217
    .local v11, "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v11, :cond_4

    iget-boolean v1, v11, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->isVideoCall:Z

    if-eqz v1, :cond_4

    .line 1218
    const/4 v1, 0x1

    iput-boolean v1, v9, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->isVideoCall:Z

    .line 1221
    :cond_4
    if-eqz v11, :cond_7

    iget v1, v11, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchListType:I

    if-ltz v1, :cond_7

    .line 1223
    iget v1, v11, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchListType:I

    .line 1222
    invoke-direct {p0, v10, v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getTypePhoneNumber(Lcom/vlingo/core/internal/contacts/ContactMatch;I)I

    move-result v1

    iput v1, v9, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    .line 1228
    :goto_2
    const-string/jumbo v1, "DM_DIAL"

    invoke-static {v1, v9}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0

    .line 1172
    .end local v0    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v9    # "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v10    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v11    # "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_5
    iget v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    if-ne v1, v5, :cond_2

    .line 1173
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1174
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr p2, v1

    .line 1176
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt p2, v1, :cond_2

    goto/16 :goto_0

    .line 1201
    .restart local v0    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .restart local v10    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_6
    iget v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    if-ne v1, v5, :cond_3

    .line 1202
    const-string/jumbo v1, "PhoneActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "currentItem : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1203
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    check-cast v10, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 1204
    .restart local v10    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .end local v0    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    iget-wide v1, v10, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    .line 1205
    iget-object v3, v10, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 1206
    invoke-virtual {v10}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v4

    .line 1207
    invoke-virtual {v10}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v5

    .line 1204
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .restart local v0    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    goto/16 :goto_1

    .line 1225
    .restart local v9    # "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .restart local v11    # "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_7
    invoke-direct {p0, v10}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getMainPhoneNumber(Lcom/vlingo/core/internal/contacts/ContactMatch;)I

    move-result v1

    iput v1, v9, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    goto :goto_2

    .line 1235
    .end local v0    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v9    # "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v10    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v11    # "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :sswitch_1
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    iget-wide v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberContactId:J

    .line 1236
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserDisplayName:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    move-object v7, v6

    .line 1235
    invoke-direct/range {v2 .. v8}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1238
    .local v2, "d":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    iget v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberUserIndex:I

    invoke-direct {p0, v1, v2, p2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->startPrepareDialActivity(ILcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;I)V

    goto/16 :goto_0

    .line 1188
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x42 -> :sswitch_0
        0x80 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 461
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 462
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 463
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setInitUI(Landroid/content/Intent;)V

    .line 465
    :cond_0
    return-void
.end method

.method public onPrepareFlowChange(Ljava/lang/String;)V
    .locals 3
    .param p1, "nextFlowID"    # Ljava/lang/String;

    .prologue
    .line 2360
    const-string/jumbo v0, "PhoneActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "nextFlowID : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2361
    const-string/jumbo v0, "DM_DIAL"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2362
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mToDial:Z

    .line 2363
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->finish()V

    .line 2365
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    .line 2230
    iput-boolean v13, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mToDial:Z

    .line 2232
    iget-boolean v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchMode:Z

    if-eqz v9, :cond_1

    .line 2234
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 2235
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->hideVoiceLayout()V

    .line 2237
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v9, v13}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2238
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2239
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->clearFocus()V

    .line 2240
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->requestFocus()Z

    .line 2242
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchMode:Z

    .line 2243
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedText:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2244
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->length()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setSelection(I)V

    .line 2246
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getDLLastConfigInstance()Ljava/util/HashMap;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 2247
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getDLLastConfigInstance()Ljava/util/HashMap;

    move-result-object v9

    const-string/jumbo v10, "mContactChoices"

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 2248
    .local v6, "obj":Ljava/lang/Object;
    if-eqz v6, :cond_0

    .line 2249
    iget-boolean v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mIsRotation:Z

    if-eqz v9, :cond_0

    .line 2250
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getDLLastConfigInstance()Ljava/util/HashMap;

    move-result-object v9

    .line 2251
    const-string/jumbo v10, "mContactChoices"

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    .line 2250
    iput-object v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    .line 2258
    .end local v6    # "obj":Ljava/lang/Object;
    :cond_0
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedResultTitle:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2263
    iget-boolean v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSipOpenMode:Z

    if-eqz v9, :cond_5

    .line 2264
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;

    new-instance v10, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$20;

    invoke-direct {v10, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$20;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    .line 2278
    const-wide/16 v11, 0x12c

    .line 2264
    invoke-virtual {v9, v10, v11, v12}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2292
    :cond_1
    :goto_0
    iget v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    invoke-direct {p0, v9}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setCurrentMode(I)V

    .line 2297
    iget-boolean v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mIsError:Z

    if-eqz v9, :cond_2

    .line 2298
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mErrorStr:Ljava/lang/String;

    invoke-virtual {v9, v10}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->displayError(Ljava/lang/CharSequence;)V

    .line 2299
    iput-boolean v13, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mIsError:Z

    .line 2302
    :cond_2
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onResume()V

    .line 2304
    iget v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    const/16 v10, 0x10

    if-ne v9, v10, :cond_3

    .line 2305
    const/4 v3, 0x0

    .line 2306
    .local v3, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v2

    .line 2307
    .local v2, "flowID":Ljava/lang/String;
    const-string/jumbo v9, "DM_DIAL_CONTACT"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2309
    const-string/jumbo v9, "DM_DIAL_CONTACT"

    invoke-static {v9}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v3

    .line 2310
    if-eqz v3, :cond_3

    iget-object v9, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPrePromptString:Ljava/lang/String;

    if-eqz v9, :cond_3

    .line 2312
    const-string/jumbo v9, "DM_DIAL_CONTACT"

    invoke-static {v9}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->resetFlowParam(Ljava/lang/String;)V

    .line 2318
    .end local v2    # "flowID":Ljava/lang/String;
    .end local v3    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_3
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasPendingReq()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2319
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->removePendingReq()Z

    .line 2324
    :cond_4
    sget-wide v7, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->startTransitionTime:J

    .line 2325
    .local v7, "startTransition":J
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 2326
    .local v0, "endTransitionTime":J
    sub-long v9, v0, v7

    const-wide/32 v11, 0xf4240

    div-long v4, v9, v11

    .line 2328
    .local v4, "measuredTransition":J
    const-string/jumbo v9, "PhoneActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Transition time: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "ms"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2329
    return-void

    .line 2280
    .end local v0    # "endTransitionTime":J
    .end local v4    # "measuredTransition":J
    .end local v7    # "startTransition":J
    :cond_5
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v9, v13}, Landroid/widget/EditText;->setCursorVisible(Z)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 2197
    const-string/jumbo v1, "PhoneActivity"

    const-string/jumbo v2, "onSaveInstanceState"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2198
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2199
    const-string/jumbo v1, "PHONE_MODE"

    iget v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2200
    const-string/jumbo v1, "PHONE_PREMODE"

    iget v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mPreviousMode:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2202
    const-string/jumbo v1, "PhoneActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "searchedText : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2203
    const-string/jumbo v1, "PhoneActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "searchedResult : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedResult:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2205
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedText:Ljava/lang/String;

    .line 2206
    const-string/jumbo v1, "SIP_SEARCH_MODE"

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchMode:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2207
    const-string/jumbo v1, "SIP_SEARCH_TEXT"

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedText:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2208
    const-string/jumbo v1, "SIP_OPEN_MODE"

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSipOpenMode:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2209
    const-string/jumbo v1, "SIP_SEARCH_RESULT"

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedResult:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2211
    const/4 v0, 0x0

    .line 2212
    .local v0, "strResult":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 2213
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2214
    :cond_0
    const-string/jumbo v1, "SIP_SEARCH_RESULT_TEXT"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2216
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->isErrorState()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2217
    const-string/jumbo v1, "IS_ERROR_STATE"

    invoke-virtual {p1, v1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2218
    const-string/jumbo v1, "ERROR_STRING"

    .line 2219
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->getErrorCharSequence()Ljava/lang/CharSequence;

    move-result-object v2

    .line 2218
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 2222
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->isTTSState()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2223
    const-string/jumbo v1, "IS_TTS_STATE"

    invoke-virtual {p1, v1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2225
    :cond_2
    return-void
.end method

.method protected onStartResume()V
    .locals 1

    .prologue
    .line 455
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchMode:Z

    if-nez v0, :cond_0

    .line 456
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onStartResume()V

    .line 458
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 2333
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onStop()V

    .line 2335
    const-string/jumbo v0, "PhoneActivity"

    const-string/jumbo v1, "onStop - setAutoShrink : false"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2336
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setAutoShrink(Z)V

    .line 2338
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/DLApplication;->DL_DEMO:Z

    if-eqz v0, :cond_0

    .line 2339
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->finish()V

    .line 2340
    :cond_0
    return-void
.end method

.method protected setDayMode()V
    .locals 5

    .prologue
    const v4, 0x7f08002a

    const/4 v3, 0x0

    .line 2524
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->setDayMode()V

    .line 2526
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNightMode:Z

    .line 2528
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->setDayMode()V

    .line 2529
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2530
    const v2, 0x7f080015

    .line 2529
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 2532
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    sparse-switch v0, :sswitch_data_0

    .line 2562
    :cond_0
    :goto_0
    return-void

    .line 2534
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;

    if-eqz v0, :cond_0

    .line 2535
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->setNightMode(Z)V

    .line 2536
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->notifyDataSetChanged()V

    .line 2537
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 2538
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 2542
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    if-eqz v0, :cond_0

    .line 2543
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->setNightMode(Z)V

    .line 2544
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->notifyDataSetChanged()V

    .line 2545
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 2546
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 2550
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberPageAdapter:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;

    if-eqz v0, :cond_0

    .line 2551
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberPageAdapter:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->setNightMode(Z)V

    .line 2552
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberPageAdapter:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->notifyDataSetChanged()V

    .line 2553
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 2554
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 2532
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x42 -> :sswitch_1
        0x80 -> :sswitch_2
    .end sparse-switch
.end method

.method protected setNightMode()V
    .locals 5

    .prologue
    const v4, 0x7f08002b

    const/4 v3, 0x1

    .line 2479
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->setNightMode()V

    .line 2481
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNightMode:Z

    .line 2483
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->setNightMode()V

    .line 2484
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2485
    const v2, 0x7f08002e

    .line 2484
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 2487
    const-string/jumbo v0, "JINSEIL"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "JINSEIL setNightMode mCurrentMode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2489
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    sparse-switch v0, :sswitch_data_0

    .line 2520
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 2491
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;

    if-eqz v0, :cond_0

    .line 2492
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->setNightMode(Z)V

    .line 2493
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->notifyDataSetChanged()V

    .line 2494
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 2495
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 2499
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    if-eqz v0, :cond_0

    .line 2500
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->setNightMode(Z)V

    .line 2501
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->notifyDataSetChanged()V

    .line 2502
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 2503
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 2489
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x42 -> :sswitch_2
        0x80 -> :sswitch_0
    .end sparse-switch
.end method

.method public showSearchResult(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/util/Map;)V
    .locals 1
    .param p1, "ContactMatch"    # Lcom/vlingo/core/internal/contacts/ContactMatch;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1478
    .local p2, "contactMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 1479
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactMap:Ljava/util/Map;

    .line 1481
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-nez v0, :cond_0

    .line 1486
    :cond_0
    return-void
.end method

.method public showSearchResult(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "displayedChoices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v7, 0x0

    const/16 v4, 0x8

    .line 1490
    const/4 v0, 0x0

    .line 1491
    .local v0, "index":I
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    .line 1493
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 1494
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNolistText:Landroid/widget/TextView;

    const v3, 0x7f0a035a

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1495
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1496
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1497
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1499
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1576
    :goto_0
    return-void

    .line 1503
    :cond_1
    iget v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I

    const/16 v3, 0x42

    if-ne v2, v3, :cond_2

    .line 1504
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1505
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1506
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1508
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1511
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    if-nez v2, :cond_5

    .line 1512
    const-string/jumbo v2, "VAC_CONTACT_SEARCH_HIGHLIGHT"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1513
    new-instance v2, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    .line 1514
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    .line 1515
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getListItemOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedResult:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Landroid/view/View$OnClickListener;Ljava/lang/String;)V

    .line 1513
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    .line 1521
    :goto_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 1534
    :goto_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    if-eqz v2, :cond_3

    .line 1535
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v0

    .line 1538
    :cond_3
    new-instance v2, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    .line 1539
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    .line 1540
    const/4 v3, -0x2

    .line 1539
    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1541
    .local v1, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1542
    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1543
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v2, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1544
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setGravity(I)V

    .line 1545
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1546
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->getPageCount()I

    move-result v3

    .line 1547
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v4

    .line 1546
    invoke-virtual {v2, v3, v7, v4}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setDotIndicator(IILandroid/view/View$OnClickListener;)V

    .line 1548
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;

    new-instance v3, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$14;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$14;-><init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    goto/16 :goto_0

    .line 1517
    .end local v1    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    new-instance v2, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    .line 1518
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    .line 1519
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getListItemOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Landroid/view/View$OnClickListener;)V

    .line 1517
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    goto :goto_1

    .line 1523
    :cond_5
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 1524
    const-string/jumbo v2, "VAC_CONTACT_SEARCH_HIGHLIGHT"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1525
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchedResult:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->setSearchedText(Ljava/lang/String;)V

    .line 1527
    :cond_6
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->setSearchList(Ljava/util/List;)V

    .line 1528
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->notifyDataSetChanged()V

    .line 1529
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;

    .line 1530
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->getInitialPosition()I

    move-result v3

    .line 1529
    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto/16 :goto_2
.end method
