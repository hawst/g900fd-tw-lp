.class public Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;
.super Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;
.source "MusicPlaylistManager.java"


# static fields
.field public static final INVALID_POS:I = -0x1


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMusicIndexListForShuffle:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mMusicPlayIndex:I

.field private mMusicPlayShuffleIndex:I

.field private mMusicPlaylist:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation
.end field

.field private mbShuffle:Z

.field random:Ljava/util/Random;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;-><init>()V

    .line 17
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    .line 18
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    .line 19
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayIndex:I

    .line 20
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayShuffleIndex:I

    .line 21
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mbShuffle:Z

    .line 22
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->random:Ljava/util/Random;

    .line 23
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mContext:Landroid/content/Context;

    .line 26
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    .line 27
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayIndex:I

    .line 28
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mbShuffle:Z

    .line 29
    return-void
.end method

.method private isValidMusicIndex(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 301
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 302
    const/4 v0, 0x1

    .line 305
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeMusicIndexListForSuffle(I)V
    .locals 3
    .param p1, "size"    # I

    .prologue
    .line 339
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    .line 340
    if-gtz p1, :cond_0

    .line 341
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayShuffleIndex:I

    .line 360
    :goto_0
    return-void

    .line 345
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, p1, :cond_1

    .line 349
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 351
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->random:Ljava/util/Random;

    invoke-virtual {v1, p1}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayShuffleIndex:I

    .line 352
    const/4 v0, 0x0

    :goto_2
    if-lt v0, p1, :cond_2

    .line 359
    :goto_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayShuffleIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->moveTo(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    goto :goto_0

    .line 346
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 353
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayIndex:I

    if-ne v1, v2, :cond_3

    .line 354
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayShuffleIndex:I

    goto :goto_3

    .line 352
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method private makeMusicListShuffledWithLastPlayedMusic(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 364
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v1

    .line 365
    invoke-virtual {v1, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getMusicListWithIdOnly(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    .line 366
    .local v0, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_1

    .line 376
    :cond_0
    :goto_0
    return-void

    .line 370
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->setMusicPlaylist(Ljava/util/ArrayList;I)V

    .line 371
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->setShuffle(Z)V

    .line 373
    if-eqz p2, :cond_0

    .line 374
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->moveToMusicId(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    goto :goto_0
.end method

.method private moveTo(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x0

    .line 177
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->isValidMusicIndex(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 191
    :cond_0
    :goto_0
    return-object v0

    .line 181
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 185
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayIndex:I

    .line 187
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 189
    .local v0, "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->saveLastPlayedMusic(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    goto :goto_0
.end method

.method private restoreLastMusicList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v2

    .line 310
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->restoreLastMusicIdList()Ljava/util/ArrayList;

    move-result-object v1

    .line 312
    .local v1, "lastMusicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v2

    .line 313
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 314
    .local v0, "contentManager":Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;
    invoke-virtual {v0, p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->fillMusicListByMusicId(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 316
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 317
    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Ljava/util/ArrayList;

    move-result-object v1

    .line 320
    :cond_0
    return-object v1
.end method

.method private restoreNSetLastPlayedMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .locals 2

    .prologue
    .line 328
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v1

    .line 329
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->restoreLastPlayedMusicId()I

    move-result v0

    .line 331
    .local v0, "musicId":I
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->moveToMusicId(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v1

    return-object v1
.end method

.method private saveLastMusicList(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 324
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->saveLastMusicIdList(Ljava/util/ArrayList;)V

    .line 325
    return-void
.end method

.method private saveLastPlayedMusic(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 1
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 335
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->saveLastPlayedMusicId(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    .line 336
    return-void
.end method


# virtual methods
.method public getMusicPlayIndex()I
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->isShuffled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayShuffleIndex:I

    .line 83
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayIndex:I

    goto :goto_0
.end method

.method public getMusicPlaylist()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 121
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    move-object v0, v2

    .line 147
    :cond_0
    :goto_0
    return-object v0

    .line 125
    :cond_1
    iget v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayIndex:I

    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->isValidMusicIndex(I)Z

    move-result v3

    if-nez v3, :cond_2

    move-object v0, v2

    .line 126
    goto :goto_0

    .line 129
    :cond_2
    const/4 v0, 0x0

    .line 130
    .local v0, "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    :goto_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_3

    move-object v0, v2

    .line 147
    goto :goto_0

    .line 131
    :cond_3
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    iget v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayIndex:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 133
    .restart local v0    # "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_4

    .line 134
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 135
    .local v1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v3

    .line 137
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mContext:Landroid/content/Context;

    .line 136
    invoke-virtual {v3, v4, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->fillMusicListByMusicId(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 140
    .end local v1    # "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    :cond_4
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    .line 141
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->removeMusic(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Z

    goto :goto_1
.end method

.method public initialize(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->initialize(Landroid/content/Context;)Z

    .line 35
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mContext:Landroid/content/Context;

    .line 37
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayIndex:I

    .line 50
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v2

    .line 51
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->restoreLastPlayedMusicId()I

    move-result v0

    .line 52
    .local v0, "lastMusicId":I
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;-><init>()V

    .line 53
    .local v1, "lastPlayedMusic":Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;
    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setId(I)V

    .line 55
    invoke-direct {p0, p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->makeMusicListShuffledWithLastPlayedMusic(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    .line 57
    const/4 v2, 0x1

    return v2
.end method

.method public isShuffled()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mbShuffle:Z

    return v0
.end method

.method public moveToFirst()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->isShuffled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 196
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 197
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_1

    .line 210
    :cond_0
    :goto_0
    return-object v0

    .line 201
    :cond_1
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayShuffleIndex:I

    .line 203
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayShuffleIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 204
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 203
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->moveTo(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    goto :goto_0

    .line 206
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 210
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->moveTo(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    goto :goto_0
.end method

.method public moveToLast()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 215
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->isShuffled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 216
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 217
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_1

    .line 230
    :cond_0
    :goto_0
    return-object v0

    .line 221
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayShuffleIndex:I

    .line 223
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayShuffleIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 224
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 223
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->moveTo(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    goto :goto_0

    .line 226
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 230
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->moveTo(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    goto :goto_0
.end method

.method public moveToMusicId(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .locals 4
    .param p1, "musicId"    # I

    .prologue
    const/4 v3, 0x0

    .line 151
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    move-object v2, v3

    .line 173
    :goto_0
    return-object v2

    .line 155
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    move-object v2, v3

    .line 173
    goto :goto_0

    .line 156
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getId()I

    move-result v2

    if-ne v2, p1, :cond_5

    .line 159
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 160
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_3

    .line 169
    .end local v1    # "j":I
    :cond_2
    :goto_3
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->moveTo(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v2

    goto :goto_0

    .line 161
    .restart local v1    # "j":I
    :cond_3
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v0, :cond_4

    .line 162
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayShuffleIndex:I

    goto :goto_3

    .line 160
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 155
    .end local v1    # "j":I
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public moveToNext()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 235
    const/4 v0, 0x0

    .line 237
    .local v0, "nextMusic":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->isShuffled()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 238
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 239
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_1

    .line 262
    :cond_0
    :goto_0
    return-object v1

    .line 243
    :cond_1
    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayShuffleIndex:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 244
    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayShuffleIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayShuffleIndex:I

    .line 245
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    .line 246
    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayShuffleIndex:I

    .line 245
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 246
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 245
    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->moveTo(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    :goto_1
    move-object v1, v0

    .line 262
    goto :goto_0

    .line 248
    :cond_2
    const/4 v0, 0x0

    .line 250
    goto :goto_1

    .line 251
    :cond_3
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 255
    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayIndex:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 256
    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->moveTo(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 257
    goto :goto_1

    .line 258
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public moveToPrev()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 266
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 297
    :goto_0
    return-object v0

    .line 270
    :cond_1
    const/4 v0, 0x0

    .line 272
    .local v0, "prevMusic":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->isShuffled()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 273
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 274
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_3

    :cond_2
    move-object v0, v1

    .line 275
    goto :goto_0

    .line 278
    :cond_3
    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayShuffleIndex:I

    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_4

    .line 279
    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayShuffleIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayShuffleIndex:I

    .line 280
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicIndexListForShuffle:Ljava/util/ArrayList;

    .line 281
    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayShuffleIndex:I

    .line 280
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 281
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 280
    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->moveTo(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 282
    goto :goto_0

    .line 283
    :cond_4
    const/4 v0, 0x0

    .line 285
    goto :goto_0

    .line 286
    :cond_5
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_7

    :cond_6
    move-object v0, v1

    .line 287
    goto :goto_0

    .line 290
    :cond_7
    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayIndex:I

    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_8

    .line 291
    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayIndex:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->moveTo(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 292
    goto :goto_0

    .line 293
    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeMusic(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Z
    .locals 3
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 106
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 108
    .local v1, "musicIndex":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 110
    .local v0, "bSuccess":Z
    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayIndex:I

    if-gt v1, v2, :cond_0

    .line 111
    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayIndex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayIndex:I

    .line 112
    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayIndex:I

    if-gez v2, :cond_0

    .line 113
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlayIndex:I

    .line 117
    :cond_0
    return v0
.end method

.method public setMusicPlaylist(Ljava/util/ArrayList;I)V
    .locals 1
    .param p2, "indexMusicPlayed"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 89
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    .line 91
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->saveLastMusicList(Ljava/util/ArrayList;)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->makeMusicIndexListForSuffle(I)V

    .line 96
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->moveTo(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 98
    :cond_0
    return-void
.end method

.method public setShuffle(Z)V
    .locals 1
    .param p1, "bShuffle"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mbShuffle:Z

    .line 74
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mbShuffle:Z

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mMusicPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->makeMusicIndexListForSuffle(I)V

    .line 77
    :cond_0
    return-void
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->mContext:Landroid/content/Context;

    .line 64
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->terminate(Landroid/content/Context;)V

    .line 65
    return-void
.end method
