.class public final enum Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;
.super Ljava/lang/Enum;
.source "NotificationItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NotiFlowState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CANCEL:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

.field public static final enum FINISH:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

.field public static final enum IDLE:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

.field public static final enum LISTENING:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

.field public static final enum PAUSE:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

.field public static final enum RESTART:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

.field public static final enum SOUND:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

.field public static final enum TIMEOUT:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

.field public static final enum TTS:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 76
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    const-string/jumbo v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->IDLE:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    const-string/jumbo v1, "SOUND"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->SOUND:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    const-string/jumbo v1, "TTS"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->TTS:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    const-string/jumbo v1, "LISTENING"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->LISTENING:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    const-string/jumbo v1, "TIMEOUT"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->TIMEOUT:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    const-string/jumbo v1, "FINISH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->FINISH:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    const-string/jumbo v1, "PAUSE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->PAUSE:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    const-string/jumbo v1, "RESTART"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->RESTART:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    const-string/jumbo v1, "CANCEL"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->CANCEL:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    .line 75
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->IDLE:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->SOUND:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->TTS:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->LISTENING:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->TIMEOUT:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->FINISH:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->PAUSE:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->RESTART:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->CANCEL:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
