.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$2;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->OnContactChoice(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$2;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    .line 2147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 2180
    const-string/jumbo v0, "MessageActivity"

    .line 2181
    const-string/jumbo v1, "OnContactChoice - setAutoShrink : false n true"

    .line 2180
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2182
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$2;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 2183
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$2;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 2184
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 3
    .param p1, "arg0"    # I
    .param p2, "arg1"    # F
    .param p3, "arg2"    # I

    .prologue
    const/4 v2, 0x0

    .line 2160
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$2;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->isLoaded:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2161
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$2;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;Z)V

    .line 2162
    if-nez p1, :cond_1

    .line 2163
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$2;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$28(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    .line 2164
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$2;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v1

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$25(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    move-result-object v1

    .line 2165
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->getInitialPosition()I

    move-result v1

    .line 2164
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2166
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$2;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2167
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$2;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$25(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2168
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$2;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$25(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    move-result-object v0

    .line 2169
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->getPageCount()I

    move-result v0

    .line 2168
    if-eqz v0, :cond_0

    .line 2170
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$2;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 2176
    :cond_0
    :goto_0
    return-void

    .line 2172
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$2;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 2173
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$2;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v1

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$25(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 2172
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    goto :goto_0
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 2151
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$2;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$25(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->getPageCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2152
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$2;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 2153
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$2;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v1

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$25(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 2152
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 2155
    :cond_0
    return-void
.end method
