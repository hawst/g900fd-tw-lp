.class Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;
.super Ljava/lang/Object;
.source "SettingsAddContactsActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseRequestContactList(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    const/16 v3, 0x8

    const/4 v4, 0x0

    .line 226
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$10(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Z)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$13(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Ljava/util/ArrayList;)V

    .line 229
    const-string/jumbo v0, "[SettingsAddContactsActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "dlServiceInterface onResponseRequestContactList : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mCommonList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$14(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mCommonList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$14(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 232
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->tvNoListLayout:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$15(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 233
    const v2, 0x7f0a020e

    .line 232
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->noListLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$16(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->lvFavorites:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$11(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->noListLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$16(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->lvFavorites:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$11(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVisibility(I)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    .line 240
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mCommonList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$14(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->buildItemList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 239
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$17(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Ljava/util/List;)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->adapter:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$12(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 243
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->adapter:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$12(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->mImageLoader:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->stopThread()V

    .line 245
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    .line 246
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Landroid/content/Context;)V

    .line 245
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$18(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mFavoriteContactsFetched:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$19(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->lvFavorites:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$11(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->adapter:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$12(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-static {v0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$8(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Z)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-static {v0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$10(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Z)V

    goto :goto_0
.end method

.method public onResponseRequestSearchedContactList(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    const/16 v3, 0x8

    const/4 v4, 0x0

    .line 193
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$10(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Z)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$13(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Ljava/util/ArrayList;)V

    .line 196
    const-string/jumbo v0, "[SettingsAddContactsActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "dlServiceInterface onResponseRequestSearchedContactList : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mCommonList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$14(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mCommonList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$14(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 199
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->tvNoListLayout:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$15(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 200
    const v2, 0x7f0a035a

    .line 199
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->noListLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$16(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->lvFavorites:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$11(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->noListLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$16(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 205
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->lvFavorites:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$11(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVisibility(I)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    .line 207
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mCommonList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$14(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->buildItemList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 206
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$17(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Ljava/util/List;)V

    .line 208
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->adapter:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$12(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 209
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->adapter:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$12(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->mImageLoader:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->stopThread()V

    .line 211
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    .line 212
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Landroid/content/Context;)V

    .line 211
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$18(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)V

    .line 214
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mFavoriteContactsFetched:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$19(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-static {v0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$8(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Z)V

    .line 216
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-static {v0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$10(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Z)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->lvFavorites:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$11(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->adapter:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$12(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method
