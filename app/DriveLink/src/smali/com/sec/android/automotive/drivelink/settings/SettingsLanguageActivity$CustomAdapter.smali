.class Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$CustomAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SettingsLanguageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CustomAdapter"
.end annotation


# instance fields
.field context:Landroid/app/Activity;

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;Landroid/app/Activity;)V
    .locals 2
    .param p2, "context"    # Landroid/app/Activity;

    .prologue
    .line 401
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$CustomAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;

    .line 402
    const v0, 0x7f030088

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->settableLanguageDescriptions:Ljava/util/ArrayList;
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, p2, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 403
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$CustomAdapter;->context:Landroid/app/Activity;

    .line 404
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v7, 0xf5

    .line 408
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$CustomAdapter;->context:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 409
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f030088

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 410
    .local v4, "row":Landroid/view/View;
    const v5, 0x7f09027c

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 411
    .local v2, "label":Landroid/widget/TextView;
    const v5, 0x7f09027d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 412
    .local v0, "icon":Landroid/widget/ImageView;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$CustomAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->settableLanguageDescriptions:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 413
    invoke-static {v7, v7, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 414
    const-string/jumbo v5, "language"

    const-string/jumbo v6, "en-US"

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 415
    .local v3, "language":Ljava/lang/String;
    if-eqz v3, :cond_0

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$CustomAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->settableLanguages:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 416
    const v5, 0x7f02041e

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 420
    :goto_0
    return-object v4

    .line 418
    :cond_0
    const v5, 0x7f02041d

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method
