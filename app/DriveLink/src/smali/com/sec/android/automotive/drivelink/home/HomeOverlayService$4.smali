.class Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;
.super Ljava/lang/Object;
.source "HomeOverlayService.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    .line 603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFlowCommandCall(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 670
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandCall(Ljava/lang/String;)V

    .line 673
    :cond_0
    return-void
.end method

.method public onFlowCommandCancel(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 703
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 704
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandCancel(Ljava/lang/String;)V

    .line 706
    :cond_0
    return-void
.end method

.method public onFlowCommandCustom(Ljava/lang/String;)Z
    .locals 1
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 691
    const/4 v0, 0x0

    return v0
.end method

.method public onFlowCommandExcute(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 628
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 629
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandExcute(Ljava/lang/String;)V

    .line 631
    :cond_0
    return-void
.end method

.method public onFlowCommandIgnore(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 635
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 636
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandIgnore(Ljava/lang/String;)V

    .line 638
    :cond_0
    return-void
.end method

.method public onFlowCommandLookup(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 642
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 643
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandLookup(Ljava/lang/String;)V

    .line 645
    :cond_0
    return-void
.end method

.method public onFlowCommandNext(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 684
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 685
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandNext(Ljava/lang/String;)V

    .line 687
    :cond_0
    return-void
.end method

.method public onFlowCommandNo(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 614
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 615
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandNo(Ljava/lang/String;)V

    .line 617
    :cond_0
    return-void
.end method

.method public onFlowCommandRead(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 677
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 678
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandRead(Ljava/lang/String;)V

    .line 680
    :cond_0
    return-void
.end method

.method public onFlowCommandReply(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 663
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 664
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandReply(Ljava/lang/String;)V

    .line 666
    :cond_0
    return-void
.end method

.method public onFlowCommandReset(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 649
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 650
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandReset(Ljava/lang/String;)V

    .line 652
    :cond_0
    return-void
.end method

.method public onFlowCommandReturnMain()V
    .locals 1

    .prologue
    .line 696
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 697
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandReturnMain()V

    .line 699
    :cond_0
    return-void
.end method

.method public onFlowCommandRoute(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 656
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 657
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandRoute(Ljava/lang/String;)V

    .line 659
    :cond_0
    return-void
.end method

.method public onFlowCommandSend(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 621
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 622
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandSend(Ljava/lang/String;)V

    .line 624
    :cond_0
    return-void
.end method

.method public onFlowCommandYes(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 607
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 608
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandYes(Ljava/lang/String;)V

    .line 610
    :cond_0
    return-void
.end method
