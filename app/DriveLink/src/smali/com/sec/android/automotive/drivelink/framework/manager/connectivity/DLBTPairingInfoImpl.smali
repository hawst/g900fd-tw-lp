.class public Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;
.source "DLBTPairingInfoImpl.java"


# instance fields
.field public mBTPairingType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

.field public mDeviceName:Ljava/lang/String;

.field public mPassKey:I


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;Ljava/lang/String;I)V
    .locals 0
    .param p1, "btPairingType"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "passKey"    # I

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;->mBTPairingType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    .line 14
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;->mDeviceName:Ljava/lang/String;

    .line 15
    iput p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;->mPassKey:I

    .line 16
    return-void
.end method


# virtual methods
.method public getBTPairingType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;->mBTPairingType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getPassKey()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;->mPassKey:I

    return v0
.end method
