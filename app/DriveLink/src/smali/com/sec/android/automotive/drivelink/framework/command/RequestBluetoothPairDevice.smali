.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairDevice;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestBluetoothPairDevice.java"


# instance fields
.field private mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

.field private mBluetoothDevicePaired:I

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 19
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 21
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairDevice;->mContext:Landroid/content/Context;

    .line 22
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairDevice;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 23
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 47
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairDevice;->mContext:Landroid/content/Context;

    .line 48
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairDevice;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 49
    return-void
.end method

.method protected process()V
    .locals 3

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairDevice;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 29
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getConnectivityManager()Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v0

    .line 31
    .local v0, "connectivityManager":Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairDevice;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairDevice;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 30
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->pairBluetoothDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairDevice;->mBluetoothDevicePaired:I

    .line 32
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairDevice;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 38
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkConnectivityListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;

    move-result-object v0

    .line 39
    .local v0, "connectivityListener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;
    if-eqz v0, :cond_0

    .line 41
    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairDevice;->mBluetoothDevicePaired:I

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;->onResponseRequestBluetoothPairDevice(I)V

    .line 43
    :cond_0
    return-void
.end method
