.class public Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "SettingsRegisterCarActivity.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final KEY_DISCONNECT_BT_ADDRESS:Ljava/lang/String; = "KEY_DISCONNECT_BT_ADDRESS"

.field private static final KEY_DISCONNECT_DIALOG_IS_OPEN:Ljava/lang/String; = "KEY_DISCONNECT_DIALOG_IS_OPEN"

.field private static final KEY_DISCONNECT_NAME:Ljava/lang/String; = "KEY_DISCONNECT_NAME"

.field private static final KEY_PENDING_BT_ADDRESS:Ljava/lang/String; = "KEY_PENDING_BT_ADDRESS"

.field private static final KEY_PENDING_TASK_TYPE:Ljava/lang/String; = "KEY_PENDING_TASK_TYPE"

.field private static final TAG:Ljava/lang/String; = "[SettingsRegisterCarActivity]"


# instance fields
.field private cbAutoLaunch:Landroid/widget/CheckBox;

.field private layoutSettingsAddCar:Landroid/widget/LinearLayout;

.field private layoutSettingsMyCarAutoLaunch:Landroid/widget/LinearLayout;

.field private layoutSettingsRegisteredCars:Landroid/widget/LinearLayout;

.field private mAdapter:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

.field private mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

.field private preference:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->cbAutoLaunch:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->preference:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    return-object v0
.end method


# virtual methods
.method public isConnectedDevice(Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;)Z
    .locals 2
    .param p1, "settingsCarBTDevice"    # Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    .prologue
    .line 262
    const-string/jumbo v0, "[SettingsRegisterCarActivity]"

    const-string/jumbo v1, "isConnectedDevice"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->getConnectedDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isSameDevice(Landroid/bluetooth/BluetoothDevice;Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;)Z

    move-result v0

    .line 265
    if-nez v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->getConnectedDevice2()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .line 266
    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isSameDevice(Landroid/bluetooth/BluetoothDevice;Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;)Z

    move-result v0

    .line 268
    if-eqz v0, :cond_1

    .line 269
    :cond_0
    const/4 v0, 0x1

    .line 271
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v9, 0x7f0a03e5

    const/4 v8, 0x0

    .line 72
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 74
    const v4, 0x7f03002f

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->setContentView(I)V

    .line 76
    const-string/jumbo v4, "[SettingsRegisterCarActivity]"

    const-string/jumbo v5, "onCreate"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v4

    .line 78
    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 80
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    invoke-interface {v4, p0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestBluetoothStartAdapter(Landroid/content/Context;)V

    .line 82
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->preference:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    .line 85
    const v4, 0x7f090144

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 84
    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->layoutSettingsRegisteredCars:Landroid/widget/LinearLayout;

    .line 86
    new-instance v4, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;)V

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mAdapter:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    .line 88
    new-instance v4, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mAdapter:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    invoke-direct {v4, v5}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;)V

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    .line 89
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    invoke-virtual {v4, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->register(Landroid/content/Context;)V

    .line 90
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->setInitialValues(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;)V

    .line 92
    if-eqz p1, :cond_1

    .line 94
    const-string/jumbo v4, "KEY_DISCONNECT_DIALOG_IS_OPEN"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 95
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mAdapter:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    .line 96
    const-string/jumbo v5, "KEY_DISCONNECT_DIALOG_IS_OPEN"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 95
    iput-boolean v5, v4, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->isDialogOpen:Z

    .line 97
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mAdapter:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    .line 98
    const-string/jumbo v5, "KEY_DISCONNECT_NAME"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 97
    iput-object v5, v4, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->disconnectName:Ljava/lang/String;

    .line 99
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mAdapter:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    iget-boolean v4, v4, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->isDialogOpen:Z

    if-eqz v4, :cond_0

    .line 100
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mAdapter:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->showDisconnectDialog()V

    .line 102
    :cond_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mAdapter:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    .line 103
    const-string/jumbo v5, "KEY_DISCONNECT_BT_ADDRESS"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 102
    iput-object v5, v4, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->disconnectBtAddress:Ljava/lang/String;

    .line 105
    const-string/jumbo v4, "KEY_PENDING_BT_ADDRESS"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 107
    .local v2, "pendingBtAddresses":[Ljava/lang/String;
    const-string/jumbo v4, "KEY_PENDING_TASK_TYPE"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v3

    .line 108
    .local v3, "pendingTaskTypes":[Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v3

    if-lt v1, v4, :cond_2

    .line 115
    .end local v1    # "i":I
    .end local v2    # "pendingBtAddresses":[Ljava/lang/String;
    .end local v3    # "pendingTaskTypes":[Z
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v8, v5, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 118
    const v4, 0x7f090143

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 117
    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->layoutSettingsAddCar:Landroid/widget/LinearLayout;

    .line 119
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->layoutSettingsAddCar:Landroid/widget/LinearLayout;

    new-instance v5, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity$1;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    const v4, 0x7f090142

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->cbAutoLaunch:Landroid/widget/CheckBox;

    .line 130
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->cbAutoLaunch:Landroid/widget/CheckBox;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->preference:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    .line 131
    const-string/jumbo v6, "PREF_SETTINGS_MY_CAR_AUTO_LAUNCH"

    .line 130
    invoke-virtual {v5, v6, v8}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 132
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->cbAutoLaunch:Landroid/widget/CheckBox;

    .line 133
    const v5, 0x7f0a03ef

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 132
    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 135
    const v4, 0x7f090141

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 134
    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->layoutSettingsMyCarAutoLaunch:Landroid/widget/LinearLayout;

    .line 136
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->layoutSettingsMyCarAutoLaunch:Landroid/widget/LinearLayout;

    .line 137
    new-instance v5, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity$2;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    const v4, 0x7f090005

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 167
    check-cast v0, Landroid/widget/LinearLayout;

    .line 169
    .local v0, "btnNavBack":Landroid/widget/LinearLayout;
    new-instance v4, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity$3;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;)V

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    new-instance v4, Ljava/lang/StringBuilder;

    .line 176
    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 177
    const-string/jumbo v5, ". "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 178
    const v5, 0x7f0a03f4

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 175
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 180
    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->setTitle(I)V

    .line 182
    return-void

    .line 109
    .end local v0    # "btnNavBack":Landroid/widget/LinearLayout;
    .restart local v1    # "i":I
    .restart local v2    # "pendingBtAddresses":[Ljava/lang/String;
    .restart local v3    # "pendingTaskTypes":[Z
    :cond_2
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mAdapter:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    iget-object v4, v4, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->pendingTasks:Ljava/util/Queue;

    .line 110
    new-instance v5, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;

    .line 111
    aget-object v6, v2, v1

    aget-boolean v7, v3, v1

    .line 110
    invoke-direct {v5, v6, v7}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v4, v5}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 108
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 301
    new-instance v0, Landroid/content/CursorLoader;

    sget-object v2, Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    .line 302
    sget-object v3, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->FR_RESULTSET_COLUMNS:[Ljava/lang/String;

    .line 303
    const-string/jumbo v4, "type = ? and (category = ? or _id = 2) and location_type = ? and bluetooth_mac_address not null"

    sget-object v5, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->CAR_SELECTION_ARGS:[Ljava/lang/String;

    .line 304
    const/4 v6, 0x0

    move-object v1, p0

    .line 301
    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 220
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onDestroy()V

    .line 221
    const-string/jumbo v0, "[SettingsRegisterCarActivity]"

    const-string/jumbo v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 223
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    .line 224
    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 316
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mAdapter:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    invoke-virtual {v0, p2}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 317
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->refreshList()V

    .line 318
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Landroid/content/Loader;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 329
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mAdapter:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 330
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->layoutSettingsRegisteredCars:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 331
    const-string/jumbo v0, "[SettingsRegisterCarActivity]"

    const-string/jumbo v1, "onLoaderReset"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 228
    const-string/jumbo v0, "[SettingsRegisterCarActivity]"

    const-string/jumbo v1, "onPause"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onPause()V

    .line 230
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 234
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onResume()V

    .line 235
    const-string/jumbo v0, "[SettingsRegisterCarActivity]"

    const-string/jumbo v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    const-string/jumbo v0, "[SettingsRegisterCarActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "DrivingStatus :  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 239
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 238
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->SettingsDrivingDialog(Landroid/content/Context;)V

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->cbAutoLaunch:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->preference:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    .line 244
    const-string/jumbo v2, "PREF_SETTINGS_MY_CAR_AUTO_LAUNCH"

    const/4 v3, 0x1

    .line 243
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 247
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->refreshList()V

    .line 248
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 186
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 188
    const-string/jumbo v4, "KEY_DISCONNECT_DIALOG_IS_OPEN"

    .line 189
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mAdapter:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    iget-boolean v5, v5, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->isDialogOpen:Z

    .line 187
    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 190
    const-string/jumbo v4, "KEY_DISCONNECT_NAME"

    .line 191
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mAdapter:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->disconnectName:Ljava/lang/String;

    .line 190
    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const-string/jumbo v4, "KEY_DISCONNECT_BT_ADDRESS"

    .line 194
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mAdapter:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->disconnectBtAddress:Ljava/lang/String;

    .line 192
    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mAdapter:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    iget-object v4, v4, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->pendingTasks:Ljava/util/Queue;

    .line 196
    invoke-interface {v4}, Ljava/util/Queue;->size()I

    move-result v4

    .line 195
    new-array v1, v4, [Ljava/lang/String;

    .line 197
    .local v1, "pendingBtAddresses":[Ljava/lang/String;
    array-length v4, v1

    new-array v2, v4, [Z

    .line 198
    .local v2, "pendingTaskTypes":[Z
    const/4 v0, 0x0

    .line 199
    .local v0, "i":I
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mAdapter:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    iget-object v4, v4, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->pendingTasks:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 205
    const-string/jumbo v4, "KEY_PENDING_BT_ADDRESS"

    .line 204
    invoke-virtual {p1, v4, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 208
    const-string/jumbo v4, "KEY_PENDING_TASK_TYPE"

    .line 207
    invoke-virtual {p1, v4, v2}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 210
    return-void

    .line 199
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;

    .line 200
    .local v3, "task":Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;
    iget-object v5, v3, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;->btAddress:Ljava/lang/String;

    aput-object v5, v1, v0

    .line 201
    iget-boolean v5, v3, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;->connect:Z

    aput-boolean v5, v2, v0

    .line 202
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected refreshList()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 283
    const-string/jumbo v2, "[SettingsRegisterCarActivity]"

    const-string/jumbo v3, "refreshList"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->layoutSettingsRegisteredCars:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 285
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mAdapter:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->getCount()I

    move-result v0

    .line 286
    .local v0, "adapterCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 290
    return-void

    .line 287
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->layoutSettingsRegisteredCars:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->mAdapter:Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;

    invoke-virtual {v3, v1, v4, v4}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 286
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
