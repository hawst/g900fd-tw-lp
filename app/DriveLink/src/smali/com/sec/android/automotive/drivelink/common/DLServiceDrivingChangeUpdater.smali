.class public Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;
.super Ljava/lang/Object;
.source "DLServiceDrivingChangeUpdater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;,
        Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;
    }
.end annotation


# static fields
.field private static mInstance:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;


# instance fields
.field private mDrivingChangeListener:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->mDrivingChangeListener:Ljava/util/ArrayList;

    .line 22
    return-void
.end method

.method public static getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->mInstance:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;

    if-nez v0, :cond_0

    .line 26
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->mInstance:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;

    .line 29
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->mInstance:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;

    return-object v0
.end method


# virtual methods
.method public addDrivingChangeListener(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;)V
    .locals 1
    .param p1, "viv"    # Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->mDrivingChangeListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    return-void
.end method

.method public onChangeStatus(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;)V
    .locals 3
    .param p1, "type"    # Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    .prologue
    .line 46
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->mDrivingChangeListener:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 50
    return-void

    .line 46
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    .line 47
    .local v0, "viv":Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;
    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;->onDriveStatusChanged(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;)V

    goto :goto_0
.end method

.method public removeAllDrivingChangeListener()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->mDrivingChangeListener:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 43
    return-void
.end method

.method public removeDrivingChangeListener(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;)V
    .locals 1
    .param p1, "viv"    # Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->mDrivingChangeListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 39
    return-void
.end method
