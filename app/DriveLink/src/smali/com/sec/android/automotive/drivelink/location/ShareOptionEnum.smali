.class public final enum Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;
.super Ljava/lang/Enum;
.source "ShareOptionEnum.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

.field public static final enum SEND_LOCATION:Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

.field public static final enum SHARE_GROUP:Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

.field public static final enum SHARE_REQUEST:Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;


# instance fields
.field private imgId:I

.field private textId:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 22
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    const-string/jumbo v1, "SEND_LOCATION"

    const v2, 0x7f0a04de

    .line 23
    const v3, 0x7f020208

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;-><init>(Ljava/lang/String;III)V

    .line 22
    sput-object v0, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;->SEND_LOCATION:Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    .line 23
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    const-string/jumbo v1, "SHARE_REQUEST"

    .line 24
    const v2, 0x7f0a04ec

    .line 25
    const v3, 0x7f020207

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;-><init>(Ljava/lang/String;III)V

    .line 23
    sput-object v0, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;->SHARE_REQUEST:Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    .line 25
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    const-string/jumbo v1, "SHARE_GROUP"

    .line 26
    const v2, 0x7f0a04ed

    const v3, 0x7f020206

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;-><init>(Ljava/lang/String;III)V

    .line 25
    sput-object v0, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;->SHARE_GROUP:Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    .line 20
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    sget-object v1, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;->SEND_LOCATION:Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;->SHARE_REQUEST:Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;->SHARE_GROUP:Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "textId"    # I
    .param p4, "imgId"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    iput p3, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;->textId:I

    .line 33
    iput p4, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;->imgId:I

    .line 34
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getIconId()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;->imgId:I

    return v0
.end method

.method public getOptionText(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/ShareOptionEnum;->textId:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
