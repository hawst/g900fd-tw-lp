.class Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$9;
.super Ljava/lang/Object;
.source "SettingsRejectMessageActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->switchRejectMessage:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    .line 252
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 254
    const/4 v1, 0x5

    .line 253
    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 257
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 258
    const-string/jumbo v1, "drivelink_rejectmessage_on"

    .line 259
    const/4 v2, 0x1

    .line 256
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 271
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->switchRejectMessage:Landroid/widget/Switch;

    invoke-virtual {v1}, Landroid/widget/Switch;->isChecked()Z

    move-result v1

    # invokes: Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->enableLayout(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;Z)V

    .line 272
    return-void

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    .line 262
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 264
    const/4 v1, 0x3

    .line 263
    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 267
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 268
    const-string/jumbo v1, "drivelink_rejectmessage_on"

    .line 269
    const/4 v2, 0x0

    .line 266
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method
