.class Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$1;
.super Ljava/lang/Object;
.source "UpdateManager.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageInstalled;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public packageInstalled(Ljava/lang/String;I)V
    .locals 3
    .param p1, "pkgname"    # Ljava/lang/String;
    .param p2, "returncode"    # I

    .prologue
    .line 61
    const-string/jumbo v0, "UpdateManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "returnCode"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->sendUpdateSuccessResult()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;)V

    .line 74
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mFileUtil:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;)Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mFileUtil:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;)Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->delete()Z

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->INSTALL_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->sendUpdateFailResult(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;)V

    goto :goto_0
.end method
