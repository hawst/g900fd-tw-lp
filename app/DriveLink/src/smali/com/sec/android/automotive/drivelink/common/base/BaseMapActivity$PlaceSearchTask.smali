.class public Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;
.super Landroid/os/AsyncTask;
.source "BaseMapActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PlaceSearchTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private address:Ljava/lang/String;

.field private latitude:D

.field private longitude:D

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;DDLjava/lang/String;)V
    .locals 2
    .param p2, "lat"    # D
    .param p4, "lng"    # D
    .param p6, "addr"    # Ljava/lang/String;

    .prologue
    const-wide/16 v0, 0x0

    .line 1058
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1054
    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->latitude:D

    .line 1055
    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->longitude:D

    .line 1056
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->address:Ljava/lang/String;

    .line 1059
    iput-wide p2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->latitude:D

    .line 1060
    iput-wide p4, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->longitude:D

    .line 1061
    iput-object p6, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->address:Ljava/lang/String;

    .line 1062
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 10
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 1067
    const/4 v7, 0x0

    .line 1069
    .local v7, "latLng":Lcom/google/android/gms/maps/model/LatLng;
    :try_start_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/places/PlaceServiceFactory;->getPlaceService()Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;

    move-result-object v0

    .line 1070
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->address:Ljava/lang/String;

    const-string/jumbo v2, "UTF-8"

    invoke-static {v1, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->latitude:D

    .line 1071
    iget-wide v4, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->longitude:D

    .line 1069
    invoke-interface/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;->nearbySearch(Ljava/lang/String;DD)Ljava/util/List;

    move-result-object v9

    .line 1072
    .local v9, "mPlaces":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    if-eqz v9, :cond_4

    .line 1073
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;

    .line 1074
    .local v8, "mPlace":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->getLat()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->latitude:D

    .line 1075
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->getLng()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->longitude:D

    .line 1076
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->getAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->address:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1092
    .end local v8    # "mPlace":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    .end local v9    # "mPlaces":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->address:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->address:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1093
    :cond_1
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->latitude:D

    .line 1094
    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->longitude:D

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    .line 1093
    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/location/map/util/GeocodingAPI;->getAddressFromLatLng(DDLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->address:Ljava/lang/String;

    .line 1096
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->address:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->address:Ljava/lang/String;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1097
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a021b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->address:Ljava/lang/String;

    .line 1100
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->address:Ljava/lang/String;

    return-object v0

    .line 1078
    .restart local v9    # "mPlaces":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :cond_4
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->address:Ljava/lang/String;

    .line 1079
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->address:Ljava/lang/String;

    .line 1080
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1079
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/util/GeocodingAPI;->getLatLngFromAddress(Ljava/lang/String;Landroid/content/Context;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v7

    .line 1082
    if-eqz v7, :cond_0

    .line 1083
    iget-wide v0, v7, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->latitude:D

    .line 1084
    iget-wide v0, v7, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->longitude:D
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1088
    .end local v9    # "mPlaces":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :catch_0
    move-exception v6

    .line 1089
    .local v6, "e":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->finishIsOffline()Z

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 6
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 1106
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$4(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Ljava/lang/String;)V

    .line 1107
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    iget-wide v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->latitude:D

    iget-wide v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->longitude:D

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setLocationPoint(DD)V

    .line 1108
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$6(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->latitude:D

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->setLatitude(D)V

    .line 1109
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$6(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->longitude:D

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->setLongitude(D)V

    .line 1111
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    iget-wide v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->latitude:D

    iget-wide v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->longitude:D

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->createMarks(DDLjava/lang/String;)V

    .line 1113
    return-void
.end method
