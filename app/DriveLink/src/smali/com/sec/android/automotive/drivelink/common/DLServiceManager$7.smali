.class Lcom/sec/android/automotive/drivelink/common/DLServiceManager$7;
.super Ljava/lang/Object;
.source "DLServiceManager.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getOnDLDrivingListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$7;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    .line 925
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNotifyCarSpeedStatusChange(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;)V
    .locals 2
    .param p1, "currentCarSpeed"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;

    .prologue
    .line 964
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$7;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$51(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;)V

    .line 965
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$7;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$52(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 966
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$7;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$52(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;->getCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;->CAR_SPEED_STATUS_STOPPED:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    if-ne v0, v1, :cond_0

    .line 967
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$7;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$50(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 973
    :goto_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;

    move-result-object v0

    .line 974
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->STATUS_NOTIFY_CAR_SPEED_STATUS_CHANGE:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    .line 973
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->onChangeStatus(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;)V

    .line 979
    :goto_1
    return-void

    .line 969
    :cond_0
    const-string/jumbo v0, "DLServiceManager"

    const-string/jumbo v1, "mIsDriving... set true"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$7;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$50(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    goto :goto_0

    .line 976
    :cond_1
    const-string/jumbo v0, "DLServiceManager"

    .line 977
    const-string/jumbo v1, "onNotifyCarSpeedStatusChange mCarSpeedStatus = null"

    .line 976
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onResponseRequestStartDrivingStatusMonitoring(Z)V
    .locals 5
    .param p1, "result"    # Z

    .prologue
    const/4 v1, 0x0

    .line 931
    const-string/jumbo v2, "onResponseRequestStartDrivingStatusMonitoring"

    .line 932
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, " itemCount = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 931
    # invokes: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->Debug(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$18(Ljava/lang/String;Ljava/lang/String;)V

    .line 934
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$7;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$50(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 935
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$7;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$3(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v2

    .line 936
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$7;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$4(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getCarSpeedStatus(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;

    move-result-object v0

    .line 937
    .local v0, "carSpeedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    if-eqz v0, :cond_1

    .line 938
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$7;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;->getCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    move-result-object v3

    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;->CAR_SPEED_STATUS_STOPPED:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    if-eq v3, v4, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$50(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 941
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;

    move-result-object v1

    .line 942
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->STATUS_REQUEST_START_DRIVING:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    .line 941
    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->onChangeStatus(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;)V

    .line 943
    return-void
.end method

.method public onResponseRequestStopDrivingStatusMonitoring(Z)V
    .locals 3
    .param p1, "result"    # Z

    .prologue
    .line 949
    const-string/jumbo v0, "onResponseRequestStopDrivingStatusMonitoring"

    .line 950
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, " itemCount = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 949
    # invokes: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->Debug(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$18(Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    if-eqz p1, :cond_0

    .line 953
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$7;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$50(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 955
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;

    move-result-object v0

    .line 956
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->STATUS_REQUEST_STOP_DRIVING:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    .line 955
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->onChangeStatus(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;)V

    .line 958
    return-void
.end method
