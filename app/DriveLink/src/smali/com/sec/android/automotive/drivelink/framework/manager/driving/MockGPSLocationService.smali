.class public Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;
.super Landroid/app/Service;
.source "MockGPSLocationService.java"


# static fields
.field public static final LOCATION_ELAPSED_REAL_TIME_NANOS:Ljava/lang/String; = "MockGPSLocationService.ElapsedRealTimeNanos"

.field public static final LOCATION_LATITUDE:Ljava/lang/String; = "MockGPSLocationService.Latitude"

.field public static final LOCATION_LONGITUDE:Ljava/lang/String; = "MockGPSLocationService.Longitude"

.field public static final LOCATION_SPEED:Ljava/lang/String; = "MockGPSLocationService.Speed"

.field public static final MOCK_GPS_LOCATION:Ljava/lang/String; = "com.sec.android.automotive.drivelink.framework.manager.driving.DrivingManager.MockGPSLocationService.MockGPSLocation"

.field public static final MOCK_LOCATION_PROVIDER:Ljava/lang/String; = "MOCK_LOCATION_PROVIDER"


# instance fields
.field private mDummyBootupTime:J

.field private mLocationGeneratorThread:Ljava/lang/Thread;

.field mMockLocations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mThreadExit:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 36
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 30
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->mLocationGeneratorThread:Ljava/lang/Thread;

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->mThreadExit:Z

    .line 32
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->mDummyBootupTime:J

    .line 34
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->mMockLocations:Ljava/util/List;

    .line 38
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;)V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->generateMockLocationsFromFile()V

    return-void
.end method

.method private generateMockLocations()V
    .locals 26

    .prologue
    .line 178
    const/16 v22, 0x64

    .line 179
    .local v22, "waitTime":I
    const-wide v16, 0x402d19999999999aL    # 14.55

    .line 180
    .local v16, "originLatitude":D
    const-wide v18, 0x405e41e353f7ced9L    # 121.0295

    .line 181
    .local v18, "originLongitude":D
    const-wide v2, 0x402d0f5c28f5c290L    # 14.530000000000001

    .line 182
    .local v2, "destinationLatitude":D
    const-wide v4, 0x405e432b020c49baL    # 121.0495

    .line 183
    .local v4, "destinationLongitude":D
    const-wide v13, 0x3ef4f8b588e368f1L    # 2.0E-5

    .line 184
    .local v13, "maxOffset":D
    new-instance v21, Ljava/util/Random;

    invoke-direct/range {v21 .. v21}, Ljava/util/Random;-><init>()V

    .line 185
    .local v21, "randomPosition":Ljava/util/Random;
    new-instance v20, Ljava/util/Random;

    invoke-direct/range {v20 .. v20}, Ljava/util/Random;-><init>()V

    .line 186
    .local v20, "randomOffset":Ljava/util/Random;
    const/4 v7, 0x1

    .line 188
    .local v7, "goForward":Z
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->mThreadExit:Z

    move/from16 v23, v0

    if-eqz v23, :cond_1

    .line 238
    return-void

    .line 191
    :cond_1
    move-wide/from16 v9, v16

    .line 192
    .local v9, "latitude":D
    move-wide/from16 v11, v18

    .line 194
    .local v11, "longitude":D
    const/4 v8, 0x0

    .local v8, "index":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->mMockLocations:Ljava/util/List;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Ljava/util/List;->size()I

    move-result v23

    move/from16 v0, v23

    if-ge v8, v0, :cond_0

    .line 197
    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v23, v0

    :try_start_0
    invoke-static/range {v23 .. v24}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    :goto_1
    new-instance v15, Landroid/content/Intent;

    const-string/jumbo v23, "com.sec.android.automotive.drivelink.framework.manager.driving.DrivingManager.MockGPSLocationService.MockGPSLocation"

    move-object/from16 v0, v23

    invoke-direct {v15, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 204
    .local v15, "mockGPSLocationIntent":Landroid/content/Intent;
    const-string/jumbo v23, "MockGPSLocationService.Latitude"

    move-object/from16 v0, v23

    invoke-virtual {v15, v0, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 205
    const-string/jumbo v23, "MockGPSLocationService.Longitude"

    move-object/from16 v0, v23

    invoke-virtual {v15, v0, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 207
    const-string/jumbo v23, "MockGPSLocationService.generateMockLocations()"

    .line 208
    new-instance v24, Ljava/lang/StringBuilder;

    const-string/jumbo v25, "Location: "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string/jumbo v25, ", "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v11, v12}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 207
    invoke-static/range {v23 .. v24}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->sendBroadcast(Landroid/content/Intent;)V

    .line 211
    if-eqz v7, :cond_4

    .line 212
    const/16 v23, 0x2

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v23

    if-nez v23, :cond_3

    .line 213
    invoke-virtual/range {v20 .. v20}, Ljava/util/Random;->nextDouble()D

    move-result-wide v23

    mul-double v23, v23, v13

    sub-double v9, v9, v23

    .line 214
    cmpg-double v23, v9, v2

    if-gez v23, :cond_2

    .line 215
    const/4 v7, 0x0

    .line 194
    :cond_2
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 198
    .end local v15    # "mockGPSLocationIntent":Landroid/content/Intent;
    :catch_0
    move-exception v6

    .line 200
    .local v6, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v6}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 218
    .end local v6    # "e":Ljava/lang/InterruptedException;
    .restart local v15    # "mockGPSLocationIntent":Landroid/content/Intent;
    :cond_3
    invoke-virtual/range {v20 .. v20}, Ljava/util/Random;->nextDouble()D

    move-result-wide v23

    mul-double v23, v23, v13

    add-double v11, v11, v23

    .line 219
    cmpl-double v23, v11, v4

    if-lez v23, :cond_2

    .line 220
    const/4 v7, 0x0

    .line 223
    goto :goto_2

    .line 224
    :cond_4
    const/16 v23, 0x2

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v23

    if-nez v23, :cond_5

    .line 225
    invoke-virtual/range {v20 .. v20}, Ljava/util/Random;->nextDouble()D

    move-result-wide v23

    mul-double v23, v23, v13

    add-double v9, v9, v23

    .line 226
    cmpl-double v23, v9, v16

    if-lez v23, :cond_2

    .line 227
    const/4 v7, 0x1

    .line 229
    goto :goto_2

    .line 230
    :cond_5
    invoke-virtual/range {v20 .. v20}, Ljava/util/Random;->nextDouble()D

    move-result-wide v23

    mul-double v23, v23, v13

    sub-double v11, v11, v23

    .line 231
    cmpg-double v23, v11, v18

    if-gez v23, :cond_2

    .line 232
    const/4 v7, 0x1

    goto :goto_2
.end method

.method private generateMockLocationsFromFile()V
    .locals 19

    .prologue
    .line 127
    const/16 v14, 0x3e8

    .line 132
    .local v14, "waitTime":I
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->mMockLocations:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    if-lt v2, v15, :cond_0

    .line 173
    return-void

    .line 135
    :cond_0
    int-to-long v15, v14

    :try_start_0
    invoke-static/range {v15 .. v16}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 141
    :goto_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->mMockLocations:Ljava/util/List;

    invoke-interface {v15, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    const-string/jumbo v16, ","

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 142
    .local v5, "locationTokens":[Ljava/lang/String;
    const/4 v15, 0x0

    aget-object v15, v5, v15

    invoke-static {v15}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    .line 143
    .local v3, "latitude":D
    const/4 v15, 0x1

    aget-object v15, v5, v15

    invoke-static {v15}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    .line 145
    .local v6, "longitude":D
    const/4 v15, 0x2

    aget-object v15, v5, v15

    invoke-static {v15}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    const-wide/16 v17, 0x3e8

    mul-long v12, v15, v17

    .line 146
    .local v12, "timeMillis":J
    const/4 v15, 0x3

    aget-object v15, v5, v15

    invoke-static {v15}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Float;->floatValue()F

    move-result v11

    .line 159
    .local v11, "speed":F
    move-object/from16 v0, p0

    iget-wide v15, v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->mDummyBootupTime:J

    sub-long v15, v12, v15

    const-wide/32 v17, 0xf4240

    mul-long v9, v15, v17

    .line 161
    .local v9, "realTimeNanos":J
    new-instance v8, Landroid/content/Intent;

    const-string/jumbo v15, "com.sec.android.automotive.drivelink.framework.manager.driving.DrivingManager.MockGPSLocationService.MockGPSLocation"

    invoke-direct {v8, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 162
    .local v8, "mockGPSLocationIntent":Landroid/content/Intent;
    const-string/jumbo v15, "MockGPSLocationService.Latitude"

    invoke-virtual {v8, v15, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 163
    const-string/jumbo v15, "MockGPSLocationService.Longitude"

    invoke-virtual {v8, v15, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 164
    const-string/jumbo v15, "MockGPSLocationService.ElapsedRealTimeNanos"

    invoke-virtual {v8, v15, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 166
    const-string/jumbo v15, "MockGPSLocationService.Speed"

    invoke-virtual {v8, v15, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 170
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->sendBroadcast(Landroid/content/Intent;)V

    .line 132
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 136
    .end local v3    # "latitude":D
    .end local v5    # "locationTokens":[Ljava/lang/String;
    .end local v6    # "longitude":D
    .end local v8    # "mockGPSLocationIntent":Landroid/content/Intent;
    .end local v9    # "realTimeNanos":J
    .end local v11    # "speed":F
    .end local v12    # "timeMillis":J
    :catch_0
    move-exception v1

    .line 138
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method

.method private initializeMockLocations()V
    .locals 7

    .prologue
    .line 97
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->mMockLocations:Ljava/util/List;

    .line 99
    const/4 v3, 0x0

    .line 101
    .local v3, "reader":Ljava/io/BufferedReader;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v5

    const-string/jumbo v6, "MockLocations.txt"

    invoke-virtual {v5, v6}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 102
    .local v1, "input":Ljava/io/InputStream;
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    const-string/jumbo v6, "UTF-8"

    invoke-direct {v5, v1, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .local v4, "reader":Ljava/io/BufferedReader;
    const/4 v2, 0x0

    .line 105
    .local v2, "locationInfo":Ljava/lang/String;
    :goto_0
    :try_start_1
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    if-nez v2, :cond_1

    .line 115
    if-eqz v4, :cond_3

    .line 116
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-object v3, v4

    .line 123
    .end local v1    # "input":Ljava/io/InputStream;
    .end local v2    # "locationInfo":Ljava/lang/String;
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :cond_0
    :goto_1
    return-void

    .line 107
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "input":Ljava/io/InputStream;
    .restart local v2    # "locationInfo":Ljava/lang/String;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :try_start_3
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->mMockLocations:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 110
    :catch_0
    move-exception v0

    move-object v3, v4

    .line 112
    .end local v1    # "input":Ljava/io/InputStream;
    .end local v2    # "locationInfo":Ljava/lang/String;
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .local v0, "e":Ljava/io/IOException;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :goto_2
    :try_start_4
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 115
    if-eqz v3, :cond_0

    .line 116
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 118
    :catch_1
    move-exception v0

    .line 120
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 113
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 115
    :goto_3
    if-eqz v3, :cond_2

    .line 116
    :try_start_6
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 122
    :cond_2
    :goto_4
    throw v5

    .line 118
    :catch_2
    move-exception v0

    .line 120
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 118
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "input":Ljava/io/InputStream;
    .restart local v2    # "locationInfo":Ljava/lang/String;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catch_3
    move-exception v0

    .line 120
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .line 113
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_3

    .line 110
    .end local v1    # "input":Ljava/io/InputStream;
    .end local v2    # "locationInfo":Ljava/lang/String;
    :catch_4
    move-exception v0

    goto :goto_2
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 43
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 7

    .prologue
    .line 49
    const-string/jumbo v5, "MockGPSLocationService"

    const-string/jumbo v6, "onCreate..."

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->initializeMockLocations()V

    .line 52
    new-instance v4, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService$1;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;)V

    .line 62
    .local v4, "runnable":Ljava/lang/Runnable;
    new-instance v5, Ljava/lang/Thread;

    invoke-direct {v5, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->mLocationGeneratorThread:Ljava/lang/Thread;

    .line 64
    const-string/jumbo v0, "2013-11-28 00:00:00"

    .line 65
    .local v0, "bootupDate":Ljava/lang/String;
    new-instance v2, Ljava/text/SimpleDateFormat;

    .line 66
    const-string/jumbo v5, "yyyy-MM-dd hh:mm:ss"

    .line 65
    invoke-direct {v2, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 68
    .local v2, "dateFormat":Ljava/text/SimpleDateFormat;
    :try_start_0
    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 69
    .local v1, "date":Ljava/util/Date;
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->mDummyBootupTime:J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    .end local v1    # "date":Ljava/util/Date;
    :goto_0
    return-void

    .line 70
    :catch_0
    move-exception v3

    .line 71
    .local v3, "e":Ljava/text/ParseException;
    const-string/jumbo v5, "MockGPSLocationService.onCreate()"

    const-string/jumbo v6, "Unable to parse Date!"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 78
    const-string/jumbo v0, "MockGPSLocationService"

    const-string/jumbo v1, "onDestroy..."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->mThreadExit:Z

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->mLocationGeneratorThread:Ljava/lang/Thread;

    .line 81
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 86
    const-string/jumbo v0, "MockGPSLocationService"

    const-string/jumbo v1, "onStartCommand..."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->mLocationGeneratorThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/MockGPSLocationService;->mLocationGeneratorThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 92
    :cond_0
    const/4 v0, 0x2

    return v0
.end method
