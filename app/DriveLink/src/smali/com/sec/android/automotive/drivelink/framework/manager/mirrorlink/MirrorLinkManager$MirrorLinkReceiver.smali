.class Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager$MirrorLinkReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MirrorLinkManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MirrorLinkReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;


# direct methods
.method private constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager$MirrorLinkReceiver;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager$MirrorLinkReceiver;)V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager$MirrorLinkReceiver;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 118
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 121
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v1, "samsung.intent.action.MIRRORING_STARTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager$MirrorLinkReceiver;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getNotificationManager()Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    move-result-object v1

    .line 123
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->NotifyMirrorLinkSetup()V

    .line 127
    :cond_0
    const-string/jumbo v1, "samsung.intent.action.MIRRORLINK_SHUTDOWN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 128
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager$MirrorLinkReceiver;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getNotificationManager()Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    move-result-object v1

    .line 129
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->NotifyMirrorLinkShutDown()V

    .line 131
    :cond_1
    return-void
.end method
