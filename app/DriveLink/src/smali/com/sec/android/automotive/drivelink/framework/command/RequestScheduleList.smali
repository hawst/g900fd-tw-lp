.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestScheduleList.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFromDate:Ljava/util/Date;

.field private mScheduleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;",
            ">;"
        }
    .end annotation
.end field

.field private mToDate:Ljava/util/Date;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/util/Date;Ljava/util/Date;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "fromDate"    # Ljava/util/Date;
    .param p4, "toDate"    # Ljava/util/Date;

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 14
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;->mContext:Landroid/content/Context;

    .line 15
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;->mFromDate:Ljava/util/Date;

    .line 16
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;->mToDate:Ljava/util/Date;

    .line 17
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;->mScheduleList:Ljava/util/ArrayList;

    .line 24
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;->mContext:Landroid/content/Context;

    .line 25
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;->mFromDate:Ljava/util/Date;

    .line 26
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;->mToDate:Ljava/util/Date;

    .line 27
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;->mContext:Landroid/content/Context;

    .line 50
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;->mFromDate:Ljava/util/Date;

    .line 51
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;->mToDate:Ljava/util/Date;

    .line 52
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;->mScheduleList:Ljava/util/ArrayList;

    .line 53
    return-void
.end method

.method protected process()V
    .locals 4

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 32
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 33
    .local v0, "contentManager":Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;->mFromDate:Ljava/util/Date;

    .line 34
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;->mToDate:Ljava/util/Date;

    .line 33
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getScheduleList(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;->mScheduleList:Ljava/util/ArrayList;

    .line 35
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 40
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkScheduleListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkScheduleListener;

    move-result-object v0

    .line 42
    .local v0, "scheduleListener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkScheduleListener;
    if-eqz v0, :cond_0

    .line 43
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;->mScheduleList:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkScheduleListener;->onResponseRequestSchduleList(Ljava/util/ArrayList;)V

    .line 45
    :cond_0
    return-void
.end method
