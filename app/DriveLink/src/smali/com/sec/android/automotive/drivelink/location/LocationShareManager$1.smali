.class Lcom/sec/android/automotive/drivelink/location/LocationShareManager$1;
.super Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;
.source "LocationShareManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->shareMyLocation(Landroid/content/Context;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

.field private final synthetic val$context:Landroid/content/Context;

.field private final synthetic val$friendPhoneNumber:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationShareManager;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$1;->val$friendPhoneNumber:Ljava/lang/String;

    .line 141
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;-><init>()V

    return-void
.end method


# virtual methods
.method protected onResponseRequestGeoLocation(Landroid/location/Location;Z)V
    .locals 4
    .param p1, "location"    # Landroid/location/Location;
    .param p2, "timeout"    # Z

    .prologue
    .line 146
    if-eqz p1, :cond_0

    if-eqz p2, :cond_1

    .line 147
    :cond_0
    const-string/jumbo v1, "LocationManager"

    const-string/jumbo v2, "Fail to get my current location!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$1;->val$context:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v1

    .line 151
    const v2, 0x7f0a0391

    .line 150
    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->onNotifyLocationFailedRequestSent(I)V

    .line 161
    :goto_0
    return-void

    .line 155
    :cond_1
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;-><init>()V

    .line 156
    .local v0, "dlLocation":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->setLatitude(D)V

    .line 157
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->setLongitude(D)V

    .line 159
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$1;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mDLinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationShareManager;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$1;->val$context:Landroid/content/Context;

    .line 160
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$1;->val$friendPhoneNumber:Ljava/lang/String;

    .line 159
    invoke-interface {v1, v2, v3, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestShareMyLocation(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    goto :goto_0
.end method
