.class public Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;
.super Ljava/lang/Object;
.source "Controller.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;,
        Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$RotateAnimateUpdater;
    }
.end annotation


# instance fields
.field private final DROP_DURATION:I

.field private final DROP_FROM_RATIO:F

.field private final DROP_TO_RATIO:F

.field private final QUADEASEOUT:Landroid/animation/TimeInterpolator;

.field private final ROTATE_DELAY:I

.field private final ROTATE_DURATION:I

.field private mDropAnimator:Landroid/animation/ValueAnimator;

.field private final mDropSandView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

.field private final mDropUpdateListener:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;

.field private final mLayout:Landroid/view/View;

.field private final mLowerSandView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

.field private mRotateAnimator:Landroid/animation/ObjectAnimator;

.field private final mRotateUpdateListener:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$RotateAnimateUpdater;

.field private final mUpperSandView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

.field private mbStopped:Z


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;)V
    .locals 2
    .param p1, "layout"    # Landroid/view/View;
    .param p2, "upperSandView"    # Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;
    .param p3, "lowerSandView"    # Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;
    .param p4, "dropSandView"    # Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->DROP_DURATION:I

    .line 18
    const/16 v0, 0x190

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->ROTATE_DURATION:I

    .line 19
    const/16 v0, 0x12c

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->ROTATE_DELAY:I

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->DROP_FROM_RATIO:F

    .line 22
    const v0, 0x3fb33333    # 1.4f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->DROP_TO_RATIO:F

    .line 24
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/easing/QuadEaseOut;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/easing/QuadEaseOut;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->QUADEASEOUT:Landroid/animation/TimeInterpolator;

    .line 42
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mLayout:Landroid/view/View;

    .line 43
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mUpperSandView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    .line 44
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mLowerSandView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    .line 45
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropSandView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    .line 47
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;-><init>(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropUpdateListener:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;

    .line 48
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$RotateAnimateUpdater;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$RotateAnimateUpdater;-><init>(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$RotateAnimateUpdater;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mRotateUpdateListener:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$RotateAnimateUpdater;

    .line 49
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;)Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mUpperSandView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;)Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mLowerSandView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;)Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropSandView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mbStopped:Z

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;II)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->playRotate(II)V

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;II)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->playDrop(II)V

    return-void
.end method

.method private playDrop(II)V
    .locals 3
    .param p1, "duration"    # I
    .param p2, "delay"    # I

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->stopDropAnimator()V

    .line 89
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->stopRotateAnimator()V

    .line 91
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->recoveryForDropAnimator()V

    .line 93
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropUpdateListener:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;

    const/4 v1, 0x0

    iput v1, v0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;->FROM:F

    .line 94
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropUpdateListener:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;

    const v1, 0x3fb33333    # 1.4f

    iput v1, v0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;->TO:F

    .line 96
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropUpdateListener:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;

    iget v2, v2, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;->FROM:F

    aput v2, v0, v1

    const/4 v1, 0x1

    .line 97
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropUpdateListener:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;

    iget v2, v2, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;->TO:F

    aput v2, v0, v1

    .line 96
    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    .line 98
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropUpdateListener:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropUpdateListener:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$DropAnimateUpdater;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->QUADEASEOUT:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 102
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    int-to-long v1, p2

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 105
    return-void
.end method

.method private playRotate(II)V
    .locals 3
    .param p1, "duration"    # I
    .param p2, "delay"    # I

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->stopDropAnimator()V

    .line 111
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->stopRotateAnimator()V

    .line 113
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->recoveryForRotateAnimator()V

    .line 115
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mLayout:Landroid/view/View;

    const-string/jumbo v1, "rotation"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    .line 117
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mRotateUpdateListener:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$RotateAnimateUpdater;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->QUADEASEOUT:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 120
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    int-to-long v1, p2

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 123
    return-void

    .line 115
    :array_0
    .array-data 4
        0x0
        0x43340000    # 180.0f
    .end array-data
.end method

.method private recoveryForDropAnimator()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 127
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setRotation(F)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mUpperSandView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->render(F)V

    .line 130
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mLowerSandView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->render(F)V

    .line 131
    return-void
.end method

.method private recoveryForRotateAnimator()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 135
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mUpperSandView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->render(F)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mLowerSandView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->render(F)V

    .line 139
    return-void
.end method

.method private stopDropAnimator()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 66
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 68
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    .line 71
    :cond_0
    return-void
.end method

.method private stopRotateAnimator()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->removeAllListeners()V

    .line 78
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->removeAllUpdateListeners()V

    .line 80
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    .line 83
    :cond_0
    return-void
.end method


# virtual methods
.method public play(I)V
    .locals 1
    .param p1, "delay"    # I

    .prologue
    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mbStopped:Z

    .line 54
    const/16 v0, 0x3e8

    invoke-direct {p0, v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->playDrop(II)V

    .line 55
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mbStopped:Z

    .line 59
    return-void
.end method
