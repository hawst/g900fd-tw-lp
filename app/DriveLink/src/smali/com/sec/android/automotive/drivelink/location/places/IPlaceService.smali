.class public interface abstract Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;
.super Ljava/lang/Object;
.source "IPlaceService.java"


# virtual methods
.method public abstract autocomplete(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;",
            ">;"
        }
    .end annotation
.end method

.method public abstract details(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
.end method

.method public abstract nearbySearch(Ljava/lang/String;DD)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "DD)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;",
            ">;"
        }
    .end annotation
.end method
