.class public Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;
.super Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkBaseListenerImp;
.source "OnDriveLinkMusicPlayerListenerImp.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;


# instance fields
.field private mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkBaseListenerImp;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    return-object v0
.end method


# virtual methods
.method public onMusicPlayCompleted(Landroid/media/MediaPlayer;)V
    .locals 3
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 93
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    if-nez v1, :cond_0

    .line 106
    :goto_0
    return-void

    .line 97
    :cond_0
    move-object v0, p1

    .line 98
    .local v0, "_mp":Landroid/media/MediaPlayer;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$5;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$5;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;Landroid/media/MediaPlayer;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onMusicPlayError(Landroid/media/MediaPlayer;IILjava/lang/Object;)V
    .locals 7
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    if-nez v0, :cond_0

    .line 146
    :goto_0
    return-void

    .line 132
    :cond_0
    move-object v2, p1

    .line 133
    .local v2, "_mp":Landroid/media/MediaPlayer;
    move v3, p2

    .line 134
    .local v3, "_what":I
    move v4, p3

    .line 135
    .local v4, "_extra":I
    move-object v5, p4

    .line 137
    .local v5, "_obj":Ljava/lang/Object;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v6

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$7;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$7;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;Landroid/media/MediaPlayer;IILjava/lang/Object;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onMusicPlayPaused(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;I)V
    .locals 4
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .param p2, "position"    # I

    .prologue
    .line 57
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    if-nez v2, :cond_0

    .line 72
    :goto_0
    return-void

    .line 61
    :cond_0
    move-object v0, p1

    .line 62
    .local v0, "_music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    move v1, p2

    .line 63
    .local v1, "_position":I
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$3;

    invoke-direct {v3, p0, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$3;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;I)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onMusicPlayResumed(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 3
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 76
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    if-nez v1, :cond_0

    .line 89
    :goto_0
    return-void

    .line 80
    :cond_0
    move-object v0, p1

    .line 81
    .local v0, "_music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$4;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$4;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onMusicPlaySeekComplete(Landroid/media/MediaPlayer;)V
    .locals 3
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 110
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    if-nez v1, :cond_0

    .line 124
    :goto_0
    return-void

    .line 114
    :cond_0
    move-object v0, p1

    .line 115
    .local v0, "_mp":Landroid/media/MediaPlayer;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$6;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$6;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;Landroid/media/MediaPlayer;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onMusicPlayStarted(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Z)V
    .locals 4
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .param p2, "bPlayNow"    # Z

    .prologue
    .line 21
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    if-nez v2, :cond_0

    .line 36
    :goto_0
    return-void

    .line 25
    :cond_0
    move-object v1, p1

    .line 26
    .local v1, "_music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    move v0, p2

    .line 27
    .local v0, "_bPlayNow":Z
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$1;

    invoke-direct {v3, p0, v1, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Z)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onMusicPlayStopped(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 3
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 40
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    if-nez v1, :cond_0

    .line 53
    :goto_0
    return-void

    .line 44
    :cond_0
    move-object v0, p1

    .line 45
    .local v0, "_music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$2;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$2;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onMusicPlayerEnvInfoChanged(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;)V
    .locals 3
    .param p1, "musicPlayerEnvInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    .prologue
    .line 151
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    if-nez v1, :cond_0

    .line 166
    :goto_0
    return-void

    .line 155
    :cond_0
    move-object v0, p1

    .line 157
    .local v0, "_musicPlayerEnvInfo":Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$8;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$8;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public setDriveLinkListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;

    .prologue
    .line 16
    check-cast p1, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    .end local p1    # "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    .line 17
    return-void
.end method
