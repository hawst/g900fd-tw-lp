.class public Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;
.super Ljava/lang/Object;
.source "GcmClient.java"


# static fields
.field private static final PLAY_SERVICES_RESOLUTION_REQUEST:I = 0x2328

.field private static final TAG:Ljava/lang/String; = "GcmClient"

.field private static mInstance:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;


# instance fields
.field private SENDER_ID:Ljava/lang/String;

.field private mGcm:Lcom/google/android/gms/gcm/GoogleCloudMessaging;

.field private mPushId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->mInstance:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;

    .line 23
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string/jumbo v0, "1026668644931"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->SENDER_ID:Ljava/lang/String;

    .line 24
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->mGcm:Lcom/google/android/gms/gcm/GoogleCloudMessaging;

    .line 25
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->mPushId:Ljava/lang/String;

    .line 29
    return-void
.end method

.method private checkPlayServices(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 70
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v1

    .line 71
    .local v1, "resultCode":I
    if-eqz v1, :cond_1

    .line 72
    invoke-static {v1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isUserRecoverableError(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 73
    const-string/jumbo v3, "GcmClient"

    .line 74
    const/16 v4, 0x2328

    invoke-static {v4}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->getErrorString(I)Ljava/lang/String;

    move-result-object v4

    .line 73
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    :cond_0
    const-string/jumbo v3, "GcmClient"

    const-string/jumbo v4, "This device is not supported."

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    .end local v1    # "resultCode":I
    :goto_0
    return v2

    .line 79
    .restart local v1    # "resultCode":I
    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    .line 80
    .end local v1    # "resultCode":I
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "GcmClient"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Error: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->mInstance:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;

    if-nez v0, :cond_0

    .line 33
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->mInstance:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;

    .line 34
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->mInstance:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;

    return-object v0
.end method


# virtual methods
.method public getPushId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->mPushId:Ljava/lang/String;

    return-object v0
.end method

.method public register(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->mPushId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 43
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->mPushId:Ljava/lang/String;

    .line 64
    :goto_0
    return-object v1

    .line 47
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->checkPlayServices(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 48
    const-string/jumbo v1, "GcmClient"

    const-string/jumbo v2, "No valid Google Play Services APK found."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    const/4 v1, 0x0

    goto :goto_0

    .line 51
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->mGcm:Lcom/google/android/gms/gcm/GoogleCloudMessaging;

    if-nez v1, :cond_2

    .line 52
    invoke-static {p1}, Lcom/google/android/gms/gcm/GoogleCloudMessaging;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/gcm/GoogleCloudMessaging;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->mGcm:Lcom/google/android/gms/gcm/GoogleCloudMessaging;

    .line 56
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->mGcm:Lcom/google/android/gms/gcm/GoogleCloudMessaging;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->SENDER_ID:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/gms/gcm/GoogleCloudMessaging;->register([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->mPushId:Ljava/lang/String;

    .line 57
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->mPushId:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->mPushId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 58
    :cond_3
    const-string/jumbo v1, "GcmClient"

    const-string/jumbo v2, "Fail to register new pushId."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->mPushId:Ljava/lang/String;

    goto :goto_0

    .line 59
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method
