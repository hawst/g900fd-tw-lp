.class Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$4;
.super Ljava/lang/Object;
.source "SettingsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;

    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 196
    const-string/jumbo v1, "[SettingsActivity]"

    const-string/jumbo v2, "layoutSuggestedContacts OnClick"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 198
    const-string/jumbo v1, "[SettingsActivity]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "DrivingStatus :  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 199
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 198
    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->SettingsDrivingDialog(Landroid/content/Context;)V

    .line 206
    :goto_0
    return-void

    .line 202
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;

    .line 203
    const-class v2, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    .line 202
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 204
    .local v0, "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
