.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestFavoriteContactList.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation
.end field

.field private mRescanContactDB:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 12
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;->mContext:Landroid/content/Context;

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;->mList:Ljava/util/ArrayList;

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;->mRescanContactDB:Z

    .line 20
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;->mContext:Landroid/content/Context;

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "mRescanContactDB"    # Z

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 12
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;->mContext:Landroid/content/Context;

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;->mList:Ljava/util/ArrayList;

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;->mRescanContactDB:Z

    .line 28
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;->mContext:Landroid/content/Context;

    .line 29
    iput-boolean p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;->mRescanContactDB:Z

    .line 30
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;->mContext:Landroid/content/Context;

    .line 50
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;->mList:Ljava/util/ArrayList;

    .line 51
    return-void
.end method

.method protected process()V
    .locals 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 35
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getFavoriteContactList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    .line 34
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;->mList:Ljava/util/ArrayList;

    .line 36
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 41
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkFavoriteContactListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;

    move-result-object v0

    .line 42
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;
    if-eqz v0, :cond_0

    .line 43
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;->mList:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;->onResponseRequestFavoriteContactList(Ljava/util/ArrayList;)V

    .line 45
    :cond_0
    return-void
.end method
