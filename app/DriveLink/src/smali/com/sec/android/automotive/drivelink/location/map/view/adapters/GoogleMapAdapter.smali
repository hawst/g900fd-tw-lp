.class public Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;
.super Lcom/sec/android/automotive/drivelink/location/map/LocationMap;
.source "GoogleMapAdapter.java"

# interfaces
.implements Lcom/google/android/gms/maps/GoogleMap$OnMarkerDragListener;


# static fields
.field private static final FAKE_ZOOM:D = 0.024

.field private static final MAX_ZOOM_LEVEL:F = 21.0f

.field private static final MIN_ZOOM_LEVEL:F = 2.0f

.field private static final TAG:Ljava/lang/String; = "GoogleMapAdapter"


# instance fields
.field private mIsMyLocationButtonEnable:Ljava/lang/Boolean;

.field private mMap:Lcom/google/android/gms/maps/GoogleMap;

.field private mMarkers:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;",
            "Lcom/google/android/gms/maps/model/Marker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;-><init>()V

    .line 55
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    .line 56
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMarkers:Ljava/util/Hashtable;

    .line 57
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mIsMyLocationButtonEnable:Ljava/lang/Boolean;

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->createMapFragment()V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;)V
    .locals 5
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 69
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;-><init>()V

    .line 55
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    .line 56
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMarkers:Ljava/util/Hashtable;

    .line 57
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mIsMyLocationButtonEnable:Ljava/lang/Boolean;

    .line 70
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMarkers:Ljava/util/Hashtable;

    move-object v0, p1

    .line 72
    check-cast v0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;

    .line 73
    .local v0, "mFragment":Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;
    invoke-virtual {v0, p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;->setAdapter(Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;)V

    .line 75
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;->getMap()Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    .line 77
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v2, :cond_0

    .line 78
    const-string/jumbo v2, "FMC"

    const-string/jumbo v3, "GoogleMapAdapter 3: TRUE"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/maps/GoogleMap;->setMyLocationEnabled(Z)V

    .line 88
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v2}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v1

    .line 89
    .local v1, "uiSettings":Lcom/google/android/gms/maps/UiSettings;
    invoke-virtual {v1, v4}, Lcom/google/android/gms/maps/UiSettings;->setMyLocationButtonEnabled(Z)V

    .line 92
    .end local v1    # "uiSettings":Lcom/google/android/gms/maps/UiSettings;
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->setMapFragment(Landroid/support/v4/app/Fragment;)V

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;Z)V
    .locals 3
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p2, "isEnableMyLocButton"    # Z

    .prologue
    const/4 v2, 0x0

    .line 95
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;-><init>()V

    .line 55
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    .line 56
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMarkers:Ljava/util/Hashtable;

    .line 57
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mIsMyLocationButtonEnable:Ljava/lang/Boolean;

    .line 96
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mIsMyLocationButtonEnable:Ljava/lang/Boolean;

    .line 98
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMarkers:Ljava/util/Hashtable;

    move-object v0, p1

    .line 100
    check-cast v0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;

    .line 101
    .local v0, "mFragment":Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;
    invoke-virtual {v0, p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;->setAdapter(Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;)V

    .line 103
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;->getMap()Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    .line 105
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v2, :cond_0

    .line 107
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v2, p2}, Lcom/google/android/gms/maps/GoogleMap;->setMyLocationEnabled(Z)V

    .line 108
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v2}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v1

    .line 109
    .local v1, "uiSettings":Lcom/google/android/gms/maps/UiSettings;
    invoke-virtual {v1, p2}, Lcom/google/android/gms/maps/UiSettings;->setMyLocationButtonEnabled(Z)V

    .line 112
    .end local v1    # "uiSettings":Lcom/google/android/gms/maps/UiSettings;
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->setMapFragment(Landroid/support/v4/app/Fragment;)V

    .line 113
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "isEnableMyLocButton"    # Z

    .prologue
    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;-><init>()V

    .line 55
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    .line 56
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMarkers:Ljava/util/Hashtable;

    .line 57
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mIsMyLocationButtonEnable:Ljava/lang/Boolean;

    .line 64
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mIsMyLocationButtonEnable:Ljava/lang/Boolean;

    .line 66
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->createMapFragment(Z)V

    .line 67
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;F)V
    .locals 0

    .prologue
    .line 47
    iput p1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->zoomCameraCurrent:F

    return-void
.end method

.method private camera(Lcom/google/android/gms/maps/CameraUpdate;)V
    .locals 1
    .param p1, "camera"    # Lcom/google/android/gms/maps/CameraUpdate;

    .prologue
    .line 292
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->getMap()Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 293
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->getMap()Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 294
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->getMap()Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 296
    :cond_0
    return-void
.end method

.method private createGMarkerOptionsFromLMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)Lcom/google/android/gms/maps/model/MarkerOptions;
    .locals 6
    .param p1, "marker"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .prologue
    .line 347
    if-nez p1, :cond_1

    .line 348
    const-string/jumbo v2, "GoogleMapAdapter"

    const-string/jumbo v3, "createGMarkerOptionsFromLMarkerItem: marker is null!!!"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    const/4 v1, 0x0

    .line 372
    :cond_0
    :goto_0
    return-object v1

    .line 352
    :cond_1
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLatitude()D

    move-result-wide v2

    .line 353
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLongitude()D

    move-result-wide v4

    .line 352
    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 354
    .local v0, "gLatLong":Lcom/google/android/gms/maps/model/LatLng;
    new-instance v1, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    .line 356
    .local v1, "gMarkerOptions":Lcom/google/android/gms/maps/model/MarkerOptions;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->isDraggable()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->draggable(Z)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 357
    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 358
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getSnippet()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->snippet(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 360
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getAnchor()Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 361
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getAnchor()Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;->getX()F

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getAnchor()Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;

    move-result-object v3

    .line 362
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;->getY()F

    move-result v3

    .line 361
    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->anchor(FF)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 364
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getResourceId()I

    move-result v2

    if-lez v2, :cond_3

    .line 366
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getResourceId()I

    move-result v2

    .line 365
    invoke-static {v2}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromResource(I)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->icon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)Lcom/google/android/gms/maps/model/MarkerOptions;

    goto :goto_0

    .line 367
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getIcon()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 369
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getIcon()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 368
    invoke-static {v2}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromBitmap(Landroid/graphics/Bitmap;)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->icon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)Lcom/google/android/gms/maps/model/MarkerOptions;

    goto :goto_0
.end method


# virtual methods
.method public addMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V
    .locals 3
    .param p1, "marker"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .prologue
    .line 161
    if-nez p1, :cond_1

    .line 162
    const-string/jumbo v1, "GoogleMapAdapter"

    const-string/jumbo v2, "addMarkerItem: marker is null!!!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 166
    invoke-static {v1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_0

    .line 168
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->addMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V

    .line 170
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v1, :cond_2

    .line 171
    const-string/jumbo v1, "GoogleMapAdapter"

    const-string/jumbo v2, "addMarkerItem: mMap is null."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 175
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMarkers:Ljava/util/Hashtable;

    if-nez v1, :cond_3

    .line 176
    const-string/jumbo v1, "GoogleMapAdapter"

    const-string/jumbo v2, "addMarkerItem: mMarkers is null."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 180
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    .line 181
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->createGMarkerOptionsFromLMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/GoogleMap;->addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v0

    .line 182
    .local v0, "gMarker":Lcom/google/android/gms/maps/model/Marker;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMarkers:Ljava/util/Hashtable;

    invoke-virtual {v1, p1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public clearMarkers()V
    .locals 1

    .prologue
    .line 242
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->clearMarkers()V

    .line 243
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->clear()V

    .line 246
    :cond_0
    return-void
.end method

.method protected createMapFragment()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 116
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMarkers:Ljava/util/Hashtable;

    .line 118
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;-><init>(Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;)V

    .line 120
    .local v0, "fragment":Lcom/google/android/gms/maps/SupportMapFragment;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v2, :cond_0

    .line 130
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/GoogleMap;->setMyLocationEnabled(Z)V

    .line 131
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v2}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v1

    .line 132
    .local v1, "uiSettings":Lcom/google/android/gms/maps/UiSettings;
    invoke-virtual {v1, v3}, Lcom/google/android/gms/maps/UiSettings;->setMyLocationButtonEnabled(Z)V

    .line 135
    .end local v1    # "uiSettings":Lcom/google/android/gms/maps/UiSettings;
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->setMapFragment(Landroid/support/v4/app/Fragment;)V

    .line 136
    return-void
.end method

.method protected createMapFragment(Z)V
    .locals 3
    .param p1, "isEnableMyLocButton"    # Z

    .prologue
    .line 139
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMarkers:Ljava/util/Hashtable;

    .line 141
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;-><init>(Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;)V

    .line 143
    .local v0, "fragment":Lcom/google/android/gms/maps/SupportMapFragment;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v2, :cond_0

    .line 145
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v2, p1}, Lcom/google/android/gms/maps/GoogleMap;->setMyLocationEnabled(Z)V

    .line 146
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v2}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v1

    .line 147
    .local v1, "uiSettings":Lcom/google/android/gms/maps/UiSettings;
    invoke-virtual {v1, p1}, Lcom/google/android/gms/maps/UiSettings;->setMyLocationButtonEnabled(Z)V

    .line 150
    .end local v1    # "uiSettings":Lcom/google/android/gms/maps/UiSettings;
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->setMapFragment(Landroid/support/v4/app/Fragment;)V

    .line 151
    return-void
.end method

.method public createUpdateableRoute(I)Lcom/sec/android/automotive/drivelink/location/model/IGraphicRoute;
    .locals 6
    .param p1, "color"    # I

    .prologue
    const/4 v5, 0x1

    .line 438
    new-instance v2, Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-direct {v2}, Lcom/google/android/gms/maps/model/PolylineOptions;-><init>()V

    .line 439
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v3

    const/high16 v4, 0x40a00000    # 5.0f

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->dipToPixels(Landroid/content/Context;F)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/PolylineOptions;->width(F)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v2

    .line 440
    invoke-virtual {v2, p1}, Lcom/google/android/gms/maps/model/PolylineOptions;->color(I)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/google/android/gms/maps/model/PolylineOptions;->geodesic(Z)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v0

    .line 442
    .local v0, "options":Lcom/google/android/gms/maps/model/PolylineOptions;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/maps/GoogleMap;->addPolyline(Lcom/google/android/gms/maps/model/PolylineOptions;)Lcom/google/android/gms/maps/model/Polyline;

    move-result-object v1

    .line 443
    .local v1, "polyline":Lcom/google/android/gms/maps/model/Polyline;
    invoke-virtual {v1, v5}, Lcom/google/android/gms/maps/model/Polyline;->setVisible(Z)V

    .line 445
    new-instance v2, Lcom/sec/android/automotive/drivelink/location/model/GoogleGraphicRoute;

    invoke-direct {v2, v1}, Lcom/sec/android/automotive/drivelink/location/model/GoogleGraphicRoute;-><init>(Lcom/google/android/gms/maps/model/Polyline;)V

    return-object v2
.end method

.method public drawRoute(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "points":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    const/4 v9, 0x1

    .line 421
    new-instance v4, Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-direct {v4}, Lcom/google/android/gms/maps/model/PolylineOptions;-><init>()V

    .line 422
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v5

    const/high16 v6, 0x40a00000    # 5.0f

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->dipToPixels(Landroid/content/Context;F)F

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/maps/model/PolylineOptions;->width(F)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v4

    .line 423
    const v5, -0x12e0dc

    invoke-virtual {v4, v5}, Lcom/google/android/gms/maps/model/PolylineOptions;->color(I)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v4

    invoke-virtual {v4, v9}, Lcom/google/android/gms/maps/model/PolylineOptions;->geodesic(Z)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v1

    .line 425
    .local v1, "options":Lcom/google/android/gms/maps/model/PolylineOptions;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 431
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v4, v1}, Lcom/google/android/gms/maps/GoogleMap;->addPolyline(Lcom/google/android/gms/maps/model/PolylineOptions;)Lcom/google/android/gms/maps/model/Polyline;

    move-result-object v3

    .line 433
    .local v3, "polyline":Lcom/google/android/gms/maps/model/Polyline;
    invoke-virtual {v3, v9}, Lcom/google/android/gms/maps/model/Polyline;->setVisible(Z)V

    .line 434
    return-void

    .line 425
    .end local v3    # "polyline":Lcom/google/android/gms/maps/model/Polyline;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 426
    .local v0, "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v5

    .line 427
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v7

    .line 426
    invoke-direct {v2, v5, v6, v7, v8}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 428
    .local v2, "point":Lcom/google/android/gms/maps/model/LatLng;
    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/PolylineOptions;->add(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/PolylineOptions;

    goto :goto_0
.end method

.method public getHashMarkers()Ljava/util/Hashtable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Hashtable",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;",
            "Lcom/google/android/gms/maps/model/Marker;",
            ">;"
        }
    .end annotation

    .prologue
    .line 228
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMarkers:Ljava/util/Hashtable;

    return-object v0
.end method

.method protected getMap()Lcom/google/android/gms/maps/GoogleMap;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    return-object v0
.end method

.method public onMarkerDrag(Lcom/google/android/gms/maps/model/Marker;)V
    .locals 0
    .param p1, "marker"    # Lcom/google/android/gms/maps/model/Marker;

    .prologue
    .line 377
    return-void
.end method

.method public onMarkerDragEnd(Lcom/google/android/gms/maps/model/Marker;)V
    .locals 6
    .param p1, "marker"    # Lcom/google/android/gms/maps/model/Marker;

    .prologue
    .line 381
    if-nez p1, :cond_0

    .line 382
    const-string/jumbo v2, "[GoogleMapAdapter]"

    const-string/jumbo v3, "Marker parameter is null"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    :goto_0
    return-void

    .line 386
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/Marker;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    .line 387
    .local v0, "gLatLng":Lcom/google/android/gms/maps/model/LatLng;
    if-nez v0, :cond_1

    .line 388
    const-string/jumbo v2, "[GoogleMapAdapter]"

    const-string/jumbo v3, "gLatLng variable is null"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 392
    :cond_1
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .line 393
    iget-wide v2, v0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    .line 392
    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;-><init>(DD)V

    .line 395
    .local v1, "lMarkerItem":Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->getDragDropListener()Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMarkerDragDropListener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMarkerDragDropListener;->onMarkerDragEnd(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V

    goto :goto_0
.end method

.method public onMarkerDragStart(Lcom/google/android/gms/maps/model/Marker;)V
    .locals 6
    .param p1, "marker"    # Lcom/google/android/gms/maps/model/Marker;

    .prologue
    .line 401
    if-nez p1, :cond_0

    .line 402
    const-string/jumbo v2, "[GoogleMapAdapter]"

    const-string/jumbo v3, "Marker parameter is null"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    :goto_0
    return-void

    .line 406
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/Marker;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    .line 407
    .local v0, "gLatLng":Lcom/google/android/gms/maps/model/LatLng;
    if-nez v0, :cond_1

    .line 408
    const-string/jumbo v2, "[GoogleMapAdapter]"

    const-string/jumbo v3, "gLatLng variable is null"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 412
    :cond_1
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .line 413
    iget-wide v2, v0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    .line 412
    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;-><init>(DD)V

    .line 415
    .local v1, "lMarkerItem":Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->getDragDropListener()Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMarkerDragDropListener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMarkerDragDropListener;->onDrag(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V

    goto :goto_0
.end method

.method public removeMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V
    .locals 3
    .param p1, "marker"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .prologue
    .line 215
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->removeMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V

    .line 216
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMarkers:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/Marker;

    .line 218
    .local v0, "gMarker":Lcom/google/android/gms/maps/model/Marker;
    if-eqz v0, :cond_0

    .line 219
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/Marker;->setVisible(Z)V

    .line 220
    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/Marker;->remove()V

    .line 221
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMarkers:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    :goto_0
    return-void

    .line 223
    :cond_0
    const-string/jumbo v1, "TESTE"

    const-string/jumbo v2, "The LocationMarkerItem does not exist in list."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setCompassEnabled(Z)V
    .locals 1
    .param p1, "option"    # Z

    .prologue
    .line 314
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/UiSettings;->setCompassEnabled(Z)V

    .line 315
    return-void
.end method

.method public setLocationPoint(DD)V
    .locals 1
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 234
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 233
    invoke-static {v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    .line 235
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setLocationPoint(DD)V

    .line 236
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-static {v0}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLng(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->camera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 238
    :cond_0
    return-void
.end method

.method protected setMap(Lcom/google/android/gms/maps/GoogleMap;)V
    .locals 3
    .param p1, "map"    # Lcom/google/android/gms/maps/GoogleMap;

    .prologue
    .line 318
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    .line 320
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v1, :cond_0

    .line 330
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mIsMyLocationButtonEnable:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/GoogleMap;->setMyLocationEnabled(Z)V

    .line 331
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v0

    .line 332
    .local v0, "uiSettings":Lcom/google/android/gms/maps/UiSettings;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mIsMyLocationButtonEnable:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/UiSettings;->setMyLocationButtonEnabled(Z)V

    .line 334
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v2, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter$1;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter$1;-><init>(Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/GoogleMap;->setOnCameraChangeListener(Lcom/google/android/gms/maps/GoogleMap$OnCameraChangeListener;)V

    .line 341
    .end local v0    # "uiSettings":Lcom/google/android/gms/maps/UiSettings;
    :cond_0
    return-void
.end method

.method public setOnMarkerClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMarkerClickListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/gms/maps/GoogleMap$OnMarkerClickListener;

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/GoogleMap;->setOnMarkerClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMarkerClickListener;)V

    .line 156
    :cond_0
    return-void
.end method

.method public setZoomControlsEnabled(Z)V
    .locals 1
    .param p1, "option"    # Z

    .prologue
    .line 309
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/UiSettings;->setZoomControlsEnabled(Z)V

    .line 311
    :cond_0
    return-void
.end method

.method public setZoomLevel(DD)V
    .locals 2
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 285
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 284
    invoke-static {v1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_0

    .line 286
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 287
    .local v0, "gLatLong":Lcom/google/android/gms/maps/model/LatLng;
    invoke-static {v0}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLng(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->camera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 289
    .end local v0    # "gLatLong":Lcom/google/android/gms/maps/model/LatLng;
    :cond_0
    return-void
.end method

.method public setZoomLevel(F)V
    .locals 4
    .param p1, "level"    # F

    .prologue
    const/high16 v2, 0x41980000    # 19.0f

    const/high16 v3, 0x40000000    # 2.0f

    .line 252
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 251
    invoke-static {v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    .line 253
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setZoomLevel(F)V

    .line 255
    mul-float v0, p1, v2

    add-float/2addr v0, v3

    .line 254
    invoke-static {v0}, Lcom/google/android/gms/maps/CameraUpdateFactory;->zoomTo(F)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->camera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 256
    const-string/jumbo v0, "M\u00e9todo: adapter() simples"

    new-instance v1, Ljava/lang/StringBuilder;

    .line 257
    mul-float/2addr v2, p1

    add-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 256
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    :cond_0
    return-void
.end method

.method public setZoomLevel(FDDI)V
    .locals 6
    .param p1, "level"    # F
    .param p2, "lat"    # D
    .param p4, "lng"    # D
    .param p6, "userIndex"    # I

    .prologue
    const/high16 v5, 0x41980000    # 19.0f

    const/high16 v4, 0x40000000    # 2.0f

    .line 266
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 265
    invoke-static {v1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_2

    .line 267
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setZoomLevel(F)V

    .line 268
    if-eqz p6, :cond_0

    const/4 v1, 0x1

    if-eq p6, v1, :cond_0

    const/4 v1, 0x3

    if-ne p6, v1, :cond_1

    .line 271
    :cond_0
    const-wide v1, 0x3f989374bc6a7efaL    # 0.024

    add-double/2addr p2, v1

    .line 273
    :cond_1
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v0, p2, p3, p4, p5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 274
    .local v0, "gLatLong":Lcom/google/android/gms/maps/model/LatLng;
    const-string/jumbo v1, "M\u00e9todo: adapter() completo"

    new-instance v2, Ljava/lang/StringBuilder;

    .line 275
    mul-float v3, p1, v5

    add-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 274
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    mul-float v1, p1, v5

    add-float/2addr v1, v4

    .line 277
    invoke-static {v0, v1}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngZoom(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->camera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 280
    .end local v0    # "gLatLong":Lcom/google/android/gms/maps/model/LatLng;
    :cond_2
    return-void
.end method

.method public updateIconMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "marker"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    .param p2, "icon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 189
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 188
    invoke-static {v1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_0

    .line 190
    invoke-super {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->updateIconMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;Landroid/graphics/Bitmap;)V

    .line 192
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMarkers:Ljava/util/Hashtable;

    if-nez v1, :cond_1

    .line 193
    const-string/jumbo v1, "GoogleMapAdapter"

    const-string/jumbo v2, "addMarkerItem: mMarkers is null."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMarkers:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/Marker;

    .line 199
    .local v0, "gMarker":Lcom/google/android/gms/maps/model/Marker;
    if-nez v0, :cond_2

    .line 200
    const-string/jumbo v1, "GoogleMapAdapter"

    const-string/jumbo v2, "addMarkerItem: marker is null!!!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 204
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v1, :cond_3

    .line 205
    const-string/jumbo v1, "GoogleMapAdapter"

    const-string/jumbo v2, "addMarkerItem: mMap is null."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 209
    :cond_3
    invoke-static {p2}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromBitmap(Landroid/graphics/Bitmap;)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/Marker;->setIcon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)V

    goto :goto_0
.end method

.method public zoomCameraCurrent()F
    .locals 1

    .prologue
    .line 299
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->getMap()Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->getMap()Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 300
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->getMap()Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    iget v0, v0, Lcom/google/android/gms/maps/model/CameraPosition;->zoom:F

    .line 301
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    goto :goto_0
.end method

.method public zoomToArea(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;I)V
    .locals 7
    .param p1, "loc1"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p2, "loc2"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p3, "padding"    # I

    .prologue
    .line 451
    new-instance v1, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;-><init>()V

    .line 452
    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v3

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v5

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    move-result-object v1

    .line 453
    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v3

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v5

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    move-result-object v1

    .line 454
    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->build()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    .line 456
    .local v0, "bounds":Lcom/google/android/gms/maps/model/LatLngBounds;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-static {v0, p3}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngBounds(Lcom/google/android/gms/maps/model/LatLngBounds;I)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 457
    return-void
.end method
