.class public Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "SettingsRejectMessageActivity.java"


# static fields
.field private static final LENGTH_ALERT_IS_SHOWING:Ljava/lang/String; = "LENGTH_ALERT_IS_SHOWING"

.field private static final TAG:Ljava/lang/String; = "[SettingsRejectMessageActivity]"


# instance fields
.field private CharacterLimit:I

.field btnRejectMessageCancel:Landroid/widget/ImageButton;

.field btnSettingsCancel:Landroid/widget/Button;

.field btnSettingsOk:Landroid/widget/Button;

.field etInput:Landroid/widget/EditText;

.field private mAlertDialog:Landroid/app/AlertDialog;

.field switchRejectMessage:Landroid/widget/Switch;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    .line 44
    const/16 v0, 0x50

    iput v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->CharacterLimit:I

    .line 35
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 303
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->savePrefSettings(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->CharacterLimit:I

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;Z)V
    .locals 0

    .prologue
    .line 313
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->enableLayout(Z)V

    return-void
.end method

.method private enableLayout(Z)V
    .locals 5
    .param p1, "enable"    # Z

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 314
    const v1, 0x7f09014c

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 316
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnSettingsCancel:Landroid/widget/Button;

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 317
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    if-eqz p1, :cond_0

    .line 318
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnSettingsOk:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 322
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 323
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnRejectMessageCancel:Landroid/widget/ImageButton;

    invoke-virtual {v1, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 324
    if-eqz p1, :cond_1

    .line 325
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnRejectMessageCancel:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 331
    :goto_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v1, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 333
    const-string/jumbo v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 332
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 334
    .local v0, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    if-eqz p1, :cond_3

    .line 335
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 341
    :goto_2
    return-void

    .line 320
    .end local v0    # "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnSettingsOk:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 327
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnRejectMessageCancel:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1

    .line 329
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnRejectMessageCancel:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1

    .line 339
    .restart local v0    # "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    .line 338
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_2
.end method

.method private getRejectMessage()Ljava/lang/String;
    .locals 4

    .prologue
    .line 345
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 346
    const-string/jumbo v2, "drivelink_rejectmessage_body"

    .line 344
    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 348
    .local v0, "rejectMessage":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 350
    const-string/jumbo v2, "PREF_SETTINGS_GENERAL_REJECT_MESSAGE_DEFAULT"

    .line 351
    const/4 v3, 0x1

    .line 349
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v1

    .line 351
    if-eqz v1, :cond_0

    .line 352
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 353
    const v2, 0x7f0a03dc

    .line 352
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 355
    .end local v0    # "rejectMessage":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private savePrefSettings(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "newMessage"    # Ljava/lang/String;
    .param p2, "isSave"    # Z

    .prologue
    .line 304
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 305
    const-string/jumbo v1, "drivelink_rejectmessage_body"

    .line 304
    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 308
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 309
    const-string/jumbo v1, "PREF_SETTINGS_GENERAL_REJECT_MESSAGE_DEFAULT"

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 311
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v12, 0x7f0a03db

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 49
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    const v4, 0x7f030031

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->setContentView(I)V

    .line 52
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSipFix;->assistActivity(Landroid/app/Activity;)V

    .line 54
    const v4, 0x7f09014d

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnSettingsCancel:Landroid/widget/Button;

    .line 55
    const v4, 0x7f09014e

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnSettingsOk:Landroid/widget/Button;

    .line 57
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnSettingsCancel:Landroid/widget/Button;

    new-instance v5, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$1;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnSettingsOk:Landroid/widget/Button;

    new-instance v5, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$2;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    new-instance v0, Landroid/app/AlertDialog$Builder;

    .line 88
    const/4 v4, 0x5

    .line 86
    invoke-direct {v0, p0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 90
    .local v0, "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const v4, 0x7f0a03dd

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 92
    const v4, 0x7f0a03de

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 93
    const v4, 0x7f0a0407

    .line 94
    new-instance v5, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$3;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;)V

    .line 93
    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 112
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->mAlertDialog:Landroid/app/AlertDialog;

    .line 114
    const v4, 0x7f09014a

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    .line 115
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    const-string/jumbo v5, "disableEmoticonInput=true;"

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 117
    if-eqz p1, :cond_1

    .line 118
    const-string/jumbo v4, "LAST_MSG"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 119
    .local v3, "savedText":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v4, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 122
    const-string/jumbo v4, "LENGTH_ALERT_IS_SHOWING"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 123
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 129
    .end local v3    # "savedText":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setSelection(I)V

    .line 131
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v4, v10}, Landroid/widget/EditText;->setSoundEffectsEnabled(Z)V

    .line 132
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    new-array v5, v11, [Landroid/text/InputFilter;

    new-instance v6, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;

    .line 133
    iget v7, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->CharacterLimit:I

    const-string/jumbo v8, "KSC5601"

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-direct {v6, v7, v8, v9}, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;-><init>(ILjava/lang/String;Landroid/app/AlertDialog;)V

    aput-object v6, v5, v10

    .line 132
    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 134
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    new-instance v5, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$4;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    new-instance v5, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 187
    const v4, 0x7f09014b

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnRejectMessageCancel:Landroid/widget/ImageButton;

    .line 188
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnRejectMessageCancel:Landroid/widget/ImageButton;

    new-instance v5, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$6;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    const v4, 0x7f090148

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 202
    .local v1, "btnNavBack":Landroid/widget/LinearLayout;
    new-instance v4, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$7;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$7;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;)V

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    new-instance v4, Ljava/lang/StringBuilder;

    .line 213
    invoke-virtual {p0, v12}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 214
    const-string/jumbo v5, ". "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 215
    const v5, 0x7f0a03f4

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 212
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 217
    invoke-virtual {p0, v12}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->setTitle(I)V

    .line 219
    const v4, 0x7f090149

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Switch;

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->switchRejectMessage:Landroid/widget/Switch;

    .line 221
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 222
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 223
    const-string/jumbo v5, "drivelink_rejectmessage_on"

    .line 221
    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v4

    if-ne v4, v11, :cond_2

    .line 224
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->switchRejectMessage:Landroid/widget/Switch;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/Switch;->setChecked(Z)V
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 231
    :goto_1
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->switchRejectMessage:Landroid/widget/Switch;

    invoke-virtual {v4}, Landroid/widget/Switch;->isChecked()Z

    move-result v4

    invoke-direct {p0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->enableLayout(Z)V

    .line 233
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    new-instance v5, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$8;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$8;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 245
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->switchRejectMessage:Landroid/widget/Switch;

    .line 246
    new-instance v5, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$9;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$9;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 274
    return-void

    .line 126
    .end local v1    # "btnNavBack":Landroid/widget/LinearLayout;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->getRejectMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 226
    .restart local v1    # "btnNavBack":Landroid/widget/LinearLayout;
    :cond_2
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->switchRejectMessage:Landroid/widget/Switch;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/Switch;->setChecked(Z)V
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 227
    :catch_0
    move-exception v2

    .line 228
    .local v2, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v2}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 278
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onPause()V

    .line 279
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 283
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onResume()V

    .line 285
    const-string/jumbo v0, "[SettingsRejectMessageActivity]"

    const-string/jumbo v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    const-string/jumbo v0, "[SettingsRejectMessageActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "DrivingStatus :  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 289
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 288
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->SettingsDrivingDialog(Landroid/content/Context;)V

    .line 293
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 297
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 298
    const-string/jumbo v0, "LAST_MSG"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const-string/jumbo v1, "LENGTH_ALERT_IS_SHOWING"

    .line 300
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 299
    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 301
    return-void

    .line 300
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
