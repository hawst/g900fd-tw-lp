.class public Lcom/sec/android/automotive/drivelink/message/MessageActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;
.source "MessageActivity.java"


# static fields
.field public static final CONTACT_CHOICE:Ljava/lang/String; = "CONTACT_CHOICE"

.field public static final CONTACT_TYPE:Ljava/lang/String; = "CONTACT_TYPE"

.field public static final DELAY_TIME_FOR_AUTO_SHRINK:J = 0x2710L

.field private static final ERROR_STRING:Ljava/lang/String; = "ERROR_STRING"

.field public static final INBOX_MODE:I = 0x20

.field public static final INBOX_MODE_SEARCH:I = 0x30

.field private static final IS_ERROR_STATE:Ljava/lang/String; = "IS_ERROR_STATE"

.field private static final IS_TTS_STATE:Ljava/lang/String; = "IS_TTS_STATE"

.field private static final ITEM_NUM_PER_PAGE:I = 0x4

.field public static final MAX_INBOX:I = 0xc

.field public static final MAX_SUGGESTIONS:I = 0xc

.field public static final MESSAGE_LIST_MODE:Ljava/lang/String; = "MESSAGE_LIST_MODE"

.field public static final MESSAGE_MODE:Ljava/lang/String; = "MESSAGE_MODE"

.field public static final MSG_AUTO_SHRINK:I = 0x0

.field public static final MULTI_SEARCH_MODE:I = 0x42

.field public static final NO_RESULT_SEARCH_MODE:I = 0x43

.field public static final NUMBER_MODE:I = 0x80

.field public static final SINGLE_SEARCH_MODE:I = 0x41

.field public static final SIP_IME:Ljava/lang/String; = "SIP_IME"

.field public static final SIP_SEARCH_MODE:Ljava/lang/String; = "SIP_SEARCH_MODE"

.field public static final SIP_SEARCH_RESULT_TEXT:Ljava/lang/String; = "SIP_SEARCH_RESULT_TEXT"

.field public static final SIP_SEARCH_TEXT:Ljava/lang/String; = "SIP_SEARCH_TEXT"

.field public static final SUGGESTIONS_MODE:I = 0x10

.field private static final TAG:Ljava/lang/String; = "MessageActivity"

.field private static isDebug:Z


# instance fields
.field getVoiceInputText:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

.field private mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

.field private mContactChoices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field

.field private mContactList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation
.end field

.field private mContactMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field private mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

.field private mContactTypeDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

.field private mContactTypeListLayout:Landroid/widget/LinearLayout;

.field private mContactTypePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;

.field private mContactTypePageNavigationLayout:Landroid/widget/RelativeLayout;

.field private mContactTypeViewPager:Landroid/support/v4/view/ViewPager;

.field private mCurrentMode:I

.field mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

.field private mDrivingMode:Z

.field private mHandler:Landroid/os/Handler;

.field private mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

.field private mInboxDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

.field private mInboxList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;"
        }
    .end annotation
.end field

.field private mInboxListLayout:Landroid/widget/LinearLayout;

.field private mInboxPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

.field private mInboxPageNavigationLayout:Landroid/widget/RelativeLayout;

.field private mInboxViewPager:Landroid/support/v4/view/ViewPager;

.field private mListLayout:Landroid/widget/LinearLayout;

.field private mListMode:I

.field private mMessageBody:Ljava/lang/String;

.field private mMessageTitle:Landroid/widget/TextView;

.field private mMsgChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MessageChangeListener;

.field private mNoListLayout:Landroid/widget/LinearLayout;

.field private mNolistText:Landroid/widget/TextView;

.field private mNumberContactId:J

.field private mNumberIsLoaded:Z

.field private mNumberUserContactImage:Landroid/graphics/Bitmap;

.field private mNumberUserDisplayName:Ljava/lang/String;

.field private mNumberUserIndex:I

.field private mNumberUserPhoneNumbers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;"
        }
    .end annotation
.end field

.field private mSIPState:Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;

.field private mSearchBtn:Landroid/widget/ImageButton;

.field private mSearchClearBtn:Landroid/widget/ImageButton;

.field private mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

.field private mSearchListLayout:Landroid/widget/LinearLayout;

.field private mSearchMode:Z

.field private mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

.field private mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;

.field private mSearchResultText:Landroid/widget/TextView;

.field private mSearchText:Landroid/widget/EditText;

.field private mSearchViewPager:Landroid/support/v4/view/ViewPager;

.field private mSearched:Z

.field private mSearchedInboxList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;"
        }
    .end annotation
.end field

.field private mSearchedInboxListVoice:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;"
        }
    .end annotation
.end field

.field private mSearchedResultText:Ljava/lang/String;

.field private mSearchedText:Ljava/lang/String;

.field private mSipIme:Z

.field private mSuggestionPageNavigationLayout:Landroid/widget/RelativeLayout;

.field private mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

.field private mSuggestionsListLayout:Landroid/widget/LinearLayout;

.field private mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;

.field private mSuggestionsViewPager:Landroid/support/v4/view/ViewPager;

.field private mTextfieldLayout:Landroid/widget/FrameLayout;

.field private mTvSearchContact:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->isDebug:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 80
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;-><init>()V

    .line 125
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberIsLoaded:Z

    .line 184
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMessageBody:Ljava/lang/String;

    .line 185
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchMode:Z

    .line 186
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedText:Ljava/lang/String;

    .line 187
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedResultText:Ljava/lang/String;

    .line 188
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSipIme:Z

    .line 189
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearched:Z

    .line 190
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mDrivingMode:Z

    .line 191
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;

    .line 417
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mHandler:Landroid/os/Handler;

    .line 1545
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMsgChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MessageChangeListener;

    .line 2006
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getVoiceInputText:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

    .line 2926
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    .line 80
    return-void
.end method

.method private static Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "level"    # Ljava/lang/String;
    .param p1, "sub"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 201
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->isDebug:Z

    if-eqz v0, :cond_0

    .line 202
    const-string/jumbo v0, "MessageActivity"

    invoke-static {p0, v0, p1, p2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    return v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1822
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getPhoneTypeItemOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypePageNavigationLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    return-object v0
.end method

.method static synthetic access$16(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    return-void
.end method

.method static synthetic access$17(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2379
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$18(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V
    .locals 0

    .prologue
    .line 401
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V

    return-void
.end method

.method static synthetic access$19(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactChoices:Ljava/util/List;

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity;I)I
    .locals 1

    .prologue
    .line 873
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setCurrentMode(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$20(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/List;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactChoices:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$21(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNolistText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$22(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxListLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$23(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchListLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$24(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    return-object v0
.end method

.method static synthetic access$25(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    return-object v0
.end method

.method static synthetic access$26(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1881
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getSearchedItemOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$27(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    return-void
.end method

.method static synthetic access$28(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$29(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    return-object v0
.end method

.method static synthetic access$31(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    return-void
.end method

.method static synthetic access$32(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$33(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    return-object v0
.end method

.method static synthetic access$35(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    return-void
.end method

.method static synthetic access$36(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$37(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$38(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1944
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getSearchedInboxItemOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$39(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 200
    invoke-static {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    return-void
.end method

.method static synthetic access$40(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V
    .locals 0

    .prologue
    .line 190
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mDrivingMode:Z

    return-void
.end method

.method static synthetic access$41(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$42(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$43(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListMode:I

    return v0
.end method

.method static synthetic access$44(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V
    .locals 0

    .prologue
    .line 185
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchMode:Z

    return-void
.end method

.method static synthetic access$45(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedText:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$46(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V
    .locals 0

    .prologue
    .line 188
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSipIme:Z

    return-void
.end method

.method static synthetic access$47(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$48(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V
    .locals 0

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->restorePrevFlow()V

    return-void
.end method

.method static synthetic access$49(Lcom/sec/android/automotive/drivelink/message/MessageActivity;I)V
    .locals 0

    .prologue
    .line 379
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->handleShrinkMessageOnMicState(I)V

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/message/MessageActivity;I)V
    .locals 0

    .prologue
    .line 116
    iput p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    return-void
.end method

.method static synthetic access$50(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;

    return-void
.end method

.method static synthetic access$51(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchClearBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$52(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mTvSearchContact:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$53(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V
    .locals 0

    .prologue
    .line 189
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearched:Z

    return-void
.end method

.method static synthetic access$54(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$55(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    return-object v0
.end method

.method static synthetic access$56(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;

    return-object v0
.end method

.method static synthetic access$57(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$58(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    return-object v0
.end method

.method static synthetic access$59(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$60(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$61(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V
    .locals 0

    .prologue
    .line 1581
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->hideInputMethod()V

    return-void
.end method

.method static synthetic access$62(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$63(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)I
    .locals 1

    .prologue
    .line 1648
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getMainPhoneNumber(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)I

    move-result v0

    return v0
.end method

.method static synthetic access$64(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)J
    .locals 2

    .prologue
    .line 120
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberContactId:J

    return-wide v0
.end method

.method static synthetic access$65(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberUserDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$66(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/vlingo/core/internal/contacts/ContactMatch;I)I
    .locals 1

    .prologue
    .line 1766
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getTypePhoneNumber(Lcom/vlingo/core/internal/contacts/ContactMatch;I)I

    move-result v0

    return v0
.end method

.method static synthetic access$67(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/vlingo/core/internal/contacts/ContactMatch;)I
    .locals 1

    .prologue
    .line 1693
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getMainPhoneNumber(Lcom/vlingo/core/internal/contacts/ContactMatch;)I

    move-result v0

    return v0
.end method

.method static synthetic access$68(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMessageBody:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$69(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMessageBody:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$70(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->finish()V

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeListLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;

    return-object v0
.end method

.method private getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2380
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$20;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$20;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    return-object v0
.end method

.method private getInboxItemOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1793
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$16;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$16;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    return-object v0
.end method

.method private getMainPhoneNumber(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)I
    .locals 7
    .param p1, "user"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 1649
    if-nez p1, :cond_0

    .line 1650
    const/4 v3, 0x0

    .line 1690
    :goto_0
    return v3

    .line 1653
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 1655
    .local v1, "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v1, v4, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getPhoneNumberList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1657
    .local v0, "dp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    const/4 v2, 0x0

    .line 1658
    .local v2, "i":I
    const/4 v3, 0x0

    .line 1660
    .local v3, "phoneType":I
    if-nez v0, :cond_1

    .line 1661
    const-string/jumbo v4, "MessageActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "dp == null , phoneType index : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1665
    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_2

    .line 1688
    :goto_2
    const-string/jumbo v4, "MessageActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "phoneType index : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1666
    :cond_2
    const-string/jumbo v5, "MessageActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "["

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "] "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "is Main? : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1667
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->isMainPhoneNumber()Z

    move-result v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1666
    invoke-static {v5, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1668
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->isMainPhoneNumber()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1669
    const-string/jumbo v4, "MessageActivity"

    const-string/jumbo v5, "is MainPhoneNumber : true"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1670
    move v3, v2

    .line 1672
    goto :goto_2

    .line 1665
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private getMainPhoneNumber(Lcom/vlingo/core/internal/contacts/ContactMatch;)I
    .locals 6
    .param p1, "user"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 1694
    if-nez p1, :cond_0

    .line 1695
    const/4 v2, 0x0

    .line 1735
    :goto_0
    return v2

    .line 1706
    :cond_0
    const/4 v1, 0x0

    .line 1707
    .local v1, "i":I
    const/4 v2, 0x0

    .line 1709
    .local v2, "phoneType":I
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v0

    .line 1711
    .local v0, "cl":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/4 v1, 0x0

    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 1733
    :goto_2
    const-string/jumbo v3, "MessageActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "phoneType index : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1712
    :cond_1
    const-string/jumbo v4, "MessageActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "["

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "] "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "is Main? : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactData;

    iget v3, v3, Lcom/vlingo/core/internal/contacts/ContactData;->isDefault:I

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1713
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactData;

    iget v3, v3, Lcom/vlingo/core/internal/contacts/ContactData;->isDefault:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 1714
    const-string/jumbo v3, "MessageActivity"

    const-string/jumbo v4, "is MainPhoneNumber : true"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1715
    move v2, v1

    .line 1717
    goto :goto_2

    .line 1711
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private getMessageSipStateListener()Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$OnMessageSipStateListener;
    .locals 1

    .prologue
    .line 434
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    return-object v0
.end method

.method private getPhoneTypeItemOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1823
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$17;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$17;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    return-object v0
.end method

.method private getSearchedInboxItemOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1945
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$19;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$19;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    return-object v0
.end method

.method private getSearchedItemOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1882
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$18;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$18;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    return-object v0
.end method

.method private getSuggestsListItemOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1592
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$15;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$15;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    return-object v0
.end method

.method private getTypePhoneNumber(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;I)I
    .locals 7
    .param p1, "user"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .param p2, "index"    # I

    .prologue
    .line 1739
    if-nez p1, :cond_0

    .line 1740
    const/4 v3, 0x0

    .line 1763
    :goto_0
    return v3

    .line 1742
    :cond_0
    const/4 v2, 0x0

    .line 1743
    .local v2, "i":I
    const/4 v3, 0x0

    .line 1746
    .local v3, "phoneType":I
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 1748
    .local v1, "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v1, v4, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getPhoneNumberList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1750
    .local v0, "dp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    const-string/jumbo v4, "MessageActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "[getTypePhoneNumber] index : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1751
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_1

    .line 1761
    :goto_2
    const-string/jumbo v4, "MessageActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "phoneType index : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1752
    :cond_1
    const-string/jumbo v5, "MessageActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "["

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "] "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "type : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneType()I

    move-result v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1753
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneType()I

    move-result v4

    if-ne v4, p2, :cond_2

    .line 1754
    const-string/jumbo v4, "MessageActivity"

    const-string/jumbo v5, "is type : true"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1755
    move v3, v2

    .line 1757
    goto :goto_2

    .line 1751
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private getTypePhoneNumber(Lcom/vlingo/core/internal/contacts/ContactMatch;I)I
    .locals 6
    .param p1, "user"    # Lcom/vlingo/core/internal/contacts/ContactMatch;
    .param p2, "index"    # I

    .prologue
    .line 1767
    if-nez p1, :cond_0

    .line 1768
    const/4 v2, 0x0

    .line 1789
    :goto_0
    return v2

    .line 1770
    :cond_0
    const/4 v1, 0x0

    .line 1771
    .local v1, "i":I
    const/4 v2, 0x0

    .line 1773
    .local v2, "phoneType":I
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v0

    .line 1775
    .local v0, "cl":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const-string/jumbo v3, "MessageActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "[getTypePhoneNumber] index : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1777
    const/4 v1, 0x0

    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 1787
    :goto_2
    const-string/jumbo v3, "MessageActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "phoneType index : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1778
    :cond_1
    const-string/jumbo v4, "MessageActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "["

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "] "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "type : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactData;->getType()I

    move-result v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1779
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactData;->getType()I

    move-result v3

    if-ne v3, p2, :cond_2

    .line 1780
    const-string/jumbo v3, "MessageActivity"

    const-string/jumbo v4, "is type : true"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1781
    move v2, v1

    .line 1783
    goto :goto_2

    .line 1777
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private handleShrinkMessageOnMicState(I)V
    .locals 7
    .param p1, "stateValue"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 380
    const-string/jumbo v3, "activity"

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 381
    .local v1, "am":Landroid/app/ActivityManager;
    invoke-virtual {v1, v6}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    .line 382
    .local v0, "Info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v2, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 384
    .local v2, "topActivity":Landroid/content/ComponentName;
    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    .line 385
    const-string/jumbo v4, "com.sec.android.automotive.drivelink.message.MessageActivity"

    .line 384
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 385
    if-nez v3, :cond_0

    .line 386
    const-string/jumbo v3, "MessageActivity"

    const-string/jumbo v4, "handleShrinkMessageOnMicState - not MessageActivity"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    :goto_0
    return-void

    .line 389
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 395
    const-string/jumbo v3, "MessageActivity"

    const-string/jumbo v4, "handleShrinkMessageOnMicState - default"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    invoke-direct {p0, v5}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V

    goto :goto_0

    .line 391
    :pswitch_0
    const-string/jumbo v3, "MessageActivity"

    const-string/jumbo v4, "handleShrinkMessageOnMicState - MIC_STATE_IDLE"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    invoke-direct {p0, v6}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V

    goto :goto_0

    .line 389
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private hideInputMethod()V
    .locals 4

    .prologue
    .line 1582
    const-string/jumbo v2, "input_method"

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1583
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    const/4 v1, 0x0

    .line 1584
    .local v1, "ret":Z
    if-eqz v0, :cond_0

    .line 1585
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    .line 1586
    :cond_0
    if-eqz v1, :cond_1

    .line 1587
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1589
    :cond_1
    return-void
.end method

.method private initSearchTextField()V
    .locals 2

    .prologue
    .line 486
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    .line 487
    const v1, 0x7f09035c

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 486
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchBtn:Landroid/widget/ImageButton;

    .line 488
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    .line 489
    const v1, 0x7f09035d

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 488
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;

    .line 490
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    .line 491
    const v1, 0x7f09035f

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP;

    .line 490
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;

    .line 492
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;

    const-string/jumbo v1, "disableEmoticonInput=true;"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 493
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    .line 494
    const v1, 0x7f090360

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 493
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchClearBtn:Landroid/widget/ImageButton;

    .line 495
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    .line 496
    const v1, 0x7f090361

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 495
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;

    .line 497
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    .line 498
    const v1, 0x7f09035e

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 497
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mTvSearchContact:Landroid/widget/TextView;

    .line 500
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;

    check-cast v0, Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP;

    .line 501
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getMessageSipStateListener()Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$OnMessageSipStateListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP;->setOnMessageSipStateListener(Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$OnMessageSipStateListener;)V

    .line 503
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchBtn:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/MessageActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$8;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 547
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/MessageActivity$9;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$9;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 573
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/MessageActivity$10;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$10;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 583
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 748
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchClearBtn:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/MessageActivity$12;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$12;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 763
    return-void
.end method

.method private setAutoShrink(Z)V
    .locals 4
    .param p1, "isOn"    # Z

    .prologue
    const/4 v3, 0x0

    .line 402
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 415
    :goto_0
    return-void

    .line 405
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 407
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchMode:Z

    if-nez v0, :cond_1

    .line 408
    const-string/jumbo v0, "MessageActivity"

    const-string/jumbo v1, "MSG_AUTO_SHRINK - start on playing"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mHandler:Landroid/os/Handler;

    .line 410
    const-wide/16 v1, 0x2710

    .line 409
    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 412
    :cond_1
    const-string/jumbo v0, "MessageActivity"

    const-string/jumbo v1, "MSG_AUTO_SHRINK - removing"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method private setCurrentMode(I)I
    .locals 12
    .param p1, "mode"    # I

    .prologue
    const/16 v11, 0x20

    const/16 v10, 0x10

    const/4 v9, 0x0

    const/16 v8, 0x8

    .line 874
    const/4 v3, 0x0

    .line 877
    .local v3, "ret":I
    const-string/jumbo v4, "i"

    const-string/jumbo v5, "setCurrentMode"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, " mode = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 878
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    invoke-virtual {v4, p1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->setCurrentMode(I)V

    .line 879
    sparse-switch p1, :sswitch_data_0

    .line 1027
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "default setCurrentMode "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/sec/android/automotive/drivelink/util/DLLog;->iFace(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1032
    :goto_1
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setSuggetionListFlowParam(I)V

    .line 1034
    :cond_0
    return v9

    .line 881
    :sswitch_0
    iput v10, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    .line 882
    iget v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    iput v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListMode:I

    .line 886
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;

    if-eqz v4, :cond_1

    .line 887
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 888
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->showSuggestionList()V

    goto :goto_1

    .line 894
    :sswitch_1
    iput v11, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    .line 895
    iget v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    iput v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListMode:I

    .line 898
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->showInboxList()V

    goto :goto_1

    .line 904
    :sswitch_2
    const/16 v4, 0x41

    iput v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    .line 910
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;

    if-eqz v4, :cond_2

    .line 911
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 920
    :cond_2
    const-string/jumbo v4, "DM_SMS_TYPE"

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v1

    .line 922
    .local v1, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v1, :cond_0

    iget-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-eqz v4, :cond_0

    .line 924
    iget-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-static {v4}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getDlContact(Lcom/vlingo/core/internal/contacts/ContactMatch;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v0

    .line 926
    .local v0, "dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    iget-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 930
    iget-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-wide v4, v4, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    iput-wide v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberContactId:J

    .line 931
    iget v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserIndex:I

    iput v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberUserIndex:I

    .line 932
    iget-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v4, v4, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberUserDisplayName:Ljava/lang/String;

    .line 934
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v4

    invoke-interface {v4, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 933
    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberUserContactImage:Landroid/graphics/Bitmap;

    .line 938
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v4

    .line 937
    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 939
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v4

    .line 940
    iget-wide v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberContactId:J

    .line 939
    invoke-virtual {v4, p0, v5, v6}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getPhoneNumberList(Landroid/content/Context;J)Ljava/util/ArrayList;

    move-result-object v4

    .line 937
    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    .line 944
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;

    if-eqz v4, :cond_3

    .line 945
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 946
    :cond_3
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMessageTitle:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 947
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMessageTitle:Landroid/widget/TextView;

    .line 948
    const v5, 0x7f0a035b

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberUserDisplayName:Ljava/lang/String;

    aput-object v7, v6, v9

    .line 947
    invoke-virtual {p0, v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 950
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getVoiceInputText:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-interface {v4, v5}, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;->OnContactTypeInfo(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    goto/16 :goto_1

    .line 957
    .end local v0    # "dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v1    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :sswitch_3
    const/16 v4, 0x42

    iput v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    .line 958
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;

    if-eqz v4, :cond_4

    .line 959
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 967
    :cond_4
    iget v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListMode:I

    if-ne v4, v10, :cond_6

    .line 968
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDLLastConfigInstance()Ljava/util/HashMap;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 969
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDLLastConfigInstance()Ljava/util/HashMap;

    move-result-object v4

    .line 970
    const-string/jumbo v5, "mContactChoices"

    .line 969
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 972
    .local v2, "obj":Ljava/lang/Object;
    if-eqz v2, :cond_5

    .line 974
    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearched:Z

    if-nez v4, :cond_5

    .line 975
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDLLastConfigInstance()Ljava/util/HashMap;

    move-result-object v4

    .line 976
    const-string/jumbo v5, "mContactChoices"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 975
    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactChoices:Ljava/util/List;

    .line 980
    .end local v2    # "obj":Ljava/lang/Object;
    :cond_5
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getVoiceInputText:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactChoices:Ljava/util/List;

    invoke-interface {v4, v5}, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;->OnContactChoice(Ljava/util/List;)V

    goto/16 :goto_1

    .line 982
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDLLastConfigInstance()Ljava/util/HashMap;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 983
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDLLastConfigInstance()Ljava/util/HashMap;

    move-result-object v4

    .line 984
    const-string/jumbo v5, "mSearchedInboxList"

    .line 983
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 986
    .restart local v2    # "obj":Ljava/lang/Object;
    if-eqz v2, :cond_7

    .line 988
    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearched:Z

    if-nez v4, :cond_7

    .line 989
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDLLastConfigInstance()Ljava/util/HashMap;

    move-result-object v4

    .line 990
    const-string/jumbo v5, "mSearchedInboxList"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    .line 989
    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxList:Ljava/util/ArrayList;

    .line 993
    .end local v2    # "obj":Ljava/lang/Object;
    :cond_7
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getVoiceInputText:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxList:Ljava/util/ArrayList;

    invoke-interface {v4, v5}, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;->OnInboxChoice(Ljava/util/ArrayList;)V

    goto/16 :goto_1

    .line 998
    :sswitch_4
    const/16 v4, 0x43

    iput v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    .line 999
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;

    if-eqz v4, :cond_8

    .line 1000
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1001
    :cond_8
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNolistText:Landroid/widget/TextView;

    const v5, 0x7f0a035a

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1002
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1003
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1004
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1005
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1006
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 1012
    :sswitch_5
    const/16 v4, 0x30

    iput v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    .line 1013
    iput v11, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListMode:I

    .line 1014
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDLLastConfigInstance()Ljava/util/HashMap;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 1015
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDLLastConfigInstance()Ljava/util/HashMap;

    move-result-object v4

    .line 1016
    const-string/jumbo v5, "mSearchedInboxListVoice"

    .line 1015
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 1018
    .restart local v2    # "obj":Ljava/lang/Object;
    if-eqz v2, :cond_9

    .line 1019
    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mIsRotation:Z

    if-eqz v4, :cond_9

    .line 1020
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDLLastConfigInstance()Ljava/util/HashMap;

    move-result-object v4

    .line 1021
    const-string/jumbo v5, "mSearchedInboxListVoice"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    .line 1020
    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    .line 1024
    .end local v2    # "obj":Ljava/lang/Object;
    :cond_9
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getVoiceInputText:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    invoke-interface {v4, v5}, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;->OnInboxSearchChoice(Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 879
    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x20 -> :sswitch_1
        0x30 -> :sswitch_5
        0x41 -> :sswitch_2
        0x42 -> :sswitch_3
        0x43 -> :sswitch_4
    .end sparse-switch
.end method

.method private setInitUI(Landroid/content/Intent;)V
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v9, 0x43

    const/16 v8, 0x42

    .line 774
    const-string/jumbo v4, "i"

    const-string/jumbo v5, "setInitUI"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, " action = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->isFlowManagerIntent(Landroid/content/Intent;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 776
    const/4 v1, 0x0

    .line 777
    .local v1, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 778
    const-string/jumbo v5, "EXTRA_FLOW_ID"

    .line 777
    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 779
    .local v0, "flowID":Ljava/lang/String;
    const-string/jumbo v4, "i"

    const-string/jumbo v5, "setInitUI"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "flow ID :: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 780
    const-string/jumbo v4, "DM_SMS_CONTACT_SEARCH_LIST"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 785
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v1

    .line 786
    iput v8, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    .line 787
    if-eqz v1, :cond_0

    .line 788
    iget-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactChoices:Ljava/util/List;

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactChoices:Ljava/util/List;

    .line 789
    iget-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageText:Ljava/lang/String;

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMessageBody:Ljava/lang/String;

    .line 868
    :cond_0
    :goto_0
    iget v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    invoke-direct {p0, v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setCurrentMode(I)I

    .line 871
    .end local v0    # "flowID":Ljava/lang/String;
    .end local v1    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_1
    return-void

    .line 791
    .restart local v0    # "flowID":Ljava/lang/String;
    .restart local v1    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_2
    const-string/jumbo v4, "DM_SMS_TYPE"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 796
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v1

    .line 804
    if-eqz v1, :cond_3

    .line 805
    iget-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 808
    :cond_3
    const/16 v4, 0x41

    iput v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    goto :goto_0

    .line 809
    :cond_4
    const-string/jumbo v4, "DM_SMS_INBOX"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 810
    iget v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    if-eq v4, v8, :cond_0

    .line 811
    iget v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    if-eq v4, v9, :cond_0

    .line 812
    const/16 v4, 0x20

    iput v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    .line 813
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->setSearchedInboxList(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 816
    :cond_5
    const-string/jumbo v4, "DM_SMS_INBOX_SEARCH_LIST"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 823
    iget v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    if-eq v4, v8, :cond_0

    .line 824
    iget v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    if-eq v4, v9, :cond_0

    .line 825
    const/16 v4, 0x30

    iput v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    .line 826
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v1

    .line 827
    iget-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchedInobxList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 828
    iget-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchedInobxList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_0

    .line 841
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    if-eqz v4, :cond_8

    .line 842
    new-instance v3, Ljava/util/ArrayList;

    .line 843
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    .line 842
    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 846
    .local v3, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;>;"
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 847
    iget-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchedInobxList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_7

    .line 856
    .end local v3    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;>;"
    :cond_6
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v4

    .line 857
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    .line 856
    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->setSearchedInboxList(Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 847
    .restart local v3    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;>;"
    :cond_7
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 848
    .local v2, "i":I
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 851
    .end local v2    # "i":I
    .end local v3    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;>;"
    :cond_8
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    .line 852
    iget-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchedInobxList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 853
    .restart local v2    # "i":I
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private setSuggetionListFlowParam(I)V
    .locals 7
    .param p1, "mode"    # I

    .prologue
    .line 1039
    const/4 v2, 0x0

    .line 1041
    .local v2, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    sparse-switch p1, :sswitch_data_0

    .line 1132
    :cond_0
    return-void

    .line 1044
    :sswitch_0
    const-string/jumbo v4, "DM_SMS_CONTACT"

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v2

    .line 1046
    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 1047
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 1048
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    .line 1050
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 1051
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    iget-object v5, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1058
    .end local v0    # "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :sswitch_1
    const-string/jumbo v4, "DM_SMS_INBOX"

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v2

    .line 1060
    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 1061
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 1062
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    .line 1063
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchedInobxList:Ljava/util/ArrayList;

    .line 1068
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 1069
    .local v3, "inbox":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getDLContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v5

    if-nez v5, :cond_1

    .line 1070
    iget-object v5, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getPhoneNumber()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1072
    :cond_1
    iget-object v5, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getDLContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v6

    .line 1073
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    .line 1072
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1082
    .end local v3    # "inbox":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    :sswitch_2
    const-string/jumbo v4, "DM_SMS_INBOX_SEARCH_LIST"

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v2

    .line 1084
    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 1085
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 1086
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    .line 1087
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchedInobxList:Ljava/util/ArrayList;

    .line 1089
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 1090
    .restart local v3    # "inbox":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getDLContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v5

    if-nez v5, :cond_2

    .line 1091
    iget-object v5, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getPhoneNumber()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1093
    :cond_2
    iget-object v5, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getDLContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v6

    .line 1094
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    .line 1093
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1102
    .end local v3    # "inbox":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    :sswitch_3
    const-string/jumbo v4, "DM_SMS_CONTACT_SEARCH_LIST"

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v2

    .line 1104
    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactChoices:Ljava/util/List;

    if-eqz v4, :cond_0

    .line 1105
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactChoices:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 1106
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    .line 1108
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactChoices:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 1109
    .local v0, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget-object v5, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    iget-object v6, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1115
    .end local v0    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :sswitch_4
    const-string/jumbo v4, "DM_SMS_TYPE"

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v2

    .line 1117
    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 1118
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 1119
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    .line 1121
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    .line 1122
    .local v1, "dp":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    iget-object v5, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1041
    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x20 -> :sswitch_1
        0x30 -> :sswitch_2
        0x41 -> :sswitch_4
        0x42 -> :sswitch_3
    .end sparse-switch
.end method

.method private showInboxList()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v3, 0x8

    .line 1473
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 1474
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNolistText:Landroid/widget/TextView;

    const v2, 0x7f0a020e

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1475
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1476
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1477
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1478
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1479
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1543
    :goto_0
    return-void

    .line 1483
    :cond_1
    iget v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    const/16 v2, 0x20

    if-ne v1, v2, :cond_2

    .line 1484
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1485
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1486
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1487
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1488
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1491
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    if-nez v1, :cond_3

    .line 1492
    new-instance v1, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    .line 1493
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxList:Ljava/util/ArrayList;

    .line 1494
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getInboxItemOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V

    .line 1492
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    .line 1495
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 1504
    :goto_1
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    .line 1505
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    .line 1506
    const/4 v2, -0x2

    .line 1505
    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1507
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1508
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1509
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1510
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setGravity(I)V

    .line 1511
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageNavigationLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1512
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getPageCount()I

    move-result v2

    .line 1513
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v3

    .line 1512
    invoke-virtual {v1, v2, v5, v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setDotIndicator(IILandroid/view/View$OnClickListener;)V

    .line 1514
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxViewPager:Landroid/support/v4/view/ViewPager;

    new-instance v2, Lcom/sec/android/automotive/drivelink/message/MessageActivity$14;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$14;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    goto/16 :goto_0

    .line 1497
    .end local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageNavigationLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 1498
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->setInboxList(Ljava/util/ArrayList;)V

    .line 1499
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->notifyDataSetChanged()V

    .line 1500
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    .line 1501
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getInitialPosition()I

    move-result v2

    .line 1500
    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_1
.end method

.method private showSuggestionList()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v3, 0x8

    .line 1393
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 1394
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNolistText:Landroid/widget/TextView;

    const v2, 0x7f0a0359

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1395
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1396
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1397
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1398
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1399
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1470
    :goto_0
    return-void

    .line 1403
    :cond_1
    iget v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    const/16 v2, 0x10

    if-ne v1, v2, :cond_2

    .line 1404
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1405
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1406
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1407
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1408
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1411
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;

    if-nez v1, :cond_3

    .line 1412
    new-instance v1, Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;

    .line 1413
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactList:Ljava/util/ArrayList;

    .line 1414
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getSuggestsListItemOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V

    .line 1412
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;

    .line 1415
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 1428
    :goto_1
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    .line 1429
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    .line 1430
    const/4 v2, -0x2

    .line 1429
    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1431
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1432
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1433
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1434
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setGravity(I)V

    .line 1435
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionPageNavigationLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1436
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    .line 1437
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;->getPageCount()I

    move-result v2

    .line 1438
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v3

    .line 1436
    invoke-virtual {v1, v2, v5, v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setDotIndicator(IILandroid/view/View$OnClickListener;)V

    .line 1439
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsViewPager:Landroid/support/v4/view/ViewPager;

    .line 1440
    new-instance v2, Lcom/sec/android/automotive/drivelink/message/MessageActivity$13;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$13;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    goto/16 :goto_0

    .line 1417
    .end local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionPageNavigationLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 1418
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;->setSuggestionsList(Ljava/util/ArrayList;)V

    .line 1419
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;->notifyDataSetChanged()V

    .line 1420
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;

    .line 1421
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;->getInitialPosition()I

    move-result v2

    .line 1420
    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_1
.end method


# virtual methods
.method public finish()V
    .locals 3

    .prologue
    .line 2684
    const-string/jumbo v0, "w"

    const-string/jumbo v1, "finish "

    const-string/jumbo v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2705
    # invokes: Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->finish()V
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$70(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    .line 2706
    const v0, 0x7f040025

    const v1, 0x7f040001

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->overridePendingTransition(II)V

    .line 2716
    return-void
.end method

.method public isComposerInputMethodShown()Z
    .locals 6

    .prologue
    .line 2672
    const/4 v1, 0x0

    .line 2673
    .local v1, "ret":Z
    const-string/jumbo v2, "input_method"

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2675
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 2676
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    .line 2678
    :cond_0
    const-string/jumbo v2, "i"

    const-string/jumbo v3, "isComposerInputMethodShown"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "ret = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2679
    return v1
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2451
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2455
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2456
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2457
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->showVoiceLayout()V

    .line 2460
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListMode:I

    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    .line 2461
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setCurrentMode(I)I

    .line 2463
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchMode:Z

    .line 2464
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSipIme:Z

    .line 2465
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedText:Ljava/lang/String;

    .line 2467
    const-string/jumbo v0, "MessageActivity"

    const-string/jumbo v1, "onBackPressed - setAutoShrink : true"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2468
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V

    .line 2496
    :goto_0
    return-void

    .line 2474
    :cond_0
    const-string/jumbo v0, "MessageActivity"

    const-string/jumbo v1, "onBackPressed - setAutoShrink : false"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2475
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V

    .line 2487
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->restorePrevFlow()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x10

    .line 207
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 209
    const v0, 0x7f030019

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setContentView(I)V

    .line 210
    const v0, 0x7f0a0250

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setTitle(I)V

    .line 217
    const v0, 0x7f0900ad

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMessageTitle:Landroid/widget/TextView;

    .line 218
    const v0, 0x7f09009f

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    .line 219
    const v0, 0x7f0900ae

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNolistText:Landroid/widget/TextView;

    .line 223
    const v0, 0x7f09009d

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;

    .line 224
    const v0, 0x7f09009e

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsViewPager:Landroid/support/v4/view/ViewPager;

    .line 225
    const v0, 0x7f0900af

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 227
    const v0, 0x7f0900b0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxListLayout:Landroid/widget/LinearLayout;

    .line 228
    const v0, 0x7f0900b1

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxViewPager:Landroid/support/v4/view/ViewPager;

    .line 229
    const v0, 0x7f0900b2

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 231
    const v0, 0x7f0900b3

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeListLayout:Landroid/widget/LinearLayout;

    .line 232
    const v0, 0x7f0900b4

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeViewPager:Landroid/support/v4/view/ViewPager;

    .line 233
    const v0, 0x7f0900b5

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypePageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 235
    const v0, 0x7f0900b6

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchListLayout:Landroid/widget/LinearLayout;

    .line 236
    const v0, 0x7f0900b7

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;

    .line 237
    const v0, 0x7f0900b8

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 239
    const v0, 0x7f0900ab

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    .line 240
    const v0, 0x7f0900ac

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListLayout:Landroid/widget/LinearLayout;

    .line 242
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    .line 243
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getMessageSuggestionList()Ljava/util/ArrayList;

    move-result-object v0

    .line 242
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactList:Ljava/util/ArrayList;

    .line 244
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInboxList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxList:Ljava/util/ArrayList;

    .line 245
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    .line 246
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getSearchedInboxList()Ljava/util/ArrayList;

    move-result-object v0

    .line 245
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    .line 248
    if-nez p1, :cond_4

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    .line 250
    if-nez p1, :cond_5

    move-object v0, v2

    :goto_1
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 253
    if-nez p1, :cond_6

    :goto_2
    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListMode:I

    .line 257
    if-eqz p1, :cond_0

    .line 258
    const-string/jumbo v0, "SIP_SEARCH_MODE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchMode:Z

    .line 259
    const-string/jumbo v0, "SIP_SEARCH_TEXT"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedText:Ljava/lang/String;

    .line 260
    const-string/jumbo v0, "SIP_IME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSipIme:Z

    .line 262
    const-string/jumbo v0, "SIP_SEARCH_RESULT_TEXT"

    .line 261
    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedResultText:Ljava/lang/String;

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    .line 329
    new-instance v1, Lcom/sec/android/automotive/drivelink/message/MessageActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->setOnVoiceMessageActionBarListener(Lcom/sec/android/automotive/drivelink/message/OnVoiceMessageActionBarListener;)V

    .line 346
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 347
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 349
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;

    move-result-object v0

    .line 350
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMsgChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MessageChangeListener;

    .line 349
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->addMessageChangeListener(Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MessageChangeListener;)V

    .line 351
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;

    move-result-object v0

    .line 352
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    .line 351
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->addDrivingChangeListener(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;)V

    .line 354
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->initSearchTextField()V

    .line 356
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 357
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setInitUI(Landroid/content/Intent;)V

    .line 361
    :goto_3
    const/high16 v0, 0x7f040000

    const v1, 0x7f040025

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->overridePendingTransition(II)V

    .line 365
    if-eqz p1, :cond_2

    .line 366
    const-string/jumbo v0, "IS_ERROR_STATE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 367
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    .line 368
    const-string/jumbo v1, "ERROR_STRING"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 367
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->displayError(Ljava/lang/CharSequence;)V

    .line 371
    :cond_2
    if-eqz p1, :cond_3

    .line 372
    const-string/jumbo v0, "IS_TTS_STATE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 373
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->setTTSState()V

    .line 376
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->moveoutOverlayService()V

    .line 377
    return-void

    .line 249
    :cond_4
    const-string/jumbo v0, "MESSAGE_MODE"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto/16 :goto_0

    .line 252
    :cond_5
    const-string/jumbo v0, "CONTACT_TYPE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 251
    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    goto/16 :goto_1

    .line 255
    :cond_6
    const-string/jumbo v0, "MESSAGE_LIST_MODE"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    goto/16 :goto_2

    .line 359
    :cond_7
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setCurrentMode(I)I

    goto :goto_3
.end method

.method protected onDLRetainConfigInstance(Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2430
    .local p1, "backupMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    if-nez p1, :cond_1

    .line 2447
    :cond_0
    :goto_0
    return-void

    .line 2434
    :cond_1
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    const/16 v1, 0x41

    if-ne v0, v1, :cond_2

    .line 2435
    const-string/jumbo v0, "mContactMap"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactMap:Ljava/util/Map;

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2436
    :cond_2
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    const/16 v1, 0x42

    if-ne v0, v1, :cond_4

    .line 2437
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListMode:I

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 2438
    const-string/jumbo v0, "mContactChoices"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactChoices:Ljava/util/List;

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2439
    :cond_3
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListMode:I

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    .line 2440
    const-string/jumbo v0, "mSearchedInboxList"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2442
    :cond_4
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    const/16 v1, 0x30

    if-ne v0, v1, :cond_0

    .line 2444
    const-string/jumbo v0, "mSearchedInboxListVoice"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 2642
    const-string/jumbo v0, "w"

    const-string/jumbo v1, "onDestroy "

    const-string/jumbo v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2643
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onDestroy()V

    .line 2645
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    if-eqz v0, :cond_0

    .line 2646
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->stopTTSBarAnimation()V

    .line 2650
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;

    move-result-object v0

    .line 2651
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMsgChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MessageChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->removeMessageChangeListener(Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MessageChangeListener;)V

    .line 2653
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 2655
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;

    move-result-object v0

    .line 2656
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->removeDrivingChangeListener(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;)V

    .line 2659
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->setContactMatches(Ljava/util/List;)V

    .line 2664
    return-void
.end method

.method public onFlowCommandCancel(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 1137
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->restorePrevFlow()V

    .line 1138
    return-void
.end method

.method protected onFlowListSelectedForMessage(Ljava/lang/String;IZLjava/lang/String;)V
    .locals 18
    .param p1, "flowID"    # Ljava/lang/String;
    .param p2, "index"    # I
    .param p3, "isOrdinal"    # Z
    .param p4, "message"    # Ljava/lang/String;

    .prologue
    .line 1144
    const-string/jumbo v2, "i"

    const-string/jumbo v3, "onFlowListSelected"

    const-string/jumbo v4, ""

    invoke-static {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1146
    if-eqz p3, :cond_2

    .line 1147
    const/4 v2, 0x4

    move/from16 v0, p2

    if-ge v0, v2, :cond_0

    .line 1148
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    const/16 v3, 0x10

    if-ne v2, v3, :cond_1

    .line 1150
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 1151
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    add-int p2, p2, v2

    .line 1153
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, p2

    if-lt v0, v2, :cond_2

    .line 1361
    :cond_0
    :goto_0
    return-void

    .line 1158
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    const/16 v3, 0x20

    if-ne v2, v3, :cond_4

    .line 1159
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 1160
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    add-int p2, p2, v2

    .line 1162
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, p2

    if-ge v0, v2, :cond_0

    .line 1218
    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 1221
    :sswitch_0
    const/4 v15, 0x0

    .line 1222
    .local v15, "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    const/4 v10, 0x0

    .line 1223
    .local v10, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    const/4 v11, 0x0

    .line 1225
    .local v11, "dlInbox":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    const/16 v3, 0x10

    if-ne v2, v3, :cond_8

    .line 1226
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactList:Ljava/util/ArrayList;

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    check-cast v15, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 1227
    .restart local v15    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-static {v15}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v10

    .line 1240
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListMode:I

    const/16 v3, 0x20

    if-ne v2, v3, :cond_9

    .line 1241
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    const/16 v3, 0x42

    if-ne v2, v3, :cond_9

    .line 1242
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxList:Ljava/util/ArrayList;

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 1243
    .local v16, "userInbox":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/MessageHolder;->getMessageHolder()Lcom/sec/android/automotive/drivelink/message/MessageHolder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/sec/android/automotive/drivelink/message/MessageHolder;->setInbox(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;)V

    .line 1244
    const-string/jumbo v2, "DM_SMS_READBACK"

    .line 1245
    const/4 v3, 0x0

    .line 1244
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0

    .line 1167
    .end local v10    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v11    # "dlInbox":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    .end local v15    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v16    # "userInbox":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    const/16 v3, 0x42

    if-ne v2, v3, :cond_6

    .line 1169
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListMode:I

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5

    .line 1170
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 1171
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 1172
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    add-int p2, p2, v2

    .line 1174
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, p2

    if-lt v0, v2, :cond_2

    goto/16 :goto_0

    .line 1180
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactChoices:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 1181
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactChoices:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 1182
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    add-int p2, p2, v2

    .line 1184
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactChoices:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, p2

    if-lt v0, v2, :cond_2

    goto/16 :goto_0

    .line 1190
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    const/16 v3, 0x41

    if-ne v2, v3, :cond_7

    .line 1191
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 1192
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 1193
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    add-int p2, p2, v2

    .line 1195
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, p2

    if-lt v0, v2, :cond_2

    goto/16 :goto_0

    .line 1200
    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    const/16 v3, 0x30

    if-ne v2, v3, :cond_2

    .line 1202
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 1203
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 1204
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    add-int p2, p2, v2

    .line 1206
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, p2

    if-le v0, v2, :cond_2

    goto/16 :goto_0

    .line 1229
    .restart local v10    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v11    # "dlInbox":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    .restart local v15    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :cond_8
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    const/16 v3, 0x42

    if-ne v2, v3, :cond_3

    .line 1230
    const-string/jumbo v2, "i"

    const-string/jumbo v3, "onFlowListSelected"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "currentItem : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1232
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListMode:I

    const/16 v3, 0x20

    if-eq v2, v3, :cond_3

    .line 1233
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactChoices:Ljava/util/List;

    move/from16 v0, p2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    check-cast v10, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .restart local v10    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    goto/16 :goto_1

    .line 1247
    :cond_9
    new-instance v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v8}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 1248
    .local v8, "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput-object v10, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 1249
    move/from16 v0, p2

    iput v0, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserIndex:I

    .line 1252
    const-string/jumbo v2, "DM_SMS_CONTACT_SEARCH_LIST"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v13

    .line 1253
    .local v13, "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v13, :cond_b

    iget v2, v13, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchListType:I

    if-ltz v2, :cond_b

    .line 1255
    iget v2, v13, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchListType:I

    .line 1254
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getTypePhoneNumber(Lcom/vlingo/core/internal/contacts/ContactMatch;I)I

    move-result v2

    iput v2, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    .line 1260
    :goto_2
    if-eqz v13, :cond_a

    .line 1261
    const/4 v2, -0x1

    iput v2, v13, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchListType:I

    .line 1264
    :cond_a
    if-nez p4, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMessageBody:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 1265
    iget-object v2, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v2, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 1267
    iget-object v2, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v2, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isJapaneseString(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1268
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 1269
    const v3, 0x7f0a014a

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 1270
    iget-object v6, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v6, v6, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    aput-object v6, v4, v5

    .line 1269
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1268
    iput-object v2, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 1278
    :goto_3
    const-string/jumbo v2, "DM_SMS_COMPOSE"

    .line 1277
    invoke-static {v2, v8}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto/16 :goto_0

    .line 1257
    :cond_b
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getMainPhoneNumber(Lcom/vlingo/core/internal/contacts/ContactMatch;)I

    move-result v2

    iput v2, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    goto :goto_2

    .line 1272
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 1273
    const v3, 0x7f0a014a

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 1274
    iget-object v6, v10, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    aput-object v6, v4, v5

    .line 1273
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1272
    iput-object v2, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    goto :goto_3

    .line 1280
    :cond_d
    if-eqz p4, :cond_f

    .line 1281
    move-object/from16 v0, p4

    iput-object v0, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageText:Ljava/lang/String;

    .line 1283
    const-string/jumbo v2, "DM_SMS_COMPLETE"

    .line 1282
    invoke-static {v2, v8}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 1289
    :cond_e
    :goto_4
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMessageBody:Ljava/lang/String;

    goto/16 :goto_0

    .line 1284
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMessageBody:Ljava/lang/String;

    if-eqz v2, :cond_e

    .line 1285
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMessageBody:Ljava/lang/String;

    iput-object v2, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageText:Ljava/lang/String;

    .line 1287
    const-string/jumbo v2, "DM_SMS_COMPLETE"

    .line 1286
    invoke-static {v2, v8}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_4

    .line 1296
    .end local v8    # "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v10    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v11    # "dlInbox":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    .end local v13    # "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v15    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxList:Ljava/util/ArrayList;

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 1297
    .restart local v16    # "userInbox":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/MessageHolder;->getMessageHolder()Lcom/sec/android/automotive/drivelink/message/MessageHolder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/sec/android/automotive/drivelink/message/MessageHolder;->setInbox(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;)V

    .line 1299
    const-string/jumbo v2, "DM_SMS_READBACK"

    .line 1300
    const/4 v3, 0x0

    .line 1299
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto/16 :goto_0

    .line 1305
    .end local v16    # "userInbox":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 1306
    .local v17, "userInbox_searched":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/MessageHolder;->getMessageHolder()Lcom/sec/android/automotive/drivelink/message/MessageHolder;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/sec/android/automotive/drivelink/message/MessageHolder;->setInbox(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;)V

    .line 1308
    const-string/jumbo v2, "DM_SMS_READBACK"

    .line 1309
    const/4 v3, 0x0

    .line 1308
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto/16 :goto_0

    .line 1313
    .end local v17    # "userInbox_searched":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    :sswitch_3
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberContactId:J

    .line 1314
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberUserDisplayName:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;

    .line 1313
    invoke-direct/range {v1 .. v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1317
    .local v1, "dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v12

    .line 1318
    .local v12, "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    check-cast v12, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 1319
    .end local v12    # "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-virtual {v12}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v2

    .line 1320
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v3

    .line 1319
    move-object/from16 v0, p0

    invoke-virtual {v2, v0, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getPhoneNumberList(Landroid/content/Context;J)Ljava/util/ArrayList;

    move-result-object v14

    .line 1322
    .local v14, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    invoke-static {v1, v14}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;Ljava/util/ArrayList;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v9

    .line 1325
    .local v9, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v8}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 1326
    .restart local v8    # "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput-object v9, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 1327
    move/from16 v0, p2

    iput v0, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    .line 1329
    if-nez p4, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMessageBody:Ljava/lang/String;

    if-nez v2, :cond_11

    .line 1330
    iget-object v2, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v2, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    if-eqz v2, :cond_10

    .line 1332
    iget-object v2, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v2, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isJapaneseString(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1333
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 1334
    const v3, 0x7f0a014a

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 1335
    iget-object v6, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v6, v6, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    aput-object v6, v4, v5

    .line 1334
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1333
    iput-object v2, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 1342
    :goto_5
    const-string/jumbo v2, "DM_SMS_COMPOSE"

    invoke-static {v2, v8}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto/16 :goto_0

    .line 1337
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 1338
    const v3, 0x7f0a014a

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 1339
    iget-object v6, v9, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    aput-object v6, v4, v5

    .line 1338
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1337
    iput-object v2, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    goto :goto_5

    .line 1345
    :cond_11
    if-eqz p4, :cond_13

    .line 1346
    move-object/from16 v0, p4

    iput-object v0, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageText:Ljava/lang/String;

    .line 1348
    const-string/jumbo v2, "DM_SMS_COMPLETE"

    .line 1347
    invoke-static {v2, v8}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 1354
    :cond_12
    :goto_6
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMessageBody:Ljava/lang/String;

    goto/16 :goto_0

    .line 1349
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMessageBody:Ljava/lang/String;

    if-eqz v2, :cond_12

    .line 1350
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMessageBody:Ljava/lang/String;

    iput-object v2, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageText:Ljava/lang/String;

    .line 1352
    const-string/jumbo v2, "DM_SMS_COMPLETE"

    .line 1351
    invoke-static {v2, v8}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_6

    .line 1218
    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x20 -> :sswitch_1
        0x30 -> :sswitch_2
        0x41 -> :sswitch_3
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 766
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 767
    if-eqz p1, :cond_0

    .line 768
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setInitUI(Landroid/content/Intent;)V

    .line 770
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 2619
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onPause()V

    .line 2623
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchMode:Z

    if-eqz v0, :cond_0

    .line 2624
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->isComposerInputMethodShown()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSipIme:Z

    .line 2625
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedResultText:Ljava/lang/String;

    .line 2626
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedText:Ljava/lang/String;

    .line 2628
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 2539
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mIsRotation:Z

    .line 2540
    .local v2, "isRoataion":Z
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onResume()V

    .line 2541
    iget v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    const/16 v5, 0x10

    if-ne v4, v5, :cond_2

    if-nez v2, :cond_2

    .line 2542
    const-string/jumbo v4, "update msg suggestion list"

    invoke-static {p0, v4}, Lcom/sec/android/automotive/drivelink/util/DLLog;->iFace(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2543
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v4

    .line 2544
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->setMessageSuggestionListUpdateListener()V

    .line 2545
    const/4 v1, 0x0

    .line 2546
    .local v1, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v0

    .line 2547
    .local v0, "flowID":Ljava/lang/String;
    const-string/jumbo v4, "DM_SMS_CONTACT"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2549
    const-string/jumbo v4, "DM_SMS_CONTACT"

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v1

    .line 2551
    if-eqz v1, :cond_0

    iget-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPrePromptString:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 2552
    const-string/jumbo v4, "DM_SMS_CONTACT"

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->resetFlowParam(Ljava/lang/String;)V

    .line 2556
    :cond_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactList:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_6

    .line 2557
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateMessageSuggestionList()V

    .line 2563
    .end local v0    # "flowID":Ljava/lang/String;
    .end local v1    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_2
    :goto_0
    iget v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    const/16 v5, 0x20

    if-ne v4, v5, :cond_3

    if-nez v2, :cond_3

    .line 2564
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->setMessageMessageUpdateListener()V

    .line 2565
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateInboxList()V

    .line 2569
    :cond_3
    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchMode:Z

    if-eqz v4, :cond_5

    .line 2571
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 2572
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->hideVoiceLayout()V

    .line 2573
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2575
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedResultText:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2576
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedText:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2577
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setSelection(I)V

    .line 2579
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDLLastConfigInstance()Ljava/util/HashMap;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 2580
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDLLastConfigInstance()Ljava/util/HashMap;

    move-result-object v4

    const-string/jumbo v5, "mContactChoices"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 2581
    .local v3, "obj":Ljava/lang/Object;
    if-eqz v3, :cond_4

    .line 2582
    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mIsRotation:Z

    if-eqz v4, :cond_4

    .line 2583
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDLLastConfigInstance()Ljava/util/HashMap;

    move-result-object v4

    .line 2584
    const-string/jumbo v5, "mContactChoices"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 2583
    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactChoices:Ljava/util/List;

    .line 2588
    .end local v3    # "obj":Ljava/lang/Object;
    :cond_4
    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSipIme:Z

    if-eqz v4, :cond_7

    .line 2589
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;

    new-instance v5, Lcom/sec/android/automotive/drivelink/message/MessageActivity$21;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$21;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    .line 2598
    const-wide/16 v6, 0x12c

    .line 2589
    invoke-virtual {v4, v5, v6, v7}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2603
    :cond_5
    :goto_1
    return-void

    .line 2559
    .restart local v0    # "flowID":Ljava/lang/String;
    .restart local v1    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_6
    iget v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    invoke-direct {p0, v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setCurrentMode(I)I

    goto :goto_0

    .line 2600
    .end local v0    # "flowID":Ljava/lang/String;
    .end local v1    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_7
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v4, v6}, Landroid/widget/EditText;->setCursorVisible(Z)V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 2500
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2501
    const-string/jumbo v1, "MESSAGE_MODE"

    iget v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2502
    const-string/jumbo v1, "CONTACT_TYPE"

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2505
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedText:Ljava/lang/String;

    .line 2506
    const-string/jumbo v1, "SIP_SEARCH_MODE"

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchMode:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2507
    const-string/jumbo v1, "SIP_SEARCH_TEXT"

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedText:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2508
    const-string/jumbo v1, "SIP_IME"

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->isComposerInputMethodShown()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2509
    const/4 v0, 0x0

    .line 2510
    .local v0, "strResult":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 2511
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2512
    :cond_0
    const-string/jumbo v1, "SIP_SEARCH_RESULT_TEXT"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2513
    const-string/jumbo v1, "MESSAGE_LIST_MODE"

    iget v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListMode:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2515
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->isErrorState()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2516
    const-string/jumbo v1, "IS_ERROR_STATE"

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2517
    const-string/jumbo v1, "ERROR_STRING"

    .line 2518
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->getErrorCharSequence()Ljava/lang/CharSequence;

    move-result-object v2

    .line 2517
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 2521
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->isTTSState()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2522
    const-string/jumbo v1, "IS_TTS_STATE"

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2525
    :cond_2
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "onSaveInstanceState"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "onSaveInstanceState mCurrentMode"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2526
    iget v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2525
    invoke-static {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2527
    return-void
.end method

.method protected onStartResume()V
    .locals 1

    .prologue
    .line 2612
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchMode:Z

    if-nez v0, :cond_0

    .line 2613
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onStartResume()V

    .line 2615
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 2632
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onStop()V

    .line 2633
    const-string/jumbo v0, "MessageActivity"

    const-string/jumbo v1, "onStop - setAutoShrink : false"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2634
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V

    .line 2635
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/DLApplication;->DL_DEMO:Z

    if-eqz v0, :cond_0

    .line 2636
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->finish()V

    .line 2638
    :cond_0
    return-void
.end method

.method protected setDayMode()V
    .locals 3

    .prologue
    const v2, 0x7f08002a

    .line 2875
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->setDayMode()V

    .line 2877
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->setDayMode()V

    .line 2878
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f08000a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 2880
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    sparse-switch v0, :sswitch_data_0

    .line 2923
    :cond_0
    :goto_0
    return-void

    .line 2882
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;

    if-eqz v0, :cond_0

    .line 2883
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;->setDayMode()V

    .line 2884
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;->notifyDataSetChanged()V

    .line 2885
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 2886
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 2890
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    if-eqz v0, :cond_0

    .line 2891
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->setDayMode()V

    .line 2892
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->notifyDataSetChanged()V

    .line 2893
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 2894
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 2902
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    if-eqz v0, :cond_1

    .line 2903
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->setDayMode()V

    .line 2904
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->notifyDataSetChanged()V

    .line 2905
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 2906
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 2907
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    if-eqz v0, :cond_0

    .line 2908
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->setDayMode()V

    .line 2909
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->notifyDataSetChanged()V

    .line 2910
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 2911
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 2915
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    if-eqz v0, :cond_0

    .line 2916
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->setDayMode()V

    .line 2917
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->notifyDataSetChanged()V

    .line 2918
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 2919
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 2880
    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x20 -> :sswitch_1
        0x30 -> :sswitch_3
        0x42 -> :sswitch_2
    .end sparse-switch
.end method

.method protected setNightMode()V
    .locals 3

    .prologue
    const v2, 0x7f08002b

    .line 2823
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->setNightMode()V

    .line 2825
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->setNightMode()V

    .line 2826
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f08002e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 2828
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I

    sparse-switch v0, :sswitch_data_0

    .line 2871
    :cond_0
    :goto_0
    return-void

    .line 2830
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;

    if-eqz v0, :cond_0

    .line 2831
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;->setNightMode()V

    .line 2832
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsPageAdapter:Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/SuggestionsMessagePageAdaper;->notifyDataSetChanged()V

    .line 2833
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 2834
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 2838
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    if-eqz v0, :cond_0

    .line 2839
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->setNightMode()V

    .line 2840
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->notifyDataSetChanged()V

    .line 2841
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 2842
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 2850
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    if-eqz v0, :cond_1

    .line 2851
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->setNightMode()V

    .line 2852
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->notifyDataSetChanged()V

    .line 2853
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 2854
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 2855
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    if-eqz v0, :cond_0

    .line 2856
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->setNightMode()V

    .line 2857
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->notifyDataSetChanged()V

    .line 2858
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 2859
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 2863
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    if-eqz v0, :cond_0

    .line 2864
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->setNightMode()V

    .line 2865
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->notifyDataSetChanged()V

    .line 2866
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 2867
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 2828
    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x20 -> :sswitch_1
        0x30 -> :sswitch_3
        0x42 -> :sswitch_2
    .end sparse-switch
.end method
