.class Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout$3;
.super Ljava/lang/Object;
.source "SearchMusicActionBarLayout.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->showHideClearButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 112
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 106
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 95
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->btn_clear:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->access$1(Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 99
    :goto_0
    return-void

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->btn_clear:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->access$1(Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method
