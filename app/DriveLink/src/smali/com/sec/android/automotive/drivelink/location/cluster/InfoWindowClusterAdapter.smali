.class public Lcom/sec/android/automotive/drivelink/location/cluster/InfoWindowClusterAdapter;
.super Landroid/widget/BaseAdapter;
.source "InfoWindowClusterAdapter.java"


# instance fields
.field private context:Landroid/content/Context;

.field private pointList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p2, "pointList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/cluster/InfoWindowClusterAdapter;->pointList:Ljava/util/List;

    .line 25
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/location/cluster/InfoWindowClusterAdapter;->pointList:Ljava/util/List;

    .line 26
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/cluster/InfoWindowClusterAdapter;->context:Landroid/content/Context;

    .line 27
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/cluster/InfoWindowClusterAdapter;->pointList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/cluster/InfoWindowClusterAdapter;->pointList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 69
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 32
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/cluster/InfoWindowClusterAdapter;->pointList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .line 34
    .local v1, "mark":Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    if-nez p2, :cond_0

    .line 35
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/cluster/InfoWindowClusterAdapter;->context:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 36
    const v4, 0x7f03004f

    const/4 v5, 0x0

    .line 35
    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 39
    :cond_0
    const v3, 0x7f0901d4

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 38
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 41
    .local v0, "layout":Landroid/widget/RelativeLayout;
    if-nez p1, :cond_1

    .line 42
    const v3, 0x7f0203c0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 51
    :goto_0
    const v3, 0x7f0901d5

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 50
    check-cast v2, Landroid/widget/TextView;

    .line 52
    .local v2, "tvName":Landroid/widget/TextView;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLabelDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    return-object p2

    .line 43
    .end local v2    # "tvName":Landroid/widget/TextView;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/cluster/InfoWindowClusterAdapter;->pointList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne p1, v3, :cond_2

    .line 44
    const v3, 0x7f0203be

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 46
    :cond_2
    const v3, 0x7f0203bf

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0
.end method
