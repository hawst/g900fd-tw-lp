.class Lcom/sec/android/automotive/drivelink/help/HelpActivity$HelpListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "HelpActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/help/HelpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HelpListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/help/HelpActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/help/HelpActivity;)V
    .locals 3

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity$HelpListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/help/HelpActivity;

    .line 89
    # getter for: Lcom/sec/android/automotive/drivelink/help/HelpActivity;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->access$0(Lcom/sec/android/automotive/drivelink/help/HelpActivity;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030058

    # getter for: Lcom/sec/android/automotive/drivelink/help/HelpActivity;->mListItems:Ljava/util/ArrayList;
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->access$1(Lcom/sec/android/automotive/drivelink/help/HelpActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 90
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 94
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity$HelpListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/help/HelpActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/help/HelpActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->access$0(Lcom/sec/android/automotive/drivelink/help/HelpActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 95
    const v2, 0x7f030058

    const/4 v3, 0x0

    .line 94
    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 97
    const v1, 0x7f0901f3

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 96
    check-cast v0, Landroid/widget/TextView;

    .line 98
    .local v0, "deviceName":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity$HelpListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/help/HelpActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/help/HelpActivity;->mListItems:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->access$1(Lcom/sec/android/automotive/drivelink/help/HelpActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    return-object p2
.end method
