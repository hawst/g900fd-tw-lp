.class public Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "SearchMusicListActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SearchMusicListActivity"

.field private static immOnOff:Z

.field private static mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

.field private static mIsIntentSearchMode:Z

.field private static mSearchString:Ljava/lang/String;


# instance fields
.field private driveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private imm:Landroid/view/inputmethod/InputMethodManager;

.field private mActionBar:Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;

.field private mMusicMainDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

.field private mMusicMainListLayout:Landroid/widget/LinearLayout;

.field private mMusicMainPageAdapter:Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;

.field private mMusicMainViewPager:Landroid/support/v4/view/ViewPager;

.field private mNoListLayout:Landroid/widget/LinearLayout;

.field private mOnDriveLinkMusicListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

.field private mPageNavigationLayout:Landroid/widget/RelativeLayout;

.field private mSIPState:Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;

.field private mSearchClearBtn:Landroid/widget/ImageButton;

.field private mSearchIconText:Landroid/widget/TextView;

.field private mSearchResultText:Landroid/widget/TextView;

.field private mSearchText:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchString:Ljava/lang/String;

    .line 67
    sput-boolean v1, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->immOnOff:Z

    .line 70
    sput-boolean v1, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mIsIntentSearchMode:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    .line 52
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->driveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 69
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;

    .line 273
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mOnDriveLinkMusicListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

    .line 47
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;)V
    .locals 0

    .prologue
    .line 53
    sput-object p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    return-void
.end method

.method static synthetic access$1()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainPageAdapter:Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->driveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->finish()V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;)V
    .locals 0

    .prologue
    .line 388
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->initSearchResultList(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;)V

    return-void
.end method

.method static synthetic access$3(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 54
    sput-object p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchString:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$4(Z)V
    .locals 0

    .prologue
    .line 67
    sput-boolean p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->immOnOff:Z

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->imm:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchClearBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchIconText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V
    .locals 0

    .prologue
    .line 255
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->search()V

    return-void
.end method

.method private getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 485
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$10;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$10;-><init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V

    return-object v0
.end method

.method private getListItemOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 468
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$9;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$9;-><init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V

    return-object v0
.end method

.method private getMusicSipStateListener()Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$OnMusicSipStateListener;
    .locals 1

    .prologue
    .line 540
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$11;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$11;-><init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V

    return-object v0
.end method

.method private initSearchResultList(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;)V
    .locals 10
    .param p1, "musicSearchResult"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    .prologue
    const/4 v6, 0x2

    const/16 v9, 0x8

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 390
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getTotalMusicCount()I

    move-result v0

    .line 391
    .local v0, "count":I
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getResultItemCount()I

    move-result v2

    .line 392
    .local v2, "size":I
    if-nez v2, :cond_0

    .line 393
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchResultText:Landroid/widget/TextView;

    const-string/jumbo v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 404
    :goto_0
    if-lez v0, :cond_2

    .line 405
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 406
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 407
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 408
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 409
    new-instance v3, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;

    .line 410
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 411
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->getListItemOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-direct {v3, v4, p1, v5}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;Landroid/view/View$OnClickListener;)V

    .line 409
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainPageAdapter:Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;

    .line 412
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainPageAdapter:Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 413
    new-instance v3, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    .line 414
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;-><init>(Landroid/content/Context;)V

    .line 413
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    .line 415
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 416
    const/4 v3, -0x1

    const/4 v4, -0x2

    .line 415
    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 417
    .local v1, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v3, 0xd

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 418
    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 419
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v3, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 420
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setGravity(I)V

    .line 421
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 422
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    .line 423
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainPageAdapter:Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->getPageCount()I

    move-result v4

    .line 424
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v5

    .line 422
    invoke-virtual {v3, v4, v7, v5}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setDotIndicator(IILandroid/view/View$OnClickListener;)V

    .line 425
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainViewPager:Landroid/support/v4/view/ViewPager;

    .line 426
    new-instance v4, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$8;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$8;-><init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 461
    .end local v1    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :goto_1
    return-void

    .line 394
    :cond_0
    if-ne v2, v8, :cond_1

    .line 395
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchResultText:Landroid/widget/TextView;

    .line 396
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 397
    const v5, 0x7f0a0372

    .line 396
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    .line 397
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    const-string/jumbo v6, ""

    aput-object v6, v5, v8

    .line 395
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 399
    :cond_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchResultText:Landroid/widget/TextView;

    .line 400
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0371

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    .line 401
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    const-string/jumbo v6, ""

    aput-object v6, v5, v8

    .line 399
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 455
    :cond_2
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 456
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 457
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method private search()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 256
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    .line 257
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchString:Ljava/lang/String;

    .line 258
    const/4 v0, 0x0

    .line 259
    .local v0, "removeBlankSearchString":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchString:Ljava/lang/String;

    const-string/jumbo v2, " "

    const-string/jumbo v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 260
    sget-boolean v1, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->immOnOff:Z

    if-nez v1, :cond_1

    .line 261
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->imm:Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 262
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setCursorVisible(Z)V

    .line 267
    :goto_0
    if-eqz v0, :cond_0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 268
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->driveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestSearchMusic(Landroid/content/Context;Ljava/lang/String;)V

    .line 270
    :cond_0
    const v1, 0x7f040025

    const v2, 0x7f040001

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->overridePendingTransition(II)V

    .line 271
    return-void

    .line 264
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->imm:Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v4}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 265
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setCursorVisible(Z)V

    goto :goto_0
.end method

.method public static setIntentSearchMode(Z)V
    .locals 0
    .param p0, "flag"    # Z

    .prologue
    .line 73
    sput-boolean p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mIsIntentSearchMode:Z

    .line 74
    return-void
.end method


# virtual methods
.method protected CarAppFinishFunc()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 519
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->CarAppFinishFunc()V

    .line 521
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->imm:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 523
    sput-boolean v2, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->immOnOff:Z

    .line 524
    sput-object v3, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    .line 525
    sput-object v3, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchString:Ljava/lang/String;

    .line 526
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->finish()V

    .line 527
    return-void
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 576
    # invokes: Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->finish()V
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$15(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V

    .line 577
    const v0, 0x7f040025

    const v1, 0x7f040001

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->overridePendingTransition(II)V

    .line 578
    return-void
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 509
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->imm:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 511
    sput-boolean v2, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->immOnOff:Z

    .line 512
    sput-object v3, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    .line 513
    sput-object v3, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchString:Ljava/lang/String;

    .line 514
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->finish()V

    .line 515
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 79
    const v1, 0x7f03001f

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->setContentView(I)V

    .line 80
    const-string/jumbo v1, "SearchMusicListActivity"

    const-string/jumbo v2, "onCreate"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    const v1, 0x7f0900e2

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainViewPager:Landroid/support/v4/view/ViewPager;

    .line 83
    const v1, 0x7f0900e1

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainListLayout:Landroid/widget/LinearLayout;

    .line 84
    const v1, 0x7f0900e3

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 85
    const v1, 0x7f09009f

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    .line 86
    const v1, 0x7f0900e0

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;

    .line 87
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;

    .line 88
    const v2, 0x7f09028e

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 87
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchResultText:Landroid/widget/TextView;

    .line 89
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;

    .line 90
    const v2, 0x7f09028c

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP;

    .line 89
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;

    .line 91
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;

    .line 92
    const v2, 0x7f09028d

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 91
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchClearBtn:Landroid/widget/ImageButton;

    .line 93
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;

    .line 94
    const v2, 0x7f09028b

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 93
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchIconText:Landroid/widget/TextView;

    .line 96
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;

    check-cast v1, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP;

    .line 97
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->getMusicSipStateListener()Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$OnMusicSipStateListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP;->setOnMusicSipStateListener(Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$OnMusicSipStateListener;)V

    .line 98
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;

    const-string/jumbo v2, "disableEmoticonInput=true;"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 100
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->driveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 101
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mOnDriveLinkMusicListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkMusicListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;)V

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 104
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    sget-boolean v1, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mIsIntentSearchMode:Z

    if-eqz v1, :cond_0

    .line 105
    const-string/jumbo v1, "keyword"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchString:Ljava/lang/String;

    .line 106
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->setIntent(Landroid/content/Intent;)V

    .line 110
    :cond_0
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mIsIntentSearchMode:Z

    .line 112
    const-string/jumbo v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->imm:Landroid/view/inputmethod/InputMethodManager;

    .line 113
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchString:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 114
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;

    sget-object v2, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchIconText:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 116
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->search()V

    .line 125
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;

    new-instance v2, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 146
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 170
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;

    .line 171
    new-instance v2, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$5;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->setOnSearchEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 202
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchClearBtn:Landroid/widget/ImageButton;

    new-instance v2, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$6;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 217
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    if-eqz v1, :cond_2

    .line 218
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->initSearchResultList(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;)V

    .line 220
    :cond_2
    const/high16 v1, 0x7f040000

    const v2, 0x7f040025

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->overridePendingTransition(II)V

    .line 221
    return-void

    .line 118
    :cond_3
    sget-boolean v1, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->immOnOff:Z

    if-nez v1, :cond_1

    .line 119
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->imm:Landroid/view/inputmethod/InputMethodManager;

    const/4 v2, 0x2

    .line 120
    const/4 v3, 0x1

    .line 119
    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 531
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onDestroy()V

    .line 532
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 464
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onPause()V

    .line 465
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 225
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->immOnOff:Z

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$7;-><init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V

    .line 233
    const-wide/16 v2, 0x258

    .line 226
    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 238
    :goto_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onResume()V

    .line 239
    return-void

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setCursorVisible(Z)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 536
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 537
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 502
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onUserLeaveHint()V

    .line 503
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->imm:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 504
    sput-boolean v2, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->immOnOff:Z

    .line 505
    return-void
.end method

.method protected setDayMode()V
    .locals 3

    .prologue
    const v2, 0x7f08007a

    .line 600
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->setDayMode()V

    .line 601
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->setDayMode()V

    .line 602
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->setDayNightMode(Z)V

    .line 603
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 604
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 605
    const v1, 0x7f08002a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 606
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 607
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainPageAdapter:Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;

    if-eqz v0, :cond_0

    .line 608
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainPageAdapter:Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->setDayMode()V

    .line 609
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainPageAdapter:Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->notifyDataSetChanged()V

    .line 611
    :cond_0
    return-void
.end method

.method protected setNightMode()V
    .locals 3

    .prologue
    const v2, 0x7f08002e

    .line 583
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->setNightMode()V

    .line 584
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->setNightMode()V

    .line 585
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->setDayNightMode(Z)V

    .line 586
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainListLayout:Landroid/widget/LinearLayout;

    .line 587
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 588
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 589
    const v1, 0x7f08002b

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 590
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mNoListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 591
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainPageAdapter:Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainPageAdapter:Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->setNightMode()V

    .line 593
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainPageAdapter:Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->notifyDataSetChanged()V

    .line 595
    :cond_0
    return-void
.end method
