.class public Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "DriveLinkDBHelper.java"


# static fields
.field private static final DB_DRIVELINK:Ljava/lang/String; = "db_drivelink"

.field public static mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;


# instance fields
.field private mDatabase:Landroid/database/sqlite/SQLiteDatabase;

.field private mDriveLinkDLCallMessageLogTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDLCallMessageLogTableHandler;

.field private mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

.field private mDriveLinkMusicTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;

.field private mPlaceOnTableHandler:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/PlaceOnTableHandler;

.field private mTableHandlerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkBaseTableHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 73
    const-string/jumbo v0, "db_drivelink"

    const/4 v1, 0x5

    invoke-direct {p0, p1, v0, v2, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 30
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    .line 32
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mTableHandlerList:Ljava/util/ArrayList;

    .line 33
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkMusicTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;

    .line 34
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 35
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDLCallMessageLogTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDLCallMessageLogTableHandler;

    .line 36
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mPlaceOnTableHandler:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/PlaceOnTableHandler;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mTableHandlerList:Ljava/util/ArrayList;

    .line 76
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mTableHandlerList:Ljava/util/ArrayList;

    .line 77
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkMusicTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mTableHandlerList:Ljava/util/ArrayList;

    .line 79
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mTableHandlerList:Ljava/util/ArrayList;

    .line 81
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDLCallMessageLogTableHandler;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDLCallMessageLogTableHandler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDLCallMessageLogTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDLCallMessageLogTableHandler;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mTableHandlerList:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/PlaceOnTableHandler;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/PlaceOnTableHandler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mPlaceOnTableHandler:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/PlaceOnTableHandler;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    .line 85
    return-void
.end method

.method public static declared-synchronized createDB(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    const-class v1, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    if-nez v0, :cond_1

    .line 40
    const-class v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 41
    :try_start_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    .line 40
    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 46
    const/4 v0, 0x1

    .line 48
    :goto_0
    monitor-exit v1

    return v0

    .line 40
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 39
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 48
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 112
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mTableHandlerList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 113
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mTableHandlerList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 117
    :cond_0
    return-void

    .line 113
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkBaseTableHandler;

    .line 114
    .local v0, "tableHandler":Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkBaseTableHandler;
    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkBaseTableHandler;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0
.end method

.method public static declared-synchronized delInstance()V
    .locals 3

    .prologue
    .line 57
    const-class v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 67
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    monitor-exit v2

    return-void

    .line 61
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 66
    .restart local v0    # "e":Ljava/lang/Exception;
    :goto_1
    const/4 v1, 0x0

    :try_start_2
    sput-object v1, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 57
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 62
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 63
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private dropTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 120
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mTableHandlerList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 121
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mTableHandlerList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 125
    :cond_0
    return-void

    .line 121
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkBaseTableHandler;

    .line 122
    .local v0, "tableHandler":Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkBaseTableHandler;
    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkBaseTableHandler;->dropTables(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    return-object v0
.end method


# virtual methods
.method public addDLCallMessageLog(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)V
    .locals 3
    .param p1, "log"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .prologue
    .line 213
    :try_start_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 215
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDLCallMessageLogTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDLCallMessageLogTableHandler;

    invoke-virtual {v2, v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDLCallMessageLogTableHandler;->addDLCallMessageLog(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    return-void

    .line 217
    :catch_0
    move-exception v1

    .line 218
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public addFriendsToGroupShared(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 6
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 333
    .local p1, "friends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 335
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    invoke-virtual {v3, v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->addFriendsToGroupShared(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v3

    .line 336
    if-nez v3, :cond_0

    .line 337
    const-string/jumbo v3, "db_drivelink"

    .line 338
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Fail to save participants of group "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 337
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    const/4 v1, 0x0

    .line 350
    :goto_0
    return v1

    .line 344
    :cond_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 345
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->getLocationTrackingTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;

    move-result-object v2

    .line 348
    .local v2, "locationTrackingTableHandler":Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;
    invoke-virtual {v2, v0, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;->saveParticipantTracking(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v1

    .line 350
    .local v1, "isFriendsAddedToGroupShared":Z
    goto :goto_0
.end method

.method public changeGroupDestination(ILcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 3
    .param p1, "groupId"    # I
    .param p2, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 354
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 356
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 357
    invoke-virtual {v2, v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->changeGroupDestination(Landroid/database/sqlite/SQLiteDatabase;ILcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v1

    .line 359
    .local v1, "isGroupDestinationChanged":Z
    return v1
.end method

.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 108
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    monitor-exit p0

    return-void

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public deleteAllGroupShared()Z
    .locals 3

    .prologue
    .line 391
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 393
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 394
    invoke-virtual {v2, v0}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->deleteAllGroupShared(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v1

    .line 396
    .local v1, "isAllGroupSharedDeleted":Z
    return v1
.end method

.method public getAllParticipantTracking(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Ljava/util/HashMap;
    .locals 3
    .param p1, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 373
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 375
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 376
    invoke-virtual {v2, v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->getAllParticipantTracking(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Ljava/util/HashMap;

    move-result-object v1

    .line 378
    .local v1, "participantTrackingList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;>;"
    return-object v1
.end method

.method public getDLCallMessageLogList(Ljava/util/Date;I)Ljava/util/ArrayList;
    .locals 4
    .param p1, "fromDate"    # Ljava/util/Date;
    .param p2, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;",
            ">;"
        }
    .end annotation

    .prologue
    .line 223
    const/4 v2, 0x0

    .line 226
    .local v2, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;>;"
    :try_start_0
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 228
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDLCallMessageLogTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDLCallMessageLogTableHandler;

    .line 229
    invoke-virtual {v3, v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDLCallMessageLogTableHandler;->getDLCallMessageLogList(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Date;I)Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 235
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    return-object v2

    .line 231
    :catch_0
    move-exception v1

    .line 232
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    return-object v0
.end method

.method public getGroupById(I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .locals 3
    .param p1, "groupId"    # I

    .prologue
    .line 363
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 365
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    invoke-virtual {v2, v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->getGroubById(Landroid/database/sqlite/SQLiteDatabase;I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    move-result-object v1

    .line 368
    .local v1, "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    return-object v1
.end method

.method public getLocation(DD)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .locals 7
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 400
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 402
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->getLocation(Landroid/database/sqlite/SQLiteDatabase;DD)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v6

    .line 405
    .local v6, "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    return-object v6
.end method

.method public getLocationGroup()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .locals 2

    .prologue
    .line 318
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 319
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->getLocationGroup(Landroid/database/sqlite/SQLiteDatabase;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    move-result-object v1

    return-object v1
.end method

.method public getProfileByPhoneNumber(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;
    .locals 3
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 254
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 256
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mPlaceOnTableHandler:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/PlaceOnTableHandler;

    .line 257
    invoke-virtual {v2, v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/PlaceOnTableHandler;->getProfileByPhoneNumber(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;

    move-result-object v1

    .line 259
    .local v1, "locationBuddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;
    return-object v1
.end method

.method public getUserProfileByPhoneNumber(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .locals 3
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 273
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 274
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 275
    invoke-virtual {v2, v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->getUserProfileByPhoneNumber(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v1

    .line 277
    .local v1, "result":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    return-object v1
.end method

.method public insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "mCountry"    # Ljava/lang/String;
    .param p2, "mSaleCode"    # Ljava/lang/String;
    .param p3, "mNavigation"    # Ljava/lang/String;

    .prologue
    .line 410
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 411
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    invoke-virtual {v1, v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->insertNavigationPackage(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public loadLocationFriendPendingList(IJ)Ljava/util/ArrayList;
    .locals 3
    .param p1, "maxCount"    # I
    .param p2, "fromTimestamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 188
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 189
    invoke-virtual {v2, v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->loadLocationFriendPendingList(Landroid/database/sqlite/SQLiteDatabase;IJ)Ljava/util/ArrayList;

    move-result-object v1

    .line 191
    .local v1, "mLocationFriendPendingList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    return-object v1
.end method

.method public loadLocationFriendSharedList(IJ)Ljava/util/ArrayList;
    .locals 3
    .param p1, "maxCount"    # I
    .param p2, "fromTimestamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 179
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 180
    invoke-virtual {v2, v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->loadLocationFriendSharedList(Landroid/database/sqlite/SQLiteDatabase;IJ)Ljava/util/ArrayList;

    move-result-object v1

    .line 181
    .local v1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    return-object v1
.end method

.method public loadLocationGroupList(IJ)Ljava/util/ArrayList;
    .locals 3
    .param p1, "maxCount"    # I
    .param p2, "fromExpiretime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 311
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 312
    invoke-virtual {v2, v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->loadLocationGroupList(Landroid/database/sqlite/SQLiteDatabase;IJ)Ljava/util/ArrayList;

    move-result-object v1

    .line 314
    .local v1, "locationGroupList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    return-object v1
.end method

.method public loadLocationMyPlaceList()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 195
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 197
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 198
    invoke-virtual {v2, v0}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->loadLocationMyPlaceList(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/ArrayList;

    move-result-object v1

    .line 200
    .local v1, "mLocationMyPlaceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    return-object v1
.end method

.method public loadLocationSearchedList(IJ)Ljava/util/ArrayList;
    .locals 3
    .param p1, "maxCount"    # I
    .param p2, "fromTimestamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 324
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 325
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 326
    invoke-virtual {v2, v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->loadLocationSearchedList(Landroid/database/sqlite/SQLiteDatabase;IJ)Ljava/util/ArrayList;

    move-result-object v1

    .line 328
    .local v1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    return-object v1
.end method

.method public loadNavigationPackage(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1, "mCountry"    # Ljava/lang/String;
    .param p2, "mSaleCode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 417
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    invoke-virtual {v1, v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->loadNavigationPackage(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 90
    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->dropTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 103
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 104
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->dropTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 96
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 97
    return-void
.end method

.method public placeOnProfileExists(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;
    .locals 4
    .param p1, "buddy"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;

    .prologue
    .line 239
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 241
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mPlaceOnTableHandler:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/PlaceOnTableHandler;

    .line 242
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    .line 241
    invoke-virtual {v2, v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/PlaceOnTableHandler;->profile(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;

    move-result-object v1

    .line 244
    .local v1, "dlLocationBuddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;
    return-object v1
.end method

.method public restoreLastMusicIdList()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 140
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkMusicTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;

    .line 141
    invoke-virtual {v2, v0}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;->restoreLastMusicIdList(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/ArrayList;

    move-result-object v1

    .line 143
    .local v1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-object v1
.end method

.method public restoreLastPlayedMusicId()I
    .locals 3

    .prologue
    .line 153
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 155
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkMusicTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;

    invoke-virtual {v2, v0}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;->restoreLastPlayedMusicId(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v1

    .line 157
    .local v1, "musicId":I
    return v1
.end method

.method public restoreMusicPlayerEnvInfo()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;
    .locals 3

    .prologue
    .line 161
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 163
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkMusicTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;

    .line 164
    invoke-virtual {v2, v0}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;->restoreMusicPlayerEnvInfo(Landroid/database/sqlite/SQLiteDatabase;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    move-result-object v1

    .line 166
    .local v1, "musicPlayerEnvInfo":Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;
    return-object v1
.end method

.method public saveGroupParticipant(ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z
    .locals 3
    .param p1, "groupId"    # I
    .param p2, "participant"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    .line 382
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 384
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 385
    invoke-virtual {v2, v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->saveGroupParticipant(Landroid/database/sqlite/SQLiteDatabase;ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z

    move-result v1

    .line 387
    .local v1, "saveGroup":Z
    return v1
.end method

.method public saveLastMusicIdList(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 132
    .local p1, "lastMusicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 134
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkMusicTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;

    invoke-virtual {v1, v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;->saveLastMusicIdList(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)V

    .line 135
    return-void
.end method

.method public saveLastPlayedMusicId(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 2
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 147
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 149
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkMusicTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;

    invoke-virtual {v1, v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;->saveLastPlayedMusicId(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    .line 150
    return-void
.end method

.method public saveLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 3
    .param p1, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 281
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 283
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    invoke-virtual {v2, v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->saveLocation(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v1

    .line 286
    .local v1, "saveLocation":Z
    return v1
.end method

.method public saveLocationFriendShared(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z
    .locals 3
    .param p1, "friend"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    .line 290
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 292
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 293
    invoke-virtual {v2, v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->saveLocationFriendShared(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z

    move-result v1

    .line 295
    .local v1, "saveLocationFriendShared":Z
    return v1
.end method

.method public saveLocationGroupShared(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 3
    .param p1, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 299
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 301
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 302
    invoke-virtual {v2, v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->saveLocationGroupShared(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v1

    .line 304
    .local v1, "saveLocation":Z
    return v1
.end method

.method public saveMusicPlayerEnvInfo(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;)V
    .locals 2
    .param p1, "musicPlayerEnvInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    .prologue
    .line 170
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 172
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkMusicTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;

    invoke-virtual {v1, v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkMusicTableHandler;->saveMusicPlayerEnvInfo(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;)V

    .line 174
    return-void
.end method

.method public saveMyPlace(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;)Z
    .locals 3
    .param p1, "place"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;

    .prologue
    .line 204
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 206
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    invoke-virtual {v2, v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->saveMyPlace(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;)Z

    move-result v1

    .line 208
    .local v1, "result":Z
    return v1
.end method

.method public saveProfile(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;)V
    .locals 2
    .param p1, "buddy"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;

    .prologue
    .line 248
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 249
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mPlaceOnTableHandler:Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/PlaceOnTableHandler;

    invoke-virtual {v1, v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/PlaceOnTableHandler;->saveProfile(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;)V

    .line 251
    return-void
.end method

.method public saveUserProfile(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)Z
    .locals 3
    .param p1, "profile"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    .prologue
    .line 264
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkDBHelper:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 266
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    invoke-virtual {v2, v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->saveUserProfile(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)Z

    move-result v1

    .line 269
    .local v1, "saveUser":Z
    return v1
.end method
