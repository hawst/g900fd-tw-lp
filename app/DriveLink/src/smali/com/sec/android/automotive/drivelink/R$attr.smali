.class public final Lcom/sec/android/automotive/drivelink/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final adSize:I = 0x7f010000

.field public static final adUnitId:I = 0x7f010001

.field public static final background:I = 0x7f010011

.field public static final black_style:I = 0x7f01001b

.field public static final btn_image:I = 0x7f010014

.field public static final cameraBearing:I = 0x7f010003

.field public static final cameraTargetLat:I = 0x7f010004

.field public static final cameraTargetLng:I = 0x7f010005

.field public static final cameraTilt:I = 0x7f010006

.field public static final cameraZoom:I = 0x7f010007

.field public static final mapType:I = 0x7f010002

.field public static final mark_show:I = 0x7f010013

.field public static final orientation:I = 0x7f010010

.field public static final show_prev_btn:I = 0x7f010019

.field public static final show_search_btn:I = 0x7f01001a

.field public static final small_mode:I = 0x7f01001c

.field public static final style_type:I = 0x7f010012

.field public static final txt_color:I = 0x7f010017

.field public static final txt_color_selector:I = 0x7f010018

.field public static final txt_string:I = 0x7f010015

.field public static final txt_style:I = 0x7f010016

.field public static final uiCompass:I = 0x7f010008

.field public static final uiRotateGestures:I = 0x7f010009

.field public static final uiScrollGestures:I = 0x7f01000a

.field public static final uiTiltGestures:I = 0x7f01000b

.field public static final uiZoomControls:I = 0x7f01000c

.field public static final uiZoomGestures:I = 0x7f01000d

.field public static final useViewLifecycle:I = 0x7f01000e

.field public static final zOrderOnTop:I = 0x7f01000f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
