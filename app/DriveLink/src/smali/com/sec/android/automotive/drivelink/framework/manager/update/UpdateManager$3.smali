.class Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$3;
.super Landroid/os/Handler;
.source "UpdateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;

    .line 92
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 94
    iget v1, p1, Landroid/os/Message;->arg1:I

    packed-switch v1, :pswitch_data_0

    .line 107
    :goto_0
    return-void

    .line 96
    :pswitch_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 97
    .local v0, "errorType":I
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;

    .line 98
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->getEnum(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    move-result-object v2

    .line 97
    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->sendUpdateFailResult(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;)V
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;)V

    goto :goto_0

    .line 102
    .end local v0    # "errorType":I
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLDownloadData;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->sendDownloadProgress(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLDownloadData;)V
    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->access$6(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLDownloadData;)V

    goto :goto_0

    .line 94
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
