.class Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$8;
.super Ljava/lang/Object;
.source "OnDriveLinkSamsungAccountListenerImp.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->onReceiveSamsungAccountAuthCode(ZLandroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;

.field private final synthetic val$_isSuccess:Z

.field private final synthetic val$_resultData:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;ZLandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$8;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;

    iput-boolean p2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$8;->val$_isSuccess:Z

    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$8;->val$_resultData:Landroid/os/Bundle;

    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$8;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$8;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v0

    .line 171
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$8;->val$_isSuccess:Z

    .line 172
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$8;->val$_resultData:Landroid/os/Bundle;

    .line 171
    invoke-interface {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onReceiveSamsungAccountAuthCode(ZLandroid/os/Bundle;)V

    .line 174
    :cond_0
    return-void
.end method
