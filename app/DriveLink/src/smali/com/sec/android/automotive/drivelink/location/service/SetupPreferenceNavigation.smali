.class public Lcom/sec/android/automotive/drivelink/location/service/SetupPreferenceNavigation;
.super Ljava/lang/Object;
.source "SetupPreferenceNavigation.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static isNaviPrefSetUp()Z
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 69
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    .line 70
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 69
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 72
    .local v1, "preference":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v2, "PREF_SETTINGS_MY_NAVIGATION"

    .line 71
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v0

    .line 74
    .local v0, "isSet":I
    if-eq v0, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static saveNaviPrefSettings(I)V
    .locals 3
    .param p0, "mapID"    # I

    .prologue
    .line 11
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->getPackageNameChosen(I)Ljava/lang/String;

    move-result-object v0

    .line 14
    .local v0, "navigationPackageName":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    .line 15
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 14
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 17
    .local v1, "preference":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v2, "PREF_SETTINGS_MY_NAVIGATION"

    invoke-virtual {v1, v2, p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;I)V

    .line 19
    const-string/jumbo v2, "PREF_SETTINGS_MY_NAVIGATION_PACKAGE"

    .line 18
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    return-void
.end method

.method public static setUpNaviPrefSettings()V
    .locals 3

    .prologue
    .line 24
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/service/SetupPreferenceNavigation;->isNaviPrefSetUp()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 64
    .local v0, "mSalesProperty":Ljava/lang/String;
    .local v1, "mUserCountry":Ljava/lang/String;
    :goto_0
    return-void

    .line 28
    .end local v0    # "mSalesProperty":Ljava/lang/String;
    .end local v1    # "mUserCountry":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->getUserCountryForNavi()Ljava/lang/String;

    move-result-object v1

    .line 29
    .restart local v1    # "mUserCountry":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->getSalesCodePropertyForNavi()Ljava/lang/String;

    move-result-object v0

    .line 31
    .restart local v0    # "mSalesProperty":Ljava/lang/String;
    if-eqz v1, :cond_1

    if-nez v0, :cond_2

    .line 32
    :cond_1
    sget v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_GOOGLE_MAPS:I

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/service/SetupPreferenceNavigation;->saveNaviPrefSettings(I)V

    goto :goto_0

    .line 37
    :cond_2
    const-string/jumbo v2, "KR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 38
    const-string/jumbo v2, "SKT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "SKC"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 39
    const-string/jumbo v2, "SKO"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 40
    :cond_3
    sget v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_TMAP:I

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/service/SetupPreferenceNavigation;->saveNaviPrefSettings(I)V

    goto :goto_0

    .line 41
    :cond_4
    const-string/jumbo v2, "KTT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 42
    const-string/jumbo v2, "KTC"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 43
    const-string/jumbo v2, "KTO"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 44
    const-string/jumbo v2, "ANY"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 45
    const-string/jumbo v2, "KOO"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 46
    :cond_5
    sget v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_OLLEH_NAVI:I

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/service/SetupPreferenceNavigation;->saveNaviPrefSettings(I)V

    goto :goto_0

    .line 47
    :cond_6
    const-string/jumbo v2, "LGT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 48
    const-string/jumbo v2, "LUC"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 49
    const-string/jumbo v2, "LUO"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 50
    :cond_7
    sget v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_UNAVI:I

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/service/SetupPreferenceNavigation;->saveNaviPrefSettings(I)V

    goto/16 :goto_0

    .line 52
    :cond_8
    sget v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_GOOGLE_MAPS:I

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/service/SetupPreferenceNavigation;->saveNaviPrefSettings(I)V

    goto/16 :goto_0

    .line 55
    :cond_9
    const-string/jumbo v2, "CN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 60
    sget v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_BAIDU_NAVI:I

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/service/SetupPreferenceNavigation;->saveNaviPrefSettings(I)V

    goto/16 :goto_0

    .line 63
    :cond_a
    sget v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_GOOGLE_MAPS:I

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/service/SetupPreferenceNavigation;->saveNaviPrefSettings(I)V

    goto/16 :goto_0
.end method
