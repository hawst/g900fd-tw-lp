.class public Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;
.super Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;
.source "PushMessageManager.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/IPushMessageServiceListener;


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field private mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;-><init>()V

    .line 132
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->mConnection:Landroid/content/ServiceConnection;

    .line 20
    return-void
.end method

.method private bindPushMessageService()V
    .locals 4

    .prologue
    .line 44
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "bindPushMessageService"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->mContext:Landroid/content/Context;

    .line 46
    const-class v2, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;

    .line 45
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 47
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->mConnection:Landroid/content/ServiceConnection;

    .line 48
    const/4 v3, 0x1

    .line 47
    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 49
    return-void
.end method


# virtual methods
.method public gcmRegister(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 129
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->getInstance()Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/GcmClient;->register(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public initialize(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->initialize(Landroid/content/Context;)Z

    .line 38
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->mContext:Landroid/content/Context;

    .line 39
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->bindPushMessageService()V

    .line 40
    const/4 v0, 0x1

    return v0
.end method

.method public onMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1, "groupId"    # Ljava/lang/String;
    .param p2, "groupType"    # Ljava/lang/String;
    .param p3, "data"    # Ljava/lang/String;

    .prologue
    .line 60
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "onMessage "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 63
    .local v2, "jObj":Lorg/json/JSONObject;
    const-string/jumbo v8, "user"

    invoke-virtual {v2, v8}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 65
    .local v3, "jsonData":Lorg/json/JSONObject;
    const-string/jumbo v8, "phoneNumber"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 66
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->TAG:Ljava/lang/String;

    const-string/jumbo v9, "Missing user phone number on \'Push Message\'"

    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    .end local v2    # "jObj":Lorg/json/JSONObject;
    .end local v3    # "jsonData":Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 70
    .restart local v2    # "jObj":Lorg/json/JSONObject;
    .restart local v3    # "jsonData":Lorg/json/JSONObject;
    :cond_0
    const/4 v1, 0x0

    .line 73
    .local v1, "intGroupType":I
    :try_start_1
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v1

    .line 80
    :try_start_2
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;->getTypeFromInt(I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    move-result-object v7

    .line 81
    .local v7, "type":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;
    const-string/jumbo v8, "phoneNumber"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 83
    .local v5, "phone":Ljava/lang/String;
    new-instance v4, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    .line 84
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 83
    invoke-direct {v4, v5, p1, v8, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 85
    .local v4, "msgInfo":Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->SMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    invoke-virtual {v4, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->setMSGType(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;)V

    .line 86
    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->setPhoneNumber(Ljava/lang/String;)V

    .line 87
    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->setName(Ljava/lang/String;)V

    .line 90
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;->REQUEST_LOCATION:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    if-ne v7, v8, :cond_3

    .line 91
    sget-object v6, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->REJECTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    .line 93
    .local v6, "statusCode":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;
    const-string/jumbo v8, "statusCode"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 95
    const-string/jumbo v8, "statusCode"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    invoke-static {v8}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->getTypeFromInt(I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    move-result-object v6

    .line 97
    :cond_1
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->ACCEPTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    if-eq v6, v8, :cond_2

    .line 98
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Friend "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 99
    const-string/jumbo v10, " did not accept share his location."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 98
    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 110
    .end local v1    # "intGroupType":I
    .end local v2    # "jObj":Lorg/json/JSONObject;
    .end local v3    # "jsonData":Lorg/json/JSONObject;
    .end local v4    # "msgInfo":Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;
    .end local v5    # "phone":Ljava/lang/String;
    .end local v6    # "statusCode":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;
    .end local v7    # "type":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Lorg/json/JSONException;
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->TAG:Ljava/lang/String;

    const-string/jumbo v9, "onMessage"

    invoke-static {v8, v9, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 74
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v1    # "intGroupType":I
    .restart local v2    # "jObj":Lorg/json/JSONObject;
    .restart local v3    # "jsonData":Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    .line 75
    .local v0, "e":Ljava/lang/NumberFormatException;
    :try_start_3
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->TAG:Ljava/lang/String;

    const-string/jumbo v9, "Fail to parse groupType \'Push Message\'"

    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 103
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .restart local v4    # "msgInfo":Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;
    .restart local v5    # "phone":Ljava/lang/String;
    .restart local v6    # "statusCode":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;
    .restart local v7    # "type":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;
    :cond_2
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->TAG:Ljava/lang/String;

    const-string/jumbo v9, "Notifying location share from Push ..."

    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getNotificationManager()Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    move-result-object v8

    invoke-virtual {v8, v4, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->NotifyLocationShare(Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 108
    .end local v6    # "statusCode":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;
    :cond_3
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->TAG:Ljava/lang/String;

    const-string/jumbo v9, "Invalid groupType for \'Push Message\'"

    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 125
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "terminate"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    return-void
.end method
