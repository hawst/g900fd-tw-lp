.class Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$1;
.super Ljava/lang/Object;
.source "SetupWizardMyCarActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 131
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mBTState:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->access$0(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    move-result-object v1

    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->UNREGISTERED_CONNECTED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    if-ne v1, v2, :cond_0

    .line 132
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    .line 133
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->saveConnectedDeviceToSharedPreferences()V

    .line 135
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 142
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->getUserTouringGuideDone()Z

    move-result v1

    if-nez v1, :cond_1

    .line 143
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    .line 144
    const-class v2, Lcom/sec/android/automotive/drivelink/firstaccess/TutorialGuideActivity01;

    .line 143
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 150
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->startActivity(Landroid/content/Intent;)V

    .line 151
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->finish()V

    .line 152
    return-void

    .line 146
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    .line 147
    const-class v2, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    .line 146
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0
.end method
