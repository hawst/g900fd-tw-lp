.class Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$3;
.super Ljava/lang/Object;
.source "VoiceMessageComposerActionBarLayout.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->startScaleAnimation(FJLandroid/view/animation/Interpolator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    .line 451
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 455
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 459
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->isRestore:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->access$7(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 460
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->access$8(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;Z)V

    .line 461
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    const/high16 v1, 0x3f800000    # 1.0f

    const-wide/16 v2, 0x64

    # getter for: Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->access$9()Landroid/view/animation/Interpolator;

    move-result-object v4

    # invokes: Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->startScaleAnimation(FJLandroid/view/animation/Interpolator;)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->access$10(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;FJLandroid/view/animation/Interpolator;)V

    .line 465
    :goto_0
    return-void

    .line 463
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->access$8(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;Z)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 470
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 475
    return-void
.end method
