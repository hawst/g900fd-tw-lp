.class public Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "SuggestionsPageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static final ITEM_NUM_PER_PAGE:I = 0x4

.field private static final MAX_PAGE_TO_LOOP:I = 0x4e20


# instance fields
.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mInitialPosition:I

.field private mIsNightMode:Z

.field private mPageCount:I

.field private mSuggestionsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "clickListener"    # Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    const/4 v3, 0x0

    .line 37
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 35
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mIsNightMode:Z

    .line 39
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mContext:Landroid/content/Context;

    .line 40
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    .line 41
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    .line 42
    const-string/jumbo v1, "TEST"

    const-string/jumbo v2, "SUGGESTIONSPAGEADAPTER"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->getPageCount()I

    move-result v0

    .line 44
    .local v0, "pageCount":I
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 46
    const/16 v1, 0x2710

    rem-int/2addr v1, v0

    rsub-int v1, v1, 0x2710

    .line 45
    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mInitialPosition:I

    .line 47
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mPageCount:I

    .line 52
    :goto_0
    return-void

    .line 49
    :cond_0
    iput v3, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mInitialPosition:I

    .line 50
    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mPageCount:I

    goto :goto_0
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 164
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 165
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mPageCount:I

    return v0
.end method

.method public getInitialPosition()I
    .locals 1

    .prologue
    .line 188
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mInitialPosition:I

    return v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 193
    const/4 v0, -0x2

    return v0
.end method

.method public getPageCount()I
    .locals 3

    .prologue
    .line 173
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    div-int/lit8 v0, v2, 0x4

    .line 174
    .local v0, "page":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    rem-int/lit8 v1, v2, 0x4

    .line 175
    .local v1, "remainder":I
    if-lez v1, :cond_0

    .line 176
    add-int/lit8 v0, v0, 0x1

    .line 179
    :cond_0
    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 12
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 74
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mContext:Landroid/content/Context;

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 75
    .local v5, "layoutInflater":Landroid/view/LayoutInflater;
    const v8, 0x7f030093

    const/4 v9, 0x0

    invoke-virtual {v5, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 77
    .local v7, "v":Landroid/view/View;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 78
    .local v6, "listItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/widget/RelativeLayout;>;"
    const v8, 0x7f090207

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    const v8, 0x7f090208

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    const v8, 0x7f090209

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    const v8, 0x7f09020a

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->getPageCount()I

    move-result v8

    rem-int v8, p2, v8

    mul-int/lit8 v3, v8, 0x4

    .line 88
    .local v3, "index":I
    const/4 v2, 0x1

    .line 89
    .local v2, "i":I
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_0

    .line 157
    invoke-virtual {v7, p2}, Landroid/view/View;->setId(I)V

    .line 158
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    invoke-virtual {p1, v7}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    .line 159
    return-object v7

    .line 89
    .restart local p1    # "container":Landroid/view/ViewGroup;
    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    .line 90
    .local v4, "layout":Landroid/widget/RelativeLayout;
    new-instance v1, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;

    const/4 v8, 0x0

    invoke-direct {v1, v8}, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;-><init>(Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;)V

    .line 93
    .local v1, "holder":Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;
    const v8, 0x7f0901f7

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 92
    iput-object v8, v1, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    .line 95
    const v8, 0x7f09028f

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 94
    iput-object v8, v1, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;->ivContactMaskImage:Landroid/widget/ImageView;

    .line 97
    const v8, 0x7f0901f8

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 96
    iput-object v8, v1, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    .line 99
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-le v8, v3, :cond_6

    .line 100
    invoke-virtual {v4, v2}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 101
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v8

    .line 103
    invoke-virtual {v8, v3}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getPhoneSuggestionThumbnail(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 105
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_2

    .line 106
    iget-object v8, v1, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    .line 107
    const v10, 0x7f02023d

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 112
    :goto_1
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    const/4 v10, 0x2

    if-ne v8, v10, :cond_3

    .line 113
    iget-object v8, v1, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    const/4 v10, 0x1

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setLines(I)V

    .line 118
    :goto_2
    const-string/jumbo v10, ""

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 119
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 120
    iget-object v10, v1, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 121
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v8

    .line 120
    invoke-virtual {v10, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    :goto_3
    iget-boolean v8, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mIsNightMode:Z

    if-eqz v8, :cond_5

    .line 127
    iget-object v8, v1, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;->ivContactMaskImage:Landroid/widget/ImageView;

    .line 128
    const v10, 0x7f02020a

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 129
    iget-object v8, v1, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v10

    .line 130
    invoke-virtual {v10}, Lcom/sec/android/automotive/drivelink/DLApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 131
    const v11, 0x7f080030

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    .line 129
    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 132
    const v8, 0x7f020002

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 153
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    :goto_4
    add-int/lit8 v3, v3, 0x1

    .line 154
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 109
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_2
    iget-object v8, v1, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 115
    :cond_3
    iget-object v8, v1, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    const/4 v10, 0x2

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setLines(I)V

    goto :goto_2

    .line 123
    :cond_4
    iget-object v10, v1, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 124
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v8

    const/4 v11, 0x0

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v8

    .line 123
    invoke-virtual {v10, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 134
    :cond_5
    iget-object v8, v1, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;->ivContactMaskImage:Landroid/widget/ImageView;

    .line 135
    const v10, 0x7f020209

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 136
    iget-object v8, v1, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v10

    .line 137
    invoke-virtual {v10}, Lcom/sec/android/automotive/drivelink/DLApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 138
    const v11, 0x7f08002f

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    .line 136
    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_4

    .line 143
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_6
    iget-object v8, v1, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    const/16 v10, 0x8

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 144
    iget-object v8, v1, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;->ivContactMaskImage:Landroid/widget/ImageView;

    const/16 v10, 0x8

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 145
    iget-object v8, v1, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    const/16 v10, 0x8

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 146
    iget-boolean v8, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mIsNightMode:Z

    if-eqz v8, :cond_1

    .line 147
    const v8, 0x7f020002

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_4
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 184
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setNightMode(Z)V
    .locals 0
    .param p1, "isNightMode"    # Z

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mIsNightMode:Z

    .line 70
    return-void
.end method

.method public setSuggestionsList(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "suggestionsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mSuggestionsList:Ljava/util/ArrayList;

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->getPageCount()I

    move-result v0

    .line 58
    .local v0, "pageCount":I
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 60
    const/16 v1, 0x2710

    rem-int/2addr v1, v0

    rsub-int v1, v1, 0x2710

    .line 59
    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mInitialPosition:I

    .line 61
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mPageCount:I

    .line 66
    :goto_0
    return-void

    .line 63
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mInitialPosition:I

    .line 64
    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/SuggestionsPageAdapter;->mPageCount:I

    goto :goto_0
.end method
