.class public Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "BaseActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;
.implements Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;


# static fields
.field private static final SETTING_DRIVLINK:Ljava/lang/String; = "drive_link_setting"

.field public static final TAG:Ljava/lang/String; = "BaseActivity"

.field public static airplaneModeDoneCount:I


# instance fields
.field protected keyguardManager:Landroid/app/KeyguardManager;

.field private mActivityLocale:Ljava/lang/String;

.field private mBackupMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mBlockingSoundChangeReceiver:Landroid/content/BroadcastReceiver;

.field private mCarModeOffReceiver:Landroid/content/BroadcastReceiver;

.field protected mDensity:F

.field private mDummyAudioFocus:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field protected mFlowID:Ljava/lang/String;

.field protected mIsAFRequired:Z

.field protected mIsFlowChanged:Z

.field protected mIsRotation:Z

.field private mLayoutContents:Landroid/widget/RelativeLayout;

.field protected mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

.field protected mOrientation:I

.field protected mPrevFlowID:Ljava/lang/String;

.field private mVoiceCommandReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->airplaneModeDoneCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 70
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mFlowID:Ljava/lang/String;

    .line 71
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mPrevFlowID:Ljava/lang/String;

    .line 75
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mBlockingSoundChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 78
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mBackupMap:Ljava/util/HashMap;

    .line 81
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mIsRotation:Z

    .line 84
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mIsFlowChanged:Z

    .line 85
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->keyguardManager:Landroid/app/KeyguardManager;

    .line 87
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mActivityLocale:Ljava/lang/String;

    .line 91
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mIsAFRequired:Z

    .line 964
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mDummyAudioFocus:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 65
    return-void
.end method

.method private clearCurrentActivity()V
    .locals 3

    .prologue
    .line 288
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    .line 289
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v0

    .line 290
    .local v0, "currentActivity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 291
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->setCurrentActivity(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;)V

    .line 293
    :cond_0
    return-void
.end method

.method private getdensity()V
    .locals 2

    .prologue
    .line 782
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 783
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 785
    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mDensity:F

    .line 787
    return-void
.end method

.method private isAppForeground()Z
    .locals 16

    .prologue
    .line 366
    const/4 v4, 0x0

    .line 367
    .local v4, "isForeground":Z
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    .line 368
    invoke-virtual {v12}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    .line 369
    const-string/jumbo v13, "activity"

    .line 368
    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 367
    check-cast v0, Landroid/app/ActivityManager;

    .line 392
    .local v0, "am":Landroid/app/ActivityManager;
    const/4 v12, 0x2

    invoke-virtual {v0, v12}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v11

    .line 393
    .local v11, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v11, :cond_2

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_2

    .line 394
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 395
    .local v7, "pkgNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 396
    .local v2, "count":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v12

    iget-object v1, v12, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 397
    .local v1, "appPkgName":Ljava/lang/String;
    const-string/jumbo v12, "BaseActivity"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string/jumbo v14, "App Pkg Name:"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-nez v13, :cond_3

    .line 411
    :goto_1
    if-nez v4, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->isMultiWindow()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v12

    const/4 v13, 0x2

    if-lt v12, v13, :cond_2

    .line 412
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/map/MapFactory;->newNavigationMap()Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    move-result-object v6

    .line 413
    .local v6, "navigation":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->getNaviPackageName()Ljava/lang/String;

    move-result-object v5

    .line 414
    .local v5, "naviPkgName":Ljava/lang/String;
    const-string/jumbo v12, "BaseActivity"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string/jumbo v14, "Navi Pkg Name:"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    const/4 v12, 0x0

    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 417
    .local v3, "firstPkg":Ljava/lang/String;
    const/4 v12, 0x1

    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 419
    .local v9, "secondPkg":Ljava/lang/String;
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 420
    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 421
    :cond_0
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 422
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 423
    :cond_1
    const/4 v4, 0x1

    .line 427
    .end local v1    # "appPkgName":Ljava/lang/String;
    .end local v2    # "count":I
    .end local v3    # "firstPkg":Ljava/lang/String;
    .end local v5    # "naviPkgName":Ljava/lang/String;
    .end local v6    # "navigation":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    .end local v7    # "pkgNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v9    # "secondPkg":Ljava/lang/String;
    :cond_2
    return v4

    .line 398
    .restart local v1    # "appPkgName":Ljava/lang/String;
    .restart local v2    # "count":I
    .restart local v7    # "pkgNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 399
    .local v10, "task":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v13, v10, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v13}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    .line 400
    .local v8, "pkgname":Ljava/lang/String;
    const-string/jumbo v13, "BaseActivity"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string/jumbo v15, "["

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "]PackageName:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    if-nez v2, :cond_4

    .line 402
    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 403
    const/4 v4, 0x1

    .line 404
    goto/16 :goto_1

    .line 407
    :cond_4
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 408
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0
.end method

.method private setBaseBroadCastReceiver()V
    .locals 3

    .prologue
    .line 461
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mVoiceCommandReceiver:Landroid/content/BroadcastReceiver;

    .line 576
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mVoiceCommandReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    .line 577
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.ACTION_DL_DM_FLOW_COMMAND"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 576
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 578
    return-void
.end method

.method private setBlockingSoundChangeReceiver()V
    .locals 3

    .prologue
    .line 604
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mBlockingSoundChangeReceiver:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_0

    .line 605
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/receiver/SoundSettingReceiver;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mBlockingSoundChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 606
    const-string/jumbo v0, "BaseActivity"

    const-string/jumbo v1, "register - SoundSettingReceiver"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mBlockingSoundChangeReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    .line 608
    const-string/jumbo v2, "android.media.RINGER_MODE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 607
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 610
    :cond_0
    return-void
.end method

.method private setCarModeChangeToOffReceiver()V
    .locals 3

    .prologue
    .line 581
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$6;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mCarModeOffReceiver:Landroid/content/BroadcastReceiver;

    .line 593
    const-string/jumbo v0, "BaseActivity"

    const-string/jumbo v1, "setCarModeChangeToOffReceiver 2"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mCarModeOffReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    .line 595
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.carmodeoff.finish"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 594
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 596
    return-void
.end method

.method private setMultiWindowTrayHide()V
    .locals 2

    .prologue
    .line 327
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 329
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    .line 330
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 331
    return-void
.end method

.method private setWindowParams()V
    .locals 2

    .prologue
    .line 760
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x600000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 761
    return-void
.end method

.method private unRegisterBlockingSoundChangeReceiver()V
    .locals 2

    .prologue
    .line 613
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mBlockingSoundChangeReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 614
    const-string/jumbo v0, "BaseActivity"

    const-string/jumbo v1, "Unregister - SoundSettingReceiver"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 615
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mBlockingSoundChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 616
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mBlockingSoundChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 618
    :cond_0
    return-void
.end method

.method private unRegisterBroadCastReceiver()V
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mVoiceCommandReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 600
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mCarModeOffReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 601
    return-void
.end method

.method private updateOngoingNotiLocale()V
    .locals 10

    .prologue
    const v9, 0x7f0a0265

    const/4 v7, 0x0

    const/16 v8, 0x1e3e

    .line 884
    sget-object v5, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mNotify:Landroid/app/NotificationManager;

    if-eqz v5, :cond_0

    .line 885
    sget-object v5, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mNotify:Landroid/app/NotificationManager;

    invoke-virtual {v5, v8}, Landroid/app/NotificationManager;->cancel(I)V

    .line 886
    const-string/jumbo v5, "BaseActivity"

    const-string/jumbo v6, "cancel Notify register for locale change"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 894
    :goto_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 895
    const-class v6, Lcom/sec/android/automotive/drivelink/firstaccess/OngoingNotiActivity;

    .line 894
    invoke-direct {v0, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 896
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v5, 0x30000000

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 899
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 900
    const-string/jumbo v6, "notification"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 899
    check-cast v1, Landroid/app/NotificationManager;

    .line 903
    .local v1, "mNotiManager":Landroid/app/NotificationManager;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 902
    invoke-static {v5, v7, v0, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 905
    .local v4, "pintent":Landroid/app/PendingIntent;
    new-instance v5, Landroid/app/Notification$Builder;

    .line 906
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    .line 905
    invoke-direct {v5, v6}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 908
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    .line 909
    const v7, 0x7f0a0264

    .line 908
    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 907
    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    .line 911
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 910
    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    .line 913
    const v6, 0x7f0201e8

    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v5

    .line 915
    new-instance v6, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v6}, Landroid/app/Notification$BigTextStyle;-><init>()V

    .line 916
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v6

    .line 914
    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    move-result-object v5

    .line 918
    invoke-virtual {v5, v4}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    .line 920
    .local v2, "mNotification":Landroid/app/Notification;
    invoke-virtual {v1, v8, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 922
    return-void

    .line 888
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "mNotiManager":Landroid/app/NotificationManager;
    .end local v2    # "mNotification":Landroid/app/Notification;
    .end local v4    # "pintent":Landroid/app/PendingIntent;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 889
    const-string/jumbo v6, "notification"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 888
    check-cast v3, Landroid/app/NotificationManager;

    .line 890
    .local v3, "mNotificationManager":Landroid/app/NotificationManager;
    invoke-virtual {v3, v8}, Landroid/app/NotificationManager;->cancel(I)V

    .line 891
    const-string/jumbo v5, "BaseActivity"

    const-string/jumbo v6, "force cancel Notify register for locale change"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method


# virtual methods
.method protected CarAppFinishFunc()V
    .locals 2

    .prologue
    .line 621
    const-string/jumbo v0, "BaseActivity"

    const-string/jumbo v1, "CarAppFinishFunc()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->finish()V

    .line 623
    return-void
.end method

.method protected autoMultiModeSize()Z
    .locals 1

    .prologue
    .line 764
    const/4 v0, 0x1

    return v0
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 702
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mFlowID:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->removeFlowList(Ljava/lang/String;)V

    .line 704
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mPrevFlowID:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mPrevFlowID:Ljava/lang/String;

    const-string/jumbo v1, "DM_MAIN"

    if-eq v0, v1, :cond_0

    .line 705
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 708
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 709
    return-void
.end method

.method public getActivityFlowID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 790
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mFlowID:Ljava/lang/String;

    return-object v0
.end method

.method protected getDLLastConfigInstance()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 323
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mBackupMap:Ljava/util/HashMap;

    return-object v0
.end method

.method protected getKeyguardManager()Landroid/app/KeyguardManager;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->keyguardManager:Landroid/app/KeyguardManager;

    if-nez v0, :cond_0

    .line 97
    const-string/jumbo v0, "keyguard"

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->keyguardManager:Landroid/app/KeyguardManager;

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->keyguardManager:Landroid/app/KeyguardManager;

    return-object v0
.end method

.method protected isMultiWindow()Z
    .locals 4

    .prologue
    .line 626
    const/4 v0, 0x0

    .line 628
    .local v0, "isMultiWindow":Z
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v1, :cond_0

    .line 629
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v0

    .line 631
    :cond_0
    const-string/jumbo v1, "BaseActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "isMultiWindowMode:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    return v0
.end method

.method protected moveoutOverlayService()V
    .locals 2

    .prologue
    .line 638
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->isMultiWindow()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 639
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->getState()Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    move-result-object v0

    .line 640
    .local v0, "state":Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->MovedOut:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    if-eq v0, v1, :cond_0

    .line 641
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->moveOutOfHomeOverlayService(Landroid/content/Context;)V

    .line 644
    .end local v0    # "state":Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;
    :cond_0
    return-void
.end method

.method public onBluetoothServiceConnected()V
    .locals 2

    .prologue
    .line 824
    const-string/jumbo v0, "BaseActivity"

    const-string/jumbo v1, "[LatencyCheck] onScoDisconnected()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 825
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 441
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 442
    const-string/jumbo v0, "BaseActivity"

    const-string/jumbo v1, "onConfigurationChanged()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->updateLocale(Z)V

    .line 446
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 454
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 126
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 127
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->setMultiWindowTrayHide()V

    .line 129
    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->updateLocale(Z)V

    .line 131
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 132
    .local v0, "config":Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mOrientation:I

    .line 134
    const-string/jumbo v1, "BaseActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onCreate :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 135
    const-string/jumbo v3, ", orientation :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mOrientation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 134
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    new-instance v1, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getLastCustomNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mBackupMap:Ljava/util/HashMap;

    .line 141
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mBackupMap:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    .line 142
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mBackupMap:Ljava/util/HashMap;

    const-string/jumbo v2, "rotation"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mIsRotation:Z

    .line 145
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->setBaseBroadCastReceiver()V

    .line 146
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->setCarModeChangeToOffReceiver()V

    .line 147
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->setBlockingSoundChangeReceiver()V

    .line 149
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->setWindowParams()V

    .line 150
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getdensity()V

    .line 153
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 155
    const-string/jumbo v1, "BaseActivity"

    const-string/jumbo v2, "SETTING_DRIVLINK_MULTI is set to 0"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    invoke-static {p0, v4}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->setMultiWindowState(Landroid/content/Context;I)V

    .line 159
    :cond_1
    return-void
.end method

.method protected onDLRetainConfigInstance(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 320
    .local p1, "backupMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 335
    const-string/jumbo v0, "BaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onDestroy : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->unRegisterBroadCastReceiver()V

    .line 339
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->clearCurrentActivity()V

    .line 345
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 346
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->airplaneModeDoneCount:I

    .line 347
    return-void
.end method

.method public onFlowCommandCall(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 748
    return-void
.end method

.method public onFlowCommandCancel(Ljava/lang/String;)V
    .locals 3
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 660
    const-string/jumbo v0, "BaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onFlowCommandCancel:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mFlowID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 662
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->finish()V

    .line 664
    :cond_0
    return-void
.end method

.method public onFlowCommandCustom(Ljava/lang/String;)Z
    .locals 1
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 648
    const/4 v0, 0x0

    return v0
.end method

.method public onFlowCommandExcute(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 727
    return-void
.end method

.method public onFlowCommandIgnore(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 730
    return-void
.end method

.method public onFlowCommandLookup(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 733
    return-void
.end method

.method public onFlowCommandNext(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 754
    return-void
.end method

.method public onFlowCommandNo(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 721
    return-void
.end method

.method public onFlowCommandRead(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 751
    return-void
.end method

.method public onFlowCommandReply(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 745
    return-void
.end method

.method public onFlowCommandReset(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 736
    return-void
.end method

.method public onFlowCommandReturnMain()V
    .locals 2

    .prologue
    .line 653
    const-string/jumbo v0, "BaseActivity"

    .line 654
    const-string/jumbo v1, "onFlowCommandReturnMain received. This activity will be finished."

    .line 653
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->finish()V

    .line 656
    return-void
.end method

.method public onFlowCommandRoute(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 739
    return-void
.end method

.method public onFlowCommandRouteNop(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 742
    return-void
.end method

.method public onFlowCommandSend(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 724
    return-void
.end method

.method public onFlowCommandStopNavigation(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 757
    return-void
.end method

.method public onFlowCommandYes(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 718
    return-void
.end method

.method protected onFlowListSelected(Ljava/lang/String;IZ)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;
    .param p2, "index"    # I
    .param p3, "isOrdinal"    # Z

    .prologue
    .line 671
    return-void
.end method

.method protected onFlowListSelectedFailed()V
    .locals 0

    .prologue
    .line 667
    return-void
.end method

.method protected onFlowListSelectedForMessage(Ljava/lang/String;IZLjava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;
    .param p2, "index"    # I
    .param p3, "isOrdinal"    # Z
    .param p4, "message"    # Ljava/lang/String;

    .prologue
    .line 675
    return-void
.end method

.method public onHeadsetConnected()V
    .locals 0

    .prologue
    .line 979
    return-void
.end method

.method public onHeadsetDisconnected()V
    .locals 0

    .prologue
    .line 987
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 297
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 299
    const-string/jumbo v0, "BaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onNewIntent : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    return-void
.end method

.method protected onPause()V
    .locals 4

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 164
    .local v0, "config":Landroid/content/res/Configuration;
    const-string/jumbo v1, "BaseActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onPause :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 165
    const-string/jumbo v3, " - orientation :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", mOrientation:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 166
    iget v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mOrientation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", mIsRotation:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mIsRotation:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 164
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    iget v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mOrientation:I

    iget v2, v0, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_0

    .line 169
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mIsRotation:Z

    .line 172
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mIsRotation:Z

    if-nez v1, :cond_1

    .line 173
    const-string/jumbo v1, "DLPhraseSpotter"

    const-string/jumbo v2, "[stop] : BaseActivity - onPause()"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 182
    :cond_1
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    .line 183
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->getMicState()Lcom/nuance/sample/MicState;

    move-result-object v2

    .line 182
    invoke-virtual {v1, v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->setLastMicState(Lcom/nuance/sample/MicState;)V

    .line 185
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->removeListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 192
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mIsAFRequired:Z

    .line 197
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;

    move-result-object v1

    .line 198
    const/4 v2, 0x0

    .line 197
    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->setListener(Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;)V

    .line 203
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 204
    return-void
.end method

.method public onPrepareFlowChange(Ljava/lang/String;)V
    .locals 3
    .param p1, "nextFlowID"    # Ljava/lang/String;

    .prologue
    .line 712
    const-string/jumbo v0, "BaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[onPrepareFlowChange - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 713
    const-string/jumbo v2, "] NextFlowID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 712
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mIsFlowChanged:Z

    .line 715
    return-void
.end method

.method protected onRestart()V
    .locals 3

    .prologue
    .line 432
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onRestart()V

    .line 433
    const-string/jumbo v0, "BaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onRestart() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->updateLocale(Z)V

    .line 436
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->setBlockingSoundChangeReceiver()V

    .line 437
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 210
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 215
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;

    move-result-object v0

    .line 216
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;)V

    .line 215
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->setListener(Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;)V

    .line 232
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setCurrentActiviy(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;Landroid/content/Context;)V

    .line 233
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/automotive/drivelink/DLApplication;->setCurrentActivity(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;)V

    .line 235
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getKeyguardManager()Landroid/app/KeyguardManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 236
    const-string/jumbo v0, "BaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onResume Canceled by keyguard : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 237
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 236
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    :cond_0
    :goto_0
    return-void

    .line 241
    :cond_1
    const-string/jumbo v0, "BaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onResume : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->requstAudioFocus()V

    .line 246
    invoke-static {p0, v3}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->setState(Landroid/content/Context;I)V

    .line 248
    const-string/jumbo v0, "BaseActivity"

    const-string/jumbo v1, "setState SETTING_DRIVLINK is set to 1"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "drive_link_setting"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 251
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->addListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V

    .line 253
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->autoMultiModeSize()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v0, :cond_2

    .line 254
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 255
    const-string/jumbo v0, "BaseActivity"

    .line 256
    const-string/jumbo v1, "This activity is in multiwindow mode. change size to full screen."

    .line 255
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-static {p0, v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->setMultiModeFullSize(Landroid/content/Context;Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;)Z

    .line 260
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 263
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mIsFlowChanged:Z

    .line 264
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 266
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->isAirplaneModeEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 267
    sget v0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->airplaneModeDoneCount:I

    if-nez v0, :cond_3

    .line 268
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;)V

    .line 275
    const-wide/16 v2, 0x12c

    .line 268
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 279
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    const-string/jumbo v0, "BaseActivity"

    const-string/jumbo v1, "SETTING_DRIVLINK_MULTI is set to 0"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    invoke-static {p0, v4}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->setMultiWindowState(Landroid/content/Context;I)V

    goto/16 :goto_0
.end method

.method public onRetainCustomNonConfigurationInstance()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 304
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 305
    .local v0, "backupMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string/jumbo v1, "BaseActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onRetainCustomNonConfigurationInstance :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 306
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", rotation is true"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 305
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mIsFlowChanged:Z

    if-nez v1, :cond_0

    .line 309
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mIsRotation:Z

    .line 312
    :cond_0
    const-string/jumbo v1, "rotation"

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mIsRotation:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onDLRetainConfigInstance(Ljava/util/HashMap;)V

    .line 316
    return-object v0
.end method

.method public onScoConnected()V
    .locals 2

    .prologue
    .line 818
    const-string/jumbo v0, "BaseActivity"

    const-string/jumbo v1, "[LatencyCheck] onScoConnected()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 819
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->setVolumeControlStream(I)V

    .line 820
    return-void
.end method

.method public onScoDisconnected()V
    .locals 3

    .prologue
    .line 795
    const-string/jumbo v1, "BaseActivity"

    const-string/jumbo v2, "onScoDisconnected"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->setVolumeControlStream(I)V

    .line 799
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v1

    .line 800
    invoke-interface {v1}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 802
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->isIdle()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 803
    const-string/jumbo v1, "BaseActivity"

    const-string/jumbo v2, "onScoDisconnected VAC : abandon audio focus"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 804
    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->audioFocusManager(Landroid/content/Context;)Lcom/vlingo/core/facade/audio/IAudioFocusManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/audio/IAudioFocusManager;->abandonAudioFocus()V

    .line 807
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->disconnectScoByIdle()Z

    move-result v1

    if-nez v1, :cond_1

    .line 809
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 810
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->interruptTurn()V

    .line 811
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelAudio()V

    .line 812
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 814
    :cond_1
    return-void
.end method

.method protected onStop()V
    .locals 3

    .prologue
    .line 351
    const-string/jumbo v0, "BaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onStop : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->isAppForeground()Z

    move-result v0

    if-nez v0, :cond_0

    .line 354
    const-string/jumbo v0, "BaseActivity"

    const-string/jumbo v1, "setState SETTING_DRIVLINK is set to 0"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "drive_link_setting"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 357
    const/4 v0, 0x2

    .line 356
    invoke-static {p0, v0}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->setState(Landroid/content/Context;I)V

    .line 360
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->unRegisterBlockingSoundChangeReceiver()V

    .line 362
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 363
    return-void
.end method

.method protected reloadLayout()Z
    .locals 1

    .prologue
    .line 457
    const/4 v0, 0x1

    return v0
.end method

.method protected requstAudioFocus()V
    .locals 5

    .prologue
    .line 925
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    .line 927
    .local v1, "context":Landroid/content/Context;
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->isAppInForeground()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 962
    :cond_0
    :goto_0
    return-void

    .line 931
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mIsAFRequired:Z

    if-eqz v2, :cond_2

    .line 932
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mIsAFRequired:Z

    goto :goto_0

    .line 935
    :cond_2
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mIsAFRequired:Z

    .line 939
    const-string/jumbo v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 938
    check-cast v0, Landroid/media/AudioManager;

    .line 941
    .local v0, "audioMgr":Landroid/media/AudioManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v2

    if-nez v2, :cond_0

    .line 942
    const-string/jumbo v2, "BaseActivity"

    const-string/jumbo v3, "request mDummyAudioFocus : "

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 944
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mDummyAudioFocus:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 945
    const/16 v3, 0x9

    .line 946
    const/4 v4, 0x2

    .line 944
    invoke-virtual {v0, v2, v3, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 959
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mDummyAudioFocus:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    goto :goto_0
.end method

.method protected restorePrevFlow()V
    .locals 4

    .prologue
    .line 686
    const-string/jumbo v1, "BaseActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "restorePrevFlow:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 687
    const-string/jumbo v3, "flowID :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mFlowID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", prevFlowID :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mPrevFlowID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 686
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mPrevFlowID:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mPrevFlowID:Ljava/lang/String;

    const-string/jumbo v2, "DM_MAIN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 690
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mFlowID:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->removeFlowList(Ljava/lang/String;)V

    .line 693
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mPrevFlowID:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v0

    .line 694
    .local v0, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mPrevFlowID:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 697
    .end local v0    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->finish()V

    .line 698
    return-void
.end method

.method protected setActivityFlowID(Ljava/lang/String;)V
    .locals 3
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 678
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mFlowID:Ljava/lang/String;

    .line 679
    const-string/jumbo v0, "BaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setActivityFlowID:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mFlowID:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getPrevFlowNoDuplicated(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mPrevFlowID:Ljava/lang/String;

    .line 682
    const-string/jumbo v0, "BaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setPrevActivityFlowID:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mPrevFlowID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    return-void
.end method

.method public setContentView(I)V
    .locals 5
    .param p1, "layoutResID"    # I

    .prologue
    const/4 v4, -0x1

    .line 106
    const v2, 0x7f030001

    invoke-super {p0, v2}, Landroid/support/v4/app/FragmentActivity;->setContentView(I)V

    .line 111
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->setVolumeControlStream(I)V

    .line 113
    const v2, 0x7f090007

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mLayoutContents:Landroid/widget/RelativeLayout;

    .line 114
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 115
    const/4 v3, 0x0

    .line 114
    invoke-virtual {v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 117
    .local v1, "layout":Landroid/view/View;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 120
    .local v0, "contentParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mLayoutContents:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 122
    return-void
.end method

.method protected setDayMode()V
    .locals 0

    .prologue
    .line 775
    return-void
.end method

.method protected setNightMode()V
    .locals 0

    .prologue
    .line 772
    return-void
.end method

.method public updateLocale(Z)V
    .locals 8
    .param p1, "isRecreate"    # Z

    .prologue
    .line 828
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 829
    .local v2, "config":Landroid/content/res/Configuration;
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v5

    invoke-virtual {v5}, Lcom/nuance/drivelink/DLAppUiUpdater;->getVoiceLocale()Ljava/util/Locale;

    move-result-object v1

    .line 830
    .local v1, "Locale":Ljava/util/Locale;
    const/4 v4, 0x0

    .line 833
    .local v4, "voiceLocale":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 834
    const-string/jumbo v5, "BaseActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 835
    const-string/jumbo v7, " : updateLocale - Canceled, voice sitting disabled"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 834
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 881
    :cond_0
    :goto_0
    return-void

    .line 839
    :cond_1
    if-nez v2, :cond_2

    .line 840
    const-string/jumbo v5, "BaseActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 841
    const-string/jumbo v7, " : updateLocale - Canceled"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 840
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 845
    :cond_2
    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "v_ES"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 846
    new-instance v1, Ljava/util/Locale;

    .end local v1    # "Locale":Ljava/util/Locale;
    const-string/jumbo v5, "es"

    const-string/jumbo v6, "us"

    invoke-direct {v1, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 848
    .restart local v1    # "Locale":Ljava/util/Locale;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 849
    const-string/jumbo v6, "carmode_language"

    .line 850
    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v7

    .line 848
    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 852
    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    .line 853
    iget-object v5, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 855
    .local v0, "ConfigLocale":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mActivityLocale:Ljava/lang/String;

    if-nez v5, :cond_4

    .line 856
    const-string/jumbo v5, "BaseActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 857
    const-string/jumbo v7, " : updateLocale - initActivityCurLocale : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 858
    iget-object v7, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 856
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    iget-object v5, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mActivityLocale:Ljava/lang/String;

    .line 862
    :cond_4
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mActivityLocale:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 863
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 864
    :cond_5
    const-string/jumbo v5, "BaseActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 865
    const-string/jumbo v7, " : updateLocale - curLocale : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mActivityLocale:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 866
    const-string/jumbo v7, " -> voiceLocale :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 864
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 867
    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mActivityLocale:Ljava/lang/String;

    .line 868
    invoke-static {v1}, Lcom/nuance/sample/settings/SampleAppSettings;->updateVoiceLocale(Ljava/util/Locale;)Ljava/util/Locale;

    .line 870
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->reloadLayout()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "SettingsLanguageActivity"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 871
    const-string/jumbo v5, "BaseActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 872
    const-string/jumbo v7, " : updateLocale - reCreateActivity"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 871
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 874
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 875
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->finish()V

    .line 876
    invoke-static {p0, v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 878
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->updateOngoingNotiLocale()V

    goto/16 :goto_0
.end method
