.class Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$3;
.super Ljava/lang/Object;
.source "NotificationActivity.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    .line 517
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 528
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "Appear Animation End..."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->access$5(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;Landroid/view/animation/Animation;)V

    .line 530
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 536
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 541
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->APPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    # invokes: Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->access$6(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 544
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "Appear Animation Start..."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    return-void
.end method
