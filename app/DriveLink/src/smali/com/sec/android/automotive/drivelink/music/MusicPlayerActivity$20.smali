.class Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$20;
.super Ljava/lang/Object;
.source "MusicPlayerActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$OnMusicSipStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->getMusicSipStateListener()Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$OnMusicSipStateListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$music$EditTextForMusicSIP$MusicSIPState:[I


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$music$EditTextForMusicSIP$MusicSIPState()[I
    .locals 3

    .prologue
    .line 2157
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$20;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$music$EditTextForMusicSIP$MusicSIPState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;->values()[Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;->SIP_CLOSE:Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;->SIP_SHOW:Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$20;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$music$EditTextForMusicSIP$MusicSIPState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$20;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    .line 2157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnStateChanged(Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;)V
    .locals 2
    .param p1, "state"    # Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;

    .prologue
    .line 2162
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$20;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$music$EditTextForMusicSIP$MusicSIPState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2176
    :goto_0
    :pswitch_0
    return-void

    .line 2168
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$20;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mTextfieldLayout:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$30(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/view/ViewGroup;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2169
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$20;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$31(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->showVoiceLayout()V

    .line 2170
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$20;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$32(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 2171
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$20;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    goto :goto_0

    .line 2162
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
