.class public final enum Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;
.super Ljava/lang/Enum;
.source "HomeOverlayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OverlayServiceState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

.field public static final enum MovedOut:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

.field public static final enum Paused:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

.field public static final enum Resumed:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

.field public static final enum Started:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

.field public static final enum Stoped:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    const-string/jumbo v1, "Started"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->Started:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    .line 40
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    const-string/jumbo v1, "Resumed"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->Resumed:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    .line 41
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    const-string/jumbo v1, "Paused"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->Paused:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    .line 42
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    const-string/jumbo v1, "MovedOut"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->MovedOut:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    .line 43
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    const-string/jumbo v1, "Stoped"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->Stoped:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    .line 38
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->Started:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->Resumed:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->Paused:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->MovedOut:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->Stoped:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
