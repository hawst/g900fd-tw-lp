.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageURL;
.super Ljava/lang/Object;
.source "MessageURL.java"


# static fields
.field static final MMS:Ljava/lang/String; = "content://mms"

.field static final MMS_PART:Ljava/lang/String; = "content://mms/part"

.field static final MMS_SMS:Ljava/lang/String; = "content://mms-sms"

.field static final MMS_SMS_ADDRESS:Ljava/lang/String; = "content://mms-sms/canonical-address/"

.field static final MMS_SMS_ALL_CONVERSATION:Ljava/lang/String; = "content://mms-sms/conversations?simple=true"

.field static final MMS_SMS_CONVERSATION:Ljava/lang/String; = "content://mms-sms/conversations"

.field static final MMS_SMS_INBOX:Ljava/lang/String; = "content://mms-sms/allinmessage"

.field static final SMS:Ljava/lang/String; = "content://sms"

.field static final SMS_INBOX:Ljava/lang/String; = "content://sms/inbox"

.field static final SMS_OUTBOX:Ljava/lang/String; = "content://sms/sent"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
