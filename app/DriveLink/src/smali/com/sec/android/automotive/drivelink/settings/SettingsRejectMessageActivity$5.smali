.class Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;
.super Ljava/lang/Object;
.source "SettingsRejectMessageActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4
    .param p1, "arg0"    # Landroid/text/Editable;

    .prologue
    .line 158
    :try_start_0
    const-string/jumbo v0, "[SettingsRejectMessageActivity]"

    .line 159
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Length: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 160
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 161
    const-string/jumbo v3, "KSC5601"

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    array-length v2, v2

    .line 160
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 159
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 158
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "KSC5601"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_1

    .line 163
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "KSC5601"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v0, v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->CharacterLimit:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;)I

    move-result v1

    if-gt v0, v1, :cond_1

    .line 164
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->switchRejectMessage:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnRejectMessageCancel:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnRejectMessageCancel:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnSettingsOk:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 178
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnSettingsOk:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 184
    :cond_0
    :goto_1
    return-void

    .line 168
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 169
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->switchRejectMessage:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 170
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnRejectMessageCancel:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnRejectMessageCancel:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 181
    :catch_0
    move-exception v0

    goto :goto_1

    .line 174
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnRejectMessageCancel:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnRejectMessageCancel:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnSettingsOk:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 153
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 148
    return-void
.end method
