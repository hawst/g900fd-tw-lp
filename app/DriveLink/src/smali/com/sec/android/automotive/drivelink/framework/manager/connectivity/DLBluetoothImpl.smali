.class public Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLBluetoothImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl$BT_PAIRING_TYPE;
    }
.end annotation


# instance fields
.field public mBluetoothAdapterState:I

.field public mBluetoothDeviceAddress:Ljava/lang/String;

.field public mBluetoothDeviceName:Ljava/lang/String;

.field public mBluetoothDeviceState:I

.field public mBluetoothInputKey:[B

.field public mBluetoothPairingType:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl$BT_PAIRING_TYPE;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 18
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    .line 19
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl;->mBluetoothDeviceName:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl;->mBluetoothInputKey:[B

    .line 22
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl;->mBluetoothDeviceAddress:Ljava/lang/String;

    .line 23
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl;->mBluetoothAdapterState:I

    .line 24
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl;->mBluetoothDeviceState:I

    .line 25
    return-void
.end method


# virtual methods
.method public getBluetoothAdapterState()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl;->mBluetoothAdapterState:I

    return v0
.end method

.method public getBluetoothDeviceAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl;->mBluetoothDeviceAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getBluetoothDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl;->mBluetoothDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getBluetoothDeviceState()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl;->mBluetoothDeviceState:I

    return v0
.end method

.method public getBluetoothInputKey()[B
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl;->mBluetoothInputKey:[B

    return-object v0
.end method

.method public getBluetoothPairingType()Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl$BT_PAIRING_TYPE;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl;->mBluetoothPairingType:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl$BT_PAIRING_TYPE;

    return-object v0
.end method

.method public setBluetoothAdapterState(I)V
    .locals 0
    .param p1, "bluetoothAdapterState"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl;->mBluetoothAdapterState:I

    .line 65
    return-void
.end method

.method public setBluetoothDeviceAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "bluetoothDeviceAddress"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl;->mBluetoothDeviceAddress:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public setBluetoothDeviceName(Ljava/lang/String;)V
    .locals 0
    .param p1, "bluetoothDeviceName"    # Ljava/lang/String;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl;->mBluetoothDeviceName:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public setBluetoothDeviceState(I)V
    .locals 0
    .param p1, "bluetoothDeviceState"    # I

    .prologue
    .line 72
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl;->mBluetoothDeviceState:I

    .line 73
    return-void
.end method

.method public setBluetoothInputKey([B)V
    .locals 0
    .param p1, "bluetoothInputKey"    # [B

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl;->mBluetoothInputKey:[B

    .line 49
    return-void
.end method

.method public setBluetoothPairingType(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl$BT_PAIRING_TYPE;)V
    .locals 0
    .param p1, "bluetoothPairingType"    # Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl$BT_PAIRING_TYPE;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl;->mBluetoothPairingType:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBluetoothImpl$BT_PAIRING_TYPE;

    .line 41
    return-void
.end method
