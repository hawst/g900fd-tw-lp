.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestAcceptLocationShared.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPhoneNumber:Ljava/lang/String;

.field private mResult:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

.field private mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "phoneNumber"    # Ljava/lang/String;
    .param p4, "url"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 10
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;->mContext:Landroid/content/Context;

    .line 11
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;->mPhoneNumber:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;->mUrl:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;->mResult:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 19
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;->mContext:Landroid/content/Context;

    .line 20
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;->mUrl:Ljava/lang/String;

    .line 21
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;->mPhoneNumber:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;->mContext:Landroid/content/Context;

    .line 43
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;->mUrl:Ljava/lang/String;

    .line 44
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;->mPhoneNumber:Ljava/lang/String;

    .line 45
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;->mResult:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 46
    return-void
.end method

.method protected process()V
    .locals 4

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v0

    .line 27
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;->mPhoneNumber:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->acceptSharedLocation(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    move-result-object v0

    .line 26
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;->mResult:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 28
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 33
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    move-result-object v0

    .line 34
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    if-eqz v0, :cond_0

    .line 36
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;->mResult:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;->onResponseRequestAcceptLocationShared(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V

    .line 38
    :cond_0
    return-void
.end method
