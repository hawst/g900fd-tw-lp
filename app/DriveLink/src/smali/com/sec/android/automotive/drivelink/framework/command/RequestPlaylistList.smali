.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestPlaylistList;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestPlaylistList.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mplaylistList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestPlaylistList;->mContext:Landroid/content/Context;

    .line 14
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestPlaylistList;->mplaylistList:Ljava/util/ArrayList;

    .line 19
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestPlaylistList;->mContext:Landroid/content/Context;

    .line 20
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestPlaylistList;->mContext:Landroid/content/Context;

    .line 41
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestPlaylistList;->mplaylistList:Ljava/util/ArrayList;

    .line 42
    return-void
.end method

.method protected process()V
    .locals 2

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestPlaylistList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 25
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 26
    .local v0, "contentManager":Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestPlaylistList;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getPlaylistList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestPlaylistList;->mplaylistList:Ljava/util/ArrayList;

    .line 27
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestPlaylistList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 32
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

    move-result-object v0

    .line 33
    .local v0, "musicListener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;
    if-eqz v0, :cond_0

    .line 34
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestPlaylistList;->mplaylistList:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;->onResponseRequestPlaylistList(Ljava/util/ArrayList;)V

    .line 36
    :cond_0
    return-void
.end method
