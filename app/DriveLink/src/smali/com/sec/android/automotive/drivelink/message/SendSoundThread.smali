.class public Lcom/sec/android/automotive/drivelink/message/SendSoundThread;
.super Ljava/lang/Object;
.source "SendSoundThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;
    }
.end annotation


# instance fields
.field mContext:Landroid/content/Context;

.field private mPool:Landroid/media/SoundPool;

.field mSoundID:I

.field soundThread:Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->soundThread:Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;

    .line 12
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->mPool:Landroid/media/SoundPool;

    .line 13
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->mSoundID:I

    .line 14
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->mContext:Landroid/content/Context;

    .line 21
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->mContext:Landroid/content/Context;

    .line 22
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/message/SendSoundThread;)Landroid/media/SoundPool;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->mPool:Landroid/media/SoundPool;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/message/SendSoundThread;Landroid/media/SoundPool;)V
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->mPool:Landroid/media/SoundPool;

    return-void
.end method


# virtual methods
.method public playSendSuccessSound()V
    .locals 2

    .prologue
    .line 28
    const v0, 0x7f060011

    const/16 v1, 0x14

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->playSound(II)V

    .line 30
    return-void
.end method

.method playSound(II)V
    .locals 4
    .param p1, "resourceId"    # I
    .param p2, "hundredmillis"    # I

    .prologue
    const/4 v3, 0x1

    .line 33
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->mPool:Landroid/media/SoundPool;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Landroid/media/SoundPool;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-direct {v0, v3, v1, v2}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->mPool:Landroid/media/SoundPool;

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->mPool:Landroid/media/SoundPool;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p1, v3}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->mSoundID:I

    .line 37
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;-><init>(Lcom/sec/android/automotive/drivelink/message/SendSoundThread;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->soundThread:Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;

    .line 38
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->soundThread:Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;

    iput p2, v0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->hundredmillis:I

    .line 39
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->mPool:Landroid/media/SoundPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$1;-><init>(Lcom/sec/android/automotive/drivelink/message/SendSoundThread;)V

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V

    .line 55
    return-void
.end method
