.class public Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;
.super Ljava/lang/Object;
.source "NotificationItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NotiFlowCommand"
.end annotation


# static fields
.field public static final CMD_ALERT:I = 0x1

.field public static final CMD_BARGEIN_LISTEN:I = 0x6

.field public static final CMD_BEGIN:I = 0x0

.field public static final CMD_END:I = 0xa

.field public static final CMD_FINISH:I = 0x9

.field public static final CMD_LISTEN:I = 0x7

.field public static final CMD_SOUND:I = 0x2

.field public static final CMD_TIMEOUT:I = 0x8

.field public static final CMD_TTS:I = 0x3

.field public static final CMD_TTS_WITH_FLOWID:I = 0x5

.field public static final CMD_TTS_WITH_LISTEN:I = 0x4


# instance fields
.field cmdType:I

.field cmdValue:Ljava/lang/Object;


# direct methods
.method constructor <init>(ILjava/lang/Object;)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233
    iput p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;->cmdType:I

    .line 234
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;->cmdValue:Ljava/lang/Object;

    .line 235
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;)V
    .locals 1
    .param p1, "backup"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;

    .prologue
    .line 237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 238
    if-eqz p1, :cond_0

    .line 239
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;->getCmdType()I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;->cmdType:I

    .line 240
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;->getCmdValue()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;->cmdValue:Ljava/lang/Object;

    .line 242
    :cond_0
    return-void
.end method


# virtual methods
.method public getCmdType()I
    .locals 1

    .prologue
    .line 248
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;->cmdType:I

    return v0
.end method

.method public getCmdValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;->cmdValue:Ljava/lang/Object;

    return-object v0
.end method

.method public setCmdType(I)V
    .locals 0
    .param p1, "cmdType"    # I

    .prologue
    .line 256
    iput p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;->cmdType:I

    .line 257
    return-void
.end method

.method public setCmdValue(Ljava/lang/Object;)V
    .locals 0
    .param p1, "cmdValue"    # Ljava/lang/Object;

    .prologue
    .line 271
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowCommand;->cmdValue:Ljava/lang/Object;

    .line 272
    return-void
.end method
