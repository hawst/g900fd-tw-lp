.class public final enum Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;
.super Ljava/lang/Enum;
.source "MessageContactTypePageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "PhoneNumberDisplayTypeForMsg"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

.field public static final enum FAX:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

.field public static final enum HOME:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

.field public static final enum MOBILE:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

.field public static final enum OFFICE:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

.field public static final enum PHONE:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;


# instance fields
.field private callDrawable:I

.field private phoneDrawableLandscape:I

.field private phoneDrawableNight:I

.field private phoneDrawablePortrait:I

.field private stringResource:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 214
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    const-string/jumbo v1, "HOME"

    const/4 v2, 0x0

    const v3, 0x7f0a035c

    .line 215
    const v4, 0x7f020227

    .line 216
    const v5, 0x7f020227

    .line 217
    const v6, 0x7f02022a

    .line 218
    const v7, 0x7f02012e

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;-><init>(Ljava/lang/String;IIIIII)V

    .line 214
    sput-object v0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->HOME:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    .line 218
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    const-string/jumbo v1, "MOBILE"

    const/4 v2, 0x1

    .line 219
    const v3, 0x7f0a035d

    .line 220
    const v4, 0x7f02022b

    .line 221
    const v5, 0x7f02022b

    .line 222
    const v6, 0x7f02022c

    .line 223
    const v7, 0x7f020130

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;-><init>(Ljava/lang/String;IIIIII)V

    .line 218
    sput-object v0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->MOBILE:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    .line 223
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    const-string/jumbo v1, "OFFICE"

    const/4 v2, 0x2

    .line 224
    const v3, 0x7f0a035e

    .line 225
    const v4, 0x7f02022d

    .line 226
    const v5, 0x7f02022d

    .line 227
    const v6, 0x7f020230

    .line 228
    const v7, 0x7f02012f

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;-><init>(Ljava/lang/String;IIIIII)V

    .line 223
    sput-object v0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->OFFICE:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    .line 228
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    const-string/jumbo v1, "FAX"

    const/4 v2, 0x3

    .line 229
    const v3, 0x7f0a035f

    .line 230
    const v4, 0x7f020225

    .line 231
    const v5, 0x7f020225

    .line 232
    const v6, 0x7f020226

    .line 233
    const v7, 0x7f020130

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;-><init>(Ljava/lang/String;IIIIII)V

    .line 228
    sput-object v0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->FAX:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    .line 233
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    const-string/jumbo v1, "PHONE"

    const/4 v2, 0x4

    .line 234
    const v3, 0x7f0a0360

    .line 235
    const v4, 0x7f020231

    .line 236
    const v5, 0x7f020231

    .line 237
    const v6, 0x7f020232

    .line 238
    const v7, 0x7f020130

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;-><init>(Ljava/lang/String;IIIIII)V

    .line 233
    sput-object v0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->PHONE:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    .line 213
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->HOME:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->MOBILE:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->OFFICE:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->FAX:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->PHONE:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIII)V
    .locals 0
    .param p3, "stringResource"    # I
    .param p4, "phoneDrawableLandscape"    # I
    .param p5, "phoneDrawablePortrait"    # I
    .param p6, "phoneDrawableNight"    # I
    .param p7, "callDrawable"    # I

    .prologue
    .line 252
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 255
    iput p3, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->stringResource:I

    .line 256
    iput p4, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->phoneDrawableLandscape:I

    .line 257
    iput p5, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->phoneDrawablePortrait:I

    .line 258
    iput p6, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->phoneDrawableNight:I

    .line 259
    iput p7, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->callDrawable:I

    .line 260
    return-void
.end method

.method public static getPhoneNumberDisplayType(I)Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;
    .locals 1
    .param p0, "phoneType"    # I

    .prologue
    .line 336
    packed-switch p0, :pswitch_data_0

    .line 353
    :pswitch_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->PHONE:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    :goto_0
    return-object v0

    .line 338
    :pswitch_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->HOME:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    goto :goto_0

    .line 340
    :pswitch_2
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->OFFICE:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    goto :goto_0

    .line 342
    :pswitch_3
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->MOBILE:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    goto :goto_0

    .line 346
    :pswitch_4
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->FAX:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    goto :goto_0

    .line 336
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCallDrawable()I
    .locals 1

    .prologue
    .line 321
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->callDrawable:I

    return v0
.end method

.method public getPhoneDrawableLandscape()I
    .locals 1

    .prologue
    .line 289
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->phoneDrawableLandscape:I

    return v0
.end method

.method public getPhoneDrawableNight()I
    .locals 1

    .prologue
    .line 308
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->phoneDrawableNight:I

    return v0
.end method

.method public getPhoneDrawablePortrait()I
    .locals 1

    .prologue
    .line 304
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->phoneDrawablePortrait:I

    return v0
.end method

.method public getStringResource()I
    .locals 1

    .prologue
    .line 274
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->stringResource:I

    return v0
.end method
