.class Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$2;
.super Ljava/lang/Object;
.source "BaseMapActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMarkerDragDropListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    .line 275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V
    .locals 2
    .param p1, "marker"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .prologue
    .line 290
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBalloonMarker:Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$1(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 291
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBalloonMarker:Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$1(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->removeMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V

    .line 292
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$2(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V

    .line 294
    :cond_0
    return-void
.end method

.method public onMarkerDragEnd(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V
    .locals 6
    .param p1, "marker"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLatitude()D

    move-result-wide v2

    .line 280
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;-><init>(DD)V

    .line 279
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$3(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    .line 282
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLatitude()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLongitude()D

    move-result-wide v3

    .line 283
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v5

    .line 281
    invoke-static {v1, v2, v3, v4, v5}, Lcom/sec/android/automotive/drivelink/location/map/util/GeocodingAPI;->getAddressFromLatLng(DDLandroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$4(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Ljava/lang/String;)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLatitude()D

    move-result-wide v1

    .line 285
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLongitude()D

    move-result-wide v3

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddress:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$7(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Ljava/lang/String;

    move-result-object v5

    .line 284
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->createMarks(DDLjava/lang/String;)V

    .line 286
    return-void
.end method
