.class Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$4;
.super Ljava/lang/Object;
.source "SettingsRejectMessageActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnRejectMessageCancel:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnRejectMessageCancel:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 141
    :cond_0
    return-void
.end method
