.class Lcom/sec/android/automotive/drivelink/help/HelpActivity$2;
.super Ljava/lang/Object;
.source "HelpActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/help/HelpActivity;->initHelpContentsPage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/help/HelpActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/help/HelpActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/help/HelpActivity;

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/help/HelpActivity;

    .line 76
    const-class v2, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;

    .line 75
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 77
    .local v0, "myIntent":Landroid/content/Intent;
    const-string/jumbo v1, "SCREEN_TO_LAUNCH"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 78
    const-string/jumbo v2, "Screen_Name"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/help/HelpActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/help/HelpActivity;->mListItems:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->access$1(Lcom/sec/android/automotive/drivelink/help/HelpActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/help/HelpActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/help/HelpActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/help/HelpActivity;->startActivity(Landroid/content/Intent;)V

    .line 82
    return-void
.end method
