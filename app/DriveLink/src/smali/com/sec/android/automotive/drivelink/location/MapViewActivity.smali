.class public Lcom/sec/android/automotive/drivelink/location/MapViewActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;
.source "MapViewActivity.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I


# instance fields
.field private btnCancel:Landroid/widget/Button;

.field private btnNavigate:Landroid/widget/Button;

.field private mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

.field private final mNotifiyNewMicState:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;

.field private mToast:Landroid/widget/Toast;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;-><init>()V

    .line 289
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/location/MapViewActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mNotifiyNewMicState:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;

    .line 34
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/location/MapViewActivity;Lcom/nuance/sample/MicState;)V
    .locals 0

    .prologue
    .line 276
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->handleMicStateChanged(Lcom/nuance/sample/MicState;)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/location/MapViewActivity;)Z
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->isProviderEnabled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/location/MapViewActivity;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 328
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->hasGoogleAccount(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/location/MapViewActivity;Z)V
    .locals 0

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->setCustomZoom(Z)V

    return-void
.end method

.method private handleMicStateChanged(Lcom/nuance/sample/MicState;)V
    .locals 5
    .param p1, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 277
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 287
    :goto_0
    return-void

    .line 279
    :pswitch_0
    new-array v0, v4, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->btnNavigate:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->btnCancel:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->setButtonQuoted(Z[Landroid/widget/Button;)V

    .line 280
    new-array v0, v4, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->btnNavigate:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->btnCancel:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->setButtonQuoted(Z[Landroid/widget/Button;)V

    goto :goto_0

    .line 284
    :pswitch_1
    new-array v0, v4, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->btnNavigate:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->btnCancel:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->setButtonQuoted(Z[Landroid/widget/Button;)V

    goto :goto_0

    .line 277
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private hasGoogleAccount(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 329
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 330
    .local v1, "accMan":Landroid/accounts/AccountManager;
    const-string/jumbo v3, "com.google"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 331
    .local v0, "accArray":[Landroid/accounts/Account;
    array-length v3, v0

    if-lt v3, v2, :cond_0

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected getLayoutId()I
    .locals 1

    .prologue
    .line 273
    const v0, 0x7f030018

    return v0
.end method

.method protected getLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 299
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onBackPressed()V

    .line 308
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v4, 0x7f0a0249

    .line 47
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    const v2, 0x7f0900aa

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 61
    const v2, 0x7f090098

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->btnCancel:Landroid/widget/Button;

    .line 62
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->btnCancel:Landroid/widget/Button;

    new-instance v3, Lcom/sec/android/automotive/drivelink/location/MapViewActivity$2;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/location/MapViewActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v2, v4}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setSayText(I)V

    .line 79
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v2, v4}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setSayListeningText(I)V

    .line 80
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setBlackStyle()V

    .line 81
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    new-instance v3, Lcom/sec/android/automotive/drivelink/location/MapViewActivity$3;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/location/MapViewActivity;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    const v2, 0x7f0900a9

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->btnNavigate:Landroid/widget/Button;

    .line 108
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->btnNavigate:Landroid/widget/Button;

    new-instance v3, Lcom/sec/android/automotive/drivelink/location/MapViewActivity$4;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/location/MapViewActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const v3, 0x7f0a0846

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setTitleBar(I)V

    .line 126
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "goto_navigation"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->startNavigation()V

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->finish()V

    .line 145
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v2, v3}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mNotifiyNewMicState:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setNotifiyNewMicState(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;)V

    .line 159
    return-void

    .line 146
    :catch_0
    move-exception v0

    .line 147
    .local v0, "e":Ljava/lang/Exception;
    const v2, 0x7f0a0296

    .line 148
    const/4 v3, 0x1

    .line 147
    invoke-static {p0, v2, v3}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;II)Landroid/widget/Toast;

    move-result-object v1

    .line 150
    .local v1, "toast":Landroid/widget/Toast;
    if-eqz v1, :cond_1

    .line 151
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 235
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 236
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->turnGPSOff()V

    .line 237
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onDestroy()V

    .line 238
    return-void
.end method

.method public onFlowCommandNo(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->finish()V

    .line 226
    return-void
.end method

.method public onFlowCommandRoute(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->isProviderEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v0

    .line 207
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->hasGoogleAccount(Landroid/content/Context;)Z

    move-result v0

    .line 208
    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->startNavigation()V

    .line 216
    :goto_0
    return-void

    .line 211
    :cond_0
    const v0, 0x7f0a01fb

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->showToast(I)V

    goto :goto_0

    .line 214
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->getEnableGPSPDialog()V

    goto :goto_0
.end method

.method public onFlowCommandYes(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->startNavigation()V

    .line 221
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 247
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onResume()V

    .line 249
    const v0, 0x7f090091

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 250
    const/16 v1, 0x8

    .line 249
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 251
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    instance-of v0, v0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;

    if-eqz v0, :cond_0

    .line 252
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;

    .line 253
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/MapViewActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/location/MapViewActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->setOnMarkerClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMarkerClickListener;)V

    .line 263
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->setCustomZoom()V

    .line 264
    return-void
.end method

.method protected setDayMode()V
    .locals 2

    .prologue
    .line 312
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->setDayMode()V

    .line 314
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setDayMode(Z)V

    .line 317
    :cond_0
    return-void
.end method

.method protected setNightMode()V
    .locals 2

    .prologue
    .line 321
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->setNightMode()V

    .line 323
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setNightMode(Z)V

    .line 326
    :cond_0
    return-void
.end method

.method public showToast(I)V
    .locals 1
    .param p1, "textID"    # I

    .prologue
    .line 335
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 336
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 338
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mToast:Landroid/widget/Toast;

    .line 339
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_1

    .line 340
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 341
    :cond_1
    return-void
.end method

.method protected startNavigation()V
    .locals 3

    .prologue
    .line 163
    const v1, 0x7f090091

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 164
    const/4 v2, 0x0

    .line 163
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 167
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->isProviderEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 172
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    .line 173
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 172
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->getLocationType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    move-result-object v1

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->MY_PLACE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    if-ne v1, v2, :cond_1

    .line 175
    const-string/jumbo v1, "CM19"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/MapViewActivity;->getLocationType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    move-result-object v1

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->SCHEDULE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    if-ne v1, v2, :cond_0

    .line 177
    const-string/jumbo v1, "CM21"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    goto :goto_0

    .line 196
    :cond_2
    const v1, 0x7f0a032d

    const/4 v2, 0x1

    .line 195
    invoke-static {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;II)Landroid/widget/Toast;

    move-result-object v0

    .line 198
    .local v0, "toast":Landroid/widget/Toast;
    if-eqz v0, :cond_0

    .line 199
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
