.class Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$10;
.super Ljava/lang/Object;
.source "LocationShareActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->searchContacts(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    .line 325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseRequestContactList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 344
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    return-void
.end method

.method public onResponseRequestSearchedContactList(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 330
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$7()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "search : searched! "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setResutlText(I)V

    .line 332
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$4(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;Ljava/util/ArrayList;)V

    .line 333
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->updateContacts()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$5(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isSearchMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 336
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 337
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->startConfirm(I)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$8(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;I)V

    .line 339
    :cond_0
    return-void
.end method
