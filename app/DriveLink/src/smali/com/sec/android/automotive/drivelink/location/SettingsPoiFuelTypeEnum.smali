.class public final enum Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;
.super Ljava/lang/Enum;
.source "SettingsPoiFuelTypeEnum.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DIESEL:Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

.field public static final enum LPG:Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

.field public static final enum PETROL_MID:Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

.field public static final enum PETROL_PREMIUM:Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

.field public static final enum PETROL_REGULAR:Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

.field private static codeToStatusMapping:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private code:I

.field private description:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 10
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    const-string/jumbo v1, "PETROL_REGULAR"

    .line 20
    const-string/jumbo v2, "Petrol (regular)"

    invoke-direct {v0, v1, v8, v4, v2}, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->PETROL_REGULAR:Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    new-instance v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    const-string/jumbo v1, "PETROL_MID"

    const-string/jumbo v2, "Petrol (mid-grade)"

    invoke-direct {v0, v1, v4, v5, v2}, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->PETROL_MID:Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    new-instance v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    const-string/jumbo v1, "PETROL_PREMIUM"

    .line 21
    const-string/jumbo v2, "Petrol (premium)"

    invoke-direct {v0, v1, v5, v6, v2}, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->PETROL_PREMIUM:Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    new-instance v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    const-string/jumbo v1, "DIESEL"

    const-string/jumbo v2, "Diesel"

    invoke-direct {v0, v1, v6, v7, v2}, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->DIESEL:Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    new-instance v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    const-string/jumbo v1, "LPG"

    const/4 v2, 0x5

    const-string/jumbo v3, "LPG"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->LPG:Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    sget-object v1, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->PETROL_REGULAR:Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->PETROL_MID:Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->PETROL_PREMIUM:Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->DIESEL:Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->LPG:Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    .line 26
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p3, "code"    # I
    .param p4, "description"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    iput p3, p0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->code:I

    .line 30
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->description:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public static getStatus(Ljava/lang/String;Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;
    .locals 1
    .param p0, "mKey"    # Ljava/lang/String;
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 35
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->codeToStatusMapping:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 36
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->initMapping(Landroid/content/Context;)V

    .line 38
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->codeToStatusMapping:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    return-object v0
.end method

.method public static initMapping(Landroid/content/Context;)V
    .locals 6
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 42
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->codeToStatusMapping:Ljava/util/Map;

    .line 43
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->values()[Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 49
    return-void

    .line 43
    :cond_0
    aget-object v1, v3, v2

    .line 45
    .local v1, "s":Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;
    iget-object v0, v1, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->description:Ljava/lang/String;

    .line 47
    .local v0, "mKey":Ljava/lang/String;
    sget-object v5, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->codeToStatusMapping:Ljava/util/Map;

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->code:I

    return v0
.end method

.method public getDescription(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/SettingsPoiFuelTypeEnum;->description:Ljava/lang/String;

    return-object v0
.end method
