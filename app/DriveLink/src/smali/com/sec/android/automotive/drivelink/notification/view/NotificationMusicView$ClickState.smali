.class final enum Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;
.super Ljava/lang/Enum;
.source "NotificationMusicView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "ClickState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

.field public static final enum NEXT_BTN_CLICKED:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

.field public static final enum NONE:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

.field public static final enum PLAY_PAUSE_BTN_CLICKED:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

.field public static final enum PREV_BTN_CLICKED:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 60
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    const-string/jumbo v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;->NONE:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    const-string/jumbo v1, "PREV_BTN_CLICKED"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;->PREV_BTN_CLICKED:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    const-string/jumbo v1, "NEXT_BTN_CLICKED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;->NEXT_BTN_CLICKED:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    const-string/jumbo v1, "PLAY_PAUSE_BTN_CLICKED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;->PLAY_PAUSE_BTN_CLICKED:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    .line 59
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;->NONE:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;->PREV_BTN_CLICKED:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;->NEXT_BTN_CLICKED:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;->PLAY_PAUSE_BTN_CLICKED:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
