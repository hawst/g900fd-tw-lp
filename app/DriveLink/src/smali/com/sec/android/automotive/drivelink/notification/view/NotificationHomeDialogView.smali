.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationHomeDialogView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# static fields
.field static TAG:Ljava/lang/String;

.field private static isDebug:Z


# instance fields
.field private mBtnLocation:Landroid/widget/Button;

.field private mBtnMessage:Landroid/widget/Button;

.field private mBtnMusic:Landroid/widget/Button;

.field private mBtnPhone:Landroid/widget/Button;

.field private mTopArea:Landroid/widget/RelativeLayout;

.field protected mViewLocal:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-string/jumbo v0, "NotificationHomeDialogView"

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->TAG:Ljava/lang/String;

    .line 246
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->isDebug:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mViewLocal:Landroid/view/View;

    .line 31
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mTopArea:Landroid/widget/RelativeLayout;

    .line 32
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnPhone:Landroid/widget/Button;

    .line 33
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnMessage:Landroid/widget/Button;

    .line 34
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnLocation:Landroid/widget/Button;

    .line 35
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnMusic:Landroid/widget/Button;

    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mViewLocal:Landroid/view/View;

    .line 31
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mTopArea:Landroid/widget/RelativeLayout;

    .line 32
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnPhone:Landroid/widget/Button;

    .line 33
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnMessage:Landroid/widget/Button;

    .line 34
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnLocation:Landroid/widget/Button;

    .line 35
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnMusic:Landroid/widget/Button;

    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mViewLocal:Landroid/view/View;

    .line 31
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mTopArea:Landroid/widget/RelativeLayout;

    .line 32
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnPhone:Landroid/widget/Button;

    .line 33
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnMessage:Landroid/widget/Button;

    .line 34
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnLocation:Landroid/widget/Button;

    .line 35
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnMusic:Landroid/widget/Button;

    .line 60
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 61
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mViewLocal:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 62
    return-void
.end method

.method private static Debug(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 249
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->isDebug:Z

    if-eqz v0, :cond_0

    .line 250
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->TAG:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    :cond_0
    return-void
.end method

.method private initHomeMenu()V
    .locals 0

    .prologue
    .line 123
    return-void
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    const-string/jumbo v1, "initIndicatorView"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->Debug(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 67
    const-string/jumbo v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 66
    check-cast v0, Landroid/view/LayoutInflater;

    .line 68
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mViewLocal:Landroid/view/View;

    if-nez v1, :cond_0

    .line 69
    const v1, 0x7f0300a8

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mViewLocal:Landroid/view/View;

    .line 73
    const v1, 0x7f0902d0

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mTopArea:Landroid/widget/RelativeLayout;

    .line 74
    const v1, 0x7f0902d1

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnPhone:Landroid/widget/Button;

    .line 75
    const v1, 0x7f0902d4

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnMessage:Landroid/widget/Button;

    .line 76
    const v1, 0x7f0902d7

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnLocation:Landroid/widget/Button;

    .line 77
    const v1, 0x7f0902da

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnMusic:Landroid/widget/Button;

    .line 79
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnPhone:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnMessage:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnLocation:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mBtnMusic:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->mTopArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->initHomeMenu()V

    .line 89
    :cond_0
    return-void
.end method

.method private startHomeNotiFlow()V
    .locals 0

    .prologue
    .line 107
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 202
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 203
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 204
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "dispatchKeyEvent:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->endpointReco()V

    .line 208
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public getDescription(I)Ljava/lang/String;
    .locals 1
    .param p1, "nType"    # I

    .prologue
    .line 225
    const/4 v0, 0x0

    .line 227
    .local v0, "str":Ljava/lang/String;
    return-object v0
.end method

.method public getDistanceAhead(I)Ljava/lang/String;
    .locals 1
    .param p1, "nType"    # I

    .prologue
    .line 232
    const/4 v0, 0x0

    .line 234
    .local v0, "str":Ljava/lang/String;
    return-object v0
.end method

.method public getDistanceBackup(I)Ljava/lang/String;
    .locals 1
    .param p1, "nType"    # I

    .prologue
    .line 239
    const/4 v0, 0x0

    .line 241
    .local v0, "str":Ljava/lang/String;
    return-object v0
.end method

.method public getIncident(I)Ljava/lang/String;
    .locals 1
    .param p1, "nType"    # I

    .prologue
    .line 218
    const/4 v0, 0x0

    .line 220
    .local v0, "str":Ljava/lang/String;
    return-object v0
.end method

.method protected needProcessBackKey()Z
    .locals 1

    .prologue
    .line 196
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 128
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 192
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 131
    :pswitch_1
    const-string/jumbo v1, "onClick : btnPhone"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->Debug(Ljava/lang/String;)V

    .line 132
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getListener()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    move-result-object v1

    if-nez v1, :cond_1

    .line 133
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "listener is null :MultiWindowListener"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->cancelNotiFlow()V

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->dismiss()V

    goto :goto_0

    .line 135
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getListener()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    move-result-object v1

    .line 136
    sget-object v2, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_PHONE:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    .line 135
    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;->OnState(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;)V

    goto :goto_1

    .line 143
    :pswitch_2
    const-string/jumbo v1, "onClick : btnMessage"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->Debug(Ljava/lang/String;)V

    .line 144
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getListener()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    move-result-object v1

    if-nez v1, :cond_2

    .line 145
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "listener is null :MultiWindowListener"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->cancelNotiFlow()V

    .line 151
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->dismiss()V

    goto :goto_0

    .line 147
    :cond_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getListener()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    move-result-object v1

    .line 148
    sget-object v2, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_MESSAGE:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    .line 147
    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;->OnState(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;)V

    goto :goto_2

    .line 155
    :pswitch_3
    const-string/jumbo v1, "onClick : btnLocation"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->Debug(Ljava/lang/String;)V

    .line 156
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getListener()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    move-result-object v1

    if-nez v1, :cond_3

    .line 157
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "listener is null :MultiWindowListener"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->cancelNotiFlow()V

    .line 163
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->dismiss()V

    goto :goto_0

    .line 159
    :cond_3
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getListener()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    move-result-object v1

    .line 160
    sget-object v2, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_LOCATION:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    .line 159
    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;->OnState(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;)V

    goto :goto_3

    .line 167
    :pswitch_4
    const-string/jumbo v1, "onClick : btnMusic"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->Debug(Ljava/lang/String;)V

    .line 168
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getListener()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    move-result-object v1

    if-nez v1, :cond_4

    .line 169
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "listener is null :MultiWindowListener"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    :goto_4
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->cancelNotiFlow()V

    .line 175
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->dismiss()V

    goto/16 :goto_0

    .line 171
    :cond_4
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getListener()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    move-result-object v1

    .line 172
    sget-object v2, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_MUSIC:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    .line 171
    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;->OnState(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;)V

    goto :goto_4

    .line 179
    :pswitch_5
    const-string/jumbo v1, "onClick : layout_top_area"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->Debug(Ljava/lang/String;)V

    .line 180
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getInstance()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    move-result-object v1

    .line 181
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getTopAreaHandler()Landroid/os/Handler;

    move-result-object v0

    .line 182
    .local v0, "handler":Landroid/os/Handler;
    if-eqz v0, :cond_0

    .line 183
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Touched Multiwindow Voice Area"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    sget v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->TOPAREA:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 128
    :pswitch_data_0
    .packed-switch 0x7f0902d0
        :pswitch_5
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    .line 213
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "onMicStateChanged : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->Debug(Ljava/lang/String;)V

    .line 214
    invoke-super {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V

    .line 215
    return-void
.end method

.method public onNotificationDisappeared()V
    .locals 1

    .prologue
    .line 112
    const-string/jumbo v0, "onNotificationDisappeared"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->Debug(Ljava/lang/String;)V

    .line 113
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onNotificationDisappeared()V

    .line 114
    return-void
.end method

.method public onNotificationWillAppear()V
    .locals 2

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->getFlowState()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->RESTART:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    if-ne v0, v1, :cond_1

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->restartNotiFlow()V

    .line 101
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onNotificationWillAppear()V

    .line 102
    return-void

    .line 97
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->isAppeared()Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->startHomeNotiFlow()V

    .line 99
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationHomeDialogView;->setAppeared(Z)V

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 119
    const/4 v0, 0x0

    return v0
.end method
