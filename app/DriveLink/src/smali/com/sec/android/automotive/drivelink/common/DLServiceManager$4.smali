.class Lcom/sec/android/automotive/drivelink/common/DLServiceManager$4;
.super Ljava/lang/Object;
.source "DLServiceManager.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getOnDLMessageListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$4;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    .line 429
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseRequestChangeMessageStatusToRead(Z)V
    .locals 3
    .param p1, "result"    # Z

    .prologue
    .line 463
    const-string/jumbo v0, "onResponseRequestChangeMessageStatusToRead"

    .line 464
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, " result = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 463
    # invokes: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->Debug(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$18(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;

    move-result-object v0

    .line 466
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_CHANGE_STATUS_TO_READ:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    .line 465
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->onChangeStatus(Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;)V

    .line 467
    return-void
.end method

.method public onResponseRequestInboxList(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 452
    .local p1, "inboxList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;>;"
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$4;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$16(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V

    .line 453
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$4;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$17(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 455
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;

    move-result-object v0

    .line 456
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_INBOX_LIST:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    .line 455
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->onChangeStatus(Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;)V

    .line 457
    return-void
.end method

.method public onResponseRequestIncommingMessageList(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 443
    .local p1, "messageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;>;"
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$4;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$14(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V

    .line 444
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$4;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$15(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 446
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;

    move-result-object v0

    .line 447
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_INCOMMING_MESSAGE_LIST:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    .line 446
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->onChangeStatus(Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;)V

    .line 448
    return-void
.end method

.method public onResponseRequestSendMessage(Z)V
    .locals 5
    .param p1, "result"    # Z

    .prologue
    const/4 v4, 0x0

    .line 471
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;

    move-result-object v1

    .line 472
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_SEND_MESSAGE:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    .line 471
    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->onChangeStatus(Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;)V

    .line 474
    const-string/jumbo v1, "onResponseRequestSendMessage"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, " result = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->Debug(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$18(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    if-nez p1, :cond_1

    .line 477
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;->getListener()Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener$OnMessageSendingNotiListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 478
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;

    .line 479
    const/4 v1, 0x2

    .line 478
    invoke-direct {v0, v1, v4, v4}, Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 481
    .local v0, "info":Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;->getListener()Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener$OnMessageSendingNotiListener;

    move-result-object v1

    .line 482
    invoke-interface {v1, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener$OnMessageSendingNotiListener;->OnMessageSendingState(Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;)V

    .line 534
    .end local v0    # "info":Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;
    :cond_0
    :goto_0
    return-void

    .line 489
    :cond_1
    new-instance v1, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$4;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$4(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->playSendSuccessSound()V

    goto :goto_0
.end method

.method public onResponseRequestUnreadMessageCount(I)V
    .locals 2
    .param p1, "itemCount"    # I

    .prologue
    .line 433
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$4;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$12(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;I)V

    .line 434
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$4;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$13(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 436
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;

    move-result-object v0

    .line 437
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_UNREAD_COUNT:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    .line 436
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->onChangeStatus(Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;)V

    .line 438
    return-void
.end method

.method public onResponseRequestUnreadMessageCountByInbox(I)V
    .locals 3
    .param p1, "itemCount"    # I

    .prologue
    .line 538
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$4;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$19(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;I)V

    .line 539
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$4;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$20(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 541
    const-string/jumbo v0, "onResponseRequestUnreadMessageCountByInbox"

    .line 542
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, " itemCount = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 541
    # invokes: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->Debug(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$18(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;

    move-result-object v0

    .line 544
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_UNREAD_MESSAGE_BY_INBOX:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    .line 543
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->onChangeStatus(Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;)V

    .line 545
    return-void
.end method
