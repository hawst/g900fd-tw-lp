.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestStartDrivingStatusMonitoring;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestStartDrivingStatusMonitoring.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mStartStatus:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .prologue
    .line 17
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestStartDrivingStatusMonitoring;->mContext:Landroid/content/Context;

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestStartDrivingStatusMonitoring;->mStartStatus:Z

    .line 18
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestStartDrivingStatusMonitoring;->mContext:Landroid/content/Context;

    .line 19
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestStartDrivingStatusMonitoring;->mContext:Landroid/content/Context;

    .line 44
    return-void
.end method

.method protected process()V
    .locals 2

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestStartDrivingStatusMonitoring;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 25
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDrivingManager()Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    move-result-object v0

    .line 27
    .local v0, "drivingManager":Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestStartDrivingStatusMonitoring;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->startDrivingStatusMonitoring(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestStartDrivingStatusMonitoring;->mStartStatus:Z

    .line 28
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestStartDrivingStatusMonitoring;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 34
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkDrivingStatusListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;

    move-result-object v0

    .line 35
    .local v0, "drivingStatusListener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;
    if-eqz v0, :cond_0

    .line 37
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestStartDrivingStatusMonitoring;->mStartStatus:Z

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;->onResponseRequestStartDrivingStatusMonitoring(Z)V

    .line 39
    :cond_0
    return-void
.end method
