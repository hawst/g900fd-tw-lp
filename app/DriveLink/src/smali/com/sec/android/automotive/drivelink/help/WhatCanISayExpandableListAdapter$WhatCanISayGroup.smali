.class Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;
.super Ljava/lang/Object;
.source "WhatCanISayExpandableListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WhatCanISayGroup"
.end annotation


# instance fields
.field children:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field imgResourceId:I

.field subTitle:Ljava/lang/String;

.field title:Ljava/lang/String;


# direct methods
.method private constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/util/LinkedList;)V
    .locals 1
    .param p1, "imgResourceId"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subTitle"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 235
    .local p4, "children":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->children:Ljava/util/LinkedList;

    .line 237
    iput p1, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->imgResourceId:I

    .line 238
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->title:Ljava/lang/String;

    .line 239
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->subTitle:Ljava/lang/String;

    .line 240
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->children:Ljava/util/LinkedList;

    .line 241
    return-void
.end method

.method synthetic constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/util/LinkedList;Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/util/LinkedList;)V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 291
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->getChildren()Ljava/util/LinkedList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 265
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 278
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->getSubTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;)I
    .locals 1

    .prologue
    .line 252
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->getImgResourceId()I

    move-result v0

    return v0
.end method

.method private getChildren()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 292
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->children:Ljava/util/LinkedList;

    return-object v0
.end method

.method private getImgResourceId()I
    .locals 1

    .prologue
    .line 253
    iget v0, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->imgResourceId:I

    return v0
.end method

.method private getSubTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->subTitle:Ljava/lang/String;

    return-object v0
.end method

.method private getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/WhatCanISayExpandableListAdapter$WhatCanISayGroup;->title:Ljava/lang/String;

    return-object v0
.end method
