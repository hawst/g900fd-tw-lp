.class Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$5;
.super Ljava/lang/Object;
.source "OnDriveLinkMusicPlayerListenerImp.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->onMusicPlayCompleted(Landroid/media/MediaPlayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;

.field private final synthetic val$_mp:Landroid/media/MediaPlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;Landroid/media/MediaPlayer;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$5;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$5;->val$_mp:Landroid/media/MediaPlayer;

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$5;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$5;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$5;->val$_mp:Landroid/media/MediaPlayer;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;->onMusicPlayCompleted(Landroid/media/MediaPlayer;)V

    .line 104
    :cond_0
    return-void
.end method
