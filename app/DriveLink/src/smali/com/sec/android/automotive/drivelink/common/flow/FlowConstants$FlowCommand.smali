.class public Lcom/sec/android/automotive/drivelink/common/flow/FlowConstants$FlowCommand;
.super Ljava/lang/Object;
.source "FlowConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/flow/FlowConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FlowCommand"
.end annotation


# static fields
.field public static final DM_CALL:Ljava/lang/String; = "DM_CALL"

.field public static final DM_CANCEL:Ljava/lang/String; = "DM_CANCEL"

.field public static final DM_CONFIRM_EXCUTE:Ljava/lang/String; = "DM_CONFIRM_EXCUTE"

.field public static final DM_CONFIRM_IGNORE:Ljava/lang/String; = "DM_CONFIRM_IGNORE"

.field public static final DM_CONFIRM_LOOKUP:Ljava/lang/String; = "DM_CONFIRM_LOOKUP"

.field public static final DM_CONFIRM_NO:Ljava/lang/String; = "DM_CONFIRM_NO"

.field public static final DM_CONFIRM_RESET:Ljava/lang/String; = "DM_CONFIRM_RESET"

.field public static final DM_CONFIRM_ROUTE:Ljava/lang/String; = "DM_CONFIRM_ROUTE"

.field public static final DM_CONFIRM_ROUTE_NOP:Ljava/lang/String; = "DM_CONFIRM_ROUTE_NOP"

.field public static final DM_CONFIRM_SEND:Ljava/lang/String; = "DM_CONFIRM_SEND"

.field public static final DM_CONFIRM_YES:Ljava/lang/String; = "DM_CONFIRM_YES"

.field public static final DM_LIST_ORDINAL:Ljava/lang/String; = "DM_LIST_ORDINAL"

.field public static final DM_LIST_SELECTED:Ljava/lang/String; = "DM_LIST_SELECTED"

.field public static final DM_MUSIC_PLAYER_COMMAND:Ljava/lang/String; = "DM_MUSIC_PLAYER_COMMAND"

.field public static final DM_NEXT:Ljava/lang/String; = "DM_NEXT"

.field public static final DM_READ:Ljava/lang/String; = "DM_READ"

.field public static final DM_REPLY:Ljava/lang/String; = "DM_REPLY"

.field public static final DM_RETURN_MAIN:Ljava/lang/String; = "DM_RETURN_MAIN"

.field public static final DM_STOP_NAVIGATION:Ljava/lang/String; = "DM_STOP_NAVIGATION"


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/flow/FlowConstants;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/common/flow/FlowConstants;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowConstants$FlowCommand;->this$0:Lcom/sec/android/automotive/drivelink/common/flow/FlowConstants;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
