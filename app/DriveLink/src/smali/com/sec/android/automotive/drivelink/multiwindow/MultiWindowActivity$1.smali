.class Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$1;
.super Ljava/lang/Object;
.source "MultiWindowActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$multiwindow$MultiWindowListener$OnMultiHomeListener$MultiHomeState:[I


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$multiwindow$MultiWindowListener$OnMultiHomeListener$MultiHomeState()[I
    .locals 3

    .prologue
    .line 608
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$1;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$multiwindow$MultiWindowListener$OnMultiHomeListener$MultiHomeState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->values()[Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_LOCATION:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_MESSAGE:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_MUSIC:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_PHONE:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_MIC_OFF:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_MIC_ON:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_6
    :try_start_6
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_NONE:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_7
    sput-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$1;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$multiwindow$MultiWindowListener$OnMultiHomeListener$MultiHomeState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_7

    :catch_1
    move-exception v1

    goto :goto_6

    :catch_2
    move-exception v1

    goto :goto_5

    :catch_3
    move-exception v1

    goto :goto_4

    :catch_4
    move-exception v1

    goto :goto_3

    :catch_5
    move-exception v1

    goto :goto_2

    :catch_6
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    .line 608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnRequestCall(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 10
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "number"    # Ljava/lang/String;
    .param p3, "img"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v4, 0x0

    .line 645
    const-string/jumbo v1, "[MultiWindowActivity]"

    const-string/jumbo v2, "OnRequestCall"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 648
    .local v6, "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    new-instance v9, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    .line 649
    const/16 v1, 0xc

    .line 648
    invoke-direct {v9, v1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;-><init>(ILjava/lang/String;)V

    .line 650
    .local v9, "dlPhoneNumber":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 651
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    const-wide/16 v1, 0x0

    move-object v3, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 654
    .local v0, "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-static {v0}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v8

    .line 656
    .local v8, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v7}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 657
    .local v7, "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput-object v8, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 658
    const/4 v1, 0x0

    iput v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    .line 659
    const-string/jumbo v1, "DM_DIAL"

    invoke-static {v1, v7}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 661
    return-void
.end method

.method public OnRequestReply(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 10
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "number"    # Ljava/lang/String;
    .param p3, "img"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v4, 0x0

    .line 666
    const-string/jumbo v1, "[MultiWindowActivity]"

    const-string/jumbo v2, "OnRequestReply"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 668
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 669
    .local v6, "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    new-instance v8, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    .line 670
    const/16 v1, 0xc

    .line 669
    invoke-direct {v8, v1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;-><init>(ILjava/lang/String;)V

    .line 671
    .local v8, "dlPhoneNumber":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 672
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    const-wide/16 v1, 0x0

    move-object v3, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 675
    .local v0, "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-static {v0}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v7

    .line 677
    .local v7, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v9, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v9}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 678
    .local v9, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput-object v7, v9, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 679
    const-string/jumbo v1, "DM_SMS_COMPOSE"

    invoke-static {v1, v9}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 682
    return-void
.end method

.method public OnState(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;)V
    .locals 3
    .param p1, "state"    # Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    .prologue
    .line 612
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, " mMultiHomeListener OnState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$1;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$multiwindow$MultiWindowListener$OnMultiHomeListener$MultiHomeState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 641
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 618
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$0(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasNotification()Z

    move-result v0

    if-nez v0, :cond_0

    .line 619
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->isVisibleActivity()Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$1(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 620
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;->getListener()Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener$OnHomeNotiListener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener$OnHomeNotiListener;->OnHomeState(I)V

    goto :goto_0

    .line 625
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->dismissNotiHomeMenu()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$2(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)V

    goto :goto_0

    .line 628
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->clickPhone()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$3(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)V

    goto :goto_0

    .line 631
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->clickMessage()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$4(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)V

    goto :goto_0

    .line 634
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->clickLocation()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$5(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)V

    goto :goto_0

    .line 637
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->clickMusic()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->access$6(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;)V

    goto :goto_0

    .line 613
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
