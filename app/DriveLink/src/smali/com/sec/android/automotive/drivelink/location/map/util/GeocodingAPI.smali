.class public Lcom/sec/android/automotive/drivelink/location/map/util/GeocodingAPI;
.super Ljava/lang/Object;
.source "GeocodingAPI.java"


# static fields
.field private static mGeoCoderMaxResults:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/automotive/drivelink/location/map/util/GeocodingAPI;->mGeoCoderMaxResults:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAddressFromLatLng(DDLandroid/content/Context;)Ljava/lang/String;
    .locals 16
    .param p0, "lat"    # D
    .param p2, "lng"    # D
    .param p4, "baseContext"    # Landroid/content/Context;

    .prologue
    .line 42
    new-instance v1, Landroid/location/Geocoder;

    move-object/from16 v0, p4

    invoke-direct {v1, v0}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    .line 44
    .local v1, "coder":Landroid/location/Geocoder;
    const/4 v7, 0x0

    .line 47
    .local v7, "address":Ljava/lang/String;
    :try_start_0
    sget v6, Lcom/sec/android/automotive/drivelink/location/map/util/GeocodingAPI;->mGeoCoderMaxResults:I

    move-wide/from16 v2, p0

    move-wide/from16 v4, p2

    invoke-virtual/range {v1 .. v6}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v9

    .line 48
    .local v9, "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    if-nez v9, :cond_0

    .line 49
    const/4 v2, 0x0

    .line 84
    .end local v9    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :goto_0
    return-object v2

    .line 52
    .restart local v9    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :cond_0
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_5

    .line 53
    const/4 v2, 0x0

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/location/Address;

    .line 54
    .local v13, "location":Landroid/location/Address;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 55
    .local v8, "addressList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v7, ""

    .line 57
    invoke-virtual {v13}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v15

    .line 58
    .local v15, "street":Ljava/lang/String;
    invoke-virtual {v13}, Landroid/location/Address;->getSubLocality()Ljava/lang/String;

    move-result-object v14

    .line 59
    .local v14, "neighborhood":Ljava/lang/String;
    invoke-virtual {v13}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v10

    .line 60
    .local v10, "city":Ljava/lang/String;
    invoke-virtual {v13}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v11

    .line 62
    .local v11, "country":Ljava/lang/String;
    if-eqz v15, :cond_1

    .line 63
    invoke-interface {v8, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    :cond_1
    if-eqz v14, :cond_2

    .line 67
    invoke-interface {v8, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    :cond_2
    if-eqz v10, :cond_3

    .line 71
    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    :cond_3
    if-eqz v11, :cond_4

    .line 75
    invoke-interface {v8, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    :cond_4
    const-string/jumbo v2, ", "

    invoke-static {v2, v8}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .end local v8    # "addressList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v10    # "city":Ljava/lang/String;
    .end local v11    # "country":Ljava/lang/String;
    .end local v13    # "location":Landroid/location/Address;
    .end local v14    # "neighborhood":Ljava/lang/String;
    .end local v15    # "street":Ljava/lang/String;
    :cond_5
    move-object v2, v7

    .line 84
    goto :goto_0

    .line 80
    .end local v9    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :catch_0
    move-exception v12

    .line 81
    .local v12, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getLatLngFromAddress(Ljava/lang/String;Landroid/content/Context;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 11
    .param p0, "address"    # Ljava/lang/String;
    .param p1, "baseContext"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    .line 98
    new-instance v1, Landroid/location/Geocoder;

    invoke-direct {v1, p1}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    .line 100
    .local v1, "coder":Landroid/location/Geocoder;
    const/4 v4, 0x0

    .line 103
    .local v4, "point":Lcom/google/android/gms/maps/model/LatLng;
    :try_start_0
    sget v7, Lcom/sec/android/automotive/drivelink/location/map/util/GeocodingAPI;->mGeoCoderMaxResults:I

    invoke-virtual {v1, p0, v7}, Landroid/location/Geocoder;->getFromLocationName(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    .line 104
    .local v0, "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x1

    if-ge v7, v8, :cond_1

    :cond_0
    move-object v5, v6

    .line 114
    .end local v0    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :goto_0
    return-object v5

    .line 108
    .restart local v0    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :cond_1
    const/4 v7, 0x0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/location/Address;

    .line 109
    .local v3, "location":Landroid/location/Address;
    new-instance v5, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v3}, Landroid/location/Address;->getLatitude()D

    move-result-wide v7

    invoke-virtual {v3}, Landroid/location/Address;->getLongitude()D

    move-result-wide v9

    invoke-direct {v5, v7, v8, v9, v10}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v4    # "point":Lcom/google/android/gms/maps/model/LatLng;
    .local v5, "point":Lcom/google/android/gms/maps/model/LatLng;
    move-object v4, v5

    .line 114
    .end local v5    # "point":Lcom/google/android/gms/maps/model/LatLng;
    .restart local v4    # "point":Lcom/google/android/gms/maps/model/LatLng;
    goto :goto_0

    .line 110
    .end local v0    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .end local v3    # "location":Landroid/location/Address;
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/io/IOException;
    move-object v5, v6

    .line 111
    goto :goto_0
.end method

.method public static getLocationFromLatLng(DDLandroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .locals 19
    .param p0, "lat"    # D
    .param p2, "lng"    # D
    .param p4, "baseContext"    # Landroid/content/Context;

    .prologue
    .line 119
    new-instance v2, Landroid/location/Geocoder;

    move-object/from16 v0, p4

    invoke-direct {v2, v0}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    .line 121
    .local v2, "coder":Landroid/location/Geocoder;
    const/4 v8, 0x0

    .line 124
    .local v8, "address":Ljava/lang/String;
    :try_start_0
    sget v7, Lcom/sec/android/automotive/drivelink/location/map/util/GeocodingAPI;->mGeoCoderMaxResults:I

    move-wide/from16 v3, p0

    move-wide/from16 v5, p2

    invoke-virtual/range {v2 .. v7}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v10

    .line 125
    .local v10, "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    if-eqz v10, :cond_0

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 126
    :cond_0
    const/4 v13, 0x0

    .line 169
    .end local v10    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :cond_1
    :goto_0
    return-object v13

    .line 129
    .restart local v10    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :cond_2
    const/4 v3, 0x0

    invoke-interface {v10, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/location/Address;

    .line 130
    .local v15, "location":Landroid/location/Address;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 131
    .local v9, "addressList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v8, ""

    .line 133
    invoke-virtual {v15}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v18

    .line 134
    .local v18, "street":Ljava/lang/String;
    invoke-virtual {v15}, Landroid/location/Address;->getSubLocality()Ljava/lang/String;

    move-result-object v17

    .line 135
    .local v17, "neighborhood":Ljava/lang/String;
    invoke-virtual {v15}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v11

    .line 136
    .local v11, "city":Ljava/lang/String;
    invoke-virtual {v15}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v12

    .line 137
    .local v12, "country":Ljava/lang/String;
    invoke-virtual {v15}, Landroid/location/Address;->getFeatureName()Ljava/lang/String;

    move-result-object v16

    .line 139
    .local v16, "name":Ljava/lang/String;
    if-eqz v18, :cond_3

    .line 140
    move-object/from16 v0, v18

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    :cond_3
    if-eqz v17, :cond_4

    .line 144
    move-object/from16 v0, v17

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    :cond_4
    if-eqz v11, :cond_5

    .line 148
    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    :cond_5
    if-eqz v12, :cond_6

    .line 152
    invoke-interface {v9, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    :cond_6
    const-string/jumbo v3, ", "

    invoke-static {v3, v9}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v8

    .line 156
    new-instance v13, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    invoke-direct {v13}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;-><init>()V

    .line 157
    .local v13, "dlLocation":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;
    move-wide/from16 v0, p0

    invoke-virtual {v13, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLatitude(D)V

    .line 158
    move-wide/from16 v0, p2

    invoke-virtual {v13, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLongitude(D)V

    .line 159
    invoke-virtual {v13, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLocationAddress(Ljava/lang/String;)V

    .line 161
    if-eqz v16, :cond_1

    .line 162
    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLocationName(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 165
    .end local v9    # "addressList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v10    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .end local v11    # "city":Ljava/lang/String;
    .end local v12    # "country":Ljava/lang/String;
    .end local v13    # "dlLocation":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;
    .end local v15    # "location":Landroid/location/Address;
    .end local v16    # "name":Ljava/lang/String;
    .end local v17    # "neighborhood":Ljava/lang/String;
    .end local v18    # "street":Ljava/lang/String;
    :catch_0
    move-exception v14

    .line 166
    .local v14, "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    .line 169
    const/4 v13, 0x0

    goto :goto_0
.end method
