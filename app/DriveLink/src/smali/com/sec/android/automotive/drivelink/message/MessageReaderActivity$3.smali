.class Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;
.super Ljava/lang/Object;
.source "MessageReaderActivity.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    .line 935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;
    .locals 1

    .prologue
    .line 935
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    return-object v0
.end method


# virtual methods
.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 2
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "arg1"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 940
    const-string/jumbo v0, ""

    const-string/jumbo v1, "mTTSPlaybackListener :::: onRequestCancelled"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 941
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 6
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 946
    const-string/jumbo v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "mTTSPlaybackListener :::: onRequestDidPlay size : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 947
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMessageList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$4(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " mTTSMsgState : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 948
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgState:I
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$5(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "mIsTTSlist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mIsTTSlist:Z
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$6(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 946
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 950
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mIsTTSlist:Z
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$6(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 951
    const-string/jumbo v1, ""

    const-string/jumbo v2, "mTTSPlaybackListener :::: abandonAudioFocus"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 952
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->abandonAudioFocus()V
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$7(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V

    .line 955
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mIsTTSlist:Z
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$6(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMsgState:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$5(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I

    move-result v1

    if-eq v1, v5, :cond_1

    .line 957
    const-wide/16 v1, 0x12c

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 962
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSListIndex:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$8(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMessageList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$4(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_2

    .line 963
    const-string/jumbo v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "[YANG] mTTSPlaybackListener :::: tts : "

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 964
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMessageList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$4(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 963
    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 965
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    .line 966
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMessageList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$4(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSListIndex:I
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$8(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 965
    invoke-interface {v2, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    .line 967
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSListIndex:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$8(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$9(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;I)V

    .line 990
    :cond_1
    :goto_1
    return-void

    .line 958
    :catch_0
    move-exception v0

    .line 960
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 968
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSListIndex:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$8(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMessageList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$4(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_1

    .line 969
    const-string/jumbo v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "[YANG] mTTSPlaybackListener :::: last tts : "

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 970
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMessageList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$4(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 969
    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 971
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    invoke-static {v1, v4}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$10(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Z)V

    .line 972
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    invoke-static {v1, v5}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$11(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;I)V

    .line 974
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$12(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3$1;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3$1;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 2
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "arg1"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 995
    const-string/jumbo v0, ""

    const-string/jumbo v1, "mTTSPlaybackListener :::: onRequestIgnored"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 996
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 2
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 1001
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$11(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;I)V

    .line 1002
    const-string/jumbo v0, ""

    const-string/jumbo v1, "mTTSPlaybackListener :::: onRequestWillPlay"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1003
    return-void
.end method
