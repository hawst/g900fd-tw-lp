.class public Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLGasStationInfoImp;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLGastStationInfo;
.source "DLGasStationInfoImp.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x5cf42ac7be582013L


# instance fields
.field private address:Ljava/lang/String;

.field private distance:D

.field private location:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

.field private name:Ljava/lang/String;

.field private price:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLGastStationInfo;-><init>()V

    .line 20
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLGasStationInfoImp;->price:D

    .line 8
    return-void
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLGasStationInfoImp;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getDistance()D
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLGasStationInfoImp;->distance:D

    return-wide v0
.end method

.method public getInfo1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public getInfo2()Ljava/lang/String;
    .locals 6

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLGasStationInfoImp;->getPrice()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "$"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "%.2f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLGasStationInfoImp;->getPrice()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 77
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "$ -"

    goto :goto_0
.end method

.method public getInfo3()Ljava/lang/String;
    .locals 6

    .prologue
    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "%.2f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLGasStationInfoImp;->getDistance()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, " km"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLGasStationInfoImp;->location:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLGasStationInfoImp;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPrice()D
    .locals 2

    .prologue
    .line 44
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLGasStationInfoImp;->price:D

    return-wide v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLGasStationInfoImp;->address:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public setDistance(D)V
    .locals 0
    .param p1, "distance"    # D

    .prologue
    .line 56
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLGasStationInfoImp;->distance:D

    .line 57
    return-void
.end method

.method public setLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 0
    .param p1, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLGasStationInfoImp;->location:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 61
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLGasStationInfoImp;->name:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public setPrice(D)V
    .locals 0
    .param p1, "price"    # D

    .prologue
    .line 64
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLGasStationInfoImp;->price:D

    .line 65
    return-void
.end method
