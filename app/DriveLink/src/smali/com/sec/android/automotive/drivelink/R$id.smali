.class public final Lcom/sec/android/automotive/drivelink/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final Contentlayout:I = 0x7f0902f8

.field public static final InboxPageNavigationLayout:I = 0x7f0900b2

.field public static final LinearLayout03:I = 0x7f090279

.field public static final ScrollView01:I = 0x7f090278

.field public static final action_bar_layout:I = 0x7f090062

.field public static final agls_main_layout:I = 0x7f090040

.field public static final allListLayout:I = 0x7f09009c

.field public static final area_left:I = 0x7f0901d6

.field public static final area_right:I = 0x7f0901d7

.field public static final artistname_player:I = 0x7f0900d0

.field public static final back_mark:I = 0x7f09019e

.field public static final balloonPointCluster:I = 0x7f0900f6

.field public static final bar_layout:I = 0x7f09001d

.field public static final basemap:I = 0x7f0900a8

.field public static final bmapView:I = 0x7f090198

.field public static final bottom_area:I = 0x7f090270

.field public static final bottom_bar_layout:I = 0x7f090022

.field public static final btCancel:I = 0x7f09020d

.field public static final btMessageCall:I = 0x7f0902fc

.field public static final btMessageIgnore:I = 0x7f0902e2

.field public static final btMessageMMSCall:I = 0x7f0902f7

.field public static final btMessageMMSIgnore:I = 0x7f0902f6

.field public static final btMessageRead:I = 0x7f0902f4

.field public static final btMessageReply:I = 0x7f0902fb

.field public static final btOk:I = 0x7f09020e

.field public static final btRequesetConfirmLayout:I = 0x7f0902b5

.field public static final btRequesetPinLayout:I = 0x7f0902b2

.field public static final btRoute:I = 0x7f0902e3

.field public static final bt_back:I = 0x7f090026

.field public static final bt_back_main:I = 0x7f0900fe

.field public static final bt_call:I = 0x7f090206

.field public static final bt_change_destination:I = 0x7f090024

.field public static final bt_close:I = 0x7f09029d

.field public static final bt_help_txt_confirm:I = 0x7f0902b6

.field public static final bt_help_txt_pin:I = 0x7f0902b3

.field public static final bt_nav_back:I = 0x7f09002a

.field public static final bt_new:I = 0x7f090060

.field public static final bt_next:I = 0x7f090202

.field public static final bt_pin_code:I = 0x7f0902b4

.field public static final bt_prev:I = 0x7f090201

.field public static final bt_reinvite:I = 0x7f090205

.field public static final bt_request:I = 0x7f09008e

.field public static final bt_restart:I = 0x7f09005f

.field public static final bt_route:I = 0x7f090023

.field public static final bt_search_back:I = 0x7f090331

.field public static final bt_share:I = 0x7f09008b

.field public static final bt_start_sharing:I = 0x7f09005c

.field public static final bt_update:I = 0x7f09018d

.field public static final bt_view_details:I = 0x7f09005d

.field public static final btnAccountSignIn:I = 0x7f090006

.field public static final btnAccountSignInBasic:I = 0x7f09011c

.field public static final btnAddContactsDone:I = 0x7f09011e

.field public static final btnAdditionalRegistration:I = 0x7f09016a

.field public static final btnCall:I = 0x7f0900c7

.field public static final btnCancel:I = 0x7f090098

.field public static final btnDeleteFavoriteContacts:I = 0x7f09012a

.field public static final btnLocation:I = 0x7f0902d7

.field public static final btnMessage:I = 0x7f0902d4

.field public static final btnMessageCompleteCancel:I = 0x7f0900c0

.field public static final btnMessageCompleteReplace:I = 0x7f0900bf

.field public static final btnMessageCompleteSend:I = 0x7f0900c1

.field public static final btnMessageStartCancel:I = 0x7f0900bd

.field public static final btnMusic:I = 0x7f0902da

.field public static final btnNavigate:I = 0x7f0900a9

.field public static final btnPhone:I = 0x7f0902d1

.field public static final btnRegisterFromMobile:I = 0x7f09015f

.field public static final btnRejectButtonCancel:I = 0x7f09014d

.field public static final btnRejectButtonOk:I = 0x7f09014e

.field public static final btnRejectMessageCancel:I = 0x7f09014b

.field public static final btnReply:I = 0x7f0900c6

.field public static final btnRoute:I = 0x7f090099

.field public static final btnSave:I = 0x7f0900e6

.field public static final btnSearchContactsCancel:I = 0x7f090121

.field public static final btnShare:I = 0x7f0900a2

.field public static final btn_ab_back:I = 0x7f0900f4

.field public static final btn_cont:I = 0x7f0900fc

.field public static final btn_image:I = 0x7f09019a

.field public static final btn_italic_text:I = 0x7f09019d

.field public static final btn_layout:I = 0x7f090199

.field public static final btn_multi_music_more:I = 0x7f090299

.field public static final btn_my_loc:I = 0x7f09001a

.field public static final btn_placeon_accept:I = 0x7f090300

.field public static final btn_placeon_decline:I = 0x7f0902ff

.field public static final btn_reinvite:I = 0x7f0901e7

.field public static final btn_reset_server_data:I = 0x7f090151

.field public static final btn_retry:I = 0x7f0900fd

.field public static final btn_save:I = 0x7f090018

.field public static final btn_ss_accept:I = 0x7f090302

.field public static final btn_ss_decline:I = 0x7f090301

.field public static final btn_text:I = 0x7f09019c

.field public static final btn_text_request:I = 0x7f090090

.field public static final btn_text_share:I = 0x7f09008d

.field public static final button1:I = 0x7f0902ad

.field public static final button2:I = 0x7f0902ae

.field public static final button3:I = 0x7f0902af

.field public static final button_area:I = 0x7f090088

.field public static final button_bar:I = 0x7f090097

.field public static final button_container:I = 0x7f09005b

.field public static final buttons:I = 0x7f090254

.field public static final cbAutoLaunch:I = 0x7f090142

.field public static final cbFavoriteContacts:I = 0x7f090128

.field public static final cbRegisterAutoLaunch:I = 0x7f090169

.field public static final cbTermsofServiceAgree1:I = 0x7f090116

.field public static final cb_participant:I = 0x7f0901fc

.field public static final change_list:I = 0x7f09003a

.field public static final changed_destination_message:I = 0x7f0902ee

.field public static final child_area:I = 0x7f0901a8

.field public static final circle_option1:I = 0x7f09002f

.field public static final circle_option2:I = 0x7f090030

.field public static final circle_option3:I = 0x7f090031

.field public static final circle_option4:I = 0x7f090032

.field public static final circle_option5:I = 0x7f090033

.field public static final clear_partic_text:I = 0x7f090036

.field public static final clear_text:I = 0x7f090039

.field public static final clearable_edit:I = 0x7f09027e

.field public static final clock_view:I = 0x7f09001b

.field public static final completeBtnArealayout:I = 0x7f0900be

.field public static final contactLayout1:I = 0x7f090207

.field public static final contactLayout2:I = 0x7f090208

.field public static final contactLayout3:I = 0x7f090209

.field public static final contactLayout4:I = 0x7f09020a

.field public static final contactLayout5:I = 0x7f09020b

.field public static final contactLayout6:I = 0x7f09020c

.field public static final contact_location_viewpager:I = 0x7f09002c

.field public static final content:I = 0x7f090200

.field public static final content_area:I = 0x7f090068

.field public static final content_layout_1:I = 0x7f09022c

.field public static final content_layout_2:I = 0x7f09022f

.field public static final content_layout_3:I = 0x7f090232

.field public static final content_layout_4:I = 0x7f090235

.field public static final content_layout_5:I = 0x7f090238

.field public static final content_message_pager:I = 0x7f0900f9

.field public static final content_view_pager:I = 0x7f09003c

.field public static final content_view_pager_POI:I = 0x7f0900f8

.field public static final contents:I = 0x7f090007

.field public static final cover:I = 0x7f090311

.field public static final dc_rl_root:I = 0x7f0902bc

.field public static final desc_text1:I = 0x7f09022e

.field public static final desc_text2:I = 0x7f090231

.field public static final desc_text3:I = 0x7f090234

.field public static final desc_text4:I = 0x7f090237

.field public static final desc_text5:I = 0x7f09023a

.field public static final destination_info:I = 0x7f090058

.field public static final destinations_list:I = 0x7f090268

.field public static final disclaimer_text:I = 0x7f09000f

.field public static final disclaimer_title:I = 0x7f09000e

.field public static final displayView:I = 0x7f090256

.field public static final distance:I = 0x7f090312

.field public static final divider:I = 0x7f090129

.field public static final dividerNavigation:I = 0x7f090139

.field public static final divider_1:I = 0x7f0901bd

.field public static final divider_2:I = 0x7f0901c1

.field public static final divider_3:I = 0x7f0901c5

.field public static final drawer_menu_list_item_icon:I = 0x7f0901a5

.field public static final drawer_menu_list_item_name:I = 0x7f0901a6

.field public static final drawer_menu_list_title_name:I = 0x7f0901a7

.field public static final duration_bar_player:I = 0x7f0900d2

.field public static final duration_info:I = 0x7f090045

.field public static final duration_time_info:I = 0x7f090046

.field public static final emulate_location_destination_button:I = 0x7f090263

.field public static final emulate_location_group_button:I = 0x7f090262

.field public static final emulate_location_group_expire_button:I = 0x7f090264

.field public static final emulate_location_requested_button:I = 0x7f090261

.field public static final emulate_location_shared_button:I = 0x7f090260

.field public static final emulate_safereader_message_read_button:I = 0x7f090275

.field public static final emulate_safereader_multiple_messages_button:I = 0x7f090274

.field public static final emulate_safereader_single_message_button:I = 0x7f090273

.field public static final energy:I = 0x7f090251

.field public static final etMessageComposer:I = 0x7f0900bc

.field public static final etSettingsRejectMessage:I = 0x7f09014a

.field public static final etSettingsSearchContacts:I = 0x7f090120

.field public static final et_search:I = 0x7f090035

.field public static final expandable_list:I = 0x7f090069

.field public static final expired_message:I = 0x7f0902e1

.field public static final fmc_main_layout:I = 0x7f090014

.field public static final fragment:I = 0x7f0902fd

.field public static final fragmentMapview:I = 0x7f0902bd

.field public static final fragmentmap_balloon:I = 0x7f090009

.field public static final fragmentmap_balloon_address:I = 0x7f09000b

.field public static final fragmentmap_balloon_time:I = 0x7f09001c

.field public static final fragmentmap_balloon_timestamp:I = 0x7f09000c

.field public static final fragmentmap_balloon_timestamp_end:I = 0x7f09000d

.field public static final fragmentmap_balloon_title:I = 0x7f09000a

.field public static final fragmentmap_container:I = 0x7f090008

.field public static final frame_destination:I = 0x7f090056

.field public static final frame_duration:I = 0x7f090043

.field public static final frame_participants:I = 0x7f09004d

.field public static final front_mark:I = 0x7f09019b

.field public static final group_loc_share_title:I = 0x7f090064

.field public static final grp_loc_particip_title:I = 0x7f090025

.field public static final grp_loc_search:I = 0x7f090034

.field public static final grp_loc_sh_btn_cont:I = 0x7f09005e

.field public static final grp_loc_share_title:I = 0x7f090041

.field public static final gv_joined_participant:I = 0x7f09029e

.field public static final helpMessage:I = 0x7f090073

.field public static final helpitem:I = 0x7f0901f3

.field public static final home_actionbar:I = 0x7f090076

.field public static final home_actionbar_bg:I = 0x7f090315

.field public static final home_actionbar_title:I = 0x7f090017

.field public static final home_button:I = 0x7f09025e

.field public static final home_buttons_layout:I = 0x7f090077

.field public static final home_dialog_layout:I = 0x7f090316

.field public static final home_drawer_bg_frame:I = 0x7f0901a3

.field public static final home_drawer_menu_btn:I = 0x7f090016

.field public static final home_drawer_menu_btn_layout:I = 0x7f090015

.field public static final home_drawer_menu_layout:I = 0x7f090019

.field public static final home_drawer_menu_list:I = 0x7f0901a4

.field public static final home_hi_text:I = 0x7f09031a

.field public static final home_layout:I = 0x7f090075

.field public static final home_listening_quotes_end:I = 0x7f090328

.field public static final home_listening_quotes_start:I = 0x7f090326

.field public static final home_listening_text:I = 0x7f090327

.field public static final home_listening_text_layout:I = 0x7f090325

.field public static final home_map_btn:I = 0x7f090065

.field public static final home_menu_btn:I = 0x7f090066

.field public static final home_message_btn_layout:I = 0x7f09007b

.field public static final home_message_icon:I = 0x7f09007c

.field public static final home_message_name:I = 0x7f09007d

.field public static final home_mic_btn:I = 0x7f090323

.field public static final home_mic_eq:I = 0x7f090320

.field public static final home_mic_layout:I = 0x7f09031f

.field public static final home_mic_process_bg:I = 0x7f090322

.field public static final home_mic_startq_bg:I = 0x7f090321

.field public static final home_more_btn:I = 0x7f09032a

.field public static final home_music_btn_layout:I = 0x7f090081

.field public static final home_music_icon:I = 0x7f090082

.field public static final home_music_name:I = 0x7f090083

.field public static final home_navigation_btn_layout:I = 0x7f09007e

.field public static final home_navigation_icon:I = 0x7f09007f

.field public static final home_navigation_name:I = 0x7f090080

.field public static final home_no_network:I = 0x7f09031d

.field public static final home_no_network_layout:I = 0x7f09031c

.field public static final home_notification_layout_bar:I = 0x7f0902be

.field public static final home_phone_btn_layout:I = 0x7f090078

.field public static final home_phone_icon:I = 0x7f090079

.field public static final home_phone_name:I = 0x7f09007a

.field public static final home_process_text:I = 0x7f090329

.field public static final home_process_timer:I = 0x7f090324

.field public static final home_quotes_end:I = 0x7f09031b

.field public static final home_quotes_start:I = 0x7f090319

.field public static final home_say_text:I = 0x7f090318

.field public static final home_stand_by_layout:I = 0x7f090317

.field public static final home_tts_text:I = 0x7f09031e

.field public static final homelist:I = 0x7f0901c9

.field public static final hybrid:I = 0x7f090004

.field public static final ib_grp_loc_sha_action_bar:I = 0x7f090021

.field public static final icon:I = 0x7f09027d

.field public static final image_bar_vertical:I = 0x7f090020

.field public static final image_no_poi:I = 0x7f0900fa

.field public static final imgContactImage:I = 0x7f0902e0

.field public static final imgQuoteEndWcis:I = 0x7f0901f1

.field public static final imgQuoteStartWcis:I = 0x7f0901ef

.field public static final imgWhatCanISayGrpImg:I = 0x7f0901ed

.field public static final imgWhatCanISayIndicator:I = 0x7f0901f2

.field public static final img_bt_request:I = 0x7f09008f

.field public static final img_bt_share:I = 0x7f09008c

.field public static final img_destination_icon:I = 0x7f090057

.field public static final img_duration_icon:I = 0x7f090044

.field public static final img_participant:I = 0x7f0901fb

.field public static final img_participants_icon:I = 0x7f09004e

.field public static final img_user:I = 0x7f0901a9

.field public static final inboxLayout1:I = 0x7f0901d0

.field public static final inboxLayout2:I = 0x7f0901d1

.field public static final inboxLayout3:I = 0x7f0901d2

.field public static final inboxLayout4:I = 0x7f0901d3

.field public static final inboxListLayout:I = 0x7f0900b0

.field public static final inboxViewPager:I = 0x7f0900b1

.field public static final indicator_actionbar:I = 0x7f09029a

.field public static final indicatorbtn1:I = 0x7f0901b7

.field public static final indicatorbtn1_f:I = 0x7f0901b2

.field public static final indicatorbtn2:I = 0x7f0901b5

.field public static final indicatorbtn2_f:I = 0x7f0901b0

.field public static final indicatorbtn3:I = 0x7f0901b4

.field public static final indicatorbtn3_f:I = 0x7f0901af

.field public static final indicatorbtn4:I = 0x7f0901b6

.field public static final indicatorbtn4_f:I = 0x7f0901b1

.field public static final indicatorbtn5:I = 0x7f0901b8

.field public static final indicatorbtn5_f:I = 0x7f0901b3

.field public static final initial_profile_background:I = 0x7f090182

.field public static final initial_profile_intro_bottom_layout:I = 0x7f090185

.field public static final initial_profile_intro_center_layout:I = 0x7f090183

.field public static final initial_profile_intro_video:I = 0x7f090181

.field public static final intro_next_layout:I = 0x7f090186

.field public static final ivAddContacts:I = 0x7f09015a

.field public static final ivAlbum:I = 0x7f09029b

.field public static final ivBack:I = 0x7f090112

.field public static final ivEqualizer:I = 0x7f09029c

.field public static final ivFavoriteContacts:I = 0x7f090126

.field public static final ivHomeMsgMenu:I = 0x7f0902d6

.field public static final ivHomeMusicMenu:I = 0x7f0902dc

.field public static final ivHomePhoneMenu:I = 0x7f0902d3

.field public static final ivHomeRoutesMenu:I = 0x7f0902d9

.field public static final ivIconCar:I = 0x7f090166

.field public static final ivIndicator:I = 0x7f0902e6

.field public static final ivIndicatorNotAccount:I = 0x7f0902e9

.field public static final ivWaiting:I = 0x7f0900a4

.field public static final iv_call_type_icon:I = 0x7f090214

.field public static final iv_contact_image:I = 0x7f0901f7

.field public static final iv_contact_mask_image:I = 0x7f09028f

.field public static final iv_extra_host:I = 0x7f0901e3

.field public static final iv_extra_web:I = 0x7f0901e2

.field public static final iv_inbox_icon:I = 0x7f0901cf

.field public static final iv_inbox_image:I = 0x7f0901cb

.field public static final iv_inbox_mask_image:I = 0x7f0901cc

.field public static final iv_list_status:I = 0x7f0901ae

.field public static final iv_logs_image:I = 0x7f090210

.field public static final iv_logs_mask_image:I = 0x7f090211

.field public static final iv_logs_type_icon:I = 0x7f090215

.field public static final iv_mask:I = 0x7f090295

.field public static final iv_message_number_image:I = 0x7f09021c

.field public static final iv_message_number_mask_image:I = 0x7f09021d

.field public static final iv_new_item:I = 0x7f090296

.field public static final iv_phone_number_image:I = 0x7f090244

.field public static final iv_phone_number_mask_image:I = 0x7f090245

.field public static final iv_search_music_image:I = 0x7f090283

.field public static final iv_search_music_mask_image:I = 0x7f090284

.field public static final iv_title_icon:I = 0x7f0902aa

.field public static final iv_update_thumb:I = 0x7f09018b

.field public static final iv_user_icon:I = 0x7f0901f4

.field public static final label:I = 0x7f09027c

.field public static final layoutAddContacts:I = 0x7f090158

.field public static final layoutAddContactsBack:I = 0x7f09011d

.field public static final layoutAddFavoriteContacts:I = 0x7f090125

.field public static final layoutAddFavoriteContactsBase:I = 0x7f090124

.field public static final layoutAutoSuggest:I = 0x7f090153

.field public static final layoutBack:I = 0x7f090005

.field public static final layoutBackMain:I = 0x7f090111

.field public static final layoutBasicSettingsNext:I = 0x7f090011

.field public static final layoutBasicSettingsNextBtn:I = 0x7f090012

.field public static final layoutEditSuggestedContactsBack:I = 0x7f090152

.field public static final layoutFavoriteContacts:I = 0x7f09015b

.field public static final layoutHelpBack:I = 0x7f090072

.field public static final layoutHomeButtons:I = 0x7f090084

.field public static final layoutMyCarDefault:I = 0x7f090161

.field public static final layoutMyCarNotConnectedSkipMsg:I = 0x7f090160

.field public static final layoutMyCarUnRegisteredNotConnected:I = 0x7f09015c

.field public static final layoutNavigationBack:I = 0x7f09016c

.field public static final layoutParent:I = 0x7f0900b9

.field public static final layoutRegisterAutoLaunch:I = 0x7f090168

.field public static final layoutRejectButtonCancelOk:I = 0x7f09014c

.field public static final layoutRejectMessageBack:I = 0x7f090148

.field public static final layoutSetFavorite:I = 0x7f090155

.field public static final layoutSettingsAddCar:I = 0x7f090143

.field public static final layoutSettingsIcon:I = 0x7f090250

.field public static final layoutSettingsIndicator:I = 0x7f090028

.field public static final layoutSettingsLanguageBack:I = 0x7f09012b

.field public static final layoutSettingsMainBack:I = 0x7f09012d

.field public static final layoutSettingsMainPreference:I = 0x7f09012f

.field public static final layoutSettingsMyCarAutoLaunch:I = 0x7f090141

.field public static final layoutSettingsRegisteredCars:I = 0x7f090144

.field public static final layoutSettingsRemove:I = 0x7f090147

.field public static final layoutSettingsRename:I = 0x7f090145

.field public static final layoutSetupwizardIndicator:I = 0x7f090313

.field public static final layoutSetupwizardNavigation:I = 0x7f09016b

.field public static final layoutSetupwizardNavigationPreference:I = 0x7f09016d

.field public static final layoutTermsofService1:I = 0x7f090113

.field public static final layoutTermsofServiceAgree1:I = 0x7f090115

.field public static final layoutWhatCanISayBack:I = 0x7f090196

.field public static final layout_1:I = 0x7f0901bc

.field public static final layout_2:I = 0x7f0901c0

.field public static final layout_3:I = 0x7f0901c4

.field public static final layout_4:I = 0x7f0901c8

.field public static final layout_top_area:I = 0x7f0902d0

.field public static final line:I = 0x7f090096

.field public static final linearLayout1:I = 0x7f090203

.field public static final listArea:I = 0x7f09026a

.field public static final list_participants:I = 0x7f090037

.field public static final listview:I = 0x7f09012c

.field public static final llArrivalTime:I = 0x7f0902c1

.field public static final llBottomArea:I = 0x7f0902cb

.field public static final llConnectedBTItem:I = 0x7f090165

.field public static final llDuration:I = 0x7f0902c6

.field public static final llLeftAreaOfSelection:I = 0x7f0902b7

.field public static final llNotiArea:I = 0x7f0902ca

.field public static final llPinInputArea:I = 0x7f0902b9

.field public static final llProcArea:I = 0x7f0902cc

.field public static final llProcHasNoAccount:I = 0x7f0902e8

.field public static final llResultArea:I = 0x7f0902ed

.field public static final llRightAreaOfSelection:I = 0x7f0902b8

.field public static final llSoundControl:I = 0x7f0900dd

.field public static final llTimeInfoLayouts:I = 0x7f0902c9

.field public static final ll_back:I = 0x7f0900f3

.field public static final ll_buttons:I = 0x7f0901e1

.field public static final ll_min:I = 0x7f0901e4

.field public static final ll_root:I = 0x7f09029f

.field public static final ll_server_data_reset_description1:I = 0x7f09014f

.field public static final ll_server_data_reset_description2:I = 0x7f090150

.field public static final location:I = 0x7f090259

.field public static final locationListLayout:I = 0x7f090087

.field public static final locationViewPager:I = 0x7f090089

.field public static final location_actionbar:I = 0x7f090086

.field public static final location_actionbar_title:I = 0x7f090348

.field public static final location_back_btn_image:I = 0x7f090347

.field public static final location_back_btn_layout:I = 0x7f090346

.field public static final location_emulate_button:I = 0x7f090267

.field public static final location_group_image:I = 0x7f0902dd

.field public static final location_hi_galaxy:I = 0x7f09034c

.field public static final location_listening_text:I = 0x7f090343

.field public static final location_mic_btn:I = 0x7f09033f

.field public static final location_mic_eq:I = 0x7f09033c

.field public static final location_mic_layout:I = 0x7f09033b

.field public static final location_mic_process_bg:I = 0x7f09033e

.field public static final location_mic_startq_bg:I = 0x7f09033d

.field public static final location_process_text:I = 0x7f090344

.field public static final location_process_timer:I = 0x7f090340

.field public static final location_request_waiting_actionbar:I = 0x7f0900a3

.field public static final location_search_btn:I = 0x7f090345

.field public static final location_search_result_text:I = 0x7f090332

.field public static final location_share_actionbar:I = 0x7f09009b

.field public static final location_share_button:I = 0x7f090266

.field public static final location_stand_by_layout:I = 0x7f090335

.field public static final location_start_multiwindow_view:I = 0x7f090091

.field public static final location_start_multiwindow_view1:I = 0x7f0900e7

.field public static final location_suggestions_top:I = 0x7f090265

.field public static final location_tts_bar_bg_view:I = 0x7f090342

.field public static final location_tts_bar_layout:I = 0x7f090341

.field public static final location_tts_text:I = 0x7f09033a

.field public static final location_voice_layout:I = 0x7f090334

.field public static final logsLayout1:I = 0x7f090216

.field public static final logsLayout2:I = 0x7f090217

.field public static final logsLayout3:I = 0x7f090218

.field public static final logsLayout4:I = 0x7f090219

.field public static final logsLayout5:I = 0x7f09021a

.field public static final logsLayout6:I = 0x7f09021b

.field public static final logsListLayout:I = 0x7f0900a0

.field public static final logsViewPager:I = 0x7f0900a1

.field public static final logs_list:I = 0x7f09026b

.field public static final lstWhatCanISay:I = 0x7f090197

.field public static final lvFavoriteContacts:I = 0x7f090122

.field public static final lvPointCluster:I = 0x7f0900f7

.field public static final lv_arrived:I = 0x7f090314

.field public static final lv_inbox:I = 0x7f0901ca

.field public static final lv_logs:I = 0x7f09020f

.field public static final lyr_alarm_time:I = 0x7f0902a3

.field public static final lyr_button:I = 0x7f0902ac

.field public static final lyr_contents:I = 0x7f0902ab

.field public static final lyr_description:I = 0x7f0902a6

.field public static final lyr_noti:I = 0x7f0902b0

.field public static final lyr_schedule_contents:I = 0x7f090307

.field public static final lyr_schedule_time:I = 0x7f090305

.field public static final lyr_title:I = 0x7f0902a9

.field public static final lyr_title_seperator:I = 0x7f0902b1

.field public static final main_bar:I = 0x7f0901df

.field public static final mainlistview:I = 0x7f090074

.field public static final map:I = 0x7f0902ef

.field public static final mapview_actionbar:I = 0x7f0900aa

.field public static final maskLess:I = 0x7f0900f5

.field public static final mask_img:I = 0x7f0902fe

.field public static final message:I = 0x7f0901a0

.field public static final messageEditlayout:I = 0x7f0900bb

.field public static final messageNumberLayout1:I = 0x7f090220

.field public static final messageNumberLayout2:I = 0x7f090221

.field public static final messageNumberLayout3:I = 0x7f090222

.field public static final messageNumberLayout4:I = 0x7f090223

.field public static final messageReadView:I = 0x7f0902e5

.field public static final messageReadlayout:I = 0x7f0902e4

.field public static final messageRecvlayout:I = 0x7f0902df

.field public static final message_actionbar:I = 0x7f0900ab

.field public static final message_actionbar_bg:I = 0x7f09034d

.field public static final message_actionbar_title:I = 0x7f090364

.field public static final message_back_btn_image:I = 0x7f090363

.field public static final message_back_btn_layout:I = 0x7f090362

.field public static final message_composer_actionbar:I = 0x7f0900ba

.field public static final message_composer_process_timer:I = 0x7f090365

.field public static final message_dialog_layout:I = 0x7f09034f

.field public static final message_emulate_button:I = 0x7f09026e

.field public static final message_hi_text:I = 0x7f090338

.field public static final message_inbox_button:I = 0x7f09026d

.field public static final message_list_layout:I = 0x7f0900ac

.field public static final message_listening_text:I = 0x7f09035a

.field public static final message_mic_btn:I = 0x7f090356

.field public static final message_mic_eq:I = 0x7f090353

.field public static final message_mic_layout:I = 0x7f090352

.field public static final message_mic_process_bg:I = 0x7f090355

.field public static final message_mic_startq_bg:I = 0x7f090354

.field public static final message_process_text:I = 0x7f09035b

.field public static final message_process_timer:I = 0x7f090357

.field public static final message_quotes_end:I = 0x7f090339

.field public static final message_quotes_start:I = 0x7f090337

.field public static final message_reader_actionbar:I = 0x7f0900c3

.field public static final message_reader_back_btn:I = 0x7f090368

.field public static final message_say_text:I = 0x7f090336

.field public static final message_search_btn:I = 0x7f09035c

.field public static final message_search_clear_btn:I = 0x7f090360

.field public static final message_search_result_text:I = 0x7f090361

.field public static final message_search_text:I = 0x7f09035f

.field public static final message_search_text_layout:I = 0x7f09035d

.field public static final message_send_btn:I = 0x7f090366

.field public static final message_stand_by_layout:I = 0x7f090350

.field public static final message_suggestions_button:I = 0x7f090269

.field public static final message_tts_bar_bg_view:I = 0x7f090359

.field public static final message_tts_bar_layout:I = 0x7f090358

.field public static final message_tts_text:I = 0x7f090351

.field public static final message_voice_layout:I = 0x7f09034e

.field public static final mic:I = 0x7f090252

.field public static final mms:I = 0x7f0902f1

.field public static final mmsbtnlayout:I = 0x7f0902f5

.field public static final more_menu_list_item_name:I = 0x7f09022b

.field public static final msgImgV:I = 0x7f0902d5

.field public static final msgScrollView:I = 0x7f090228

.field public static final msg_no_text_msg_read:I = 0x7f090226

.field public static final msg_scroll_text_msg_read:I = 0x7f090229

.field public static final msg_text_msg_read:I = 0x7f090225

.field public static final multi_layout:I = 0x7f0900c8

.field public static final multi_mic_btn:I = 0x7f090092

.field public static final multi_mic_eq:I = 0x7f09032c

.field public static final multi_mic_layout:I = 0x7f09032b

.field public static final multi_mic_process_bg:I = 0x7f09032e

.field public static final multi_mic_startq_bg:I = 0x7f09032d

.field public static final multi_process_timer:I = 0x7f09032f

.field public static final music:I = 0x7f090258

.field public static final musicImgV:I = 0x7f0902db

.field public static final musicMainListLayout:I = 0x7f0900e1

.field public static final musicMainPageNavigationLayout:I = 0x7f0900e3

.field public static final musicMainViewPager:I = 0x7f0900e2

.field public static final music_actionbar_title:I = 0x7f09037f

.field public static final music_back_btn_image:I = 0x7f09037e

.field public static final music_back_btn_layout:I = 0x7f09037d

.field public static final music_background:I = 0x7f0900ca

.field public static final music_dialog_layout:I = 0x7f09036b

.field public static final music_dim_layout:I = 0x7f0900db

.field public static final music_gradient_background:I = 0x7f0900cc

.field public static final music_hi_text:I = 0x7f09036f

.field public static final music_ingredient_layout:I = 0x7f0900ce

.field public static final music_listening_text:I = 0x7f090379

.field public static final music_mic_btn:I = 0x7f09009a

.field public static final music_mic_btn1:I = 0x7f0900e8

.field public static final music_mic_eq:I = 0x7f090373

.field public static final music_mic_layout:I = 0x7f090372

.field public static final music_mic_process_bg:I = 0x7f090375

.field public static final music_mic_startq_bg:I = 0x7f090374

.field public static final music_player_actionbar:I = 0x7f0900cd

.field public static final music_process_text:I = 0x7f09037a

.field public static final music_process_timer:I = 0x7f090376

.field public static final music_quotes_end:I = 0x7f090370

.field public static final music_quotes_start:I = 0x7f09036e

.field public static final music_say_text:I = 0x7f09036d

.field public static final music_search_actionbar_bg:I = 0x7f090288

.field public static final music_search_btn:I = 0x7f09037b

.field public static final music_search_clear_btn:I = 0x7f09028d

.field public static final music_search_icon_in_edittext:I = 0x7f09037c

.field public static final music_search_icon_in_edittext_searchlist:I = 0x7f09028b

.field public static final music_search_prev_btn:I = 0x7f090289

.field public static final music_search_result_text:I = 0x7f09028e

.field public static final music_search_text:I = 0x7f09028c

.field public static final music_search_text_layout:I = 0x7f09028a

.field public static final music_stand_by_layout:I = 0x7f09036c

.field public static final music_tts_bar_bg_view:I = 0x7f090378

.field public static final music_tts_bar_layout:I = 0x7f090377

.field public static final music_tts_text:I = 0x7f090371

.field public static final music_voice_background:I = 0x7f0900cb

.field public static final music_voice_layout:I = 0x7f09036a

.field public static final name:I = 0x7f0902de

.field public static final negativeButton:I = 0x7f0901a1

.field public static final next_btn_player:I = 0x7f0900d9

.field public static final noListLayout:I = 0x7f09009f

.field public static final noListTextView:I = 0x7f0900ae

.field public static final none:I = 0x7f090000

.field public static final normal:I = 0x7f090001

.field public static final not_invitation_message:I = 0x7f0902e7

.field public static final not_invitation_message_has_not_account:I = 0x7f0902ea

.field public static final not_joined_list:I = 0x7f09006c

.field public static final not_message:I = 0x7f0902eb

.field public static final noti_base:I = 0x7f0902a8

.field public static final noti_location_sent:I = 0x7f0902ec

.field public static final noti_message:I = 0x7f0902cf

.field public static final notification:I = 0x7f0900e9

.field public static final number:I = 0x7f0902f2

.field public static final numberListLayout:I = 0x7f0900f0

.field public static final numberPageNavigationLayout:I = 0x7f0900f2

.field public static final numberViewPager:I = 0x7f0900f1

.field public static final number_text1:I = 0x7f09022d

.field public static final number_text2:I = 0x7f090230

.field public static final number_text3:I = 0x7f090233

.field public static final number_text4:I = 0x7f090236

.field public static final number_text5:I = 0x7f090239

.field public static final option_1:I = 0x7f09039c

.field public static final option_2:I = 0x7f09039d

.field public static final option_3:I = 0x7f09039e

.field public static final option_4:I = 0x7f09039f

.field public static final option_5:I = 0x7f0903a0

.field public static final option_6:I = 0x7f0903a1

.field public static final overlay_carapp_icon:I = 0x7f090241

.field public static final pageNavigationLayout:I = 0x7f09008a

.field public static final parentIconView:I = 0x7f0901eb

.field public static final parentTextLayout:I = 0x7f0901e8

.field public static final parentTextView:I = 0x7f0901e9

.field public static final parent_area:I = 0x7f0901ac

.field public static final parentimageLayout:I = 0x7f0901ea

.field public static final part_container:I = 0x7f090310

.field public static final participants_info:I = 0x7f09004f

.field public static final participants_info_time:I = 0x7f090050

.field public static final phone:I = 0x7f090257

.field public static final phoneImgV:I = 0x7f0902d2

.field public static final phoneNumberLayout1:I = 0x7f090248

.field public static final phoneNumberLayout2:I = 0x7f090249

.field public static final phoneNumberLayout3:I = 0x7f09024a

.field public static final phoneNumberLayout4:I = 0x7f09024b

.field public static final phone_actionbar:I = 0x7f0900ea

.field public static final phone_actionbar_bg:I = 0x7f090380

.field public static final phone_actionbar_title:I = 0x7f09039b

.field public static final phone_back_btn_image:I = 0x7f09039a

.field public static final phone_back_btn_layout:I = 0x7f090399

.field public static final phone_dialog_layout:I = 0x7f090382

.field public static final phone_hi_text:I = 0x7f090386

.field public static final phone_layout_thumbnail:I = 0x7f0900ff

.field public static final phone_list_item:I = 0x7f09025a

.field public static final phone_list_layout:I = 0x7f0900eb

.field public static final phone_listening_text:I = 0x7f090391

.field public static final phone_logs_button:I = 0x7f090272

.field public static final phone_mic_btn:I = 0x7f09038d

.field public static final phone_mic_eq:I = 0x7f09038a

.field public static final phone_mic_layout:I = 0x7f090389

.field public static final phone_mic_process_bg:I = 0x7f09038c

.field public static final phone_mic_startq_bg:I = 0x7f09038b

.field public static final phone_prepare_layout_btn:I = 0x7f09010a

.field public static final phone_prepare_layout_btn_cancel:I = 0x7f09010e

.field public static final phone_prepare_layout_btn_image:I = 0x7f09010b

.field public static final phone_prepare_layout_btn_quote_end:I = 0x7f09010f

.field public static final phone_prepare_layout_btn_quote_start:I = 0x7f09010d

.field public static final phone_prepare_layout_btn_text:I = 0x7f09010c

.field public static final phone_prepare_layout_contact_btn:I = 0x7f090110

.field public static final phone_prepare_layout_contacticon:I = 0x7f090104

.field public static final phone_prepare_layout_contactinfo:I = 0x7f090102

.field public static final phone_prepare_layout_contactname:I = 0x7f090103

.field public static final phone_prepare_layout_contactnumber:I = 0x7f090105

.field public static final phone_prepare_layout_count:I = 0x7f090108

.field public static final phone_prepare_layout_progress:I = 0x7f090107

.field public static final phone_prepare_layout_timer:I = 0x7f090106

.field public static final phone_prepare_outgoing_layout:I = 0x7f090109

.field public static final phone_preparing_text:I = 0x7f090101

.field public static final phone_process_text:I = 0x7f090392

.field public static final phone_process_timer:I = 0x7f09038e

.field public static final phone_quotes_end:I = 0x7f090387

.field public static final phone_quotes_start:I = 0x7f090385

.field public static final phone_say_text:I = 0x7f090384

.field public static final phone_search_btn:I = 0x7f090393

.field public static final phone_search_clear_btn:I = 0x7f090397

.field public static final phone_search_result_text:I = 0x7f090398

.field public static final phone_search_text:I = 0x7f090396

.field public static final phone_search_text_layout:I = 0x7f090394

.field public static final phone_stand_by_layout:I = 0x7f090383

.field public static final phone_suggestions_button:I = 0x7f090271

.field public static final phone_thumbnail:I = 0x7f090100

.field public static final phone_tts_bar_bg_view:I = 0x7f090390

.field public static final phone_tts_bar_layout:I = 0x7f09038f

.field public static final phone_tts_text:I = 0x7f090388

.field public static final phone_voice_layout:I = 0x7f090381

.field public static final place:I = 0x7f0902c0

.field public static final place_list_view:I = 0x7f09003b

.field public static final place_notification:I = 0x7f0902bf

.field public static final place_view_pager:I = 0x7f09003d

.field public static final playingtime_player:I = 0x7f0900d3

.field public static final playpause_btn_player:I = 0x7f0900d8

.field public static final positiveButton:I = 0x7f0901a2

.field public static final prev_btn_player:I = 0x7f0900d7

.field public static final progressBar1:I = 0x7f090194

.field public static final rbAutoSuggest:I = 0x7f090154

.field public static final rbSetFavorite:I = 0x7f090156

.field public static final readName:I = 0x7f0902f9

.field public static final read_btn_msg_read:I = 0x7f090224

.field public static final readerBodyLayout:I = 0x7f0900c4

.field public static final readerBodyViewPager:I = 0x7f0900c5

.field public static final relativeLayout:I = 0x7f090190

.field public static final request_confirm_actionbar:I = 0x7f090093

.field public static final rlEmpty:I = 0x7f0900dc

.field public static final rlItemCluster:I = 0x7f0901d4

.field public static final rlLayout:I = 0x7f0902a1

.field public static final rlMusicControlArea:I = 0x7f0900d5

.field public static final rlMusicLayout:I = 0x7f0900c9

.field public static final rl_baidu_navi:I = 0x7f090177

.field public static final rl_cmcc_navi:I = 0x7f09017d

.field public static final rl_google_navi:I = 0x7f09016e

.field public static final rl_no_results:I = 0x7f09003f

.field public static final rl_olleh_navi:I = 0x7f09017a

.field public static final rl_root_voice_location:I = 0x7f090330

.field public static final rl_tmap_navi:I = 0x7f090174

.field public static final rl_uplus_navi:I = 0x7f090171

.field public static final root_act_location:I = 0x7f090085

.field public static final route_view:I = 0x7f09006a

.field public static final routesImgV:I = 0x7f0902d8

.field public static final satellite:I = 0x7f090002

.field public static final scrollView:I = 0x7f090255

.field public static final scrollingTextArea:I = 0x7f090253

.field public static final searchChoiceListLayout:I = 0x7f0900b6

.field public static final searchChoicePageNavigationLayout:I = 0x7f0900b8

.field public static final searchChoiceViewPager:I = 0x7f0900b7

.field public static final searchLayout1:I = 0x7f090291

.field public static final searchLayout2:I = 0x7f090292

.field public static final searchLayout3:I = 0x7f090293

.field public static final searchLayout4:I = 0x7f090294

.field public static final searchListLayout:I = 0x7f0900ed

.field public static final searchMusicLayout1:I = 0x7f09027f

.field public static final searchMusicLayout2:I = 0x7f090280

.field public static final searchMusicLayout3:I = 0x7f090281

.field public static final searchMusicLayout4:I = 0x7f090282

.field public static final searchPageNavigationLayout:I = 0x7f0900ef

.field public static final searchTypeListLayout:I = 0x7f0900b3

.field public static final searchTypePageNavigationLayout:I = 0x7f0900b5

.field public static final searchTypeViewPager:I = 0x7f0900b4

.field public static final searchViewPager:I = 0x7f0900ee

.field public static final search_actionBar:I = 0x7f0900e0

.field public static final search_bar:I = 0x7f090038

.field public static final search_clear_btn:I = 0x7f09034b

.field public static final search_text:I = 0x7f09034a

.field public static final search_text_layout:I = 0x7f090333

.field public static final separator:I = 0x7f0900e5

.field public static final separator1:I = 0x7f0901db

.field public static final separator2:I = 0x7f0901de

.field public static final server_preference_spinner:I = 0x7f090277

.field public static final server_preference_text:I = 0x7f090276

.field public static final set_address:I = 0x7f09003e

.field public static final settings_item_server_data_reset:I = 0x7f09013c

.field public static final settings_language_summary:I = 0x7f090131

.field public static final settings_layout_debug_settings:I = 0x7f09013f

.field public static final settings_layout_language:I = 0x7f090130

.field public static final settings_layout_my_place:I = 0x7f09013a

.field public static final settings_layout_navigation:I = 0x7f090137

.field public static final settings_layout_register_car:I = 0x7f090132

.field public static final settings_layout_reject_message:I = 0x7f090134

.field public static final settings_layout_server_data_reset:I = 0x7f09013d

.field public static final settings_layout_suggested_contacts:I = 0x7f090136

.field public static final settings_layout_terms_of_use:I = 0x7f090140

.field public static final settings_my_place_sub:I = 0x7f09013b

.field public static final settings_navigation_summary:I = 0x7f090138

.field public static final settings_radio_baidu_navi:I = 0x7f090179

.field public static final settings_radio_cmcc_navi:I = 0x7f09017f

.field public static final settings_radio_google_navi:I = 0x7f090170

.field public static final settings_radio_olleh_maps:I = 0x7f09017c

.field public static final settings_radio_tmap_maps:I = 0x7f090176

.field public static final settings_radio_uplus_navi:I = 0x7f090173

.field public static final settings_register_car_summary:I = 0x7f090133

.field public static final settings_reject_message_summary:I = 0x7f090135

.field public static final settings_server_data_reset_sub:I = 0x7f09013e

.field public static final settings_text_baidu_navi:I = 0x7f090178

.field public static final settings_text_cmcc_navi:I = 0x7f09017e

.field public static final settings_text_google_navi:I = 0x7f09016f

.field public static final settings_text_olleh_maps:I = 0x7f09017b

.field public static final settings_text_tmap_maps:I = 0x7f090175

.field public static final settings_text_uplus_navi:I = 0x7f090172

.field public static final share_image:I = 0x7f0902cd

.field public static final share_map_actionbar:I = 0x7f090180

.field public static final shrink_btn_player:I = 0x7f0902a2

.field public static final shuffle_btn_player:I = 0x7f0900da

.field public static final simulation_info:I = 0x7f09025f

.field public static final sms:I = 0x7f0902f0

.field public static final smsbtnlayout:I = 0x7f0902f3

.field public static final songtitle_player:I = 0x7f0900cf

.field public static final start_button:I = 0x7f09018a

.field public static final suggestionsListLayout:I = 0x7f09009d

.field public static final suggestionsPageNavigationLayout:I = 0x7f0900af

.field public static final suggestionsViewPager:I = 0x7f09009e

.field public static final suggestions_list:I = 0x7f09026f

.field public static final sv_participants:I = 0x7f090067

.field public static final switchRejectMessage:I = 0x7f090149

.field public static final table:I = 0x7f09002d

.field public static final terrain:I = 0x7f090003

.field public static final text:I = 0x7f09026c

.field public static final textView1:I = 0x7f09024c

.field public static final text_bar:I = 0x7f090094

.field public static final text_line_1:I = 0x7f09025b

.field public static final text_line_2:I = 0x7f09025c

.field public static final text_tos_link:I = 0x7f09027b

.field public static final text_tos_top:I = 0x7f09027a

.field public static final thumb_player:I = 0x7f0900d1

.field public static final tip_layout_1:I = 0x7f09023b

.field public static final tip_layout_2:I = 0x7f09023e

.field public static final tip_text_1:I = 0x7f09023d

.field public static final tip_text_2:I = 0x7f090240

.field public static final tip_title_text_1:I = 0x7f09023c

.field public static final tip_title_text_2:I = 0x7f09023f

.field public static final title:I = 0x7f09019f

.field public static final title_layout_1:I = 0x7f0901ba

.field public static final title_layout_2:I = 0x7f0901be

.field public static final title_layout_3:I = 0x7f0901c2

.field public static final title_layout_4:I = 0x7f0901c6

.field public static final title_text:I = 0x7f0901b9

.field public static final title_text_1:I = 0x7f0901bb

.field public static final title_text_2:I = 0x7f0901bf

.field public static final title_text_3:I = 0x7f0901c3

.field public static final title_text_4:I = 0x7f0901c7

.field public static final toast_layout_root:I = 0x7f090298

.field public static final top_area:I = 0x7f09025d

.field public static final top_bar:I = 0x7f090063

.field public static final totaltime_player:I = 0x7f0900d4

.field public static final touringguide_title:I = 0x7f090188

.field public static final touringguide_viewpager:I = 0x7f090189

.field public static final tts:I = 0x7f0902fa

.field public static final tvAddContacts:I = 0x7f090159

.field public static final tvAhead:I = 0x7f09030d

.field public static final tvBackup:I = 0x7f09030e

.field public static final tvCantFind:I = 0x7f09015e

.field public static final tvDescription:I = 0x7f09030c

.field public static final tvFavoriteContacts:I = 0x7f090127

.field public static final tvFavoriteCount:I = 0x7f090157

.field public static final tvLocationSearch:I = 0x7f090349

.field public static final tvMessageComposer:I = 0x7f0900c2

.field public static final tvMessageCount:I = 0x7f09022a

.field public static final tvMessageListTitle:I = 0x7f0900ad

.field public static final tvMessageSearchContacts:I = 0x7f09035e

.field public static final tvMyCarMsg:I = 0x7f090162

.field public static final tvMyCarMsg3:I = 0x7f09015d

.field public static final tvMyCarMsgBasic1:I = 0x7f090163

.field public static final tvMyCarMsgBasic2:I = 0x7f090164

.field public static final tvMyCarName:I = 0x7f090167

.field public static final tvNameItemCluster:I = 0x7f0901d5

.field public static final tvNotext:I = 0x7f090227

.field public static final tvPhoneListTitle:I = 0x7f0900ec

.field public static final tvPhoneSearchContacts:I = 0x7f090395

.field public static final tvRegisterNext:I = 0x7f090187

.field public static final tvSettingsCarId:I = 0x7f09024f

.field public static final tvSettingsCarMacAddress:I = 0x7f09024e

.field public static final tvSettingsCarName:I = 0x7f09024d

.field public static final tvSettingsMainBack:I = 0x7f09012e

.field public static final tvSettingsRenameDeviceName:I = 0x7f090146

.field public static final tvSettingsSearchContacts:I = 0x7f09011f

.field public static final tvTermsofServiceAgree1:I = 0x7f090117

.field public static final tvTermsofServiceBody1:I = 0x7f090114

.field public static final tvText:I = 0x7f0902ce

.field public static final tvTitle:I = 0x7f0900a7

.field public static final tvVolumeInfo:I = 0x7f0900df

.field public static final tvWaiting:I = 0x7f0900a5

.field public static final tv_add:I = 0x7f090071

.field public static final tv_alarm_title:I = 0x7f0902a7

.field public static final tv_am_pm:I = 0x7f0902a5

.field public static final tv_arrival_mode:I = 0x7f0902c8

.field public static final tv_arrival_time:I = 0x7f0902c7

.field public static final tv_arrived:I = 0x7f0901ad

.field public static final tv_arrived_status:I = 0x7f0901ab

.field public static final tv_charger_remain:I = 0x7f0902ba

.field public static final tv_contact_address:I = 0x7f090297

.field public static final tv_contact_name:I = 0x7f0901f8

.field public static final tv_contact_number:I = 0x7f090290

.field public static final tv_des:I = 0x7f09018c

.field public static final tv_des1:I = 0x7f09018e

.field public static final tv_des2:I = 0x7f09018f

.field public static final tv_destination_address:I = 0x7f09005a

.field public static final tv_destination_name:I = 0x7f090059

.field public static final tv_disclaimer:I = 0x7f09002e

.field public static final tv_distance:I = 0x7f0901dd

.field public static final tv_done:I = 0x7f090029

.field public static final tv_duration:I = 0x7f090047

.field public static final tv_duration_min:I = 0x7f090049

.field public static final tv_duration_sub:I = 0x7f09004b

.field public static final tv_duration_unit:I = 0x7f090048

.field public static final tv_duration_unit_min:I = 0x7f09004a

.field public static final tv_group_back:I = 0x7f09001f

.field public static final tv_host:I = 0x7f0901fd

.field public static final tv_hour_label:I = 0x7f0902c3

.field public static final tv_hour_num:I = 0x7f0902c2

.field public static final tv_inbox_name:I = 0x7f0901cd

.field public static final tv_inbox_time:I = 0x7f0901ce

.field public static final tv_info1:I = 0x7f0901da

.field public static final tv_info2:I = 0x7f0901dc

.field public static final tv_label_destination:I = 0x7f090055

.field public static final tv_label_duration:I = 0x7f090042

.field public static final tv_label_participants:I = 0x7f09004c

.field public static final tv_letter_user:I = 0x7f0901aa

.field public static final tv_logs_name:I = 0x7f090212

.field public static final tv_logs_time:I = 0x7f090213

.field public static final tv_message_no_poi:I = 0x7f0900fb

.field public static final tv_message_number:I = 0x7f09021f

.field public static final tv_message_number_name:I = 0x7f09021e

.field public static final tv_message_reader_name:I = 0x7f090369

.field public static final tv_message_reader_time:I = 0x7f090367

.field public static final tv_min_label:I = 0x7f0902c5

.field public static final tv_min_num:I = 0x7f0902c4

.field public static final tv_more:I = 0x7f0902a0

.field public static final tv_name:I = 0x7f090123

.field public static final tv_nav_back:I = 0x7f09001e

.field public static final tv_not_joined:I = 0x7f0901fa

.field public static final tv_not_joined_quant:I = 0x7f09006b

.field public static final tv_not_moving:I = 0x7f0901e6

.field public static final tv_participants_count_invited:I = 0x7f090053

.field public static final tv_participants_count_joined:I = 0x7f090051

.field public static final tv_participants_separator:I = 0x7f090052

.field public static final tv_participants_sub:I = 0x7f090054

.field public static final tv_phone_number:I = 0x7f090247

.field public static final tv_phone_number_name:I = 0x7f090246

.field public static final tv_place_address:I = 0x7f0901d9

.field public static final tv_place_name:I = 0x7f0901d8

.field public static final tv_progress:I = 0x7f090193

.field public static final tv_remain:I = 0x7f090191

.field public static final tv_samsungaccount_email:I = 0x7f09011b

.field public static final tv_samsungaccount_line0:I = 0x7f090118

.field public static final tv_samsungaccount_line1:I = 0x7f090119

.field public static final tv_samsungaccount_line2:I = 0x7f09011a

.field public static final tv_schedule_am_pm:I = 0x7f090306

.field public static final tv_schedule_time:I = 0x7f090303

.field public static final tv_schedule_title:I = 0x7f090304

.field public static final tv_search_music_count_album:I = 0x7f090286

.field public static final tv_search_music_count_song:I = 0x7f090287

.field public static final tv_search_music_name:I = 0x7f090285

.field public static final tv_sharing_info:I = 0x7f090061

.field public static final tv_speed:I = 0x7f090204

.field public static final tv_status:I = 0x7f090195

.field public static final tv_text:I = 0x7f0902bb

.field public static final tv_text_request:I = 0x7f090095

.field public static final tv_time:I = 0x7f0902a4

.field public static final tv_time_left:I = 0x7f0901e5

.field public static final tv_title:I = 0x7f090027

.field public static final tv_total:I = 0x7f090192

.field public static final tv_type:I = 0x7f0900a6

.field public static final tv_update_status:I = 0x7f09002b

.field public static final tv_user_letter:I = 0x7f0901f5

.field public static final tv_user_name:I = 0x7f0901f6

.field public static final tvdistanceAhead:I = 0x7f09030a

.field public static final tvdistanceBackup:I = 0x7f09030b

.field public static final tvimageincident:I = 0x7f090308

.field public static final tvincident:I = 0x7f090309

.field public static final tvminAhead:I = 0x7f09030f

.field public static final txtSearch:I = 0x7f0900e4

.field public static final txtWhatCanISayChild:I = 0x7f0901f9

.field public static final txtWhatCanISayGrpImgSubTitle:I = 0x7f0901f0

.field public static final txtWhatCanISayGrpImgTitle:I = 0x7f0901ee

.field public static final txt_place_address:I = 0x7f0901ff

.field public static final txt_place_name:I = 0x7f0901fe

.field public static final user_area:I = 0x7f0901e0

.field public static final v_intercept_touch:I = 0x7f09006d

.field public static final view_joined_participant:I = 0x7f09006e

.field public static final view_joined_participant_three:I = 0x7f09006f

.field public static final view_user_arrived:I = 0x7f090070

.field public static final voice_dialog_overlay_fragment:I = 0x7f090243

.field public static final voice_dialog_overlay_view:I = 0x7f090242

.field public static final volume_bar_player:I = 0x7f0900de

.field public static final volume_btn_player:I = 0x7f0900d6

.field public static final vwWhatCanISayDivider:I = 0x7f0901ec

.field public static final warning:I = 0x7f090010

.field public static final warning_progressBar:I = 0x7f090013

.field public static final welcome_title:I = 0x7f090184


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
