.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLPhoneNumber.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final TYPE_CAR:I = 0x9

.field public static final TYPE_COMPANY_MAIN:I = 0xa

.field public static final TYPE_FAX_HOME:I = 0x5

.field public static final TYPE_FAX_WORK:I = 0x4

.field public static final TYPE_HOME:I = 0x1

.field public static final TYPE_MAIN:I = 0xc

.field public static final TYPE_MOBILE:I = 0x2

.field public static final TYPE_OTHER:I = 0x7

.field public static final TYPE_OTHER_FAX:I = 0xd

.field public static final TYPE_WORK:I = 0x3

.field public static final TYPE_WORK_MOBILE:I = 0x11

.field private static final serialVersionUID:J = 0x7093f200adc9c7c1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getPhoneNumber()Ljava/lang/String;
.end method

.method public abstract getPhoneType()I
.end method

.method public abstract isMainPhoneNumber()Z
.end method

.method public abstract isMyPhoneNumber()Z
.end method
