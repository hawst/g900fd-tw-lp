.class public interface abstract Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;
.super Ljava/lang/Object;
.source "DriveLinkServiceInterface.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnDriveLinkNotificationListener"
.end annotation


# virtual methods
.method public abstract onNotifyAlarm(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;)V
.end method

.method public abstract onNotifyBTPairingRequest(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;)V
.end method

.method public abstract onNotifyCallState(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo;)V
.end method

.method public abstract onNotifyLocationFailedRequestSent(I)V
.end method

.method public abstract onNotifyLocationGroupDestinatiON_CHANGEdSent()V
.end method

.method public abstract onNotifyLocationGroupDestinationChanged(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
.end method

.method public abstract onNotifyLocationGroupRequestIgnored()V
.end method

.method public abstract onNotifyLocationGroupShareRequest(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
.end method

.method public abstract onNotifyLocationGroupShareRequestIgnored()V
.end method

.method public abstract onNotifyLocationGroupSharingExpired()V
.end method

.method public abstract onNotifyLocationGroupSharingRequestSent()V
.end method

.method public abstract onNotifyLocationGroupSharingStopped(Ljava/lang/String;)V
.end method

.method public abstract onNotifyLocationHasNoSamsungAccount(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;)V
.end method

.method public abstract onNotifyLocationRequest(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Ljava/lang/String;)V
.end method

.method public abstract onNotifyLocationRequestIgnored()V
.end method

.method public abstract onNotifyLocationRequestSent(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
.end method

.method public abstract onNotifyLocationRequestWaiting(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
.end method

.method public abstract onNotifyLocationShare(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Ljava/lang/String;)V
.end method

.method public abstract onNotifyLocationShareEnded()V
.end method

.method public abstract onNotifyLocationShareSent(Ljava/lang/String;)V
.end method

.method public abstract onNotifyLocationSharedIgnored()V
.end method

.method public abstract onNotifyLowBattery(I)V
.end method

.method public abstract onNotifyMirrorLinkSetup()V
.end method

.method public abstract onNotifyMirrorLinkShutDown()V
.end method

.method public abstract onNotifyOutgoingCall()V
.end method

.method public abstract onNotifyReceivedMSG(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;)V
.end method

.method public abstract onNotifySchedule(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;)V
.end method
