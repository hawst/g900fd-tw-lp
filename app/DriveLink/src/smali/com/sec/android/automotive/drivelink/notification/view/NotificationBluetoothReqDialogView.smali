.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationBluetoothReqDialogView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "[NotificationBluetoothReqDialogView]"


# instance fields
.field private mConfirmLayout:Landroid/widget/RelativeLayout;

.field protected mContext:Landroid/content/Context;

.field private mEdit:Landroid/widget/EditText;

.field private mPinLayout:Landroid/widget/RelativeLayout;

.field protected mViewLocal:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 25
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mContext:Landroid/content/Context;

    .line 26
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mViewLocal:Landroid/view/View;

    .line 27
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mPinLayout:Landroid/widget/RelativeLayout;

    .line 28
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mConfirmLayout:Landroid/widget/RelativeLayout;

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mEdit:Landroid/widget/EditText;

    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 25
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mContext:Landroid/content/Context;

    .line 26
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mViewLocal:Landroid/view/View;

    .line 27
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mPinLayout:Landroid/widget/RelativeLayout;

    .line 28
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mConfirmLayout:Landroid/widget/RelativeLayout;

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mEdit:Landroid/widget/EditText;

    .line 43
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 25
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mContext:Landroid/content/Context;

    .line 26
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mViewLocal:Landroid/view/View;

    .line 27
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mPinLayout:Landroid/widget/RelativeLayout;

    .line 28
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mConfirmLayout:Landroid/widget/RelativeLayout;

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mEdit:Landroid/widget/EditText;

    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 51
    return-void
.end method

.method private static convertPinToBytes(Ljava/lang/String;)[B
    .locals 5
    .param p0, "pin"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 196
    if-nez p0, :cond_1

    move-object v0, v2

    .line 209
    :cond_0
    :goto_0
    return-object v0

    .line 201
    :cond_1
    :try_start_0
    const-string/jumbo v3, "UTF8"

    invoke-virtual {p0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 206
    .local v0, "pinBytes":[B
    array-length v3, v0

    if-lez v3, :cond_2

    array-length v3, v0

    const/16 v4, 0x10

    if-le v3, v4, :cond_0

    :cond_2
    move-object v0, v2

    .line 207
    goto :goto_0

    .line 202
    .end local v0    # "pinBytes":[B
    :catch_0
    move-exception v1

    .line 203
    .local v1, "uee":Ljava/io/UnsupportedEncodingException;
    const-string/jumbo v3, "[NotificationBluetoothReqDialogView]"

    const-string/jumbo v4, "UTF8 not supported?!?"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    .line 204
    goto :goto_0
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 54
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mContext:Landroid/content/Context;

    .line 56
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 57
    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 56
    check-cast v1, Landroid/view/LayoutInflater;

    .line 59
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mViewLocal:Landroid/view/View;

    if-nez v3, :cond_3

    .line 60
    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->isMultiMode:Z

    if-eqz v3, :cond_4

    .line 62
    const v3, 0x7f0300a0

    .line 61
    invoke-virtual {v1, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mViewLocal:Landroid/view/View;

    .line 67
    :goto_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mViewLocal:Landroid/view/View;

    .line 68
    const v4, 0x7f0902b2

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 67
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mPinLayout:Landroid/widget/RelativeLayout;

    .line 69
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mViewLocal:Landroid/view/View;

    .line 70
    const v4, 0x7f0902b5

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 69
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mConfirmLayout:Landroid/widget/RelativeLayout;

    .line 72
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->hasNotiItemInfo()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    .line 74
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    .line 73
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;

    .line 76
    .local v0, "btInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;->getBTPairingType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    move-result-object v3

    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;->NOT_SIMPLE_SECURE_PAIRING:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    if-ne v3, v4, :cond_5

    .line 77
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mPinLayout:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_0

    .line 78
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mPinLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 81
    :cond_0
    const v3, 0x7f0902b3

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 82
    .local v2, "tvHelp":Landroid/widget/TextView;
    if-eqz v2, :cond_1

    .line 83
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 84
    const v4, 0x7f0a039a

    new-array v5, v8, [Ljava/lang/Object;

    .line 85
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    .line 83
    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    :cond_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mViewLocal:Landroid/view/View;

    .line 89
    const v4, 0x7f0902b4

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 88
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mEdit:Landroid/widget/EditText;

    .line 90
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    .line 107
    .end local v0    # "btInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;
    .end local v2    # "tvHelp":Landroid/widget/TextView;
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mViewLocal:Landroid/view/View;

    const v4, 0x7f0902b7

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 108
    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mViewLocal:Landroid/view/View;

    const v4, 0x7f0902b8

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 110
    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    :cond_3
    invoke-virtual {p0, v8}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->setUseAutoTimeout(Z)V

    .line 115
    return-void

    .line 65
    :cond_4
    const v3, 0x7f03009f

    .line 64
    invoke-virtual {v1, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mViewLocal:Landroid/view/View;

    goto/16 :goto_0

    .line 92
    .restart local v0    # "btInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;
    :cond_5
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;->getBTPairingType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    move-result-object v3

    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;->SIMPLE_SECURE_PAIRING:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    if-ne v3, v4, :cond_2

    .line 93
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mConfirmLayout:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_6

    .line 94
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mConfirmLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 97
    :cond_6
    const v3, 0x7f0902b6

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 98
    .restart local v2    # "tvHelp":Landroid/widget/TextView;
    if-eqz v2, :cond_2

    .line 99
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 100
    const v4, 0x7f0a039b

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    .line 101
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;->getPassKey()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    .line 99
    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method protected getTTSText()Ljava/lang/String;
    .locals 3

    .prologue
    .line 227
    const-string/jumbo v0, ""

    .line 228
    .local v0, "tts":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a039e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 230
    if-nez v0, :cond_0

    .line 231
    const-string/jumbo v0, ""

    .line 234
    :cond_0
    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 151
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 152
    .local v1, "id":I
    packed-switch v1, :pswitch_data_0

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 155
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->dismiss()V

    goto :goto_0

    .line 158
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->hasNotiItemInfo()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 159
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v5

    .line 160
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    .line 159
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;

    .line 161
    .local v0, "btInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;->getBTPairingType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    move-result-object v5

    sget-object v6, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;->SIMPLE_SECURE_PAIRING:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    if-ne v5, v6, :cond_2

    .line 162
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v5

    .line 163
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mContext:Landroid/content/Context;

    .line 164
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    .line 163
    invoke-interface {v5, v6, v7, v8}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestBluetoothPairingSSPAnswer(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 188
    .end local v0    # "btInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->dismiss()V

    goto :goto_0

    .line 165
    .restart local v0    # "btInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;->getBTPairingType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    move-result-object v5

    sget-object v6, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;->NOT_SIMPLE_SECURE_PAIRING:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    if-ne v5, v6, :cond_1

    .line 166
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mViewLocal:Landroid/view/View;

    .line 167
    const v6, 0x7f0902b4

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 166
    check-cast v3, Landroid/widget/EditText;

    .line 168
    .local v3, "pinEdit":Landroid/widget/EditText;
    if-nez v3, :cond_3

    .line 169
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->dismiss()V

    goto :goto_0

    .line 173
    :cond_3
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 174
    .local v2, "pinCode":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 179
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->convertPinToBytes(Ljava/lang/String;)[B

    move-result-object v4

    .line 180
    .local v4, "pins":[B
    if-eqz v4, :cond_1

    .line 182
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v5

    .line 183
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mContext:Landroid/content/Context;

    .line 184
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v7

    .line 183
    invoke-interface {v5, v6, v7, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestBluetoothPairingNSSPAnswer(Landroid/content/Context;Ljava/lang/String;[B)V

    goto :goto_1

    .line 152
    nop

    :pswitch_data_0
    .packed-switch 0x7f0902b7
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setReadoutFlag(Landroid/content/Context;Z)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "flag"    # Z

    .prologue
    .line 222
    const/4 v0, 0x1

    invoke-super {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->setReadoutFlag(Landroid/content/Context;Z)Z

    move-result v0

    return v0
.end method

.method protected startDefaultNotiTimeoutFlow()V
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x0

    .line 121
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v2

    .line 124
    .local v2, "preference":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v3, "PREF_SETTINGS_READOUT_NOTIFICATION"

    .line 123
    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v1

    .line 128
    .local v1, "globalSettings":Z
    const-string/jumbo v3, "PREF_SETTINGS_READOUT_NOTIFICATION_BT_REQUEST"

    .line 127
    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v0

    .line 131
    .local v0, "btReadout":Z
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addBegin()V

    .line 133
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addAlert()V

    .line 134
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->getTTSText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTS(Ljava/lang/String;)V

    .line 137
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addAlert()V

    .line 140
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->getTTSText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTS(Ljava/lang/String;)V

    .line 143
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 144
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addFinish()V

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationBluetoothReqDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdFlowStart()V

    .line 147
    :cond_2
    return-void
.end method
