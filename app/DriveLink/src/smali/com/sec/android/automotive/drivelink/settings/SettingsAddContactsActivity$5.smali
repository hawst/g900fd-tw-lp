.class Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$5;
.super Ljava/lang/Object;
.source "SettingsAddContactsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    .line 258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 262
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToSave:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$4(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 266
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToDelete:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 270
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->onBackPressed()V

    .line 271
    return-void

    .line 263
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    iget-object v2, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 264
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToSave:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$4(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 263
    invoke-interface {v2, v3, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->addFavoriteContact(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Z

    .line 262
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 267
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    iget-object v2, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 268
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToDelete:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 267
    invoke-interface {v2, v3, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->removeFavoriteContact(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    .line 266
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
