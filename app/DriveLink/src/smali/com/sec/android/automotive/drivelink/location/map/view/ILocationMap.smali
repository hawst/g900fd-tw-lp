.class public interface abstract Lcom/sec/android/automotive/drivelink/location/map/view/ILocationMap;
.super Ljava/lang/Object;
.source "ILocationMap.java"


# virtual methods
.method public abstract addMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V
.end method

.method public abstract getMapFragment()Landroid/support/v4/app/Fragment;
.end method

.method public abstract removeMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V
.end method

.method public abstract setLocationPoint(DD)V
.end method

.method public abstract setZoomLevel(F)V
.end method
