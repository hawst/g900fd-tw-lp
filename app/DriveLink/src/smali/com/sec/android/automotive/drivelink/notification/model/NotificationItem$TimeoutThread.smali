.class Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;
.super Ljava/lang/Thread;
.source "NotificationItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TimeoutThread"
.end annotation


# instance fields
.field public isOn:Z

.field public timeoutHandler:Landroid/os/Handler;

.field public timeoutSec:Ljava/lang/Integer;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 947
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 948
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->isOn:Z

    .line 949
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->timeoutHandler:Landroid/os/Handler;

    .line 950
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->timeoutSec:Ljava/lang/Integer;

    .line 947
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;)V
    .locals 0

    .prologue
    .line 947
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 954
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 955
    const-string/jumbo v3, "[NotificationItem]"

    const-string/jumbo v4, ">>>Timeout Thread Start"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 956
    const/4 v0, 0x0

    .line 957
    .local v0, "cnt":I
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->isOn:Z

    if-nez v3, :cond_2

    .line 977
    :cond_1
    :goto_1
    return-void

    .line 959
    :cond_2
    :try_start_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->interrupted()Z

    move-result v3

    if-nez v3, :cond_1

    .line 962
    const-wide/16 v3, 0x3e8

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 967
    add-int/lit8 v0, v0, 0x1

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->timeoutSec:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 968
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->isOn:Z

    .line 969
    const-string/jumbo v3, "[NotificationItem]"

    const-string/jumbo v4, "<<<Timeout Thread End"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->timeoutHandler:Landroid/os/Handler;

    if-eqz v3, :cond_0

    .line 971
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 972
    .local v2, "msg":Landroid/os/Message;
    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->ACTION_TIMEOUT:I
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$0()I

    move-result v3

    iput v3, v2, Landroid/os/Message;->arg1:I

    .line 973
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$TimeoutThread;->timeoutHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 963
    .end local v2    # "msg":Landroid/os/Message;
    :catch_0
    move-exception v1

    .line 964
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method
