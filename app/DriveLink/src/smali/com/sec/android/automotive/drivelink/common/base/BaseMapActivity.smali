.class public abstract Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;
.source "BaseMapActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;,
        Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceDetailTask;,
        Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;
    }
.end annotation


# static fields
.field private static final MAP_TAG:Ljava/lang/String; = "map_fragment"

.field public static final PARAM_ADDRESS:Ljava/lang/String; = "address"

.field public static final PARAM_BALLON_DISABLE:Ljava/lang/String; = "ballon_disable"

.field public static final PARAM_BITMAP:Ljava/lang/String; = "bitmap"

.field public static final PARAM_CUSTOM_PIN_RES_ID:Ljava/lang/String; = "custom_pin"

.field public static final PARAM_GOTO_NAVIGATION:Ljava/lang/String; = "goto_navigation"

.field public static final PARAM_LATITUDE:Ljava/lang/String; = "latitude"

.field public static final PARAM_LOCATION_TYPE:Ljava/lang/String; = "location_type"

.field public static final PARAM_LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final PARAM_MYLOC_BUTTON_ENABLE:Ljava/lang/String; = "myloc_enable"

.field public static final PARAM_NAME:Ljava/lang/String; = "name"

.field public static final PARAM_TIMESTAMP:Ljava/lang/String; = "timestamp"

.field public static final PARAM_TIMESTAMP_END:Ljava/lang/String; = "timestampEnd"

.field private static final STATE_ADDRESS:Ljava/lang/String; = "address"

.field private static final STATE_BALLON_DISABLE:Ljava/lang/String; = "ballon_disable"

.field private static final STATE_LATITUDE:Ljava/lang/String; = "latitude"

.field private static final STATE_LONGITUDE:Ljava/lang/String; = "longitude"

.field private static final STATE_MYLOC_BUTTON_ENABLE:Ljava/lang/String; = "myloc_enable"

.field private static final STATE_NAME:Ljava/lang/String; = "name"

.field private static final TAG:Ljava/lang/String;

.field private static final TIMEOUT_MY_CURRENT_LOCATION:Ljava/lang/Long;

.field public static final USERINDEX:Ljava/lang/String; = "userIndex"

.field private static final ZOOM_PADDING_NORTH:Ljava/lang/Double;

.field private static final ZOOM_PADDING_SOUTH:Ljava/lang/Double;

.field protected static mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;


# instance fields
.field fragment:Landroid/support/v4/app/Fragment;

.field fragment_manager:Landroid/support/v4/app/FragmentManager;

.field private isShareMyLocation:Z

.field private isViewSharedLocation:Z

.field private loading:Landroid/app/ProgressDialog;

.field private locationListener:Landroid/location/LocationListener;

.field private locationManager:Landroid/location/LocationManager;

.field private mAddress:Ljava/lang/String;

.field private mAddressTask:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;

.field private mBallonDisable:I

.field private mBalloonMarker:Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mButtonMyLocationEnable:Z

.field private mCustomPinResourceId:I

.field private mLocationType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

.field private mMarkPoints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;",
            ">;"
        }
    .end annotation
.end field

.field private mName:Ljava/lang/String;

.field private mPlaceTask:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;

.field private mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

.field private mTimestamp:Ljava/lang/String;

.field private mTimestampEnd:Ljava/lang/String;

.field private mUserIndex:I

.field private final placeService:Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 64
    const-wide v0, 0x3f90624dd2f1a9fcL    # 0.016

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->ZOOM_PADDING_NORTH:Ljava/lang/Double;

    .line 66
    const-wide v0, 0x3f86872b020c49baL    # 0.011

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->ZOOM_PADDING_SOUTH:Ljava/lang/Double;

    .line 68
    const-wide/16 v0, 0x7530

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->TIMEOUT_MY_CURRENT_LOCATION:Ljava/lang/Long;

    .line 70
    const-class v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->TAG:Ljava/lang/String;

    .line 111
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;-><init>()V

    .line 75
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mName:Ljava/lang/String;

    .line 76
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .line 77
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBalloonMarker:Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .line 78
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddress:Ljava/lang/String;

    .line 79
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mTimestamp:Ljava/lang/String;

    .line 80
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mTimestampEnd:Ljava/lang/String;

    .line 82
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/places/PlaceServiceFactory;->getPlaceService()Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->placeService:Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;

    .line 83
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBitmap:Landroid/graphics/Bitmap;

    .line 84
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->isShareMyLocation:Z

    .line 85
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->isViewSharedLocation:Z

    .line 86
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddressTask:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;

    .line 87
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPlaceTask:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;

    .line 113
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->fragment_manager:Landroid/support/v4/app/FragmentManager;

    .line 114
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->fragment:Landroid/support/v4/app/Fragment;

    .line 116
    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBallonDisable:I

    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mButtonMyLocationEnable:Z

    .line 119
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mCustomPinResourceId:I

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mMarkPoints:Ljava/util/List;

    .line 121
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->loading:Landroid/app/ProgressDialog;

    .line 62
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->placeService:Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBalloonMarker:Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;I)V
    .locals 0

    .prologue
    .line 116
    iput p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBallonDisable:I

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Z)V
    .locals 0

    .prologue
    .line 117
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mButtonMyLocationEnable:Z

    return-void
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;I)V
    .locals 0

    .prologue
    .line 119
    iput p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mCustomPinResourceId:I

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;I)V
    .locals 0

    .prologue
    .line 88
    iput p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mUserIndex:I

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->isShareMyLocation:Z

    return v0
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$16()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->TIMEOUT_MY_CURRENT_LOCATION:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1152
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->hasAddress(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$18(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->loading:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$19(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)V
    .locals 0

    .prologue
    .line 1143
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->adjustLandscapePosition()V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBalloonMarker:Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddress:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mUserIndex:I

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddress:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mTimestamp:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mTimestampEnd:Ljava/lang/String;

    return-void
.end method

.method private adjustLandscapePosition()V
    .locals 4

    .prologue
    .line 1145
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getPoint()Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v0

    .line 1147
    .local v0, "latitude":D
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    sget-object v2, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->ZOOM_PADDING_SOUTH:Ljava/lang/Double;

    :goto_0
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    sub-double/2addr v0, v2

    .line 1149
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getPoint()Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->setZoomMark(DD)V

    .line 1150
    return-void

    .line 1147
    :cond_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->ZOOM_PADDING_NORTH:Ljava/lang/Double;

    goto :goto_0
.end method

.method private formatTimestamp(Ljava/util/Date;)Ljava/lang/String;
    .locals 5
    .param p1, "timestamp"    # Ljava/util/Date;

    .prologue
    .line 804
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 805
    .local v0, "currentDate":Ljava/util/Date;
    const/4 v2, 0x0

    .line 808
    .local v2, "timestampString":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/Date;->getDay()I

    move-result v3

    invoke-virtual {v0}, Ljava/util/Date;->getDay()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 809
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "hh:mm a"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 814
    .local v1, "dateFormat":Ljava/text/SimpleDateFormat;
    :goto_0
    invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 816
    return-object v2

    .line 811
    .end local v1    # "dateFormat":Ljava/text/SimpleDateFormat;
    :cond_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "dd/MM/yyyy hh:mm a"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .restart local v1    # "dateFormat":Ljava/text/SimpleDateFormat;
    goto :goto_0
.end method

.method private hasAddress(Ljava/lang/String;)Z
    .locals 1
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 1153
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddress:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private markAlternativeLocation(Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;)V
    .locals 7
    .param p1, "point"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .prologue
    .line 525
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 526
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "markAlternativeLocation: Invalid Parameter(s)"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    :goto_0
    return-void

    .line 530
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    .line 531
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v3

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setLocationPoint(DD)V

    .line 532
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->setMarkPin(DD)V

    .line 534
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddressTask:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;

    if-eqz v0, :cond_2

    .line 535
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddressTask:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->cancel(Z)Z

    .line 537
    :cond_2
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v2

    .line 538
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v4

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;-><init>(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;DDLjava/lang/String;)V

    .line 537
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddressTask:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;

    .line 539
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddressTask:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private setMapInstance(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 373
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->fragment:Landroid/support/v4/app/Fragment;

    .line 374
    const-string/jumbo v2, "myloc_enable"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 373
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/location/map/MapFactory;->newLocationMap(Landroid/support/v4/app/Fragment;Z)Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    move-result-object v1

    sput-object v1, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    .line 376
    move-object v0, p1

    .line 378
    .local v0, "temp":Landroid/os/Bundle;
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    new-instance v2, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setMapCompletedListener(Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMapCreatedListener;)V

    .line 402
    return-void
.end method

.method private setNewMapInstance()V
    .locals 9

    .prologue
    const-wide v7, 0x40c3878000000000L    # 9999.0

    const-wide/16 v5, 0x0

    const/4 v4, 0x0

    .line 407
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 409
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 412
    const-string/jumbo v2, "ballon_disable"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 411
    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBallonDisable:I

    .line 414
    const-string/jumbo v2, "myloc_enable"

    .line 413
    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mButtonMyLocationEnable:Z

    .line 416
    const-string/jumbo v2, "custom_pin"

    const/4 v3, -0x1

    .line 415
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mCustomPinResourceId:I

    .line 417
    const-string/jumbo v2, "userIndex"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mUserIndex:I

    .line 423
    :cond_0
    :try_start_0
    const-string/jumbo v2, "VAC_CLIENT_DM"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 467
    :goto_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v2

    cmpl-double v2, v2, v5

    if-nez v2, :cond_1

    .line 468
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v2

    cmpl-double v2, v2, v5

    if-nez v2, :cond_1

    .line 469
    new-instance v2, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .line 470
    invoke-direct {v2, v7, v8, v7, v8}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;-><init>(DD)V

    .line 469
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .line 472
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddress:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 473
    const-string/jumbo v2, ""

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddress:Ljava/lang/String;

    .line 475
    :cond_2
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mButtonMyLocationEnable:Z

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/map/MapFactory;->newLocationMap(Z)Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    move-result-object v2

    sput-object v2, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    .line 477
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    new-instance v3, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setMapCompletedListener(Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMapCreatedListener;)V

    .line 516
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->fragment_manager:Landroid/support/v4/app/FragmentManager;

    .line 517
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 518
    const v3, 0x7f090008

    .line 519
    sget-object v4, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->getMapFragment()Landroid/support/v4/app/Fragment;

    move-result-object v4

    const-string/jumbo v5, "map_fragment"

    .line 518
    invoke-virtual {v2, v3, v4, v5}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 519
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 521
    return-void

    .line 460
    :catch_0
    move-exception v1

    .line 461
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->TAG:Ljava/lang/String;

    .line 462
    const-string/jumbo v3, "setNewMapInstance: couldnt find the parameters to draw map, we are on settings"

    .line 461
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Error message: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private tryParsePref(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 569
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->isShareMyLocation:Z

    if-nez v0, :cond_3

    .line 570
    if-nez p1, :cond_0

    .line 574
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 575
    const v1, 0x7f02024a

    .line 574
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBitmap:Landroid/graphics/Bitmap;

    .line 623
    :goto_0
    return-void

    .line 579
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 581
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 582
    const v1, 0x7f02024c

    .line 581
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 586
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 588
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 589
    const v1, 0x7f020131

    .line 588
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 592
    :cond_2
    const-string/jumbo v0, "VAC_CLIENT_DM"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    goto :goto_0

    .line 607
    :cond_3
    const-string/jumbo v0, "VAC_CLIENT_DM"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    goto :goto_0
.end method


# virtual methods
.method public addCustomMark(DDI)V
    .locals 6
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D
    .param p5, "customPinResourceId"    # I

    .prologue
    .line 641
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    move-wide v1, p1

    move-wide v3, p3

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;-><init>(DDI)V

    .line 643
    .local v0, "item":Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mMarkPoints:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 644
    return-void
.end method

.method public changeMainMarkLocation(DDI)V
    .locals 1
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D
    .param p5, "pinResourceId"    # I

    .prologue
    .line 698
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    if-eqz v0, :cond_0

    .line 699
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->setLatitude(D)V

    .line 700
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v0, p3, p4}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->setLongitude(D)V

    .line 702
    :cond_0
    return-void
.end method

.method public clearAllCustomMarks()V
    .locals 3

    .prologue
    .line 652
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mMarkPoints:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 655
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mMarkPoints:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 656
    return-void

    .line 652
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .line 653
    .local v0, "markPoint":Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    invoke-virtual {v2, v0}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->removeMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V

    goto :goto_0
.end method

.method public clearAllMarks()V
    .locals 1

    .prologue
    .line 672
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mMarkPoints:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 673
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    if-eqz v0, :cond_0

    .line 674
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->clearMarkers()V

    .line 675
    :cond_0
    return-void
.end method

.method protected createAllCustomMarks()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f000000    # 0.5f

    .line 662
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mMarkPoints:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 666
    return-void

    .line 662
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .line 663
    .local v0, "marker":Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    invoke-virtual {v0, v3, v3}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->setAnchor(FF)V

    .line 664
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    invoke-virtual {v2, v0}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->addMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V

    goto :goto_0
.end method

.method public createMarks(DD)V
    .locals 6
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 714
    const/4 v5, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->createMarks(DDLjava/lang/String;)V

    .line 715
    return-void
.end method

.method public createMarks(DDLjava/lang/String;)V
    .locals 7
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D
    .param p5, "pAddress"    # Ljava/lang/String;

    .prologue
    .line 707
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->setMarkPin(DD)V

    .line 708
    invoke-virtual/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->setMarkBallon(DDLjava/lang/String;)V

    .line 709
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    const v1, 0x3f19999a    # 0.6f

    iget v6, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mUserIndex:I

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setZoomLevel(FDDI)V

    .line 710
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->createAllCustomMarks()V

    .line 711
    return-void
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddress:Ljava/lang/String;

    return-object v0
.end method

.method protected abstract getLayoutId()I
.end method

.method public getLocationType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPoint()Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    return-object v0
.end method

.method public getUserIndex()I
    .locals 1

    .prologue
    .line 355
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mUserIndex:I

    return v0
.end method

.method public isGoogleMapsInstalled()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 560
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 561
    const-string/jumbo v3, "com.google.android.apps.maps"

    const/4 v4, 0x0

    .line 560
    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 562
    const/4 v1, 0x1

    .line 564
    :goto_0
    return v1

    .line 563
    :catch_0
    move-exception v0

    .line 564
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method public isMobileDataEnabled()Ljava/lang/Boolean;
    .locals 7

    .prologue
    .line 544
    const-string/jumbo v5, "connectivity"

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .local v2, "connectivityService":Ljava/lang/Object;
    move-object v1, v2

    .line 545
    check-cast v1, Landroid/net/ConnectivityManager;

    .line 548
    .local v1, "cm":Landroid/net/ConnectivityManager;
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 549
    .local v0, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v5, "getMobileDataEnabled"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v0, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 550
    .local v4, "m":Ljava/lang/reflect/Method;
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 551
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v4, v1, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 554
    .end local v0    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "m":Ljava/lang/reflect/Method;
    :goto_0
    return-object v5

    .line 552
    :catch_0
    move-exception v3

    .line 553
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 554
    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 129
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v3

    .line 130
    const-string/jumbo v4, "PREF_SETTINGS_MY_NAVIGATION"

    invoke-virtual {v3, v4, v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v2

    .line 132
    .local v2, "mapID":I
    sget v3, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_CMCC_NAVI:I

    if-eq v2, v3, :cond_0

    .line 133
    sget v3, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_BAIDU_NAVI:I

    if-ne v2, v3, :cond_5

    .line 134
    :cond_0
    const/4 v3, 0x0

    invoke-super {p0, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onCreate(Landroid/os/Bundle;)V

    .line 139
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getLayoutId()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->setContentView(I)V

    .line 142
    sget-boolean v3, Lcom/sec/android/automotive/drivelink/DLApplication;->DL_DEMO:Z

    if-eqz v3, :cond_6

    .line 143
    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->setRequestedOrientation(I)V

    .line 158
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->isGoogleMapsInstalled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 159
    sget v3, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_BAIDU_NAVI:I

    if-eq v2, v3, :cond_1

    .line 160
    sget v3, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_CMCC_NAVI:I

    if-eq v2, v3, :cond_1

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->finish()V

    .line 167
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 168
    const v4, 0x7f0a0324

    .line 167
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 169
    const/4 v4, 0x1

    .line 165
    invoke-static {p0, v3, v4}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v3

    .line 169
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 179
    :cond_1
    const-string/jumbo v3, "VAC_CLIENT_DM"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 182
    const-string/jumbo v3, "DM_LOCATION_SHARE_CONFIRM"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v1

    .line 184
    .local v1, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->isFlowManagerIntent(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_7

    if-eqz v1, :cond_7

    .line 185
    iget-boolean v3, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mIsShareMyLocation:Z

    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->isShareMyLocation:Z

    .line 193
    .end local v1    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_2
    :goto_2
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 194
    const-string/jumbo v4, "isViewSharedLocation"

    const/4 v5, 0x0

    .line 193
    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->isViewSharedLocation:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "key_title"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mName:Ljava/lang/String;

    .line 216
    const-string/jumbo v3, "VAC_CLIENT_DM"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 218
    const-string/jumbo v3, "DM_LOCATION_SHARE_CONFIRM"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v1

    .line 219
    .restart local v1    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->isFlowManagerIntent(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_8

    if-eqz v1, :cond_8

    .line 220
    iget v3, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserIndex:I

    iput v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mUserIndex:I

    .line 221
    iget v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mUserIndex:I

    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->tryParsePref(I)V

    .line 251
    .end local v1    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_3
    :goto_4
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->fragment_manager:Landroid/support/v4/app/FragmentManager;

    .line 252
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->fragment_manager:Landroid/support/v4/app/FragmentManager;

    const-string/jumbo v4, "map_fragment"

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->fragment:Landroid/support/v4/app/Fragment;

    .line 254
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->fragment:Landroid/support/v4/app/Fragment;

    if-eqz v3, :cond_4

    if-nez p1, :cond_9

    .line 255
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->setNewMapInstance()V

    .line 274
    :goto_5
    sget-object v3, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    .line 275
    new-instance v4, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$2;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setDragAndDropListener(Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMarkerDragDropListener;)V

    .line 296
    return-void

    .line 136
    :cond_5
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onCreate(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 145
    :cond_6
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v3

    .line 147
    new-instance v4, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$1;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)V

    .line 146
    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->setOnNotificationMapChangeListener(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;)V

    goto/16 :goto_1

    .line 187
    .restart local v1    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 188
    const-string/jumbo v4, "isShareMyLocation"

    .line 187
    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->isShareMyLocation:Z

    goto/16 :goto_2

    .line 195
    .end local v1    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :catch_0
    move-exception v0

    .line 196
    .local v0, "e":Ljava/lang/Exception;
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->isViewSharedLocation:Z

    goto :goto_3

    .line 223
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "userIndex"

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mUserIndex:I

    .line 224
    iget v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mUserIndex:I

    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->tryParsePref(I)V

    goto :goto_4

    .line 257
    .end local v1    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_9
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->setMapInstance(Landroid/os/Bundle;)V

    goto :goto_5
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 852
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onDestroy()V

    .line 853
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setDragAndDropListener(Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMarkerDragDropListener;)V

    .line 854
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 855
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->setOnNotificationMapChangeListener(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;)V

    .line 857
    return-void
.end method

.method protected onResponseRequestGeolocation(Landroid/location/Location;Z)V
    .locals 7
    .param p1, "location"    # Landroid/location/Location;
    .param p2, "timeout"    # Z

    .prologue
    .line 1160
    if-nez p2, :cond_0

    if-nez p1, :cond_2

    .line 1161
    :cond_0
    const-string/jumbo v0, "Fail to get current location"

    .line 1162
    const/4 v1, 0x1

    .line 1161
    invoke-static {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    .line 1162
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1163
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "Fail to get current location"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1183
    :cond_1
    :goto_0
    return-void

    .line 1166
    :cond_2
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    .line 1167
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;-><init>(DD)V

    .line 1166
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .line 1168
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->isShareMyLocation:Z

    if-eqz v0, :cond_3

    .line 1169
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->markAlternativeLocation(Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;)V

    .line 1178
    :goto_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->loading:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 1179
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->loading:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0

    .line 1170
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddress:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->hasAddress(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1171
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v2

    .line 1172
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v4

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddress:Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;-><init>(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;DDLjava/lang/String;)V

    .line 1171
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPlaceTask:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;

    .line 1173
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPlaceTask:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$PlaceSearchTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    .line 1175
    :cond_4
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->createMarks(DD)V

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 861
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBalloonMarker:Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    if-eqz v0, :cond_0

    .line 862
    const-string/jumbo v0, "latitude"

    .line 863
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBalloonMarker:Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLatitude()D

    move-result-wide v1

    .line 862
    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 864
    const-string/jumbo v0, "longitude"

    .line 865
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBalloonMarker:Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLongitude()D

    move-result-wide v1

    .line 864
    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 868
    :cond_0
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBallonDisable:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    if-eqz v0, :cond_1

    .line 869
    const-string/jumbo v0, "latitude"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 871
    const-string/jumbo v0, "longitude"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 874
    :cond_1
    const-string/jumbo v0, "address"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddress:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    const-string/jumbo v0, "name"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    const-string/jumbo v0, "ballon_disable"

    iget v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBallonDisable:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 877
    const-string/jumbo v0, "myloc_enable"

    .line 878
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mButtonMyLocationEnable:Z

    .line 877
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 880
    const-string/jumbo v0, "custom_pin"

    iget v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mCustomPinResourceId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 881
    const-string/jumbo v0, "timestamp"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mTimestamp:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 882
    const-string/jumbo v0, "timestampEnd"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mTimestampEnd:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 884
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 885
    return-void
.end method

.method public saveLocationPreference()V
    .locals 0

    .prologue
    .line 976
    return-void
.end method

.method protected setCustomZoom()V
    .locals 1

    .prologue
    .line 1120
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->setCustomZoom(Z)V

    .line 1121
    return-void
.end method

.method protected setCustomZoom(Z)V
    .locals 4
    .param p1, "fromClick"    # Z

    .prologue
    .line 1124
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getPoint()Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1126
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getPoint()Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getPoint()Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->setZoomMark(DD)V

    .line 1127
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1128
    if-eqz p1, :cond_1

    .line 1129
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->adjustLandscapePosition()V

    .line 1141
    :cond_0
    :goto_0
    return-void

    .line 1132
    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)V

    .line 1137
    const-wide/16 v2, 0x1f4

    .line 1132
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public setMarkBallon(DD)V
    .locals 6
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 744
    const/4 v5, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->setMarkBallon(DDLjava/lang/String;)V

    .line 745
    return-void
.end method

.method public setMarkBallon(DDLjava/lang/String;)V
    .locals 10
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D
    .param p5, "pAddress"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v9, 0x0

    const v8, 0x7f0a02f8

    .line 749
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBallonDisable:I

    if-ne v0, v1, :cond_0

    .line 800
    :goto_0
    return-void

    .line 752
    :cond_0
    if-eqz p5, :cond_4

    const-string/jumbo v0, ""

    if-eq p5, v0, :cond_4

    .line 753
    iput-object p5, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddress:Ljava/lang/String;

    .line 768
    :goto_1
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    const/4 v5, -0x1

    move-wide v1, p1

    move-wide v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;-><init>(DDI)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBalloonMarker:Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .line 771
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mName:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mName:Ljava/lang/String;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 772
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a02f8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 778
    :cond_2
    :goto_2
    :try_start_1
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->isViewSharedLocation:Z

    if-eqz v0, :cond_3

    .line 779
    const-string/jumbo v0, "VAC_CLIENT_DM"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 796
    :cond_3
    :goto_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBalloonMarker:Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mName:Ljava/lang/String;

    .line 797
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddress:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mTimestamp:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mTimestampEnd:Ljava/lang/String;

    move-object v0, p0

    move v6, v9

    .line 796
    invoke-static/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->drawBitmapBalloonToMarker(Landroid/app/Activity;Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 799
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBalloonMarker:Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->addMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V

    goto :goto_0

    .line 755
    :cond_4
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddressTask:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;

    if-eqz v0, :cond_5

    .line 756
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddressTask:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->cancel(Z)Z

    .line 758
    :cond_5
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;-><init>(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;DDLjava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddressTask:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;

    .line 759
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mAddressTask:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;

    new-array v1, v9, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    .line 773
    :catch_0
    move-exception v7

    .line 774
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mName:Ljava/lang/String;

    goto :goto_2

    .line 792
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v7

    .line 793
    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mName:Ljava/lang/String;

    goto :goto_3
.end method

.method public setMarkPin(DD)V
    .locals 9
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    const/4 v5, -0x1

    .line 718
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->clearMarkers()V

    .line 723
    const/4 v8, 0x0

    .line 724
    .local v8, "imageov":Landroid/graphics/Bitmap;
    iget v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mCustomPinResourceId:I

    if-eq v1, v5, :cond_0

    .line 725
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 726
    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mCustomPinResourceId:I

    .line 725
    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 729
    :cond_0
    if-nez v8, :cond_1

    .line 730
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 731
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBitmap:Landroid/graphics/Bitmap;

    .line 730
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->createBitmapMarkerFromIcon(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 734
    :cond_1
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    move-wide v1, p1

    move-wide v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;-><init>(DDI)V

    .line 736
    .local v0, "marker":Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    const/high16 v1, 0x3f000000    # 0.5f

    const v2, 0x3f5c28f6    # 0.86f

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->setAnchor(FF)V

    .line 737
    invoke-virtual {v0, v8}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->setIcon(Landroid/graphics/Bitmap;)V

    .line 739
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->addMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V

    .line 740
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    const v2, 0x3f19999a    # 0.6f

    iget v7, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mUserIndex:I

    move-wide v3, p1

    move-wide v5, p3

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setZoomLevel(FDDI)V

    .line 741
    return-void
.end method

.method public setMarkerDraggable(Z)V
    .locals 1
    .param p1, "v"    # Z

    .prologue
    .line 367
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setIsDraggable(Z)V

    .line 368
    return-void
.end method

.method public setZoomMark(DD)V
    .locals 7
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 684
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    if-nez v0, :cond_0

    .line 687
    :goto_0
    return-void

    .line 686
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    const v1, 0x3f19999a    # 0.6f

    iget v6, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mUserIndex:I

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setZoomLevel(FDDI)V

    goto :goto_0
.end method

.method public turnGPSOff()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 341
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->locationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->locationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->locationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 344
    :cond_0
    return-void
.end method

.method public turnGPSOn()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 332
    return-void
.end method
