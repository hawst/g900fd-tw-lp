.class public Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;
.super Landroid/widget/RelativeLayout;
.source "HourGlassView.java"


# static fields
.field private static CONTENT_HEIGHT:F

.field private static CONTENT_WIDTH:F


# instance fields
.field private mController:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;

.field private mSandDropView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

.field private mSandLowerView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

.field private mSandUpperView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/high16 v0, 0x42080000    # 34.0f

    sput v0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->CONTENT_WIDTH:F

    .line 17
    const/high16 v0, 0x421c0000    # 39.0f

    sput v0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->CONTENT_HEIGHT:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 27
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->init()V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->init()V

    .line 33
    return-void
.end method

.method private addDropSand()Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;
    .locals 4

    .prologue
    .line 89
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->CONTENT_WIDTH:F

    .line 90
    sget v3, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->CONTENT_HEIGHT:F

    .line 89
    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;-><init>(Landroid/content/Context;FF)V

    .line 92
    .local v0, "view":Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandDropView;
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->addView(Landroid/view/View;)V

    .line 94
    return-object v0
.end method

.method private addFrame()Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;
    .locals 4

    .prologue
    .line 59
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/ClockFrameView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->CONTENT_WIDTH:F

    .line 60
    sget v3, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->CONTENT_HEIGHT:F

    .line 59
    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/ClockFrameView;-><init>(Landroid/content/Context;FF)V

    .line 62
    .local v0, "view":Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/ClockFrameView;
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->addView(Landroid/view/View;)V

    .line 64
    return-object v0
.end method

.method private addLowerSand()Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;
    .locals 4

    .prologue
    .line 79
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandLowerView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->CONTENT_WIDTH:F

    .line 80
    sget v3, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->CONTENT_HEIGHT:F

    .line 79
    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandLowerView;-><init>(Landroid/content/Context;FF)V

    .line 82
    .local v0, "view":Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandLowerView;
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->addView(Landroid/view/View;)V

    .line 84
    return-object v0
.end method

.method private addUpperSand()Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;
    .locals 4

    .prologue
    .line 69
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->CONTENT_WIDTH:F

    .line 70
    sget v3, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->CONTENT_HEIGHT:F

    .line 69
    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;-><init>(Landroid/content/Context;FF)V

    .line 72
    .local v0, "view":Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/SandUpperView;
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->addView(Landroid/view/View;)V

    .line 74
    return-object v0
.end method

.method private init()V
    .locals 4

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->addFrame()Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    .line 38
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->addUpperSand()Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->mSandUpperView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    .line 39
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->addLowerSand()Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->mSandLowerView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    .line 40
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->addDropSand()Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->mSandDropView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    .line 42
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->mSandUpperView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->mSandLowerView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    .line 43
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->mSandDropView:Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;-><init>(Landroid/view/View;Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;)V

    .line 42
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->mController:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;

    .line 44
    return-void
.end method


# virtual methods
.method public play(I)V
    .locals 1
    .param p1, "startDelay"    # I

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->mController:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->stop()V

    .line 49
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->mController:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->play(I)V

    .line 50
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->mController:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->stop()V

    .line 55
    return-void
.end method
