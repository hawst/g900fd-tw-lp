.class Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;
.super Landroid/widget/BaseAdapter;
.source "SettingsAddContactsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ContactsListAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private defaultImage:Landroid/graphics/Bitmap;

.field private mHandler:Landroid/os/Handler;

.field private mImageLoader:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;

.field private mOnImageLoadEventListener:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$OnImageLoadEventListener;

.field private prevImageView:Landroid/widget/ImageView;

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Landroid/content/Context;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 458
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 452
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->mImageLoader:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;

    .line 454
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->mHandler:Landroid/os/Handler;

    .line 455
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->prevImageView:Landroid/widget/ImageView;

    .line 456
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->defaultImage:Landroid/graphics/Bitmap;

    .line 738
    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$1;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->mOnImageLoadEventListener:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$OnImageLoadEventListener;

    .line 459
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->context:Landroid/content/Context;

    .line 460
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->mHandler:Landroid/os/Handler;

    .line 462
    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;

    .line 463
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->mOnImageLoadEventListener:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$OnImageLoadEventListener;

    invoke-direct {v0, p2, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$OnImageLoadEventListener;)V

    .line 462
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->mImageLoader:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;

    .line 464
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->mImageLoader:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->start()V

    .line 465
    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->mImageLoader:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    return-object v0
.end method

.method private buildHeaderRow(Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;Landroid/view/View;)Landroid/view/View;
    .locals 9
    .param p1, "item"    # Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;
    .param p2, "convertView"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x0

    .line 543
    const/4 v4, -0x1

    .line 545
    .local v4, "type":I
    if-eqz p2, :cond_0

    .line 546
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;

    .line 547
    .local v0, "checkHolder":Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;
    iget v4, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->type:I

    .line 550
    .end local v0    # "checkHolder":Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;
    :cond_0
    if-eqz p2, :cond_1

    const/16 v5, 0x65

    if-ne v4, v5, :cond_2

    .line 551
    :cond_1
    const-string/jumbo v5, "[SettingsAddContactsActivity]"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "buildHeaderRow: Invalid view"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 552
    const-string/jumbo v7, " or type:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 551
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->context:Landroid/content/Context;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 556
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f03002a

    .line 555
    invoke-virtual {v2, v5, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 558
    .local v3, "row":Landroid/view/View;
    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;

    invoke-direct {v1, p0, v8}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;)V

    .line 561
    .local v1, "holder":Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;
    const v5, 0x7f090123

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 560
    iput-object v5, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->tvFavoriteContacts:Landroid/widget/TextView;

    .line 562
    const/16 v5, 0x64

    iput v5, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->type:I

    .line 563
    invoke-virtual {v3, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 569
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    iget-object v5, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->tvFavoriteContacts:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 571
    return-object v3

    .line 565
    .end local v1    # "holder":Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;
    .end local v3    # "row":Landroid/view/View;
    :cond_2
    move-object v3, p2

    .line 566
    .restart local v3    # "row":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;

    .restart local v1    # "holder":Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;
    goto :goto_0
.end method

.method private buildItemRow(Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;Landroid/view/View;)Landroid/view/View;
    .locals 14
    .param p1, "item"    # Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;
    .param p2, "convertView"    # Landroid/view/View;

    .prologue
    .line 578
    move-object v1, p1

    check-cast v1, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;

    .line 579
    .local v1, "element":Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;
    const/4 v8, -0x1

    .line 581
    .local v8, "type":I
    if-eqz p2, :cond_0

    .line 582
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;

    .line 583
    .local v0, "checkHolder":Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;
    iget v8, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->type:I

    .line 586
    .end local v0    # "checkHolder":Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;
    :cond_0
    if-eqz p2, :cond_1

    const/16 v9, 0x64

    if-ne v8, v9, :cond_6

    .line 587
    :cond_1
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->context:Landroid/content/Context;

    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 590
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v9, 0x7f03002b

    .line 591
    const/4 v10, 0x0

    .line 589
    invoke-virtual {v4, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 592
    .local v6, "row":Landroid/view/View;
    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;

    const/4 v9, 0x0

    invoke-direct {v2, p0, v9}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;)V

    .line 595
    .local v2, "holder":Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;
    const v9, 0x7f090125

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    .line 594
    iput-object v9, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->layoutAddFavoriteContacts:Landroid/widget/LinearLayout;

    .line 597
    const v9, 0x7f090127

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 596
    iput-object v9, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->tvFavoriteContacts:Landroid/widget/TextView;

    .line 599
    const v9, 0x7f090128

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/CheckBox;

    .line 598
    iput-object v9, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->cbFavoriteContacts:Landroid/widget/CheckBox;

    .line 601
    const v9, 0x7f090126

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 600
    iput-object v9, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->ivFavoriteContacts:Landroid/widget/ImageView;

    .line 602
    const/16 v9, 0x65

    iput v9, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->type:I

    .line 603
    invoke-virtual {v6, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 610
    .end local v4    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    if-eqz v1, :cond_5

    .line 611
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->tvFavoriteContacts:Landroid/widget/TextView;

    .line 612
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;->getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v10

    .line 613
    invoke-virtual {v10}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/EditText;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v11

    .line 614
    invoke-interface {v11}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v11

    const-string/jumbo v12, "#2FA6C9"

    sget-object v13, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    .line 615
    invoke-virtual {v13}, Landroid/graphics/Typeface;->getStyle()I

    move-result v13

    .line 612
    invoke-static {v10, v11, v12, v13}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->getHighlightedText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/text/SpannableString;

    move-result-object v10

    .line 611
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 616
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->cbFavoriteContacts:Landroid/widget/CheckBox;

    .line 617
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;->getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v10

    .line 616
    invoke-virtual {v9, v10}, Landroid/widget/CheckBox;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 622
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->prevImageView:Landroid/widget/ImageView;

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->prevImageView:Landroid/widget/ImageView;

    iget-object v10, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->ivFavoriteContacts:Landroid/widget/ImageView;

    if-eq v9, v10, :cond_4

    .line 623
    :cond_2
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->defaultImage:Landroid/graphics/Bitmap;

    if-nez v9, :cond_3

    .line 624
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f02024e

    const v11, 0x7f020249

    invoke-static {v9, v10, v11}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->defaultImage:Landroid/graphics/Bitmap;

    .line 625
    :cond_3
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->ivFavoriteContacts:Landroid/widget/ImageView;

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->defaultImage:Landroid/graphics/Bitmap;

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 626
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->mImageLoader:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;->getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v10

    iget-object v11, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->ivFavoriteContacts:Landroid/widget/ImageView;

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->requestContactImage(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;Landroid/widget/ImageView;)V

    .line 627
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->ivFavoriteContacts:Landroid/widget/ImageView;

    iput-object v9, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->prevImageView:Landroid/widget/ImageView;

    .line 630
    :cond_4
    const/4 v7, 0x1

    .line 632
    .local v7, "setToFalse":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mFavoriteList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v3, v9, :cond_7

    .line 657
    :goto_2
    const/4 v3, 0x0

    :goto_3
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToSave:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$4(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v3, v9, :cond_b

    .line 666
    :goto_4
    if-eqz v7, :cond_5

    .line 667
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->cbFavoriteContacts:Landroid/widget/CheckBox;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 672
    .end local v3    # "i":I
    .end local v7    # "setToFalse":Z
    :cond_5
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->layoutAddFavoriteContacts:Landroid/widget/LinearLayout;

    .line 673
    new-instance v10, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$2;

    invoke-direct {v10, p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$2;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;)V

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 683
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->cbFavoriteContacts:Landroid/widget/CheckBox;

    new-instance v10, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;

    invoke-direct {v10, p0, v2, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;)V

    invoke-virtual {v9, v10}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 735
    return-object v6

    .line 605
    .end local v2    # "holder":Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;
    .end local v6    # "row":Landroid/view/View;
    :cond_6
    move-object/from16 v6, p2

    .line 606
    .restart local v6    # "row":Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;

    .restart local v2    # "holder":Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;
    goto/16 :goto_0

    .line 633
    .restart local v3    # "i":I
    .restart local v7    # "setToFalse":Z
    :cond_7
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mFavoriteList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v9

    .line 634
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;->getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v11

    .line 633
    cmp-long v9, v9, v11

    if-nez v9, :cond_a

    .line 639
    const/4 v7, 0x0

    .line 640
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_5
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToDelete:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v5, v9, :cond_8

    .line 650
    if-nez v7, :cond_a

    .line 651
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->cbFavoriteContacts:Landroid/widget/CheckBox;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_2

    .line 641
    :cond_8
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToDelete:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v9

    .line 642
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;->getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v11

    .line 641
    cmp-long v9, v9, v11

    if-nez v9, :cond_9

    .line 647
    const/4 v7, 0x1

    .line 640
    :cond_9
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 632
    .end local v5    # "j":I
    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 658
    :cond_b
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToSave:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$4(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v9

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;->getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v11

    .line 659
    invoke-virtual {v11}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v11

    .line 658
    cmp-long v9, v9, v11

    if-nez v9, :cond_c

    .line 660
    iget-object v9, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->cbFavoriteContacts:Landroid/widget/CheckBox;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 661
    const/4 v7, 0x0

    .line 662
    goto/16 :goto_4

    .line 657
    :cond_c
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3
.end method


# virtual methods
.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 469
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 470
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->mImageLoader:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->stopThread()V

    .line 471
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->filteredItems:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    .line 476
    const/4 v0, 0x0

    .line 478
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->filteredItems:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 483
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->filteredItems:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    .line 484
    const/4 v0, 0x0

    .line 486
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->filteredItems:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->getItem(I)Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 500
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 505
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->getItem(I)Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getType()Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;->HEADER:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;

    if-ne v0, v1, :cond_0

    .line 506
    const/4 v0, 0x0

    .line 508
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v4, 0x7f090129

    .line 519
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->getItem(I)Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;

    move-result-object v0

    .line 522
    .local v0, "item":Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->getItem(I)Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getType()Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;

    move-result-object v2

    .line 523
    sget-object v3, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;->HEADER:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;

    .line 522
    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 523
    if-eqz v2, :cond_0

    .line 524
    invoke-direct {p0, v0, p2}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->buildHeaderRow(Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    .line 536
    .local v1, "row":Landroid/view/View;
    :goto_0
    return-object v1

    .line 526
    .end local v1    # "row":Landroid/view/View;
    :cond_0
    invoke-direct {p0, v0, p2}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->buildItemRow(Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    .line 527
    .restart local v1    # "row":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->getCount()I

    move-result v2

    add-int/lit8 v3, p1, 0x1

    if-eq v2, v3, :cond_1

    .line 528
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->getCount()I

    move-result v2

    add-int/lit8 v3, p1, 0x1

    if-le v2, v3, :cond_2

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->getItem(I)Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;

    move-result-object v2

    .line 529
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getType()Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;

    move-result-object v2

    .line 530
    sget-object v3, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;->HEADER:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 531
    :cond_1
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 533
    :cond_2
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 513
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;->values()[Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 491
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 492
    const/4 v0, 0x0

    .line 494
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
