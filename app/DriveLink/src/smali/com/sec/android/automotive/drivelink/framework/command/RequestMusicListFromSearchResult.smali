.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestMusicListFromSearchResult.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIndex:I

.field private mMusicList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation
.end field

.field private mMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "musicSearchResult"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;
    .param p4, "index"    # I

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 14
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;->mContext:Landroid/content/Context;

    .line 15
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;->mMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    .line 16
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;->mMusicList:Ljava/util/ArrayList;

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;->mIndex:I

    .line 24
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;->mContext:Landroid/content/Context;

    .line 25
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;->mMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    .line 26
    iput p4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;->mIndex:I

    .line 27
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 50
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;->mContext:Landroid/content/Context;

    .line 51
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;->mMusicList:Ljava/util/ArrayList;

    .line 52
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;->mMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    .line 53
    return-void
.end method

.method protected process()V
    .locals 4

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 32
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 34
    .local v0, "contentManager":Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;->mContext:Landroid/content/Context;

    .line 35
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;->mMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    iget v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;->mIndex:I

    .line 34
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getMusicListFromSearchList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;->mMusicList:Ljava/util/ArrayList;

    .line 36
    return-void
.end method

.method protected response()V
    .locals 4

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 41
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

    move-result-object v0

    .line 42
    .local v0, "musicListener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;
    if-eqz v0, :cond_0

    .line 44
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;->mMusicList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;->mMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    iget v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;->mIndex:I

    .line 43
    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;->onResponseRequestMusicListFromSearchResult(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;I)V

    .line 46
    :cond_0
    return-void
.end method
