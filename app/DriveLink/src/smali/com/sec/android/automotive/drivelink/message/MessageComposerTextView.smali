.class public Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;
.super Landroid/widget/RelativeLayout;
.source "MessageComposerTextView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;,
        Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;,
        Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$OnComposerSipStateListener;
    }
.end annotation


# static fields
.field private static final MSG_FINISH:I = 0x64

.field private static final TAG:Ljava/lang/String; = "MessageComposerTextView"


# instance fields
.field private ViewingToast:Z

.field getVoiceInputText:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

.field private isDebug:Z

.field mCompowerTextViewStateListener:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$OnComposerSipStateListener;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field mInputConnection:Landroid/view/inputmethod/InputConnection;

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mMsgSIPState:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

.field private mTextView:Landroid/widget/EditText;

.field private unknownSIP:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 67
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 52
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->ViewingToast:Z

    .line 55
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->unknownSIP:Z

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->isDebug:Z

    .line 59
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mMsgSIPState:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    .line 133
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$1;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getVoiceInputText:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

    .line 494
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$2;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mHandler:Landroid/os/Handler;

    .line 68
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "MessageComposerTextView"

    const-string/jumbo v2, ""

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->init(Landroid/content/Context;)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 73
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->ViewingToast:Z

    .line 55
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->unknownSIP:Z

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->isDebug:Z

    .line 59
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mMsgSIPState:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    .line 133
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$1;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getVoiceInputText:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

    .line 494
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$2;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mHandler:Landroid/os/Handler;

    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->init(Landroid/content/Context;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 79
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->ViewingToast:Z

    .line 55
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->unknownSIP:Z

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->isDebug:Z

    .line 59
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mMsgSIPState:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    .line 133
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$1;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getVoiceInputText:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

    .line 494
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$2;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mHandler:Landroid/os/Handler;

    .line 80
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->init(Landroid/content/Context;)V

    .line 81
    return-void
.end method

.method private Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "level"    # Ljava/lang/String;
    .param p2, "sub"    # Ljava/lang/String;
    .param p3, "msg"    # Ljava/lang/String;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->isDebug:Z

    if-eqz v0, :cond_0

    .line 63
    const-string/jumbo v0, "MessageComposerTextView"

    invoke-static {p1, v0, p2, p3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;Z)V
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->ViewingToast:Z

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->ViewingToast:Z

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 84
    const-string/jumbo v2, "i"

    const-string/jumbo v3, "init"

    const-string/jumbo v4, ""

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mContext:Landroid/content/Context;

    .line 87
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 89
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f03001b

    .line 88
    invoke-virtual {v1, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mLayout:Landroid/widget/RelativeLayout;

    .line 90
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f0900c2

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    .line 92
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    invoke-virtual {v2, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 93
    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setComposerFocusable(Z)V

    .line 95
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V

    .line 97
    sget-object v2, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setComposerStatechanged(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;)V

    .line 99
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->getInstance()Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;

    move-result-object v2

    .line 100
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getVoiceInputText:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

    .line 99
    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->registerVoiceInputListener(Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;)V

    .line 102
    const/4 v2, 0x1

    new-array v0, v2, [Landroid/text/InputFilter;

    new-instance v2, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)V

    aput-object v2, v0, v5

    .line 103
    .local v0, "filters":[Landroid/text/InputFilter;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 105
    return-void
.end method


# virtual methods
.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 3
    .param p1, "watcher"    # Landroid/text/TextWatcher;

    .prologue
    .line 305
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "addTextChangedListener"

    const-string/jumbo v2, ""

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 307
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 170
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 176
    return-void
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 186
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "dispatchKeyEventPreIme"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, " keyCode"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    .line 188
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v1

    .line 207
    :goto_0
    return v1

    .line 190
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mContext:Landroid/content/Context;

    .line 191
    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 190
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 193
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "dispatchKeyEventPreIme"

    .line 194
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "imm.isActive() ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isActive()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 193
    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "dispatchKeyEventPreIme"

    .line 196
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "imm.isInputMethodShown() ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 195
    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setComposerFocusable(Z)V

    .line 206
    sget-object v1, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_CLOSE:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setComposerStatechanged(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;)V

    .line 207
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method public getComposerFocusable()Z
    .locals 5

    .prologue
    .line 310
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->isFocusable()Z

    move-result v0

    .line 312
    .local v0, "ret":Z
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "getComposerFocusable"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "isFocusable "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    if-nez v0, :cond_0

    .line 314
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->isFocusableInTouchMode()Z

    move-result v0

    .line 315
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "getComposerFocusable"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "isFocusableInTouchMode "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    :cond_0
    return v0
.end method

.method public getComposerSIPState()Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mMsgSIPState:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    return-object v0
.end method

.method public getIsActiveImm()Z
    .locals 3

    .prologue
    .line 211
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mContext:Landroid/content/Context;

    .line 212
    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 211
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 213
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isActive()Z

    move-result v1

    return v1
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    return-object v0
.end method

.method public getUnknownSIP()Z
    .locals 1

    .prologue
    .line 284
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->unknownSIP:Z

    return v0
.end method

.method public hideInputMethod()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 248
    const/4 v1, 0x1

    .line 249
    .local v1, "ret":Z
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mContext:Landroid/content/Context;

    .line 250
    const-string/jumbo v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 249
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 252
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    const-string/jumbo v2, "i"

    const-string/jumbo v3, "hideInputMethod"

    .line 253
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "imm.isInputMethodShown() = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 252
    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setComposerFocusable(Z)V

    .line 256
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getUnknownSIP()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 257
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v6}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 260
    sget-object v2, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_CLOSE:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setComposerStatechanged(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;)V

    .line 261
    const/4 v1, 0x1

    .line 266
    :goto_0
    return v1

    .line 263
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isComposerInputMethodShown()Z
    .locals 6

    .prologue
    .line 288
    const/4 v1, 0x0

    .line 289
    .local v1, "ret":Z
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mContext:Landroid/content/Context;

    .line 290
    const-string/jumbo v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 289
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 292
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 293
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    .line 295
    :cond_0
    const-string/jumbo v2, "i"

    const-string/jumbo v3, "isComposerInputMethodShown"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "ret = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    return v1
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 181
    return-void
.end method

.method public setComposerFocusable(Z)V
    .locals 1
    .param p1, "bfocus"    # Z

    .prologue
    .line 323
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 324
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 325
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 327
    return-void
.end method

.method public setComposerStatechanged(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;)V
    .locals 1
    .param p1, "state"    # Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    .prologue
    .line 336
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mMsgSIPState:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    .line 337
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mCompowerTextViewStateListener:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$OnComposerSipStateListener;

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mCompowerTextViewStateListener:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$OnComposerSipStateListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$OnComposerSipStateListener;->OnStateChanged(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;)V

    .line 340
    :cond_0
    return-void
.end method

.method public setContentAreaHeight(ILcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;F)V
    .locals 7
    .param p1, "Orientation"    # I
    .param p2, "state"    # Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;
    .param p3, "density"    # F

    .prologue
    .line 348
    const/4 v0, 0x0

    .line 349
    .local v0, "ContentAreaH":I
    const/4 v2, 0x0

    .line 351
    .local v2, "pixelH":I
    sget-object v3, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_SHOW:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    if-ne p2, v3, :cond_0

    .line 352
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 353
    const v4, 0x7f0d00dd

    .line 352
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, p3

    float-to-int v2, v3

    .line 358
    :goto_0
    int-to-float v3, v2

    mul-float/2addr v3, p3

    float-to-int v0, v3

    .line 360
    const-string/jumbo v3, "i"

    const-string/jumbo v4, "setContentAreaHeight"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "ContentAreaH : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 361
    const-string/jumbo v6, " density : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " state : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 362
    const-string/jumbo v6, " Orientation : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 360
    invoke-direct {p0, v3, v4, v5}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v1, v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 366
    .local v1, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    invoke-virtual {v3, v1}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 368
    return-void

    .line 355
    .end local v1    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 356
    const v4, 0x7f0d00db

    .line 355
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, p3

    float-to-int v2, v3

    goto :goto_0
.end method

.method public setContentAreaHeightbyReplace(Z)V
    .locals 6
    .param p1, "bShowReplace"    # Z

    .prologue
    .line 374
    const-string/jumbo v2, "i"

    const-string/jumbo v3, "setContentAreaHeightbyReplace"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, " bShowReplace = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 375
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 374
    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    const/4 v0, 0x0

    .line 378
    .local v0, "ContentAreaH":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    if-eqz v2, :cond_1

    .line 379
    if-eqz p1, :cond_0

    .line 380
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mContext:Landroid/content/Context;

    .line 381
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 383
    const v3, 0x7f0d00df

    .line 382
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 389
    :goto_0
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 390
    const/4 v2, -0x1

    .line 389
    invoke-direct {v1, v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 391
    .local v1, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 396
    .end local v1    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :goto_1
    return-void

    .line 385
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 387
    const v3, 0x7f0d00db

    .line 386
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 385
    goto :goto_0

    .line 393
    :cond_1
    const-string/jumbo v2, "i"

    const-string/jumbo v3, "Skip setContentAreaHeightbyReplace"

    .line 394
    const-string/jumbo v4, " mTextView = null"

    .line 393
    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public setOnComposerClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 301
    return-void
.end method

.method public setOnComposerEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V
    .locals 1
    .param p1, "l"    # Landroid/widget/TextView$OnEditorActionListener;

    .prologue
    .line 128
    if-eqz p1, :cond_0

    .line 129
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 131
    :cond_0
    return-void
.end method

.method public setOnComposerSipStateListener(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$OnComposerSipStateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$OnComposerSipStateListener;

    .prologue
    .line 331
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mCompowerTextViewStateListener:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$OnComposerSipStateListener;

    .line 332
    return-void
.end method

.method setReplaceText()V
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 166
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 109
    if-nez p1, :cond_0

    .line 110
    const/4 v0, 0x0

    .line 113
    .local v0, "strMsgText":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 117
    return-void

    .line 112
    .end local v0    # "strMsgText":Ljava/lang/String;
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "strMsgText":Ljava/lang/String;
    goto :goto_0
.end method

.method public setUnknownSIP(Z)V
    .locals 1
    .param p1, "state"    # Z

    .prologue
    .line 277
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->unknownSIP:Z

    .line 279
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mCompowerTextViewStateListener:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$OnComposerSipStateListener;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mCompowerTextViewStateListener:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$OnComposerSipStateListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$OnComposerSipStateListener;->OnAbnormalSIP(Z)V

    .line 281
    :cond_0
    return-void
.end method

.method public showInputMethod()V
    .locals 5

    .prologue
    .line 218
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "showInputMethod()"

    .line 219
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "mTextView.isFocusable() ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->isFocusable()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 218
    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setComposerFocusable(Z)V

    .line 233
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 236
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mContext:Landroid/content/Context;

    .line 237
    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 236
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 238
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 243
    sget-object v1, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_SHOW:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setComposerStatechanged(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;)V

    .line 245
    return-void
.end method
