.class public Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
.super Landroid/widget/RelativeLayout;
.source "QuotationMarksButton.java"


# static fields
.field private static final BLACK:I = 0x0

.field private static final Horizontal:Ljava/lang/String; = "horizontal"

.field private static final Vertical:Ljava/lang/String; = "vertical"

.field private static final WHITE_LARGE:I = 0x1


# instance fields
.field bgImage:Landroid/graphics/drawable/Drawable;

.field imgeDrawable:Landroid/graphics/drawable/Drawable;

.field mContext:Landroid/content/Context;

.field mainLayout:Landroid/widget/LinearLayout;

.field orientation:Z

.field showMark:Z

.field strData:Ljava/lang/String;

.field textViewColor:I

.field textViewSelector:I

.field textViewStyle:I

.field txtItalicView:Landroid/widget/TextView;

.field txtview:Landroid/widget/TextView;

.field typeValue:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 66
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 34
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtview:Landroid/widget/TextView;

    .line 35
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtItalicView:Landroid/widget/TextView;

    .line 41
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->orientation:Z

    .line 46
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->typeValue:I

    .line 51
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->showMark:Z

    .line 56
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->imgeDrawable:Landroid/graphics/drawable/Drawable;

    .line 57
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->bgImage:Landroid/graphics/drawable/Drawable;

    .line 59
    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->textViewColor:I

    .line 60
    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->textViewSelector:I

    .line 61
    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->textViewStyle:I

    .line 63
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->strData:Ljava/lang/String;

    .line 67
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->Initialization(Landroid/content/Context;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 71
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtview:Landroid/widget/TextView;

    .line 35
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtItalicView:Landroid/widget/TextView;

    .line 41
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->orientation:Z

    .line 46
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->typeValue:I

    .line 51
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->showMark:Z

    .line 56
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->imgeDrawable:Landroid/graphics/drawable/Drawable;

    .line 57
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->bgImage:Landroid/graphics/drawable/Drawable;

    .line 59
    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->textViewColor:I

    .line 60
    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->textViewSelector:I

    .line 61
    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->textViewStyle:I

    .line 63
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->strData:Ljava/lang/String;

    .line 73
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->SetAttrsData(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 74
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->Initialization(Landroid/content/Context;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 79
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtview:Landroid/widget/TextView;

    .line 35
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtItalicView:Landroid/widget/TextView;

    .line 41
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->orientation:Z

    .line 46
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->typeValue:I

    .line 51
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->showMark:Z

    .line 56
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->imgeDrawable:Landroid/graphics/drawable/Drawable;

    .line 57
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->bgImage:Landroid/graphics/drawable/Drawable;

    .line 59
    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->textViewColor:I

    .line 60
    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->textViewSelector:I

    .line 61
    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->textViewStyle:I

    .line 63
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->strData:Ljava/lang/String;

    .line 81
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->SetAttrsData(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 82
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->Initialization(Landroid/content/Context;)V

    .line 83
    return-void
.end method

.method private SetButtonContent(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, -0x1

    .line 172
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->bgImage:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f090199

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 174
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->bgImage:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtview:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 178
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->textViewSelector:I

    if-eq v0, v2, :cond_5

    .line 179
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtview:Landroid/widget/TextView;

    iget v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->textViewSelector:I

    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setTextViewColor(Landroid/widget/TextView;I)V

    .line 190
    :cond_1
    :goto_0
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->textViewColor:I

    if-eq v0, v2, :cond_2

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtview:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 191
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtview:Landroid/widget/TextView;

    iget v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->textViewColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 194
    :cond_2
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->textViewStyle:I

    if-eq v0, v2, :cond_3

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtview:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 195
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtview:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->textViewStyle:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 198
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setImageAllocateQuotationMark()V

    .line 199
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setQuotationMark()V

    .line 201
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->strData:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 202
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f09019c

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 203
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->strData:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f09019d

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 205
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->strData:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    :cond_4
    return-void

    .line 185
    :cond_5
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtview:Landroid/widget/TextView;

    const v1, 0x7f02005a

    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setTextViewColor(Landroid/widget/TextView;I)V

    goto :goto_0
.end method

.method private setImageAllocateQuotationMark()V
    .locals 5

    .prologue
    const/4 v4, -0x2

    .line 227
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->imgeDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    .line 228
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    .line 229
    const v3, 0x7f09019a

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 228
    check-cast v0, Landroid/widget/ImageView;

    .line 230
    .local v0, "btnImage":Landroid/widget/ImageView;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->imgeDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 231
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 233
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->orientation:Z

    if-eqz v2, :cond_0

    .line 234
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 236
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v2, 0x1

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 237
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 240
    .end local v0    # "btnImage":Landroid/widget/ImageView;
    .end local v1    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    return-void
.end method

.method private setQuotationMark()V
    .locals 6

    .prologue
    const v5, 0x7f020363

    const/16 v4, 0x8

    const/4 v1, 0x0

    const v3, 0x7f09019e

    const v2, 0x7f09019b

    .line 245
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->showMark:Z

    if-eqz v0, :cond_3

    .line 251
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtview:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtview:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtItalicView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 257
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 258
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 259
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 261
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->typeValue:I

    if-nez v0, :cond_1

    .line 262
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 263
    const v1, 0x7f020365

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 265
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 293
    :goto_0
    return-void

    .line 266
    :cond_1
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->typeValue:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 267
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 268
    const v1, 0x7f020356

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 269
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 270
    const v1, 0x7f020355

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 272
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 273
    const v1, 0x7f020365

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 274
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 275
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 283
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtview:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 284
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtview:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 286
    :cond_4
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtItalicView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 289
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 290
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 291
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setTextViewColor(Landroid/widget/TextView;I)V
    .locals 4
    .param p1, "textview"    # Landroid/widget/TextView;
    .param p2, "selector"    # I

    .prologue
    .line 210
    const/4 v2, 0x0

    .line 211
    .local v2, "parser":Landroid/content/res/XmlResourceParser;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v2

    .line 213
    if-eqz v2, :cond_0

    .line 216
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 215
    invoke-static {v3, v2}, Landroid/content/res/ColorStateList;->createFromXml(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 217
    .local v0, "colors":Landroid/content/res/ColorStateList;
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 224
    .end local v0    # "colors":Landroid/content/res/ColorStateList;
    :cond_0
    :goto_0
    return-void

    .line 218
    :catch_0
    move-exception v1

    .line 219
    .local v1, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_0

    .line 220
    .end local v1    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v1

    .line 221
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public Initialization(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 147
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mContext:Landroid/content/Context;

    .line 149
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mContext:Landroid/content/Context;

    .line 150
    const-string/jumbo v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 149
    check-cast v0, Landroid/view/LayoutInflater;

    .line 152
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030040

    const/4 v2, 0x0

    .line 151
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    .line 153
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->orientation:Z

    if-eqz v1, :cond_2

    .line 154
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 157
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->addView(Landroid/view/View;)V

    .line 159
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setClickable(Z)V

    .line 160
    const/16 v1, 0x11

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setGravity(I)V

    .line 162
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtview:Landroid/widget/TextView;

    if-nez v1, :cond_0

    .line 163
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    const v2, 0x7f09019c

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtview:Landroid/widget/TextView;

    .line 164
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtItalicView:Landroid/widget/TextView;

    if-nez v1, :cond_1

    .line 165
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    .line 166
    const v2, 0x7f09019d

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 165
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtItalicView:Landroid/widget/TextView;

    .line 168
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->SetButtonContent(Landroid/content/Context;)V

    .line 169
    return-void

    .line 156
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    goto :goto_0
.end method

.method public SetAttrsData(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, -0x1

    .line 92
    .line 93
    sget-object v5, Lcom/sec/android/automotive/drivelink/R$styleable;->QuotationMarksButton:[I

    .line 92
    invoke-virtual {p1, p2, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 95
    .local v0, "attr":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_5

    .line 97
    invoke-virtual {v0, v8}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 98
    .local v1, "orientation":Ljava/lang/CharSequence;
    if-eqz v1, :cond_0

    .line 99
    const-string/jumbo v5, "horizontal"

    invoke-virtual {v1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 100
    iput-boolean v8, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->orientation:Z

    .line 107
    :cond_0
    :goto_0
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 108
    .local v4, "styleType":Ljava/lang/CharSequence;
    if-eqz v4, :cond_1

    .line 109
    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->typeValue:I

    .line 112
    :cond_1
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 113
    .local v2, "showValue":Ljava/lang/CharSequence;
    if-eqz v2, :cond_2

    .line 114
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "true"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 115
    iput-boolean v9, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->showMark:Z

    .line 121
    :cond_2
    :goto_1
    const/4 v5, 0x4

    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 120
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->imgeDrawable:Landroid/graphics/drawable/Drawable;

    .line 123
    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 122
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->bgImage:Landroid/graphics/drawable/Drawable;

    .line 126
    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 127
    .local v3, "strValue":Ljava/lang/CharSequence;
    if-eqz v3, :cond_3

    .line 128
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, " "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->strData:Ljava/lang/String;

    .line 132
    :cond_3
    const/4 v5, 0x7

    .line 131
    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    iput v5, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->textViewColor:I

    .line 133
    iget v5, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->textViewColor:I

    if-ne v5, v7, :cond_4

    .line 136
    const/16 v5, 0x8

    .line 135
    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    .line 134
    iput v5, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->textViewSelector:I

    .line 140
    :cond_4
    const/4 v5, 0x6

    .line 139
    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    iput v5, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->textViewStyle:I

    .line 142
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 144
    .end local v1    # "orientation":Ljava/lang/CharSequence;
    .end local v2    # "showValue":Ljava/lang/CharSequence;
    .end local v3    # "strValue":Ljava/lang/CharSequence;
    .end local v4    # "styleType":Ljava/lang/CharSequence;
    :cond_5
    return-void

    .line 101
    .restart local v1    # "orientation":Ljava/lang/CharSequence;
    :cond_6
    const-string/jumbo v5, "vertical"

    invoke-virtual {v1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 102
    iput-boolean v9, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->orientation:Z

    goto/16 :goto_0

    .line 117
    .restart local v2    # "showValue":Ljava/lang/CharSequence;
    .restart local v4    # "styleType":Ljava/lang/CharSequence;
    :cond_7
    iput-boolean v8, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->showMark:Z

    goto :goto_1
.end method

.method public getText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 331
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f09019c

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 332
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 331
    return-object v0
.end method

.method public isMarkShow()Z
    .locals 1

    .prologue
    .line 301
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->showMark:Z

    return v0
.end method

.method public setButtonStyleOff(I)V
    .locals 3
    .param p1, "style"    # I

    .prologue
    .line 336
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    const v2, 0x7f09019c

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 338
    .local v0, "btText":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 339
    return-void
.end method

.method public setButtonStyleOn(I)V
    .locals 3
    .param p1, "style"    # I

    .prologue
    .line 342
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    .line 343
    const v2, 0x7f09019d

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 342
    check-cast v0, Landroid/widget/TextView;

    .line 345
    .local v0, "btText":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 346
    return-void
.end method

.method public setButtonText(Ljava/lang/String;)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 321
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->strData:Ljava/lang/String;

    .line 322
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->mainLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f09019c

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 323
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 350
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 352
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtview:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtview:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtItalicView:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 356
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->txtItalicView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 358
    :cond_1
    return-void
.end method

.method public setMarkShow(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 311
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->showMark:Z

    .line 312
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setQuotationMark()V

    .line 313
    return-void
.end method
