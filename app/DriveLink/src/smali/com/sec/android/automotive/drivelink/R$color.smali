.class public final Lcom/sec/android/automotive/drivelink/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final ab_night_text_color:I = 0x7f080019

.field public static final ab_text_color:I = 0x7f080018

.field public static final ab_text_shadow75_color:I = 0x7f08001b

.field public static final ab_text_shadow_color:I = 0x7f08001a

.field public static final actionbar_text_color:I = 0x7f08004d

.field public static final background_color_night:I = 0x7f08004e

.field public static final base_bg_color:I = 0x7f080015

.field public static final black:I = 0x7f08000f

.field public static final black_shadow40_color:I = 0x7f08001e

.field public static final black_shadow70_color:I = 0x7f08001d

.field public static final blue:I = 0x7f08000c

.field public static final btn_basic_black_nor:I = 0x7f08009d

.field public static final btn_basic_black_sel:I = 0x7f08009e

.field public static final btn_basic_white_nor:I = 0x7f08009b

.field public static final btn_basic_white_sel:I = 0x7f08009c

.field public static final button_basic_color_selector:I = 0x7f0800a2

.field public static final button_text_disable:I = 0x7f080051

.field public static final button_text_normal:I = 0x7f080050

.field public static final call_james_text_color:I = 0x7f080037

.field public static final call_james_text_color_night:I = 0x7f080038

.field public static final common_action_bar_splitter:I = 0x7f080009

.field public static final common_signin_btn_dark_text_default:I = 0x7f080000

.field public static final common_signin_btn_dark_text_disabled:I = 0x7f080002

.field public static final common_signin_btn_dark_text_focused:I = 0x7f080003

.field public static final common_signin_btn_dark_text_pressed:I = 0x7f080001

.field public static final common_signin_btn_default_background:I = 0x7f080008

.field public static final common_signin_btn_light_text_default:I = 0x7f080004

.field public static final common_signin_btn_light_text_disabled:I = 0x7f080006

.field public static final common_signin_btn_light_text_focused:I = 0x7f080007

.field public static final common_signin_btn_light_text_pressed:I = 0x7f080005

.field public static final common_signin_btn_text_dark:I = 0x7f0800a3

.field public static final common_signin_btn_text_light:I = 0x7f0800a4

.field public static final dialog_bg_black_color:I = 0x7f08009f

.field public static final dialog_bg_color:I = 0x7f080016

.field public static final dialog_bg_color_night:I = 0x7f080017

.field public static final gray:I = 0x7f080012

.field public static final green:I = 0x7f08000d

.field public static final hi_text_color:I = 0x7f080033

.field public static final hi_text_color_night:I = 0x7f080034

.field public static final home_base_bg_color:I = 0x7f08002d

.field public static final home_base_bg_color_night:I = 0x7f08002e

.field public static final home_btn_text_color:I = 0x7f08002f

.field public static final home_btn_text_color_night:I = 0x7f080030

.field public static final indicator_time_color:I = 0x7f080052

.field public static final list_item_bg_color:I = 0x7f080020

.field public static final list_item_index_color:I = 0x7f080022

.field public static final list_item_main_text_color_n:I = 0x7f080023

.field public static final list_item_main_text_color_p:I = 0x7f080024

.field public static final list_item_search_highlight_color:I = 0x7f08002c

.field public static final list_item_seperator_color:I = 0x7f080021

.field public static final list_item_sub2_text_color_n:I = 0x7f080027

.field public static final list_item_sub2_text_color_p:I = 0x7f080028

.field public static final list_item_sub_text_color_n:I = 0x7f080025

.field public static final list_item_sub_text_color_p:I = 0x7f080026

.field public static final list_title_text_color:I = 0x7f08001f

.field public static final location_base_bg_color:I = 0x7f080044

.field public static final location_base_bg_color_night:I = 0x7f080045

.field public static final location_btn_text_color:I = 0x7f080046

.field public static final location_btn_text_color_night:I = 0x7f080047

.field public static final location_button_background:I = 0x7f080040

.field public static final location_dim_text_color:I = 0x7f080042

.field public static final location_share_duration_text_color:I = 0x7f08003c

.field public static final location_share_mapdetails_info_text_color:I = 0x7f08003e

.field public static final location_share_mapdetails_label_text_color:I = 0x7f08003d

.field public static final location_share_mapdetails_min_text_color:I = 0x7f08003f

.field public static final location_suggestion_bg:I = 0x7f08003a

.field public static final location_timestamp_color:I = 0x7f080041

.field public static final message_button_textcolor:I = 0x7f08007b

.field public static final message_composer_background:I = 0x7f08007c

.field public static final message_composer_selection_area:I = 0x7f08007d

.field public static final message_suggestion_bg:I = 0x7f080082

.field public static final music_artist_color:I = 0x7f08006e

.field public static final music_background_bg:I = 0x7f080078

.field public static final music_background_bg_night:I = 0x7f080079

.field public static final music_endtime_color:I = 0x7f080070

.field public static final music_hi_text_color:I = 0x7f080076

.field public static final music_hi_text_color_night:I = 0x7f080077

.field public static final music_panel_artist_color:I = 0x7f080071

.field public static final music_panel_title_color:I = 0x7f080072

.field public static final music_playtime_color:I = 0x7f08006f

.field public static final music_say_text_color:I = 0x7f080074

.field public static final music_say_text_color_night:I = 0x7f080075

.field public static final music_search_bg:I = 0x7f08007a

.field public static final music_title_color:I = 0x7f08006d

.field public static final music_volume_text_color:I = 0x7f080073

.field public static final no_list_text_color:I = 0x7f080029

.field public static final noti_bg:I = 0x7f080053

.field public static final noti_content_alarm_time:I = 0x7f080054

.field public static final noti_content_bg:I = 0x7f08005b

.field public static final noti_group_share_location_event:I = 0x7f08003b

.field public static final noti_home_bg:I = 0x7f080062

.field public static final noti_message_popup_contents_bg_color:I = 0x7f08007e

.field public static final noti_message_popup_contents_text_color:I = 0x7f080080

.field public static final noti_message_popup_contents_text_item_color:I = 0x7f080081

.field public static final noti_message_popup_selection_bg_color:I = 0x7f08007f

.field public static final noti_msg_txt_color_btn:I = 0x7f080060

.field public static final noti_music_bg:I = 0x7f080061

.field public static final noti_music_dialog_artist:I = 0x7f080056

.field public static final noti_music_dialog_title:I = 0x7f080055

.field public static final noti_popup_bg:I = 0x7f080058

.field public static final noti_popup_contents_bg_color:I = 0x7f08004f

.field public static final noti_popup_selection_bg_color:I = 0x7f08005a

.field public static final noti_smartalert_eventinfo:I = 0x7f080094

.field public static final noti_smartalert_eventinfo_content_text:I = 0x7f080098

.field public static final noti_smartalert_eventinfo_label:I = 0x7f080096

.field public static final noti_smartalert_eventinfo_text:I = 0x7f080097

.field public static final noti_smartalert_eventinfo_title:I = 0x7f080095

.field public static final noti_smartalert_linecolor:I = 0x7f080099

.field public static final noti_smartalert_mainlinecolor:I = 0x7f08009a

.field public static final noti_title_bg:I = 0x7f080059

.field public static final noti_title_bg_wo_button:I = 0x7f080057

.field public static final noti_txt_color_btn:I = 0x7f08005f

.field public static final noti_txt_color_home:I = 0x7f080063

.field public static final noti_txt_color_input:I = 0x7f08005e

.field public static final noti_txt_color_main:I = 0x7f08005c

.field public static final noti_txt_color_sub:I = 0x7f08005d

.field public static final page_navigation_bg_color:I = 0x7f08002a

.field public static final page_navigation_bg_night_color:I = 0x7f08002b

.field public static final phone_prepare_layout_contactinfo_bg:I = 0x7f08006a

.field public static final phone_prepare_layout_contactname_color:I = 0x7f08006c

.field public static final phone_prepare_layout_count_color:I = 0x7f08006b

.field public static final phone_thumbnail_bg_endcall:I = 0x7f080066

.field public static final phone_thumbnail_bg_incall:I = 0x7f080065

.field public static final phone_thumbnail_bg_incomingcall:I = 0x7f080067

.field public static final phone_thumbnail_bg_outgoingcall:I = 0x7f080064

.field public static final phone_thumbnail_status_incall:I = 0x7f080069

.field public static final phone_thumbnail_status_outgoingcall:I = 0x7f080068

.field public static final pink:I = 0x7f080010

.field public static final progress_loading_text:I = 0x7f080043

.field public static final red:I = 0x7f08000b

.field public static final say_text_color:I = 0x7f080031

.field public static final say_text_color_night:I = 0x7f080032

.field public static final settings_bg:I = 0x7f080084

.field public static final settings_commute_notification_pop_up:I = 0x7f08008a

.field public static final settings_commute_notification_time_am:I = 0x7f080088

.field public static final settings_commute_notification_time_pm:I = 0x7f080089

.field public static final settings_connected_device:I = 0x7f08008b

.field public static final settings_disabled_white:I = 0x7f08008e

.field public static final settings_list_bg:I = 0x7f080083

.field public static final settings_list_divider:I = 0x7f08008c

.field public static final settings_toggle_bg_normal:I = 0x7f080085

.field public static final settings_toggle_bg_pressed:I = 0x7f080086

.field public static final settings_white:I = 0x7f08008d

.field public static final settings_winset_statusbar:I = 0x7f080087

.field public static final speak_now_text_color:I = 0x7f080035

.field public static final speak_now_text_color_night:I = 0x7f080036

.field public static final translucent_background:I = 0x7f080013

.field public static final transparent:I = 0x7f08000a

.field public static final transparent_background:I = 0x7f080014

.field public static final update_description_color:I = 0x7f0800a1

.field public static final update_text_color:I = 0x7f0800a0

.field public static final voice_2depth_button_text_color_n:I = 0x7f08004a

.field public static final voice_2depth_button_text_color_p:I = 0x7f080049

.field public static final voice_message_color:I = 0x7f08001c

.field public static final voice_tts_background:I = 0x7f080048

.field public static final voice_tts_text_color:I = 0x7f08004b

.field public static final voice_tts_text_color_night:I = 0x7f08004c

.field public static final welcome_background_color:I = 0x7f080039

.field public static final what_can_i_say_bg:I = 0x7f08008f

.field public static final what_can_i_say_divider:I = 0x7f080090

.field public static final what_can_i_say_item_child_bg:I = 0x7f080093

.field public static final what_can_i_say_summary:I = 0x7f080092

.field public static final what_can_i_say_title:I = 0x7f080091

.field public static final white:I = 0x7f08000e

.field public static final yellow:I = 0x7f080011


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
