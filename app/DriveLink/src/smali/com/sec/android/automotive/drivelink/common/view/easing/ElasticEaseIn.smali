.class public Lcom/sec/android/automotive/drivelink/common/view/easing/ElasticEaseIn;
.super Ljava/lang/Object;
.source "ElasticEaseIn.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 13
    .param p1, "input"    # F

    .prologue
    const v12, 0x40490fdb    # (float)Math.PI

    const/high16 v11, 0x40000000    # 2.0f

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 18
    const/4 v1, 0x0

    .line 19
    .local v1, "p":F
    const/4 v0, 0x0

    .line 20
    .local v0, "a":F
    cmpl-float v5, p1, v3

    if-nez v5, :cond_0

    .line 35
    :goto_0
    return v3

    .line 23
    :cond_0
    div-float/2addr p1, v4

    cmpl-float v5, p1, v4

    if-nez v5, :cond_1

    move v3, v4

    .line 24
    goto :goto_0

    .line 26
    :cond_1
    cmpl-float v5, v1, v3

    if-nez v5, :cond_2

    .line 27
    const v1, 0x3e99999a    # 0.3f

    .line 29
    :cond_2
    cmpl-float v3, v0, v3

    if-eqz v3, :cond_3

    cmpg-float v3, v0, v4

    if-gez v3, :cond_4

    .line 30
    :cond_3
    const/high16 v0, 0x3f800000    # 1.0f

    .line 31
    const/high16 v3, 0x40800000    # 4.0f

    div-float v2, v1, v3

    .line 35
    .local v2, "s":F
    :goto_1
    float-to-double v5, v0

    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    const/high16 v3, 0x41200000    # 10.0f

    sub-float/2addr p1, v4

    mul-float/2addr v3, p1

    float-to-double v9, v3

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v7

    mul-double/2addr v5, v7

    .line 36
    mul-float v3, p1, v4

    sub-float/2addr v3, v2

    mul-float/2addr v3, v12

    mul-float/2addr v3, v11

    div-float/2addr v3, v1

    invoke-static {v3}, Landroid/util/FloatMath;->sin(F)F

    move-result v3

    float-to-double v3, v3

    .line 35
    mul-double/2addr v3, v5

    neg-double v3, v3

    .line 36
    const-wide/16 v5, 0x0

    .line 35
    add-double/2addr v3, v5

    double-to-float v3, v3

    goto :goto_0

    .line 33
    .end local v2    # "s":F
    :cond_4
    div-float v3, v1, v12

    mul-float/2addr v3, v11

    float-to-double v5, v3

    div-float v3, v4, v0

    float-to-double v7, v3

    invoke-static {v7, v8}, Ljava/lang/Math;->asin(D)D

    move-result-wide v7

    mul-double/2addr v5, v7

    double-to-float v2, v5

    .restart local v2    # "s":F
    goto :goto_1
.end method
