.class public Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;
.super Landroid/widget/SimpleCursorAdapter;
.source "MyPlaceCarSimpleCursorAdapter.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;
.implements Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;
    }
.end annotation


# static fields
.field protected static final FR_RESULTSET_COLUMNS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "[MyPlaceCarSimpleCursorAdapter]"

.field private static final TO_VIEW_RESOURCE_ID:[I


# instance fields
.field private carItemOnClickListener:Landroid/view/View$OnClickListener;

.field private carItemSettingsOnClickListener:Landroid/view/View$OnClickListener;

.field protected disconnectBtAddress:Ljava/lang/String;

.field protected disconnectName:Ljava/lang/String;

.field protected isDialogOpen:Z

.field private mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

.field private mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field protected pendingTasks:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 48
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 49
    const-string/jumbo v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "bluetooth_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 50
    const-string/jumbo v2, "bluetooth_mac_address"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    .line 48
    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->FR_RESULTSET_COLUMNS:[Ljava/lang/String;

    .line 51
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->TO_VIEW_RESOURCE_ID:[I

    .line 53
    return-void

    .line 51
    nop

    :array_0
    .array-data 4
        0x1020016
        0x7f09024d
        0x7f09024e
        0x7f09024f
    .end array-data
.end method

.method public constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;)V
    .locals 7
    .param p1, "context"    # Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    .prologue
    .line 88
    const v2, 0x7f030077

    const/4 v3, 0x0

    .line 89
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->FR_RESULTSET_COLUMNS:[Ljava/lang/String;

    .line 90
    sget-object v5, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->TO_VIEW_RESOURCE_ID:[I

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 91
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    .line 92
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->pendingTasks:Ljava/util/Queue;

    .line 95
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 94
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 96
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 97
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestBluetoothStartAdapter(Landroid/content/Context;)V

    .line 98
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 99
    invoke-interface {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkConnectivityListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;)V

    .line 101
    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$1;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$1;-><init>(Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->carItemOnClickListener:Landroid/view/View$OnClickListener;

    .line 128
    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$2;-><init>(Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->carItemSettingsOnClickListener:Landroid/view/View$OnClickListener;

    .line 159
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;)V
    .locals 0

    .prologue
    .line 220
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->doNextTask()V

    return-void
.end method

.method private doNextTask()V
    .locals 5

    .prologue
    .line 221
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->pendingTasks:Ljava/util/Queue;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->pendingTasks:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 222
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->pendingTasks:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;

    .line 224
    .local v1, "task":Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;
    if-eqz v1, :cond_0

    .line 226
    iget-object v2, v1, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;->btAddress:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->getPairedBluetoothDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .line 227
    .local v0, "bluetoothDevice":Landroid/bluetooth/BluetoothDevice;
    if-eqz v0, :cond_2

    .line 228
    iget-boolean v2, v1, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;->connect:Z

    if-eqz v2, :cond_1

    .line 229
    const-string/jumbo v2, "[MyPlaceCarSimpleCursorAdapter]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Request BT Connect:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 231
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    invoke-interface {v2, v3, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestBluetoothConnectDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)V

    .line 247
    .end local v0    # "bluetoothDevice":Landroid/bluetooth/BluetoothDevice;
    .end local v1    # "task":Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;
    :cond_0
    :goto_0
    return-void

    .line 234
    .restart local v0    # "bluetoothDevice":Landroid/bluetooth/BluetoothDevice;
    .restart local v1    # "task":Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;
    :cond_1
    const-string/jumbo v2, "[MyPlaceCarSimpleCursorAdapter]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Request BT Disconnect:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 237
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    .line 236
    invoke-interface {v2, v3, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestBluetoothDisconnectDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_0

    .line 241
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->pendingTasks:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    .line 242
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->refreshList()V

    .line 243
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->doNextTask()V

    goto :goto_0
.end method


# virtual methods
.method protected connect(Ljava/lang/String;Landroid/view/View;Landroid/widget/TextView;)V
    .locals 3
    .param p1, "btAddress"    # Ljava/lang/String;
    .param p2, "listItem"    # Landroid/view/View;
    .param p3, "tvSummary"    # Landroid/widget/TextView;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 261
    invoke-virtual {p2, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 262
    const v0, 0x1020016

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 263
    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 264
    const v0, 0x7f090250

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 265
    const v0, 0x1020006

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 266
    const v0, 0x7f0a03e7

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(I)V

    .line 267
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->pendingTasks:Ljava/util/Queue;

    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;

    invoke-direct {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 268
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->pendingTasks:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 269
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->doNextTask()V

    .line 271
    :cond_0
    return-void
.end method

.method protected disconnect(Ljava/lang/String;Landroid/view/View;Landroid/widget/TextView;)V
    .locals 3
    .param p1, "btAddress"    # Ljava/lang/String;
    .param p2, "listItem"    # Landroid/view/View;
    .param p3, "tvSummary"    # Landroid/widget/TextView;

    .prologue
    const/4 v2, 0x0

    .line 286
    invoke-virtual {p2, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 288
    const v1, 0x1020016

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 287
    check-cast v0, Landroid/widget/TextView;

    .line 289
    .local v0, "title":Landroid/widget/TextView;
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 290
    invoke-virtual {p3, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 291
    const v1, 0x1020006

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 292
    const v1, 0x7f0a03ed

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(I)V

    .line 293
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->disconnectName:Ljava/lang/String;

    .line 294
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->disconnectBtAddress:Ljava/lang/String;

    .line 295
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->showDisconnectDialog()V

    .line 296
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v10, 0x0

    .line 169
    invoke-super {p0, p1, p2, p3}, Landroid/widget/SimpleCursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 171
    .local v1, "carItemView":Landroid/view/View;
    const v7, 0x1020016

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 170
    check-cast v6, Landroid/widget/TextView;

    .line 173
    .local v6, "tvTitle":Landroid/widget/TextView;
    const v7, 0x1020010

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 172
    check-cast v5, Landroid/widget/TextView;

    .line 175
    .local v5, "tvSummary":Landroid/widget/TextView;
    const v7, 0x7f09024e

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 174
    check-cast v4, Landroid/widget/TextView;

    .line 177
    .local v4, "tvSettingsCarMacAddress":Landroid/widget/TextView;
    const v7, 0x7f090250

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 178
    .local v2, "layoutSettingsIcon":Landroid/view/View;
    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 179
    .local v0, "btAddress":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    new-instance v8, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    const-string/jumbo v9, ""

    .line 180
    invoke-direct {v8, v9, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-virtual {v7, v8}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->isConnectedDevice(Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;)Z

    move-result v7

    .line 180
    if-eqz v7, :cond_2

    .line 181
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 182
    const v8, 0x7f08008b

    .line 181
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 183
    const v7, 0x7f0a03e6

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(I)V

    .line 189
    :goto_0
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->carItemSettingsOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->carItemOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->pendingTasks:Ljava/util/Queue;

    invoke-interface {v7}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_3

    .line 209
    :cond_1
    :goto_1
    return-object v1

    .line 185
    :cond_2
    const v7, 0x7f0a03ed

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 192
    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;

    .line 193
    .local v3, "pendingTask":Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;
    iget-object v8, v3, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;->btAddress:Ljava/lang/String;

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 195
    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 196
    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 197
    invoke-virtual {v2, v10}, Landroid/view/View;->setEnabled(Z)V

    .line 198
    const v7, 0x1020006

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/view/View;->setEnabled(Z)V

    .line 200
    invoke-virtual {v1, v10}, Landroid/view/View;->setEnabled(Z)V

    .line 201
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 202
    const v8, 0x7f08008e

    .line 201
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 203
    iget-boolean v7, v3, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$Task;->connect:Z

    if-eqz v7, :cond_1

    .line 204
    const v7, 0x7f0a03e7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method

.method public onBondStateChanged(Landroid/bluetooth/BluetoothDevice;I)V
    .locals 1
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "state"    # I

    .prologue
    .line 398
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->refreshList()V

    .line 399
    return-void
.end method

.method public onConnect(Landroid/bluetooth/BluetoothDevice;)V
    .locals 1
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 372
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->refreshList()V

    .line 373
    return-void
.end method

.method public onDisconnect(Landroid/bluetooth/BluetoothDevice;)V
    .locals 1
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->refreshList()V

    .line 386
    return-void
.end method

.method public onNotifyBluetoothDeviceConnected()V
    .locals 0

    .prologue
    .line 499
    return-void
.end method

.method public onNotifyBluetoothDeviceDisconnected()V
    .locals 0

    .prologue
    .line 505
    return-void
.end method

.method public onNotifyBluetoothDevicePaired()V
    .locals 0

    .prologue
    .line 487
    return-void
.end method

.method public onNotifyBluetoothDeviceUnpaired()V
    .locals 0

    .prologue
    .line 493
    return-void
.end method

.method public onResponseRequestBluetoothConnectDevice(Z)V
    .locals 1
    .param p1, "result"    # Z

    .prologue
    .line 457
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->pendingTasks:Ljava/util/Queue;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->pendingTasks:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 458
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->pendingTasks:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    .line 460
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    if-eqz v0, :cond_1

    .line 461
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->refreshList()V

    .line 462
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->doNextTask()V

    .line 464
    :cond_1
    return-void
.end method

.method public onResponseRequestBluetoothConnectionState(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 481
    return-void
.end method

.method public onResponseRequestBluetoothDisconnectDevice(Z)V
    .locals 1
    .param p1, "result"    # Z

    .prologue
    .line 468
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->pendingTasks:Ljava/util/Queue;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->pendingTasks:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->pendingTasks:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    .line 471
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    if-eqz v0, :cond_1

    .line 472
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->refreshList()V

    .line 473
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->doNextTask()V

    .line 475
    :cond_1
    return-void
.end method

.method public onResponseRequestBluetoothMakeDeviceDiscoverable()V
    .locals 0

    .prologue
    .line 417
    return-void
.end method

.method public onResponseRequestBluetoothPairDevice(I)V
    .locals 0
    .param p1, "result"    # I

    .prologue
    .line 447
    return-void
.end method

.method public onResponseRequestBluetoothPairingNSSPAnswer(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 435
    return-void
.end method

.method public onResponseRequestBluetoothPairingSSPAnswer(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 441
    return-void
.end method

.method public onResponseRequestBluetoothSearchDiscoverableDevices()V
    .locals 0

    .prologue
    .line 423
    return-void
.end method

.method public onResponseRequestBluetoothSearchPairedDevices()V
    .locals 0

    .prologue
    .line 429
    return-void
.end method

.method public onResponseRequestBluetoothStartAdapter(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 405
    return-void
.end method

.method public onResponseRequestBluetoothStopAdapter()V
    .locals 0

    .prologue
    .line 411
    return-void
.end method

.method public onResponseRequestBluetoothUnpairDevice(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 453
    return-void
.end method

.method protected showDisconnectDialog()V
    .locals 7

    .prologue
    .line 299
    new-instance v0, Landroid/app/AlertDialog$Builder;

    .line 300
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    const/4 v3, 0x4

    .line 299
    invoke-direct {v0, v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 301
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0a03ea

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 302
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->mContext:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;

    .line 303
    const v3, 0x7f0a03eb

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;->disconnectName:Ljava/lang/String;

    aput-object v6, v4, v5

    .line 302
    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 305
    const v2, 0x7f0a043a

    .line 306
    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$3;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$3;-><init>(Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;)V

    .line 305
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 323
    const v2, 0x7f0a043b

    .line 324
    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$4;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$4;-><init>(Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;)V

    .line 323
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 335
    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$5;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$5;-><init>(Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 350
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 352
    .local v1, "dialog":Landroid/app/AlertDialog;
    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$6;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter$6;-><init>(Lcom/sec/android/automotive/drivelink/settings/MyPlaceCarSimpleCursorAdapter;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 360
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 361
    return-void
.end method
