.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
.source "DLPhoneNumberImpl.java"


# static fields
.field private static final serialVersionUID:J = 0x379d90ccfa43bd0cL


# instance fields
.field private mIsMainPhoneNumber:Z

.field private mIsMyPhoneNumber:Z

.field private mPhoneNumber:Ljava/lang/String;

.field private mType:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;-><init>()V

    .line 12
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;->mType:I

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;->mPhoneNumber:Ljava/lang/String;

    .line 14
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;->mIsMyPhoneNumber:Z

    .line 15
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;->mIsMainPhoneNumber:Z

    .line 19
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;->mType:I

    .line 20
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;->mPhoneNumber:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public checkIsMainPhoneNumber(Ljava/lang/String;)V
    .locals 5
    .param p1, "mainPhoneNumber"    # Ljava/lang/String;

    .prologue
    .line 43
    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;->mPhoneNumber:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 45
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->getStrippedReversed(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "phoneNumber_1":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;->mPhoneNumber:Ljava/lang/String;

    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->getStrippedReversed(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 49
    .local v1, "phoneNumber_2":Ljava/lang/String;
    const-string/jumbo v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "[snowdeer] phoneNumber 1 : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    const-string/jumbo v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "[snowdeer] phoneNumber 2 : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 54
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;->mIsMainPhoneNumber:Z

    .line 58
    .end local v0    # "phoneNumber_1":Ljava/lang/String;
    .end local v1    # "phoneNumber_2":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;->mPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneType()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;->mType:I

    return v0
.end method

.method public isMainPhoneNumber()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;->mIsMainPhoneNumber:Z

    return v0
.end method

.method public isMyPhoneNumber()Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;->mIsMyPhoneNumber:Z

    return v0
.end method

.method public setIsMyPhoneNumber(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;->mIsMyPhoneNumber:Z

    .line 40
    return-void
.end method
