.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationGetMyPlacesDialog;
.super Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialogFragment;
.source "NotificationGetMyPlacesDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/notification/view/NotificationGetMyPlacesDialog$ClickListener;
    }
.end annotation


# static fields
.field public static TAG:Ljava/lang/String;


# instance fields
.field private mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGetMyPlacesDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGetMyPlacesDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 23
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGetMyPlacesDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 24
    const v2, 0x7f0a02f4

    .line 25
    const v3, 0x7f0a02f3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;-><init>(Landroid/content/Context;II)V

    .line 23
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGetMyPlacesDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    .line 27
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGetMyPlacesDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    const v1, 0x7f0a02f2

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->setTitle(I)V

    .line 28
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGetMyPlacesDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    const v1, 0x7f0a02c0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->setMessage(I)V

    .line 30
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGetMyPlacesDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGetMyPlacesDialog$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGetMyPlacesDialog$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationGetMyPlacesDialog;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->setOnClickListener(Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog$BasicDialogListener;)V

    .line 42
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGetMyPlacesDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method
