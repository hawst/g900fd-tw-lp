.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationSendingDialogView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field static TAG:Ljava/lang/String;

.field private static isDebug:Z


# instance fields
.field private TIMEOUT:I

.field protected mContext:Landroid/content/Context;

.field protected mViewLocal:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-string/jumbo v0, "NotificationSendingDialogView"

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->TAG:Ljava/lang/String;

    .line 163
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->isDebug:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 23
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->mContext:Landroid/content/Context;

    .line 24
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->mViewLocal:Landroid/view/View;

    .line 25
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->TIMEOUT:I

    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 23
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->mContext:Landroid/content/Context;

    .line 24
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->mViewLocal:Landroid/view/View;

    .line 25
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->TIMEOUT:I

    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 23
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->mContext:Landroid/content/Context;

    .line 24
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->mViewLocal:Landroid/view/View;

    .line 25
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->TIMEOUT:I

    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method private static Debug(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 166
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->isDebug:Z

    if-eqz v0, :cond_0

    .line 167
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->TAG:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :cond_0
    return-void
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v6, 0x7f0902e6

    .line 49
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->mContext:Landroid/content/Context;

    .line 51
    const/4 v2, 0x0

    .line 52
    .local v2, "sendInfo":Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 53
    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 52
    check-cast v0, Landroid/view/LayoutInflater;

    .line 55
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->mViewLocal:Landroid/view/View;

    if-nez v3, :cond_1

    .line 57
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "isMultiMode"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->isMultiMode:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->Debug(Ljava/lang/String;)V

    .line 58
    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->isMultiMode:Z

    if-eqz v3, :cond_2

    .line 60
    const v3, 0x7f0300ca

    .line 59
    invoke-virtual {v0, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->mViewLocal:Landroid/view/View;

    .line 66
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "sendInfo":Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;
    check-cast v2, Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;

    .line 69
    .restart local v2    # "sendInfo":Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;
    :cond_0
    if-eqz v2, :cond_1

    .line 70
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;->getSendingType()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 71
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->mViewLocal:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 72
    const/16 v4, 0x8

    .line 71
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 73
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->mViewLocal:Landroid/view/View;

    const v4, 0x7f0902ce

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 74
    const v4, 0x7f0a038e

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addBegin()V

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    const v4, 0x7f06000f

    const/16 v5, 0x14

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addSound(II)V

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->TIMEOUT:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addFinish()V

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdFlowStart()V

    .line 110
    :cond_1
    :goto_1
    return-void

    .line 63
    :cond_2
    const v3, 0x7f0300c9

    .line 62
    invoke-virtual {v0, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->mViewLocal:Landroid/view/View;

    goto :goto_0

    .line 82
    :cond_3
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;->getSendingType()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 83
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->isAppeared()Z

    move-result v3

    if-nez v3, :cond_1

    .line 84
    const-string/jumbo v3, "sending !!!"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->Debug(Ljava/lang/String;)V

    .line 85
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v3

    .line 86
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->mContext:Landroid/content/Context;

    .line 87
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;->getDLMessage()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    move-result-object v5

    .line 86
    invoke-interface {v3, v4, v5}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestSendMessage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)V

    .line 89
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->mContext:Landroid/content/Context;

    .line 90
    const v4, 0x7f040030

    .line 89
    invoke-static {v3, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 91
    .local v1, "rotation":Landroid/view/animation/Animation;
    const/4 v3, -0x1

    invoke-virtual {v1, v3}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 92
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->mViewLocal:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addBegin()V

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    .line 97
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->mContext:Landroid/content/Context;

    const v5, 0x7f0a0241

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 96
    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTS(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->TIMEOUT:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addFinish()V

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSendingDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdFlowStart()V

    goto :goto_1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 116
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 121
    .local v0, "id":I
    return-void
.end method
