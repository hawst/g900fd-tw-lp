.class public Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;
.super Landroid/app/Dialog;
.source "ProgressLoadingDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private btNavBack:Landroid/widget/LinearLayout;

.field private context:Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;

.field private poiType:Ljava/lang/String;

.field private tvTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;
    .param p2, "poiType"    # Ljava/lang/String;

    .prologue
    .line 26
    const v0, 0x7f0c0003

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 27
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;->context:Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;

    .line 28
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;->poiType:Ljava/lang/String;

    .line 29
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;->requestWindowFeature(I)Z

    .line 30
    const v0, 0x7f030075

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;->setContentView(I)V

    .line 31
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;->setCancelable(Z)V

    .line 32
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 49
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 55
    :goto_0
    return-void

    .line 51
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;->dismiss()V

    .line 52
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;->context:Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->finish()V

    goto :goto_0

    .line 49
    nop

    :pswitch_data_0
    .packed-switch 0x7f0900f3
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 36
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const v2, 0x7f0900f3

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;->btNavBack:Landroid/widget/LinearLayout;

    .line 39
    const v2, 0x7f090027

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;->tvTitle:Landroid/widget/TextView;

    .line 40
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;->poiType:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->loadStringByName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 41
    .local v1, "title":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 42
    const v3, 0x7f0a0302

    .line 41
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "mask":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;->tvTitle:Landroid/widget/TextView;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/ProgressLoadingDialog;->btNavBack:Landroid/widget/LinearLayout;

    invoke-virtual {v2, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    return-void
.end method
