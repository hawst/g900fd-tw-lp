.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothStopDiscoveringDevices;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestBluetoothStopDiscoveringDevices.java"


# instance fields
.field mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .prologue
    .line 14
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 16
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothStopDiscoveringDevices;->mContext:Landroid/content/Context;

    .line 17
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothStopDiscoveringDevices;->mContext:Landroid/content/Context;

    .line 40
    return-void
.end method

.method protected process()V
    .locals 2

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothStopDiscoveringDevices;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 23
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getConnectivityManager()Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v0

    .line 24
    .local v0, "connectivityManager":Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothStopDiscoveringDevices;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->stopDiscoveringDevices(Landroid/content/Context;)Z

    .line 25
    return-void
.end method

.method protected response()V
    .locals 0

    .prologue
    .line 35
    return-void
.end method
