.class public abstract Lcom/sec/android/automotive/drivelink/framework/command/Request;
.super Ljava/lang/Object;
.source "Request.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;
.implements Ljava/lang/Runnable;


# static fields
.field private static final TAG:Ljava/lang/String; = "Framework-Request"

.field protected static mCallbackHandler:Landroid/os/Handler;


# instance fields
.field private mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/command/Request;->mCallbackHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V
    .locals 1
    .param p1, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/Request;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 15
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/Request;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 16
    return-void
.end method


# virtual methods
.method public execute()V
    .locals 6

    .prologue
    .line 24
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 25
    .local v0, "startTime":J
    const-string/jumbo v2, "Framework-Request"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/Request;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, " => start time : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/Request;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    .line 38
    :goto_0
    return-void

    .line 31
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/Request;->process()V

    .line 33
    const-string/jumbo v2, "Framework-Request"

    .line 34
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/Request;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, " => execute time : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 35
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 34
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 33
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/command/Request;->mCallbackHandler:Landroid/os/Handler;

    invoke-virtual {v2, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected abstract getClassName()Ljava/lang/String;
.end method

.method protected getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/Request;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    return-object v0
.end method

.method protected abstract postprocess()V
.end method

.method protected abstract process()V
.end method

.method protected abstract response()V
.end method

.method public run()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/Request;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    :goto_0
    return-void

    .line 46
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/Request;->response()V

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/Request;->postprocess()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/Request;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    goto :goto_0
.end method
