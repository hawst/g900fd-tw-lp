.class Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$6;
.super Ljava/lang/Object;
.source "SettingsRegisterCarOptionsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->showRenameDialog(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

.field private final synthetic val$inputMethodManager:Landroid/view/inputmethod/InputMethodManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;Landroid/view/inputmethod/InputMethodManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$6;->val$inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$6;)Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    return-object v0
.end method


# virtual methods
.method public onShow(Landroid/content/DialogInterface;)V
    .locals 5
    .param p1, "dialogInterface"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v4, 0x1

    .line 284
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)Landroid/app/AlertDialog;

    move-result-object v1

    .line 285
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 287
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->isDialogOkEnabled:Z
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->access$4(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)Z

    move-result v2

    .line 286
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 288
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    .line 289
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    .line 288
    if-ne v4, v1, :cond_0

    .line 290
    const-string/jumbo v1, "JINSEIL"

    const-string/jumbo v2, "SettingsRegisterCarOptionAcitivity onShow!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 292
    .local v0, "h":Landroid/os/Handler;
    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$6$1;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$6;->val$inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$6$1;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$6;Landroid/view/inputmethod/InputMethodManager;)V

    .line 300
    const-wide/16 v2, 0x32

    .line 292
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 307
    .end local v0    # "h":Landroid/os/Handler;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    invoke-static {v1, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->access$5(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;Z)V

    .line 308
    return-void
.end method
