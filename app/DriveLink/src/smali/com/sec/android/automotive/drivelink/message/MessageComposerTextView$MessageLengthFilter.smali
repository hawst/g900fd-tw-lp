.class public Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;
.super Ljava/lang/Object;
.source "MessageComposerTextView.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MessageLengthFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)V
    .locals 0

    .prologue
    .line 410
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 412
    return-void
.end method

.method private showMaxNumExceeded()V
    .locals 6

    .prologue
    const v5, 0x7f0a03b1

    const/4 v4, 0x0

    .line 474
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 475
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 477
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v0

    .line 478
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->access$5(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 476
    invoke-static {v0, v1, v4}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    .line 480
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 491
    :goto_0
    return-void

    .line 482
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    const-string/jumbo v1, "w"

    .line 483
    const-string/jumbo v2, "filter"

    .line 484
    const-string/jumbo v3, "DLToast DLApplication.getInstance() == null or DLApplication.getInstance().getCurrentActivity() == null"

    .line 482
    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->access$5(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 487
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->access$5(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 485
    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 489
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 14
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    .line 417
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    const-string/jumbo v10, "w"

    const-string/jumbo v11, "filter"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "start = "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " end ="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 418
    const-string/jumbo v13, " dstart ="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p5

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " dend ="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p6

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 417
    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v9, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    new-instance v9, Ljava/lang/StringBuilder;

    const/4 v10, 0x0

    move-object/from16 v0, p4

    move/from16 v1, p5

    invoke-interface {v0, v10, v1}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 421
    invoke-interface/range {p1 .. p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 422
    invoke-interface/range {p4 .. p4}, Landroid/text/Spanned;->length()I

    move-result v10

    move-object/from16 v0, p4

    move/from16 v1, p6

    invoke-interface {v0, v1, v10}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 420
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 423
    .local v5, "message":Ljava/lang/String;
    const/4 v9, 0x0

    invoke-static {v5, v9}, Landroid/telephony/gsm/SmsMessage;->calculateLength(Ljava/lang/String;Z)[I

    move-result-object v2

    .line 424
    .local v2, "Params":[I
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    const-string/jumbo v10, "i"

    const-string/jumbo v11, "filter"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "total [0] = "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v13, 0x0

    aget v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " Params[1] ="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 425
    const/4 v13, 0x1

    aget v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " Params[2] ="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x2

    aget v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " Params[3] ="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 426
    const/4 v13, 0x3

    aget v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 424
    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v9, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    const/4 v9, 0x0

    aget v6, v2, v9

    .line 430
    .local v6, "nPage":I
    const/4 v9, 0x1

    if-gt v6, v9, :cond_0

    .line 431
    const/4 v9, 0x0

    .line 469
    :goto_0
    return-object v9

    .line 434
    :cond_0
    move/from16 v3, p2

    .local v3, "i":I
    :goto_1
    move/from16 v0, p3

    if-lt v3, v0, :cond_2

    .line 462
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    const-string/jumbo v10, "i"

    const-string/jumbo v11, "filter"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "nPage ="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v9, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->ViewingToast:Z
    invoke-static {v9}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->access$3(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 465
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mHandler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->access$4(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)Landroid/os/Handler;

    move-result-object v9

    const/16 v10, 0x64

    const-wide/16 v11, 0x7d0

    invoke-virtual {v9, v10, v11, v12}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 466
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;Z)V

    .line 467
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;->showMaxNumExceeded()V

    .line 469
    :cond_1
    const-string/jumbo v9, ""

    goto :goto_0

    .line 436
    :cond_2
    new-instance v9, Ljava/lang/StringBuilder;

    const/4 v10, 0x0

    move-object/from16 v0, p4

    move/from16 v1, p5

    invoke-interface {v0, v10, v1}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 437
    move/from16 v0, p2

    invoke-interface {p1, v0, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 438
    invoke-interface/range {p4 .. p4}, Landroid/text/Spanned;->length()I

    move-result v10

    move-object/from16 v0, p4

    move/from16 v1, p6

    invoke-interface {v0, v1, v10}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 436
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 440
    .local v7, "subMessage":Ljava/lang/String;
    const/4 v9, 0x0

    .line 439
    invoke-static {v7, v9}, Landroid/telephony/gsm/SmsMessage;->calculateLength(Ljava/lang/String;Z)[I

    move-result-object v8

    .line 442
    .local v8, "subParams":[I
    const/4 v9, 0x0

    aget v9, v8, v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_5

    .line 443
    add-int/lit8 v4, v3, -0x1

    .line 444
    .local v4, "iNum":I
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    const-string/jumbo v10, "i"

    const-string/jumbo v11, "filter"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "iNum = "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " start ="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 445
    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " end ="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 444
    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v9, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    sub-int v9, p3, p2

    if-ge v4, v9, :cond_3

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->ViewingToast:Z
    invoke-static {v9}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->access$3(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 448
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mHandler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->access$4(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)Landroid/os/Handler;

    move-result-object v9

    const/16 v10, 0x64

    const-wide/16 v11, 0x7d0

    invoke-virtual {v9, v10, v11, v12}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 449
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;Z)V

    .line 450
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageLengthFilter;->showMaxNumExceeded()V

    .line 453
    :cond_3
    move/from16 v0, p2

    if-le v4, v0, :cond_4

    .line 454
    move/from16 v0, p2

    invoke-interface {p1, v0, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v9

    goto/16 :goto_0

    .line 456
    :cond_4
    const-string/jumbo v9, ""

    goto/16 :goto_0

    .line 434
    .end local v4    # "iNum":I
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1
.end method
