.class public Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLDownloadData;
.source "DownloadDataImpl.java"


# instance fields
.field private mDownloadRate:J

.field private mDownloadSize:J

.field private mTotalSize:J


# direct methods
.method public constructor <init>(JJJ)V
    .locals 2
    .param p1, "totalSize"    # J
    .param p3, "downloadSize"    # J
    .param p5, "downloadRate"    # J

    .prologue
    const-wide/16 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLDownloadData;-><init>()V

    .line 6
    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;->mTotalSize:J

    .line 7
    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;->mDownloadSize:J

    .line 8
    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;->mDownloadRate:J

    .line 12
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;->mTotalSize:J

    .line 13
    iput-wide p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;->mDownloadSize:J

    .line 14
    iput-wide p5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;->mDownloadRate:J

    .line 15
    return-void
.end method


# virtual methods
.method public getDownloadRate()J
    .locals 2

    .prologue
    .line 32
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;->mDownloadRate:J

    return-wide v0
.end method

.method public getDownloadSize()J
    .locals 2

    .prologue
    .line 26
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;->mDownloadSize:J

    return-wide v0
.end method

.method public getTotalSize()J
    .locals 2

    .prologue
    .line 20
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;->mTotalSize:J

    return-wide v0
.end method
