.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationSmartAlertDialogView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# static fields
.field static final TAG:Ljava/lang/String; = "NotificationSmartAlertDialogView"

.field static final TIME_OUT:I = 0x14

.field private static isDebug:Z


# instance fields
.field protected mContext:Landroid/content/Context;

.field mTvDescription:Landroid/widget/TextView;

.field mTvDistanceAhead:Landroid/widget/TextView;

.field mTvDistanceBackup:Landroid/widget/TextView;

.field mTvIncident:Landroid/widget/TextView;

.field protected mViewLocal:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 275
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->isDebug:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 28
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mContext:Landroid/content/Context;

    .line 29
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mViewLocal:Landroid/view/View;

    .line 42
    invoke-virtual {p4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    .line 44
    .local v0, "notiInfo":Ljava/lang/Object;
    const-class v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;

    .end local v0    # "notiInfo":Ljava/lang/Object;
    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->initIndicatorView(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;)V

    .line 47
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 28
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mContext:Landroid/content/Context;

    .line 29
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mViewLocal:Landroid/view/View;

    .line 54
    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    .line 56
    .local v0, "notiInfo":Ljava/lang/Object;
    const-class v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;

    .end local v0    # "notiInfo":Ljava/lang/Object;
    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->initIndicatorView(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;)V

    .line 59
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v2, 0x0

    .line 63
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 28
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mContext:Landroid/content/Context;

    .line 29
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mViewLocal:Landroid/view/View;

    .line 67
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v1

    .line 69
    .local v1, "notiInfo":Ljava/lang/Object;
    const-class v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;

    invoke-virtual {v2, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 70
    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;

    .end local v1    # "notiInfo":Ljava/lang/Object;
    invoke-direct {p0, p1, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->initIndicatorView(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;)V

    .line 89
    :goto_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mViewLocal:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 90
    return-void

    .line 75
    .restart local v1    # "notiInfo":Ljava/lang/Object;
    :cond_0
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mContext:Landroid/content/Context;

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 77
    const-string/jumbo v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 76
    check-cast v0, Landroid/view/LayoutInflater;

    .line 79
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f0300cf

    .line 78
    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mViewLocal:Landroid/view/View;

    .line 83
    const v2, 0x7f090309

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mTvIncident:Landroid/widget/TextView;

    .line 84
    const v2, 0x7f09030c

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mTvDescription:Landroid/widget/TextView;

    .line 85
    const v2, 0x7f09030a

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mTvDistanceAhead:Landroid/widget/TextView;

    .line 86
    const v2, 0x7f09030b

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mTvDistanceBackup:Landroid/widget/TextView;

    goto :goto_0
.end method

.method private static Debug(Ljava/lang/String;)V
    .locals 3
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 278
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->isDebug:Z

    if-eqz v0, :cond_0

    .line 279
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "NotificationSmartAlertDialogView"

    invoke-static {v0, v1, v2, p0}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    :cond_0
    return-void
.end method

.method private initIndicatorView(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;)V
    .locals 23
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dlIncidentInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;

    .prologue
    .line 101
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mContext:Landroid/content/Context;

    .line 102
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->getContext()Landroid/content/Context;

    move-result-object v17

    .line 103
    const-string/jumbo v18, "layout_inflater"

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    .line 102
    check-cast v13, Landroid/view/LayoutInflater;

    .line 104
    .local v13, "inflater":Landroid/view/LayoutInflater;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mViewLocal:Landroid/view/View;

    move-object/from16 v17, v0

    if-nez v17, :cond_0

    .line 105
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->isMultiMode:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1

    .line 107
    const v17, 0x7f0300cf

    .line 106
    move/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v13, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mViewLocal:Landroid/view/View;

    .line 114
    :goto_0
    const v17, 0x7f090309

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mTvIncident:Landroid/widget/TextView;

    .line 115
    const v17, 0x7f09030c

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mTvDescription:Landroid/widget/TextView;

    .line 116
    const v17, 0x7f09030a

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mTvDistanceAhead:Landroid/widget/TextView;

    .line 117
    const v17, 0x7f09030b

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mTvDistanceBackup:Landroid/widget/TextView;

    .line 126
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;->getType()I

    move-result v14

    .line 127
    .local v14, "mIncidentType":I
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;->getFullDescription()Ljava/lang/String;

    move-result-object v4

    .line 128
    .local v4, "IncidentDescription":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mTvDescription:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;->getDistance()D

    move-result-wide v2

    .line 130
    .local v2, "Distance":D
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;->getDelayImpactDistance()D

    move-result-wide v9

    .line 132
    .local v9, "StagnantSection":D
    const-string/jumbo v11, ""

    .line 163
    .local v11, "description":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mTvIncident:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mTvDistanceAhead:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const-string/jumbo v18, "%.2f"

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v21

    aput-object v21, v19, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mTvDistanceBackup:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const-string/jumbo v18, "%.2f"

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    .line 166
    invoke-static {v9, v10}, Ljava/lang/Math;->abs(D)D

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v21

    aput-object v21, v19, v20

    .line 165
    invoke-static/range {v18 .. v19}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;->getLatitude()D

    move-result-wide v5

    .line 169
    .local v5, "Latitude":D
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;->getLongitude()D

    move-result-wide v7

    .line 171
    .local v7, "Logitude":D
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, "/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, "/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v17

    .line 172
    const-string/jumbo v18, "/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, "/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, "/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v17

    .line 171
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->Debug(Ljava/lang/String;)V

    .line 174
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    .line 175
    const v18, 0x7f0a022e

    .line 174
    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 176
    .local v15, "prompt":Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 177
    .local v12, "incidentDsc":Ljava/lang/String;
    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-object v12, v17, v18

    move-object/from16 v0, v17

    invoke-static {v15, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 179
    .local v16, "promptDefault":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    .line 180
    return-void

    .line 110
    .end local v2    # "Distance":D
    .end local v4    # "IncidentDescription":Ljava/lang/String;
    .end local v5    # "Latitude":D
    .end local v7    # "Logitude":D
    .end local v9    # "StagnantSection":D
    .end local v11    # "description":Ljava/lang/String;
    .end local v12    # "incidentDsc":Ljava/lang/String;
    .end local v14    # "mIncidentType":I
    .end local v15    # "prompt":Ljava/lang/String;
    .end local v16    # "promptDefault":Ljava/lang/String;
    :cond_1
    const v17, 0x7f0300ce

    .line 109
    move/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v13, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mViewLocal:Landroid/view/View;

    goto/16 :goto_0
.end method

.method public static isAppOnForeground(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 291
    const/4 v3, 0x0

    .line 293
    .local v3, "fareground":Z
    const-string/jumbo v6, "activity"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 292
    check-cast v0, Landroid/app/ActivityManager;

    .line 295
    .local v0, "activityManager":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    .line 296
    .local v2, "appProcesses":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    if-nez v2, :cond_0

    move v4, v3

    .line 307
    .end local v3    # "fareground":Z
    .local v4, "fareground":I
    :goto_0
    return v4

    .line 299
    .end local v4    # "fareground":I
    .restart local v3    # "fareground":Z
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 300
    .local v5, "packageName":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_2

    :goto_1
    move v4, v3

    .line 307
    .restart local v4    # "fareground":I
    goto :goto_0

    .line 300
    .end local v4    # "fareground":I
    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 301
    .local v1, "appProcess":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget v7, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v8, 0x64

    if-ne v7, v8, :cond_1

    .line 302
    iget-object v7, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 303
    const/4 v3, 0x1

    .line 304
    goto :goto_1
.end method

.method private startSmartAlertNotiFlow()V
    .locals 6

    .prologue
    .line 231
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    if-nez v0, :cond_0

    .line 244
    :goto_0
    return-void

    .line 234
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addBegin()V

    .line 235
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addAlert()V

    .line 236
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    .line 237
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mContext:Landroid/content/Context;

    const v2, 0x7f0a0223

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 238
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mTvIncident:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mTvDescription:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    .line 239
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mTvDistanceAhead:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->mTvDistanceBackup:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v3, v4

    .line 237
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTS(Ljava/lang/String;)V

    .line 241
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 242
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addFinish()V

    .line 243
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdFlowStart()V

    goto :goto_0
.end method


# virtual methods
.method public getDescription(I)Ljava/lang/String;
    .locals 1
    .param p1, "nType"    # I

    .prologue
    .line 254
    const/4 v0, 0x0

    .line 256
    .local v0, "str":Ljava/lang/String;
    return-object v0
.end method

.method public getDistanceAhead(I)Ljava/lang/String;
    .locals 1
    .param p1, "nType"    # I

    .prologue
    .line 261
    const/4 v0, 0x0

    .line 263
    .local v0, "str":Ljava/lang/String;
    return-object v0
.end method

.method public getDistanceBackup(I)Ljava/lang/String;
    .locals 1
    .param p1, "nType"    # I

    .prologue
    .line 268
    const/4 v0, 0x0

    .line 270
    .local v0, "str":Ljava/lang/String;
    return-object v0
.end method

.method public getIncident(I)Ljava/lang/String;
    .locals 1
    .param p1, "nType"    # I

    .prologue
    .line 247
    const/4 v0, 0x0

    .line 249
    .local v0, "str":Ljava/lang/String;
    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 212
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 220
    .local v0, "id":I
    return-void
.end method

.method public onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    .line 226
    invoke-super {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V

    .line 227
    return-void
.end method

.method public onNotificationDisappeared()V
    .locals 1

    .prologue
    .line 198
    const-string/jumbo v0, "onNotificationDisappeared"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->Debug(Ljava/lang/String;)V

    .line 199
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onNotificationDisappeared()V

    .line 200
    return-void
.end method

.method public onNotificationWillAppear()V
    .locals 2

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->getFlowState()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->RESTART:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    if-ne v0, v1, :cond_1

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->restartNotiFlow()V

    .line 192
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onNotificationWillAppear()V

    .line 193
    return-void

    .line 188
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->isAppeared()Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->startSmartAlertNotiFlow()V

    .line 190
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->setAppeared(Z)V

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->cancelNotiFlow()V

    .line 206
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSmartAlertDialogView;->dismiss()V

    .line 207
    const/4 v0, 0x0

    return v0
.end method
