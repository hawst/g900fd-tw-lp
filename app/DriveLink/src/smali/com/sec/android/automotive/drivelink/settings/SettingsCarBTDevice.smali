.class public Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;
.super Ljava/lang/Object;
.source "SettingsCarBTDevice.java"


# instance fields
.field private btName:Ljava/lang/String;

.field private id:I

.field private macAddress:Ljava/lang/String;

.field private placeName:Ljava/lang/String;


# direct methods
.method protected constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "placeName"    # Ljava/lang/String;
    .param p3, "deviceAddress"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p2, p3}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method protected constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "placeName"    # Ljava/lang/String;
    .param p3, "btName"    # Ljava/lang/String;
    .param p4, "deviceAddress"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->id:I

    .line 59
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->placeName:Ljava/lang/String;

    .line 60
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->btName:Ljava/lang/String;

    .line 61
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->macAddress:Ljava/lang/String;

    .line 62
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "placeName"    # Ljava/lang/String;
    .param p2, "deviceAddress"    # Ljava/lang/String;

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 35
    return-void
.end method


# virtual methods
.method protected getBtName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->btName:Ljava/lang/String;

    return-object v0
.end method

.method protected getId()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->id:I

    return v0
.end method

.method protected getMacAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->macAddress:Ljava/lang/String;

    return-object v0
.end method

.method protected getPlaceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->placeName:Ljava/lang/String;

    return-object v0
.end method

.method protected setBtName(Ljava/lang/String;)V
    .locals 0
    .param p1, "btName"    # Ljava/lang/String;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->btName:Ljava/lang/String;

    .line 143
    return-void
.end method

.method protected setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 88
    iput p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->id:I

    .line 89
    return-void
.end method

.method protected setMacAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "macAddress"    # Ljava/lang/String;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->macAddress:Ljava/lang/String;

    .line 170
    return-void
.end method

.method protected setPlaceName(Ljava/lang/String;)V
    .locals 0
    .param p1, "placeName"    # Ljava/lang/String;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->placeName:Ljava/lang/String;

    .line 116
    return-void
.end method
