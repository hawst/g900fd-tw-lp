.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationLocationShareEnded.java"


# static fields
.field protected static mContext:Landroid/content/Context;


# instance fields
.field private mImageView:Landroid/widget/ImageView;

.field private mViewLocal:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;->mViewLocal:Landroid/view/View;

    .line 22
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;->mImageView:Landroid/widget/ImageView;

    .line 39
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;->initIndicatorView(Landroid/content/Context;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;->mViewLocal:Landroid/view/View;

    .line 22
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;->mImageView:Landroid/widget/ImageView;

    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;->initIndicatorView(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;->mViewLocal:Landroid/view/View;

    .line 22
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;->mImageView:Landroid/widget/ImageView;

    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;->initIndicatorView(Landroid/content/Context;)V

    .line 28
    return-void
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v5, 0x7f02023b

    .line 43
    if-nez p1, :cond_0

    .line 89
    :goto_0
    return-void

    .line 47
    :cond_0
    sput-object p1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;->mContext:Landroid/content/Context;

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 49
    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 48
    check-cast v0, Landroid/view/LayoutInflater;

    .line 51
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;->mViewLocal:Landroid/view/View;

    if-nez v3, :cond_2

    .line 53
    const v3, 0x7f0300b5

    .line 52
    invoke-virtual {v0, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;->mViewLocal:Landroid/view/View;

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 55
    const v4, 0x7f0a038f

    .line 54
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    .line 62
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;->mViewLocal:Landroid/view/View;

    const v4, 0x7f0902cd

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;->mImageView:Landroid/widget/ImageView;

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 66
    const v4, 0x7f020238

    .line 64
    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 68
    .local v2, "shareBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 67
    invoke-static {v3, v2, v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 70
    .local v1, "maskedBitmap":Landroid/graphics/Bitmap;
    if-nez v1, :cond_1

    .line 71
    sget-object v3, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 72
    const v4, 0x7f02023d

    .line 71
    invoke-static {v3, v4, v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 75
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 76
    const v4, 0x7f02023c

    .line 75
    invoke-static {v3, v1, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageOverlay(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 78
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 82
    .end local v1    # "maskedBitmap":Landroid/graphics/Bitmap;
    .end local v2    # "shareBitmap":Landroid/graphics/Bitmap;
    :cond_2
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded$1;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareEnded;)V

    .line 88
    const-wide/16 v5, 0xbb8

    .line 82
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
