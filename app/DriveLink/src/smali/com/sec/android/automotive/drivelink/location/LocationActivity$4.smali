.class Lcom/sec/android/automotive/drivelink/location/LocationActivity$4;
.super Ljava/lang/Object;
.source "LocationActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getOnUpdateLocationSuggestionListListener()Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    .line 326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdateLocationSuggestionList(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 331
    .local p1, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    if-nez p1, :cond_0

    .line 337
    :goto_0
    return-void

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$4(Lcom/sec/android/automotive/drivelink/location/LocationActivity;Ljava/util/ArrayList;)V

    .line 336
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->updateLocationList()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$5(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    goto :goto_0
.end method
