.class Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$7;
.super Ljava/lang/Object;
.source "OnDriveLinkNotificationListenerImp.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->onNotifyLocationRequest(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;

.field private final synthetic val$_msgInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

.field private final synthetic val$_url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$7;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$7;->val$_msgInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$7;->val$_url:Ljava/lang/String;

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$7;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$7;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 143
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$7;->val$_msgInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$7;->val$_url:Ljava/lang/String;

    .line 142
    invoke-interface {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyLocationRequest(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Ljava/lang/String;)V

    .line 145
    :cond_0
    return-void
.end method
