.class public Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;
.super Ljava/lang/Object;
.source "HomeFirstFlowManager.java"


# static fields
.field private static mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getUserRegulationAgreement()Z
    .locals 3

    .prologue
    .line 67
    sget-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 69
    .local v0, "pref":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v1, "PREF_USER_REGULATION_AGREEMENT"

    const/4 v2, 0x0

    .line 68
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getUserRunBasicSettingOnce()Z
    .locals 3

    .prologue
    .line 81
    sget-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 83
    .local v0, "pref":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v1, "PREF_USER_BASIC_SETTING_RUN_ONCE"

    const/4 v2, 0x0

    .line 82
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getUserTouringGuideDone()Z
    .locals 3

    .prologue
    .line 74
    sget-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 75
    .local v0, "pref":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v1, "PREF_USER_TOURINGGUIDE_DONE"

    .line 76
    const/4 v2, 0x0

    .line 75
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getUserWarningAgreement()Z
    .locals 3

    .prologue
    .line 59
    sget-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 61
    .local v0, "pref":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v1, "PREF_USER_CAR_APP_WARNING_AGREEMENT"

    .line 62
    const/4 v2, 0x0

    .line 60
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getUserWelcome()Z
    .locals 3

    .prologue
    .line 53
    sget-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 54
    .local v0, "pref":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v1, "PREF_USER_WELCOME"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static setContext(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 11
    sput-object p0, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->mContext:Landroid/content/Context;

    .line 12
    return-void
.end method

.method public static setUserRegulationAgreement(Z)V
    .locals 2
    .param p0, "agreed"    # Z

    .prologue
    .line 34
    sget-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 35
    .local v0, "pref":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v1, "PREF_USER_REGULATION_AGREEMENT"

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 36
    return-void
.end method

.method public static setUserRunBasicSettingOnce()V
    .locals 3

    .prologue
    .line 46
    sget-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 47
    .local v0, "pref":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v1, "PREF_USER_BASIC_SETTING_RUN_ONCE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 48
    return-void
.end method

.method public static setUserTouringGuideDone()V
    .locals 3

    .prologue
    .line 40
    sget-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 41
    .local v0, "pref":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v1, "PREF_USER_TOURINGGUIDE_DONE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 42
    return-void
.end method

.method public static setUserWarningAgreement()V
    .locals 3

    .prologue
    .line 27
    sget-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 28
    .local v0, "pref":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v1, "PREF_USER_CAR_APP_WARNING_AGREEMENT"

    .line 29
    const/4 v2, 0x1

    .line 28
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 30
    return-void
.end method

.method public static setUserWelcome()V
    .locals 3

    .prologue
    .line 21
    sget-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 22
    .local v0, "pref":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v1, "PREF_USER_WELCOME"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 23
    return-void
.end method
