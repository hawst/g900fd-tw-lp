.class Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout$1;
.super Ljava/lang/Object;
.source "SearchMusicActionBarLayout.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 65
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->edit_text:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->access$0(Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->btn_clear:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->access$1(Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    if-nez p2, :cond_0

    .line 68
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->btn_clear:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->access$1(Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method
