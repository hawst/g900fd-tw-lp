.class public Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;
.super Landroid/os/AsyncTask;
.source "SettingsDrivingStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field static TAG:Ljava/lang/String;


# instance fields
.field private FLAG_CONTINUE:Z

.field private dialog:Landroid/app/AlertDialog;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string/jumbo v0, "SettingsDrivingStatus"

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;->mContext:Landroid/content/Context;

    .line 23
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;->FLAG_CONTINUE:Z

    .line 25
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 26
    const v1, 0x7f0a045e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 27
    const v1, 0x7f0a045f

    .line 28
    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus$1;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus$1;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;)V

    .line 27
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 25
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;->dialog:Landroid/app/AlertDialog;

    .line 34
    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;->exitSettings()V

    return-void
.end method

.method private exitSettings()V
    .locals 3

    .prologue
    .line 69
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    .line 70
    const-class v2, Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    .line 69
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 71
    .local v0, "i":Landroid/content/Intent;
    const/high16 v1, 0x44000000    # 512.0f

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 73
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->startActivity(Landroid/content/Intent;)V

    .line 74
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->finish()V

    .line 75
    return-void
.end method


# virtual methods
.method public displayRestrictionPopup()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 81
    :cond_0
    return-void
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "arg0"    # [Ljava/lang/String;

    .prologue
    .line 44
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;->FLAG_CONTINUE:Z

    if-eqz v1, :cond_0

    .line 45
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v1

    if-nez v1, :cond_0

    .line 47
    const-wide/16 v1, 0x3e8

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 48
    :catch_0
    move-exception v0

    .line 49
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 52
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "FLAG_CONTINUE : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;->FLAG_CONTINUE:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 53
    const-string/jumbo v3, " DrivingStatus : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 54
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 52
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    const/4 v1, 0x0

    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;->FLAG_CONTINUE:Z

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;->displayRestrictionPopup()V

    .line 65
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 66
    return-void
.end method

.method public setContinueFlag(Z)V
    .locals 3
    .param p1, "flag"    # Z

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setContinueFlag : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsDrivingStatus;->FLAG_CONTINUE:Z

    .line 39
    return-void
.end method
