.class Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$5;
.super Ljava/lang/Object;
.source "OnDriveLinkSamsungAccountListenerImp.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->onReceiveSamsungAccountAccessToken(ZLandroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;

.field private final synthetic val$_isSuccess:Z

.field private final synthetic val$_resultData:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;ZLandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$5;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;

    iput-boolean p2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$5;->val$_isSuccess:Z

    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$5;->val$_resultData:Landroid/os/Bundle;

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$5;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$5;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    move-result-object v0

    .line 105
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$5;->val$_isSuccess:Z

    .line 106
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$5;->val$_resultData:Landroid/os/Bundle;

    .line 105
    invoke-interface {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;->onReceiveSamsungAccountAccessToken(ZLandroid/os/Bundle;)V

    .line 108
    :cond_0
    return-void
.end method
