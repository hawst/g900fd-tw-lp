.class public Lcom/sec/android/automotive/drivelink/location/places/impl/GooglePlaceService;
.super Ljava/lang/Object;
.source "GooglePlaceService.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;


# static fields
.field private static final FIELD_STATUS:Ljava/lang/String; = "status"

.field private static final OPTION_INPUT:Ljava/lang/String; = "&input="

.field private static final OPTION_KEY:Ljava/lang/String; = "&key=AIzaSyCdnE4MNl6HbMEitvtPBascKE3U3DfggGY"

.field private static final OPTION_LANGUAGE:Ljava/lang/String; = "&language=en"

.field private static final OPTION_LOCATION:Ljava/lang/String; = "&location="

.field private static final OPTION_NAME:Ljava/lang/String; = "&name="

.field private static final OPTION_RANKBY:Ljava/lang/String; = "&rankby=distance"

.field private static final OPTION_REFERENCE:Ljava/lang/String; = "&reference="

.field private static final OPTION_SENSOR:Ljava/lang/String; = "&sensor=false"

.field private static final PLACES_API_BASE_URL:Ljava/lang/String; = "https://maps.googleapis.com/maps/api/place"

.field private static final REQUEST_AUTOCOMPLETE:Ljava/lang/String; = "/autocomplete"

.field private static final REQUEST_DETAIL:Ljava/lang/String; = "/details"

.field private static final REQUEST_NEARBYSEARCH:Ljava/lang/String; = "/nearbysearch"

.field private static final RESPONSE_OUT_PUT:Ljava/lang/String; = "/json?"

.field private static final STATUS_INVALID_REQUEST:Ljava/lang/String; = "INVALID_REQUEST"

.field private static final STATUS_OVER_QUERY_LIMIT:Ljava/lang/String; = "OVER_QUERY_LIMIT"

.field private static final STATUS_REQUEST_DENIED:Ljava/lang/String; = "REQUEST_DENIED"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private createUri()Ljava/lang/StringBuilder;
    .locals 2

    .prologue
    .line 192
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "https://maps.googleapis.com/maps/api/place"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private getContent(Ljava/net/URL;)Lorg/json/JSONObject;
    .locals 11
    .param p1, "url"    # Ljava/net/URL;

    .prologue
    .line 237
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 238
    .local v3, "content":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .line 239
    .local v2, "conn":Ljava/net/HttpURLConnection;
    const/4 v6, 0x0

    .line 242
    .local v6, "json":Lorg/json/JSONObject;
    :try_start_0
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v2, v0

    .line 245
    const/16 v9, 0x400

    new-array v1, v9, [C

    .line 246
    .local v1, "buff":[C
    new-instance v5, Ljava/io/InputStreamReader;

    .line 247
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    const-string/jumbo v10, "UTF-8"

    .line 246
    invoke-direct {v5, v9, v10}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 249
    .local v5, "in":Ljava/io/InputStreamReader;
    invoke-virtual {v5, v1}, Ljava/io/InputStreamReader;->read([C)I

    move-result v8

    .line 250
    .local v8, "numOfCharsRead":I
    :goto_0
    const/4 v9, -0x1

    if-ne v8, v9, :cond_2

    .line 255
    new-instance v7, Lorg/json/JSONObject;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 261
    .end local v6    # "json":Lorg/json/JSONObject;
    .local v7, "json":Lorg/json/JSONObject;
    if-eqz v2, :cond_0

    .line 262
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_0
    move-object v6, v7

    .line 265
    .end local v1    # "buff":[C
    .end local v5    # "in":Ljava/io/InputStreamReader;
    .end local v7    # "json":Lorg/json/JSONObject;
    .end local v8    # "numOfCharsRead":I
    .restart local v6    # "json":Lorg/json/JSONObject;
    :cond_1
    :goto_1
    return-object v6

    .line 251
    .restart local v1    # "buff":[C
    .restart local v5    # "in":Ljava/io/InputStreamReader;
    .restart local v8    # "numOfCharsRead":I
    :cond_2
    const/4 v9, 0x0

    :try_start_1
    invoke-virtual {v3, v1, v9, v8}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 252
    invoke-virtual {v5, v1}, Ljava/io/InputStreamReader;->read([C)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v8

    goto :goto_0

    .line 256
    .end local v1    # "buff":[C
    .end local v5    # "in":Ljava/io/InputStreamReader;
    .end local v8    # "numOfCharsRead":I
    :catch_0
    move-exception v4

    .line 257
    .local v4, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 261
    if-eqz v2, :cond_1

    .line 262
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_1

    .line 258
    .end local v4    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v4

    .line 259
    .local v4, "e":Lorg/json/JSONException;
    :try_start_3
    invoke-virtual {v4}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 261
    if-eqz v2, :cond_1

    .line 262
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_1

    .line 260
    .end local v4    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v9

    .line 261
    if-eqz v2, :cond_3

    .line 262
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 264
    :cond_3
    throw v9
.end method

.method private makeUrl(Ljava/lang/StringBuilder;)Ljava/net/URL;
    .locals 4
    .param p1, "uri"    # Ljava/lang/StringBuilder;

    .prologue
    .line 219
    const/4 v1, 0x0

    .line 221
    .local v1, "url":Ljava/net/URL;
    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "url":Ljava/net/URL;
    .local v2, "url":Ljava/net/URL;
    move-object v1, v2

    .line 226
    .end local v2    # "url":Ljava/net/URL;
    .restart local v1    # "url":Ljava/net/URL;
    :goto_0
    return-object v1

    .line 222
    :catch_0
    move-exception v0

    .line 223
    .local v0, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_0
.end method

.method private setCommonOption(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 1
    .param p1, "builder"    # Ljava/lang/StringBuilder;

    .prologue
    .line 203
    const-string/jumbo v0, "&rankby=distance"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    const-string/jumbo v0, "&language=en"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    const-string/jumbo v0, "&sensor=false"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    const-string/jumbo v0, "&key=AIzaSyCdnE4MNl6HbMEitvtPBascKE3U3DfggGY"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    return-object p1
.end method

.method private validateStatus(Lorg/json/JSONObject;)V
    .locals 3
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/ConnectException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 278
    if-nez p1, :cond_0

    .line 279
    new-instance v1, Lorg/json/JSONException;

    const-string/jumbo v2, "JSONObject is null"

    invoke-direct {v1, v2}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 281
    :cond_0
    const-string/jumbo v1, "status"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 283
    .local v0, "status":Ljava/lang/String;
    const-string/jumbo v1, "REQUEST_DENIED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 284
    const-string/jumbo v1, "INVALID_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 285
    const-string/jumbo v1, "OVER_QUERY_LIMIT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 286
    :cond_1
    new-instance v1, Ljava/net/ConnectException;

    invoke-direct {v1, v0}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 290
    :cond_2
    return-void
.end method


# virtual methods
.method public autocomplete(Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .param p1, "place"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;",
            ">;"
        }
    .end annotation

    .prologue
    .line 151
    const/4 v2, 0x0

    .line 152
    .local v2, "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/places/impl/GooglePlaceService;->createUri()Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "/autocomplete"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 153
    const-string/jumbo v7, "/json?"

    .line 152
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 155
    .local v4, "uri":Ljava/lang/StringBuilder;
    invoke-direct {p0, v4}, Lcom/sec/android/automotive/drivelink/location/places/impl/GooglePlaceService;->setCommonOption(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 156
    const-string/jumbo v6, "&input="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    :try_start_0
    const-string/jumbo v6, "UTF-8"

    invoke-static {p1, v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    invoke-direct {p0, v4}, Lcom/sec/android/automotive/drivelink/location/places/impl/GooglePlaceService;->makeUrl(Ljava/lang/StringBuilder;)Ljava/net/URL;

    move-result-object v5

    .line 162
    .local v5, "url":Ljava/net/URL;
    if-nez v5, :cond_0

    .line 163
    const-string/jumbo v6, "[GooglePlaceService]"

    const-string/jumbo v7, "url object is null"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 183
    .end local v2    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .end local v5    # "url":Ljava/net/URL;
    .local v3, "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :goto_0
    return-object v3

    .line 167
    .end local v3    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v2    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v5    # "url":Ljava/net/URL;
    :cond_0
    invoke-direct {p0, v5}, Lcom/sec/android/automotive/drivelink/location/places/impl/GooglePlaceService;->getContent(Ljava/net/URL;)Lorg/json/JSONObject;

    move-result-object v1

    .line 168
    .local v1, "json":Lorg/json/JSONObject;
    if-nez v1, :cond_1

    .line 169
    const-string/jumbo v6, "[GooglePlaceService]"

    const-string/jumbo v7, "json object is null"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 170
    .end local v2    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v3    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    goto :goto_0

    .line 173
    .end local v3    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v2    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :cond_1
    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/location/places/impl/GooglePlaceService;->validateStatus(Lorg/json/JSONObject;)V

    .line 174
    new-instance v6, Lcom/sec/android/automotive/drivelink/location/places/impl/PlaceParser;

    invoke-direct {v6}, Lcom/sec/android/automotive/drivelink/location/places/impl/PlaceParser;-><init>()V

    invoke-virtual {v6, v1}, Lcom/sec/android/automotive/drivelink/location/places/impl/PlaceParser;->parseAutocomplete(Lorg/json/JSONObject;)Ljava/util/List;
    :try_end_0
    .catch Ljava/net/ConnectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v2

    .end local v1    # "json":Lorg/json/JSONObject;
    .end local v5    # "url":Ljava/net/URL;
    :goto_1
    move-object v3, v2

    .line 183
    .end local v2    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v3    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    goto :goto_0

    .line 175
    .end local v3    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v2    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :catch_0
    move-exception v0

    .line 176
    .local v0, "e":Ljava/net/ConnectException;
    invoke-virtual {v0}, Ljava/net/ConnectException;->printStackTrace()V

    goto :goto_1

    .line 177
    .end local v0    # "e":Ljava/net/ConnectException;
    :catch_1
    move-exception v0

    .line 178
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 179
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_2
    move-exception v0

    .line 180
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1
.end method

.method public details(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    .locals 8
    .param p1, "reference"    # Ljava/lang/String;

    .prologue
    .line 110
    const/4 v3, 0x0

    .line 112
    .local v3, "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/places/impl/GooglePlaceService;->createUri()Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "/details"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 113
    const-string/jumbo v7, "/json?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "&sensor=false"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 114
    const-string/jumbo v7, "&key=AIzaSyCdnE4MNl6HbMEitvtPBascKE3U3DfggGY"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 116
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v6, "&reference="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/places/impl/GooglePlaceService;->makeUrl(Ljava/lang/StringBuilder;)Ljava/net/URL;

    move-result-object v5

    .line 120
    .local v5, "url":Ljava/net/URL;
    if-nez v5, :cond_0

    .line 121
    const-string/jumbo v6, "[GooglePlaceService]"

    const-string/jumbo v7, "url object is null"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v3

    .line 140
    .end local v3    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    .local v4, "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    :goto_0
    return-object v4

    .line 125
    .end local v4    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    .restart local v3    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    :cond_0
    invoke-direct {p0, v5}, Lcom/sec/android/automotive/drivelink/location/places/impl/GooglePlaceService;->getContent(Ljava/net/URL;)Lorg/json/JSONObject;

    move-result-object v2

    .line 126
    .local v2, "json":Lorg/json/JSONObject;
    if-nez v2, :cond_1

    .line 127
    const-string/jumbo v6, "[GooglePlaceService]"

    const-string/jumbo v7, "json object is null"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v3

    .line 128
    .end local v3    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    .restart local v4    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    goto :goto_0

    .line 132
    .end local v4    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    .restart local v3    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    :cond_1
    :try_start_0
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/location/places/impl/GooglePlaceService;->validateStatus(Lorg/json/JSONObject;)V

    .line 133
    new-instance v6, Lcom/sec/android/automotive/drivelink/location/places/impl/PlaceParser;

    invoke-direct {v6}, Lcom/sec/android/automotive/drivelink/location/places/impl/PlaceParser;-><init>()V

    invoke-virtual {v6, v2}, Lcom/sec/android/automotive/drivelink/location/places/impl/PlaceParser;->parseDetail(Lorg/json/JSONObject;)Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    :try_end_0
    .catch Ljava/net/ConnectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    :goto_1
    move-object v4, v3

    .line 140
    .end local v3    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    .restart local v4    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    goto :goto_0

    .line 134
    .end local v4    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    .restart local v3    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    :catch_0
    move-exception v1

    .line 135
    .local v1, "e":Ljava/net/ConnectException;
    invoke-virtual {v1}, Ljava/net/ConnectException;->printStackTrace()V

    goto :goto_1

    .line 136
    .end local v1    # "e":Ljava/net/ConnectException;
    :catch_1
    move-exception v1

    .line 137
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method

.method public nearbySearch(Ljava/lang/String;DD)Ljava/util/List;
    .locals 8
    .param p1, "place"    # Ljava/lang/String;
    .param p2, "latitude"    # D
    .param p4, "longitude"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "DD)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    const/4 v3, 0x0

    .line 65
    .local v3, "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/places/impl/GooglePlaceService;->createUri()Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "/nearbysearch"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 66
    const-string/jumbo v7, "/json?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 68
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/places/impl/GooglePlaceService;->setCommonOption(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 70
    const-string/jumbo v6, "&name="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    const-string/jumbo v6, "&location="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 74
    const-string/jumbo v6, ","

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 77
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/places/impl/GooglePlaceService;->makeUrl(Ljava/lang/StringBuilder;)Ljava/net/URL;

    move-result-object v5

    .line 78
    .local v5, "url":Ljava/net/URL;
    if-nez v5, :cond_0

    .line 79
    const-string/jumbo v6, "[GooglePlaceService]"

    const-string/jumbo v7, "url object is null"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v3

    .line 98
    .end local v3    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .local v4, "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :goto_0
    return-object v4

    .line 83
    .end local v4    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v3    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :cond_0
    invoke-direct {p0, v5}, Lcom/sec/android/automotive/drivelink/location/places/impl/GooglePlaceService;->getContent(Ljava/net/URL;)Lorg/json/JSONObject;

    move-result-object v2

    .line 84
    .local v2, "json":Lorg/json/JSONObject;
    if-nez v2, :cond_1

    .line 85
    const-string/jumbo v6, "[GooglePlaceService]"

    const-string/jumbo v7, "json object is null"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v3

    .line 86
    .end local v3    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v4    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    goto :goto_0

    .line 90
    .end local v4    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v3    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :cond_1
    :try_start_0
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/location/places/impl/GooglePlaceService;->validateStatus(Lorg/json/JSONObject;)V

    .line 91
    new-instance v6, Lcom/sec/android/automotive/drivelink/location/places/impl/PlaceParser;

    invoke-direct {v6}, Lcom/sec/android/automotive/drivelink/location/places/impl/PlaceParser;-><init>()V

    invoke-virtual {v6, v2}, Lcom/sec/android/automotive/drivelink/location/places/impl/PlaceParser;->parse(Lorg/json/JSONObject;)Ljava/util/List;
    :try_end_0
    .catch Ljava/net/ConnectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    :goto_1
    move-object v4, v3

    .line 98
    .end local v3    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v4    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    goto :goto_0

    .line 92
    .end local v4    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v3    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :catch_0
    move-exception v1

    .line 93
    .local v1, "e":Ljava/net/ConnectException;
    invoke-virtual {v1}, Ljava/net/ConnectException;->printStackTrace()V

    goto :goto_1

    .line 94
    .end local v1    # "e":Ljava/net/ConnectException;
    :catch_1
    move-exception v1

    .line 95
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method
