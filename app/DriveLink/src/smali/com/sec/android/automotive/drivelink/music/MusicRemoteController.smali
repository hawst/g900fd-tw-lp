.class public Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;
.super Ljava/lang/Object;
.source "MusicRemoteController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;,
        Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "RemoteMusicController"


# instance fields
.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mClientUpdateListener:Landroid/media/RemoteController$OnClientUpdateListener;

.field private final mContext:Landroid/content/Context;

.field private mCurrentMusicDuration:J

.field private mCurrentMusicPosition:J

.field mDLMusic:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

.field private mFlagForFirstPlayingChecker:Z

.field private mHandler:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;

.field private mIsRegistered:Z

.field private final mListener:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;

.field private mMusicPlayerState:I

.field private final mRemoteController:Landroid/media/RemoteController;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "artWorkWidth"    # I
    .param p3, "artWorkHeight"    # I
    .param p4, "listener"    # Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mMusicPlayerState:I

    .line 37
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mDLMusic:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

    .line 229
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$1;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mClientUpdateListener:Landroid/media/RemoteController$OnClientUpdateListener;

    .line 42
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mContext:Landroid/content/Context;

    .line 43
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mContext:Landroid/content/Context;

    .line 44
    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 43
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mAudioManager:Landroid/media/AudioManager;

    .line 45
    new-instance v0, Landroid/media/RemoteController;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mContext:Landroid/content/Context;

    .line 46
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mClientUpdateListener:Landroid/media/RemoteController$OnClientUpdateListener;

    invoke-direct {v0, v1, v2}, Landroid/media/RemoteController;-><init>(Landroid/content/Context;Landroid/media/RemoteController$OnClientUpdateListener;)V

    .line 45
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    .line 47
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    invoke-virtual {v0, p2, p3}, Landroid/media/RemoteController;->setArtworkConfiguration(II)Z

    .line 48
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mListener:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;

    .line 49
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mFlagForFirstPlayingChecker:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;Z)V
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mFlagForFirstPlayingChecker:Z

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;J)V
    .locals 0

    .prologue
    .line 36
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mCurrentMusicDuration:J

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;)J
    .locals 2

    .prologue
    .line 36
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mCurrentMusicDuration:J

    return-wide v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;)Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mListener:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;I)V
    .locals 0

    .prologue
    .line 221
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->setMusicPlayerState(I)V

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;J)V
    .locals 0

    .prologue
    .line 35
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mCurrentMusicPosition:J

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;)Z
    .locals 1

    .prologue
    .line 204
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->registerRemoteController()Z

    move-result v0

    return v0
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;)V
    .locals 0

    .prologue
    .line 216
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->unregisterRemoteController()V

    return-void
.end method

.method private registerRemoteController()Z
    .locals 3

    .prologue
    .line 205
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mAudioManager:Landroid/media/AudioManager;

    .line 206
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->registerRemoteController(Landroid/media/RemoteController;)Z

    move-result v0

    .line 205
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mIsRegistered:Z

    .line 207
    const-string/jumbo v0, "RemoteMusicController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "registerRemoteController "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mIsRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mListener:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mIsRegistered:Z

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mListener:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;

    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;->onRegistered()V

    .line 213
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mIsRegistered:Z

    return v0
.end method

.method private setMusicPlayerState(I)V
    .locals 6
    .param p1, "state"    # I

    .prologue
    .line 222
    iput p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mMusicPlayerState:I

    .line 223
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mListener:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mListener:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;

    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mCurrentMusicPosition:J

    .line 225
    iget-wide v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mCurrentMusicDuration:J

    move v1, p1

    .line 224
    invoke-interface/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;->onPlaybackStateUpdate(IJJ)V

    .line 227
    :cond_0
    return-void
.end method

.method private unregisterRemoteController()V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterRemoteController(Landroid/media/RemoteController;)V

    .line 218
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mIsRegistered:Z

    .line 219
    return-void
.end method


# virtual methods
.method public ffoward(Z)V
    .locals 4
    .param p1, "isUp"    # Z

    .prologue
    const/16 v3, 0x5a

    .line 161
    if-eqz p1, :cond_0

    .line 162
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    new-instance v1, Landroid/view/KeyEvent;

    .line 163
    const/4 v2, 0x1

    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 162
    invoke-virtual {v0, v1}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    .line 168
    :goto_0
    return-void

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    new-instance v1, Landroid/view/KeyEvent;

    .line 166
    const/4 v2, 0x0

    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 165
    invoke-virtual {v0, v1}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    goto :goto_0
.end method

.method public getCurrentPosition(I)J
    .locals 6
    .param p1, "progress"    # I

    .prologue
    const-wide/16 v0, 0x0

    .line 111
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    invoke-virtual {v2}, Landroid/media/RemoteController;->getEstimatedMediaPosition()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mCurrentMusicPosition:J

    .line 114
    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mCurrentMusicPosition:J

    cmp-long v2, v2, v0

    if-ltz v2, :cond_0

    .line 115
    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mCurrentMusicPosition:J

    iget-wide v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mCurrentMusicDuration:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 118
    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mCurrentMusicPosition:J

    goto :goto_0
.end method

.method public getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .locals 5

    .prologue
    .line 186
    const-string/jumbo v2, "RemoteMusicController"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "getPlayingMusic registered:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mIsRegistered:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mIsRegistered:Z

    if-nez v2, :cond_0

    .line 188
    const/4 v1, 0x0

    .line 201
    :goto_0
    return-object v1

    .line 190
    :cond_0
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;-><init>()V

    .line 191
    .local v1, "ret":Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mDLMusic:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setTitle(Ljava/lang/String;)V

    .line 192
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mDLMusic:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->getArtist()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setArtist(Ljava/lang/String;)V

    .line 193
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mDLMusic:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->getDuration()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setDuration(I)V

    .line 194
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mDLMusic:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->getAlbumArt()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 195
    .local v0, "albumArt":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 196
    invoke-static {v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setAlbumArt(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 198
    :cond_1
    const-string/jumbo v2, "RemoteMusicController"

    const-string/jumbo v3, "album art is null"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getPlayingMusicDuration()J
    .locals 2

    .prologue
    .line 122
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mCurrentMusicDuration:J

    return-wide v0
.end method

.method public initialize()V
    .locals 7

    .prologue
    .line 52
    const-string/jumbo v3, "RemoteMusicController"

    const-string/jumbo v4, "initialize"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    new-instance v3, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;Landroid/os/Looper;)V

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mHandler:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;

    .line 56
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mContext:Landroid/content/Context;

    .line 57
    const-string/jumbo v4, "activity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 56
    check-cast v0, Landroid/app/ActivityManager;

    .line 58
    .local v0, "am":Landroid/app/ActivityManager;
    if-nez v0, :cond_1

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    const v3, 0x7fffffff

    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v1

    .line 64
    .local v1, "runningServiceList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    if-eqz v1, :cond_0

    .line 68
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 69
    .local v2, "service":Landroid/app/ActivityManager$RunningServiceInfo;
    if-eqz v2, :cond_2

    .line 73
    const-string/jumbo v4, "com.samsung.musicplus.service.PlayerService"

    .line 74
    iget-object v5, v2, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 75
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->registerRemoteController()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 76
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mFlagForFirstPlayingChecker:Z

    .line 77
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mHandler:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;

    .line 78
    const/4 v4, 0x2

    const-wide/16 v5, 0x12c

    .line 77
    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 2

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mMusicPlayerState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 106
    const/4 v0, 0x1

    .line 107
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRegisteredRemoteController()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mIsRegistered:Z

    return v0
.end method

.method public next()V
    .locals 4

    .prologue
    const/16 v3, 0x57

    .line 126
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x0

    .line 127
    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 126
    invoke-virtual {v0, v1}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    .line 128
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x1

    .line 129
    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 128
    invoke-virtual {v0, v1}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    .line 130
    return-void
.end method

.method public pause()V
    .locals 4

    .prologue
    const/16 v3, 0x7f

    .line 140
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x0

    .line 141
    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 140
    invoke-virtual {v0, v1}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    .line 142
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x1

    .line 143
    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 142
    invoke-virtual {v0, v1}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    .line 144
    return-void
.end method

.method public play()V
    .locals 4

    .prologue
    const/16 v3, 0x7e

    .line 147
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x0

    .line 148
    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 147
    invoke-virtual {v0, v1}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    .line 149
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x1

    .line 150
    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 149
    invoke-virtual {v0, v1}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    .line 151
    return-void
.end method

.method public prev()V
    .locals 4

    .prologue
    const/16 v3, 0x58

    .line 133
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x0

    .line 134
    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 133
    invoke-virtual {v0, v1}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    .line 135
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x1

    .line 136
    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 135
    invoke-virtual {v0, v1}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    .line 137
    return-void
.end method

.method public requestReRegister()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mHandler:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;->sendEmptyMessage(I)Z

    .line 102
    return-void
.end method

.method public rewind(Z)V
    .locals 4
    .param p1, "isUp"    # Z

    .prologue
    const/16 v3, 0x59

    .line 171
    if-eqz p1, :cond_0

    .line 172
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    new-instance v1, Landroid/view/KeyEvent;

    .line 173
    const/4 v2, 0x1

    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 172
    invoke-virtual {v0, v1}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    .line 178
    :goto_0
    return-void

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    new-instance v1, Landroid/view/KeyEvent;

    .line 176
    const/4 v2, 0x0

    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 175
    invoke-virtual {v0, v1}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    goto :goto_0
.end method

.method public seekTo(J)V
    .locals 1
    .param p1, "position"    # J

    .prologue
    .line 181
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mCurrentMusicPosition:J

    .line 182
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    invoke-virtual {v0, p1, p2}, Landroid/media/RemoteController;->seekTo(J)Z

    .line 183
    return-void
.end method

.method public stop()V
    .locals 4

    .prologue
    const/16 v3, 0x56

    .line 154
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x0

    .line 155
    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 154
    invoke-virtual {v0, v1}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    .line 156
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x1

    .line 157
    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 156
    invoke-virtual {v0, v1}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    .line 158
    return-void
.end method

.method public terminate()V
    .locals 2

    .prologue
    .line 86
    const-string/jumbo v0, "RemoteMusicController"

    const-string/jumbo v1, "terminate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mHandler:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;->terminate()V

    .line 89
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mRemoteController:Landroid/media/RemoteController;

    invoke-virtual {v0}, Landroid/media/RemoteController;->clearArtworkConfiguration()Z

    .line 91
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mIsRegistered:Z

    if-eqz v0, :cond_0

    .line 92
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->unregisterRemoteController()V

    .line 94
    :cond_0
    return-void
.end method
