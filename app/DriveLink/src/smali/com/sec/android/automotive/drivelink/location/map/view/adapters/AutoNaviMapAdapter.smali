.class public Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;
.super Lcom/sec/android/automotive/drivelink/location/map/LocationMap;
.source "AutoNaviMapAdapter.java"

# interfaces
.implements Lcom/amap/api/maps/AMap$OnMarkerDragListener;


# static fields
.field private static final FAKE_ZOOM:D = 0.011

.field private static final MAX_ZOOM_LEVEL:F = 21.0f

.field private static final MIN_ZOOM_LEVEL:F = 2.0f

.field private static final TAG:Ljava/lang/String; = "AutoNaviMapAdapter"


# instance fields
.field private mMap:Lcom/amap/api/maps/AMap;

.field private mMarkers:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;",
            "Lcom/amap/api/maps/model/Marker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;-><init>()V

    .line 46
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMap:Lcom/amap/api/maps/AMap;

    .line 47
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMarkers:Ljava/util/Hashtable;

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->createMapFragment()V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;)V
    .locals 2
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;-><init>()V

    .line 46
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMap:Lcom/amap/api/maps/AMap;

    .line 47
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMarkers:Ljava/util/Hashtable;

    .line 54
    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMarkers:Ljava/util/Hashtable;

    move-object v0, p1

    .line 56
    check-cast v0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapFragment;

    .line 57
    .local v0, "mFragment":Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapFragment;
    invoke-virtual {v0, p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapFragment;->setAdapter(Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;)V

    .line 58
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapFragment;->getMap()Lcom/amap/api/maps/AMap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMap:Lcom/amap/api/maps/AMap;

    .line 59
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->setMapFragment(Landroid/support/v4/app/Fragment;)V

    .line 60
    return-void
.end method

.method private camera(Lcom/amap/api/maps/CameraUpdate;)V
    .locals 1
    .param p1, "camera"    # Lcom/amap/api/maps/CameraUpdate;

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->getMap()Lcom/amap/api/maps/AMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amap/api/maps/AMap;->moveCamera(Lcom/amap/api/maps/CameraUpdate;)V

    .line 144
    return-void
.end method

.method private createGMarkerOptionsFromLMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)Lcom/amap/api/maps/model/MarkerOptions;
    .locals 8
    .param p1, "marker"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .prologue
    .line 158
    if-nez p1, :cond_1

    .line 159
    const-string/jumbo v4, "AutoNaviMapAdapter"

    const-string/jumbo v5, "createGMarkerOptionsFromLMarkerItem: marker is null!!!"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    const/4 v1, 0x0

    .line 189
    :cond_0
    :goto_0
    return-object v1

    .line 163
    :cond_1
    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLatitude()D

    move-result-wide v4

    .line 164
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLongitude()D

    move-result-wide v6

    .line 163
    invoke-direct {v3, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 165
    .local v3, "gLatLong":Lcom/amap/api/maps/model/LatLng;
    new-instance v1, Lcom/amap/api/maps/model/MarkerOptions;

    invoke-direct {v1}, Lcom/amap/api/maps/model/MarkerOptions;-><init>()V

    .line 167
    .local v1, "bMarkerOptions":Lcom/amap/api/maps/model/MarkerOptions;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->isDraggable()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/amap/api/maps/model/MarkerOptions;->draggable(Z)Lcom/amap/api/maps/model/MarkerOptions;

    .line 168
    invoke-virtual {v1, v3}, Lcom/amap/api/maps/model/MarkerOptions;->position(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/MarkerOptions;

    .line 170
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getAnchor()Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 171
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getAnchor()Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;->getX()F

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getAnchor()Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;

    move-result-object v5

    .line 172
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/location/map/common/AnchorPoint;->getY()F

    move-result v5

    .line 171
    invoke-virtual {v1, v4, v5}, Lcom/amap/api/maps/model/MarkerOptions;->anchor(FF)Lcom/amap/api/maps/model/MarkerOptions;

    .line 174
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getResourceId()I

    move-result v4

    if-lez v4, :cond_3

    .line 176
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getResourceId()I

    move-result v4

    .line 175
    invoke-static {v4}, Lcom/amap/api/maps/model/BitmapDescriptorFactory;->fromResource(I)Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/amap/api/maps/model/MarkerOptions;->icon(Lcom/amap/api/maps/model/BitmapDescriptor;)Lcom/amap/api/maps/model/MarkerOptions;

    goto :goto_0

    .line 177
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getIcon()Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 178
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getIcon()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-static {v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 180
    .local v0, "b":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-static {v0}, Lcom/amap/api/maps/model/BitmapDescriptorFactory;->fromBitmap(Landroid/graphics/Bitmap;)Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/amap/api/maps/model/MarkerOptions;->icon(Lcom/amap/api/maps/model/BitmapDescriptor;)Lcom/amap/api/maps/model/MarkerOptions;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 181
    :catch_0
    move-exception v2

    .line 182
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getIcon()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 183
    invoke-static {v0}, Lcom/amap/api/maps/model/BitmapDescriptorFactory;->fromBitmap(Landroid/graphics/Bitmap;)Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/amap/api/maps/model/MarkerOptions;->icon(Lcom/amap/api/maps/model/BitmapDescriptor;)Lcom/amap/api/maps/model/MarkerOptions;

    .line 184
    const-string/jumbo v4, "AutoNaviMapAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Error message: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method


# virtual methods
.method public addMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V
    .locals 3
    .param p1, "marker"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .prologue
    .line 73
    if-nez p1, :cond_0

    .line 74
    const-string/jumbo v1, "AutoNaviMapAdapter"

    const-string/jumbo v2, "addMarkerItem: marker is null!!!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    :goto_0
    return-void

    .line 78
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMap:Lcom/amap/api/maps/AMap;

    if-nez v1, :cond_1

    .line 79
    const-string/jumbo v1, "AutoNaviMapAdapter"

    const-string/jumbo v2, "addMarkerItem: mMap is null!!!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 83
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMarkers:Ljava/util/Hashtable;

    if-nez v1, :cond_2

    .line 84
    const-string/jumbo v1, "AutoNaviMapAdapter"

    const-string/jumbo v2, "addMarkerItem: mMarkers is null!!!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 88
    :cond_2
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->addMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V

    .line 89
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMap:Lcom/amap/api/maps/AMap;

    .line 90
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->createGMarkerOptionsFromLMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/AMap;->addMarker(Lcom/amap/api/maps/model/MarkerOptions;)Lcom/amap/api/maps/model/Marker;

    move-result-object v0

    .line 91
    .local v0, "bMarker":Lcom/amap/api/maps/model/Marker;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMarkers:Ljava/util/Hashtable;

    invoke-virtual {v1, p1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public clearMarkers()V
    .locals 1

    .prologue
    .line 114
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->clearMarkers()V

    .line 115
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMap:Lcom/amap/api/maps/AMap;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMap:Lcom/amap/api/maps/AMap;

    invoke-virtual {v0}, Lcom/amap/api/maps/AMap;->clear()V

    .line 118
    :cond_0
    return-void
.end method

.method protected createMapFragment()V
    .locals 2

    .prologue
    .line 63
    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMarkers:Ljava/util/Hashtable;

    .line 65
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapFragment;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapFragment;-><init>(Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;)V

    .line 66
    .local v0, "fragment":Lcom/amap/api/maps/SupportMapFragment;
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->setMapFragment(Landroid/support/v4/app/Fragment;)V

    .line 67
    return-void
.end method

.method public createUpdateableRoute(I)Lcom/sec/android/automotive/drivelink/location/model/IGraphicRoute;
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 241
    const/4 v0, 0x0

    return-object v0
.end method

.method public drawRoute(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "points":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    const/4 v9, 0x1

    .line 217
    new-instance v4, Lcom/amap/api/maps/model/PolylineOptions;

    invoke-direct {v4}, Lcom/amap/api/maps/model/PolylineOptions;-><init>()V

    const/high16 v5, 0x41000000    # 8.0f

    invoke-virtual {v4, v5}, Lcom/amap/api/maps/model/PolylineOptions;->width(F)Lcom/amap/api/maps/model/PolylineOptions;

    move-result-object v4

    .line 218
    const v5, -0xd76d51

    invoke-virtual {v4, v5}, Lcom/amap/api/maps/model/PolylineOptions;->color(I)Lcom/amap/api/maps/model/PolylineOptions;

    move-result-object v4

    invoke-virtual {v4, v9}, Lcom/amap/api/maps/model/PolylineOptions;->geodesic(Z)Lcom/amap/api/maps/model/PolylineOptions;

    move-result-object v1

    .line 220
    .local v1, "options":Lcom/amap/api/maps/model/PolylineOptions;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 226
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMap:Lcom/amap/api/maps/AMap;

    invoke-virtual {v4, v1}, Lcom/amap/api/maps/AMap;->addPolyline(Lcom/amap/api/maps/model/PolylineOptions;)Lcom/amap/api/maps/model/Polyline;

    move-result-object v3

    .line 228
    .local v3, "polyline":Lcom/amap/api/maps/model/Polyline;
    invoke-virtual {v3, v9}, Lcom/amap/api/maps/model/Polyline;->setVisible(Z)V

    .line 230
    return-void

    .line 220
    .end local v3    # "polyline":Lcom/amap/api/maps/model/Polyline;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 221
    .local v0, "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    new-instance v2, Lcom/amap/api/maps/model/LatLng;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v5

    .line 222
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v7

    .line 221
    invoke-direct {v2, v5, v6, v7, v8}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 223
    .local v2, "point":Lcom/amap/api/maps/model/LatLng;
    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/PolylineOptions;->add(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/PolylineOptions;

    goto :goto_0
.end method

.method protected getMap()Lcom/amap/api/maps/AMap;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMap:Lcom/amap/api/maps/AMap;

    return-object v0
.end method

.method public onMarkerDrag(Lcom/amap/api/maps/model/Marker;)V
    .locals 0
    .param p1, "marker"    # Lcom/amap/api/maps/model/Marker;

    .prologue
    .line 194
    return-void
.end method

.method public onMarkerDragEnd(Lcom/amap/api/maps/model/Marker;)V
    .locals 6
    .param p1, "marker"    # Lcom/amap/api/maps/model/Marker;

    .prologue
    .line 198
    invoke-virtual {p1}, Lcom/amap/api/maps/model/Marker;->getPosition()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    .line 199
    .local v0, "gLatLng":Lcom/amap/api/maps/model/LatLng;
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .line 200
    iget-wide v2, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v4, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    .line 199
    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;-><init>(DD)V

    .line 202
    .local v1, "lMarkerItem":Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->getDragDropListener()Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMarkerDragDropListener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMarkerDragDropListener;->onMarkerDragEnd(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V

    .line 203
    return-void
.end method

.method public onMarkerDragStart(Lcom/amap/api/maps/model/Marker;)V
    .locals 6
    .param p1, "marker"    # Lcom/amap/api/maps/model/Marker;

    .prologue
    .line 207
    invoke-virtual {p1}, Lcom/amap/api/maps/model/Marker;->getPosition()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    .line 208
    .local v0, "gLatLng":Lcom/amap/api/maps/model/LatLng;
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .line 209
    iget-wide v2, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v4, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    .line 208
    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;-><init>(DD)V

    .line 211
    .local v1, "lMarkerItem":Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->getDragDropListener()Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMarkerDragDropListener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMarkerDragDropListener;->onDrag(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V

    .line 212
    return-void
.end method

.method public removeMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V
    .locals 2
    .param p1, "marker"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .prologue
    .line 96
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->removeMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V

    .line 97
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMarkers:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/maps/model/Marker;

    .line 99
    .local v0, "bMarker":Lcom/amap/api/maps/model/Marker;
    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {v0}, Lcom/amap/api/maps/model/Marker;->remove()V

    .line 101
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMarkers:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    :cond_0
    return-void
.end method

.method public setCompassEnabled(Z)V
    .locals 1
    .param p1, "option"    # Z

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMap:Lcom/amap/api/maps/AMap;

    invoke-virtual {v0}, Lcom/amap/api/maps/AMap;->getUiSettings()Lcom/amap/api/maps/UiSettings;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amap/api/maps/UiSettings;->setCompassEnabled(Z)V

    .line 251
    return-void
.end method

.method public setLocationPoint(DD)V
    .locals 1
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 108
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setLocationPoint(DD)V

    .line 109
    new-instance v0, Lcom/amap/api/maps/model/LatLng;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-static {v0}, Lcom/amap/api/maps/CameraUpdateFactory;->newLatLng(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/CameraUpdate;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->camera(Lcom/amap/api/maps/CameraUpdate;)V

    .line 110
    return-void
.end method

.method protected setMap(Lcom/amap/api/maps/AMap;)V
    .locals 0
    .param p1, "map"    # Lcom/amap/api/maps/AMap;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMap:Lcom/amap/api/maps/AMap;

    .line 152
    return-void
.end method

.method public setZoomControlsEnabled(Z)V
    .locals 1
    .param p1, "option"    # Z

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->mMap:Lcom/amap/api/maps/AMap;

    invoke-virtual {v0}, Lcom/amap/api/maps/AMap;->getUiSettings()Lcom/amap/api/maps/UiSettings;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amap/api/maps/UiSettings;->setZoomControlsEnabled(Z)V

    .line 246
    return-void
.end method

.method public setZoomLevel(F)V
    .locals 2
    .param p1, "level"    # F

    .prologue
    .line 124
    const/high16 v0, 0x40000000    # 2.0f

    .line 125
    const/high16 v1, 0x41980000    # 19.0f

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    .line 124
    invoke-static {v0}, Lcom/amap/api/maps/CameraUpdateFactory;->zoomTo(F)Lcom/amap/api/maps/CameraUpdate;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->camera(Lcom/amap/api/maps/CameraUpdate;)V

    .line 126
    return-void
.end method

.method public setZoomLevel(FDDI)V
    .locals 3
    .param p1, "level"    # F
    .param p2, "lat"    # D
    .param p4, "lng"    # D
    .param p6, "userIndex"    # I

    .prologue
    .line 132
    const/4 v1, 0x1

    if-eq p6, v1, :cond_0

    const/4 v1, 0x2

    if-eq p6, v1, :cond_0

    const/4 v1, 0x3

    if-ne p6, v1, :cond_1

    .line 135
    :cond_0
    const-wide v1, 0x3f86872b020c49baL    # 0.011

    add-double/2addr p2, v1

    .line 137
    :cond_1
    new-instance v0, Lcom/amap/api/maps/model/LatLng;

    invoke-direct {v0, p2, p3, p4, p5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 138
    .local v0, "gLatLong":Lcom/amap/api/maps/model/LatLng;
    const/high16 v1, 0x40000000    # 2.0f

    .line 139
    const/high16 v2, 0x41980000    # 19.0f

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    .line 138
    invoke-static {v0, v1}, Lcom/amap/api/maps/CameraUpdateFactory;->newLatLngZoom(Lcom/amap/api/maps/model/LatLng;F)Lcom/amap/api/maps/CameraUpdate;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->camera(Lcom/amap/api/maps/CameraUpdate;)V

    .line 140
    return-void
.end method

.method public zoomToArea(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;I)V
    .locals 0
    .param p1, "loc1"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p2, "loc2"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p3, "padding"    # I

    .prologue
    .line 236
    return-void
.end method
