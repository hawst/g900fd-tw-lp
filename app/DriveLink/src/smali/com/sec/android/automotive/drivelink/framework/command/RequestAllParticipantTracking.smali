.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestAllParticipantTracking;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestAllParticipantTracking.java"


# instance fields
.field private mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

.field private mParticipantsTracks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 1
    .param p1, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAllParticipantTracking;->mParticipantsTracks:Ljava/util/HashMap;

    .line 22
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAllParticipantTracking;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .line 23
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAllParticipantTracking;->mParticipantsTracks:Ljava/util/HashMap;

    .line 43
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAllParticipantTracking;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .line 44
    return-void
.end method

.method protected process()V
    .locals 2

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestAllParticipantTracking;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAllParticipantTracking;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getAllParticipantTracking(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Ljava/util/HashMap;

    move-result-object v0

    .line 27
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAllParticipantTracking;->mParticipantsTracks:Ljava/util/HashMap;

    .line 29
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestAllParticipantTracking;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 34
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    move-result-object v0

    .line 35
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    if-eqz v0, :cond_0

    .line 36
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAllParticipantTracking;->mParticipantsTracks:Ljava/util/HashMap;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;->onResponseRequestAllParticipantTracking(Ljava/util/HashMap;)V

    .line 38
    :cond_0
    return-void
.end method
