.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationChargerDialogView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "[NotificationChargerDialogView]"


# instance fields
.field mLlNotiBase:Landroid/widget/LinearLayout;

.field private mTouchListener:Landroid/view/View$OnTouchListener;

.field mTvRemains:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->mLlNotiBase:Landroid/widget/LinearLayout;

    .line 21
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->mTvRemains:Landroid/widget/TextView;

    .line 133
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 38
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->initSubClassNotification(Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->mLlNotiBase:Landroid/widget/LinearLayout;

    .line 21
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->mTvRemains:Landroid/widget/TextView;

    .line 133
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 32
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->initSubClassNotification(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->mLlNotiBase:Landroid/widget/LinearLayout;

    .line 21
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->mTvRemains:Landroid/widget/TextView;

    .line 133
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 26
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->initSubClassNotification(Landroid/content/Context;)V

    .line 27
    return-void
.end method


# virtual methods
.method protected getTTSText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    const-string/jumbo v0, ""

    .line 129
    .local v0, "tts":Ljava/lang/String;
    return-object v0
.end method

.method protected initSubClassNotification(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    const-string/jumbo v2, "[NotificationChargerDialogView]"

    const-string/jumbo v3, "initSubClassNotification"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 44
    const-string/jumbo v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 43
    check-cast v0, Landroid/view/LayoutInflater;

    .line 46
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const/4 v1, 0x0

    .line 47
    .local v1, "view":Landroid/view/View;
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->isMultiMode:Z

    if-eqz v2, :cond_1

    .line 48
    const v2, 0x7f0300b9

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 54
    :goto_0
    const v2, 0x7f0902ba

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->mTvRemains:Landroid/widget/TextView;

    .line 55
    const v2, 0x7f0902a8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->mLlNotiBase:Landroid/widget/LinearLayout;

    .line 56
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->mLlNotiBase:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->hasNotiItemInfo()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->setChargerInfo(Ljava/lang/Integer;)V

    .line 62
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->setUseAutoTimeout(Z)V

    .line 64
    return-void

    .line 51
    :cond_1
    const v2, 0x7f0300b8

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public setChargerInfo(Ljava/lang/Integer;)V
    .locals 7
    .param p1, "level"    # Ljava/lang/Integer;

    .prologue
    .line 68
    const-string/jumbo v4, "[NotificationChargerDialogView]"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Battery Level:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 71
    const v5, 0x7f0a0394

    .line 70
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 72
    .local v3, "remainString":Ljava/lang/String;
    const/4 v2, 0x0

    .line 74
    .local v2, "realpercent":Ljava/lang/String;
    const/4 v1, 0x0

    .line 76
    .local v1, "percent":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 81
    :goto_0
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 82
    const-string/jumbo v4, "xx"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz v1, :cond_0

    .line 83
    const-string/jumbo v4, "xx"

    invoke-virtual {v3, v4, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 86
    :cond_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->mTvRemains:Landroid/widget/TextView;

    if-eqz v4, :cond_1

    if-eqz v2, :cond_1

    .line 87
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->mTvRemains:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    :cond_1
    return-void

    .line 77
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected startDefaultNotiTimeoutFlow()V
    .locals 2

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->getTTSText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->getTTSText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addBegin()V

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addAlert()V

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->getTTSText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTS(Ljava/lang/String;)V

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addFinish()V

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationChargerDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdFlowStart()V

    .line 108
    :cond_0
    return-void
.end method
