.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;
.super Landroid/widget/LinearLayout;
.source "NotificationMessageReadFullView.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field mBtCall:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

.field mBtReplay:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

.field private mBtTts:Landroid/widget/ImageButton;

.field mClickListener:Landroid/view/View$OnClickListener;

.field private mMMS:Landroid/widget/ImageView;

.field private mNameView:Landroid/widget/TextView;

.field private mSMS:Landroid/widget/ImageView;

.field protected mViewLocal:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 22
    const-string/jumbo v0, "NotificationMessageReadFullView"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->TAG:Ljava/lang/String;

    .line 24
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mViewLocal:Landroid/view/View;

    .line 31
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mNameView:Landroid/widget/TextView;

    .line 33
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mClickListener:Landroid/view/View$OnClickListener;

    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->initIndicatorView(Landroid/content/Context;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    const-string/jumbo v0, "NotificationMessageReadFullView"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->TAG:Ljava/lang/String;

    .line 24
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mViewLocal:Landroid/view/View;

    .line 31
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mNameView:Landroid/widget/TextView;

    .line 33
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mClickListener:Landroid/view/View$OnClickListener;

    .line 47
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->initIndicatorView(Landroid/content/Context;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    const-string/jumbo v0, "NotificationMessageReadFullView"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->TAG:Ljava/lang/String;

    .line 24
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mViewLocal:Landroid/view/View;

    .line 31
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mNameView:Landroid/widget/TextView;

    .line 33
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mClickListener:Landroid/view/View$OnClickListener;

    .line 40
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->initIndicatorView(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 61
    const-string/jumbo v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 60
    check-cast v0, Landroid/view/LayoutInflater;

    .line 63
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mViewLocal:Landroid/view/View;

    if-nez v1, :cond_0

    .line 65
    const v1, 0x7f0300bd

    .line 64
    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mViewLocal:Landroid/view/View;

    .line 66
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mViewLocal:Landroid/view/View;

    const v2, 0x7f0902f0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mSMS:Landroid/widget/ImageView;

    .line 67
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mViewLocal:Landroid/view/View;

    const v2, 0x7f0902f1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mMMS:Landroid/widget/ImageView;

    .line 68
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mViewLocal:Landroid/view/View;

    const v2, 0x7f0902f9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mNameView:Landroid/widget/TextView;

    .line 69
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mViewLocal:Landroid/view/View;

    .line 70
    const v2, 0x7f0902fc

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 69
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mBtCall:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 71
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mViewLocal:Landroid/view/View;

    .line 72
    const v2, 0x7f0902fb

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 71
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mBtReplay:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 73
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mViewLocal:Landroid/view/View;

    const v2, 0x7f0902fa

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mBtTts:Landroid/widget/ImageButton;

    .line 75
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->isCalling()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mBtCall:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setEnabled(Z)V

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mBtCall:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method private isCalling()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 137
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mContext:Landroid/content/Context;

    .line 138
    const-string/jumbo v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 137
    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 139
    .local v1, "telephoneMgr":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    .line 140
    .local v0, "callStatus":I
    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    .line 141
    if-ne v0, v2, :cond_1

    .line 142
    :cond_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "isCalling true - callStatus :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public initMember(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;Landroid/graphics/Bitmap;Ljava/lang/String;JLjava/lang/String;)V
    .locals 4
    .param p1, "type"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;
    .param p2, "image"    # Landroid/graphics/Bitmap;
    .param p3, "Name"    # Ljava/lang/String;
    .param p4, "time"    # J
    .param p6, "message"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 85
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->SMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    invoke-virtual {p1, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mSMS:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 89
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mMMS:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 99
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mSMS:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 92
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mMMS:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 93
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 94
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 95
    const v2, 0x7f0d00b8

    .line 94
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 96
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public setEnableQuotesbutton(Z)V
    .locals 1
    .param p1, "isVisable"    # Z

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mBtCall:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setMarkShow(Z)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mBtReplay:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setMarkShow(Z)V

    .line 119
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mClickListener:Landroid/view/View$OnClickListener;

    .line 106
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mBtCall:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mBtReplay:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mBtTts:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    return-void
.end method

.method public setTTSEnable(Z)V
    .locals 2
    .param p1, "b"    # Z

    .prologue
    .line 127
    if-eqz p1, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mBtTts:Landroid/widget/ImageButton;

    const v1, 0x7f0203a6

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 133
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mBtTts:Landroid/widget/ImageButton;

    const v1, 0x7f0203a5

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setTTSVisable(I)V
    .locals 1
    .param p1, "b"    # I

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->mBtTts:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 123
    return-void
.end method
