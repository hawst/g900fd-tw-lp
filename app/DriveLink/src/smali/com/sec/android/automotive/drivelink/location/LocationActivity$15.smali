.class Lcom/sec/android/automotive/drivelink/location/LocationActivity$15;
.super Ljava/lang/Object;
.source "LocationActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    .line 1048
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1053
    const-string/jumbo v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "getDotIndicatorOnClickListener: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1055
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$13(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 1056
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$12(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    .line 1055
    sub-int/2addr v1, v2

    .line 1056
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 1055
    add-int v0, v1, v2

    .line 1057
    .local v0, "currentItem":I
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$13(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 1059
    return-void
.end method
