.class public Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLRouteInfoImp;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLRouteInfo;
.source "DLRouteInfoImp.java"


# static fields
.field public static final TRAFFIC_CLOSED:I = 0x4

.field public static final TRAFFIC_FREE_FLOW:I = 0x0

.field public static final TRAFFIC_HEAVY:I = 0x2

.field public static final TRAFFIC_MODERATE:I = 0x1

.field public static final TRAFFIC_STOP_AND_GO:I = 0x3

.field public static final TRAFFIC_UNKNOWN:I = 0x5

.field private static final serialVersionUID:J = 0x7c548fbdb11eeed9L


# instance fields
.field private arrivalTime:J

.field private incidents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;",
            ">;"
        }
    .end annotation
.end field

.field private polyline:Ljava/lang/String;

.field private routePoints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;"
        }
    .end annotation
.end field

.field private totalDistance:D

.field private travelTime:I

.field private travelTimeInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLRouteTravelTimeInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLRouteInfo;-><init>()V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLRouteInfoImp;->travelTimeInfo:Ljava/util/List;

    .line 33
    return-void
.end method


# virtual methods
.method public addTravelTimeInfoItem(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLRouteTravelTimeInfo;)V
    .locals 1
    .param p1, "item"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLRouteTravelTimeInfo;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLRouteInfoImp;->travelTimeInfo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    return-void
.end method

.method public getArrivalTime()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLRouteInfoImp;->arrivalTime:J

    return-wide v0
.end method

.method public getIncidents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLRouteInfoImp;->incidents:Ljava/util/List;

    return-object v0
.end method

.method public getPolyline()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLRouteInfoImp;->polyline:Ljava/lang/String;

    return-object v0
.end method

.method public getRoutePoints()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLRouteInfoImp;->routePoints:Ljava/util/List;

    return-object v0
.end method

.method public getTotalDistance()D
    .locals 2

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLRouteInfoImp;->totalDistance:D

    return-wide v0
.end method

.method public getTravelTime()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLRouteInfoImp;->travelTime:I

    return v0
.end method

.method public getTravelTimeInfo()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLRouteTravelTimeInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLRouteInfoImp;->travelTimeInfo:Ljava/util/List;

    return-object v0
.end method

.method public setArrivalTime(J)V
    .locals 0
    .param p1, "arrivalTime"    # J

    .prologue
    .line 82
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLRouteInfoImp;->arrivalTime:J

    .line 83
    return-void
.end method

.method public setIncidents(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    .local p1, "incidents":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLRouteInfoImp;->incidents:Ljava/util/List;

    .line 87
    return-void
.end method

.method public setPolyline(Ljava/lang/String;)V
    .locals 0
    .param p1, "polyline"    # Ljava/lang/String;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLRouteInfoImp;->polyline:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public setRoutePoints(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "routePoints":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLRouteInfoImp;->routePoints:Ljava/util/List;

    .line 91
    return-void
.end method

.method public setTotalDistance(D)V
    .locals 0
    .param p1, "totalDistance"    # D

    .prologue
    .line 71
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLRouteInfoImp;->totalDistance:D

    .line 72
    return-void
.end method

.method public setTravelTime(I)V
    .locals 4
    .param p1, "travelTime"    # I

    .prologue
    .line 75
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLRouteInfoImp;->travelTime:I

    .line 78
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    mul-int/lit8 v2, p1, 0x3c

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLRouteInfoImp;->setArrivalTime(J)V

    .line 79
    return-void
.end method

.method public setTravelTimeInfo(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLRouteTravelTimeInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 94
    .local p1, "travelTimeInfo":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLRouteTravelTimeInfo;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/DLRouteInfoImp;->travelTimeInfo:Ljava/util/List;

    .line 95
    return-void
.end method
