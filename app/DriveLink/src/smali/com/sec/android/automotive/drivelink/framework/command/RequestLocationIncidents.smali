.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestLocationIncidents;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestLocationIncidents.java"


# instance fields
.field private location:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V
    .locals 0
    .param p1, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestLocationIncidents;->location:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 38
    return-void
.end method

.method protected process()V
    .locals 0

    .prologue
    .line 21
    return-void
.end method

.method protected response()V
    .locals 0

    .prologue
    .line 25
    return-void
.end method

.method public setLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 0
    .param p1, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestLocationIncidents;->location:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 29
    return-void
.end method

.method public setRadius(D)V
    .locals 0
    .param p1, "radius"    # D

    .prologue
    .line 33
    return-void
.end method
