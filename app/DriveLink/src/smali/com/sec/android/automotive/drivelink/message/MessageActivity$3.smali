.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private isLoaded:Z

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 2006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2007
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->isLoaded:Z

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Z
    .locals 1

    .prologue
    .line 2007
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->isLoaded:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;Z)V
    .locals 0

    .prologue
    .line 2007
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->isLoaded:Z

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    .locals 1

    .prologue
    .line 2006
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    return-object v0
.end method


# virtual methods
.method public OnContactChoice(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "displayedChoices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v5, 0x0

    const/16 v3, 0x8

    .line 2101
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-static {v1, p1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$19(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Ljava/util/List;)V

    .line 2102
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    const/16 v2, 0x42

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$5(Lcom/sec/android/automotive/drivelink/message/MessageActivity;I)V

    .line 2104
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactChoices:Ljava/util/List;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$20(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactChoices:Ljava/util/List;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$20(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 2105
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNolistText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$21(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/TextView;

    move-result-object v1

    const v2, 0x7f0a035a

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 2106
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNoListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$7(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2107
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$6(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2108
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$22(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2109
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$23(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2110
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$8(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2187
    :goto_0
    return-void

    .line 2115
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$24(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->setCurrentMode(I)V

    .line 2117
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$6(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2118
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNoListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$7(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2119
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$8(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2120
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$22(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2121
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$23(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2123
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$25(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2124
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    new-instance v2, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    .line 2125
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 2126
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getSearchedItemOnClickListener()Landroid/view/View$OnClickListener;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$26(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-direct {v2, v3, p1, v4}, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Landroid/view/View$OnClickListener;)V

    .line 2124
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$27(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;)V

    .line 2127
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$28(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$25(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 2136
    :goto_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    new-instance v2, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$31(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;)V

    .line 2137
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2138
    const/4 v1, -0x1

    const/4 v2, -0x2

    .line 2137
    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2139
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2140
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2141
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2142
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v1

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setGravity(I)V

    .line 2143
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$29(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2144
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$25(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->getPageCount()I

    move-result v2

    .line 2145
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$17(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/view/View$OnClickListener;

    move-result-object v3

    .line 2144
    invoke-virtual {v1, v2, v5, v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setDotIndicator(IILandroid/view/View$OnClickListener;)V

    .line 2146
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$28(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    .line 2147
    new-instance v2, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$2;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$2;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    goto/16 :goto_0

    .line 2129
    .end local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$29(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 2130
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$25(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->setContactChoiceList(Ljava/util/List;)V

    .line 2131
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$25(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->notifyDataSetChanged()V

    .line 2132
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$28(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$25(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;

    move-result-object v2

    .line 2133
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->getInitialPosition()I

    move-result v2

    .line 2132
    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto/16 :goto_1
.end method

.method public OnContactTypeInfo(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 7
    .param p1, "ContactMatch"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const/16 v3, 0x8

    const/4 v6, 0x0

    .line 2018
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-static {v1, p1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$4(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 2020
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    const/16 v2, 0x41

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$5(Lcom/sec/android/automotive/drivelink/message/MessageActivity;I)V

    .line 2024
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$6(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2025
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNoListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$7(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2026
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$8(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2028
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$9(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2029
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    new-instance v2, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;

    .line 2030
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$10(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;

    move-result-object v4

    .line 2031
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getPhoneTypeItemOnClickListener()Landroid/view/View$OnClickListener;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$11(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V

    .line 2029
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$12(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;)V

    .line 2032
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$13(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$9(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 2043
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    new-instance v2, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    .line 2044
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;-><init>(Landroid/content/Context;)V

    .line 2043
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$16(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;)V

    .line 2045
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2046
    const/4 v1, -0x1

    const/4 v2, -0x2

    .line 2045
    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2047
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2048
    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2049
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$15(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2050
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$15(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v1

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setGravity(I)V

    .line 2051
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypePageNavigationLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$14(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$15(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2052
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$15(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v1

    .line 2053
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$9(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->getPageCount()I

    move-result v2

    .line 2054
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$17(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/view/View$OnClickListener;

    move-result-object v3

    .line 2052
    invoke-virtual {v1, v2, v6, v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setDotIndicator(IILandroid/view/View$OnClickListener;)V

    .line 2055
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$13(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    .line 2056
    new-instance v2, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$1;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$1;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 2097
    return-void

    .line 2034
    .end local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypePageNavigationLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$14(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    .line 2035
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$15(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 2036
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$9(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;

    move-result-object v1

    .line 2037
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNumberUserPhoneNumbers:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$10(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->setContactTypeList(Ljava/util/ArrayList;)V

    .line 2038
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$9(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->notifyDataSetChanged()V

    .line 2039
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$13(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$9(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;

    move-result-object v2

    .line 2040
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->getInitialPosition()I

    move-result v2

    .line 2039
    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto/16 :goto_0
.end method

.method public OnInboxChoice(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "displayedChoices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;>;"
    const/4 v5, 0x0

    const/16 v3, 0x8

    .line 2191
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-static {v1, p1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$32(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Ljava/util/ArrayList;)V

    .line 2192
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    const/16 v2, 0x42

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$5(Lcom/sec/android/automotive/drivelink/message/MessageActivity;I)V

    .line 2194
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$33(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$33(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 2195
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNolistText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$21(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/TextView;

    move-result-object v1

    const v2, 0x7f0a020e

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 2196
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNoListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$7(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2197
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$6(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2198
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$22(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2199
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$23(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2200
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$8(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2280
    :goto_0
    return-void

    .line 2205
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$24(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->setCurrentMode(I)V

    .line 2207
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$6(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2208
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNoListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$7(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2209
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$8(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2210
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$22(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2211
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$23(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2213
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2214
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    new-instance v2, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    .line 2215
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 2216
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getSearchedItemOnClickListener()Landroid/view/View$OnClickListener;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$26(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-direct {v2, v3, p1, v4}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V

    .line 2214
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$35(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;)V

    .line 2217
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$28(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 2226
    :goto_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    new-instance v2, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$31(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;)V

    .line 2227
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2228
    const/4 v1, -0x1

    const/4 v2, -0x2

    .line 2227
    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2229
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2230
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2231
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2232
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v1

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setGravity(I)V

    .line 2233
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$29(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2234
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v1

    .line 2235
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getPageCount()I

    move-result v2

    .line 2236
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$17(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/view/View$OnClickListener;

    move-result-object v3

    .line 2234
    invoke-virtual {v1, v2, v5, v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setDotIndicator(IILandroid/view/View$OnClickListener;)V

    .line 2237
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$28(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    .line 2238
    new-instance v2, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$3;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$3;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    goto/16 :goto_0

    .line 2219
    .end local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$29(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 2220
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->setInboxList(Ljava/util/ArrayList;)V

    .line 2221
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->notifyDataSetChanged()V

    .line 2222
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$28(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v2

    .line 2223
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getInitialPosition()I

    move-result v2

    .line 2222
    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto/16 :goto_1
.end method

.method public OnInboxSearchChoice(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "searchedInboxList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;>;"
    const/4 v6, 0x0

    const/16 v3, 0x8

    .line 2287
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-static {v1, p1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$36(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Ljava/util/ArrayList;)V

    .line 2288
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    const/16 v2, 0x30

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$5(Lcom/sec/android/automotive/drivelink/message/MessageActivity;I)V

    .line 2290
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$37(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2291
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$37(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 2292
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNolistText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$21(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/TextView;

    move-result-object v1

    const v2, 0x7f0a020e

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 2293
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNoListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$7(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2294
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$6(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2295
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$22(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2296
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$23(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2297
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$8(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2376
    :goto_0
    return-void

    .line 2302
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$24(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->setCurrentMode(I)V

    .line 2304
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$6(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2305
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mNoListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$7(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2306
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactTypeListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$8(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2307
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$22(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2308
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchListLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$23(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2310
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2311
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    new-instance v2, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    .line 2312
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$37(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;

    move-result-object v4

    .line 2313
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getSearchedInboxItemOnClickListener()Landroid/view/View$OnClickListener;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$38(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V

    .line 2311
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$35(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;)V

    .line 2314
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$28(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 2323
    :goto_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    new-instance v2, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$31(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;)V

    .line 2324
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2325
    const/4 v1, -0x1

    const/4 v2, -0x2

    .line 2324
    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2326
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2327
    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2328
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2329
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v1

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setGravity(I)V

    .line 2330
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$29(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2331
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v1

    .line 2332
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getPageCount()I

    move-result v2

    .line 2333
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$17(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/view/View$OnClickListener;

    move-result-object v3

    .line 2331
    invoke-virtual {v1, v2, v6, v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setDotIndicator(IILandroid/view/View$OnClickListener;)V

    .line 2334
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$28(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    .line 2335
    new-instance v2, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$4;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$4;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    goto/16 :goto_0

    .line 2316
    .end local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchPageNavigationLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$29(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 2317
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$37(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->setInboxList(Ljava/util/ArrayList;)V

    .line 2318
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->notifyDataSetChanged()V

    .line 2319
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$28(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v2

    .line 2320
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getInitialPosition()I

    move-result v2

    .line 2319
    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto/16 :goto_1
.end method

.method public OnInputTextUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    .line 2013
    return-void
.end method
