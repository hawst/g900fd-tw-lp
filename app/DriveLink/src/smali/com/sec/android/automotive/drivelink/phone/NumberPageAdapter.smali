.class public Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "NumberPageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;,
        Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;
    }
.end annotation


# static fields
.field public static final ITEM_NUM_PER_PAGE:I = 0x4

.field private static final MAX_PAGE_TO_LOOP:I = 0x4e20


# instance fields
.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mDLPhoneNumbers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;"
        }
    .end annotation
.end field

.field private mInitialPosition:I

.field private mIsNightMode:Z

.field private mPageCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "clickListener"    # Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "dlPhoneNumbers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    const/4 v2, 0x0

    .line 58
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 49
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mIsNightMode:Z

    .line 61
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mContext:Landroid/content/Context;

    .line 62
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mDLPhoneNumbers:Ljava/util/ArrayList;

    .line 63
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->getPageCount()I

    move-result v0

    .line 66
    .local v0, "pageCount":I
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 68
    const/16 v1, 0x2710

    rem-int/2addr v1, v0

    rsub-int v1, v1, 0x2710

    .line 67
    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mInitialPosition:I

    .line 69
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mPageCount:I

    .line 74
    :goto_0
    return-void

    .line 71
    :cond_0
    iput v2, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mInitialPosition:I

    .line 72
    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mPageCount:I

    goto :goto_0
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 207
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 208
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 217
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mPageCount:I

    return v0
.end method

.method public getInitialPosition()I
    .locals 1

    .prologue
    .line 263
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mInitialPosition:I

    return v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 271
    const/4 v0, -0x2

    return v0
.end method

.method public getPageCount()I
    .locals 3

    .prologue
    .line 230
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mDLPhoneNumbers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    div-int/lit8 v0, v2, 0x4

    .line 232
    .local v0, "page":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mDLPhoneNumbers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    rem-int/lit8 v1, v2, 0x4

    .line 234
    .local v1, "remainder":I
    if-lez v1, :cond_0

    .line 235
    add-int/lit8 v0, v0, 0x1

    .line 238
    :cond_0
    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 15
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 96
    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mContext:Landroid/content/Context;

    invoke-static {v11}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 97
    .local v6, "layoutInflater":Landroid/view/LayoutInflater;
    const v11, 0x7f030074

    const/4 v12, 0x0

    invoke-virtual {v6, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 99
    .local v10, "v":Landroid/view/View;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 100
    .local v7, "listItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/widget/RelativeLayout;>;"
    const v11, 0x7f090248

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    const v11, 0x7f090249

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    const v11, 0x7f09024a

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    const v11, 0x7f09024b

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->getPageCount()I

    move-result v11

    rem-int v11, p2, v11

    mul-int/lit8 v4, v11, 0x4

    .line 108
    .local v4, "index":I
    const/4 v8, 0x0

    .line 109
    .local v8, "maskedBitmap":Landroid/graphics/Bitmap;
    const/4 v1, 0x0

    .line 112
    .local v1, "dlPhoneNumber":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    const/4 v3, 0x1

    .line 113
    .local v3, "i":I
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_0

    .line 193
    move/from16 v0, p2

    invoke-virtual {v10, v0}, Landroid/view/View;->setId(I)V

    .line 194
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    .line 195
    return-object v10

    .line 113
    .restart local p1    # "container":Landroid/view/ViewGroup;
    :cond_0
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    .line 114
    .local v5, "layout":Landroid/widget/RelativeLayout;
    new-instance v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;

    const/4 v11, 0x0

    invoke-direct {v2, v11}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;-><init>(Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;)V

    .line 116
    .local v2, "holder":Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;
    const v11, 0x7f090244

    invoke-virtual {v5, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    .line 115
    iput-object v11, v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;->ivPhoneNumberImage:Landroid/widget/ImageView;

    .line 118
    const v11, 0x7f090245

    invoke-virtual {v5, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    .line 117
    iput-object v11, v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;->ivPhoneNumberMaskImage:Landroid/widget/ImageView;

    .line 120
    const v11, 0x7f090246

    invoke-virtual {v5, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 119
    iput-object v11, v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;->tvPhoneNumberName:Landroid/widget/TextView;

    .line 122
    const v11, 0x7f090247

    invoke-virtual {v5, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 121
    iput-object v11, v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;->tvPhoneNumber:Landroid/widget/TextView;

    .line 124
    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mDLPhoneNumbers:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-le v11, v4, :cond_5

    .line 126
    invoke-virtual {v5, v3}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 127
    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v11}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mDLPhoneNumbers:Ljava/util/ArrayList;

    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "dlPhoneNumber":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    .line 132
    .restart local v1    # "dlPhoneNumber":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneType()I

    move-result v11

    invoke-static {v11}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->getPhoneNumberDisplayType(I)Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    move-result-object v9

    .line 134
    .local v9, "phoneNumberDisplayType":Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;
    iget-boolean v11, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mIsNightMode:Z

    if-eqz v11, :cond_2

    .line 135
    iget-object v11, v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;->ivPhoneNumberImage:Landroid/widget/ImageView;

    .line 137
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->getPhoneDrawableNight()I

    move-result v13

    .line 136
    invoke-virtual {v11, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 150
    :goto_1
    iget-object v11, v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;->tvPhoneNumberName:Landroid/widget/TextView;

    .line 151
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->getStringResource()I

    move-result v13

    .line 150
    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(I)V

    .line 152
    iget-object v11, v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;->tvPhoneNumber:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    iget-object v11, v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;->ivPhoneNumberMaskImage:Landroid/widget/ImageView;

    .line 155
    const v13, 0x7f02020b

    invoke-virtual {v11, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 156
    iget-boolean v11, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mIsNightMode:Z

    if-eqz v11, :cond_4

    .line 158
    iget-object v11, v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;->tvPhoneNumberName:Landroid/widget/TextView;

    .line 159
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/automotive/drivelink/DLApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 160
    const v14, 0x7f080030

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    .line 158
    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 161
    iget-object v11, v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;->tvPhoneNumber:Landroid/widget/TextView;

    .line 162
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/automotive/drivelink/DLApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 163
    const v14, 0x7f080030

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    .line 161
    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 164
    const v11, 0x7f020002

    invoke-virtual {v5, v11}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 189
    .end local v9    # "phoneNumberDisplayType":Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;
    :cond_1
    :goto_2
    add-int/lit8 v4, v4, 0x1

    .line 190
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 139
    .restart local v9    # "phoneNumberDisplayType":Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;
    :cond_2
    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v11

    iget v11, v11, Landroid/content/res/Configuration;->orientation:I

    const/4 v13, 0x2

    if-ne v11, v13, :cond_3

    .line 140
    iget-object v11, v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;->ivPhoneNumberImage:Landroid/widget/ImageView;

    .line 142
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->getPhoneDrawableLandscape()I

    move-result v13

    .line 141
    invoke-virtual {v11, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 144
    :cond_3
    iget-object v11, v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;->ivPhoneNumberImage:Landroid/widget/ImageView;

    .line 146
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->getPhoneDrawablePortrait()I

    move-result v13

    .line 145
    invoke-virtual {v11, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 167
    :cond_4
    iget-object v11, v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;->tvPhoneNumberName:Landroid/widget/TextView;

    .line 168
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/automotive/drivelink/DLApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 169
    const v14, 0x7f08002f

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    .line 167
    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 170
    iget-object v11, v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;->tvPhoneNumber:Landroid/widget/TextView;

    .line 171
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/automotive/drivelink/DLApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 172
    const v14, 0x7f08002f

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    .line 170
    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 178
    .end local v9    # "phoneNumberDisplayType":Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;
    :cond_5
    iget-object v11, v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;->ivPhoneNumberImage:Landroid/widget/ImageView;

    const/16 v13, 0x8

    invoke-virtual {v11, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 179
    iget-object v11, v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;->ivPhoneNumberMaskImage:Landroid/widget/ImageView;

    const/16 v13, 0x8

    invoke-virtual {v11, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 180
    iget-object v11, v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;->tvPhoneNumber:Landroid/widget/TextView;

    const/16 v13, 0x8

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 181
    iget-object v11, v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$ViewHolder;->tvPhoneNumberName:Landroid/widget/TextView;

    const/16 v13, 0x8

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 182
    iget-boolean v11, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mIsNightMode:Z

    if-eqz v11, :cond_1

    .line 183
    const v11, 0x7f020002

    invoke-virtual {v5, v11}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_2
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 250
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setDLPhoneNumbers(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 77
    .local p1, "phoneNumbers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mDLPhoneNumbers:Ljava/util/ArrayList;

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->getPageCount()I

    move-result v0

    .line 80
    .local v0, "pageCount":I
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 82
    const/16 v1, 0x2710

    rem-int/2addr v1, v0

    rsub-int v1, v1, 0x2710

    .line 81
    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mInitialPosition:I

    .line 83
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mPageCount:I

    .line 88
    :goto_0
    return-void

    .line 85
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mInitialPosition:I

    .line 86
    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mPageCount:I

    goto :goto_0
.end method

.method public setNightMode(Z)V
    .locals 0
    .param p1, "isNightMode"    # Z

    .prologue
    .line 91
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;->mIsNightMode:Z

    .line 92
    return-void
.end method
