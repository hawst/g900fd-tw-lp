.class public Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;
.super Ljava/lang/Object;
.source "DriveLinkServiceInterfaceImp.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;


# static fields
.field private static final MAX_THREAD:I = 0xa

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

.field private mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

.field private mbInitialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 102
    const-class v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 103
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 102
    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    .line 104
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    .line 106
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 107
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mbInitialized:Z

    .line 110
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    .line 111
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 112
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mbInitialized:Z

    .line 113
    return-void
.end method

.method private getMethodName()Ljava/lang/String;
    .locals 7

    .prologue
    .line 118
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    .line 119
    .local v2, "sts":[Ljava/lang/StackTraceElement;
    if-nez v2, :cond_0

    .line 120
    const/4 v3, 0x0

    .line 136
    :goto_0
    return-object v3

    .line 123
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 124
    .local v1, "strDump":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    array-length v4, v2

    const/4 v3, 0x0

    :goto_1
    if-lt v3, v4, :cond_1

    .line 136
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 125
    :cond_1
    aget-object v0, v2, v3

    .line 126
    .local v0, "st":Ljava/lang/StackTraceElement;
    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->isNativeMethod()Z

    move-result v5

    if-nez v5, :cond_2

    .line 127
    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "getStackTrace"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 128
    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "getMethodName"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 125
    :cond_2
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 132
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "### "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "::"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 133
    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ")\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 132
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method


# virtual methods
.method public addDLCallMessageLogData(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "DLLog"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .prologue
    .line 979
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 980
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 981
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 980
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 987
    :goto_0
    return-void

    .line 985
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddDLCallMessageLogData;

    .line 986
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddDLCallMessageLogData;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)V

    .line 985
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public addFavoriteContact(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 1912
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1913
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1914
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1913
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1915
    const/4 v0, 0x0

    .line 1918
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->addFavoriteContact(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Z

    move-result v0

    goto :goto_0
.end method

.method public clearNewMissedCallFromCallLog(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1002
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1003
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1004
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1003
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1009
    :goto_0
    return-void

    .line 1008
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->clearNewMissedCallFromCallLog(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public dbGetGroupInfo()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1993
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1994
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1995
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1994
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2003
    :cond_0
    :goto_0
    return-object v1

    .line 1999
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v0

    .line 2000
    .local v0, "dbHelper":Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;
    if-eqz v0, :cond_0

    .line 2003
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getLocationGroup()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    move-result-object v1

    goto :goto_0
.end method

.method public dbGetLocation(DD)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .locals 3
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 2008
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2009
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2010
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2009
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2011
    const/4 v0, 0x0

    .line 2014
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getInstance()Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->getLocation(DD)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v0

    goto :goto_0
.end method

.method public getAllMusicList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 479
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 480
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 481
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 480
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getAllMusicSearchList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchKeyword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 637
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 638
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 639
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 638
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    const/4 v0, 0x0

    .line 643
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 644
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getAllMusicSearchList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getBTPairingType(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1295
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1296
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1297
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1296
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1298
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    .line 1301
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getBTPairingType(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    move-result-object v0

    goto :goto_0
.end method

.method public getBluetoothCommunicatingState(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1339
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1340
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1341
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1340
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1342
    const/4 v0, 0x0

    .line 1345
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1346
    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getBluetoothCommunicatingState(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method public getBluetoothConnectedState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 1352
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1353
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1354
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1353
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1355
    const/4 v0, 0x0

    .line 1358
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getBluetoothConnectedState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    goto :goto_0
.end method

.method public getBluetoothConnectedState(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "btDeviceMacAddress"    # Ljava/lang/String;

    .prologue
    .line 1365
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1366
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1367
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1366
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1368
    const/4 v0, 0x0

    .line 1371
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getBluetoothConnectedState(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public getBluetoothConnectionState(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1328
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1329
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1330
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1329
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1331
    const/4 v0, 0x0

    .line 1334
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getBluetoothConnectionState(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method public getBluetoothDevicePairedState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 1414
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1415
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1416
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1415
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1417
    const/4 v0, 0x0

    .line 1420
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getBluetoothDevicePairedState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    goto :goto_0
.end method

.method public getBluetoothDevicesDiscovered(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1402
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1403
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1404
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1403
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1405
    const/4 v0, 0x0

    .line 1408
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getBluetoothDevicesDiscovered(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getBluetoothProfileConnectionState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 1378
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1379
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1380
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1379
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1381
    const/4 v0, 0x0

    .line 1384
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getBluetoothProfileConnectionState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    goto :goto_0
.end method

.method public getCallLogImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "calllog"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .prologue
    .line 749
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 750
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 751
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 750
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 752
    const/4 v0, 0x0

    .line 755
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getCallLogImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getCarSpeedStatus(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 931
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 932
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 933
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 932
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 934
    const/4 v0, 0x0

    .line 937
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getCarSpeedStatus(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;

    move-result-object v0

    goto :goto_0
.end method

.method public getContactFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 2035
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2036
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2037
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2036
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2038
    const/4 v0, 0x0

    .line 2041
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContactFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v0

    goto :goto_0
.end method

.method public getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 738
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 739
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 740
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 739
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 741
    const/4 v0, 0x0

    .line 744
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getContactImageFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 1757
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1758
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1759
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1758
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1760
    const/4 v0, 0x0

    .line 1763
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContactImageFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getDevice(Landroid/content/Context;)Landroid/bluetooth/BluetoothDevice;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1390
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1391
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1392
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1391
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1393
    const/4 v0, 0x0

    .line 1396
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDevice(Landroid/content/Context;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    goto :goto_0
.end method

.method public getDeviceName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1306
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1307
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1308
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1307
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1309
    const/4 v0, 0x0

    .line 1312
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    return-object v0
.end method

.method public getLocation(Landroid/content/Context;)Landroid/location/Location;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 942
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 943
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 944
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 943
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 945
    const/4 v0, 0x0

    .line 948
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocation(Landroid/content/Context;)Landroid/location/Location;

    move-result-object v0

    goto :goto_0
.end method

.method public getLocation(DD)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .locals 3
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 1781
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1782
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1783
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1782
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1784
    const/4 v0, 0x0

    .line 1787
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getLocation(DD)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v0

    goto :goto_0
.end method

.method public getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "album"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    .prologue
    .line 705
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 706
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 707
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 706
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    const/4 v0, 0x0

    .line 711
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "artist"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .prologue
    .line 716
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 717
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 718
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 717
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 719
    const/4 v0, 0x0

    .line 722
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "folder"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;

    .prologue
    .line 727
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 728
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 729
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 728
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 730
    const/4 v0, 0x0

    .line 733
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 694
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 695
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 696
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 695
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 697
    const/4 v0, 0x0

    .line 700
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getMusicCount(Landroid/content/Context;)I
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 649
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 650
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 651
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 650
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 652
    const/4 v0, 0x0

    .line 655
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMusicCount(Landroid/content/Context;)I

    move-result v0

    goto :goto_0
.end method

.method public getMusicPlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
    .locals 3

    .prologue
    .line 683
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 684
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 685
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 684
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    const/4 v0, 0x0

    .line 689
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMusicPlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v0

    goto :goto_0
.end method

.method public getMusicPlaylistManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;
    .locals 3

    .prologue
    .line 672
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 673
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 674
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 673
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    const/4 v0, 0x0

    .line 678
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMusicPlaylistManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    move-result-object v0

    goto :goto_0
.end method

.method public getMusicRemotePlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;
    .locals 3

    .prologue
    .line 2047
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2048
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2049
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2048
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2050
    const/4 v0, 0x0

    .line 2053
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMusicRemotePlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    move-result-object v0

    goto :goto_0
.end method

.method public getNewMissedCallCount(Landroid/content/Context;)I
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 991
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 992
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 993
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 992
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 994
    const/4 v0, 0x0

    .line 997
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getNewMissedCallCount(Landroid/content/Context;)I

    move-result v0

    goto :goto_0
.end method

.method public getPassKey(Landroid/content/Context;)I
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1317
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1318
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1319
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1318
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1320
    const/4 v0, 0x0

    .line 1323
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getPassKey(Landroid/content/Context;)I

    move-result v0

    goto :goto_0
.end method

.method public getPhoneNumberList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Ljava/util/ArrayList;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2093
    if-nez p2, :cond_1

    .line 2104
    :cond_0
    :goto_0
    return-object v3

    :cond_1
    move-object v0, p2

    .line 2097
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 2098
    .local v0, "_contact":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->getContactId()J

    move-result-wide v1

    .line 2100
    .local v1, "contactId":J
    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-lez v4, :cond_0

    .line 2104
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v3

    invoke-virtual {v3, p1, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getPhoneNumberList(Landroid/content/Context;J)Ljava/util/ArrayList;

    move-result-object v3

    goto :goto_0
.end method

.method public getUserProfile()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .locals 3

    .prologue
    .line 1832
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1833
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1834
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1833
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1835
    const/4 v0, 0x0

    .line 1838
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getUserProfile()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v0

    goto :goto_0
.end method

.method public declared-synchronized initialize(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;

    .prologue
    const/4 v0, 0x1

    .line 142
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "drivelink framework initialize start"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 144
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "drivelink framework initialize fail"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    const/4 v0, 0x0

    .line 162
    :goto_0
    monitor-exit p0

    return v0

    .line 148
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->createDB(Landroid/content/Context;)Z

    .line 150
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->startThreadPool()V

    .line 152
    if-nez p2, :cond_1

    .line 153
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v1, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->initialize(Landroid/content/Context;)Z

    .line 159
    :goto_1
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->setInitializedFlag(Z)V

    .line 161
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "drivelink framework initialize end"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 155
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceInitialize;

    .line 156
    invoke-direct {v2, p1, p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceInitialize;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;)V

    .line 155
    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "mCountry"    # Ljava/lang/String;
    .param p2, "mSaleCode"    # Ljava/lang/String;
    .param p3, "mNavigation"    # Ljava/lang/String;

    .prologue
    .line 1073
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1074
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1075
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1074
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1076
    const/4 v0, 0x0

    .line 1079
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 201
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mbInitialized:Z

    return v0
.end method

.method public isMirrorLinkRunning()Z
    .locals 3

    .prologue
    .line 1451
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1452
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1453
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1452
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1454
    const/4 v0, 0x0

    .line 1457
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMirrorLinkManager()Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;

    move-result-object v0

    .line 1458
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;->isMirrorLinkRunning()Z

    move-result v0

    goto :goto_0
.end method

.method public registerMusicRemoteControl(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 2071
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2072
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2073
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2072
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2077
    :goto_0
    return-void

    .line 2076
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->registerMusicRemoteControl(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public registerNotification(Landroid/content/Context;ZZZZZ)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "call"    # Z
    .param p3, "msg"    # Z
    .param p4, "btConnect"    # Z
    .param p5, "locationShare"    # Z
    .param p6, "alarm"    # Z

    .prologue
    .line 829
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 830
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 831
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 830
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    :goto_0
    return-void

    .line 835
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->registerNotification(Landroid/content/Context;ZZZZZ)V

    goto :goto_0
.end method

.method public removeFavoriteContact(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 1924
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1925
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1926
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1925
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1932
    :goto_0
    return-void

    .line 1930
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->removeFavoriteContact(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    goto :goto_0
.end method

.method public requestAcceptLocationShared(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "friendPhoneNumber"    # Ljava/lang/String;
    .param p3, "url"    # Ljava/lang/String;

    .prologue
    .line 1650
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1651
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1652
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1651
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1660
    :goto_0
    return-void

    .line 1656
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;

    .line 1657
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1656
    invoke-direct {v0, p1, v1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/lang/String;Ljava/lang/String;)V

    .line 1659
    .local v0, "request":Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptLocationShared;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestAcceptShareMyLocation(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "groupUrl"    # Ljava/lang/String;
    .param p3, "myStatus"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;
    .param p4, "myLocation"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p5, "duration"    # J

    .prologue
    .line 1741
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1742
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1743
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1742
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1752
    :goto_0
    return-void

    .line 1747
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;

    .line 1748
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    .line 1747
    invoke-direct/range {v0 .. v7}, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)V

    .line 1751
    .local v0, "request":Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestAddFriendsToGroup(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1665
    .local p2, "friends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1666
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1667
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1666
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1675
    :goto_0
    return-void

    .line 1671
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;

    .line 1672
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1671
    invoke-direct {v0, p1, v1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V

    .line 1674
    .local v0, "request":Lcom/sec/android/automotive/drivelink/framework/command/RequestAddFriendToGroup;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestAlbumList(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 551
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 552
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 553
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 552
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    :goto_0
    return-void

    .line 557
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestAlbumList;

    .line 558
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestAlbumList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 557
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestAllParticipantTracking(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 4
    .param p1, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 1793
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1794
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1795
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1794
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1803
    :goto_0
    return-void

    .line 1799
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAllParticipantTracking;

    .line 1800
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1799
    invoke-direct {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/framework/command/RequestAllParticipantTracking;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V

    .line 1802
    .local v0, "request":Lcom/sec/android/automotive/drivelink/framework/command/RequestAllParticipantTracking;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestArtistList(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 563
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 564
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 565
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 564
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    :goto_0
    return-void

    .line 569
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestArtistList;

    .line 570
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestArtistList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 569
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestBluetoothConnectDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 1270
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1271
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1272
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1271
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1278
    :goto_0
    return-void

    .line 1276
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothConnectDevice;

    .line 1277
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothConnectDevice;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Landroid/bluetooth/BluetoothDevice;)V

    .line 1276
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestBluetoothDisconnectDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 1283
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1284
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1285
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1284
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1291
    :goto_0
    return-void

    .line 1289
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothDisconnectDevice;

    .line 1290
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothDisconnectDevice;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Landroid/bluetooth/BluetoothDevice;)V

    .line 1289
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestBluetoothMakeDeviceDiscoverable(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1169
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1170
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1171
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1170
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1177
    :goto_0
    return-void

    .line 1175
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothMakeDeviceDiscoverable;

    .line 1176
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothMakeDeviceDiscoverable;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 1175
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestBluetoothPairDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 1244
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1245
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1246
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1245
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1252
    :goto_0
    return-void

    .line 1250
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairDevice;

    .line 1251
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairDevice;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Landroid/bluetooth/BluetoothDevice;)V

    .line 1250
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestBluetoothPairingNSSPAnswer(Landroid/content/Context;Ljava/lang/String;[B)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "inputPIN"    # [B

    .prologue
    .line 1218
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1219
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1220
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1219
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1226
    :goto_0
    return-void

    .line 1224
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;

    .line 1225
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/lang/String;[B)V

    .line 1224
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestBluetoothPairingSSPAnswer(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "answer"    # Z

    .prologue
    .line 1231
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1232
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1233
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1232
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1239
    :goto_0
    return-void

    .line 1237
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingSSPAnswer;

    .line 1238
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingSSPAnswer;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/lang/String;Z)V

    .line 1237
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestBluetoothSearchDiscoverableDevices(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1181
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1182
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1183
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1182
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1189
    :goto_0
    return-void

    .line 1187
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothSearchDiscoverableDevices;

    .line 1188
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothSearchDiscoverableDevices;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 1187
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestBluetoothSearchPairedDevices(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1205
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1206
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1207
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1206
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1213
    :goto_0
    return-void

    .line 1211
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothSearchPairedDevices;

    .line 1212
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothSearchPairedDevices;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 1211
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestBluetoothStartAdapter(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1145
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1146
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1147
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1146
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1153
    :goto_0
    return-void

    .line 1151
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothStartAdapter;

    .line 1152
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothStartAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 1151
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestBluetoothStopAdapter(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1157
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1158
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1159
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1158
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1165
    :goto_0
    return-void

    .line 1163
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothStopAdapter;

    .line 1164
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothStopAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 1163
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestBluetoothStopDiscoveringDevices(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1193
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1194
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1195
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1194
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1201
    :goto_0
    return-void

    .line 1199
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothStopDiscoveringDevices;

    .line 1200
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothStopDiscoveringDevices;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 1199
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestBluetoothUnpairDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 1257
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1258
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1259
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1258
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    :goto_0
    return-void

    .line 1263
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothUnpairDevice;

    .line 1264
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothUnpairDevice;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Landroid/bluetooth/BluetoothDevice;)V

    .line 1263
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestCallLogList(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I

    .prologue
    .line 357
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 358
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 359
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 358
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    :goto_0
    return-void

    .line 363
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestCallLogList;

    .line 364
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestCallLogList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;I)V

    .line 363
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestCancelUpdateApplication(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1110
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1111
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1112
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1111
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1117
    :goto_0
    return-void

    .line 1116
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->requestCancelUpdateApplication(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public requestCancelUpdateVersionCheck(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1121
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1122
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1123
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1122
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1128
    :goto_0
    return-void

    .line 1127
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->requestCancelUpdateVersionCheck(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public requestChangeGroupDestination(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .param p3, "userLocation"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 1858
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1859
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1860
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1859
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1867
    :goto_0
    return-void

    .line 1864
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/command/RequestChangeGroupDestination;

    .line 1865
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1864
    invoke-direct {v0, p1, v1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/command/RequestChangeGroupDestination;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 1866
    .local v0, "request":Lcom/sec/android/automotive/drivelink/framework/command/RequestChangeGroupDestination;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestChangeMessageStatusToRead(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .prologue
    .line 432
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 433
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 434
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 433
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    :goto_0
    return-void

    .line 438
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestChangeMessageStatusToRead;

    .line 439
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, p2, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestChangeMessageStatusToRead;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 438
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestCheckUpdateVersion(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1085
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1086
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1087
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1086
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1093
    :goto_0
    return-void

    .line 1091
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestCheckUpdateVersion;

    .line 1092
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestCheckUpdateVersion;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 1091
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestContactList(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I

    .prologue
    .line 760
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 761
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 762
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 761
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 769
    :goto_0
    return-void

    .line 766
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestContactList;

    .line 767
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestContactList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;I)V

    .line 766
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestCreateGroupShare(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .param p3, "myLocation"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p4, "duration"    # J

    .prologue
    .line 1725
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1726
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1727
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1726
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1735
    :goto_0
    return-void

    .line 1731
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCreateGroupShare;

    .line 1732
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-wide v5, p4

    .line 1731
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/command/RequestCreateGroupShare;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)V

    .line 1734
    .local v0, "request":Lcom/sec/android/automotive/drivelink/framework/command/RequestCreateGroupShare;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestCreateUserProfile(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "profile"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    .prologue
    .line 876
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 877
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 878
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 877
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 891
    :goto_0
    return-void

    .line 882
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCreateUserProfile;

    .line 883
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 882
    invoke-direct {v0, p1, v1, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestCreateUserProfile;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)V

    .line 885
    .local v0, "requestCreateUserProfile":Lcom/sec/android/automotive/drivelink/framework/command/RequestCreateUserProfile;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    if-nez v1, :cond_1

    .line 886
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Attribute mThreadPool is null."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 890
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestDLCallMessageLogList(Landroid/content/Context;Ljava/util/Date;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fromDate"    # Ljava/util/Date;
    .param p3, "type"    # I

    .prologue
    .line 967
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 968
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 969
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 968
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 975
    :goto_0
    return-void

    .line 973
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLCallMessageLogList;

    .line 974
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLCallMessageLogList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/util/Date;I)V

    .line 973
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestFavoriteContactList(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1884
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1885
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1886
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1885
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1893
    :goto_0
    return-void

    .line 1890
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;

    .line 1891
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1890
    invoke-direct {v0, p1, v1}, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 1892
    .local v0, "request":Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestFavoriteContactList(Landroid/content/Context;Z)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mRescanContactDB"    # Z

    .prologue
    .line 1899
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1900
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1901
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1900
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1908
    :goto_0
    return-void

    .line 1905
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;

    .line 1906
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1905
    invoke-direct {v0, p1, v1, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Z)V

    .line 1907
    .local v0, "request":Lcom/sec/android/automotive/drivelink/framework/command/RequestFavoriteContactList;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestFriendLocationShare(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "friendPhoneNumber"    # Ljava/lang/String;

    .prologue
    .line 1710
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1711
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1712
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1711
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1720
    :goto_0
    return-void

    .line 1716
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;

    .line 1717
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1716
    invoke-direct {v0, p1, v1, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/lang/String;)V

    .line 1719
    .local v0, "request":Lcom/sec/android/automotive/drivelink/framework/command/RequestFriendLocationShare;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestGroupSharedUpdateInfo(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "friendsGroup"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .param p3, "myLocation"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 1680
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1681
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1682
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1681
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1690
    :goto_0
    return-void

    .line 1686
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;

    .line 1687
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1686
    invoke-direct {v0, p1, v1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 1689
    .local v0, "request":Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestInboxList(Landroid/content/Context;IZI)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I
    .param p3, "isGetAllMessage"    # Z
    .param p4, "msgLimitCount"    # I

    .prologue
    .line 370
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 371
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 372
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 371
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    :goto_0
    return-void

    .line 376
    :cond_0
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;

    .line 377
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;-><init>(Landroid/content/Context;IZILcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 376
    invoke-virtual {v6, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestIncommingMessageList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "inbox"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    .param p3, "itemCount"    # I

    .prologue
    .line 383
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 384
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 385
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 384
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    :goto_0
    return-void

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestIncommingMessageList;

    .line 390
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, p2, p3, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestIncommingMessageList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;ILcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 389
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestMapAvailableByCountryAndSaleCode(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "mCountryCode"    # Ljava/lang/String;
    .param p2, "mSaleCode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1060
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1061
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1062
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1061
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1063
    const/4 v0, 0x0

    .line 1066
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1067
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->requestMapAvailableByCountryAndSaleCode(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public requestMusicFolderList(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 575
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 576
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 577
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 576
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    :goto_0
    return-void

    .line 581
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicFolderList;

    .line 582
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicFolderList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 581
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestMusicList(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 467
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 468
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 469
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 468
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    :goto_0
    return-void

    .line 473
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;

    .line 474
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    const/4 v3, 0x0

    invoke-direct {v1, p1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)V

    .line 473
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "album"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    .prologue
    .line 503
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 504
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 505
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 504
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    :goto_0
    return-void

    .line 509
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;

    .line 510
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)V

    .line 509
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "artist"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .prologue
    .line 515
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 516
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 517
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 516
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    :goto_0
    return-void

    .line 521
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;

    .line 522
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)V

    .line 521
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "folder"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;

    .prologue
    .line 527
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 528
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 529
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 528
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    :goto_0
    return-void

    .line 533
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;

    .line 534
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)V

    .line 533
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 491
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 492
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 493
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 492
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    :goto_0
    return-void

    .line 497
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;

    .line 498
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)V

    .line 497
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "playlist"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;

    .prologue
    .line 539
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 540
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 541
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 540
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    :goto_0
    return-void

    .line 545
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;

    .line 546
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)V

    .line 545
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestMusicListFromSearchResult(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "musicSearchResult"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;
    .param p3, "index"    # I

    .prologue
    .line 612
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 613
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 614
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 613
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    :goto_0
    return-void

    .line 618
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;

    .line 619
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicListFromSearchResult;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;I)V

    .line 618
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestPlaylistList(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 587
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 588
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 589
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 588
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    :goto_0
    return-void

    .line 593
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestPlaylistList;

    .line 594
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestPlaylistList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 593
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestQuitFromGroup(I)V
    .locals 4
    .param p1, "groupId"    # I

    .prologue
    .line 1871
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1872
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1873
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1872
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1880
    :goto_0
    return-void

    .line 1877
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/command/RequestQuitFromGroup;

    .line 1878
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1877
    invoke-direct {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/framework/command/RequestQuitFromGroup;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;I)V

    .line 1879
    .local v0, "request":Lcom/sec/android/automotive/drivelink/framework/command/RequestQuitFromGroup;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestRecommendedContactListForCall(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I

    .prologue
    .line 774
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 775
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 776
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 775
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 783
    :goto_0
    return-void

    .line 780
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForCall;

    .line 781
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForCall;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;I)V

    .line 780
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestRecommendedContactListForCall(Landroid/content/Context;IZ)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I
    .param p3, "useFavoriteList"    # Z

    .prologue
    .line 1951
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1952
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1953
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1952
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1961
    :goto_0
    return-void

    .line 1957
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    .line 1958
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForCall;

    .line 1959
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1958
    invoke-direct {v1, p1, v2, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForCall;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;IZ)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestRecommendedContactListForMessage(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I

    .prologue
    .line 788
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 789
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 790
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 789
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    :goto_0
    return-void

    .line 794
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;

    .line 795
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;I)V

    .line 794
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestRecommendedContactListForMessage(Landroid/content/Context;IZ)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I
    .param p3, "useFavoriteList"    # Z

    .prologue
    .line 1966
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1967
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1968
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1967
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1976
    :goto_0
    return-void

    .line 1972
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    .line 1973
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;

    .line 1974
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1973
    invoke-direct {v1, p1, v2, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;IZ)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestRecommendedLocationList(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I

    .prologue
    .line 852
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 853
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 854
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 853
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 860
    :goto_0
    return-void

    .line 858
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedLocationList;

    .line 859
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedLocationList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;I)V

    .line 858
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestReinviteFriendToGroup(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .param p3, "friend"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    .line 1981
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1982
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1983
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1982
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1989
    :goto_0
    return-void

    .line 1987
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestReiniviteFriendToGroup;

    .line 1988
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/command/RequestReiniviteFriendToGroup;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V

    .line 1987
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestRemoveGroupShared(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 1807
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1808
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1809
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1808
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1817
    :goto_0
    return-void

    .line 1813
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRemoveGroupShared;

    .line 1814
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1813
    invoke-direct {v0, v1, p2, p1}, Lcom/sec/android/automotive/drivelink/framework/command/RequestRemoveGroupShared;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Landroid/content/Context;)V

    .line 1816
    .local v0, "request":Lcom/sec/android/automotive/drivelink/framework/command/RequestRemoveGroupShared;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestRestartGroupShared(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .param p3, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 1844
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1845
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1846
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1845
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1853
    :goto_0
    return-void

    .line 1850
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;

    .line 1851
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1850
    invoke-direct {v0, v1, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 1852
    .local v0, "request":Lcom/sec/android/automotive/drivelink/framework/command/RequestRestartGroupShared;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestSamsungAccountAccessToken(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 1520
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1521
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1522
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1521
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1529
    :goto_0
    return-void

    .line 1526
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;

    .line 1527
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1528
    const/16 v3, 0xad2

    invoke-direct {v1, p1, v2, v3, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;ILandroid/os/Bundle;)V

    .line 1526
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestSamsungAccountAuthCode(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 1561
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1562
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1563
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1562
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1570
    :goto_0
    return-void

    .line 1567
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;

    .line 1568
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1569
    const/16 v3, 0xad5

    invoke-direct {v1, p1, v2, v3, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;ILandroid/os/Bundle;)V

    .line 1567
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestSamsungAccountChecklistValidation(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 1534
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1535
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1536
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1535
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1543
    :goto_0
    return-void

    .line 1540
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;

    .line 1541
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1542
    const/16 v3, 0xad3

    invoke-direct {v1, p1, v2, v3, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;ILandroid/os/Bundle;)V

    .line 1540
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestSamsungAccountConnectService(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1463
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1464
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1465
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1464
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1472
    :goto_0
    return-void

    .line 1469
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;

    .line 1470
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1471
    const/16 v3, 0xace

    const/4 v4, 0x0

    invoke-direct {v1, p1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;ILandroid/os/Bundle;)V

    .line 1469
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestSamsungAccountDisclaimerAgreement(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 1548
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1549
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1550
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1549
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1557
    :goto_0
    return-void

    .line 1554
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;

    .line 1555
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1556
    const/16 v3, 0xad4

    invoke-direct {v1, p1, v2, v3, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;ILandroid/os/Bundle;)V

    .line 1554
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestSamsungAccountDisconnectService(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1476
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1477
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1478
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1477
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1485
    :goto_0
    return-void

    .line 1482
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;

    .line 1483
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1484
    const/16 v3, 0xacf

    const/4 v4, 0x0

    invoke-direct {v1, p1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;ILandroid/os/Bundle;)V

    .line 1482
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestSamsungAccountRegisterCallback(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "clientID"    # Ljava/lang/String;
    .param p3, "clientSecret"    # Ljava/lang/String;
    .param p4, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1490
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1491
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1492
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1491
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1503
    :goto_0
    return-void

    .line 1496
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1497
    .local v0, "data":Landroid/os/Bundle;
    const-string/jumbo v1, "clientID"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1498
    const-string/jumbo v1, "clienSecret"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1499
    const-string/jumbo v1, "packageName"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1500
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;

    .line 1501
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1502
    const/16 v4, 0xad0

    invoke-direct {v2, p1, v3, v4, v0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;ILandroid/os/Bundle;)V

    .line 1500
    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestSamsungAccountSCloudAccessToken(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 1575
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1576
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1577
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1576
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1584
    :goto_0
    return-void

    .line 1581
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;

    .line 1582
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1583
    const/16 v3, 0xad6

    invoke-direct {v1, p1, v2, v3, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;ILandroid/os/Bundle;)V

    .line 1581
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestSamsungAccountUnregisterCallback(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1507
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1508
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1509
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1508
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1516
    :goto_0
    return-void

    .line 1513
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;

    .line 1514
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1515
    const/16 v3, 0xad1

    const/4 v4, 0x0

    invoke-direct {v1, p1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;ILandroid/os/Bundle;)V

    .line 1513
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestSamsungAccountUserName(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 1588
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1589
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1590
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1589
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1597
    :goto_0
    return-void

    .line 1594
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;

    .line 1595
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1596
    const/16 v3, 0xad7

    invoke-direct {v1, p1, v2, v3, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;ILandroid/os/Bundle;)V

    .line 1594
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestScheduleList(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fromDate"    # Ljava/util/Date;
    .param p3, "toDate"    # Ljava/util/Date;

    .prologue
    .line 455
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 456
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 457
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 456
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    :goto_0
    return-void

    .line 461
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;

    .line 462
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/command/RequestScheduleList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/util/Date;Ljava/util/Date;)V

    .line 461
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestSearchAllMusic(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchKeyword"    # Ljava/lang/String;

    .prologue
    .line 624
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 625
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 626
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 625
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    :goto_0
    return-void

    .line 630
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchAllMusic;

    .line 631
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchAllMusic;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/lang/String;)V

    .line 630
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestSearchMusic(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchKeyword"    # Ljava/lang/String;

    .prologue
    .line 599
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 600
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 601
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 600
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    :goto_0
    return-void

    .line 605
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchMusic;

    .line 606
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchMusic;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/lang/String;)V

    .line 605
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestSearchedContactList(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "keyword"    # Ljava/lang/String;

    .prologue
    .line 815
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 816
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 817
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 816
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 824
    :goto_0
    return-void

    .line 821
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;

    .line 822
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    const/4 v3, -0x1

    invoke-direct {v1, p1, v2, p2, v3}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/lang/String;I)V

    .line 821
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestSearchedContactList(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "itemCount"    # I

    .prologue
    .line 802
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 803
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 804
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 803
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    :goto_0
    return-void

    .line 808
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;

    .line 809
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchedContactList;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/lang/String;I)V

    .line 808
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestSendMessage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .prologue
    .line 444
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 445
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 446
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 445
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    :goto_0
    return-void

    .line 450
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->requestSendMessage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)V

    goto :goto_0
.end method

.method public requestSetMyLocation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "myLocation"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    const/4 v3, 0x0

    .line 1821
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1822
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1823
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1822
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1827
    :cond_0
    return v3
.end method

.method public requestShareMyLocation(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "friendPhoneNumber"    # Ljava/lang/String;
    .param p3, "locationShared"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 1695
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1696
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1697
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1696
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1705
    :goto_0
    return-void

    .line 1701
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/command/RequestShareMyLocation;

    .line 1702
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1701
    invoke-direct {v0, p1, v1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/command/RequestShareMyLocation;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 1704
    .local v0, "request":Lcom/sec/android/automotive/drivelink/framework/command/RequestShareMyLocation;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestStartDrivingStatusMonitoring(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 907
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 908
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 909
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 908
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 915
    :goto_0
    return-void

    .line 913
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestStartDrivingStatusMonitoring;

    .line 914
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestStartDrivingStatusMonitoring;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 913
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestStartMirrorLink(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1427
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1428
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1429
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1428
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1435
    :goto_0
    return-void

    .line 1433
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestStartMirrorLink;

    .line 1434
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestStartMirrorLink;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 1433
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestStopDrivingStatusMonitoring(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 919
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 920
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 921
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 920
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 927
    :goto_0
    return-void

    .line 925
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestStopDrivingStatusMonitoring;

    .line 926
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestStopDrivingStatusMonitoring;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 925
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestStopMirrorLink(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1439
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1440
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1441
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1440
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1447
    :goto_0
    return-void

    .line 1445
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestStopMirrorLink;

    .line 1446
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestStopMirrorLink;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 1445
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestUnreadMessageCount(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 395
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 396
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 397
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 396
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    :goto_0
    return-void

    .line 401
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCount;

    .line 402
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCount;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 401
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestUnreadMessageCountByInbox(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "inbox"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .prologue
    .line 419
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 420
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 421
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 420
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    :goto_0
    return-void

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCountByInbox;

    .line 426
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, p2, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestUnreadMessageCountByInbox;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 425
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestUnreadMessageCountBySync(Landroid/content/Context;)I
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 407
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 408
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 409
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 408
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    const/4 v0, 0x0

    .line 413
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 414
    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getUnreadMessageCountBySync(Landroid/content/Context;)I

    move-result v0

    goto :goto_0
.end method

.method public requestUpdateApplication(Landroid/content/Context;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isCheckVersion"    # Z

    .prologue
    .line 1097
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1098
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1099
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1098
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1106
    :goto_0
    return-void

    .line 1103
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateApplication;

    .line 1104
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-direct {v1, p1, p2, v2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateApplication;-><init>(Landroid/content/Context;ZLcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 1103
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public requestUpdateParticipantStatus(Landroid/content/Context;ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "groupId"    # I
    .param p3, "status"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;
    .param p4, "myLocation"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p5, "duration"    # J

    .prologue
    .line 2021
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2022
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2023
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2022
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2030
    :goto_0
    return-void

    .line 2027
    :cond_0
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;

    .line 2028
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    .line 2029
    invoke-direct/range {v0 .. v7}, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)V

    .line 2027
    invoke-virtual {v8, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V

    goto :goto_0
.end method

.method public saveLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 3
    .param p1, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 1769
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1770
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1771
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1770
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1772
    const/4 v0, 0x0

    .line 1775
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->saveLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v0

    goto :goto_0
.end method

.method public saveMyPlace(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;)V
    .locals 3
    .param p1, "place"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;

    .prologue
    .line 864
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 865
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 866
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 865
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    :goto_0
    return-void

    .line 870
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->saveMyPlace(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;)Z

    goto :goto_0
.end method

.method public setCarSpeedThreshold(Landroid/content/Context;III)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "minSpeed"    # I
    .param p3, "maxSpeed"    # I
    .param p4, "speedCheckDurationInSec"    # I

    .prologue
    .line 954
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 955
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 956
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 955
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 962
    :goto_0
    return-void

    .line 960
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setCarSpeedThreshold(Landroid/content/Context;III)V

    goto :goto_0
.end method

.method public setInitializedFlag(Z)V
    .locals 0
    .param p1, "bInitialized"    # Z

    .prologue
    .line 196
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mbInitialized:Z

    .line 197
    return-void
.end method

.method public setOnDriveLinkCallLogListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 240
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 241
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 240
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    :goto_0
    return-void

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkCallLogListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;)V

    goto :goto_0
.end method

.method public setOnDriveLinkConnectivityListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;

    .prologue
    .line 1134
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1135
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1136
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1135
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1141
    :goto_0
    return-void

    .line 1140
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkConnectivityListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;)V

    goto :goto_0
.end method

.method public setOnDriveLinkContactListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 228
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 229
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 228
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    :goto_0
    return-void

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkContactListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;)V

    goto :goto_0
.end method

.method public setOnDriveLinkDLCallMessageLogListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDLCallMessageLogListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDLCallMessageLogListener;

    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 323
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 324
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 323
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    :goto_0
    return-void

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkDLCallMessageListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDLCallMessageLogListener;)V

    goto :goto_0
.end method

.method public setOnDriveLinkDrivingStatusListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;

    .prologue
    .line 896
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 897
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 898
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 897
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 903
    :goto_0
    return-void

    .line 902
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkDrivingStatusListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;)V

    goto :goto_0
.end method

.method public setOnDriveLinkEventListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 216
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 217
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 216
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    :goto_0
    return-void

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkEventListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;)V

    goto :goto_0
.end method

.method public setOnDriveLinkFavoriteContactListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;

    .prologue
    .line 1937
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1938
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1939
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1938
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1946
    :goto_0
    return-void

    .line 1943
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1944
    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkFavoriteContactListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;)V

    goto :goto_0
.end method

.method public setOnDriveLinkLocationListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 311
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 312
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 311
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    :goto_0
    return-void

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkLocationListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V

    goto :goto_0
.end method

.method public setOnDriveLinkMessageListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;

    .prologue
    .line 251
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 252
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 253
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 252
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    :goto_0
    return-void

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkMessageListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;)V

    goto :goto_0
.end method

.method public setOnDriveLinkMusicListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 276
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 275
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    :goto_0
    return-void

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkMusicListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;)V

    goto :goto_0
.end method

.method public setOnDriveLinkMusicPlayerListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    .prologue
    .line 298
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 299
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 300
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 299
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    :goto_0
    return-void

    .line 304
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkMusicPlayerListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;)V

    goto :goto_0
.end method

.method public setOnDriveLinkMusicRemotePlayerListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    .prologue
    .line 2059
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2060
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2061
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2060
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2067
    :goto_0
    return-void

    .line 2065
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 2066
    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkMusicRemotePlayerListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;)V

    goto :goto_0
.end method

.method public setOnDriveLinkNotificationListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    .prologue
    .line 286
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 287
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 288
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 287
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    :goto_0
    return-void

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkNotificationListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;)V

    goto :goto_0
.end method

.method public setOnDriveLinkRecommendedContactForCallListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;

    .prologue
    .line 1624
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1625
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1626
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1625
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1632
    :goto_0
    return-void

    .line 1630
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1631
    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkRecommendedContactForCallListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;)V

    goto :goto_0
.end method

.method public setOnDriveLinkRecommendedContactForMessageListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;

    .prologue
    .line 1637
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1638
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1639
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1638
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1645
    :goto_0
    return-void

    .line 1643
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 1644
    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkRecommendedContactForMessageListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;)V

    goto :goto_0
.end method

.method public setOnDriveLinkSamsungAccountListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    .prologue
    .line 334
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 335
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 336
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 335
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    :goto_0
    return-void

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 341
    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkSamsungAccountListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;)V

    goto :goto_0
.end method

.method public setOnDriveLinkScheduleListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkScheduleListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkScheduleListener;

    .prologue
    .line 263
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 264
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 265
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 264
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    :goto_0
    return-void

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkScheduleListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkScheduleListener;)V

    goto :goto_0
.end method

.method public setOnDriveLinkUpdateListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

    .prologue
    .line 346
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 347
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 348
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 347
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    :goto_0
    return-void

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkUpdateListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;)V

    goto :goto_0
.end method

.method public startThreadPool()V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->startWorkers()V

    .line 206
    return-void
.end method

.method public stopThreadPool()V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->stopWorkers()V

    .line 210
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->waitWorkersStopped()V

    .line 211
    return-void
.end method

.method public declared-synchronized terminate(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;

    .prologue
    .line 168
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "drivelink framework terminate start"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "drivelink framework terminate fail"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    :goto_0
    monitor-exit p0

    return-void

    .line 175
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->setInitializedFlag(Z)V

    .line 177
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->stopThreadPool()V

    .line 179
    if-nez p2, :cond_1

    .line 180
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->terminate(Landroid/content/Context;)V

    .line 186
    :goto_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkDBHelper;->delInstance()V

    .line 188
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "drivelink framework terminate end"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 168
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 182
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mThreadPool:Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceFinalize;

    .line 183
    invoke-direct {v1, p1, p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceFinalize;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;)V

    .line 182
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public unregisterMusicRemoteControl(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 2081
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2082
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2083
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2082
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2088
    :goto_0
    return-void

    .line 2087
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->unregisterMusicRemoteControl(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public unregisterNotification(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 841
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 842
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 843
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 842
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 848
    :goto_0
    return-void

    .line 847
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->unregisterNotification(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public updatePlayedMusicMetaData(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 660
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 661
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "@@@ drivelink framework is not initialized!!!"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 662
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 661
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 663
    const/4 v0, 0x0

    .line 666
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->updatePlayedMusicMetaData(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Z

    move-result v0

    goto :goto_0
.end method
