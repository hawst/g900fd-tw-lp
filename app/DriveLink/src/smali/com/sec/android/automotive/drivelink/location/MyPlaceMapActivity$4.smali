.class Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$4;
.super Ljava/lang/Object;
.source "MyPlaceMapActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;

.field private final synthetic val$placesAdapter:Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$4;->val$placesAdapter:Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x0

    .line 76
    const/16 v1, 0x42

    if-ne p2, v1, :cond_0

    .line 77
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$4;->val$placesAdapter:Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->txtSearch:Landroid/widget/AutoCompleteTextView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;)Landroid/widget/AutoCompleteTextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->applySearch(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 79
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->txtSearch:Landroid/widget/AutoCompleteTextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;)Landroid/widget/AutoCompleteTextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 81
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    return v3
.end method
