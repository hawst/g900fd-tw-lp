.class Lcom/sec/android/automotive/drivelink/home/HomeActivity$1;
.super Ljava/lang/Object;
.source "HomeActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/home/HomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    .line 562
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    .line 566
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    const/4 v4, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->setBtnClickable(Z)V
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$0(Lcom/sec/android/automotive/drivelink/home/HomeActivity;Z)V

    .line 567
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    const v4, 0x7f090081

    if-ne v3, v4, :cond_1

    .line 568
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 570
    :try_start_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    .line 571
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setCheckListSettingMusicList()Z

    move-result v2

    .line 572
    .local v2, "value":Z
    if-nez v2, :cond_1

    .line 573
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$1()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "MusicList is null 1"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    const/4 v4, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->setBtnClickable(Z)V
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$0(Lcom/sec/android/automotive/drivelink/home/HomeActivity;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 696
    .end local v2    # "value":Z
    :cond_0
    :goto_0
    return-void

    .line 577
    :catch_0
    move-exception v0

    .line 578
    .local v0, "e":Ljava/lang/Exception;
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$1()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "MUSIC exception occured : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->setBtnClickable(Z)V
    invoke-static {v3, v6}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$0(Lcom/sec/android/automotive/drivelink/home/HomeActivity;Z)V

    goto :goto_0

    .line 601
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 690
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->setBtnClickable(Z)V
    invoke-static {v3, v6}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$0(Lcom/sec/android/automotive/drivelink/home/HomeActivity;Z)V

    goto :goto_0

    .line 604
    :sswitch_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v3

    .line 605
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 604
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 606
    const-string/jumbo v3, "CM05"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 607
    const-string/jumbo v3, "VAC_CLIENT_DM"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 608
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 610
    .local v1, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v3, "DM_DIAL_CONTACT"

    .line 609
    invoke-static {v3, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0

    .line 615
    .end local v1    # "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :sswitch_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v3

    .line 616
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 615
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 617
    const-string/jumbo v3, "CM08"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 619
    const-string/jumbo v3, "VAC_CLIENT_DM"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 620
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 622
    .restart local v1    # "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v3, "DM_SMS_CONTACT"

    .line 621
    invoke-static {v3, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0

    .line 627
    .end local v1    # "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :sswitch_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v3

    .line 628
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 627
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 629
    const-string/jumbo v3, "CM12"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 631
    const-string/jumbo v3, "VAC_CLIENT_DM"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 632
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 633
    .restart local v1    # "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v3, "DM_LOCATION"

    invoke-static {v3, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto/16 :goto_0

    .line 641
    .end local v1    # "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :sswitch_3
    :try_start_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 642
    const-string/jumbo v3, "VAC_CLIENT_DM"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 643
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 644
    .restart local v1    # "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 645
    const/4 v3, 0x0

    iput-object v3, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 652
    :goto_1
    const-string/jumbo v3, "DM_MUSIC_PLAYER"

    .line 651
    invoke-static {v3, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 687
    .end local v1    # "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_2
    :goto_2
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->setBtnClickable(Z)V
    invoke-static {v3, v6}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$0(Lcom/sec/android/automotive/drivelink/home/HomeActivity;Z)V

    goto/16 :goto_0

    .line 647
    .restart local v1    # "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_3
    :try_start_2
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$2(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 648
    const v5, 0x7f0a004f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 647
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$3(Lcom/sec/android/automotive/drivelink/home/HomeActivity;Ljava/lang/String;)V

    .line 649
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->promptForMusic:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$4(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 657
    .end local v1    # "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :catch_1
    move-exception v0

    .line 658
    .restart local v0    # "e":Ljava/lang/Exception;
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$1()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "MUSIC is null : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->setBtnClickable(Z)V
    invoke-static {v3, v6}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$0(Lcom/sec/android/automotive/drivelink/home/HomeActivity;Z)V

    goto :goto_2

    .line 655
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    :try_start_3
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->finish()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 601
    :sswitch_data_0
    .sparse-switch
        0x7f090078 -> :sswitch_0
        0x7f09007b -> :sswitch_1
        0x7f09007e -> :sswitch_2
        0x7f090081 -> :sswitch_3
    .end sparse-switch
.end method
