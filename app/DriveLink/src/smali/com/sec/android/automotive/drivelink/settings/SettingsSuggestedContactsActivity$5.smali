.class Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$5;
.super Ljava/lang/Object;
.source "SettingsSuggestedContactsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 132
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->preference:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    .line 133
    const-string/jumbo v1, "PREF_SETTINGS_SUGGEST_CONTACTS"

    .line 132
    invoke-virtual {v0, v1, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->rbAutoSuggest:Landroid/widget/RadioButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->rbSetFavorite:Landroid/widget/RadioButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutAddContact:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutFavorites:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->tvAddContact:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->access$4(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->ivAddContact:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->access$5(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->setItemOnClickEnable(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->access$6(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;Z)V

    .line 142
    return-void
.end method
