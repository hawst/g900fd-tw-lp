.class public final enum Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;
.super Ljava/lang/Enum;
.source "DLUpdateInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UPDATE_ERROR_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DOWNLOAD_CANCEL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

.field public static final enum DOWNLOAD_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

.field public static final enum INSTALL_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

.field public static final enum NO_DOWNLOAD_INFO:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

.field public static final enum NO_DOWNLOAD_VERSION:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

.field public static final enum UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

.field public static final enum WRONG_URL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 37
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    const-string/jumbo v1, "WRONG_URL"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->WRONG_URL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    const-string/jumbo v1, "NO_DOWNLOAD_VERSION"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->NO_DOWNLOAD_VERSION:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    const-string/jumbo v1, "NO_DOWNLOAD_INFO"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->NO_DOWNLOAD_INFO:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    const-string/jumbo v1, "DOWNLOAD_FAIL"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->DOWNLOAD_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    const-string/jumbo v1, "DOWNLOAD_CANCEL"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->DOWNLOAD_CANCEL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    const-string/jumbo v1, "INSTALL_FAIL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->INSTALL_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    const-string/jumbo v1, "UNKNOWN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .line 36
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->WRONG_URL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->NO_DOWNLOAD_VERSION:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->NO_DOWNLOAD_INFO:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->DOWNLOAD_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->DOWNLOAD_CANCEL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->INSTALL_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getEnum(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->WRONG_URL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v0

    if-ne v0, p0, :cond_0

    .line 41
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->WRONG_URL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .line 53
    :goto_0
    return-object v0

    .line 42
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->NO_DOWNLOAD_VERSION:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v0

    if-ne v0, p0, :cond_1

    .line 43
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->NO_DOWNLOAD_VERSION:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    goto :goto_0

    .line 44
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->NO_DOWNLOAD_INFO:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v0

    if-ne v0, p0, :cond_2

    .line 45
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->NO_DOWNLOAD_INFO:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    goto :goto_0

    .line 46
    :cond_2
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->INSTALL_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v0

    if-ne v0, p0, :cond_3

    .line 47
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->INSTALL_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    goto :goto_0

    .line 48
    :cond_3
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->DOWNLOAD_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v0

    if-ne v0, p0, :cond_4

    .line 49
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->DOWNLOAD_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    goto :goto_0

    .line 50
    :cond_4
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->DOWNLOAD_CANCEL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v0

    if-ne v0, p0, :cond_5

    .line 51
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->DOWNLOAD_CANCEL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    goto :goto_0

    .line 53
    :cond_5
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
