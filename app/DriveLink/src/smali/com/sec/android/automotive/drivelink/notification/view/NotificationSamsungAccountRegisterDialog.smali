.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog;
.super Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialogFragment;
.source "NotificationSamsungAccountRegisterDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog$ClickListener;
    }
.end annotation


# static fields
.field public static TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

.field private mOnClickListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog$ClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog;

    .line 17
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 16
    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog;->TAG:Ljava/lang/String;

    .line 17
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog$ClickListener;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog;->mOnClickListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog$ClickListener;

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 60
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 66
    :try_start_0
    check-cast p1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog$ClickListener;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog;->mOnClickListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog$ClickListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :goto_0
    return-void

    .line 67
    :catch_0
    move-exception v0

    .line 69
    .local v0, "e":Ljava/lang/ClassCastException;
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Activity don\u00b4t implement listener."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 29
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 30
    const v2, 0x7f0a034a

    .line 31
    const v3, 0x7f0a0349

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;-><init>(Landroid/content/Context;II)V

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    .line 33
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    const v1, 0x7f0a0347

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->setTitle(I)V

    .line 34
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    const v1, 0x7f0a0348

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->setMessage(I)V

    .line 36
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->setOnClickListener(Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog$BasicDialogListener;)V

    .line 53
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public setOnClickListener(Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog$ClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog$ClickListener;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog;->mOnClickListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog$ClickListener;

    .line 75
    return-void
.end method
