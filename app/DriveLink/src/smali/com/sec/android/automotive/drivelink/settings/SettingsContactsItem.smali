.class public abstract Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;
.super Ljava/lang/Object;
.source "SettingsContactsItem.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildItemList(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    const/4 v6, 0x0

    .line 27
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 28
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;>;"
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 30
    .local v2, "headers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 43
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 47
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 49
    return-object v3

    .line 30
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 33
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 34
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 35
    new-instance v5, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;

    invoke-direct {v5, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;-><init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isLetter(C)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 37
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    .line 38
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 37
    invoke-static {v5}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 43
    .end local v0    # "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 44
    .local v1, "header":Ljava/lang/String;
    new-instance v5, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsHeader;

    invoke-direct {v5, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsHeader;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public compareTo(Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;)I
    .locals 8
    .param p1, "other"    # Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;

    .prologue
    const/4 v7, 0x0

    .line 54
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 55
    const/4 v2, 0x1

    .line 104
    :goto_0
    return v2

    .line 58
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 59
    const/4 v2, -0x1

    goto :goto_0

    .line 64
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getType()Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;

    move-result-object v5

    sget-object v6, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;->ITEM:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;

    if-ne v5, v6, :cond_4

    .line 65
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getType()Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;

    move-result-object v5

    sget-object v6, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;->ITEM:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;

    if-ne v5, v6, :cond_2

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 68
    .local v3, "sCompare1":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    .line 69
    .local v4, "sCompare2":Ljava/lang/String;
    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .line 70
    .local v2, "comparison":I
    goto :goto_0

    .line 72
    .end local v2    # "comparison":I
    .end local v3    # "sCompare1":Ljava/lang/String;
    .end local v4    # "sCompare2":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 73
    .local v1, "charHeader":C
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 75
    .local v0, "charContact":C
    if-ne v0, v1, :cond_3

    .line 76
    const/4 v2, 0x1

    .line 77
    .restart local v2    # "comparison":I
    goto :goto_0

    .line 78
    .end local v2    # "comparison":I
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 79
    .restart local v3    # "sCompare1":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    .line 80
    .restart local v4    # "sCompare2":Ljava/lang/String;
    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .line 83
    .restart local v2    # "comparison":I
    goto :goto_0

    .line 84
    .end local v0    # "charContact":C
    .end local v1    # "charHeader":C
    .end local v2    # "comparison":I
    .end local v3    # "sCompare1":Ljava/lang/String;
    .end local v4    # "sCompare2":Ljava/lang/String;
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getType()Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;

    move-result-object v5

    sget-object v6, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;->HEADER:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;

    if-ne v5, v6, :cond_5

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 87
    .restart local v3    # "sCompare1":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    .line 88
    .restart local v4    # "sCompare2":Ljava/lang/String;
    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .line 89
    .restart local v2    # "comparison":I
    goto :goto_0

    .line 91
    .end local v2    # "comparison":I
    .end local v3    # "sCompare1":Ljava/lang/String;
    .end local v4    # "sCompare2":Ljava/lang/String;
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 92
    .restart local v1    # "charHeader":C
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 94
    .restart local v0    # "charContact":C
    if-ne v1, v0, :cond_6

    .line 95
    const/4 v2, -0x1

    .line 96
    .restart local v2    # "comparison":I
    goto/16 :goto_0

    .line 97
    .end local v2    # "comparison":I
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 98
    .restart local v3    # "sCompare1":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    .line 99
    .restart local v4    # "sCompare2":Ljava/lang/String;
    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .restart local v2    # "comparison":I
    goto/16 :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;

    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;->compareTo(Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;)I

    move-result v0

    return v0
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getType()Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem$Type;
.end method
