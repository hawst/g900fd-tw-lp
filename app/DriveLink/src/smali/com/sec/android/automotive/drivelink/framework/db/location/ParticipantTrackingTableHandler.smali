.class public Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;
.super Ljava/lang/Object;
.source "ParticipantTrackingTableHandler.java"


# static fields
.field private static final TABLE_LOCATION_TRACKING:Ljava/lang/String; = "tb_participant_tracking"

.field private static final TAG:Ljava/lang/String; = "ParticipantTrackingTableHandler"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method private getParticipantTrackingFromCursor(Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 392
    const/4 v0, 0x0

    .line 394
    .local v0, "i":I
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;-><init>()V

    .line 395
    .local v2, "track":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;
    add-int/lit8 v0, v0, 0x1

    .line 396
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLatitude(D)V

    .line 397
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLongitude(D)V

    .line 398
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .restart local v1    # "i":I
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setTimestamp(J)V

    .line 400
    return-object v2
.end method

.method private insertParticipantTracking(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z
    .locals 11
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "participant"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    .line 127
    const-wide/16 v2, -0x1

    .line 129
    .local v2, "result":J
    :try_start_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 130
    .local v4, "time":Ljava/util/Calendar;
    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    .line 132
    .local v5, "timestamp":J
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v1

    .line 134
    .local v1, "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 135
    .local v7, "values":Landroid/content/ContentValues;
    const-string/jumbo v8, "phone_number"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const-string/jumbo v8, "latitude"

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 137
    const-string/jumbo v8, "longitude"

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 138
    const-string/jumbo v8, "timestamp"

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 140
    const-string/jumbo v8, "tb_participant_tracking"

    const/4 v9, 0x0

    invoke-virtual {p1, v8, v9, v7}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v2

    .line 147
    .end local v1    # "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .end local v4    # "time":Ljava/util/Calendar;
    .end local v5    # "timestamp":J
    .end local v7    # "values":Landroid/content/ContentValues;
    :goto_0
    const-wide/16 v8, -0x1

    cmp-long v8, v2, v8

    if-nez v8, :cond_0

    const/4 v8, 0x0

    :goto_1
    return v8

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string/jumbo v8, "ParticipantTrackingTableHandler"

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 143
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 144
    .local v0, "e":Landroid/database/SQLException;
    const-string/jumbo v8, "ParticipantTrackingTableHandler"

    invoke-virtual {v0}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 147
    .end local v0    # "e":Landroid/database/SQLException;
    :cond_0
    const/4 v8, 0x1

    goto :goto_1
.end method

.method private isParticipantTrackingInTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 106
    const/4 v2, 0x0

    .line 108
    .local v2, "result":Z
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "select * from tb_participant_tracking where phone_number = \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 109
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 108
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 111
    .local v3, "sql":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 112
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 113
    const/4 v2, 0x1

    .line 115
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    .line 122
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v3    # "sql":Ljava/lang/String;
    :goto_0
    return v2

    .line 116
    :catch_0
    move-exception v1

    .line 117
    .local v1, "e":Ljava/lang/NullPointerException;
    const-string/jumbo v4, "ParticipantTrackingTableHandler"

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 118
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .line 119
    .local v1, "e":Landroid/database/SQLException;
    const-string/jumbo v4, "ParticipantTrackingTableHandler"

    invoke-virtual {v1}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private saveTracking(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "participant"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    const/4 v0, 0x0

    .line 87
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 88
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 89
    :cond_0
    const-string/jumbo v1, "ParticipantTrackingTableHandler"

    const-string/jumbo v2, "participant or phone number is null"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    :goto_0
    return v0

    .line 93
    :cond_1
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v1

    if-nez v1, :cond_2

    .line 94
    const-string/jumbo v1, "ParticipantTrackingTableHandler"

    const-string/jumbo v2, "location is null"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 98
    :cond_2
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;->isParticipantTrackingInTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 99
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;->updateParticipantTracking(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z

    move-result v0

    goto :goto_0

    .line 101
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;->insertParticipantTracking(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z

    move-result v0

    goto :goto_0
.end method

.method private updateParticipantTracking(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z
    .locals 12
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "participant"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 152
    const-wide/16 v2, 0x0

    .line 154
    .local v2, "result":J
    :try_start_0
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v1

    .line 156
    .local v1, "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 157
    .local v4, "values":Landroid/content/ContentValues;
    const-string/jumbo v9, "latitude"

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 158
    const-string/jumbo v9, "longitude"

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 160
    const-string/jumbo v6, "phone_number = ?"

    .line 161
    .local v6, "whereClause":Ljava/lang/String;
    const/4 v9, 0x1

    new-array v5, v9, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v5, v9

    .line 163
    .local v5, "whereArgs":[Ljava/lang/String;
    const-string/jumbo v9, "tb_participant_tracking"

    invoke-virtual {p1, v9, v4, v6, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v9

    int-to-long v2, v9

    .line 171
    .end local v1    # "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .end local v4    # "values":Landroid/content/ContentValues;
    .end local v5    # "whereArgs":[Ljava/lang/String;
    .end local v6    # "whereClause":Ljava/lang/String;
    :goto_0
    const-wide/16 v9, 0x0

    cmp-long v9, v2, v9

    if-nez v9, :cond_0

    :goto_1
    return v7

    .line 165
    :catch_0
    move-exception v0

    .line 166
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string/jumbo v9, "ParticipantTrackingTableHandler"

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 167
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 168
    .local v0, "e":Landroid/database/SQLException;
    const-string/jumbo v9, "ParticipantTrackingTableHandler"

    invoke-virtual {v0}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .end local v0    # "e":Landroid/database/SQLException;
    :cond_0
    move v7, v8

    .line 171
    goto :goto_1
.end method


# virtual methods
.method public createParticipantTrackingTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 28
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;->getNewPlaceOnParticipantsTableHandler()Ljava/lang/String;

    move-result-object v1

    .line 30
    .local v1, "participantsTableName":Ljava/lang/String;
    :try_start_0
    const-string/jumbo v2, ""

    .line 31
    .local v2, "sql":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "CREATE TABLE IF NOT EXISTS tb_participant_tracking (phone_number VARCHAR, latitude REAL, longitude REAL, timestamp INTEGER, FOREIGN KEY (phone_number) REFERENCES "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 35
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "(phone_number));"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 31
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 36
    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    .end local v2    # "sql":Ljava/lang/String;
    :goto_0
    return-void

    .line 37
    :catch_0
    move-exception v0

    .line 38
    .local v0, "e":Landroid/database/SQLException;
    const-string/jumbo v3, "ParticipantTrackingTableHandler"

    invoke-virtual {v0}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public deleteAllTracking(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 405
    :try_start_0
    const-string/jumbo v1, "tb_participant_tracking"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 407
    const/4 v1, 0x1

    .line 414
    :goto_0
    return v1

    .line 408
    :catch_0
    move-exception v0

    .line 409
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string/jumbo v1, "ParticipantTrackingTableHandler"

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 410
    :catch_1
    move-exception v0

    .line 411
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string/jumbo v1, "ParticipantTrackingTableHandler"

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public dropParticipantTrackingTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 45
    :try_start_0
    const-string/jumbo v1, "DROP TABLE IF EXISTS tb_participant_tracking;"

    .line 46
    .local v1, "sql":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    .end local v1    # "sql":Ljava/lang/String;
    :goto_0
    return-void

    .line 47
    :catch_0
    move-exception v0

    .line 48
    .local v0, "e":Landroid/database/SQLException;
    const-string/jumbo v2, "ParticipantTrackingTableHandler"

    invoke-virtual {v0}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getAllParticipantLastTrack(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Ljava/util/HashMap;
    .locals 10
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 329
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v7

    if-nez v7, :cond_1

    .line 330
    :cond_0
    const-string/jumbo v7, "ParticipantTrackingTableHandler"

    const-string/jumbo v8, "group or participants is null"

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v6

    .line 355
    :goto_0
    return-object v0

    .line 334
    :cond_1
    const/4 v0, 0x0

    .line 336
    .local v0, "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    :try_start_0
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v4

    .line 337
    .local v4, "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    .line 340
    .end local v0    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    .local v1, "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    :try_start_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_3

    move-object v0, v1

    .line 349
    .end local v1    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    .restart local v0    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    goto :goto_0

    .line 340
    .end local v0    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    .restart local v1    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 341
    .local v3, "participant":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-virtual {p0, p1, v3}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;->getParticipantLastTrack(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v5

    .line 343
    .local v5, "track":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    if-nez v5, :cond_2

    .line 344
    const-string/jumbo v7, "ParticipantTrackingTableHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "phoneNumber: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 345
    const-string/jumbo v9, " no tracking"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 344
    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_2

    move-object v0, v6

    .line 346
    goto :goto_0

    .line 349
    .end local v1    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    .end local v3    # "participant":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .end local v4    # "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    .end local v5    # "track":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .restart local v0    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    :catch_0
    move-exception v2

    .line 350
    .local v2, "e":Ljava/lang/NullPointerException;
    :goto_1
    const-string/jumbo v6, "ParticipantTrackingTableHandler"

    invoke-virtual {v2}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 351
    .end local v2    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v2

    .line 352
    .local v2, "e":Landroid/database/SQLException;
    :goto_2
    const-string/jumbo v6, "ParticipantTrackingTableHandler"

    invoke-virtual {v2}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 351
    .end local v0    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    .end local v2    # "e":Landroid/database/SQLException;
    .restart local v1    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    .restart local v4    # "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    :catch_2
    move-exception v2

    move-object v0, v1

    .end local v1    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    .restart local v0    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    goto :goto_2

    .line 349
    .end local v0    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    .restart local v1    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    :catch_3
    move-exception v2

    move-object v0, v1

    .end local v1    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    .restart local v0    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    goto :goto_1
.end method

.method public getAllParticipantTracking(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Ljava/util/HashMap;
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 248
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v6

    if-nez v6, :cond_1

    .line 249
    :cond_0
    const-string/jumbo v6, "ParticipantTrackingTableHandler"

    const-string/jumbo v7, "group or participants is null"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    const/4 v0, 0x0

    .line 277
    :goto_0
    return-object v0

    .line 253
    :cond_1
    const/4 v0, 0x0

    .line 255
    .local v0, "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;>;"
    :try_start_0
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v4

    .line 256
    .local v4, "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    .line 259
    .end local v0    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;>;"
    .local v1, "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;>;"
    :try_start_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_3

    move-object v0, v1

    .line 271
    .end local v1    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;>;"
    .restart local v0    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;>;"
    goto :goto_0

    .line 259
    .end local v0    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;>;"
    .restart local v1    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;>;"
    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 260
    .local v3, "participant":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-virtual {p0, p1, v3}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;->getParticipantTracking(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Ljava/util/ArrayList;

    move-result-object v5

    .line 262
    .local v5, "tracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    if-eqz v5, :cond_2

    .line 266
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 271
    .end local v3    # "participant":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .end local v5    # "tracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    :catch_0
    move-exception v2

    move-object v0, v1

    .line 272
    .end local v1    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;>;"
    .end local v4    # "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    .restart local v0    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;>;"
    .local v2, "e":Ljava/lang/NullPointerException;
    :goto_2
    const-string/jumbo v6, "ParticipantTrackingTableHandler"

    invoke-virtual {v2}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 273
    .end local v2    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v2

    .line 274
    .local v2, "e":Landroid/database/SQLException;
    :goto_3
    const-string/jumbo v6, "ParticipantTrackingTableHandler"

    invoke-virtual {v2}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 273
    .end local v0    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;>;"
    .end local v2    # "e":Landroid/database/SQLException;
    .restart local v1    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;>;"
    .restart local v4    # "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    :catch_2
    move-exception v2

    move-object v0, v1

    .end local v1    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;>;"
    .restart local v0    # "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;>;"
    goto :goto_3

    .line 271
    .end local v4    # "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    :catch_3
    move-exception v2

    goto :goto_2
.end method

.method public getParticipantLastTrack(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "participant"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    const/4 v3, 0x0

    .line 366
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 388
    :cond_0
    :goto_0
    return-object v3

    .line 369
    :cond_1
    const/4 v3, 0x0

    .line 370
    .local v3, "track":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    const/4 v0, 0x0

    .line 371
    .local v0, "cursor":Landroid/database/Cursor;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "SELECT * FROM tb_participant_tracking WHERE phone_number = \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 372
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 373
    const-string/jumbo v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " ORDER BY timestamp DESC;"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 371
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 376
    .local v2, "sql":Ljava/lang/String;
    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 377
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 378
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;->getParticipantTrackingFromCursor(Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 385
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 380
    :catch_0
    move-exception v1

    .line 381
    .local v1, "e":Ljava/lang/NullPointerException;
    :try_start_1
    const-string/jumbo v4, "ParticipantTrackingTableHandler"

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 385
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 382
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .line 383
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    const-string/jumbo v4, "ParticipantTrackingTableHandler"

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 385
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 384
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v4

    .line 385
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 386
    throw v4
.end method

.method public getParticipantTracking(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Ljava/util/ArrayList;
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "participant"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 287
    const-string/jumbo v6, "ParticipantTrackingTableHandler"

    const-string/jumbo v7, "[getParticipantTracking]"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 317
    :cond_0
    :goto_0
    return-object v4

    .line 292
    :cond_1
    const/4 v0, 0x0

    .line 293
    .local v0, "cursor":Landroid/database/Cursor;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "SELECT * FROM tb_participant_tracking WHERE phone_number = \'"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 294
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 295
    const-string/jumbo v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " ORDER BY timestamp;"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 293
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 296
    .local v2, "sql":Ljava/lang/String;
    const/4 v4, 0x0

    .line 299
    .local v4, "tracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p1, v2, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 301
    if-eqz v0, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 302
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 304
    .end local v4    # "tracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    .local v5, "tracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    :cond_2
    :try_start_1
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;->getParticipantTrackingFromCursor(Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v3

    .line 305
    .local v3, "track":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    if-eqz v3, :cond_3

    .line 306
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 307
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v6

    if-nez v6, :cond_2

    move-object v4, v5

    .line 314
    .end local v3    # "track":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .end local v5    # "tracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    .restart local v4    # "tracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 309
    :catch_0
    move-exception v1

    .line 310
    .local v1, "e":Ljava/lang/NullPointerException;
    :goto_1
    :try_start_2
    const-string/jumbo v6, "ParticipantTrackingTableHandler"

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 314
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 311
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .line 312
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :goto_2
    :try_start_3
    const-string/jumbo v6, "ParticipantTrackingTableHandler"

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 314
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 313
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v6

    .line 314
    :goto_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 315
    throw v6

    .line 313
    .end local v4    # "tracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    .restart local v5    # "tracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "tracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    .restart local v4    # "tracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    goto :goto_3

    .line 311
    .end local v4    # "tracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    .restart local v5    # "tracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    :catch_2
    move-exception v1

    move-object v4, v5

    .end local v5    # "tracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    .restart local v4    # "tracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    goto :goto_2

    .line 309
    .end local v4    # "tracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    .restart local v5    # "tracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    :catch_3
    move-exception v1

    move-object v4, v5

    .end local v5    # "tracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    .restart local v4    # "tracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    goto :goto_1
.end method

.method public removeAllParticipantTracking(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 183
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v5

    if-nez v5, :cond_2

    .line 184
    :cond_0
    const-string/jumbo v5, "ParticipantTrackingTableHandler"

    const-string/jumbo v6, "group or participants is null"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    const/4 v4, 0x0

    .line 211
    :cond_1
    :goto_0
    return v4

    .line 188
    :cond_2
    const/4 v4, 0x1

    .line 191
    .local v4, "status":Z
    :try_start_0
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v2

    .line 193
    .local v2, "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 194
    .local v1, "participant":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-virtual {p0, p1, v1}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;->removeParticipantTracking(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    .line 196
    .local v3, "result":Z
    if-nez v3, :cond_3

    .line 199
    move v4, v3

    goto :goto_1

    .line 205
    .end local v1    # "participant":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .end local v2    # "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    .end local v3    # "result":Z
    :catch_0
    move-exception v0

    .line 206
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string/jumbo v5, "ParticipantTrackingTableHandler"

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 207
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 208
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string/jumbo v5, "ParticipantTrackingTableHandler"

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeParticipantTracking(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "participant"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 223
    const/4 v1, 0x0

    .line 225
    .local v1, "result":I
    :try_start_0
    const-string/jumbo v3, "phone_number = ?"

    .line 226
    .local v3, "whereClause":Ljava/lang/String;
    const/4 v6, 0x1

    new-array v2, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v6

    .line 228
    .local v2, "whereArgs":[Ljava/lang/String;
    const-string/jumbo v6, "tb_participant_tracking"

    invoke-virtual {p1, v6, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 235
    .end local v2    # "whereArgs":[Ljava/lang/String;
    .end local v3    # "whereClause":Ljava/lang/String;
    :goto_0
    if-nez v1, :cond_0

    :goto_1
    return v4

    .line 229
    :catch_0
    move-exception v0

    .line 230
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string/jumbo v6, "ParticipantTrackingTableHandler"

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 231
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 232
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string/jumbo v6, "ParticipantTrackingTableHandler"

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_0
    move v4, v5

    .line 235
    goto :goto_1
.end method

.method public saveParticipantTracking(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    const/4 v3, 0x0

    .line 61
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v4

    if-nez v4, :cond_1

    .line 62
    :cond_0
    const-string/jumbo v4, "ParticipantTrackingTableHandler"

    const-string/jumbo v5, "group or participants is null"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :goto_0
    return v3

    .line 68
    :cond_1
    :try_start_0
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v2

    .line 70
    .local v2, "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 76
    const/4 v3, 0x1

    goto :goto_0

    .line 70
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 71
    .local v1, "participant":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-direct {p0, p1, v1}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;->saveTracking(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 72
    const-string/jumbo v4, "ParticipantTrackingTableHandler"

    const-string/jumbo v5, "Fail to save track of group participant."

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 77
    .end local v1    # "participant":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .end local v2    # "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string/jumbo v4, "ParticipantTrackingTableHandler"

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 79
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 80
    .local v0, "e":Landroid/database/SQLException;
    const-string/jumbo v4, "ParticipantTrackingTableHandler"

    invoke-virtual {v0}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
