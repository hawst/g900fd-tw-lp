.class public Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;
.super Ljava/lang/Object;
.source "NotiGeneralMessageInfo.java"


# instance fields
.field private message:Ljava/lang/String;

.field private messageId:I

.field private timeout:I

.field private tts:Ljava/lang/String;

.field private ttsId:I


# direct methods
.method public constructor <init>(III)V
    .locals 2
    .param p1, "msgId"    # I
    .param p2, "ttsId"    # I
    .param p3, "timeoutSeconds"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->messageId:I

    .line 8
    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->ttsId:I

    .line 9
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->message:Ljava/lang/String;

    .line 10
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->tts:Ljava/lang/String;

    .line 21
    iput p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->messageId:I

    .line 22
    iput p2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->ttsId:I

    .line 23
    iput p3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->timeout:I

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "ttsString"    # Ljava/lang/String;
    .param p3, "timeoutSeconds"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->messageId:I

    .line 8
    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->ttsId:I

    .line 9
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->message:Ljava/lang/String;

    .line 10
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->tts:Ljava/lang/String;

    .line 15
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->message:Ljava/lang/String;

    .line 16
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->tts:Ljava/lang/String;

    .line 17
    iput p3, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->timeout:I

    .line 18
    return-void
.end method


# virtual methods
.method public getMessage(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->message:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->message:Ljava/lang/String;

    .line 35
    :goto_0
    return-object v0

    .line 31
    :cond_0
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->messageId:I

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 32
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->messageId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 35
    :cond_1
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public getMessageId()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->messageId:I

    return v0
.end method

.method public getTTS(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->tts:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->tts:Ljava/lang/String;

    .line 48
    :goto_0
    return-object v0

    .line 44
    :cond_0
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->ttsId:I

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->ttsId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 48
    :cond_1
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public getTimeout()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->timeout:I

    return v0
.end method
