.class public Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;
.super Landroid/app/Service;
.source "PushMessageService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$PushMessageHandler;,
        Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$ServiceBinder;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private listener:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/IPushMessageServiceListener;

.field private final mBinder:Landroid/os/IBinder;

.field private final mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 21
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$ServiceBinder;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$ServiceBinder;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->mBinder:Landroid/os/IBinder;

    .line 23
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$PushMessageHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$PushMessageHandler;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$PushMessageHandler;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->mHandler:Landroid/os/Handler;

    .line 17
    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;)Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/IPushMessageServiceListener;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->listener:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/IPushMessageServiceListener;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/IPushMessageServiceListener;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->listener:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/IPushMessageServiceListener;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 37
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 38
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onCreate"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 48
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onStartCommand"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    if-eqz p1, :cond_0

    .line 50
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 51
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 52
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 54
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    const/4 v1, 0x2

    return v1
.end method
