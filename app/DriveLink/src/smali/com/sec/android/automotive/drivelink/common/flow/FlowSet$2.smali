.class Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;
.super Ljava/util/HashMap;
.source "FlowSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 110
    const-string/jumbo v0, "DM_MAIN"

    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_MAIN:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    const-string/jumbo v0, "DM_DIAL_CONTACT"

    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_CONTACT:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    const-string/jumbo v0, "DM_DIAL_CONTACT_SEARCH_LIST"

    .line 115
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_CONTACT:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 114
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    const-string/jumbo v0, "DM_DIAL_TYPE"

    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_TYPE:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    const-string/jumbo v0, "DM_DIAL"

    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    const-string/jumbo v0, "DM_SMS_CONTACT"

    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_SMS_CONTACT:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    const-string/jumbo v0, "DM_SMS_CONTACT_SEARCH_LIST"

    .line 121
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_SMS_CONTACT:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 120
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    const-string/jumbo v0, "DM_SMS_TYPE"

    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_SMS_TYPE:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    const-string/jumbo v0, "DM_SMS_COMPOSE"

    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_SMS_MSG:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    const-string/jumbo v0, "DM_SMS_COMPOSING"

    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_SMS_MSG:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    const-string/jumbo v0, "DM_SMS_COMPLETE"

    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_SMS_MAIN:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    const-string/jumbo v0, "DM_SMS_SEND"

    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_MAIN:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    const-string/jumbo v0, "DM_SMS_INBOX"

    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_SMS_CONTACT:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    const-string/jumbo v0, "DM_SMS_INBOX_SEARCH_LIST"

    .line 129
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_SMS_CONTACT:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 128
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    const-string/jumbo v0, "DM_SMS_READBACK"

    .line 132
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_VP_CAR_SAFEREADER_READBACKMSG:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 131
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    const-string/jumbo v0, "DM_SMS_READBACK_NOTI"

    .line 134
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_VP_CAR_SAFEREADER_READBACKMSG:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 133
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    const-string/jumbo v0, "DM_LOCATION"

    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_HOME:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    const-string/jumbo v0, "DM_LOCATION_CONTACT_SHARE"

    .line 138
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_CONTACT:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 137
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    const-string/jumbo v0, "DM_LOCATION_CONTACT_SHARE_SEARCH_LIST"

    .line 140
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_CONTACT:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 139
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    const-string/jumbo v0, "DM_LOCATION_CONTACT_SHARE_TYPE"

    .line 142
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_CONTACT:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 141
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    const-string/jumbo v0, "DM_LOCATION_CONTACT_REQUEST"

    .line 145
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_CONTACT:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 144
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    const-string/jumbo v0, "DM_LOCATION_CONTACT_REQUEST_SEARCH_LIST"

    .line 147
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_CONTACT:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 146
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    const-string/jumbo v0, "DM_LOCATION_CONTACT_REQUEST_TYPE"

    .line 149
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_CONTACT:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 148
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    const-string/jumbo v0, "DM_LOCATION_SHARE_CONFIRM"

    .line 152
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_SHARE_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 151
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    const-string/jumbo v0, "DM_LOCATION_REQ_CONFIRM"

    .line 154
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_REQUEST_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 153
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    const-string/jumbo v0, "DM_LOCATION_ADDR_TYPE"

    .line 156
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_TYPE:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 155
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    const-string/jumbo v0, "DM_LOCATION_NAV_SEARCH_CONFIRM"

    .line 158
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_ROUTE_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 157
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    const-string/jumbo v0, "DM_MUSIC_PLAYER"

    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_MUSIC_HOME:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    const-string/jumbo v0, "DM_WHAT_CAN_I_SAY"

    .line 175
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_SHARE_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 174
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1
    return-void
.end method
