.class Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$2;
.super Ljava/lang/Object;
.source "MusicPlayerManager.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnSeekCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->initialize(Landroid/content/Context;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSeekComplete(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDriveLinkMusicPlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v0

    .line 117
    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;->onMusicPlaySeekComplete(Landroid/media/MediaPlayer;)V

    .line 118
    return-void
.end method
