.class public final Lcom/sec/android/automotive/drivelink/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final activity_account:I = 0x7f030000

.field public static final activity_base:I = 0x7f030001

.field public static final activity_basemap:I = 0x7f030002

.field public static final activity_disclaimer:I = 0x7f030003

.field public static final activity_findmycar:I = 0x7f030004

.field public static final activity_findmycar_map:I = 0x7f030005

.field public static final activity_group_location_destination_details:I = 0x7f030006

.field public static final activity_group_location_detail_map:I = 0x7f030007

.field public static final activity_group_location_duration:I = 0x7f030008

.field public static final activity_group_location_participants:I = 0x7f030009

.field public static final activity_group_location_setting_destination:I = 0x7f03000a

.field public static final activity_group_location_share:I = 0x7f03000b

.field public static final activity_group_location_share_details:I = 0x7f03000c

.field public static final activity_group_location_sharing_participants:I = 0x7f03000d

.field public static final activity_help:I = 0x7f03000e

.field public static final activity_home:I = 0x7f03000f

.field public static final activity_lbh_mapview:I = 0x7f030010

.field public static final activity_location:I = 0x7f030011

.field public static final activity_location_confirm:I = 0x7f030012

.field public static final activity_location_request:I = 0x7f030013

.field public static final activity_location_request_confirm:I = 0x7f030014

.field public static final activity_location_request_waiting:I = 0x7f030015

.field public static final activity_location_share:I = 0x7f030016

.field public static final activity_location_share_confirm:I = 0x7f030017

.field public static final activity_mapview:I = 0x7f030018

.field public static final activity_message:I = 0x7f030019

.field public static final activity_message_composer:I = 0x7f03001a

.field public static final activity_message_composer_textview:I = 0x7f03001b

.field public static final activity_message_reader:I = 0x7f03001c

.field public static final activity_multiwindow:I = 0x7f03001d

.field public static final activity_music_full:I = 0x7f03001e

.field public static final activity_music_search_list:I = 0x7f03001f

.field public static final activity_myplace:I = 0x7f030020

.field public static final activity_navigation:I = 0x7f030021

.field public static final activity_notification:I = 0x7f030022

.field public static final activity_phone:I = 0x7f030023

.field public static final activity_point_of_interest_map:I = 0x7f030024

.field public static final activity_prepare_dial_phone:I = 0x7f030025

.field public static final activity_prepare_dial_phone_mw:I = 0x7f030026

.field public static final activity_regulations:I = 0x7f030027

.field public static final activity_samsung_account:I = 0x7f030028

.field public static final activity_settings_add_favorite_contacts:I = 0x7f030029

.field public static final activity_settings_add_favorite_contacts_custom_list_header:I = 0x7f03002a

.field public static final activity_settings_add_favorite_contacts_custom_list_item:I = 0x7f03002b

.field public static final activity_settings_display_favorite_contacts_custom_list_item:I = 0x7f03002c

.field public static final activity_settings_language:I = 0x7f03002d

.field public static final activity_settings_main:I = 0x7f03002e

.field public static final activity_settings_register_car:I = 0x7f03002f

.field public static final activity_settings_register_car_options:I = 0x7f030030

.field public static final activity_settings_reject_message:I = 0x7f030031

.field public static final activity_settings_server_data_reset:I = 0x7f030032

.field public static final activity_settings_suggested_contacts:I = 0x7f030033

.field public static final activity_setupwizard_mycar:I = 0x7f030034

.field public static final activity_setupwizard_navigation:I = 0x7f030035

.field public static final activity_share_map:I = 0x7f030036

.field public static final activity_splash_screen:I = 0x7f030037

.field public static final activity_svoice_disalbed_exit_popup:I = 0x7f030038

.field public static final activity_svoice_launch_popup:I = 0x7f030039

.field public static final activity_touringguide:I = 0x7f03003a

.field public static final activity_tutorial_01:I = 0x7f03003b

.field public static final activity_update_default:I = 0x7f03003c

.field public static final activity_update_progress:I = 0x7f03003d

.field public static final activity_what_can_i_say:I = 0x7f03003e

.field public static final baidu_map:I = 0x7f03003f

.field public static final btn_quotation_marks:I = 0x7f030040

.field public static final common_action_bar:I = 0x7f030041

.field public static final dialog_basic:I = 0x7f030042

.field public static final drawer_menu_layout:I = 0x7f030043

.field public static final drawer_menu_list_item:I = 0x7f030044

.field public static final drawer_menu_list_title:I = 0x7f030045

.field public static final expandableview_child_row:I = 0x7f030046

.field public static final expandableview_parent_row:I = 0x7f030047

.field public static final guide_indicator:I = 0x7f030048

.field public static final help_registering_car:I = 0x7f030049

.field public static final home_bubble_popup:I = 0x7f03004a

.field public static final home_list_item_notitle:I = 0x7f03004b

.field public static final home_popup_menu:I = 0x7f03004c

.field public static final inbox_list_item_layout:I = 0x7f03004d

.field public static final inbox_list_items:I = 0x7f03004e

.field public static final info_cluster_point_window_item:I = 0x7f03004f

.field public static final item_poi_grid:I = 0x7f030050

.field public static final item_poi_list:I = 0x7f030051

.field public static final linearrouteview_item:I = 0x7f030052

.field public static final linearrouteview_mainbar:I = 0x7f030053

.field public static final linearrouteview_user_frame:I = 0x7f030054

.field public static final list_group_help:I = 0x7f030055

.field public static final list_group_what_can_i_say:I = 0x7f030056

.field public static final list_item_arrived_participant:I = 0x7f030057

.field public static final list_item_help:I = 0x7f030058

.field public static final list_item_joined_participant:I = 0x7f030059

.field public static final list_item_layout:I = 0x7f03005a

.field public static final list_item_what_can_i_say:I = 0x7f03005b

.field public static final list_item_what_can_i_say_first:I = 0x7f03005c

.field public static final location_expandable_view_frame:I = 0x7f03005d

.field public static final location_group_share_header_item:I = 0x7f03005e

.field public static final location_group_share_participant_item:I = 0x7f03005f

.field public static final location_group_share_participant_sharing_item:I = 0x7f030060

.field public static final location_group_share_setting_location_list_item:I = 0x7f030061

.field public static final location_group_share_setting_location_pager_item:I = 0x7f030062

.field public static final location_item_group_detail:I = 0x7f030063

.field public static final location_list_item_layout:I = 0x7f030064

.field public static final location_list_items:I = 0x7f030065

.field public static final location_share_allow_dialog:I = 0x7f030066

.field public static final location_share_list_items:I = 0x7f030067

.field public static final locshare_suggestions_list_item_layout:I = 0x7f030068

.field public static final logs_list_item_layout:I = 0x7f030069

.field public static final logs_list_items:I = 0x7f03006a

.field public static final message_number_list_item_layout:I = 0x7f03006b

.field public static final message_number_list_items:I = 0x7f03006c

.field public static final message_reader_body:I = 0x7f03006d

.field public static final more_menu_list_item:I = 0x7f03006e

.field public static final ondevicehelp_items:I = 0x7f03006f

.field public static final overlay_home_icon:I = 0x7f030070

.field public static final overlay_voice_dialog:I = 0x7f030071

.field public static final participant_not_joined:I = 0x7f030072

.field public static final phone_number_list_item_layout:I = 0x7f030073

.field public static final phone_number_list_items:I = 0x7f030074

.field public static final progress_loading_dialog:I = 0x7f030075

.field public static final register_list_text_and_add:I = 0x7f030076

.field public static final register_list_text_and_settings:I = 0x7f030077

.field public static final sampleapp_activity_main:I = 0x7f030078

.field public static final sampleapp_location_list_item:I = 0x7f030079

.field public static final sampleapp_location_sharing:I = 0x7f03007a

.field public static final sampleapp_location_simulation_fragment:I = 0x7f03007b

.field public static final sampleapp_location_suggestions:I = 0x7f03007c

.field public static final sampleapp_message_inbox:I = 0x7f03007d

.field public static final sampleapp_message_list_item:I = 0x7f03007e

.field public static final sampleapp_message_suggestions:I = 0x7f03007f

.field public static final sampleapp_music_main:I = 0x7f030080

.field public static final sampleapp_phone_list_item:I = 0x7f030081

.field public static final sampleapp_phone_logs:I = 0x7f030082

.field public static final sampleapp_phone_suggestions:I = 0x7f030083

.field public static final sampleapp_safereader_simulation_fragment:I = 0x7f030084

.field public static final sampleapp_server_preference_layout:I = 0x7f030085

.field public static final sampleapp_tos_dialog:I = 0x7f030086

.field public static final sampleapp_widget_language_choice:I = 0x7f030087

.field public static final sampleapp_widget_language_list_view:I = 0x7f030088

.field public static final search_edit_text_layout:I = 0x7f030089

.field public static final search_inbox_list_item_layout:I = 0x7f03008a

.field public static final search_inbox_list_items:I = 0x7f03008b

.field public static final search_music_content_list_items:I = 0x7f03008c

.field public static final search_music_list_item_layout:I = 0x7f03008d

.field public static final search_music_player_actionbar_layout:I = 0x7f03008e

.field public static final search_phone_list_item_layout:I = 0x7f03008f

.field public static final search_phone_list_items:I = 0x7f030090

.field public static final simple_dropdown_item_1line:I = 0x7f030091

.field public static final suggestions_list_item_layout:I = 0x7f030092

.field public static final suggestions_list_items:I = 0x7f030093

.field public static final suggestions_list_location_item:I = 0x7f030094

.field public static final suggestions_location_list_items:I = 0x7f030095

.field public static final toast_layout:I = 0x7f030096

.field public static final view_indicator_in_multi:I = 0x7f030097

.field public static final view_joined_participant:I = 0x7f030098

.field public static final view_joined_participant_more:I = 0x7f030099

.field public static final view_multi_music:I = 0x7f03009a

.field public static final view_noti_alarm_content:I = 0x7f03009b

.field public static final view_noti_alarm_content_in_multi:I = 0x7f03009c

.field public static final view_noti_base:I = 0x7f03009d

.field public static final view_noti_base_in_multi:I = 0x7f03009e

.field public static final view_noti_bluetooth_req_dialog:I = 0x7f03009f

.field public static final view_noti_bluetooth_req_dialog_in_multi:I = 0x7f0300a0

.field public static final view_noti_charger_content:I = 0x7f0300a1

.field public static final view_noti_common_layout:I = 0x7f0300a2

.field public static final view_noti_commute_dialog:I = 0x7f0300a3

.field public static final view_noti_commute_dialog_in_multi:I = 0x7f0300a4

.field public static final view_noti_failed_request_location:I = 0x7f0300a5

.field public static final view_noti_general_message:I = 0x7f0300a6

.field public static final view_noti_general_message_in_multi:I = 0x7f0300a7

.field public static final view_noti_home:I = 0x7f0300a8

.field public static final view_noti_home_02:I = 0x7f0300a9

.field public static final view_noti_location_group_share_dialog:I = 0x7f0300aa

.field public static final view_noti_location_group_share_extend:I = 0x7f0300ab

.field public static final view_noti_location_group_share_ignore_dialog:I = 0x7f0300ac

.field public static final view_noti_location_group_share_ignore_invitation_dialog:I = 0x7f0300ad

.field public static final view_noti_location_group_share_stop_dialog:I = 0x7f0300ae

.field public static final view_noti_location_has_no_account:I = 0x7f0300af

.field public static final view_noti_location_request_dialog:I = 0x7f0300b0

.field public static final view_noti_location_send_request_dialog:I = 0x7f0300b1

.field public static final view_noti_location_sent:I = 0x7f0300b2

.field public static final view_noti_location_share_dialog:I = 0x7f0300b3

.field public static final view_noti_location_shared:I = 0x7f0300b4

.field public static final view_noti_location_shared_ended:I = 0x7f0300b5

.field public static final view_noti_location_sharing_changed_destination:I = 0x7f0300b6

.field public static final view_noti_location_waiting_request_dialog:I = 0x7f0300b7

.field public static final view_noti_low_battery:I = 0x7f0300b8

.field public static final view_noti_low_battery_in_multi:I = 0x7f0300b9

.field public static final view_noti_maptest:I = 0x7f0300ba

.field public static final view_noti_message_dialog:I = 0x7f0300bb

.field public static final view_noti_message_dialog_in_multi:I = 0x7f0300bc

.field public static final view_noti_message_read_dialog:I = 0x7f0300bd

.field public static final view_noti_message_read_dialog_in_multi:I = 0x7f0300be

.field public static final view_noti_music:I = 0x7f0300bf

.field public static final view_noti_music_in_multi:I = 0x7f0300c0

.field public static final view_noti_placeon_disclaimer:I = 0x7f0300c1

.field public static final view_noti_samsung_account_dialog:I = 0x7f0300c2

.field public static final view_noti_schedule_content:I = 0x7f0300c3

.field public static final view_noti_schedule_with_button:I = 0x7f0300c4

.field public static final view_noti_schedule_with_button_in_multi:I = 0x7f0300c5

.field public static final view_noti_schedule_wo_button:I = 0x7f0300c6

.field public static final view_noti_schedule_wo_button_in_multi:I = 0x7f0300c7

.field public static final view_noti_sending_changed_destination:I = 0x7f0300c8

.field public static final view_noti_sending_dialog:I = 0x7f0300c9

.field public static final view_noti_sending_dialog_in_multi:I = 0x7f0300ca

.field public static final view_noti_sending_invitations:I = 0x7f0300cb

.field public static final view_noti_sharing_dialog:I = 0x7f0300cc

.field public static final view_noti_sharing_dialog_in_multi:I = 0x7f0300cd

.field public static final view_noti_smartalert_dialog:I = 0x7f0300ce

.field public static final view_noti_smartalert_dialog_in_multi:I = 0x7f0300cf

.field public static final view_noti_test:I = 0x7f0300d0

.field public static final view_participant:I = 0x7f0300d1

.field public static final view_setup_indicator:I = 0x7f0300d2

.field public static final view_user_arrived:I = 0x7f0300d3

.field public static final voice_home_actionbar_layout:I = 0x7f0300d4

.field public static final voice_indicator_actionbar_layout:I = 0x7f0300d5

.field public static final voice_location_actionbar_layout:I = 0x7f0300d6

.field public static final voice_message_actionbar_layout:I = 0x7f0300d7

.field public static final voice_message_composer_actionbar_layout:I = 0x7f0300d8

.field public static final voice_message_reader_actionbar_layout:I = 0x7f0300d9

.field public static final voice_music_player_actionbar_layout:I = 0x7f0300da

.field public static final voice_phone_actionbar_layout:I = 0x7f0300db


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3043
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
