.class Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$4;
.super Ljava/lang/Object;
.source "MessageReaderActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceDrivingChangeUpdater$DRVSTATUS:[I


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceDrivingChangeUpdater$DRVSTATUS()[I
    .locals 3

    .prologue
    .line 1089
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$4;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceDrivingChangeUpdater$DRVSTATUS:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->values()[Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->STATUS_CHANGE_NONE:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->STATUS_NOTIFY_CAR_SPEED_STATUS_CHANGE:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->STATUS_REQUEST_START_DRIVING:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->STATUS_REQUEST_STOP_DRIVING:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$4;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceDrivingChangeUpdater$DRVSTATUS:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    .line 1089
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDriveStatusChanged(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;)V
    .locals 6
    .param p1, "Type"    # Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1095
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "mDrivingChangeListener Type="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->Debug(Ljava/lang/String;)V

    .line 1096
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$4;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceDrivingChangeUpdater$DRVSTATUS()[I

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DRVSTATUS;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1125
    :goto_0
    :pswitch_0
    return-void

    .line 1101
    :pswitch_1
    const-string/jumbo v2, "MessageReaderActivity"

    const-string/jumbo v3, "MessageReader - driving on/off"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1102
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    .line 1103
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;

    move-result-object v0

    .line 1104
    .local v0, "dlspeedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    if-eqz v0, :cond_0

    .line 1105
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;->getCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    move-result-object v2

    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;->CAR_SPEED_STATUS_STOPPED:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    if-eq v2, v3, :cond_0

    .line 1106
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->setCurrentPage(Z)V
    invoke-static {v2, v5}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$13(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Z)V

    goto :goto_0

    .line 1108
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->setCurrentPage(Z)V
    invoke-static {v2, v4}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$13(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Z)V

    goto :goto_0

    .line 1112
    .end local v0    # "dlspeedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    :pswitch_2
    const-string/jumbo v2, "MessageReaderActivity"

    const-string/jumbo v3, "MessageReader - driving speed changed"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1113
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    .line 1114
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->responseCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;

    move-result-object v1

    .line 1116
    .local v1, "speedStatus":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    if-eqz v1, :cond_1

    .line 1117
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;->getCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    move-result-object v2

    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;->CAR_SPEED_STATUS_STOPPED:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    if-eq v2, v3, :cond_1

    .line 1118
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->setCurrentPage(Z)V
    invoke-static {v2, v5}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$13(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Z)V

    goto :goto_0

    .line 1120
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->setCurrentPage(Z)V
    invoke-static {v2, v4}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$13(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Z)V

    goto :goto_0

    .line 1096
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
