.class public Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "SettingsSuggestedContactsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$ViewHolder;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "[SettingsSuggestedContactsActivity]"


# instance fields
.field dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private ivAddContact:Landroid/widget/ImageView;

.field private layoutAddContact:Landroid/widget/LinearLayout;

.field private layoutAutoSuggest:Landroid/widget/LinearLayout;

.field private layoutFavorites:Landroid/widget/LinearLayout;

.field private layoutSetFavorite:Landroid/widget/LinearLayout;

.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation
.end field

.field preference:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

.field private rbAutoSuggest:Landroid/widget/RadioButton;

.field private rbSetFavorite:Landroid/widget/RadioButton;

.field private tvAddContact:Landroid/widget/TextView;

.field private tvFavoriteCount:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)Landroid/widget/RadioButton;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->rbAutoSuggest:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)Landroid/widget/RadioButton;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->rbSetFavorite:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;I)V
    .locals 0

    .prologue
    .line 208
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->setTextViewFavoriteContactsCount(I)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutAddContact:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutFavorites:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->tvAddContact:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->ivAddContact:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;Z)V
    .locals 0

    .prologue
    .line 310
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->setItemOnClickEnable(Z)V

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->mList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)V
    .locals 0

    .prologue
    .line 213
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->loadListContents()V

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->mList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private loadContent()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 160
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 162
    .local v0, "preference":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v1, "PREF_SETTINGS_SUGGEST_CONTACTS"

    .line 161
    invoke-virtual {v0, v1, v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v1

    .line 162
    if-eqz v1, :cond_0

    .line 163
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->rbAutoSuggest:Landroid/widget/RadioButton;

    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 164
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->rbSetFavorite:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 170
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutAddContact:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->rbSetFavorite:Landroid/widget/RadioButton;

    invoke-virtual {v2}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 171
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutFavorites:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->rbSetFavorite:Landroid/widget/RadioButton;

    invoke-virtual {v2}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 172
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->tvAddContact:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->rbSetFavorite:Landroid/widget/RadioButton;

    invoke-virtual {v2}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 173
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->ivAddContact:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->rbSetFavorite:Landroid/widget/RadioButton;

    invoke-virtual {v2}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 175
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 176
    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$7;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$7;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)V

    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkFavoriteContactListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;)V

    .line 193
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->isContactsChanged()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 194
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/automotive/drivelink/DLApplication;->setContactsChanged(Z)V

    .line 195
    invoke-direct {p0, v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->requestFavoriteContacts(Z)V

    .line 196
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->setPhoneSuggestionListUpdateListener()V

    .line 197
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updatePhoneSuggestionList()V

    .line 201
    :goto_1
    return-void

    .line 166
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->rbAutoSuggest:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 167
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->rbSetFavorite:Landroid/widget/RadioButton;

    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 199
    :cond_1
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->requestFavoriteContacts(Z)V

    goto :goto_1
.end method

.method private loadListContents()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/16 v12, 0xff

    .line 216
    const-string/jumbo v8, "layout_inflater"

    invoke-virtual {p0, v8}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 218
    .local v4, "layoutInflater":Landroid/view/LayoutInflater;
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutFavorites:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 220
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->mList:Ljava/util/ArrayList;

    if-nez v8, :cond_1

    .line 276
    :cond_0
    return-void

    .line 223
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->mList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v2, v8, :cond_0

    .line 224
    move v6, v2

    .line 227
    .local v6, "position":I
    const v8, 0x7f03002c

    .line 226
    invoke-virtual {v4, v8, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 229
    .local v7, "v":Landroid/view/View;
    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$ViewHolder;

    invoke-direct {v1, p0, v13}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$ViewHolder;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$ViewHolder;)V

    .line 232
    .local v1, "holder":Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$ViewHolder;
    const v8, 0x7f090127

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 231
    iput-object v8, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$ViewHolder;->tvFavoriteContacts:Landroid/widget/TextView;

    .line 234
    const v8, 0x7f09012a

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    .line 233
    iput-object v8, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$ViewHolder;->btnFavoriteContacts:Landroid/widget/Button;

    .line 236
    iget-object v8, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$ViewHolder;->btnFavoriteContacts:Landroid/widget/Button;

    .line 237
    new-instance v9, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$8;

    invoke-direct {v9, p0, v6}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$8;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;I)V

    invoke-virtual {v8, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    const-string/jumbo v8, "[SettingsSuggestedContactsActivity]"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "isEnabled : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutFavorites:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    iget-object v8, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$ViewHolder;->btnFavoriteContacts:Landroid/widget/Button;

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutFavorites:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/Button;->setEnabled(Z)V

    .line 252
    iget-object v8, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$ViewHolder;->tvFavoriteContacts:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutFavorites:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 254
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->mList:Ljava/util/ArrayList;

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 255
    .local v3, "item":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-eqz v3, :cond_2

    .line 256
    iget-object v8, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$ViewHolder;->tvFavoriteContacts:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    iget-object v8, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$ViewHolder;->btnFavoriteContacts:Landroid/widget/Button;

    new-instance v9, Ljava/lang/StringBuilder;

    .line 258
    const v10, 0x7f0a0636

    invoke-virtual {p0, v10}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 259
    const-string/jumbo v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 261
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 257
    invoke-virtual {v8, v9}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 264
    :cond_2
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutFavorites:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 266
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 267
    .local v0, "divider":Landroid/view/View;
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    .line 268
    const/4 v8, -0x1

    .line 269
    const/4 v9, 0x1

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 270
    invoke-virtual {v11}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v11

    .line 268
    invoke-static {v9, v10, v11}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v9

    float-to-int v9, v9

    .line 267
    invoke-direct {v5, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 271
    .local v5, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 272
    const/16 v8, 0x26

    invoke-static {v8, v12, v12, v12}, Landroid/graphics/Color;->argb(IIII)I

    move-result v8

    invoke-virtual {v0, v8}, Landroid/view/View;->setBackgroundColor(I)V

    .line 274
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutFavorites:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 223
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0
.end method

.method private requestFavoriteContacts(Z)V
    .locals 2
    .param p1, "reload"    # Z

    .prologue
    .line 204
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->setTextViewFavoriteContactsCount(I)V

    .line 205
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestFavoriteContactList(Landroid/content/Context;Z)V

    .line 206
    return-void
.end method

.method private setItemOnClickEnable(Z)V
    .locals 6
    .param p1, "isEnable"    # Z

    .prologue
    .line 311
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutFavorites:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    .line 312
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 323
    return-void

    .line 313
    :cond_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutFavorites:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 314
    const v5, 0x7f09012a

    .line 313
    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 315
    .local v3, "v":Landroid/view/View;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutFavorites:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 316
    const v5, 0x7f090127

    .line 315
    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 318
    .local v2, "textView":Landroid/view/View;
    instance-of v4, v3, Landroid/widget/Button;

    if-eqz v4, :cond_1

    .line 319
    invoke-virtual {v3, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 320
    invoke-virtual {v2, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 312
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private setTextViewFavoriteContactsCount(I)V
    .locals 6
    .param p1, "count"    # I

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->tvFavoriteCount:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 210
    const v2, 0x7f0a043f

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 209
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 285
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updatePhoneSuggestionList()V

    .line 286
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateMessageSuggestionList()V

    .line 287
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onBackPressed()V

    .line 288
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v3, 0x7f0a0445

    .line 58
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    const v1, 0x7f030033

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->setContentView(I)V

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->preference:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    .line 64
    const v1, 0x7f090154

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->rbAutoSuggest:Landroid/widget/RadioButton;

    .line 65
    const v1, 0x7f090156

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->rbSetFavorite:Landroid/widget/RadioButton;

    .line 66
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->rbAutoSuggest:Landroid/widget/RadioButton;

    .line 67
    const v2, 0x7f0a0453

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 66
    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->rbSetFavorite:Landroid/widget/RadioButton;

    .line 69
    const v2, 0x7f0a0455

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 68
    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 71
    const v1, 0x7f090153

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutAutoSuggest:Landroid/widget/LinearLayout;

    .line 72
    const v1, 0x7f090155

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutSetFavorite:Landroid/widget/LinearLayout;

    .line 73
    const v1, 0x7f090158

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutAddContact:Landroid/widget/LinearLayout;

    .line 74
    const v1, 0x7f090159

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->tvAddContact:Landroid/widget/TextView;

    .line 75
    const v1, 0x7f09015a

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->ivAddContact:Landroid/widget/ImageView;

    .line 77
    const v1, 0x7f09015b

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutFavorites:Landroid/widget/LinearLayout;

    .line 78
    const v1, 0x7f090157

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->tvFavoriteCount:Landroid/widget/TextView;

    .line 81
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 80
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 83
    const v1, 0x7f090152

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 84
    .local v0, "btnNavBack":Landroid/widget/LinearLayout;
    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    new-instance v1, Ljava/lang/StringBuilder;

    .line 94
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 95
    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 96
    const v2, 0x7f0a03f4

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 93
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 98
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->setTitle(I)V

    .line 100
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutAutoSuggest:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->rbAutoSuggest:Landroid/widget/RadioButton;

    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutSetFavorite:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->rbSetFavorite:Landroid/widget/RadioButton;

    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$5;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutAddContact:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$6;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 156
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 292
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onPause()V

    .line 293
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 297
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onResume()V

    .line 299
    const-string/jumbo v0, "[SettingsSuggestedContactsActivity]"

    const-string/jumbo v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    const-string/jumbo v0, "[SettingsSuggestedContactsActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "DrivingStatus :  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 303
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 302
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->SettingsDrivingDialog(Landroid/content/Context;)V

    .line 307
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->loadContent()V

    .line 308
    return-void
.end method
