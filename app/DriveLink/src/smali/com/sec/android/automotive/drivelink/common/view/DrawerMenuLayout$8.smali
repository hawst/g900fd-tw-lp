.class Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$8;
.super Ljava/lang/Object;
.source "DrawerMenuLayout.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$8;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    .line 386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I
    .param p5, "arg4"    # I
    .param p6, "arg5"    # I
    .param p7, "arg6"    # I
    .param p8, "arg7"    # I
    .param p9, "arg8"    # I

    .prologue
    const/high16 v3, 0x10e0000

    .line 391
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$8;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuBgFrame:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$11(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/FrameLayout;

    move-result-object v0

    .line 392
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 393
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 395
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$8;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    .line 394
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 397
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 398
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$8;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$10(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/ListView;

    move-result-object v0

    .line 399
    invoke-virtual {v0}, Landroid/widget/ListView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 400
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 402
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$8;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    .line 401
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 404
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 405
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$8;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuBtn:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$12(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/ImageView;

    move-result-object v0

    .line 406
    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 407
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$8;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$2(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/content/Context;

    move-result-object v1

    const/high16 v2, -0x3f600000    # -5.0f

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->dipToPixels(Landroid/content/Context;F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 409
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$8;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    .line 408
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 411
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 412
    return-void
.end method
