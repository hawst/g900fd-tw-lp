.class public Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;
.super Ljava/lang/Thread;
.source "SettingsContactsImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$OnImageLoadEventListener;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDLInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private mHandler:Landroid/os/Handler;

.field private mOnImageLoadEventListener:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$OnImageLoadEventListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$OnImageLoadEventListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$OnImageLoadEventListener;

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->mContext:Landroid/content/Context;

    .line 21
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->mDLInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 27
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->mOnImageLoadEventListener:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$OnImageLoadEventListener;

    .line 32
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->mContext:Landroid/content/Context;

    .line 33
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->mOnImageLoadEventListener:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$OnImageLoadEventListener;

    .line 35
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->mDLInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 36
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->mDLInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->sendEventToListener(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private createHandler()V
    .locals 1

    .prologue
    .line 45
    :try_start_0
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 46
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->mHandler:Landroid/os/Handler;

    .line 47
    invoke-static {}, Landroid/os/Looper;->loop()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    :goto_0
    return-void

    .line 48
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private sendEventToListener(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .param p2, "imageView"    # Landroid/widget/ImageView;
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->mOnImageLoadEventListener:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$OnImageLoadEventListener;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->mOnImageLoadEventListener:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$OnImageLoadEventListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$OnImageLoadEventListener;->onImageLoaded(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    .line 96
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized requestContactImage(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;Landroid/widget/ImageView;)V
    .locals 2
    .param p1, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .param p2, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 62
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$2;

    invoke-direct {v1, p0, p2, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$2;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;Landroid/widget/ImageView;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :cond_0
    monitor-exit p0

    return-void

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->createHandler()V

    .line 41
    return-void
.end method

.method public declared-synchronized stopThread()V
    .locals 2

    .prologue
    .line 53
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader$1;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsContactsImageLoader;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    monitor-exit p0

    return-void

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
