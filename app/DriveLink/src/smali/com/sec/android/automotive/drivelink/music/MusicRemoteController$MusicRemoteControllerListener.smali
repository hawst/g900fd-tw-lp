.class public interface abstract Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;
.super Ljava/lang/Object;
.source "MusicRemoteController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MusicRemoteControllerListener"
.end annotation


# virtual methods
.method public abstract onMetadataUpdate(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;J)V
.end method

.method public abstract onPlaybackStateUpdate(IJJ)V
.end method

.method public abstract onRegistered()V
.end method
