.class public final enum Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;
.super Ljava/lang/Enum;
.source "DLServiceMessageChangeUpdater.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MSGSTATUS"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

.field public static final enum STATUS_CHANGE_NONE:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

.field public static final enum STATUS_RECEIVE:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

.field public static final enum STATUS_REQUEST_CHANGE_STATUS_TO_READ:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

.field public static final enum STATUS_REQUEST_INBOX_LIST:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

.field public static final enum STATUS_REQUEST_INCOMMING_MESSAGE_LIST:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

.field public static final enum STATUS_REQUEST_SEND_MESSAGE:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

.field public static final enum STATUS_REQUEST_SUGGESTION_UPDATED:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

.field public static final enum STATUS_REQUEST_UNREAD_COUNT:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

.field public static final enum STATUS_REQUEST_UNREAD_MESSAGE_BY_INBOX:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    const-string/jumbo v1, "STATUS_CHANGE_NONE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_CHANGE_NONE:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    const-string/jumbo v1, "STATUS_RECEIVE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_RECEIVE:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    .line 9
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    const-string/jumbo v1, "STATUS_REQUEST_UNREAD_COUNT"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_UNREAD_COUNT:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    const-string/jumbo v1, "STATUS_REQUEST_INCOMMING_MESSAGE_LIST"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_INCOMMING_MESSAGE_LIST:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    const-string/jumbo v1, "STATUS_REQUEST_INBOX_LIST"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_INBOX_LIST:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    const-string/jumbo v1, "STATUS_REQUEST_CHANGE_STATUS_TO_READ"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_CHANGE_STATUS_TO_READ:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    const-string/jumbo v1, "STATUS_REQUEST_SEND_MESSAGE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_SEND_MESSAGE:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    const-string/jumbo v1, "STATUS_REQUEST_UNREAD_MESSAGE_BY_INBOX"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_UNREAD_MESSAGE_BY_INBOX:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    const-string/jumbo v1, "STATUS_REQUEST_SUGGESTION_UPDATED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_SUGGESTION_UPDATED:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    .line 7
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_CHANGE_NONE:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_RECEIVE:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_UNREAD_COUNT:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_INCOMMING_MESSAGE_LIST:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_INBOX_LIST:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_CHANGE_STATUS_TO_READ:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_SEND_MESSAGE:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_UNREAD_MESSAGE_BY_INBOX:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_SUGGESTION_UPDATED:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
