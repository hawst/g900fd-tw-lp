.class public Lcom/sec/android/automotive/drivelink/common/flow/FlowConstants$FlowID;
.super Ljava/lang/Object;
.source "FlowConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/flow/FlowConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FlowID"
.end annotation


# static fields
.field public static final DM_DIAL:Ljava/lang/String; = "DM_DIAL"

.field public static final DM_DIAL_CONTACT:Ljava/lang/String; = "DM_DIAL_CONTACT"

.field public static final DM_DIAL_CONTACT_SEARCH_LIST:Ljava/lang/String; = "DM_DIAL_CONTACT_SEARCH_LIST"

.field public static final DM_DIAL_TYPE:Ljava/lang/String; = "DM_DIAL_TYPE"

.field public static final DM_LOCATION:Ljava/lang/String; = "DM_LOCATION"

.field public static final DM_LOCATION_ADDR_TYPE:Ljava/lang/String; = "DM_LOCATION_ADDR_TYPE"

.field public static final DM_LOCATION_CONTACT_REQUEST:Ljava/lang/String; = "DM_LOCATION_CONTACT_REQUEST"

.field public static final DM_LOCATION_CONTACT_REQUEST_SEARCH_LIST:Ljava/lang/String; = "DM_LOCATION_CONTACT_REQUEST_SEARCH_LIST"

.field public static final DM_LOCATION_CONTACT_REQUEST_TYPE:Ljava/lang/String; = "DM_LOCATION_CONTACT_REQUEST_TYPE"

.field public static final DM_LOCATION_CONTACT_SHARE:Ljava/lang/String; = "DM_LOCATION_CONTACT_SHARE"

.field public static final DM_LOCATION_CONTACT_SHARE_SEARCH_LIST:Ljava/lang/String; = "DM_LOCATION_CONTACT_SHARE_SEARCH_LIST"

.field public static final DM_LOCATION_CONTACT_SHARE_TYPE:Ljava/lang/String; = "DM_LOCATION_CONTACT_SHARE_TYPE"

.field public static final DM_LOCATION_NAV_SEARCH_CONFIRM:Ljava/lang/String; = "DM_LOCATION_NAV_SEARCH_CONFIRM"

.field public static final DM_LOCATION_REQ_CONFIRM:Ljava/lang/String; = "DM_LOCATION_REQ_CONFIRM"

.field public static final DM_LOCATION_SHARE_CONFIRM:Ljava/lang/String; = "DM_LOCATION_SHARE_CONFIRM"

.field public static final DM_MAIN:Ljava/lang/String; = "DM_MAIN"

.field public static final DM_MUSIC_PLAYER:Ljava/lang/String; = "DM_MUSIC_PLAYER"

.field public static final DM_SMS_COMPLETE:Ljava/lang/String; = "DM_SMS_COMPLETE"

.field public static final DM_SMS_COMPOSE:Ljava/lang/String; = "DM_SMS_COMPOSE"

.field public static final DM_SMS_COMPOSING:Ljava/lang/String; = "DM_SMS_COMPOSING"

.field public static final DM_SMS_CONTACT:Ljava/lang/String; = "DM_SMS_CONTACT"

.field public static final DM_SMS_CONTACT_SEARCH_LIST:Ljava/lang/String; = "DM_SMS_CONTACT_SEARCH_LIST"

.field public static final DM_SMS_INBOX:Ljava/lang/String; = "DM_SMS_INBOX"

.field public static final DM_SMS_INBOX_SEARCH_LIST:Ljava/lang/String; = "DM_SMS_INBOX_SEARCH_LIST"

.field public static final DM_SMS_READBACK:Ljava/lang/String; = "DM_SMS_READBACK"

.field public static final DM_SMS_READBACK_NOTI:Ljava/lang/String; = "DM_SMS_READBACK_NOTI"

.field public static final DM_SMS_SEND:Ljava/lang/String; = "DM_SMS_SEND"

.field public static final DM_SMS_TYPE:Ljava/lang/String; = "DM_SMS_TYPE"

.field public static final DM_WHAT_CAN_I_SAY:Ljava/lang/String; = "DM_WHAT_CAN_I_SAY"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
