.class public Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP;
.super Landroid/widget/EditText;
.source "EditTextForSettingsSIP.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$OnSettingsSipStateListener;,
        Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;
    }
.end annotation


# instance fields
.field mLocationTextViewStateListener:Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$OnSettingsSipStateListener;

.field mcontext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 19
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP;->mcontext:Landroid/content/Context;

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP;->mcontext:Landroid/content/Context;

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP;->mcontext:Landroid/content/Context;

    .line 31
    return-void
.end method


# virtual methods
.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP;->mcontext:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 36
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 37
    invoke-super {p0, p1}, Landroid/widget/EditText;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 44
    :goto_0
    return v0

    .line 41
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;->SIP_CLOSE:Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP;->setSettingsStatechanged(Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;)V

    .line 44
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/EditText;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setOnSettingsSipStateListener(Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$OnSettingsSipStateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$OnSettingsSipStateListener;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP;->mLocationTextViewStateListener:Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$OnSettingsSipStateListener;

    .line 50
    return-void
.end method

.method public setSettingsStatechanged(Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;)V
    .locals 1
    .param p1, "state"    # Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP;->mLocationTextViewStateListener:Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$OnSettingsSipStateListener;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP;->mLocationTextViewStateListener:Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$OnSettingsSipStateListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$OnSettingsSipStateListener;->OnStateChanged(Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;)V

    .line 55
    :cond_0
    return-void
.end method
