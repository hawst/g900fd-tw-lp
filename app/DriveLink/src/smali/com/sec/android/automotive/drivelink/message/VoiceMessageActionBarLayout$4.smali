.class Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$4;
.super Ljava/lang/Object;
.source "VoiceMessageActionBarLayout.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->runTTSBarAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$4;->this$0:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    .line 584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 608
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$4;->this$0:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->access$16(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;Z)V

    .line 609
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 600
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$4;->this$0:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->isRunning:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->access$12(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$4;->this$0:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$4;->this$0:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mDirection:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->access$13(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->access$14(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;Z)V

    .line 602
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$4;->this$0:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    # invokes: Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->runTTSBarAnimation()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->access$15(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)V

    .line 604
    :cond_0
    return-void

    .line 601
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 596
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 590
    return-void
.end method
