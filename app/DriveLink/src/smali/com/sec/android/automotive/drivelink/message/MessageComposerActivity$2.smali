.class Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$2;
.super Ljava/lang/Object;
.source "MessageComposerActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    .line 812
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseRequestContactList(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 817
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_2

    .line 820
    :cond_0
    const-string/jumbo v2, "MessageComposerActivity"

    const-string/jumbo v3, "(contactList == null) || (contactList.size() <= 0) finish"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 821
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->finish()V

    .line 835
    :cond_1
    return-void

    .line 824
    :cond_2
    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mIndex:I
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$6()I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 826
    .local v0, "dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-eqz v0, :cond_1

    .line 827
    const/4 v1, 0x0

    .line 828
    .local v1, "i":I
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 829
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$7(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$7(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    .line 828
    if-gtz v2, :cond_1

    .line 830
    :cond_3
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    .line 831
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    .line 830
    invoke-static {v3, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$8(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;Ljava/lang/String;)V

    .line 829
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public onResponseRequestSearchedContactList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 841
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    return-void
.end method
