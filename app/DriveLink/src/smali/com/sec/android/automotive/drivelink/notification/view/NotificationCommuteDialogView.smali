.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommuteDialogView;
.super Landroid/widget/LinearLayout;
.source "NotificationCommuteDialogView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field protected mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommuteDialogView;->mView:Landroid/view/View;

    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommuteDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommuteDialogView;->mView:Landroid/view/View;

    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommuteDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommuteDialogView;->mView:Landroid/view/View;

    .line 24
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommuteDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 26
    return-void
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommuteDialogView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 46
    const-string/jumbo v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 45
    check-cast v1, Landroid/view/LayoutInflater;

    .line 48
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommuteDialogView;->mView:Landroid/view/View;

    if-nez v2, :cond_0

    .line 49
    const v2, 0x7f0300a3

    invoke-virtual {v1, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommuteDialogView;->mView:Landroid/view/View;

    .line 56
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommuteDialogView;->initilizeMap()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_0
    return-void

    .line 58
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private initilizeMap()V
    .locals 0

    .prologue
    .line 92
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 68
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 69
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 79
    :pswitch_0
    return-void

    .line 69
    :pswitch_data_0
    .packed-switch 0x7f0902b7
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
