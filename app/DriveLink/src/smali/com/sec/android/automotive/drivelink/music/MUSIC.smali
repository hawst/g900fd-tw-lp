.class public Lcom/sec/android/automotive/drivelink/music/MUSIC;
.super Ljava/lang/Object;
.source "MUSIC.java"


# static fields
.field public static final FEATURE_REMOTE_PLAYER:Z = false

.field private static final SUB:Ljava/lang/String; = "MUSIC"

.field private static final TAG:Ljava/lang/String; = "[CarMode]"

.field private static instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;


# instance fields
.field private isBound:Z

.field private isStarted:Z

.field private mContext:Landroid/content/Context;

.field private mService:Lcom/sec/android/automotive/drivelink/music/MusicService;

.field private mServiceConnection:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/music/MUSIC;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    .line 21
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->isBound:Z

    .line 20
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->isStarted:Z

    .line 24
    return-void
.end method

.method public static StartService(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "connection"    # Landroid/content/ServiceConnection;

    .prologue
    const/4 v5, 0x1

    .line 32
    const-string/jumbo v1, "e"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSIC"

    .line 33
    const-string/jumbo v4, "[MUSIC] Drivelink Music Service started"

    .line 32
    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    iput-object p0, v1, Lcom/sec/android/automotive/drivelink/music/MUSIC;->mContext:Landroid/content/Context;

    .line 35
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    iput-object p1, v1, Lcom/sec/android/automotive/drivelink/music/MUSIC;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 36
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 37
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    .line 38
    sget-object v2, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/music/MUSIC;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 37
    invoke-virtual {p0, v0, v2, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v2

    iput-boolean v2, v1, Lcom/sec/android/automotive/drivelink/music/MUSIC;->isBound:Z

    .line 39
    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 40
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    iput-boolean v5, v1, Lcom/sec/android/automotive/drivelink/music/MUSIC;->isStarted:Z

    .line 41
    return-void
.end method

.method public static getService()Lcom/sec/android/automotive/drivelink/music/MusicService;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->mService:Lcom/sec/android/automotive/drivelink/music/MusicService;

    return-object v0
.end method

.method public static isBound()Z
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    iget-boolean v0, v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->isBound:Z

    return v0
.end method

.method public static isStarted()Z
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    iget-boolean v0, v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->isStarted:Z

    return v0
.end method

.method public static declared-synchronized setWhenOnConnected(Landroid/content/Context;Landroid/os/IBinder;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "service"    # Landroid/os/IBinder;

    .prologue
    .line 53
    const-class v3, Lcom/sec/android/automotive/drivelink/music/MUSIC;

    monitor-enter v3

    :try_start_0
    const-string/jumbo v2, "e"

    const-string/jumbo v4, "[CarMode]"

    const-string/jumbo v5, "MUSIC"

    .line 54
    const-string/jumbo v6, "[MUSIC] Drivelink Music Service binded"

    .line 53
    invoke-static {v2, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    move-object v0, p1

    check-cast v0, Lcom/sec/android/automotive/drivelink/music/MusicService$MyBinder;

    move-object v1, v0

    .line 56
    .local v1, "binder":Lcom/sec/android/automotive/drivelink/music/MusicService$MyBinder;
    sget-object v2, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService$MyBinder;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v4

    iput-object v4, v2, Lcom/sec/android/automotive/drivelink/music/MUSIC;->mService:Lcom/sec/android/automotive/drivelink/music/MusicService;

    .line 57
    sget-object v2, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/music/MUSIC;->mService:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v2, p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->init(Landroid/content/Context;)V

    .line 58
    sget-object v2, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/music/MUSIC;->mService:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->registerListener(Lcom/sec/android/automotive/drivelink/music/MusicListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    monitor-exit v3

    return-void

    .line 53
    .end local v1    # "binder":Lcom/sec/android/automotive/drivelink/music/MusicService$MyBinder;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static declared-synchronized setWhenOnDestoryed()V
    .locals 5

    .prologue
    .line 62
    const-class v1, Lcom/sec/android/automotive/drivelink/music/MUSIC;

    monitor-enter v1

    :try_start_0
    const-string/jumbo v0, "e"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSIC"

    .line 63
    const-string/jumbo v4, "[MUSIC] Drivelink Music Service unbinding"

    .line 62
    invoke-static {v0, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->mService:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->unregisterListener(Lcom/sec/android/automotive/drivelink/music/MusicListener;)V

    .line 66
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    iget-boolean v0, v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->isBound:Z

    if-eqz v0, :cond_0

    .line 67
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/music/MUSIC;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 70
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->mService:Lcom/sec/android/automotive/drivelink/music/MusicService;

    if-eqz v0, :cond_1

    .line 71
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->mService:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->abandonAudioFocus()V

    .line 74
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->mService:Lcom/sec/android/automotive/drivelink/music/MusicService;

    .line 75
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->isBound:Z

    .line 76
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->instance:Lcom/sec/android/automotive/drivelink/music/MUSIC;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/sec/android/automotive/drivelink/music/MUSIC;->isStarted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    monitor-exit v1

    return-void

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
