.class public Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;
.super Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;
.source "MessageManager.java"


# static fields
.field private static final SENT:Ljava/lang/String; = "SMS_SENT"

.field private static final TAG:Ljava/lang/String; = "MessageManager"


# instance fields
.field private mContext:Landroid/content/Context;

.field mMsgManagerReceiver:Landroid/content/BroadcastReceiver;

.field private mPhoneNumber:Ljava/lang/String;

.field mSentPI:Landroid/app/PendingIntent;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;-><init>()V

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->mContext:Landroid/content/Context;

    .line 21
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->mPhoneNumber:Ljava/lang/String;

    .line 26
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->mSentPI:Landroid/app/PendingIntent;

    .line 54
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->mMsgManagerReceiver:Landroid/content/BroadcastReceiver;

    .line 30
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;Z)V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->sendRequestResult(Z)V

    return-void
.end method

.method private registerSendReceiver(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 89
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "SMS_SENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->mSentPI:Landroid/app/PendingIntent;

    .line 91
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->mMsgManagerReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v2, "SMS_SENT"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 92
    return-void
.end method

.method private sendMessage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;

    .prologue
    .line 108
    :try_start_0
    const-string/jumbo v1, "MessageManager"

    const-string/jumbo v2, "sendMessage"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    .line 112
    .local v0, "smsMgr":Landroid/telephony/SmsManager;
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 113
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getMsgBody()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->mSentPI:Landroid/app/PendingIntent;

    const/4 v5, 0x0

    .line 112
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    .end local v0    # "smsMgr":Landroid/telephony/SmsManager;
    :goto_0
    return-void

    .line 115
    :catch_0
    move-exception v6

    .line 116
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 118
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->sendRequestResult(Z)V

    .line 120
    const-string/jumbo v1, "MessageManager"

    const-string/jumbo v2, "sendMessage return false"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private sendRequestResult(Z)V
    .locals 9
    .param p1, "result"    # Z

    .prologue
    const/4 v3, 0x0

    .line 125
    const-string/jumbo v1, "MessageManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "sendRequestResult : result "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v8

    .line 129
    .local v8, "provider":Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    if-nez v8, :cond_0

    .line 130
    const-string/jumbo v1, "MessageManager"

    const-string/jumbo v2, "getMediator() null"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    :goto_0
    return-void

    .line 134
    :cond_0
    if-eqz p1, :cond_1

    .line 135
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;

    .line 136
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;->OUTGOING_TYPE:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    .line 137
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->LOG_TYPE_SMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->mPhoneNumber:Ljava/lang/String;

    .line 138
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    move-object v5, v3

    .line 135
    invoke-direct/range {v0 .. v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;-><init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 140
    .local v0, "calllog":Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 141
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->addDLCallMessageLogData(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)V

    .line 148
    .end local v0    # "calllog":Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;
    :cond_1
    :goto_1
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMessageListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 149
    const-string/jumbo v1, "MessageManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onResponseRequestSendMessage("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMessageListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;

    move-result-object v1

    .line 151
    invoke-interface {v1, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;->onResponseRequestSendMessage(Z)V

    goto :goto_0

    .line 144
    .restart local v0    # "calllog":Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;
    :cond_2
    const-string/jumbo v1, "MessageManager"

    const-string/jumbo v2, "getContentManager() null"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 153
    .end local v0    # "calllog":Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;
    :cond_3
    const-string/jumbo v1, "MessageManager"

    const-string/jumbo v2, "getOnDriveLinkMessageListener() null"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public initialize(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->initialize(Landroid/content/Context;)Z

    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->registerSendReceiver(Landroid/content/Context;)V

    .line 38
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->mContext:Landroid/content/Context;

    .line 40
    const/4 v0, 0x1

    return v0
.end method

.method public requestSendMessage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;

    .prologue
    .line 95
    const-string/jumbo v0, "MessageManager"

    const-string/jumbo v1, "requestSendMessage"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->mContext:Landroid/content/Context;

    .line 98
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->mPhoneNumber:Ljava/lang/String;

    .line 100
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->sendMessage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;)V

    .line 102
    return-void
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->mSentPI:Landroid/app/PendingIntent;

    .line 47
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->mMsgManagerReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->mMsgManagerReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 51
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->terminate(Landroid/content/Context;)V

    .line 52
    return-void
.end method
