.class public Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;
.super Ljava/lang/Object;
.source "NavigationUtils.java"


# static fields
.field public static APP_NAME_BAIDU_NAVI:I = 0x0

.field public static APP_NAME_CMCC_NAVI:I = 0x0

.field public static APP_NAME_GOOGLE_MAPS:I = 0x0

.field public static APP_NAME_OLLEH_NAVI:I = 0x0

.field public static APP_NAME_TMAP:I = 0x0

.field public static APP_NAME_UNAVI:I = 0x0

.field public static LANGUAGE_APP_NAME_BAIDU_NAVI:Ljava/lang/String; = null

.field public static LANGUAGE_APP_NAME_CMCC_NAVI:Ljava/lang/String; = null

.field public static LANGUAGE_APP_NAME_GOOGLE_MAPS:Ljava/lang/String; = null

.field public static LANGUAGE_APP_NAME_OLLEH_NAVI:Ljava/lang/String; = null

.field public static LANGUAGE_APP_NAME_TMAP:Ljava/lang/String; = null

.field public static LANGUAGE_APP_NAME_UNAVI:Ljava/lang/String; = null

.field public static final PCK_BAIDU_NAVI:Ljava/lang/String; = "com.baidu.BaiduMap"

.field public static final PCK_CMCC_NAVI:Ljava/lang/String; = "com.autonavi.cmccmap"

.field public static final PCK_GOOGLE_MAPS:Ljava/lang/String; = "com.google.android.apps.maps"

.field public static final PCK_OLLEH_NAVI:Ljava/lang/String; = "kt.navi"

.field public static final PCK_TMAP_91:Ljava/lang/String; = "com.skt.skaf.l001mtm091"

.field public static final PCK_TMAP_92:Ljava/lang/String; = "com.skt.skaf.l001mtm092"

.field public static final PCK_UNAVI:Ljava/lang/String; = "com.mnsoft.lgunavi"

.field public static final TMAP_DONWLOAD:Ljava/lang/String; = "http://m.tstore.co.kr/userpoc/mp.jsp?pid=0000163382"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 18
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_GOOGLE_MAPS:I

    .line 19
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_UNAVI:I

    .line 20
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_TMAP:I

    .line 21
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_BAIDU_NAVI:I

    .line 22
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_OLLEH_NAVI:I

    .line 23
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_CMCC_NAVI:I

    .line 25
    sput-object v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->LANGUAGE_APP_NAME_GOOGLE_MAPS:Ljava/lang/String;

    .line 26
    sput-object v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->LANGUAGE_APP_NAME_UNAVI:Ljava/lang/String;

    .line 27
    sput-object v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->LANGUAGE_APP_NAME_TMAP:Ljava/lang/String;

    .line 28
    sput-object v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->LANGUAGE_APP_NAME_BAIDU_NAVI:Ljava/lang/String;

    .line 29
    sput-object v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->LANGUAGE_APP_NAME_OLLEH_NAVI:Ljava/lang/String;

    .line 30
    sput-object v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->LANGUAGE_APP_NAME_CMCC_NAVI:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPackageNameChosen(I)Ljava/lang/String;
    .locals 2
    .param p0, "mapID"    # I

    .prologue
    .line 50
    packed-switch p0, :pswitch_data_0

    .line 68
    const-string/jumbo v0, "com.google.android.apps.maps"

    :goto_0
    return-object v0

    .line 52
    :pswitch_0
    const-string/jumbo v0, "com.google.android.apps.maps"

    goto :goto_0

    .line 54
    :pswitch_1
    const-string/jumbo v0, "com.mnsoft.lgunavi"

    goto :goto_0

    .line 56
    :pswitch_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "com.skt.skaf.l001mtm091"

    .line 56
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 57
    if-eqz v0, :cond_0

    .line 58
    const-string/jumbo v0, "com.skt.skaf.l001mtm091"

    goto :goto_0

    .line 60
    :cond_0
    const-string/jumbo v0, "com.skt.skaf.l001mtm092"

    goto :goto_0

    .line 62
    :pswitch_3
    const-string/jumbo v0, "com.baidu.BaiduMap"

    goto :goto_0

    .line 64
    :pswitch_4
    const-string/jumbo v0, "kt.navi"

    goto :goto_0

    .line 66
    :pswitch_5
    const-string/jumbo v0, "com.autonavi.cmccmap"

    goto :goto_0

    .line 50
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static getResourcesID()I
    .locals 5

    .prologue
    const v1, 0x7f0a03f6

    .line 185
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 184
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v2

    .line 186
    const-string/jumbo v3, "PREF_SETTINGS_MY_NAVIGATION"

    const/4 v4, 0x0

    .line 185
    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v0

    .line 187
    .local v0, "res":I
    packed-switch v0, :pswitch_data_0

    .line 201
    :goto_0
    :pswitch_0
    return v1

    .line 191
    :pswitch_1
    const v1, 0x7f0a03fc

    goto :goto_0

    .line 193
    :pswitch_2
    const v1, 0x7f0a03f9

    goto :goto_0

    .line 195
    :pswitch_3
    const v1, 0x7f0a03fb

    goto :goto_0

    .line 197
    :pswitch_4
    const v1, 0x7f0a03fa

    goto :goto_0

    .line 199
    :pswitch_5
    const v1, 0x7f0a03fd

    goto :goto_0

    .line 187
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static getSalesCodePropertyForNavi()Ljava/lang/String;
    .locals 2

    .prologue
    .line 127
    const-string/jumbo v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 129
    .local v0, "salesCode":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 130
    const-string/jumbo v0, "OTHERS"

    .line 136
    :cond_0
    :goto_0
    return-object v0

    .line 132
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 133
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->isKoreanPhone(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->isChinesePhone(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 134
    const-string/jumbo v0, "OTHERS"

    goto :goto_0
.end method

.method public static getUserCountryForNavi()Ljava/lang/String;
    .locals 2

    .prologue
    .line 114
    const-string/jumbo v1, "ro.csc.countryiso_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 116
    .local v0, "country":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 117
    const-string/jumbo v0, "OTHERS"

    .line 123
    :cond_0
    :goto_0
    return-object v0

    .line 119
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 120
    const-string/jumbo v1, "KR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 121
    const-string/jumbo v0, "OTHERS"

    goto :goto_0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 167
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 168
    const v1, 0x7f0a03f6

    .line 167
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->LANGUAGE_APP_NAME_GOOGLE_MAPS:Ljava/lang/String;

    .line 169
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 170
    const v1, 0x7f0a03fc

    .line 169
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->LANGUAGE_APP_NAME_UNAVI:Ljava/lang/String;

    .line 171
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 172
    const v1, 0x7f0a03f9

    .line 171
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->LANGUAGE_APP_NAME_TMAP:Ljava/lang/String;

    .line 173
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 174
    const v1, 0x7f0a03fb

    .line 173
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->LANGUAGE_APP_NAME_BAIDU_NAVI:Ljava/lang/String;

    .line 175
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 176
    const v1, 0x7f0a03fa

    .line 175
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->LANGUAGE_APP_NAME_OLLEH_NAVI:Ljava/lang/String;

    .line 177
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 178
    const v1, 0x7f0a03fd

    .line 177
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->LANGUAGE_APP_NAME_CMCC_NAVI:Ljava/lang/String;

    .line 180
    return-void
.end method

.method public static isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 74
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v0, 0x0

    .line 76
    .local v0, "app_installed":Z
    const/4 v3, 0x1

    :try_start_0
    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    const/4 v0, 0x1

    .line 81
    :goto_0
    return v0

    .line 78
    :catch_0
    move-exception v1

    .line 79
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isChinesePhone(Ljava/lang/String;)Z
    .locals 1
    .param p0, "salesCode"    # Ljava/lang/String;

    .prologue
    .line 146
    const-string/jumbo v0, "CHN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "CHM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    const-string/jumbo v0, "CHU"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "CHC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    const-string/jumbo v0, "CTC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    :cond_0
    const/4 v0, 0x1

    .line 151
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isKoreanPhone(Ljava/lang/String;)Z
    .locals 1
    .param p0, "salesCode"    # Ljava/lang/String;

    .prologue
    .line 155
    const-string/jumbo v0, "SKT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "SKC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 156
    const-string/jumbo v0, "SKO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "KTT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 157
    const-string/jumbo v0, "KTC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "KTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 158
    const-string/jumbo v0, "LGT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "LUC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    const-string/jumbo v0, "LUO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    :cond_0
    const/4 v0, 0x1

    .line 162
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPackageInstalled(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pckgName"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 45
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static numberInstalledNavigationApps()I
    .locals 8

    .prologue
    .line 85
    const/4 v0, 0x0

    .line 87
    .local v0, "count":I
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->getUserCountryForNavi()Ljava/lang/String;

    move-result-object v6

    .line 88
    .local v6, "mUserCountry":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->getSalesCodePropertyForNavi()Ljava/lang/String;

    move-result-object v5

    .line 90
    .local v5, "mSalesProperty":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 92
    .local v1, "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-interface {v1, v6, v5}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestMapAvailableByCountryAndSaleCode(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 94
    .local v3, "mListNavitationAvailable":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v3, :cond_1

    .line 110
    :cond_0
    return v0

    .line 96
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    if-ge v2, v7, :cond_0

    .line 97
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 98
    .local v4, "mPackage":Ljava/lang/String;
    const-string/jumbo v7, "com.google.android.apps.maps"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 99
    add-int/lit8 v0, v0, 0x1

    .line 100
    :cond_2
    const-string/jumbo v7, "com.mnsoft.lgunavi"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 101
    add-int/lit8 v0, v0, 0x1

    .line 102
    :cond_3
    const-string/jumbo v7, "com.skt.skaf.l001mtm091"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    const-string/jumbo v7, "com.skt.skaf.l001mtm092"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 103
    :cond_4
    add-int/lit8 v0, v0, 0x1

    .line 104
    :cond_5
    const-string/jumbo v7, "kt.navi"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 105
    add-int/lit8 v0, v0, 0x1

    .line 106
    :cond_6
    const-string/jumbo v7, "com.baidu.BaiduMap"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 107
    add-int/lit8 v0, v0, 0x1

    .line 96
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
