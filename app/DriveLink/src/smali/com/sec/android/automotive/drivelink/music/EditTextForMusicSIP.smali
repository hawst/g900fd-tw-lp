.class public Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP;
.super Landroid/widget/EditText;
.source "EditTextForMusicSIP.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;,
        Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$OnMusicSipStateListener;
    }
.end annotation


# instance fields
.field mMusicTextViewStateListener:Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$OnMusicSipStateListener;

.field mcontext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 19
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP;->mcontext:Landroid/content/Context;

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP;->mcontext:Landroid/content/Context;

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP;->mcontext:Landroid/content/Context;

    .line 30
    return-void
.end method


# virtual methods
.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 35
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 36
    invoke-super {p0, p1}, Landroid/widget/EditText;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 48
    :goto_0
    return v0

    .line 41
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 42
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;->SIP_CLOSE:Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP;->setMusicStatechanged(Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;)V

    .line 48
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/EditText;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setMusicStatechanged(Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;)V
    .locals 1
    .param p1, "state"    # Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP;->mMusicTextViewStateListener:Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$OnMusicSipStateListener;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP;->mMusicTextViewStateListener:Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$OnMusicSipStateListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$OnMusicSipStateListener;->OnStateChanged(Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$MusicSIPState;)V

    .line 58
    :cond_0
    return-void
.end method

.method public setOnMusicSipStateListener(Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$OnMusicSipStateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$OnMusicSipStateListener;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP;->mMusicTextViewStateListener:Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$OnMusicSipStateListener;

    .line 53
    return-void
.end method
