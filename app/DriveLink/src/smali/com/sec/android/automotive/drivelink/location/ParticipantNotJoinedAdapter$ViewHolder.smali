.class Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "ParticipantNotJoinedAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolder"
.end annotation


# instance fields
.field private imgUser:Landroid/widget/ImageView;

.field private tvLetterUser:Landroid/widget/TextView;

.field private tvName:Landroid/widget/TextView;

.field private tvNotJoined:Landroid/widget/TextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;)V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;-><init>()V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->imgUser:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->tvLetterUser:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public getImgUser()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->imgUser:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getTvName()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    return-object v0
.end method

.method public getTvNotJoined()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->tvNotJoined:Landroid/widget/TextView;

    return-object v0
.end method

.method public setImgUser(Landroid/widget/ImageView;)V
    .locals 0
    .param p1, "imgUser"    # Landroid/widget/ImageView;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->imgUser:Landroid/widget/ImageView;

    .line 145
    return-void
.end method

.method public setTvLetterUser(Landroid/widget/TextView;)V
    .locals 0
    .param p1, "tvLetterUser"    # Landroid/widget/TextView;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->tvLetterUser:Landroid/widget/TextView;

    .line 157
    return-void
.end method

.method public setTvName(Landroid/widget/TextView;)V
    .locals 0
    .param p1, "tvName"    # Landroid/widget/TextView;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    .line 153
    return-void
.end method

.method public setTvNotJoined(Landroid/widget/TextView;)V
    .locals 0
    .param p1, "tvNotJoined"    # Landroid/widget/TextView;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->tvNotJoined:Landroid/widget/TextView;

    .line 165
    return-void
.end method
