.class public Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastType;
.super Ljava/lang/Object;
.source "ConnectivityManagerBroadcastType.java"


# static fields
.field public static final BLUETOOTH_ACL_CONNECTED:I = 0x5

.field public static final BLUETOOTH_ACL_DISCONNECTED:I = 0x6

.field public static final BLUETOOTH_ACTION_FOUND:I = 0x9

.field public static final BLUETOOTH_BOND_STATE_BONDED:I = 0x3

.field public static final BLUETOOTH_BOND_STATE_BONDING:I = 0x2

.field public static final BLUETOOTH_BOND_STATE_NONE:I = 0x4

.field public static final BLUETOOTH_FORCE_RECOVERY:I = 0xa

.field public static final BLUETOOTH_PAIRING_REQUEST:I = 0x1

.field public static final BLUETOOTH_STATE_CONNECTED:I = 0x7

.field public static final BLUETOOTH_STATE_DISCONNECTED:I = 0x8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
