.class public Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "SVoiceLaunchPopupActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;)V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;->launchSVoiceForTosAcceptance()V

    return-void
.end method

.method private finishApp()V
    .locals 2

    .prologue
    .line 149
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 151
    .local v0, "finishintent":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.automotive.drivelink.carmodeoff.finish"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    const-string/jumbo v1, "com.sec.android.automotive.drivelink"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 154
    return-void
.end method

.method private launchSVoiceForTosAcceptance()V
    .locals 4

    .prologue
    .line 118
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mIsLauchSVoice:Z

    .line 119
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 120
    .local v1, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/content/ComponentName;

    .line 121
    const-string/jumbo v2, "com.vlingo.midas"

    const-string/jumbo v3, "com.vlingo.midas.gui.ConversationActivity"

    .line 120
    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    .local v0, "componentName":Landroid/content/ComponentName;
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 123
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 132
    const-string/jumbo v2, "drivelink"

    .line 133
    const/4 v3, 0x1

    .line 131
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 135
    const-string/jumbo v2, "launch_after_terms_of_service_acceptance_package"

    .line 136
    const-string/jumbo v3, "com.sec.android.automotive.drivelink"

    .line 134
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    const-string/jumbo v2, "launch_aafter_terms_of_service_acceptance_activity"

    .line 139
    const-string/jumbo v3, "com.sec.android.automotive.drivelink.firstaccess.SplashScreenActivity"

    .line 137
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 140
    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;->startActivity(Landroid/content/Intent;)V

    .line 144
    const-string/jumbo v2, "former_tos_acceptance_state"

    .line 145
    const-string/jumbo v3, "reminder_not_needed"

    .line 143
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 111
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onBackPressed()V

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;->finish()V

    .line 114
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->terminate()V

    .line 115
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    const v2, 0x7f030039

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;->setContentView(I)V

    .line 28
    new-instance v1, Landroid/app/AlertDialog$Builder;

    .line 29
    const/4 v2, 0x4

    .line 28
    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 30
    .local v1, "dialog":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0a0286

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 31
    const v3, 0x7f0a0287

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 32
    new-instance v4, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity$1;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;)V

    .line 31
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 47
    const v3, 0x7f0a062f

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 48
    new-instance v4, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity$2;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;)V

    .line 47
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 67
    new-instance v2, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 88
    new-instance v2, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 97
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 98
    .local v0, "alert":Landroid/app/AlertDialog;
    const v2, 0x7f0a0257

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 99
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 101
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 105
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onResume()V

    .line 106
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mIsLauchSVoice:Z

    .line 107
    return-void
.end method
