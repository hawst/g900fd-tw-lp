.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
.source "DLLocationFriend.java"


# static fields
.field private static final serialVersionUID:J = 0x211fc697771a23b9L


# instance fields
.field private mSharedDate:Ljava/util/Date;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->FRIENDS_SHARED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;)V

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;->mSharedDate:Ljava/util/Date;

    .line 13
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;->mSharedDate:Ljava/util/Date;

    .line 14
    return-void
.end method

.method private removeCountryCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 67
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 68
    :cond_0
    const-string/jumbo v0, ""

    .line 69
    :goto_0
    return-object v0

    :cond_1
    const-string/jumbo v0, "(\\+[0-9][0-9])"

    const-string/jumbo v1, "0"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 44
    if-nez p1, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v1

    .line 46
    :cond_1
    instance-of v2, p1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 49
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;

    .line 51
    .local v0, "other":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 54
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getFriendName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;->getLocationTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSharedAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;->getLocationDescription()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSharedDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;->mSharedDate:Ljava/util/Date;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public setFriendName(Ljava/lang/String;)V
    .locals 0
    .param p1, "friendName"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;->setLocationTitle(Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method public setSharedAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;->setLocationDescription(Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method public setSharedDate(Ljava/util/Date;)V
    .locals 0
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;->mSharedDate:Ljava/util/Date;

    .line 40
    return-void
.end method
