.class Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;
.super Ljava/util/HashMap;
.source "FlowSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Class",
        "<*>;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 349
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 352
    const-string/jumbo v0, "DM_MUSIC_PLAYER_COMMAND"

    const-class v1, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    const-string/jumbo v0, "DM_RETURN_MAIN"

    const-class v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    const-string/jumbo v0, "DM_CANCEL"

    const-class v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    const-string/jumbo v0, "DM_CONFIRM_YES"

    const-class v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    const-string/jumbo v0, "DM_CONFIRM_NO"

    const-class v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    const-string/jumbo v0, "DM_CONFIRM_SEND"

    const-class v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    const-string/jumbo v0, "DM_CONFIRM_EXCUTE"

    const-class v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    const-string/jumbo v0, "DM_CONFIRM_IGNORE"

    const-class v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    const-string/jumbo v0, "DM_CONFIRM_LOOKUP"

    const-class v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    const-string/jumbo v0, "DM_CONFIRM_RESET"

    const-class v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 363
    const-string/jumbo v0, "DM_CONFIRM_ROUTE"

    const-class v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    const-string/jumbo v0, "DM_CONFIRM_ROUTE_NOP"

    const-class v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    const-string/jumbo v0, "DM_REPLY"

    const-class v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 367
    const-string/jumbo v0, "DM_CALL"

    const-class v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 368
    const-string/jumbo v0, "DM_READ"

    const-class v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    const-string/jumbo v0, "DM_NEXT"

    const-class v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 371
    const-string/jumbo v0, "DM_STOP_NAVIGATION"

    const-class v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    const-string/jumbo v0, "DM_LIST_SELECTED"

    const-class v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    const-string/jumbo v0, "DM_LIST_ORDINAL"

    const-class v1, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1
    return-void
.end method
