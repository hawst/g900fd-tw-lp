.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestGasStation;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestGasStation.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "RequestGasStation"


# instance fields
.field private mDLLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

.field private mOrder:I

.field private mProductTypes:I

.field private mRadius:D


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;DII)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p4, "radius"    # D
    .param p6, "productTypes"    # I
    .param p7, "order"    # I

    .prologue
    const/4 v2, -0x1

    .line 21
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGasStation;->mDLLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 14
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGasStation;->mRadius:D

    .line 15
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGasStation;->mProductTypes:I

    .line 16
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGasStation;->mOrder:I

    .line 23
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGasStation;->mDLLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 24
    iput p6, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGasStation;->mProductTypes:I

    .line 25
    iput-wide p4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGasStation;->mRadius:D

    .line 26
    iput p7, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGasStation;->mOrder:I

    .line 27
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGasStation;->mDLLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 57
    return-void
.end method

.method protected process()V
    .locals 4

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestGasStation;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 33
    .local v1, "service":Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    if-nez v1, :cond_1

    .line 34
    const-string/jumbo v2, "RequestGasStation"

    const-string/jumbo v3, "[stop] DriveLinkServiceProvider is null."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    :cond_0
    :goto_0
    return-void

    .line 38
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v0

    .line 40
    .local v0, "manager":Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;
    if-nez v0, :cond_0

    .line 41
    const-string/jumbo v2, "RequestGasStation"

    const-string/jumbo v3, "[stop] RecommendationManager is null."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected response()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method
