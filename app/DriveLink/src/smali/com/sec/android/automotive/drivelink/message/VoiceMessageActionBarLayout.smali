.class public Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;
.super Landroid/widget/RelativeLayout;
.source "VoiceMessageActionBarLayout.java"

# interfaces
.implements Lcom/nuance/drivelink/DLUiUpdater;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$SineEaseOut;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I

.field private static final SINE_OUT:Landroid/view/animation/Interpolator;

.field private static TAG:Ljava/lang/String;

.field private static isDebug:Z


# instance fields
.field private isRestore:Z

.field private isRunning:Z

.field private mAnimator:Landroid/view/ViewPropertyAnimator;

.field private mBackBtn:Landroid/widget/LinearLayout;

.field private mBackBtnImage:Landroid/widget/ImageView;

.field private mBgView:Landroid/widget/ImageView;

.field private mContext:Landroid/content/Context;

.field private mCurrentMicState:Lcom/nuance/sample/MicState;

.field private mCurrentMode:I

.field private mDialogLayout:Landroid/widget/RelativeLayout;

.field private mDirection:Z

.field private mErrorCharSequence:Ljava/lang/CharSequence;

.field private mHiText:Landroid/widget/TextView;

.field private mIsErrorState:Z

.field private mIsFirstSystemTurn:Z

.field private mIsMicDisplayed:Z

.field private mIsPhraseSotting:Z

.field private mIsStartQEnded:Z

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mListeningText:Landroid/widget/TextView;

.field private mMicBtn:Landroid/widget/ImageView;

.field private mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

.field private mMicLayout:Landroid/widget/RelativeLayout;

.field private mMicProcessBg:Landroid/widget/ImageView;

.field private mMicStartQBg:Landroid/widget/ImageView;

.field private mNoNetworkLayout:Landroid/widget/LinearLayout;

.field private mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

.field private mOnVoiceMessageActionBarListener:Lcom/sec/android/automotive/drivelink/message/OnVoiceMessageActionBarListener;

.field private mPhraseSpotterHandler:Landroid/os/Handler;

.field private mProcessText:Landroid/widget/TextView;

.field private mQuoteEnd:Landroid/widget/ImageView;

.field private mQuoteStart:Landroid/widget/ImageView;

.field private mRatio:F

.field private mSayText:Landroid/widget/TextView;

.field private mSearchBtn:Landroid/widget/ImageButton;

.field private mStandByLayout:Landroid/widget/LinearLayout;

.field private mStartQAni:Landroid/view/animation/Animation;

.field private mTTSBar:Landroid/widget/ImageView;

.field private mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

.field private mTTSBarLayout:Landroid/widget/RelativeLayout;

.field private mTTSText:Landroid/widget/TextView;

.field private mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

.field private mTitle:Landroid/widget/TextView;

.field private mVoiceLayout:Landroid/widget/RelativeLayout;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string/jumbo v0, "VoiceMessageActionBarLayout"

    sput-object v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->TAG:Ljava/lang/String;

    .line 42
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->isDebug:Z

    .line 510
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$SineEaseOut;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$SineEaseOut;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 102
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 46
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsPhraseSotting:Z

    .line 512
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mRatio:F

    .line 514
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->isRestore:Z

    .line 577
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mDirection:Z

    .line 578
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->isRunning:Z

    .line 752
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    .line 103
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->init(Landroid/content/Context;)V

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 107
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsPhraseSotting:Z

    .line 512
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mRatio:F

    .line 514
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->isRestore:Z

    .line 577
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mDirection:Z

    .line 578
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->isRunning:Z

    .line 752
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    .line 108
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->init(Landroid/content/Context;)V

    .line 109
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 113
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsPhraseSotting:Z

    .line 512
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mRatio:F

    .line 514
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->isRestore:Z

    .line 577
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mDirection:Z

    .line 578
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->isRunning:Z

    .line 752
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    .line 114
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->init(Landroid/content/Context;)V

    .line 115
    return-void
.end method

.method private static Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "level"    # Ljava/lang/String;
    .param p1, "sub"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 97
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->isDebug:Z

    if-eqz v0, :cond_0

    .line 98
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->TAG:Ljava/lang/String;

    invoke-static {p0, v0, p1, p2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsMicDisplayed:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$10()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 510
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;FJLandroid/view/animation/Interpolator;)V
    .locals 0

    .prologue
    .line 516
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->startScaleAnimation(FJLandroid/view/animation/Interpolator;)V

    return-void
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)Z
    .locals 1

    .prologue
    .line 578
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->isRunning:Z

    return v0
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)Z
    .locals 1

    .prologue
    .line 577
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mDirection:Z

    return v0
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 577
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mDirection:Z

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)V
    .locals 0

    .prologue
    .line 581
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->runTTSBarAnimation()V

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 578
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->isRunning:Z

    return-void
.end method

.method static synthetic access$17(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSBar:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)Lcom/sec/android/automotive/drivelink/common/view/ListeningView;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsStartQEnded:Z

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)Z
    .locals 1

    .prologue
    .line 514
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->isRestore:Z

    return v0
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 514
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->isRestore:Z

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mContext:Landroid/content/Context;

    .line 119
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 120
    .local v0, "inflater":Landroid/view/LayoutInflater;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;->isListening()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsPhraseSotting:Z

    .line 121
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsFirstSystemTurn:Z

    .line 123
    const v1, 0x7f0300d7

    .line 122
    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 125
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 126
    const v2, 0x7f090362

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 125
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    .line 127
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mContext:Landroid/content/Context;

    .line 128
    const v4, 0x7f0a0250

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 129
    const-string/jumbo v3, ". "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 130
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mContext:Landroid/content/Context;

    const v4, 0x7f0a03f4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 127
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 132
    const v2, 0x7f090363

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 131
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mBackBtnImage:Landroid/widget/ImageView;

    .line 133
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090364

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTitle:Landroid/widget/TextView;

    .line 146
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 147
    const v2, 0x7f09034e

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 146
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    .line 148
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 149
    const v2, 0x7f09034f

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 148
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mDialogLayout:Landroid/widget/RelativeLayout;

    .line 151
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 152
    const v2, 0x7f090350

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 151
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    .line 153
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 154
    const v2, 0x7f09031c

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 153
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    .line 155
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090351

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSText:Landroid/widget/TextView;

    .line 156
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 157
    const v2, 0x7f090352

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 156
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    .line 158
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 159
    const v2, 0x7f09035a

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 158
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mListeningText:Landroid/widget/TextView;

    .line 160
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 161
    const v2, 0x7f09035b

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 160
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mProcessText:Landroid/widget/TextView;

    .line 162
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 163
    const v2, 0x7f09035c

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 162
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    .line 165
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090356

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    .line 166
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 167
    const v2, 0x7f090357

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    .line 166
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    .line 168
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090353

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    .line 169
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 170
    const v2, 0x7f090354

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 169
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    .line 171
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 172
    const v2, 0x7f090355

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 171
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    .line 174
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090336

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mSayText:Landroid/widget/TextView;

    .line 175
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090338

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mHiText:Landroid/widget/TextView;

    .line 176
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 177
    const v2, 0x7f090337

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 176
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mQuoteStart:Landroid/widget/ImageView;

    .line 178
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090339

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mQuoteEnd:Landroid/widget/ImageView;

    .line 180
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 181
    const v2, 0x7f090358

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 180
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    .line 182
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 183
    const v2, 0x7f090359

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 182
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSBar:Landroid/widget/ImageView;

    .line 185
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mContext:Landroid/content/Context;

    .line 186
    const v2, 0x7f04001e

    .line 185
    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    .line 187
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    new-instance v2, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$2;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$2;-><init>(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 205
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mHiText:Landroid/widget/TextView;

    .line 206
    const-string/jumbo v2, "/system/fonts/Cooljazz.ttf"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->createTypefaceFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    .line 205
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 208
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->isMicDisplayed()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsMicDisplayed:Z

    .line 210
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f09034d

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mBgView:Landroid/widget/ImageView;

    .line 212
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/nuance/sample/OnClickMicListenerImpl;

    .line 213
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-direct {v2, v3, v4}, Lcom/nuance/sample/OnClickMicListenerImpl;-><init>(Lcom/nuance/sample/MicStateMaster;Landroid/view/View;)V

    .line 212
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mOnVoiceMessageActionBarListener:Lcom/sec/android/automotive/drivelink/message/OnVoiceMessageActionBarListener;

    .line 217
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->setHasInternetLayout(Z)V

    .line 218
    return-void
.end method

.method private runTTSBarAnimation()V
    .locals 5

    .prologue
    const-wide/16 v3, 0x2bc

    .line 582
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    if-nez v0, :cond_0

    .line 583
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSBar:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    .line 584
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$4;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$4;-><init>(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 612
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$5;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$5;-><init>(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)Landroid/view/ViewPropertyAnimator;

    .line 623
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mDirection:Z

    if-eqz v0, :cond_1

    .line 624
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mContext:Landroid/content/Context;

    const/high16 v2, 0x428c0000    # 70.0f

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->dipToPixels(Landroid/content/Context;F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 625
    invoke-virtual {v0, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 629
    :goto_0
    return-void

    .line 627
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method private startScaleAnimation(FJLandroid/view/animation/Interpolator;)V
    .locals 2
    .param p1, "ratio"    # F
    .param p2, "duration"    # J
    .param p4, "interpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 519
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    if-nez v0, :cond_0

    .line 520
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setScaleX(F)V

    .line 521
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setScaleY(F)V

    .line 523
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    .line 524
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$3;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout$3;-><init>(Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 552
    :cond_0
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mRatio:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 560
    :goto_0
    return-void

    .line 555
    :cond_1
    iput p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mRatio:F

    .line 557
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 558
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 559
    sget-object v1, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method private startTTSBarAnimation()V
    .locals 1

    .prologue
    .line 632
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->isRunning:Z

    .line 633
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->runTTSBarAnimation()V

    .line 634
    return-void
.end method


# virtual methods
.method public displayError(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 275
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 278
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mErrorCharSequence:Ljava/lang/CharSequence;

    .line 280
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsErrorState:Z

    .line 281
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsStartQEnded:Z

    .line 282
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 283
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->stopTTSBarAnimation()V

    .line 284
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 285
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 286
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 290
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 291
    return-void
.end method

.method public displaySystemTurn(Ljava/lang/CharSequence;)V
    .locals 4
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 301
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 303
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 304
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 305
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 306
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 311
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "displaySystemTurn mTTSBarLayout setVisible"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 315
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 317
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 319
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 320
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02038c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 322
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->startTTSBarAnimation()V

    .line 333
    return-void
.end method

.method public displayUserTurn(Ljava/lang/CharSequence;)V
    .locals 4
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 296
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "displayUserTurn "

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "cs = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    return-void
.end method

.method public displayWidgetContent(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 339
    return-void
.end method

.method protected getErrorCharSequence()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mErrorCharSequence:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 1

    .prologue
    .line 655
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMicState()Lcom/nuance/sample/MicState;
    .locals 4

    .prologue
    .line 442
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "getMicState "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    return-object v0
.end method

.method public handleUserCancel()V
    .locals 0

    .prologue
    .line 650
    return-void
.end method

.method public hideVoiceLayout()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 677
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 678
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 679
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 680
    return-void
.end method

.method protected isErrorState()Z
    .locals 1

    .prologue
    .line 468
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsErrorState:Z

    return v0
.end method

.method protected isTTSState()Z
    .locals 1

    .prologue
    .line 476
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->isRunning:Z

    return v0
.end method

.method public onClickable(Z)V
    .locals 1
    .param p1, "isClickable"    # Z

    .prologue
    .line 454
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 455
    return-void
.end method

.method public onDisplayMic(Z)V
    .locals 4
    .param p1, "isMicDisplayed"    # Z

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 773
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsMicDisplayed:Z

    .line 779
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    if-ne v0, v1, :cond_0

    .line 780
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsMicDisplayed:Z

    if-eqz v0, :cond_1

    .line 781
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsErrorState:Z

    .line 782
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsStartQEnded:Z

    .line 783
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 784
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 786
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 787
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 788
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 789
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 790
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 791
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 793
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 794
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 795
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 796
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 797
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 798
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02038c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 814
    :cond_0
    :goto_0
    return-void

    .line 800
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsErrorState:Z

    .line 801
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsStartQEnded:Z

    .line 802
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 803
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 805
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 806
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 807
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 808
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 809
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 810
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 811
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onPhraseSpotterStateChanged(Z)V
    .locals 3
    .param p1, "isSpotting"    # Z

    .prologue
    .line 739
    const-string/jumbo v0, "UiUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " - PhraseSpotting : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 740
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 739
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 742
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsPhraseSotting:Z

    .line 750
    return-void
.end method

.method public setCurrentMode(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    const v3, 0x7f0a024a

    const v2, 0x7f0a020f

    .line 237
    iput p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mCurrentMode:I

    .line 239
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mCurrentMode:I

    const/16 v1, 0x41

    if-ne v0, v1, :cond_0

    .line 240
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 271
    :goto_0
    return-void

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method protected setDayMode()V
    .locals 4

    .prologue
    const v1, 0x7f020001

    const v3, 0x7f080031

    .line 723
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mBgView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 724
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 725
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mSayText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 726
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mHiText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080033

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 727
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mQuoteStart:Landroid/widget/ImageView;

    const v1, 0x7f02035b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 728
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mQuoteEnd:Landroid/widget/ImageView;

    const v1, 0x7f020359

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 729
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 730
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 732
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 734
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mBackBtnImage:Landroid/widget/ImageView;

    const v1, 0x7f020044

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 735
    return-void
.end method

.method public setHasInternetLayout(Z)V
    .locals 3
    .param p1, "hasInternet"    # Z

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 222
    if-eqz p1, :cond_0

    .line 224
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 234
    :goto_0
    return-void

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 232
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected setMicDisplayed(Z)V
    .locals 0
    .param p1, "isMicDisplayed"    # Z

    .prologue
    .line 817
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsMicDisplayed:Z

    .line 818
    return-void
.end method

.method protected setNightMode()V
    .locals 3

    .prologue
    const v1, 0x7f08004e

    const v2, 0x7f080030

    .line 705
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mBgView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 706
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 707
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mSayText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 709
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mHiText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 711
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mQuoteStart:Landroid/widget/ImageView;

    const v1, 0x7f02035e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 712
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mQuoteEnd:Landroid/widget/ImageView;

    const v1, 0x7f02035d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 713
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 715
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 717
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 719
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mBackBtnImage:Landroid/widget/ImageView;

    const v1, 0x7f020045

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 720
    return-void
.end method

.method public setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 3
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 462
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "setOnBackBtnClickListener"

    const-string/jumbo v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 465
    return-void
.end method

.method public setOnMicStateChangeListener(Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;)V
    .locals 3
    .param p1, "l"    # Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .prologue
    .line 448
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "setOnMicStateChangeListener "

    const-string/jumbo v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .line 450
    return-void
.end method

.method public setOnVoiceMessageActionBarListener(Lcom/sec/android/automotive/drivelink/message/OnVoiceMessageActionBarListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/automotive/drivelink/message/OnVoiceMessageActionBarListener;

    .prologue
    .line 768
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mOnVoiceMessageActionBarListener:Lcom/sec/android/automotive/drivelink/message/OnVoiceMessageActionBarListener;

    .line 769
    return-void
.end method

.method protected setTTSState()V
    .locals 1

    .prologue
    .line 480
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->displaySystemTurn(Ljava/lang/CharSequence;)V

    .line 481
    return-void
.end method

.method public showVoiceLayout()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 659
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 660
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 661
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 662
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 668
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 669
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 670
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 671
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 672
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 673
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 674
    return-void

    .line 665
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 666
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected stopTTSBarAnimation()V
    .locals 1

    .prologue
    .line 637
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_0

    .line 638
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 640
    :cond_0
    return-void
.end method

.method public updateMicRMSChange(I)V
    .locals 1
    .param p1, "rmsValue"    # I

    .prologue
    .line 488
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsStartQEnded:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->update(I)V

    .line 491
    :cond_0
    return-void
.end method

.method public updateMicState(Lcom/nuance/sample/MicState;)V
    .locals 5
    .param p1, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 343
    const-string/jumbo v0, "UiUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    .line 346
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    if-nez v0, :cond_2

    .line 347
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 433
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mOnVoiceMessageActionBarListener:Lcom/sec/android/automotive/drivelink/message/OnVoiceMessageActionBarListener;

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mOnVoiceMessageActionBarListener:Lcom/sec/android/automotive/drivelink/message/OnVoiceMessageActionBarListener;

    .line 436
    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    .line 434
    invoke-interface {v0, v4, v1}, Lcom/sec/android/automotive/drivelink/message/OnVoiceMessageActionBarListener;->onVoiceMessageActionBarUpdate(II)V

    .line 438
    :cond_0
    return-void

    .line 349
    :pswitch_0
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsErrorState:Z

    .line 350
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsStartQEnded:Z

    .line 351
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 352
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->stopTTSBarAnimation()V

    .line 353
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 360
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsMicDisplayed:Z

    if-eqz v0, :cond_1

    .line 361
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 362
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 363
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 364
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 365
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 366
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 367
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 368
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02038c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 374
    :goto_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 375
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 376
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 377
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 380
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSText:Landroid/widget/TextView;

    const v1, 0x7f0a020f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 385
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    goto :goto_0

    .line 370
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 371
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x2bc

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 389
    :pswitch_1
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsErrorState:Z

    .line 390
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 391
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->stopTTSBarAnimation()V

    .line 392
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 393
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 394
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 395
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 396
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 397
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 399
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 400
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 401
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 402
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 403
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 405
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020385

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 406
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020388

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 407
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 411
    :pswitch_2
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsErrorState:Z

    .line 412
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mIsStartQEnded:Z

    .line 413
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->stopTTSBarAnimation()V

    .line 414
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 415
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 416
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 417
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 418
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 419
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 421
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 422
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 423
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 424
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 425
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 426
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->play(I)V

    goto/16 :goto_0

    .line 430
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    invoke-interface {v0, p0, p1}, Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;->onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V

    goto/16 :goto_0

    .line 347
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
