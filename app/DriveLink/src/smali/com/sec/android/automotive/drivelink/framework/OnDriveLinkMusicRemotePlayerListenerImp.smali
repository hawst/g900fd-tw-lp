.class public Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;
.super Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkBaseListenerImp;
.source "OnDriveLinkMusicRemotePlayerListenerImp.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;


# instance fields
.field private mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkBaseListenerImp;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    return-object v0
.end method


# virtual methods
.method public onMusicRemotePlayerErrorStop()V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    if-nez v0, :cond_0

    .line 215
    :goto_0
    return-void

    .line 206
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$11;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$11;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onMusicRemotePlayerFForward(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    if-nez v0, :cond_0

    .line 122
    :goto_0
    return-void

    .line 113
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$6;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$6;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onMusicRemotePlayerNext()V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    if-nez v0, :cond_0

    .line 158
    :goto_0
    return-void

    .line 149
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$8;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$8;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onMusicRemotePlayerPause()V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    if-nez v0, :cond_0

    .line 69
    :goto_0
    return-void

    .line 60
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$3;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$3;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onMusicRemotePlayerPlay()V
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    if-nez v0, :cond_0

    .line 33
    :goto_0
    return-void

    .line 24
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onMusicRemotePlayerPlayPause()V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    if-nez v0, :cond_0

    .line 86
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$4;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$4;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onMusicRemotePlayerPrevious()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    if-nez v0, :cond_0

    .line 140
    :goto_0
    return-void

    .line 131
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$7;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$7;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onMusicRemotePlayerRewind(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    if-nez v0, :cond_0

    .line 104
    :goto_0
    return-void

    .line 95
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$5;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$5;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onMusicRemotePlayerShuffle(Z)V
    .locals 3
    .param p1, "bShuffle"    # Z

    .prologue
    .line 182
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    if-nez v1, :cond_0

    .line 197
    :goto_0
    return-void

    .line 186
    :cond_0
    move v0, p1

    .line 188
    .local v0, "_bShuffle":Z
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$10;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$10;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;Z)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onMusicRemotePlayerStop()V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    if-nez v0, :cond_0

    .line 51
    :goto_0
    return-void

    .line 42
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$2;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onMusicRemotePositionUpdate(I)V
    .locals 3
    .param p1, "newPositionMs"    # I

    .prologue
    .line 163
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    if-nez v1, :cond_0

    .line 178
    :goto_0
    return-void

    .line 167
    :cond_0
    move v0, p1

    .line 169
    .local v0, "_newPositionMs":I
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$9;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$9;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public setDriveLinkListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;

    .prologue
    .line 14
    check-cast p1, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    .end local p1    # "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    .line 15
    return-void
.end method
