.class public final Lcom/sec/android/automotive/drivelink/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final action_bar_button_width:I = 0x7f0d0100

.field public static final action_bar_icon_margin:I = 0x7f0d0103

.field public static final action_bar_icon_size:I = 0x7f0d0102

.field public static final action_bar_layout_height:I = 0x7f0d00ff

.field public static final action_bar_text_padding_left:I = 0x7f0d0101

.field public static final activity_horizontal_margin:I = 0x7f0d001b

.field public static final activity_vertical_margin:I = 0x7f0d001c

.field public static final basic_dialog_button_bar_height:I = 0x7f0d0035

.field public static final basic_dialog_button_bar_padding_bottom:I = 0x7f0d0036

.field public static final basic_dialog_button_bar_separator:I = 0x7f0d0037

.field public static final basic_dialog_button_width:I = 0x7f0d0038

.field public static final basic_dialog_padding_bottom:I = 0x7f0d0033

.field public static final basic_dialog_padding_left:I = 0x7f0d0031

.field public static final basic_dialog_padding_top:I = 0x7f0d0032

.field public static final basic_dialog_shadow_height:I = 0x7f0d0034

.field public static final basic_dialog_title_bar_height:I = 0x7f0d002f

.field public static final basic_dialog_title_bar_padding:I = 0x7f0d0030

.field public static final basic_dialog_width:I = 0x7f0d002e

.field public static final best_time_lo_leave_title_height_land:I = 0x7f0d003d

.field public static final best_time_lo_leave_title_height_port:I = 0x7f0d003e

.field public static final best_time_to_leave_info_height_land:I = 0x7f0d003a

.field public static final best_time_to_leave_info_height_port:I = 0x7f0d0039

.field public static final best_time_to_leave_info_time_width_land:I = 0x7f0d003c

.field public static final best_time_to_leave_info_width_land:I = 0x7f0d003b

.field public static final button_icon_text_padding:I = 0x7f0d0002

.field public static final common_button_navigation_padding_left:I = 0x7f0d0108

.field public static final full_height:I = 0x7f0d0001

.field public static final full_width:I = 0x7f0d0000

.field public static final grp_loc_sha_btn_height:I = 0x7f0d001d

.field public static final grp_loc_sha_btn_width:I = 0x7f0d001e

.field public static final height_loc_detail_with_buttons:I = 0x7f0d0029

.field public static final height_loc_detail_without_buttons:I = 0x7f0d002a

.field public static final height_navigation_bar_land:I = 0x7f0d0024

.field public static final help_about_image_height:I = 0x7f0d00fe

.field public static final help_about_image_width:I = 0x7f0d00fd

.field public static final indicator_height:I = 0x7f0d0003

.field public static final indicator_left_am_height:I = 0x7f0d000b

.field public static final indicator_left_icon_height:I = 0x7f0d0008

.field public static final indicator_left_icon_margin:I = 0x7f0d0009

.field public static final indicator_left_icon_mic_height:I = 0x7f0d0006

.field public static final indicator_left_icon_mic_width:I = 0x7f0d0005

.field public static final indicator_left_icon_width:I = 0x7f0d0007

.field public static final indicator_left_margin_bottom:I = 0x7f0d000a

.field public static final indicator_left_style_width:I = 0x7f0d0004

.field public static final indicator_left_time_height:I = 0x7f0d000c

.field public static final isolated_split_center_point_x:I = 0x7f0d001f

.field public static final isolated_split_center_point_y:I = 0x7f0d0020

.field public static final list_item_image_margin_top:I = 0x7f0d004d

.field public static final list_item_inbox_icon_height:I = 0x7f0d00e2

.field public static final list_item_inbox_icon_margin_left:I = 0x7f0d00e5

.field public static final list_item_inbox_icon_margin_right:I = 0x7f0d00e4

.field public static final list_item_inbox_icon_margin_top:I = 0x7f0d00e3

.field public static final list_item_inbox_icon_width:I = 0x7f0d00e1

.field public static final list_item_inbox_image_margin_top:I = 0x7f0d00e0

.field public static final list_item_logs_icon_margin_bottom:I = 0x7f0d0109

.field public static final list_item_logs_icon_margin_right:I = 0x7f0d004f

.field public static final list_item_logs_image_margin_top:I = 0x7f0d004e

.field public static final list_item_main_font_size:I = 0x7f0d0014

.field public static final list_item_sub2_font_size:I = 0x7f0d0016

.field public static final list_item_sub_font_size:I = 0x7f0d0015

.field public static final list_item_text_margin_left:I = 0x7f0d000f

.field public static final list_item_text_margin_right:I = 0x7f0d0010

.field public static final list_item_text_margin_top:I = 0x7f0d0012

.field public static final list_mark_destination_height:I = 0x7f0d0028

.field public static final list_mark_height:I = 0x7f0d0026

.field public static final list_title_height:I = 0x7f0d000e

.field public static final list_title_text_font_size:I = 0x7f0d0013

.field public static final location_group_share_buttons_size:I = 0x7f0d0106

.field public static final location_group_share_duration_text_padding:I = 0x7f0d0105

.field public static final location_group_share_image_size:I = 0x7f0d0104

.field public static final location_group_share_linear_route_height:I = 0x7f0d0107

.field public static final locshare_list_item_image_margin_top:I = 0x7f0d00ad

.field public static final mapview_balloon_address_margin_top:I = 0x7f0d00ac

.field public static final mapview_balloon_height:I = 0x7f0d00a6

.field public static final mapview_balloon_padding_bottom:I = 0x7f0d00aa

.field public static final mapview_balloon_padding_left:I = 0x7f0d00a7

.field public static final mapview_balloon_padding_right:I = 0x7f0d00a8

.field public static final mapview_balloon_padding_top:I = 0x7f0d00a9

.field public static final mapview_balloon_title_height:I = 0x7f0d00ab

.field public static final mapview_balloon_width:I = 0x7f0d00a5

.field public static final margin_bottom_navigation_bar_land:I = 0x7f0d0025

.field public static final margin_imageview_left_locationactivity:I = 0x7f0d002b

.field public static final margin_textview_left_locationactivity:I = 0x7f0d002c

.field public static final margin_textview_right_locationactivity:I = 0x7f0d002d

.field public static final message_content_area_height:I = 0x7f0d00db

.field public static final message_content_area_height_with_replace:I = 0x7f0d00df

.field public static final message_content_area_margin:I = 0x7f0d00d4

.field public static final message_content_edit_full_height:I = 0x7f0d00dd

.field public static final message_content_edit_height:I = 0x7f0d00dc

.field public static final message_content_edit_height_with_replace:I = 0x7f0d00de

.field public static final message_content_text_padding:I = 0x7f0d00d5

.field public static final message_eventicon_area_bottom_margin:I = 0x7f0d00d0

.field public static final message_eventicon_area_height:I = 0x7f0d00cd

.field public static final message_eventicon_area_margin:I = 0x7f0d00ce

.field public static final message_eventicon_area_top_margin:I = 0x7f0d00cf

.field public static final message_eventicon_icon_height:I = 0x7f0d00d2

.field public static final message_eventicon_icon_width:I = 0x7f0d00d1

.field public static final message_eventinfo_icon_margin:I = 0x7f0d00d3

.field public static final message_selection_area_button_height:I = 0x7f0d00d7

.field public static final message_selection_area_height:I = 0x7f0d00d6

.field public static final message_selection_area_padding:I = 0x7f0d00d8

.field public static final message_selection_area_replace_button_height:I = 0x7f0d00d9

.field public static final message_selection_area_replace_button_width:I = 0x7f0d00da

.field public static final music_list_item_font_size:I = 0x7f0d00af

.field public static final music_list_item_index_margin_left:I = 0x7f0d00b4

.field public static final music_list_item_index_margin_top:I = 0x7f0d00b5

.field public static final music_list_item_sub2_font_size:I = 0x7f0d00b1

.field public static final music_list_item_sub_font_size:I = 0x7f0d00b0

.field public static final music_main_item_font_size:I = 0x7f0d00ae

.field public static final music_mini_player_artist_item_font_size:I = 0x7f0d00b3

.field public static final music_mini_player_title_item_font_size:I = 0x7f0d00b2

.field public static final no_list_font_size:I = 0x7f0d0017

.field public static final noti_Bluetooth_content_area_Title_height:I = 0x7f0d0090

.field public static final noti_Bluetooth_content_area_height:I = 0x7f0d008f

.field public static final noti_Bluetooth_content_area_padding_left:I = 0x7f0d0091

.field public static final noti_Bluetooth_content_area_padding_right:I = 0x7f0d0092

.field public static final noti_Bluetooth_content_button_height:I = 0x7f0d0096

.field public static final noti_Bluetooth_content_text_Confirm_height:I = 0x7f0d0095

.field public static final noti_Bluetooth_content_text_Pin_Edit_TITLE_height:I = 0x7f0d010c

.field public static final noti_Bluetooth_content_text_Pin_Edit_height:I = 0x7f0d0094

.field public static final noti_Bluetooth_content_text_Pin_height:I = 0x7f0d0093

.field public static final noti_content_area_height:I = 0x7f0d0050

.field public static final noti_content_area_mms_height:I = 0x7f0d0051

.field public static final noti_content_message_icon_width:I = 0x7f0d0052

.field public static final noti_content_message_read_area_height:I = 0x7f0d010d

.field public static final noti_content_tts_icon_width:I = 0x7f0d0054

.field public static final noti_content_x_btn_width:I = 0x7f0d0056

.field public static final noti_contnet_area_padding_right:I = 0x7f0d0059

.field public static final noti_contnet_horizon_first_margin:I = 0x7f0d005a

.field public static final noti_contnet_horizon_sec_margin:I = 0x7f0d005b

.field public static final noti_contnet_horizon_thr_margin:I = 0x7f0d005c

.field public static final noti_contnet_message_icon_height:I = 0x7f0d0053

.field public static final noti_contnet_tts_icon_height:I = 0x7f0d0055

.field public static final noti_contnet_x_btn_height:I = 0x7f0d0057

.field public static final noti_contnet_x_btn_margin_right:I = 0x7f0d0058

.field public static final noti_land_bottom_shadow_h:I = 0x7f0d00a0

.field public static final noti_land_button_h:I = 0x7f0d00a3

.field public static final noti_land_multi_button_h:I = 0x7f0d009d

.field public static final noti_land_multi_popup_h:I = 0x7f0d0098

.field public static final noti_land_multi_popup_w:I = 0x7f0d0097

.field public static final noti_land_multi_right_shadow_w:I = 0x7f0d0099

.field public static final noti_land_multi_title_icon_h:I = 0x7f0d009c

.field public static final noti_land_multi_title_icon_w:I = 0x7f0d009b

.field public static final noti_land_multi_title_layer_h:I = 0x7f0d009a

.field public static final noti_land_repeat_icon:I = 0x7f0d0040

.field public static final noti_land_repeat_icon_w:I = 0x7f0d00a4

.field public static final noti_land_title_icon_h:I = 0x7f0d00a2

.field public static final noti_land_title_icon_w:I = 0x7f0d00a1

.field public static final noti_land_title_layer_h:I = 0x7f0d009e

.field public static final noti_land_title_twolines_h:I = 0x7f0d009f

.field public static final noti_location_buttons_area_height:I = 0x7f0d0084

.field public static final noti_location_buttons_area_margin:I = 0x7f0d0085

.field public static final noti_location_buttons_area_padding:I = 0x7f0d0083

.field public static final noti_location_content_area_height:I = 0x7f0d007a

.field public static final noti_location_content_area_padding_bottom:I = 0x7f0d007e

.field public static final noti_location_content_area_padding_left:I = 0x7f0d007b

.field public static final noti_location_content_area_padding_right:I = 0x7f0d007d

.field public static final noti_location_content_area_padding_top:I = 0x7f0d007c

.field public static final noti_location_content_img_height:I = 0x7f0d0080

.field public static final noti_location_content_img_width:I = 0x7f0d007f

.field public static final noti_location_content_margin_between_img_text:I = 0x7f0d0081

.field public static final noti_location_content_text_height:I = 0x7f0d0082

.field public static final noti_location_content_text_margin_bottom:I = 0x7f0d010b

.field public static final noti_location_content_text_margin_top:I = 0x7f0d010a

.field public static final noti_location_message_eventinfo_name_margin:I = 0x7f0d011c

.field public static final noti_location_sent_content_text_height:I = 0x7f0d008b

.field public static final noti_location_sent_height:I = 0x7f0d0086

.field public static final noti_location_sent_padding_bottom:I = 0x7f0d008a

.field public static final noti_location_sent_padding_left:I = 0x7f0d0087

.field public static final noti_location_sent_padding_right:I = 0x7f0d0089

.field public static final noti_location_sent_padding_top:I = 0x7f0d0088

.field public static final noti_location_sharing_content_text_height:I = 0x7f0d008e

.field public static final noti_location_sharing_image_height:I = 0x7f0d008d

.field public static final noti_location_sharing_image_width:I = 0x7f0d008c

.field public static final noti_message_button_height:I = 0x7f0d00c7

.field public static final noti_message_button_icon_padding:I = 0x7f0d011e

.field public static final noti_message_button_icon_padding_left:I = 0x7f0d0120

.field public static final noti_message_button_icon_padding_top:I = 0x7f0d011f

.field public static final noti_message_button_line_padding:I = 0x7f0d00c8

.field public static final noti_message_eventicon_area_height:I = 0x7f0d00b7

.field public static final noti_message_eventicon_area_right_margin:I = 0x7f0d00b8

.field public static final noti_message_eventicon_area_width:I = 0x7f0d00b6

.field public static final noti_message_eventicon_icon_cancel_height:I = 0x7f0d0116

.field public static final noti_message_eventicon_icon_cancel_width:I = 0x7f0d0115

.field public static final noti_message_eventicon_icon_height:I = 0x7f0d00ba

.field public static final noti_message_eventicon_icon_height_event:I = 0x7f0d00ca

.field public static final noti_message_eventicon_icon_tts_height:I = 0x7f0d00bf

.field public static final noti_message_eventicon_icon_tts_width:I = 0x7f0d00be

.field public static final noti_message_eventicon_icon_width:I = 0x7f0d00b9

.field public static final noti_message_eventicon_icon_width_event:I = 0x7f0d00c9

.field public static final noti_message_eventicon_read_area_height:I = 0x7f0d0114

.field public static final noti_message_eventinfo_icon_cancel_right_margin:I = 0x7f0d0118

.field public static final noti_message_eventinfo_icon_cancel_top_margin:I = 0x7f0d0117

.field public static final noti_message_eventinfo_icon_left_margin:I = 0x7f0d00bc

.field public static final noti_message_eventinfo_icon_margin:I = 0x7f0d00bd

.field public static final noti_message_eventinfo_icon_right_margin:I = 0x7f0d00bb

.field public static final noti_message_eventinfo_icon_tts_left_margin:I = 0x7f0d011b

.field public static final noti_message_eventinfo_icon_tts_margin:I = 0x7f0d00c0

.field public static final noti_message_eventinfo_icon_tts_right_margin:I = 0x7f0d011a

.field public static final noti_message_eventinfo_icon_tts_top_margin:I = 0x7f0d0119

.field public static final noti_message_eventinfo_name_height:I = 0x7f0d00c1

.field public static final noti_message_eventinfo_name_height_event:I = 0x7f0d00cc

.field public static final noti_message_eventinfo_name_margin:I = 0x7f0d00c2

.field public static final noti_message_eventinfo_number_height:I = 0x7f0d00c3

.field public static final noti_message_eventinfo_number_margin:I = 0x7f0d00c4

.field public static final noti_message_selection_area_height:I = 0x7f0d00c5

.field public static final noti_message_selection_area_height_event:I = 0x7f0d00cb

.field public static final noti_message_selection_area_icon_height:I = 0x7f0d011d

.field public static final noti_message_selection_area_padding:I = 0x7f0d00c6

.field public static final noti_music_control_area_width:I = 0x7f0d006c

.field public static final noti_music_control_icon_exp_height:I = 0x7f0d0073

.field public static final noti_music_control_icon_exp_width:I = 0x7f0d0072

.field public static final noti_music_control_icon_height:I = 0x7f0d006f

.field public static final noti_music_control_icon_last_margin:I = 0x7f0d0071

.field public static final noti_music_control_icon_margin:I = 0x7f0d0070

.field public static final noti_music_control_icon_width:I = 0x7f0d006e

.field public static final noti_music_control_padding_righr:I = 0x7f0d006d

.field public static final noti_music_height:I = 0x7f0d0062

.field public static final noti_music_info_adjustment:I = 0x7f0d006b

.field public static final noti_music_info_area_width:I = 0x7f0d0063

.field public static final noti_music_info_artist_height:I = 0x7f0d006a

.field public static final noti_music_info_horizon_first_margin:I = 0x7f0d0067

.field public static final noti_music_info_horizon_sec_margin:I = 0x7f0d0068

.field public static final noti_music_info_icon_height:I = 0x7f0d0066

.field public static final noti_music_info_icon_width:I = 0x7f0d0065

.field public static final noti_music_info_title_height:I = 0x7f0d0069

.field public static final noti_music_info_top_padding:I = 0x7f0d0064

.field public static final noti_port_bottom_shadow_h:I = 0x7f0d0110

.field public static final noti_port_button_h:I = 0x7f0d0113

.field public static final noti_port_multi_button_h:I = 0x7f0d010f

.field public static final noti_port_title_icon_h:I = 0x7f0d0112

.field public static final noti_port_title_icon_w:I = 0x7f0d0111

.field public static final noti_selection_area_height:I = 0x7f0d005d

.field public static final noti_selection_area_icon_height:I = 0x7f0d0061

.field public static final noti_selection_area_icon_width:I = 0x7f0d0060

.field public static final noti_selection_area_margin_between_two_buttons:I = 0x7f0d005f

.field public static final noti_selection_area_padding:I = 0x7f0d005e

.field public static final noti_sending_height:I = 0x7f0d0074

.field public static final noti_sending_progress_height:I = 0x7f0d0076

.field public static final noti_sending_progress_margin_right:I = 0x7f0d0077

.field public static final noti_sending_progress_width:I = 0x7f0d0075

.field public static final noti_sending_result_left_right_padding:I = 0x7f0d0079

.field public static final noti_sending_textview_height:I = 0x7f0d0078

.field public static final noti_sharing_height:I = 0x7f0d010e

.field public static final noti_smartalert_eventicon_area_height:I = 0x7f0d00e7

.field public static final noti_smartalert_eventicon_area_right_margin:I = 0x7f0d00e8

.field public static final noti_smartalert_eventicon_area_width:I = 0x7f0d00e6

.field public static final noti_smartalert_eventicon_icon_height:I = 0x7f0d00ea

.field public static final noti_smartalert_eventicon_icon_width:I = 0x7f0d00e9

.field public static final noti_smartalert_eventinfo_content_height:I = 0x7f0d00f6

.field public static final noti_smartalert_eventinfo_content_height_event:I = 0x7f0d00f9

.field public static final noti_smartalert_eventinfo_content_miles_width:I = 0x7f0d0124

.field public static final noti_smartalert_eventinfo_content_width:I = 0x7f0d00f5

.field public static final noti_smartalert_eventinfo_icon_left_margin:I = 0x7f0d00eb

.field public static final noti_smartalert_eventinfo_icon_right_margin:I = 0x7f0d00ec

.field public static final noti_smartalert_eventinfo_label_backup_margin_right:I = 0x7f0d0123

.field public static final noti_smartalert_eventinfo_label_delay_margin_right:I = 0x7f0d0122

.field public static final noti_smartalert_eventinfo_label_height:I = 0x7f0d00f2

.field public static final noti_smartalert_eventinfo_label_margin_botteom:I = 0x7f0d00f4

.field public static final noti_smartalert_eventinfo_label_margin_top:I = 0x7f0d00f3

.field public static final noti_smartalert_eventinfo_label_width:I = 0x7f0d00f1

.field public static final noti_smartalert_eventinfo_label_width_backup:I = 0x7f0d0121

.field public static final noti_smartalert_eventinfo_line_height:I = 0x7f0d00ed

.field public static final noti_smartalert_eventinfo_margin_left:I = 0x7f0d00ee

.field public static final noti_smartalert_eventinfo_margin_left_event:I = 0x7f0d00fa

.field public static final noti_smartalert_eventinfo_margin_right:I = 0x7f0d00ef

.field public static final noti_smartalert_eventinfo_margin_right_event:I = 0x7f0d00fb

.field public static final noti_smartalert_eventinfo_text_height:I = 0x7f0d00f7

.field public static final noti_smartalert_eventinfo_text_height_event:I = 0x7f0d00f8

.field public static final noti_smartalert_eventinfo_title_height:I = 0x7f0d00f0

.field public static final page_navigation_height:I = 0x7f0d001a

.field public static final phone_isolated_split_center_point_x:I = 0x7f0d0021

.field public static final phone_isolated_split_center_point_y:I = 0x7f0d0022

.field public static final phone_isolated_split_center_point_y_1:I = 0x7f0d0023

.field public static final phone_tab_text_padding:I = 0x7f0d000d

.field public static final position_difference_top:I = 0x7f0d0027

.field public static final quotation_mark_margin_top:I = 0x7f0d0011

.field public static final settings_action_bar_padding:I = 0x7f0d0041

.field public static final settings_list_item_height:I = 0x7f0d0042

.field public static final settings_list_item_right_margin:I = 0x7f0d0043

.field public static final settings_mycar_padding:I = 0x7f0d0044

.field public static final terms_of_service_height:I = 0x7f0d00fc

.field public static final voice_2depth_tts_text_font_size:I = 0x7f0d0018

.field public static final voice_2depth_tts_text_small_font_size:I = 0x7f0d0019

.field public static final voice_dialog_depth_height:I = 0x7f0d0047

.field public static final voice_dialog_depth_mic_effect_height:I = 0x7f0d004c

.field public static final voice_dialog_depth_mic_effect_width:I = 0x7f0d004b

.field public static final voice_dialog_home_full_height:I = 0x7f0d0045

.field public static final voice_dialog_home_mic_effect_height:I = 0x7f0d004a

.field public static final voice_dialog_home_mic_effect_width:I = 0x7f0d0049

.field public static final voice_dialog_home_mini_height:I = 0x7f0d0046

.field public static final voice_dialog_overlay_height:I = 0x7f0d0048

.field public static final voicelocation_black_style_height:I = 0x7f0d003f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
