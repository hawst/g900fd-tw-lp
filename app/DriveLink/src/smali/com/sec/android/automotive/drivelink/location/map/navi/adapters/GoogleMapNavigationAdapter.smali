.class public Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;
.super Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
.source "GoogleMapNavigationAdapter.java"


# static fields
.field private static final NAVIGATION_MODE:C = 'd'

.field private static final NAVIGATION_VIEW:C = '1'

.field private static final TAG:Ljava/lang/String; = "[GoogleMapNavigationAdapter]"


# instance fields
.field private mToast:Landroid/widget/Toast;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;-><init>()V

    .line 34
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;->setNeedOrigin(Z)V

    .line 35
    return-void
.end method

.method private getTTSText(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p1, "adress"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 211
    move-object v0, p1

    .line 213
    .local v0, "mTts":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 214
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 215
    const v2, 0x7f0a04e9

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 214
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 220
    :cond_0
    return-object v0
.end method

.method private hasGoogleAccount(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 224
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 225
    .local v1, "accMan":Landroid/accounts/AccountManager;
    const-string/jumbo v3, "com.google"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 226
    .local v0, "accArray":[Landroid/accounts/Account;
    array-length v3, v0

    if-lt v3, v2, :cond_0

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private showToast(I)V
    .locals 2
    .param p1, "textID"    # I

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 238
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 239
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v0

    const/4 v1, 0x1

    .line 238
    invoke-static {v0, p1, v1}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;->mToast:Landroid/widget/Toast;

    .line 240
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_1

    .line 241
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 242
    :cond_1
    return-void
.end method


# virtual methods
.method public getNaviPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    const-string/jumbo v0, "com.google.android.apps.maps"

    return-object v0
.end method

.method public getNavigationIntent(DDLjava/lang/String;)Landroid/content/Intent;
    .locals 6
    .param p1, "lat"    # D
    .param p3, "lng"    # D
    .param p5, "address"    # Ljava/lang/String;

    .prologue
    const-wide/16 v4, 0x0

    .line 41
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 42
    .local v1, "query":Ljava/lang/StringBuilder;
    cmpl-double v3, p1, v4

    if-eqz v3, :cond_0

    cmpl-double v3, p3, v4

    if-eqz v3, :cond_0

    .line 44
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 45
    const/16 v3, 0x2c

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 46
    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 50
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "http://maps.google.com/maps?"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 51
    .local v2, "uri":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "daddr="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 53
    const-string/jumbo v3, "&nav="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    const/16 v3, 0x31

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 55
    const-string/jumbo v3, "&dirflg="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 58
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.VIEW"

    .line 59
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 58
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 60
    .local v0, "navigationIntent":Landroid/content/Intent;
    const v3, 0x10208000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 63
    const-string/jumbo v3, "com.google.android.apps.maps"

    .line 64
    const-string/jumbo v4, "com.google.android.maps.MapsActivity"

    .line 63
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    return-object v0

    .line 48
    .end local v0    # "navigationIntent":Landroid/content/Intent;
    .end local v2    # "uri":Ljava/lang/StringBuilder;
    :cond_0
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public startMap(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 160
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;->hasGoogleAccount(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 163
    const v2, 0x7f0a061c

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 165
    .local v0, "mTts":Ljava/lang/String;
    const-string/jumbo v2, "[GoogleMapNavigationAdapter]"

    const-string/jumbo v3, "startMap"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    const-string/jumbo v2, "MULTI_WINDOW_ENABLE"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 169
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    .line 170
    const-string/jumbo v2, "com.google.android.apps.maps"

    .line 171
    const-string/jumbo v3, "com.google.android.maps.MapsActivity"

    .line 169
    invoke-direct {v1, p1, v2, v3}, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    .local v1, "multiData":Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;
    const/4 v2, 0x0

    invoke-virtual {p0, p1, v2, v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;->startMultiWindowWithIndicator(Landroid/content/Context;ZLjava/lang/String;Landroid/os/Parcelable;)V

    .line 181
    .end local v0    # "mTts":Ljava/lang/String;
    .end local v1    # "multiData":Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    const v2, 0x7f0a01fb

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;->showToast(I)V

    goto :goto_0
.end method

.method public startMultiNavi(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;)Z
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parcel"    # Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    .prologue
    const/4 v11, 0x0

    .line 69
    const/4 v10, 0x1

    .line 70
    .local v10, "ret":Z
    invoke-super {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->startMultiNavi(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;)Z

    .line 71
    const-string/jumbo v0, "GoogleMapNavigationAdapter"

    const-string/jumbo v1, "startMultiNavi"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    if-nez p2, :cond_0

    .line 74
    const-string/jumbo v0, "[GoogleMapNavigationAdapter]"

    const-string/jumbo v1, "Parcel is null!!!!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v11

    .line 155
    :goto_0
    return v0

    .line 78
    :cond_0
    iget v0, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->isStartMap:I

    if-nez v0, :cond_3

    .line 81
    :try_start_0
    new-instance v9, Landroid/content/Intent;

    const-string/jumbo v0, "android.intent.action.VIEW"

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 85
    .local v9, "navigationIntent":Landroid/content/Intent;
    const v0, 0x10208000

    invoke-virtual {v9, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 89
    iget-object v0, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->packageName:Ljava/lang/String;

    .line 90
    iget-object v1, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->className:Ljava/lang/String;

    .line 89
    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    const-string/jumbo v0, "MULTI_WINDOW_ENABLE"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;->setMultiWindowIntentToNavi(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v9

    .line 97
    :cond_1
    invoke-virtual {p1, v9}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v9    # "navigationIntent":Landroid/content/Intent;
    :goto_1
    move v0, v10

    .line 155
    goto :goto_0

    .line 99
    :catch_0
    move-exception v6

    .line 100
    .local v6, "e":Ljava/lang/Exception;
    const/4 v10, 0x0

    .line 101
    const-string/jumbo v0, "GoogleMapNavigationAdapter"

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 103
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 107
    const-string/jumbo v1, "PREF_SETTINGS_MY_NAVIGATION"

    .line 106
    invoke-virtual {v0, v1, v11}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v8

    .line 109
    .local v8, "mapID":I
    invoke-virtual {p0, v8}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;->getInstallNavigationApp(I)Landroid/content/Intent;

    move-result-object v7

    .line 110
    .local v7, "goToMarket":Landroid/content/Intent;
    if-eqz v7, :cond_2

    .line 111
    invoke-virtual {p1, v7}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 113
    :cond_2
    const-string/jumbo v0, "Error Google Navi"

    .line 114
    const-string/jumbo v1, "Error on starting Google Navigation Intent"

    .line 113
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 122
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v7    # "goToMarket":Landroid/content/Intent;
    .end local v8    # "mapID":I
    :cond_3
    :try_start_1
    iget-wide v1, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLat:D

    .line 123
    iget-wide v3, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLng:D

    iget-object v5, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mTts:Ljava/lang/String;

    move-object v0, p0

    .line 122
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;->getNavigationIntent(DDLjava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    .line 127
    .restart local v9    # "navigationIntent":Landroid/content/Intent;
    const-string/jumbo v0, "MULTI_WINDOW_ENABLE"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 129
    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;->setMultiWindowIntentToNavi(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v9

    .line 133
    :cond_4
    invoke-virtual {p1, v9}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 135
    .end local v9    # "navigationIntent":Landroid/content/Intent;
    :catch_1
    move-exception v6

    .line 136
    .restart local v6    # "e":Ljava/lang/Exception;
    const/4 v10, 0x0

    .line 137
    const-string/jumbo v0, "GoogleMapNavigationAdapter"

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 141
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 139
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 143
    const-string/jumbo v1, "PREF_SETTINGS_MY_NAVIGATION"

    .line 142
    invoke-virtual {v0, v1, v11}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v8

    .line 145
    .restart local v8    # "mapID":I
    invoke-virtual {p0, v8}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;->getInstallNavigationApp(I)Landroid/content/Intent;

    move-result-object v7

    .line 146
    .restart local v7    # "goToMarket":Landroid/content/Intent;
    if-eqz v7, :cond_5

    .line 147
    invoke-virtual {p1, v7}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 149
    :cond_5
    const-string/jumbo v0, "Error Google Navi"

    .line 150
    const-string/jumbo v1, "Error on starting Google Navigation Intent"

    .line 149
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public startNavigation(Landroid/content/Context;DDLjava/lang/String;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "lat"    # D
    .param p4, "lng"    # D
    .param p6, "tts"    # Ljava/lang/String;

    .prologue
    .line 186
    const-string/jumbo v1, "[GoogleMapNavigationAdapter]"

    const-string/jumbo v2, "startNavigation"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    invoke-direct {p0, p6, p1}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;->getTTSText(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 189
    .local v7, "ttsText":Ljava/lang/String;
    if-nez p1, :cond_1

    .line 190
    const-string/jumbo v1, "[GoogleMapNavigationAdapter]"

    const-string/jumbo v2, "startNavigation: context == null"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;->hasGoogleAccount(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 197
    const-string/jumbo v1, "MULTI_WINDOW_ENABLE"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 199
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;-><init>(Landroid/content/Context;DDLjava/lang/String;)V

    .line 201
    .local v0, "multiData":Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v7, v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;->startMultiWindowWithIndicator(Landroid/content/Context;ZLjava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 206
    .end local v0    # "multiData":Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;
    :cond_2
    const v1, 0x7f0a01fb

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;->showToast(I)V

    goto :goto_0
.end method
