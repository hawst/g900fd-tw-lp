.class Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$3;
.super Ljava/lang/Object;
.source "MessageComposerActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    .line 935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnCheckKeyboard()Z
    .locals 2

    .prologue
    .line 952
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$11(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_SHOW:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    if-ne v0, v1, :cond_0

    .line 953
    const/4 v0, 0x1

    .line 955
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public OnDisplayKeyboard(Z)V
    .locals 4
    .param p1, "keyboard"    # Z

    .prologue
    .line 940
    const/4 v0, 0x3

    const-string/jumbo v1, "OnDisplayKeyboard"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "display  = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$9(ILjava/lang/String;Ljava/lang/String;)V

    .line 943
    if-eqz p1, :cond_0

    .line 944
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setMessageComposerTextViewBtn(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$10(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;Z)V

    .line 948
    :goto_0
    return-void

    .line 946
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setMessageComposerTextViewBtn(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$10(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;Z)V

    goto :goto_0
.end method
