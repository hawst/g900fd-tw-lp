.class public Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;
.super Ljava/lang/Object;
.source "PlaceOn.java"


# static fields
.field public static final ACCESS_TOKEN:Ljava/lang/String; = "AIzaSyAC0aY2kAWfU5T_VqNac3CFok8nH8v2ZwQ"

.field private static final TAG:Ljava/lang/String; = "PlaceOn"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addFriendsToGroup(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 8
    .param p0, "userId"    # Ljava/lang/String;
    .param p1, "groupId"    # Ljava/lang/String;
    .param p2, "content"    # Lorg/json/JSONObject;

    .prologue
    .line 367
    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "Accept"

    const-string/jumbo v7, "application/JSON"

    invoke-direct {v0, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    .local v0, "acceptHeader":Lorg/apache/http/message/BasicHeader;
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "Content-Type"

    .line 369
    const-string/jumbo v7, "application/JSON"

    .line 368
    invoke-direct {v2, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    .local v2, "contentType":Lorg/apache/http/message/BasicHeader;
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "sspGuid"

    const-string/jumbo v7, "0123456789"

    invoke-direct {v4, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    .local v4, "sspGuidHeader":Lorg/apache/http/message/BasicHeader;
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "accessToken"

    .line 372
    const-string/jumbo v7, "1234567890"

    .line 371
    invoke-direct {v1, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    .local v1, "accessTokenHeader":Lorg/apache/http/message/BasicHeader;
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "userId"

    invoke-direct {v5, v6, p0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    .local v5, "userIdHeader":Lorg/apache/http/message/BasicHeader;
    const/4 v6, 0x5

    new-array v3, v6, [Lorg/apache/http/Header;

    .line 376
    .local v3, "headers":[Lorg/apache/http/Header;
    const/4 v6, 0x0

    aput-object v0, v3, v6

    .line 377
    const/4 v6, 0x1

    aput-object v2, v3, v6

    .line 378
    const/4 v6, 0x2

    aput-object v4, v3, v6

    .line 379
    const/4 v6, 0x3

    aput-object v1, v3, v6

    .line 380
    const/4 v6, 0x4

    aput-object v5, v3, v6

    .line 382
    const/4 v6, 0x0

    return-object v6
.end method

.method public static createShareGroup(ILjava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 4
    .param p0, "userId"    # I
    .param p1, "sspGuid"    # Ljava/lang/String;
    .param p2, "countryCode"    # Ljava/lang/String;
    .param p3, "content"    # Lorg/json/JSONObject;

    .prologue
    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 46
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "Accept"

    const-string/jumbo v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 47
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "Content-Type"

    const-string/jumbo v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "sspGuid"

    invoke-direct {v1, v2, p1}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "userId"

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "accessToken"

    const-string/jumbo v3, "AIzaSyAC0aY2kAWfU5T_VqNac3CFok8nH8v2ZwQ"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "countryCode"

    invoke-direct {v1, v2, p2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    const/4 v1, 0x0

    return-object v1
.end method

.method public static deleteFavoriteLocation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 5
    .param p0, "userId"    # Ljava/lang/String;
    .param p1, "sspGuid"    # Ljava/lang/String;
    .param p2, "locationId"    # Ljava/lang/String;

    .prologue
    .line 299
    const/4 v1, 0x0

    .line 300
    .local v1, "result":Lorg/json/JSONObject;
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isString(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isString(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 301
    invoke-static {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isString(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 323
    :cond_0
    :goto_0
    return-object v1

    .line 305
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 306
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v3, "accept"

    const-string/jumbo v4, "application/json"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 307
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v3, "content-Type"

    const-string/jumbo v4, "application/json"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 308
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v3, "accessToken"

    const-string/jumbo v4, "dkbj2#$$asdf1293jdk"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 309
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v3, "userId"

    invoke-direct {v2, v3, p0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 310
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v3, "sspGuid"

    invoke-direct {v2, v3, p1}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static deleteGroup(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 7
    .param p0, "userId"    # Ljava/lang/String;
    .param p1, "sspGuid"    # Ljava/lang/String;
    .param p2, "groupId"    # Ljava/lang/String;

    .prologue
    .line 111
    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v5, "Accept"

    const-string/jumbo v6, "application/JSON"

    invoke-direct {v0, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    .local v0, "acceptHeader":Lorg/apache/http/message/BasicHeader;
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v5, "Content-Type"

    .line 113
    const-string/jumbo v6, "application/JSON"

    .line 112
    invoke-direct {v1, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    .local v1, "contentType":Lorg/apache/http/message/BasicHeader;
    new-instance v3, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v5, "sspGuid"

    invoke-direct {v3, v5, p1}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    .local v3, "sspGuidHeader":Lorg/apache/http/message/BasicHeader;
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v5, "userId"

    invoke-direct {v4, v5, p0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    .local v4, "userIdHeader":Lorg/apache/http/message/BasicHeader;
    const/4 v5, 0x4

    new-array v2, v5, [Lorg/apache/http/Header;

    .line 118
    .local v2, "headers":[Lorg/apache/http/Header;
    const/4 v5, 0x0

    aput-object v0, v2, v5

    .line 119
    const/4 v5, 0x1

    aput-object v1, v2, v5

    .line 120
    const/4 v5, 0x2

    aput-object v3, v2, v5

    .line 121
    const/4 v5, 0x3

    aput-object v4, v2, v5

    .line 123
    const/4 v5, 0x0

    return-object v5
.end method

.method public static expandShortURL(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 1
    .param p0, "shortUrl"    # Ljava/lang/String;

    .prologue
    .line 347
    const/4 v0, 0x0

    .line 358
    .local v0, "result":Lorg/json/JSONObject;
    return-object v0
.end method

.method public static getFavoriteLocation(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 5
    .param p0, "userId"    # Ljava/lang/String;
    .param p1, "sspGuid"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 275
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isString(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isString(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 290
    :cond_0
    :goto_0
    return-object v4

    .line 279
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 280
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "accept"

    const-string/jumbo v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 281
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "content-Type"

    const-string/jumbo v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 282
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "accessToken"

    const-string/jumbo v3, "dkbj2#$$asdf1293jdk"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 283
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "userId"

    invoke-direct {v1, v2, p0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 284
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "sspGuid"

    invoke-direct {v1, v2, p1}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getGroup(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 6
    .param p0, "userId"    # Ljava/lang/String;
    .param p1, "groupId"    # Ljava/lang/String;

    .prologue
    .line 132
    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v4, "accept"

    const-string/jumbo v5, "application/json"

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    .local v0, "acceptHeader":Lorg/apache/http/message/BasicHeader;
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v4, "content-Type"

    .line 134
    const-string/jumbo v5, "application/json"

    .line 133
    invoke-direct {v1, v4, v5}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    .local v1, "contentType":Lorg/apache/http/message/BasicHeader;
    new-instance v3, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v4, "userId"

    invoke-direct {v3, v4, p0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    .local v3, "userIdHeader":Lorg/apache/http/message/BasicHeader;
    const/4 v4, 0x3

    new-array v2, v4, [Lorg/apache/http/Header;

    .line 138
    .local v2, "headers":[Lorg/apache/http/Header;
    const/4 v4, 0x0

    aput-object v0, v2, v4

    .line 139
    const/4 v4, 0x1

    aput-object v1, v2, v4

    .line 140
    const/4 v4, 0x2

    aput-object v3, v2, v4

    .line 142
    const/4 v4, 0x0

    return-object v4
.end method

.method public static getGroupByUrl(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 11
    .param p0, "userId"    # Ljava/lang/String;
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 149
    const/4 v4, 0x0

    .line 151
    .local v4, "result":Lorg/json/JSONObject;
    if-eqz p0, :cond_0

    const-string/jumbo v7, ""

    invoke-virtual {p0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    if-eqz p1, :cond_0

    .line 152
    const-string/jumbo v7, ""

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 185
    :cond_0
    :goto_0
    return-object v10

    .line 155
    :cond_1
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn;->expandShortURL(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 157
    if-eqz v4, :cond_2

    const-string/jumbo v7, "longUrl"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 158
    :cond_2
    const-string/jumbo v7, "PlaceOn"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Fail to expand short url. "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 168
    :cond_3
    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v7, "Accept"

    const-string/jumbo v8, "application/JSON"

    invoke-direct {v0, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    .local v0, "acceptHeader":Lorg/apache/http/message/BasicHeader;
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v7, "Content-Type"

    .line 170
    const-string/jumbo v8, "application/JSON"

    .line 169
    invoke-direct {v2, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    .local v2, "contentType":Lorg/apache/http/message/BasicHeader;
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v7, "sspGuid"

    const-string/jumbo v8, "1234567890"

    invoke-direct {v5, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    .local v5, "sspGuidHeader":Lorg/apache/http/message/BasicHeader;
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v7, "userId"

    invoke-direct {v6, v7, p0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    .local v6, "userIdHeader":Lorg/apache/http/message/BasicHeader;
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v7, "accessToken"

    .line 174
    const-string/jumbo v8, "1234567890"

    .line 173
    invoke-direct {v1, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    .local v1, "accessTokenHeader":Lorg/apache/http/message/BasicHeader;
    const/4 v7, 0x5

    new-array v3, v7, [Lorg/apache/http/Header;

    .line 177
    .local v3, "headers":[Lorg/apache/http/Header;
    const/4 v7, 0x0

    aput-object v0, v3, v7

    .line 178
    const/4 v7, 0x1

    aput-object v2, v3, v7

    .line 179
    const/4 v7, 0x2

    aput-object v5, v3, v7

    .line 180
    const/4 v7, 0x3

    aput-object v6, v3, v7

    .line 181
    const/4 v7, 0x4

    aput-object v1, v3, v7

    goto :goto_0
.end method

.method public static getMyProfile(I)Lorg/json/JSONObject;
    .locals 4
    .param p0, "userId"    # I

    .prologue
    .line 210
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 211
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "Accept"

    const-string/jumbo v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "Content-Type"

    const-string/jumbo v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 213
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "accessToken"

    const-string/jumbo v3, "AIzaSyAC0aY2kAWfU5T_VqNac3CFok8nH8v2ZwQ"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "userId"

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 216
    const/4 v1, 0x0

    return-object v1
.end method

.method public static getProfileByPhoneNumber(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 7
    .param p0, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 327
    const/4 v2, 0x0

    .line 328
    .local v2, "result":Lorg/json/JSONObject;
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 343
    :cond_0
    :goto_0
    return-object v2

    .line 336
    :cond_1
    const/4 v3, 0x2

    :try_start_0
    new-array v1, v3, [Lorg/apache/http/Header;

    .line 337
    .local v1, "headers":[Lorg/apache/http/Header;
    const/4 v3, 0x0

    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v5, "Accept"

    const-string/jumbo v6, "application/json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v1, v3

    .line 338
    const/4 v3, 0x1

    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v5, "Content-Type"

    const-string/jumbo v6, "application/json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v1, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 340
    .end local v1    # "headers":[Lorg/apache/http/Header;
    :catch_0
    move-exception v0

    .line 341
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getUserProfile(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 5
    .param p0, "userId"    # Ljava/lang/String;
    .param p1, "accessToken"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 225
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isString(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 226
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 239
    :cond_0
    :goto_0
    return-object v4

    .line 230
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 231
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "accept"

    const-string/jumbo v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 232
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "content-Type"

    const-string/jumbo v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "accessToken"

    const-string/jumbo v3, "dkbj2#$$asdf1293jdk"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static sendMessage(Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 1
    .param p0, "userId"    # Ljava/lang/String;
    .param p1, "content"    # Lorg/json/JSONObject;

    .prologue
    .line 74
    const/4 v0, 0x0

    return-object v0
.end method

.method public static setMyLocation(Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 8
    .param p0, "userId"    # Ljava/lang/String;
    .param p1, "location"    # Lorg/json/JSONObject;

    .prologue
    .line 19
    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "Accept"

    const-string/jumbo v7, "application/JSON"

    invoke-direct {v0, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .local v0, "acceptHeader":Lorg/apache/http/message/BasicHeader;
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "Content-Type"

    .line 21
    const-string/jumbo v7, "application/JSON"

    .line 20
    invoke-direct {v2, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .local v2, "contentType":Lorg/apache/http/message/BasicHeader;
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "userId"

    invoke-direct {v5, v6, p0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .local v5, "userIdHeader":Lorg/apache/http/message/BasicHeader;
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "accessToken"

    .line 24
    const-string/jumbo v7, "1234567890"

    .line 23
    invoke-direct {v1, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .local v1, "accessTokenHeader":Lorg/apache/http/message/BasicHeader;
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "sspGuid"

    const-string/jumbo v7, "0123456789"

    invoke-direct {v4, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .local v4, "sspGuidHeader":Lorg/apache/http/message/BasicHeader;
    const/4 v6, 0x5

    new-array v3, v6, [Lorg/apache/http/Header;

    .line 28
    .local v3, "headers":[Lorg/apache/http/Header;
    const/4 v6, 0x0

    aput-object v0, v3, v6

    .line 29
    const/4 v6, 0x1

    aput-object v2, v3, v6

    .line 30
    const/4 v6, 0x2

    aput-object v5, v3, v6

    .line 31
    const/4 v6, 0x3

    aput-object v1, v3, v6

    .line 32
    const/4 v6, 0x4

    aput-object v4, v3, v6

    .line 34
    const/4 v6, 0x0

    return-object v6
.end method

.method public static updateFavoriteLocation(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 5
    .param p0, "userProfile"    # Lorg/json/JSONObject;
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "sspGuid"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 249
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isValidUser(Lorg/json/JSONObject;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 250
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isString(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 251
    invoke-static {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOn_Utils;->isString(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 252
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 266
    :cond_0
    :goto_0
    return-object v4

    .line 259
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 260
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "accept"

    const-string/jumbo v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 261
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "content-Type"

    const-string/jumbo v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 262
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "accessToken"

    const-string/jumbo v3, "dkbj2#$$asdf1293jdk"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 263
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "userId"

    invoke-direct {v1, v2, p1}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 264
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "sspGuid"

    invoke-direct {v1, v2, p2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static updateGroupParticipantStatus(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 8
    .param p0, "userId"    # Ljava/lang/String;
    .param p1, "groupId"    # Ljava/lang/String;
    .param p2, "content"    # Lorg/json/JSONObject;

    .prologue
    .line 396
    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "Accept"

    const-string/jumbo v7, "application/JSON"

    invoke-direct {v0, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    .local v0, "acceptHeader":Lorg/apache/http/message/BasicHeader;
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "Content-Type"

    .line 398
    const-string/jumbo v7, "application/JSON"

    .line 397
    invoke-direct {v2, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    .local v2, "contentType":Lorg/apache/http/message/BasicHeader;
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "sspGuid"

    const-string/jumbo v7, "0123456789"

    invoke-direct {v4, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    .local v4, "sspGuidHeader":Lorg/apache/http/message/BasicHeader;
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "accessToken"

    .line 401
    const-string/jumbo v7, "1234567890"

    .line 400
    invoke-direct {v1, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    .local v1, "accessTokenHeader":Lorg/apache/http/message/BasicHeader;
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "userId"

    invoke-direct {v5, v6, p0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    .local v5, "userIdHeader":Lorg/apache/http/message/BasicHeader;
    const/4 v6, 0x5

    new-array v3, v6, [Lorg/apache/http/Header;

    .line 405
    .local v3, "headers":[Lorg/apache/http/Header;
    const/4 v6, 0x0

    aput-object v0, v3, v6

    .line 406
    const/4 v6, 0x1

    aput-object v2, v3, v6

    .line 407
    const/4 v6, 0x2

    aput-object v4, v3, v6

    .line 408
    const/4 v6, 0x3

    aput-object v1, v3, v6

    .line 409
    const/4 v6, 0x4

    aput-object v5, v3, v6

    .line 411
    const/4 v6, 0x0

    return-object v6
.end method

.method public static updateProfile(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 4
    .param p0, "userProfile"    # Lorg/json/JSONObject;

    .prologue
    .line 195
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 196
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "Accept"

    const-string/jumbo v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 197
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "Content-Type"

    const-string/jumbo v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "accessToken"

    const-string/jumbo v3, "AIzaSyAC0aY2kAWfU5T_VqNac3CFok8nH8v2ZwQ"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 200
    const/4 v1, 0x0

    return-object v1
.end method

.method public static updateShareGroup(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 8
    .param p0, "sspGuid"    # Ljava/lang/String;
    .param p1, "accessToken"    # Ljava/lang/String;
    .param p2, "userId"    # Ljava/lang/String;
    .param p3, "groupId"    # I
    .param p4, "content"    # Lorg/json/JSONObject;

    .prologue
    .line 86
    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "Accept"

    const-string/jumbo v7, "application/json"

    invoke-direct {v0, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    .local v0, "acceptHeader":Lorg/apache/http/message/BasicHeader;
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "Content-Type"

    .line 88
    const-string/jumbo v7, "application/json"

    .line 87
    invoke-direct {v2, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    .local v2, "contentType":Lorg/apache/http/message/BasicHeader;
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "sspGuid"

    invoke-direct {v4, v6, p0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    .local v4, "sspGuidHeader":Lorg/apache/http/message/BasicHeader;
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "accessToken"

    invoke-direct {v1, v6, p1}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .local v1, "accessTokenHeader":Lorg/apache/http/message/BasicHeader;
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "userId"

    invoke-direct {v5, v6, p2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .local v5, "userIdHeader":Lorg/apache/http/message/BasicHeader;
    const/4 v6, 0x5

    new-array v3, v6, [Lorg/apache/http/Header;

    .line 95
    .local v3, "headers":[Lorg/apache/http/Header;
    const/4 v6, 0x0

    aput-object v0, v3, v6

    .line 96
    const/4 v6, 0x1

    aput-object v2, v3, v6

    .line 97
    const/4 v6, 0x2

    aput-object v4, v3, v6

    .line 98
    const/4 v6, 0x3

    aput-object v1, v3, v6

    .line 99
    const/4 v6, 0x4

    aput-object v5, v3, v6

    .line 101
    const/4 v6, 0x0

    return-object v6
.end method
