.class Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$3;
.super Ljava/lang/Object;
.source "OnDriveLinkNotificationListenerImp.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->onNotifyReceivedMSG(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;

.field private final synthetic val$_msgInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$3;->val$_msgInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 68
    const-string/jumbo v0, "OnDriveLinkNotificationListenerImp"

    .line 69
    const-string/jumbo v1, "[YANG] befor onNotifyReceivedMSG "

    .line 68
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 71
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$3;->val$_msgInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyReceivedMSG(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;)V

    .line 73
    :cond_0
    return-void
.end method
