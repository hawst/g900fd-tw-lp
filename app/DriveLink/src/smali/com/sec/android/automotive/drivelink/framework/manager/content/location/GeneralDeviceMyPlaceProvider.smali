.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;
.super Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;
.source "GeneralDeviceMyPlaceProvider.java"


# static fields
.field private static MYPLACE_ADDRESS:Ljava/lang/String;

.field private static MYPLACE_ID:Ljava/lang/String;

.field private static MYPLACE_LATITUDE:Ljava/lang/String;

.field private static MYPLACE_LONGITUDE:Ljava/lang/String;

.field private static MYPLACE_PROFILE_NAME:Ljava/lang/String;

.field private static MYPLACE_PROVIDER:Ljava/lang/String;

.field private static MYPLACE_TYPE:Ljava/lang/String;

.field private static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 5
    const-string/jumbo v0, "content://com.android.settings.myplace.MyPlaceProvider"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_PROVIDER:Ljava/lang/String;

    .line 7
    const-string/jumbo v0, "_id"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_ID:Ljava/lang/String;

    .line 9
    const-string/jumbo v0, "type"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_TYPE:Ljava/lang/String;

    .line 11
    const-string/jumbo v0, "profile_name"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_PROFILE_NAME:Ljava/lang/String;

    .line 13
    const-string/jumbo v0, "gps_location"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_ADDRESS:Ljava/lang/String;

    .line 15
    const-string/jumbo v0, "gps_latitude"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_LATITUDE:Ljava/lang/String;

    .line 17
    const-string/jumbo v0, "gps_longitude"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_LONGITUDE:Ljava/lang/String;

    .line 19
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_ID:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_TYPE:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 20
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_PROFILE_NAME:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_ADDRESS:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_LATITUDE:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 21
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_LONGITUDE:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 19
    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->PROJECTION:[Ljava/lang/String;

    .line 21
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;-><init>()V

    return-void
.end method


# virtual methods
.method protected getLatLngDegree(D)D
    .locals 2
    .param p1, "latLng"    # D

    .prologue
    .line 65
    double-to-int v0, p1

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->microDegreesToDegrees(I)D

    move-result-wide v0

    return-wide v0
.end method

.method protected getNameFieldMyPlaceAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_ADDRESS:Ljava/lang/String;

    return-object v0
.end method

.method protected getNameFieldMyPlaceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_ID:Ljava/lang/String;

    return-object v0
.end method

.method protected getNameFieldMyPlaceLatitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_LATITUDE:Ljava/lang/String;

    return-object v0
.end method

.method protected getNameFieldMyPlaceLongitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_LONGITUDE:Ljava/lang/String;

    return-object v0
.end method

.method protected getNameFieldMyPlaceProfile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_PROFILE_NAME:Ljava/lang/String;

    return-object v0
.end method

.method protected getNameFieldMyPlaceType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method protected getProviderUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;->MYPLACE_PROVIDER:Ljava/lang/String;

    return-object v0
.end method
