.class public Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForCall;
.super Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;
.source "RecommendationManagerForCall.java"


# static fields
.field private static final WEIGHT_CALL_CONTACT_COUNT_ON_CALLLOG:I = 0x2

.field private static final WEIGHT_CALL_CONTACT_COUNT_ON_DRIVELINK:I = 0x6

.field private static final WEIGHT_CALL_CONTACT_IN_SCHEDULE:I = 0x8

.field private static final WEIGHT_CALL_IS_FAVORITE:I = 0x4

.field private static final WEIGHT_CALL_LOCATION_GOAL_PHONE_NUMBER:I = 0x5

.field private static final WEIGHT_CALL_LOCATION_SHARE:I = 0xa

.field private static final WEIGHT_CALL_MISSED_CALL:I = 0xc


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;-><init>()V

    return-void
.end method


# virtual methods
.method public getRecommendedList(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    const-string/jumbo v9, "C"

    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForCall;->setLogTag(Ljava/lang/String;)V

    .line 30
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 31
    .local v5, "startTime":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 32
    .local v7, "targetTime":Ljava/util/Calendar;
    const/16 v9, 0xa

    const/16 v10, -0x960

    invoke-virtual {v7, v9, v10}, Ljava/util/Calendar;->add(II)V

    .line 34
    const-wide/16 v9, -0x1

    const/16 v11, 0xc

    invoke-virtual {p0, p1, v9, v10, v11}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForCall;->checkMissedCallList(Landroid/content/Context;JI)V

    .line 36
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 37
    .local v0, "endTime":J
    const-string/jumbo v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "[snowdeer] Rec : Missed Call : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v11, v0, v5

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 41
    const/4 v9, 0x4

    invoke-virtual {p0, p1, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForCall;->checkFavoriteContactist(Landroid/content/Context;I)V

    .line 42
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 43
    const-string/jumbo v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "[snowdeer] Rec : Favorite : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v11, v0, v5

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 49
    const/16 v9, 0x3e8

    const/16 v10, 0x41a0

    .line 50
    const/4 v11, 0x2

    .line 49
    invoke-virtual {p0, p1, v9, v10, v11}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForCall;->checkCallLogList(Landroid/content/Context;III)V

    .line 51
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 52
    const-string/jumbo v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "[snowdeer] Rec : CallLog : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v11, v0, v5

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 56
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 57
    .local v2, "fromTime":Ljava/util/Calendar;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    .line 58
    .local v8, "toTime":Ljava/util/Calendar;
    const/16 v9, 0xa

    const/16 v10, 0xc

    invoke-virtual {v8, v9, v10}, Ljava/util/Calendar;->add(II)V

    .line 59
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v9

    invoke-virtual {v8}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v10

    .line 60
    const/16 v11, 0x8

    .line 59
    invoke-virtual {p0, p1, v9, v10, v11}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForCall;->checkScheduleList(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;I)V

    .line 61
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 62
    const-string/jumbo v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "[snowdeer] Rec : Schedule : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v11, v0, v5

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 66
    sget-object v9, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->LOG_TYPE_CALL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->ordinal()I

    move-result v3

    .line 68
    .local v3, "logType":I
    const/4 v9, 0x6

    .line 67
    invoke-virtual {p0, p1, v3, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForCall;->checkDLCallLogList(Landroid/content/Context;II)V

    .line 69
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 70
    const-string/jumbo v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "[snowdeer] Rec : During DL app : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v11, v0, v5

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 75
    const/16 v9, 0xa

    :try_start_0
    invoke-virtual {p0, p1, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForCall;->checkLocationSharingList(Landroid/content/Context;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 80
    const-string/jumbo v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "[snowdeer] Rec : Location Sharing : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 81
    sub-long v11, v0, v5

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 80
    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 86
    const/4 v9, 0x5

    :try_start_1
    invoke-virtual {p0, p1, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForCall;->checkPOIList(Landroid/content/Context;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 90
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 91
    const-string/jumbo v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "[snowdeer] Rec : POI : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v11, v0, v5

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 95
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForCall;->getContactListFromHashMap(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v4

    .line 97
    .local v4, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 98
    const-string/jumbo v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "[snowdeer] Rec : getContactListFromHashMap : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 99
    sub-long v11, v0, v5

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 98
    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    return-object v4

    .line 76
    .end local v4    # "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    :catch_0
    move-exception v9

    goto :goto_0

    .line 87
    :catch_1
    move-exception v9

    goto :goto_1
.end method
