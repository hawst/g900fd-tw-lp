.class public Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;
.super Landroid/widget/BaseAdapter;
.source "PlaceListAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;


# static fields
.field private static final COLOR:Ljava/lang/String; = "#2FA6C9"


# instance fields
.field private final fInflater:Landroid/view/LayoutInflater;

.field private placeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;",
            ">;"
        }
    .end annotation
.end field

.field final placeService:Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;

.field private searchString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "aContext"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 24
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/places/PlaceServiceFactory;->getPlaceService()Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->placeService:Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;

    .line 27
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->searchString:Ljava/lang/String;

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->placeList:Ljava/util/List;

    .line 34
    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 33
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->fInflater:Landroid/view/LayoutInflater;

    .line 35
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->placeList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;)Ljava/util/List;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->placeList:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->placeList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter$1;-><init>(Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;)V

    .line 118
    .local v0, "filter":Landroid/widget/Filter;
    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->placeList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 49
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 54
    move-object v0, p2

    .line 56
    .local v0, "lView":Landroid/view/View;
    if-nez v0, :cond_0

    .line 57
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->fInflater:Landroid/view/LayoutInflater;

    .line 58
    const v5, 0x7f030061

    .line 59
    const/4 v6, 0x0

    .line 57
    invoke-virtual {v4, v5, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 62
    :cond_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->placeList:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;

    .line 65
    .local v1, "p":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    const v4, 0x7f0901fe

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 64
    check-cast v3, Landroid/widget/TextView;

    .line 67
    .local v3, "txtPlaceName":Landroid/widget/TextView;
    const v4, 0x7f0901ff

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 66
    check-cast v2, Landroid/widget/TextView;

    .line 69
    .local v2, "txtPlaceAddress":Landroid/widget/TextView;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->getName()Ljava/lang/String;

    move-result-object v4

    .line 70
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->searchString:Ljava/lang/String;

    const-string/jumbo v6, "#2FA6C9"

    sget-object v7, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v7}, Landroid/graphics/Typeface;->getStyle()I

    move-result v7

    .line 69
    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->getHighlightedText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    return-object v0
.end method

.method public setPlaceList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 122
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->placeList:Ljava/util/List;

    .line 123
    return-void
.end method

.method public setSearchString(Ljava/lang/String;)V
    .locals 0
    .param p1, "search"    # Ljava/lang/String;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/PlaceListAdapter;->searchString:Ljava/lang/String;

    .line 127
    return-void
.end method
