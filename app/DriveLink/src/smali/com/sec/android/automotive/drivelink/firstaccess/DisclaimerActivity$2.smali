.class Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$2;
.super Landroid/os/Handler;
.source "DisclaimerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    .line 470
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 474
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$0(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)Landroid/content/Context;

    move-result-object v1

    .line 475
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$0(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 476
    const v3, 0x7f0a026a

    .line 475
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 476
    const/16 v3, 0xbb8

    .line 473
    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 477
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 479
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$0(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 480
    const-string/jumbo v2, "car_mode_on"

    .line 479
    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    .line 480
    const/4 v2, 0x1

    .line 479
    if-ne v1, v2, :cond_0

    .line 481
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$0(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 482
    const-string/jumbo v2, "car_mode_on"

    const/4 v3, 0x0

    .line 481
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 487
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 488
    return-void

    .line 483
    :catch_0
    move-exception v0

    .line 484
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
