.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;
.super Ljava/lang/Object;
.source "DriveLinkServiceSingleton.java"


# static fields
.field public static TAG:Ljava/lang/String;

.field private static sDriveLinkServiceInterfaceImp:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    const-string/jumbo v0, "DriveLinkServiceSingleton"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->TAG:Ljava/lang/String;

    .line 8
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->sDriveLinkServiceInterfaceImp:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static delDriveLinkServiceInterface()V
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->sDriveLinkServiceInterfaceImp:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    if-eqz v0, :cond_0

    .line 24
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->sDriveLinkServiceInterfaceImp:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 26
    :cond_0
    return-void
.end method

.method public static getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    .locals 2

    .prologue
    .line 11
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->sDriveLinkServiceInterfaceImp:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    if-nez v0, :cond_1

    .line 12
    const-class v1, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;

    monitor-enter v1

    .line 13
    :try_start_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->sDriveLinkServiceInterfaceImp:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    if-nez v0, :cond_0

    .line 14
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->sDriveLinkServiceInterfaceImp:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 12
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 19
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->sDriveLinkServiceInterfaceImp:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    return-object v0

    .line 12
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
