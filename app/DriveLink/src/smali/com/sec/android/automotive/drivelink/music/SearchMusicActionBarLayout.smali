.class public Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;
.super Landroid/widget/RelativeLayout;
.source "SearchMusicActionBarLayout.java"


# instance fields
.field private btn_clear:Landroid/widget/ImageButton;

.field private btn_prev:Landroid/widget/ImageButton;

.field private edit_text:Landroid/widget/EditText;

.field private inflater:Landroid/view/LayoutInflater;

.field private mBgView:Landroid/widget/ImageView;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->inflater:Landroid/view/LayoutInflater;

    .line 31
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->mContext:Landroid/content/Context;

    .line 32
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->init()V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->inflater:Landroid/view/LayoutInflater;

    .line 37
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->mContext:Landroid/content/Context;

    .line 38
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->init()V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->inflater:Landroid/view/LayoutInflater;

    .line 44
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->mContext:Landroid/content/Context;

    .line 45
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->init()V

    .line 46
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->edit_text:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->btn_clear:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->inflater:Landroid/view/LayoutInflater;

    .line 50
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->inflater:Landroid/view/LayoutInflater;

    const v1, 0x7f03008e

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 51
    const v0, 0x7f09028c

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->edit_text:Landroid/widget/EditText;

    .line 52
    const v0, 0x7f090289

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->btn_prev:Landroid/widget/ImageButton;

    .line 53
    const v0, 0x7f09028d

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->btn_clear:Landroid/widget/ImageButton;

    .line 54
    const v0, 0x7f090288

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->mBgView:Landroid/widget/ImageView;

    .line 55
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->btn_clear:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->clearText()V

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->showHideClearButton()V

    .line 60
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->edit_text:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 72
    return-void
.end method


# virtual methods
.method clearText()V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->btn_clear:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout$2;-><init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    return-void
.end method

.method public getText()Landroid/text/Editable;
    .locals 2

    .prologue
    .line 117
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->edit_text:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 118
    .local v0, "text":Landroid/text/Editable;
    return-object v0
.end method

.method protected setDayMode()V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->mBgView:Landroid/widget/ImageView;

    const v1, 0x7f020001

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 137
    return-void
.end method

.method protected setNightMode()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->mBgView:Landroid/widget/ImageView;

    const v1, 0x7f08004e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 132
    return-void
.end method

.method public setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->btn_prev:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    return-void
.end method

.method public setOnSearchEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V
    .locals 1
    .param p1, "l"    # Landroid/widget/TextView$OnEditorActionListener;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->edit_text:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 127
    return-void
.end method

.method showHideClearButton()V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;->edit_text:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout$3;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout$3;-><init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 114
    return-void
.end method
