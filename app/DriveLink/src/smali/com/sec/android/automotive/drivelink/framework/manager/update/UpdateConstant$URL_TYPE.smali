.class public final enum Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;
.super Ljava/lang/Enum;
.source "UpdateConstant.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "URL_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CHECK:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;

.field public static final enum DONWLOAD:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 87
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;

    const-string/jumbo v1, "CHECK"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;->CHECK:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;

    const-string/jumbo v1, "DONWLOAD"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;->DONWLOAD:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;

    .line 86
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;->CHECK:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;->DONWLOAD:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
