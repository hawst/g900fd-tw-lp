.class Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$4;
.super Ljava/lang/Object;
.source "MessageComposerActivity.java"

# interfaces
.implements Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 1036
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$4;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$4;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    .line 1036
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/4 v2, 0x0

    .line 1041
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$4;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p2}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1052
    const/4 v0, 0x3

    const-string/jumbo v1, "onMicStateChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "default = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$9(ILjava/lang/String;Ljava/lang/String;)V

    .line 1055
    :goto_0
    return-void

    .line 1043
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->enableQuotesButton(Z)V

    goto :goto_0

    .line 1046
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->enableQuotesButton(Z)V

    goto :goto_0

    .line 1049
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->enableQuotesButton(Z)V

    goto :goto_0

    .line 1041
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
