.class Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$2;
.super Ljava/lang/Object;
.source "NotificationActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    .line 475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRemoveDirect(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V
    .locals 3
    .param p1, "view"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    .param p2, "gotoMulti"    # Z

    .prologue
    .line 508
    const-string/jumbo v0, "[NotificationActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onRemoveDirect:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->removeNotification(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->access$3(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;Z)V

    .line 510
    if-eqz p2, :cond_0

    .line 511
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->finishNotiActivity()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->access$4(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;)V

    .line 513
    :cond_0
    return-void
.end method

.method public onRemoveRequest(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    .locals 2
    .param p1, "view"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .prologue
    .line 480
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "Remove Notification Request Received!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->dismissNotification(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V
    invoke-static {v0, p1, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->access$0(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V

    .line 482
    return-void
.end method

.method public onRemoveRequest(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Lcom/sec/android/automotive/drivelink/notification/INotiCommand;)V
    .locals 2
    .param p1, "view"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    .param p2, "command"    # Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    .prologue
    .line 487
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "Remove Notification and do command Request Received!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    if-eqz p2, :cond_0

    .line 489
    invoke-static {p2}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->access$1(Lcom/sec/android/automotive/drivelink/notification/INotiCommand;)V

    .line 490
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->dismissNotification(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V
    invoke-static {v0, p1, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->access$0(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V

    .line 495
    :goto_0
    return-void

    .line 492
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->dismissNotification(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V
    invoke-static {v0, p1, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->access$0(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V

    goto :goto_0
.end method

.method public onSetVoiceFlowIdRequest(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowId"    # Ljava/lang/String;

    .prologue
    .line 500
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 501
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->setFlowId(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->access$2(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;Ljava/lang/String;)V

    .line 504
    :cond_0
    return-void
.end method
