.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLCallLog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;,
        Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getDate()J
.end method

.method public abstract getDuration()Ljava/lang/String;
.end method

.method public abstract getLogType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getPhoneNumber()Ljava/lang/String;
.end method

.method public abstract getType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;
.end method
