.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestChangeMessageStatusToRead;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestChangeMessageStatusToRead.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDLMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

.field private result:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;
    .param p3, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .prologue
    const/4 v0, 0x0

    .line 17
    invoke-direct {p0, p3}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 11
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestChangeMessageStatusToRead;->mContext:Landroid/content/Context;

    .line 12
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestChangeMessageStatusToRead;->mDLMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestChangeMessageStatusToRead;->result:Z

    .line 19
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestChangeMessageStatusToRead;->mContext:Landroid/content/Context;

    .line 20
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestChangeMessageStatusToRead;->mDLMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .line 21
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestChangeMessageStatusToRead;->mContext:Landroid/content/Context;

    .line 42
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestChangeMessageStatusToRead;->mDLMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .line 43
    return-void
.end method

.method protected process()V
    .locals 3

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestChangeMessageStatusToRead;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 26
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 27
    .local v0, "contentManager":Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestChangeMessageStatusToRead;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestChangeMessageStatusToRead;->mDLMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->changeMessageStatusToRead(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestChangeMessageStatusToRead;->result:Z

    .line 28
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestChangeMessageStatusToRead;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 33
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMessageListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;

    move-result-object v0

    .line 34
    .local v0, "messageListener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;
    if-eqz v0, :cond_0

    .line 35
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestChangeMessageStatusToRead;->result:Z

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;->onResponseRequestChangeMessageStatusToRead(Z)V

    .line 37
    :cond_0
    return-void
.end method
