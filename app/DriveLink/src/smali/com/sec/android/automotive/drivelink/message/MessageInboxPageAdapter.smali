.class public Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "MessageInboxPageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static final ITEM_NUM_PER_PAGE:I = 0x4

.field private static final MAX_PAGE_TO_LOOP:I = 0x4e20


# instance fields
.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mInboxList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;"
        }
    .end annotation
.end field

.field private mInitialPosition:I

.field private mIsDayMode:Z

.field private mPageCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "clickListener"    # Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;>;"
    const/4 v1, 0x1

    .line 40
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 38
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mIsDayMode:Z

    .line 42
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mContext:Landroid/content/Context;

    .line 43
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mInboxList:Ljava/util/ArrayList;

    .line 44
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    .line 45
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getPageCount()I

    move-result v0

    .line 46
    .local v0, "pageCount":I
    if-le v0, v1, :cond_0

    .line 48
    const/16 v1, 0x2710

    rem-int/2addr v1, v0

    rsub-int v1, v1, 0x2710

    .line 47
    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mInitialPosition:I

    .line 49
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mPageCount:I

    .line 54
    :goto_0
    return-void

    .line 51
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mInitialPosition:I

    .line 52
    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mPageCount:I

    goto :goto_0
.end method

.method private getDate(J)Ljava/lang/String;
    .locals 13
    .param p1, "date"    # J

    .prologue
    const/4 v9, 0x6

    const/4 v10, 0x1

    .line 276
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 277
    .local v0, "cal":Ljava/util/Calendar;
    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 279
    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v7

    .line 280
    .local v7, "today":I
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 282
    .local v4, "lToday":J
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 283
    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 285
    .local v1, "day":I
    sub-int v2, v7, v1

    .line 286
    .local v2, "diffDay":I
    if-nez v2, :cond_1

    .line 287
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mContext:Landroid/content/Context;

    invoke-static {v8}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v3

    .line 288
    .local v3, "is24":Z
    if-eqz v3, :cond_0

    .line 289
    const-string/jumbo v8, "kk:mm"

    new-instance v9, Ljava/util/Date;

    invoke-direct {v9, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-static {v8, v9}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 304
    .end local v3    # "is24":Z
    .local v6, "ret":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 291
    .end local v6    # "ret":Ljava/lang/String;
    .restart local v3    # "is24":Z
    :cond_0
    const-string/jumbo v8, "hh:mm AA"

    new-instance v9, Ljava/util/Date;

    invoke-direct {v9, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-static {v8, v9}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 293
    .restart local v6    # "ret":Ljava/lang/String;
    goto :goto_0

    .end local v3    # "is24":Z
    .end local v6    # "ret":Ljava/lang/String;
    :cond_1
    if-ne v2, v10, :cond_2

    .line 294
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mContext:Landroid/content/Context;

    const v9, 0x7f0a016c

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string/jumbo v12, ""

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 295
    .restart local v6    # "ret":Ljava/lang/String;
    goto :goto_0

    .line 301
    .end local v6    # "ret":Ljava/lang/String;
    :cond_2
    const-string/jumbo v8, "MM/dd"

    new-instance v9, Ljava/util/Date;

    invoke-direct {v9, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-static {v8, v9}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "ret":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 243
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 244
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 248
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mPageCount:I

    return v0
.end method

.method public getInitialPosition()I
    .locals 1

    .prologue
    .line 267
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mInitialPosition:I

    return v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 312
    const/4 v0, -0x2

    return v0
.end method

.method public getPageCount()I
    .locals 3

    .prologue
    .line 252
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mInboxList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    div-int/lit8 v0, v2, 0x4

    .line 253
    .local v0, "page":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mInboxList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    rem-int/lit8 v1, v2, 0x4

    .line 254
    .local v1, "remainder":I
    if-lez v1, :cond_0

    .line 255
    add-int/lit8 v0, v0, 0x1

    .line 258
    :cond_0
    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 21
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 72
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v12

    .line 73
    .local v12, "layoutInflater":Landroid/view/LayoutInflater;
    const v17, 0x7f03004e

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v16

    .line 75
    .local v16, "v":Landroid/view/View;
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .local v13, "listItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/widget/RelativeLayout;>;"
    const v17, 0x7f0901d0

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    const v17, 0x7f0901d1

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    const v17, 0x7f0901d2

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    const v17, 0x7f0901d3

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v5

    .line 83
    .local v5, "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getPageCount()I

    move-result v17

    rem-int v17, p2, v17

    mul-int/lit8 v10, v17, 0x4

    .line 86
    .local v10, "index":I
    const/4 v3, 0x0

    .line 87
    .local v3, "bitmap":Landroid/graphics/Bitmap;
    const/4 v14, 0x0

    .line 90
    .local v14, "maskedBitmap":Landroid/graphics/Bitmap;
    const/4 v8, 0x1

    .line 91
    .local v8, "i":I
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-nez v17, :cond_0

    .line 228
    move-object/from16 v0, v16

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 229
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    .line 230
    return-object v16

    .line 91
    .restart local p1    # "container":Landroid/view/ViewGroup;
    :cond_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout;

    .line 92
    .local v11, "layout":Landroid/widget/RelativeLayout;
    new-instance v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;

    const/16 v17, 0x0

    move-object/from16 v0, v17

    invoke-direct {v7, v0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;)V

    .line 95
    .local v7, "holder":Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;
    const v17, 0x7f0901cb

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/ImageView;

    .line 94
    move-object/from16 v0, v17

    iput-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    .line 97
    const v17, 0x7f0901cc

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/ImageView;

    .line 96
    move-object/from16 v0, v17

    iput-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->ivLogsMaskImage:Landroid/widget/ImageView;

    .line 98
    const v17, 0x7f0901cd

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    iput-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    .line 99
    const v17, 0x7f0901ce

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    iput-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->tvTime:Landroid/widget/TextView;

    .line 101
    const v17, 0x7f0901cf

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 100
    move-object/from16 v0, v17

    iput-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->ivInboxIcon:Landroid/widget/TextView;

    .line 103
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mInboxList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-le v0, v10, :cond_8

    .line 104
    invoke-virtual {v11, v8}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 105
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mInboxList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 108
    .local v9, "inbox":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    .line 109
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getDLContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v19

    .line 108
    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-interface {v5, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 132
    if-nez v3, :cond_4

    .line 133
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v17

    move-object/from16 v0, v17

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v17, v0

    const/16 v19, 0x2

    move/from16 v0, v17

    move/from16 v1, v19

    if-ne v0, v1, :cond_3

    .line 134
    iget-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    .line 135
    const v19, 0x7f02024e

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 144
    :goto_1
    const-string/jumbo v15, ""

    .line 145
    .local v15, "name":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getDLContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v17

    if-eqz v17, :cond_6

    .line 146
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getDLContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v17

    const-string/jumbo v19, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_5

    .line 147
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getDLContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v17

    if-eqz v17, :cond_5

    .line 148
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getDLContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v15

    .line 183
    :cond_1
    :goto_2
    iget-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    iget-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->tvTime:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getLastReceivedDate()J

    move-result-wide v19

    move-object/from16 v0, p0

    move-wide/from16 v1, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getDate(J)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mIsDayMode:Z

    move/from16 v17, v0

    if-eqz v17, :cond_7

    .line 187
    iget-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 188
    const v20, 0x7f08002f

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    .line 187
    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 189
    iget-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->ivLogsMaskImage:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    .line 190
    const v19, 0x7f020209

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 208
    :goto_3
    iget-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->ivInboxIcon:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v19, 0x8

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 224
    .end local v9    # "inbox":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    .end local v15    # "name":Ljava/lang/String;
    :cond_2
    :goto_4
    add-int/lit8 v10, v10, 0x1

    .line 225
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 137
    .restart local v9    # "inbox":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    :cond_3
    iget-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    .line 138
    const v19, 0x7f02023d

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 141
    :cond_4
    iget-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_1

    .line 150
    .restart local v15    # "name":Ljava/lang/String;
    :cond_5
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getPhoneNumber()Ljava/lang/String;

    move-result-object v15

    .line 151
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    .line 152
    const-string/jumbo v19, "country_detector"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 151
    check-cast v4, Landroid/location/CountryDetector;

    .line 154
    .local v4, "detector":Landroid/location/CountryDetector;
    invoke-virtual {v4}, Landroid/location/CountryDetector;->detectCountry()Landroid/location/Country;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/location/Country;->getCountryIso()Ljava/lang/String;

    move-result-object v17

    .line 153
    move-object/from16 v0, v17

    invoke-static {v15, v0}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 155
    if-nez v15, :cond_1

    .line 156
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getPhoneNumber()Ljava/lang/String;

    move-result-object v15

    .line 171
    goto/16 :goto_2

    .line 172
    .end local v4    # "detector":Landroid/location/CountryDetector;
    :cond_6
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getPhoneNumber()Ljava/lang/String;

    move-result-object v15

    .line 173
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    .line 174
    const-string/jumbo v19, "country_detector"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 173
    check-cast v4, Landroid/location/CountryDetector;

    .line 176
    .restart local v4    # "detector":Landroid/location/CountryDetector;
    invoke-virtual {v4}, Landroid/location/CountryDetector;->detectCountry()Landroid/location/Country;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/location/Country;->getCountryIso()Ljava/lang/String;

    move-result-object v17

    .line 175
    move-object/from16 v0, v17

    invoke-static {v15, v0}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 179
    .local v6, "formName":Ljava/lang/String;
    if-eqz v6, :cond_1

    .line 180
    move-object v15, v6

    goto/16 :goto_2

    .line 193
    .end local v4    # "detector":Landroid/location/CountryDetector;
    .end local v6    # "formName":Ljava/lang/String;
    :cond_7
    iget-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 194
    const v20, 0x7f080030

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    .line 193
    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 195
    iget-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->ivLogsMaskImage:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    .line 196
    const v19, 0x7f02020c

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 197
    const v17, 0x7f020002

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto/16 :goto_3

    .line 212
    .end local v9    # "inbox":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    .end local v15    # "name":Ljava/lang/String;
    :cond_8
    iget-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    const/16 v19, 0x8

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 213
    iget-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->ivLogsMaskImage:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    const/16 v19, 0x8

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 214
    iget-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v19, 0x8

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 215
    iget-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->tvTime:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v19, 0x8

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 216
    iget-object v0, v7, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter$ViewHolder;->ivInboxIcon:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v19, 0x8

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 217
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mIsDayMode:Z

    move/from16 v17, v0

    if-nez v17, :cond_2

    .line 220
    const v17, 0x7f020002

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto/16 :goto_4
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 263
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected setDayMode()V
    .locals 1

    .prologue
    .line 238
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mIsDayMode:Z

    .line 239
    return-void
.end method

.method public setInboxList(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "inboxList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mInboxList:Ljava/util/ArrayList;

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getPageCount()I

    move-result v0

    .line 60
    .local v0, "pageCount":I
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 62
    const/16 v1, 0x2710

    rem-int/2addr v1, v0

    rsub-int v1, v1, 0x2710

    .line 61
    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mInitialPosition:I

    .line 63
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mPageCount:I

    .line 68
    :goto_0
    return-void

    .line 65
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mInitialPosition:I

    .line 66
    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mPageCount:I

    goto :goto_0
.end method

.method protected setNightMode()V
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->mIsDayMode:Z

    .line 235
    return-void
.end method
