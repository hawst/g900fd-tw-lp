.class Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageInstallObserver;
.super Landroid/content/pm/IPackageInstallObserver$Stub;
.source "ApplicationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PackageInstallObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)V
    .locals 0

    .prologue
    .line 395
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    invoke-direct {p0}, Landroid/content/pm/IPackageInstallObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public packageInstalled(Ljava/lang/String;I)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 398
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)Ljava/lang/String;

    move-result-object v1

    .line 399
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "PackageInstallObserver packageInstalled onInstalledPackaged"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 400
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->onInstalledPackaged:Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageInstalled;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageInstalled;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "packageName"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 401
    const-string/jumbo v3, " returnCode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 399
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 398
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->onInstalledPackaged:Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageInstalled;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageInstalled;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 405
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    iput-object p1, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->pkgname:Ljava/lang/String;

    .line 406
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    iput p2, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->returncode:I

    .line 408
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->access$4(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 409
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->access$4(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 413
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method
