.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLLocation.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final INVALID_LATLONG_VALUE:D = 9999.0

.field public static final LOCATION_TYPE_DEST:I = 0x3

.field public static final LOCATION_TYPE_MYPLACE:I = 0x1

.field public static final LOCATION_TYPE_SCHEDULE:I = 0x0

.field public static final LOCATION_TYPE_SHARED:I = 0x2

.field public static final RAIDIUS_ERROR:D = 0.001

.field private static final serialVersionUID:J = 0x7acca97c5c9807aaL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    return-void
.end method

.method public static isValidCoords(DD)Z
    .locals 2
    .param p0, "latitude"    # D
    .param p2, "longitude"    # D

    .prologue
    .line 58
    const-wide v0, -0x3fa9800000000000L    # -90.0

    cmpl-double v0, p0, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x4056800000000000L    # 90.0

    cmpg-double v0, p0, v0

    if-gtz v0, :cond_0

    const-wide v0, -0x3f99800000000000L    # -180.0

    cmpl-double v0, p2, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x4066800000000000L    # 180.0

    cmpg-double v0, p2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract getCount()I
.end method

.method public abstract getLatitude()D
.end method

.method public abstract getLocationAddress()Ljava/lang/String;
.end method

.method public abstract getLocationID()Ljava/lang/String;
.end method

.method public abstract getLocationName()Ljava/lang/String;
.end method

.method public abstract getLocationType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;
.end method

.method public abstract getLongitude()D
.end method

.method public abstract getTimestamp()J
.end method

.method public abstract isSamePosition(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
.end method

.method public abstract isValidAddres()Z
.end method

.method public abstract isValidCoords()Z
.end method

.method public abstract setLatitude(D)V
.end method

.method public abstract setLocationAddress(Ljava/lang/String;)V
.end method

.method public abstract setLocationID(Ljava/lang/String;)V
.end method

.method public abstract setLocationName(Ljava/lang/String;)V
.end method

.method public abstract setLongitude(D)V
.end method
