.class public Lcom/sec/android/automotive/drivelink/common/DLFeatures;
.super Ljava/lang/Object;
.source "DLFeatures.java"


# static fields
.field public static final DEBUG:Ljava/lang/String; = "DEBUG"

.field public static final DELETE_USER_DATA:Ljava/lang/String; = "DELETE_USER_DATA"

.field private static DlFeatureMap:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final FALSE:Ljava/lang/String; = "FALSE"

.field public static final HIDDEN_FEATURE_DEBUG_SETTING:Ljava/lang/String; = "HIDDEN_FEATURE_DEBUG_SETTING"

.field public static final HIDDEN_FEATURE_DISP_USERTURN:Ljava/lang/String; = "HIDDEN_FEATURE_DISP_USERTURN"

.field public static final HIDDEN_FEATURE_DISP_WAKEUP_NOTI:Ljava/lang/String; = "HIDDEN_FEATURE_DISP_WAKEUP_NOTI"

.field public static final HIDDEN_FEATURE_OBD_TRIPGEN:Ljava/lang/String; = "OBD-TRIPGEN"

.field public static final HIDDEN_FEATURE_OBD_VALIDATION:Ljava/lang/String; = "OBD-VALIDATION"

.field public static final MODEL:Ljava/lang/String; = "MODEL"

.field public static final MULTI_WINDOW_ENABLE:Ljava/lang/String; = "MULTI_WINDOW_ENABLE"

.field public static final PRODUCT:Ljava/lang/String; = "PRODUCT"

.field private static final TAG:Ljava/lang/String; = "DLFeatures"

.field public static final TRUE:Ljava/lang/String; = "TRUE"

.field public static final VAC_CLIENT_DM:Ljava/lang/String; = "VAC_CLIENT_DM"

.field public static final VAC_CONTACT_MATCHER:Ljava/lang/String; = "VAC_CONTACT_MATCHER"

.field public static final VAC_CONTACT_SEARCH_HIGHLIGHT:Ljava/lang/String; = "VAC_CONTACT_SEARCH_HIGHLIGHT"

.field public static final VAC_DRIVELINK:Ljava/lang/String; = "VAC_DRIVELINK"

.field public static final VIP_DEMO:Ljava/lang/String; = "VIP_DEMO"

.field private static isInitialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isInitialized:Z

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->DlFeatureMap:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 80
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isInitialized:Z

    if-nez v0, :cond_0

    .line 81
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->initFeatureMap()V

    .line 84
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->DlFeatureMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private static initFeatureMap()V
    .locals 4

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->DlFeatureMap:Ljava/util/HashMap;

    const-string/jumbo v1, "VIP_DEMO"

    const-string/jumbo v2, "FALSE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->DlFeatureMap:Ljava/util/HashMap;

    const-string/jumbo v1, "DEBUG"

    const-string/jumbo v2, "TRUE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->DlFeatureMap:Ljava/util/HashMap;

    const-string/jumbo v1, "MODEL"

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->DlFeatureMap:Ljava/util/HashMap;

    const-string/jumbo v1, "PRODUCT"

    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->DlFeatureMap:Ljava/util/HashMap;

    const-string/jumbo v1, "VAC_DRIVELINK"

    const-string/jumbo v2, "TRUE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->DlFeatureMap:Ljava/util/HashMap;

    const-string/jumbo v1, "VAC_CLIENT_DM"

    const-string/jumbo v2, "TRUE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->DlFeatureMap:Ljava/util/HashMap;

    const-string/jumbo v1, "VAC_CONTACT_MATCHER"

    const-string/jumbo v2, "TRUE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->DlFeatureMap:Ljava/util/HashMap;

    const-string/jumbo v1, "VAC_CONTACT_SEARCH_HIGHLIGHT"

    const-string/jumbo v2, "FALSE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->DlFeatureMap:Ljava/util/HashMap;

    const-string/jumbo v2, "MULTI_WINDOW_ENABLE"

    .line 49
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v0, v3, :cond_0

    const-string/jumbo v0, "TRUE"

    .line 48
    :goto_0
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->DlFeatureMap:Ljava/util/HashMap;

    const-string/jumbo v1, "HIDDEN_FEATURE_DISP_USERTURN"

    const-string/jumbo v2, "FALSE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->DlFeatureMap:Ljava/util/HashMap;

    const-string/jumbo v1, "HIDDEN_FEATURE_DISP_WAKEUP_NOTI"

    const-string/jumbo v2, "FALSE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->DlFeatureMap:Ljava/util/HashMap;

    const-string/jumbo v1, "DELETE_USER_DATA"

    const-string/jumbo v2, "FALSE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->DlFeatureMap:Ljava/util/HashMap;

    const-string/jumbo v1, "HIDDEN_FEATURE_DEBUG_SETTING"

    const-string/jumbo v2, "FALSE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isInitialized:Z

    .line 55
    return-void

    .line 49
    :cond_0
    const-string/jumbo v0, "FALSE"

    goto :goto_0
.end method

.method public static initValue(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->initFeatureMap()V

    .line 60
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 62
    .local v0, "featureList":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "DL Feature Info"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 63
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "\n  DEBUG : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "DEBUG"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 64
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "\n  MODEL : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "MODEL"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 66
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "\n  is K Model : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isKmodel()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 68
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "\nMULTI_WINDOW_ENABLE : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 69
    const-string/jumbo v2, "MULTI_WINDOW_ENABLE"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 68
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 71
    const-string/jumbo v1, "DLFeatures"

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    return-void
.end method

.method public static isKmodel()Z
    .locals 2

    .prologue
    .line 98
    const-string/jumbo v1, "PRODUCT"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 99
    .local v0, "productName":Ljava/lang/String;
    const-string/jumbo v1, "k3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "klte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 100
    :cond_0
    const/4 v1, 0x1

    .line 103
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isTrue(Ljava/lang/String;)Z
    .locals 3
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 107
    const/4 v0, 0x0

    .line 109
    .local v0, "ret":Z
    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "TRUE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 110
    const/4 v0, 0x1

    .line 113
    :cond_0
    return v0
.end method

.method public static setValue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 75
    const-string/jumbo v0, "DLFeatures"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[setValue] key:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", value :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->DlFeatureMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    return-void
.end method
