.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$18;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getSearchedItemOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 1882
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 1886
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->hideInputMethod()V
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$61(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    .line 1887
    const/4 v1, 0x0

    .line 1889
    .local v1, "currentItem":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v6

    .line 1890
    mul-int/lit8 v6, v6, 0x4

    .line 1889
    add-int/2addr v5, v6

    add-int/lit8 v1, v5, -0x1

    .line 1892
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListMode:I
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$43(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)I

    move-result v5

    const/16 v6, 0x10

    if-ne v5, v6, :cond_3

    .line 1894
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mContactChoices:Ljava/util/List;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$20(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 1896
    .local v4, "user":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 1898
    .local v0, "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1899
    .local v3, "numList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    new-instance v6, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    .line 1900
    invoke-virtual {v4}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/contacts/ContactData;

    iget v7, v5, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    invoke-virtual {v4}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v5

    .line 1901
    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/contacts/ContactData;

    iget-object v5, v5, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-direct {v6, v7, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;-><init>(ILjava/lang/String;)V

    .line 1899
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1904
    const-string/jumbo v5, "DM_SMS_CONTACT_SEARCH_LIST"

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v2

    .line 1905
    .local v2, "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v2, :cond_1

    iget v5, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchListType:I

    if-ltz v5, :cond_1

    .line 1906
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 1907
    iget v6, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchListType:I

    .line 1906
    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getTypePhoneNumber(Lcom/vlingo/core/internal/contacts/ContactMatch;I)I
    invoke-static {v5, v4, v6}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$66(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/vlingo/core/internal/contacts/ContactMatch;I)I

    move-result v5

    iput v5, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    .line 1912
    :goto_0
    if-eqz v2, :cond_0

    .line 1913
    const/4 v5, -0x1

    iput v5, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchListType:I

    .line 1916
    :cond_0
    iput-object v4, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 1917
    iput v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserIndex:I

    .line 1919
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMessageBody:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$68(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1920
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mMessageBody:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$68(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageText:Ljava/lang/String;

    .line 1922
    const-string/jumbo v5, "DM_SMS_COMPLETE"

    .line 1921
    invoke-static {v5, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 1923
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-static {v5, v9}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$69(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Ljava/lang/String;)V

    .line 1939
    .end local v0    # "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v2    # "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v3    # "numList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    .end local v4    # "user":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :goto_1
    return-void

    .line 1909
    .restart local v0    # "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .restart local v2    # "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .restart local v3    # "numList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    .restart local v4    # "user":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_1
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getMainPhoneNumber(Lcom/vlingo/core/internal/contacts/ContactMatch;)I
    invoke-static {v5, v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$67(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/vlingo/core/internal/contacts/ContactMatch;)I

    move-result v5

    iput v5, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    goto :goto_0

    .line 1926
    :cond_2
    const-string/jumbo v5, "DM_SMS_COMPOSE"

    .line 1925
    invoke-static {v5, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_1

    .line 1930
    .end local v0    # "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v2    # "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v3    # "numList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    .end local v4    # "user":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_3
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxList:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$33(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 1931
    .local v4, "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/MessageHolder;->getMessageHolder()Lcom/sec/android/automotive/drivelink/message/MessageHolder;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/sec/android/automotive/drivelink/message/MessageHolder;->setInbox(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;)V

    .line 1934
    const-string/jumbo v5, "DM_SMS_READBACK"

    .line 1933
    invoke-static {v5, v9}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_1
.end method
