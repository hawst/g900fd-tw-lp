.class public Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;
.super Ljava/lang/Object;
.source "LocationFriendSharedTableHandler.java"


# static fields
.field private static final TABLE_LOCATION_SHARED:Ljava/lang/String; = "tb_friend_location_shared"

.field private static final TAG:Ljava/lang/String; = "LocationFriendSharedTableHandler"


# instance fields
.field mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;)V
    .locals 1
    .param p1, "driveLinkLocationTableHandler"    # Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 26
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 27
    return-void
.end method

.method private addLocationFriendShared(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z
    .locals 10
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "buddy"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    const/4 v7, 0x0

    .line 157
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 158
    .local v3, "time":Ljava/util/Calendar;
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 159
    .local v4, "timestamp":J
    const-wide/16 v1, -0x1

    .line 161
    .local v1, "result":J
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v8

    if-nez v8, :cond_1

    .line 180
    :cond_0
    :goto_0
    return v7

    .line 166
    :cond_1
    :try_start_0
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 167
    .local v6, "values":Landroid/content/ContentValues;
    const-string/jumbo v8, "friend_phonenumber"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    const-string/jumbo v8, "friend_name"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getFriendName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const-string/jumbo v8, "status"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getStatus()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->getValue()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 170
    const-string/jumbo v8, "timestamp"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 171
    const-string/jumbo v8, "count"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 172
    const-string/jumbo v8, "location_id"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationID()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string/jumbo v8, "creation_date"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 175
    const-string/jumbo v8, "tb_friend_location_shared"

    const/4 v9, 0x0

    invoke-virtual {p1, v8, v9, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    .line 180
    .end local v6    # "values":Landroid/content/ContentValues;
    :goto_1
    const-wide/16 v8, -0x1

    cmp-long v8, v1, v8

    if-eqz v8, :cond_0

    const/4 v7, 0x1

    goto :goto_0

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method private existsLocationShared(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 215
    if-eqz p2, :cond_0

    const-string/jumbo v4, ""

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 216
    :cond_0
    const-string/jumbo v4, "LocationFriendSharedTableHandler"

    const-string/jumbo v5, "Trying to find empty phone in frineds locations shared"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    :cond_1
    :goto_0
    return v3

    .line 221
    :cond_2
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "select friend_phonenumber from tb_friend_location_shared where friend_phonenumber = \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 223
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 221
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 224
    .local v2, "sql":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 226
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    .line 229
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 230
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 231
    const/4 v3, 0x1

    goto :goto_0

    .line 234
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 235
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v2    # "sql":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 236
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method private getLocationSharedFromCursor(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 244
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;-><init>()V

    .line 245
    .local v0, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    const/4 v3, 0x0

    .line 246
    .local v3, "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    const/4 v4, 0x0

    .line 248
    .local v4, "locationId":Ljava/lang/String;
    const/4 v1, 0x0

    .line 249
    .local v1, "i":I
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 250
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setPhoneNumber(Ljava/lang/String;)V

    .line 251
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setFriendName(Ljava/lang/String;)V

    .line 253
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 252
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->getTypeFromInt(I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setStatus(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;)V

    .line 254
    new-instance v5, Ljava/util/Date;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setSharedDate(Ljava/util/Date;)V

    .line 257
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->getLocationsTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;

    move-result-object v5

    .line 258
    invoke-virtual {v5, p1, v4}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->getLocation(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v3

    .line 259
    if-nez v3, :cond_0

    .line 262
    const/4 v0, 0x0

    .line 268
    .end local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    :goto_0
    return-object v0

    .line 265
    .restart local v0    # "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    :cond_0
    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 266
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->setSharedAddress(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateLocationFriendShared(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z
    .locals 13
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "buddy"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 185
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 186
    .local v3, "time":Ljava/util/Calendar;
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 187
    .local v4, "timestamp":J
    const-wide/16 v1, 0x0

    .line 189
    .local v1, "result":J
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v11

    if-nez v11, :cond_1

    .line 211
    :cond_0
    :goto_0
    return v9

    .line 194
    :cond_1
    :try_start_0
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 195
    .local v6, "values":Landroid/content/ContentValues;
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getFriendName()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_2

    .line 196
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getFriendName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_2

    .line 197
    const-string/jumbo v11, "friend_name"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getFriendName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :cond_2
    const-string/jumbo v11, "status"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getStatus()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->getValue()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v6, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 199
    const-string/jumbo v11, "timestamp"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v6, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 200
    const-string/jumbo v11, "count"

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v6, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 201
    const-string/jumbo v11, "location_id"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationID()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const-string/jumbo v8, "friend_phonenumber = ?"

    .line 204
    .local v8, "whereClause":Ljava/lang/String;
    const/4 v11, 0x1

    new-array v7, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v7, v11

    .line 205
    .local v7, "whereArgs":[Ljava/lang/String;
    const-string/jumbo v11, "tb_friend_location_shared"

    invoke-virtual {p1, v11, v6, v8, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v11

    int-to-long v1, v11

    .line 211
    .end local v6    # "values":Landroid/content/ContentValues;
    .end local v7    # "whereArgs":[Ljava/lang/String;
    .end local v8    # "whereClause":Ljava/lang/String;
    :goto_1
    const-wide/16 v11, 0x0

    cmp-long v11, v1, v11

    if-eqz v11, :cond_0

    move v9, v10

    goto :goto_0

    .line 207
    :catch_0
    move-exception v0

    .line 208
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public createLocationsTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 33
    :try_start_0
    const-string/jumbo v1, "CREATE TABLE IF NOT EXISTS tb_friend_location_shared (friend_phonenumber VARCHAR PRIMARY KEY,  friend_name VARCHAR, status INTEGER, timestamp INTEGER, count INTEGER, location_id VARCHAR, creation_date INTEGER, FOREIGN KEY(location_id) REFERENCES tb_locations(location_id));"

    .line 39
    .local v1, "sql":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    .end local v1    # "sql":Ljava/lang/String;
    :goto_0
    return-void

    .line 40
    :catch_0
    move-exception v0

    .line 41
    .local v0, "e":Landroid/database/SQLException;
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    goto :goto_0
.end method

.method public dropLocationsTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 48
    :try_start_0
    const-string/jumbo v1, "DROP TABLE IF EXISTS tb_friend_location_shared;"

    .line 49
    .local v1, "sql":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    .end local v1    # "sql":Ljava/lang/String;
    :goto_0
    return-void

    .line 50
    :catch_0
    move-exception v0

    .line 51
    .local v0, "e":Landroid/database/SQLException;
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    goto :goto_0
.end method

.method public loadLocationFriendPendingList(Landroid/database/sqlite/SQLiteDatabase;IJ)Ljava/util/ArrayList;
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "maxCount"    # I
    .param p3, "fromTimestamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "IJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .local v2, "sharedLocationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    const-string/jumbo v3, "select location_id, friend_phonenumber, friend_name, status, creation_date from tb_friend_location_shared"

    .line 120
    .local v3, "sql":Ljava/lang/String;
    const-wide/16 v4, 0x0

    cmp-long v4, p3, v4

    if-lez v4, :cond_0

    .line 121
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, " where status = 0 and creation_date >= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 124
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, " order by timestamp desc"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 126
    if-lez p2, :cond_1

    .line 127
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, " LIMIT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 130
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 132
    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 134
    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_2

    .line 152
    :goto_0
    return-object v2

    .line 137
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_3

    .line 138
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 143
    :cond_3
    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;->getLocationSharedFromCursor(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    move-result-object v1

    .line 145
    .local v1, "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    if-eqz v1, :cond_4

    .line 146
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 150
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public loadLocationFriendSharedList(Landroid/database/sqlite/SQLiteDatabase;IJ)Ljava/util/ArrayList;
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "maxCount"    # I
    .param p3, "fromTimestamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "IJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 75
    .local v2, "sharedLocationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    const-string/jumbo v3, "select location_id, friend_phonenumber, friend_name, status, creation_date from tb_friend_location_shared"

    .line 78
    .local v3, "sql":Ljava/lang/String;
    const-wide/16 v4, 0x0

    cmp-long v4, p3, v4

    if-lez v4, :cond_0

    .line 79
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, " where status = 1 and timestamp >= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 82
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, " order by timestamp desc"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 84
    if-lez p2, :cond_1

    .line 85
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, " LIMIT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 88
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 90
    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 92
    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_2

    .line 110
    :goto_0
    return-object v2

    .line 95
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_3

    .line 96
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 101
    :cond_3
    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;->getLocationSharedFromCursor(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    move-result-object v1

    .line 103
    .local v1, "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    if-eqz v1, :cond_4

    .line 104
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 108
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public saveLocationFriendShared(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "buddy"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    const/4 v0, 0x0

    .line 57
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v1

    if-nez v1, :cond_2

    .line 58
    :cond_0
    const-string/jumbo v1, "LocationFriendSharedTableHandler"

    const-string/jumbo v2, "Invalid buddy object to save."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    :cond_1
    :goto_0
    return v0

    .line 62
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->getLocationsTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;

    move-result-object v1

    .line 63
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->saveLocation(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 66
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;->existsLocationShared(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 67
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;->updateLocationFriendShared(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z

    move-result v0

    goto :goto_0

    .line 68
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;->addLocationFriendShared(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z

    move-result v0

    goto :goto_0
.end method
