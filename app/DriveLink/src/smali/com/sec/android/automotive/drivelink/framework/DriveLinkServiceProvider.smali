.class public Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
.super Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;
.source "DriveLinkServiceProvider.java"


# static fields
.field public static TAG:Ljava/lang/String;


# instance fields
.field private mOnDriveLinkCallLogListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;

.field private mOnDriveLinkConnectivityListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;

.field private mOnDriveLinkContactListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;

.field private mOnDriveLinkDLCallMessageListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDLCallMessageLogListener;

.field private mOnDriveLinkDrivingStatusListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;

.field private mOnDriveLinkEventListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;

.field private mOnDriveLinkFavoriteContactListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;

.field private mOnDriveLinkLocationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

.field private mOnDriveLinkMessageListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;

.field private mOnDriveLinkMusicListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

.field private mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

.field private mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

.field private mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

.field private mOnDriveLinkRecommendedContactForCallListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;

.field private mOnDriveLinkRecommendedContactForMessageListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;

.field private mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

.field private mOnDriveLinkScheduleListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkScheduleListener;

.field private mOnDriveLinkUpdateListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

.field private mbInitialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-string/jumbo v0, "DriveLinkServiceInterfaceImp"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 67
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;-><init>()V

    .line 44
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkEventListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;

    .line 45
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkContactListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;

    .line 46
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkFavoriteContactListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;

    .line 47
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkRecommendedContactForCallListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;

    .line 48
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkRecommendedContactForMessageListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;

    .line 49
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkCallLogListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;

    .line 50
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkMessageListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;

    .line 51
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkScheduleListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkScheduleListener;

    .line 52
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkMusicListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

    .line 53
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    .line 54
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    .line 55
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkLocationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    .line 56
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkDrivingStatusListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;

    .line 57
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkConnectivityListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;

    .line 58
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    .line 59
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    .line 61
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkDLCallMessageListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDLCallMessageLogListener;

    .line 62
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkUpdateListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mbInitialized:Z

    .line 68
    return-void
.end method

.method private setInitialized(Z)V
    .locals 0
    .param p1, "bInit"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mbInitialized:Z

    .line 72
    return-void
.end method


# virtual methods
.method public addFavoriteContact(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 555
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->addFavoriteContact(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Z

    move-result v0

    return v0
.end method

.method public clearNewMissedCallFromCallLog(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 388
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->clearNewMissedCallFromCallLog(Landroid/content/Context;)V

    .line 389
    return-void
.end method

.method public getBTPairingType(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 398
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getConnectivityManager()Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getBTPairingType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    move-result-object v0

    return-object v0
.end method

.method public getBluetoothCommunicatingState(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 425
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getConnectivityManager()Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getBluetoothCommunicatingState(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public getBluetoothConnectedState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 431
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getConnectivityManager()Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getBluetoothConnectedState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method public getBluetoothConnectedState(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "btDeviceMacAddress"    # Ljava/lang/String;

    .prologue
    .line 438
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getConnectivityManager()Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getBluetoothConnectedState(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getBluetoothConnectionState(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 411
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getConnectivityManager()Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getBluetoothConnectionState(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public getBluetoothDevicePairedState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 456
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getConnectivityManager()Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getBluetoothDevicePairedState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method public getBluetoothDevicesDiscovered(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getConnectivityManager()Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getBluetoothDevicesDiscovered(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getBluetoothProfileConnectionState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 445
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getConnectivityManager()Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getBluetoothProfileConnectionState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method public getCallLogImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "calllog"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getCallLogImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getCalllogList(Landroid/content/Context;II)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "hour"    # I
    .param p3, "itemCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;",
            ">;"
        }
    .end annotation

    .prologue
    .line 547
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getCalllogList(Landroid/content/Context;II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCarSpeedStatus(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 370
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDrivingManager()Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->getCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;

    move-result-object v0

    return-object v0
.end method

.method public getContactFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 337
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getContactFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v0

    return-object v0
.end method

.method public getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getContactImageFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 313
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getContactImageFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getContactList(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 342
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getContactList(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getDLCallMessageLogList(Landroid/content/Context;Ljava/util/Date;I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fromDate"    # Ljava/util/Date;
    .param p3, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Date;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;",
            ">;"
        }
    .end annotation

    .prologue
    .line 393
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getDLCallMessageLogList(Landroid/content/Context;Ljava/util/Date;I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getDevice(Landroid/content/Context;)Landroid/bluetooth/BluetoothDevice;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 450
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getConnectivityManager()Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getDevice(Landroid/content/Context;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 402
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getConnectivityManager()Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDriveLinkMusicPlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    return-object v0
.end method

.method public getFavoriteContactList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getFavoriteContactList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getLocation(Landroid/content/Context;)Landroid/location/Location;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 374
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDrivingManager()Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->getLocation()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "object"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getMusicCount(Landroid/content/Context;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 300
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getMusicCount(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public getNewMissedCallCount(Landroid/content/Context;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 384
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getNewMissedCallCount(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public getOnDriveLinkCallLogListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkCallLogListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;

    return-object v0
.end method

.method public getOnDriveLinkConnectivityListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkConnectivityListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;

    return-object v0
.end method

.method public getOnDriveLinkContactListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkContactListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;

    return-object v0
.end method

.method public getOnDriveLinkDLCallMessageLogListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDLCallMessageLogListener;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkDLCallMessageListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDLCallMessageLogListener;

    return-object v0
.end method

.method public getOnDriveLinkDrivingStatusListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkDrivingStatusListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;

    return-object v0
.end method

.method public getOnDriveLinkEventListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkEventListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;

    return-object v0
.end method

.method public getOnDriveLinkFavoriteContactListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkFavoriteContactListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;

    return-object v0
.end method

.method public getOnDriveLinkLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkLocationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    return-object v0
.end method

.method public getOnDriveLinkMessageListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkMessageListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;

    return-object v0
.end method

.method public getOnDriveLinkMusicListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkMusicListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

    return-object v0
.end method

.method public getOnDriveLinkMusicRemotePlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    return-object v0
.end method

.method public getOnDriveLinkNotificationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    return-object v0
.end method

.method public getOnDriveLinkRecommendedContactForCallListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkRecommendedContactForCallListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;

    return-object v0
.end method

.method public getOnDriveLinkRecommendedContactForMessageListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkRecommendedContactForMessageListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;

    return-object v0
.end method

.method public getOnDriveLinkSamsungAccountListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    return-object v0
.end method

.method public getOnDriveLinkScheduleListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkScheduleListener;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkScheduleListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkScheduleListener;

    return-object v0
.end method

.method public getOnDriveLinkUpdateListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkUpdateListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

    return-object v0
.end method

.method public getPassKey(Landroid/content/Context;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 406
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getConnectivityManager()Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getPassKey()I

    move-result v0

    return v0
.end method

.method public getPhoneNumberList(Landroid/content/Context;J)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contactId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;"
        }
    .end annotation

    .prologue
    .line 361
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getPhoneNumberList(Landroid/content/Context;J)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getPhoneNumberList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contactName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;"
        }
    .end annotation

    .prologue
    .line 366
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getPhoneNumberList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getScheduleList(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fromDate"    # Ljava/util/Date;
    .param p3, "toDate"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 356
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getScheduleList(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public initialize(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 80
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->initialize(Landroid/content/Context;)Z

    .line 82
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkEventListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;

    .line 83
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkContactListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;

    .line 84
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkCallLogListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;

    .line 85
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkMessageListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;

    .line 86
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkScheduleListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkScheduleListener;

    .line 87
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkMusicListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

    .line 88
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    .line 89
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    .line 90
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkLocationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    .line 91
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkDrivingStatusListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;

    .line 92
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkDLCallMessageListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDLCallMessageLogListener;

    .line 93
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkUpdateListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

    .line 94
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    .line 96
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkRecommendedContactForCallListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;

    .line 97
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkRecommendedContactForMessageListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;

    .line 98
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkFavoriteContactListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;

    .line 100
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    .line 102
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setInitialized(Z)V

    .line 104
    return v2
.end method

.method public insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "mCountry"    # Ljava/lang/String;
    .param p2, "mSaleCode"    # Ljava/lang/String;
    .param p3, "mNavigation"    # Ljava/lang/String;

    .prologue
    .line 541
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mbInitialized:Z

    return v0
.end method

.method public registerMusicRemoteControl(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 569
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMusicRemotePlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->registerMusicRemoteControl(Landroid/content/Context;)V

    .line 570
    return-void
.end method

.method public registerNotification(Landroid/content/Context;ZZZZZ)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "call"    # Z
    .param p3, "msg"    # Z
    .param p4, "btConnect"    # Z
    .param p5, "locationShare"    # Z
    .param p6, "alarm"    # Z

    .prologue
    .line 327
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getNotificationManager()Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    move-result-object v0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->registerNotification(Landroid/content/Context;ZZZZZ)V

    .line 329
    return-void
.end method

.method public removeFavoriteContact(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 560
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->removeFavoriteContact(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    .line 562
    return-void
.end method

.method public requestCancelUpdateApplication(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 461
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getUpdateManager()Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->cancelUpdateApplication(Landroid/content/Context;)V

    .line 462
    return-void
.end method

.method public requestCancelUpdateVersionCheck(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 465
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getUpdateManager()Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->cancelUpdateVersionCheck(Landroid/content/Context;)V

    .line 466
    return-void
.end method

.method public requestMapAvailableByCountryAndSaleCode(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "mCountryCode"    # Ljava/lang/String;
    .param p2, "mSaleCode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 535
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->requestMapAvailableByCountryAndSaleCode(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public requestSamsungAccountAccessToken(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 489
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getSamsungAccountManager()Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountAccessToken(Landroid/content/Context;Landroid/os/Bundle;)Z

    .line 491
    return-void
.end method

.method public requestSamsungAccountAuthCode(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 506
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getSamsungAccountManager()Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountAuthCode(Landroid/content/Context;Landroid/os/Bundle;)Z

    .line 507
    return-void
.end method

.method public requestSamsungAccountChecklistValidation(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 495
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getSamsungAccountManager()Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountChecklistValidation(Landroid/content/Context;Landroid/os/Bundle;)Z

    .line 497
    return-void
.end method

.method public requestSamsungAccountConnectService(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 469
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getSamsungAccountManager()Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountConnectService(Landroid/content/Context;)V

    .line 470
    return-void
.end method

.method public requestSamsungAccountDisclaimerAgreement(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 501
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getSamsungAccountManager()Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountDisclaimerAgreement(Landroid/content/Context;Landroid/os/Bundle;)Z

    .line 503
    return-void
.end method

.method public requestSamsungAccountDisconnectService(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 473
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getSamsungAccountManager()Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountDisconnectService(Landroid/content/Context;)V

    .line 475
    return-void
.end method

.method public requestSamsungAccountRegisterCallback(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "clientID"    # Ljava/lang/String;
    .param p3, "clientSecret"    # Ljava/lang/String;
    .param p4, "packageName"    # Ljava/lang/String;

    .prologue
    .line 479
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getSamsungAccountManager()Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountRegisterCallback(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    return-void
.end method

.method public requestSamsungAccountSCloudAccessToken(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 511
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getSamsungAccountManager()Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountSCloudAccessToken(Landroid/content/Context;Landroid/os/Bundle;)Z

    .line 513
    return-void
.end method

.method public requestSamsungAccountUnregisterCallback(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 484
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getSamsungAccountManager()Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountUnregisterCallback(Landroid/content/Context;)V

    .line 486
    return-void
.end method

.method public requestSamsungAccoutUserName(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 516
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getSamsungAccountManager()Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountUserName(Landroid/content/Context;Landroid/os/Bundle;)Z

    .line 517
    return-void
.end method

.method public requestSendMessage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .prologue
    .line 420
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMessageManager()Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;

    move-result-object v0

    .line 421
    check-cast p2, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;

    .end local p2    # "message":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->requestSendMessage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;)V

    .line 422
    return-void
.end method

.method public rescanContactInfo(Landroid/content/Context;J)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contactId"    # J

    .prologue
    .line 346
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->rescanContactInfo(Landroid/content/Context;J)V

    .line 347
    return-void
.end method

.method public rescanContactList(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 351
    .local p2, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->rescanContactList(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 352
    return-void
.end method

.method public setCarSpeedThreshold(Landroid/content/Context;III)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "minSpeed"    # I
    .param p3, "maxSpeed"    # I
    .param p4, "speedCheckDurationInSec"    # I

    .prologue
    .line 379
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDrivingManager()Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;->setCarSpeedThreshold(III)V

    .line 381
    return-void
.end method

.method public setOnDriveLinkCallLogListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkCallLogListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;

    .line 159
    return-void
.end method

.method public setOnDriveLinkConnectivityListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;

    .prologue
    .line 220
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkConnectivityListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;

    .line 221
    return-void
.end method

.method public setOnDriveLinkContactListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkContactListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;

    .line 139
    return-void
.end method

.method public setOnDriveLinkDLCallMessageListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDLCallMessageLogListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDLCallMessageLogListener;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkDLCallMessageListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDLCallMessageLogListener;

    .line 212
    return-void
.end method

.method public setOnDriveLinkDrivingStatusListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkDrivingStatusListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;

    .line 207
    return-void
.end method

.method public setOnDriveLinkEventListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkEventListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;

    .line 134
    return-void
.end method

.method public setOnDriveLinkFavoriteContactListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkFavoriteContactListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;

    .line 144
    return-void
.end method

.method public setOnDriveLinkLocationListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    .prologue
    .line 201
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkLocationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    .line 202
    return-void
.end method

.method public setOnDriveLinkMessageListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkMessageListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;

    .line 164
    return-void
.end method

.method public setOnDriveLinkMusicListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkMusicListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

    .line 173
    return-void
.end method

.method public setOnDriveLinkMusicPlayerListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;

    .line 187
    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->setDriveLinkListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;)V

    .line 189
    :cond_0
    return-void
.end method

.method public setOnDriveLinkMusicRemotePlayerListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;

    .line 195
    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->setDriveLinkListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;)V

    .line 197
    :cond_0
    return-void
.end method

.method public setOnDriveLinkNotificationListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;

    .line 179
    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->setDriveLinkListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;)V

    .line 181
    :cond_0
    return-void
.end method

.method public setOnDriveLinkRecommendedContactForCallListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkRecommendedContactForCallListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;

    .line 149
    return-void
.end method

.method public setOnDriveLinkRecommendedContactForMessageListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkRecommendedContactForMessageListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;

    .line 154
    return-void
.end method

.method public setOnDriveLinkSamsungAccountListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;

    .line 227
    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->setDriveLinkListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;)V

    .line 229
    :cond_0
    return-void
.end method

.method public setOnDriveLinkScheduleListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkScheduleListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkScheduleListener;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkScheduleListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkScheduleListener;

    .line 169
    return-void
.end method

.method public setOnDriveLinkUpdateListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->mOnDriveLinkUpdateListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

    .line 216
    return-void
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 109
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setInitialized(Z)V

    .line 111
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->terminate(Landroid/content/Context;)V

    .line 130
    return-void
.end method

.method public unregisterMusicRemoteControl(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 573
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getMusicRemotePlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->unregisterMusicRemotecontrol(Landroid/content/Context;)V

    .line 574
    return-void
.end method

.method public unregisterNotification(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 332
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getNotificationManager()Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->unregisterNotification(Landroid/content/Context;)V

    .line 333
    return-void
.end method

.method public updatePlayedMusicMetaData(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 304
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->updatePlayedMusicMetaData(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Z

    move-result v0

    return v0
.end method
