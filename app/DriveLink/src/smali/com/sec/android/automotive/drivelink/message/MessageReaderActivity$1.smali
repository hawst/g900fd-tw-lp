.class Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$1;
.super Ljava/lang/Object;
.source "MessageReaderActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$OnReaderMicStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$message$VoiceMessageReaderActionBarLayout$MessageReaderState:[I


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$message$VoiceMessageReaderActionBarLayout$MessageReaderState()[I
    .locals 3

    .prologue
    .line 232
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$1;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$message$VoiceMessageReaderActionBarLayout$MessageReaderState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;->values()[Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;->QUOTES_DISABLE:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;->QUOTES_ENABLE:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;->QUOTES_NONE:Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$1;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$message$VoiceMessageReaderActionBarLayout$MessageReaderState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    .line 232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnStateChangeed(Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;)V
    .locals 6
    .param p1, "State"    # Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;

    .prologue
    const v5, 0x7f0c00b7

    const/4 v4, 0x1

    const v3, 0x7f0c00b6

    const/4 v2, 0x0

    .line 236
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$1;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$message$VoiceMessageReaderActionBarLayout$MessageReaderState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageReaderActionBarLayout$MessageReaderState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 261
    :goto_0
    return-void

    .line 238
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCallBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setMarkShow(Z)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mReplyBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setMarkShow(Z)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCallBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setButtonStyleOff(I)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mReplyBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v0

    .line 242
    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setButtonStyleOff(I)V

    goto :goto_0

    .line 245
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCallBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setMarkShow(Z)V

    .line 246
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mReplyBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setMarkShow(Z)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCallBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setButtonStyleOn(I)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mReplyBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v0

    .line 249
    invoke-virtual {v0, v5}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setButtonStyleOn(I)V

    goto :goto_0

    .line 252
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCallBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setMarkShow(Z)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mReplyBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setMarkShow(Z)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCallBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setButtonStyleOff(I)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mReplyBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v0

    .line 256
    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setButtonStyleOff(I)V

    goto :goto_0

    .line 236
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
