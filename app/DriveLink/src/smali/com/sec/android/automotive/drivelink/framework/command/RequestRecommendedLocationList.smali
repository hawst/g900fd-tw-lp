.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedLocationList;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestRecommendedLocationList.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "RequestRecommendedLocationList"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mItemCount:I

.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "itemCount"    # I

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 15
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedLocationList;->mContext:Landroid/content/Context;

    .line 16
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedLocationList;->mList:Ljava/util/ArrayList;

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedLocationList;->mItemCount:I

    .line 22
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedLocationList;->mContext:Landroid/content/Context;

    .line 23
    iput p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedLocationList;->mItemCount:I

    .line 24
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 57
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedLocationList;->mContext:Landroid/content/Context;

    .line 58
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedLocationList;->mList:Ljava/util/ArrayList;

    .line 59
    return-void
.end method

.method protected process()V
    .locals 4

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedLocationList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 30
    .local v1, "service":Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    if-nez v1, :cond_0

    .line 31
    const-string/jumbo v2, "RequestRecommendedLocationList"

    const-string/jumbo v3, "[stop] DriveLinkServiceProvider is null."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    :goto_0
    return-void

    .line 36
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getRecommandationManagerForLocation()Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;

    move-result-object v0

    .line 38
    .local v0, "recommendation":Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;
    if-nez v0, :cond_1

    .line 39
    const-string/jumbo v2, "RequestRecommendedLocationList"

    const-string/jumbo v3, "[stop] RecommendationManager is null."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 43
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedLocationList;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedLocationList;->mItemCount:I

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->getRecommendedLocationList(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedLocationList;->mList:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedLocationList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 49
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    move-result-object v0

    .line 50
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    if-eqz v0, :cond_0

    .line 51
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedLocationList;->mList:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;->onResponseRequestRecommendedLocationList(Ljava/util/ArrayList;)V

    .line 53
    :cond_0
    return-void
.end method
