.class public interface abstract Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;
.super Ljava/lang/Object;
.source "DriveLinkServiceInterface.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnDriveLinkMusicListener"
.end annotation


# virtual methods
.method public abstract onResponseRequestAlbumList(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onResponseRequestArtistList(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onResponseRequestFolderList(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onResponseRequestMusicAlbumArt(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
.end method

.method public abstract onResponseRequestMusicList(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onResponseRequestMusicListByAlbum(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onResponseRequestMusicListByArtist(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onResponseRequestMusicListByFolder(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onResponseRequestMusicListByMusic(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onResponseRequestMusicListByPlaylist(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onResponseRequestMusicListFromSearchResult(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;",
            "I)V"
        }
    .end annotation
.end method

.method public abstract onResponseRequestPlaylistList(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onResponseRequestSearchAllMusic(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onResponseRequestSearchMusic(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;)V
.end method
