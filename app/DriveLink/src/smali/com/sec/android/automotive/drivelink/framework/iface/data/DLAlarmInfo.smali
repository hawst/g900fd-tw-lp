.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLAlarmInfo.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getAlarmId()I
.end method

.method public abstract getAlarmName()Ljava/lang/String;
.end method

.method public abstract getAlertTime()J
.end method

.method public abstract getSNZActive()Z
.end method
