.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;
.super Ljava/lang/Object;
.source "MessageDataHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "InboxThreadIDsNDate"
.end annotation


# instance fields
.field private mLastDateMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mThreadIds:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 1
    .param p2, "threadIds"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "lastDateMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Long;>;"
    const/4 v0, 0x0

    .line 750
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;

    .line 751
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 746
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;->mThreadIds:Ljava/lang/String;

    .line 747
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;->mLastDateMap:Ljava/util/HashMap;

    .line 752
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;->mThreadIds:Ljava/lang/String;

    .line 753
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;->mLastDateMap:Ljava/util/HashMap;

    .line 754
    return-void
.end method


# virtual methods
.method public getThreadIds()Ljava/lang/String;
    .locals 1

    .prologue
    .line 757
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;->mThreadIds:Ljava/lang/String;

    return-object v0
.end method

.method public getmLastDateMap()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 761
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;->mLastDateMap:Ljava/util/HashMap;

    return-object v0
.end method
