.class Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$8;
.super Ljava/lang/Object;
.source "SettingsRegisterCarOptionsActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->showRenameDialog(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    .line 324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 340
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    .line 341
    invoke-interface {p1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 340
    :goto_0
    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->access$6(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;Z)V

    .line 342
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    .line 343
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 345
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->isDialogOkEnabled:Z
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->access$4(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)Z

    move-result v1

    .line 344
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    .line 347
    invoke-interface {p1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 346
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->access$7(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;Ljava/lang/String;)V

    .line 348
    return-void

    .line 341
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 336
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 330
    return-void
.end method
