.class public Lcom/sec/android/automotive/drivelink/location/LocationActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;
.source "LocationActivity.java"


# static fields
.field public static final DELAY_TIME_FOR_AUTO_SHRINK:J = 0x2710L

.field private static final ERROR_STRING:Ljava/lang/String; = "ERROR_STRING"

.field private static final GOOGLE_MAP:Ljava/lang/String; = "google_map"

.field private static final IS_ERROR_STATE:Ljava/lang/String; = "IS_ERROR_STATE"

.field private static final IS_TTS_STATE:Ljava/lang/String; = "IS_TTS_STATE"

.field private static final LOCATION_HOME:Ljava/lang/String; = "location_home"

.field private static final LOCATION_MYPLACES:Ljava/lang/String; = "location_myplace"

.field private static final LOCATION_OFFICE:Ljava/lang/String; = "location_office"

.field public static final MAX_RECOMMENDATIONS:I = 0xc

.field public static final MSG_AUTO_SHRINK:I = 0x0

.field private static final NAVIGATION_SEARCH:I = 0x4b1

.field public static final SIP_SEARCH_MODE:Ljava/lang/String; = "SIP_SEARCH_MODE"

.field public static final SIP_SEARCH_TEXT:Ljava/lang/String; = "SIP_SEARCH_TEXT"

.field private static final WAS_SHOWING_ALLOW_DIALOG:Ljava/lang/String; = "wasShowingDialog"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private allowDialog:Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;

.field private dotIndex:I

.field private idsToSave:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private locationListLayout:Landroid/widget/RelativeLayout;

.field private mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

.field private mBtnBack:Landroid/widget/ImageButton;

.field private mButtonArea:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mImgBtRequest:Landroid/widget/ImageView;

.field private mImgBtShare:Landroid/widget/ImageView;

.field private mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

.field private mLocationList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation
.end field

.field private mLocationPageAdapter:Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;

.field private mLocationViewPager:Landroid/support/v4/view/ViewPager;

.field private mMainLayout:Landroid/widget/RelativeLayout;

.field private mOnUpdateLocationSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;

.field private mSearchBtn:Landroid/widget/ImageButton;

.field private mSearchClearBtn:Landroid/widget/ImageButton;

.field protected mSearchMode:Z

.field private mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

.field protected mSearchedText:Ljava/lang/String;

.field private mTextfieldLayout:Landroid/widget/FrameLayout;

.field private mTvSearchLocation:Landroid/widget/TextView;

.field private pageNavigationLayout:Landroid/widget/RelativeLayout;

.field private saveIds:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;-><init>()V

    .line 78
    const-string/jumbo v0, "LocationActivity"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->TAG:Ljava/lang/String;

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mOnUpdateLocationSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;

    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->idsToSave:Ljava/util/ArrayList;

    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->saveIds:Z

    .line 231
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mHandler:Landroid/os/Handler;

    .line 72
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->allowDialog:Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Z
    .locals 1

    .prologue
    .line 619
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->hideSearchBar()Z

    move-result v0

    return v0
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/location/LocationActivity;I)Z
    .locals 1

    .prologue
    .line 762
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->selectItem(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V
    .locals 0

    .prologue
    .line 748
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->enableViewPager()V

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationPageAdapter:Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->finish()V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/location/LocationActivity;Z)V
    .locals 0

    .prologue
    .line 215
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->setAutoShrink(Z)V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/location/LocationActivity;I)V
    .locals 0

    .prologue
    .line 453
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->handleShrinkMessageOnMicState(I)V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/location/LocationActivity;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V
    .locals 0

    .prologue
    .line 676
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->updateLocationList()V

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Z
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->IsOffline()Z

    move-result v0

    return v0
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Z
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->isProviderEnabled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/location/LocationActivity;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 1723
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->hasGoogleAccount(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public static adjustToMaxSizeSuggestionList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/ArrayList",
            "<TT;>;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p0, "itens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    const/16 v3, 0xc

    .line 1521
    const/16 v0, 0xc

    .line 1522
    .local v0, "max":I
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v3, :cond_0

    .line 1523
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v3}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object p0, v1

    .line 1525
    .end local p0    # "itens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    :cond_0
    return-object p0
.end method

.method private bindViews()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 248
    const v0, 0x7f090085

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mMainLayout:Landroid/widget/RelativeLayout;

    .line 249
    const v0, 0x7f090089

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationViewPager:Landroid/support/v4/view/ViewPager;

    .line 250
    const v0, 0x7f090086

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 251
    const v0, 0x7f09008a

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->pageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 253
    const v0, 0x7f090088

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mButtonArea:Landroid/widget/LinearLayout;

    .line 255
    const v0, 0x7f090087

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->locationListLayout:Landroid/widget/RelativeLayout;

    .line 257
    const v0, 0x7f09008c

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mImgBtShare:Landroid/widget/ImageView;

    .line 258
    const v0, 0x7f09008f

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mImgBtRequest:Landroid/widget/ImageView;

    .line 260
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->isUScsc()Z

    move-result v0

    if-nez v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mButtonArea:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 263
    :cond_0
    const v0, 0x7f090091

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 265
    return-void
.end method

.method private enableViewPager()V
    .locals 4

    .prologue
    .line 750
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationActivity$14;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$14;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    .line 757
    .local v1, "r":Ljava/lang/Runnable;
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 758
    .local v0, "handler":Landroid/os/Handler;
    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 760
    return-void
.end method

.method private getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1048
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$15;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$15;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    return-object v0
.end method

.method private getListItemOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 718
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$13;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$13;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    return-object v0
.end method

.method private getLocationSipStateListener()Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$OnLocationSipStateListener;
    .locals 1

    .prologue
    .line 476
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$6;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    return-object v0
.end method

.method private getMicStateChangeListener()Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;
    .locals 1

    .prologue
    .line 1075
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$16;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$16;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    return-object v0
.end method

.method private getOnEditorActionListener()Landroid/widget/TextView$OnEditorActionListener;
    .locals 1

    .prologue
    .line 342
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    return-object v0
.end method

.method private getOnUpdateLocationSuggestionListListener()Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;
    .locals 1

    .prologue
    .line 326
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    return-object v0
.end method

.method private handleShrinkMessageOnMicState(I)V
    .locals 7
    .param p1, "stateValue"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 454
    const-string/jumbo v3, "activity"

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 455
    .local v1, "am":Landroid/app/ActivityManager;
    invoke-virtual {v1, v6}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    .line 456
    .local v0, "Info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v2, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 458
    .local v2, "topActivity":Landroid/content/ComponentName;
    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    .line 459
    const-string/jumbo v4, "com.sec.android.automotive.drivelink.location.LocationActivity"

    .line 458
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 459
    if-nez v3, :cond_0

    .line 460
    const-string/jumbo v3, "LocationActivity"

    const-string/jumbo v4, "handleShrinkMessageOnMicState - not LocationActivity"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    :goto_0
    return-void

    .line 463
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 469
    const-string/jumbo v3, "LocationActivity"

    const-string/jumbo v4, "handleShrinkMessageOnMicState - default"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    invoke-direct {p0, v5}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->setAutoShrink(Z)V

    goto :goto_0

    .line 465
    :pswitch_0
    const-string/jumbo v3, "LocationActivity"

    const-string/jumbo v4, "handleShrinkMessageOnMicState - MIC_STATE_IDLE"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    invoke-direct {p0, v6}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->setAutoShrink(Z)V

    goto :goto_0

    .line 463
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private hasGoogleAccount(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 1724
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1725
    .local v1, "accMan":Landroid/accounts/AccountManager;
    const-string/jumbo v3, "com.google"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 1726
    .local v0, "accArray":[Landroid/accounts/Account;
    array-length v3, v0

    if-lt v3, v2, :cond_0

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private hideSearchBar()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 620
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->hideLocationLayout()V

    .line 621
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->showVoiceLayout()V

    .line 623
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 624
    const-string/jumbo v2, "input_method"

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 625
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v2

    .line 626
    const-wide/16 v3, 0x258

    invoke-virtual {v2, v3, v4}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    .line 627
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 628
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchMode:Z

    .line 629
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchedText:Ljava/lang/String;

    .line 630
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 631
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 633
    :cond_0
    const/4 v1, 0x1

    .line 636
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_1
    return v1
.end method

.method private initGetMyPlacesDialog()V
    .locals 3

    .prologue
    .line 599
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGetMyPlacesDialog;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGetMyPlacesDialog;-><init>()V

    .line 601
    .local v0, "dialog":Lcom/sec/android/automotive/drivelink/notification/view/NotificationGetMyPlacesDialog;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    .line 602
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "dialog 2"

    .line 601
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationGetMyPlacesDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 603
    return-void
.end method

.method private initSearchTextField()V
    .locals 2

    .prologue
    .line 506
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 507
    const v1, 0x7f090333

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 506
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;

    .line 508
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 509
    const v1, 0x7f090349

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 508
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mTvSearchLocation:Landroid/widget/TextView;

    .line 510
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 511
    const v1, 0x7f09034a

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    .line 510
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    .line 512
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 513
    const v1, 0x7f09034b

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 512
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchClearBtn:Landroid/widget/ImageButton;

    .line 514
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const v1, 0x7f090331

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mBtnBack:Landroid/widget/ImageButton;

    .line 515
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 516
    const v1, 0x7f090345

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 515
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchBtn:Landroid/widget/ImageButton;

    .line 518
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchBtn:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$7;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 546
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    .line 547
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getLocationSipStateListener()Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$OnLocationSipStateListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->setOnLocationSipStateListener(Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$OnLocationSipStateListener;)V

    .line 549
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$8;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 565
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mBtnBack:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationActivity$9;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$9;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 588
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchClearBtn:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationActivity$10;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$10;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 596
    return-void
.end method

.method private prepareAllowShareDialog()V
    .locals 4

    .prologue
    .line 660
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->allowDialog:Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;->show()V

    .line 661
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->allowDialog:Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;

    const v3, 0x7f09020d

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 662
    .local v0, "btCancel":Landroid/widget/Button;
    new-instance v2, Lcom/sec/android/automotive/drivelink/location/LocationActivity$11;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$11;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 668
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->allowDialog:Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;

    const v3, 0x7f09020e

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 669
    .local v1, "btOk":Landroid/widget/Button;
    new-instance v2, Lcom/sec/android/automotive/drivelink/location/LocationActivity$12;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$12;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 674
    return-void
.end method

.method private refreshList()V
    .locals 5

    .prologue
    .line 1646
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    if-nez v3, :cond_1

    .line 1658
    :cond_0
    :goto_0
    return-void

    .line 1648
    :cond_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 1649
    .local v1, "currentItem":I
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v0

    .line 1650
    .local v0, "currentDot":I
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->showRecommendLocationsList()V

    .line 1652
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1653
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v3, v0}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1654
    :catch_0
    move-exception v2

    .line 1655
    .local v2, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string/jumbo v3, "LocationActivity"

    const-string/jumbo v4, "refreshList"

    invoke-static {v3, v4, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1656
    invoke-virtual {v2}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method private selectItem(I)Z
    .locals 12
    .param p1, "currentItem"    # I

    .prologue
    const v11, 0xddd5

    const v10, 0x7f0a01fb

    const v9, 0x7f090091

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 770
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationList:Ljava/util/ArrayList;

    if-nez v7, :cond_1

    .line 1006
    :cond_0
    :goto_0
    return v5

    .line 774
    :cond_1
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-le p1, v7, :cond_2

    .line 775
    const-string/jumbo v6, "LocationActivity"

    const-string/jumbo v7, "currentItem is out of location list"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 779
    :cond_2
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationList:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;

    .line 780
    .local v2, "locationItem":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v1

    .line 782
    .local v1, "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    .line 781
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v7

    .line 783
    const-string/jumbo v8, "PREF_SETTINGS_MY_NAVIGATION"

    .line 782
    invoke-virtual {v7, v8, v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v4

    .line 785
    .local v4, "mapID":I
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 786
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v3

    .line 787
    .local v3, "locationName":Ljava/lang/String;
    const-string/jumbo v7, "location_home"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 788
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    move-result-object v7

    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->MY_PLACE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    if-ne v7, v8, :cond_6

    .line 789
    const-string/jumbo v7, "CM19"

    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 790
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a02f0

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 881
    :cond_3
    :goto_1
    if-eqz v1, :cond_4

    .line 882
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidAddres()Z

    move-result v7

    if-nez v7, :cond_12

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidCoords()Z

    move-result v7

    if-nez v7, :cond_12

    .line 892
    :cond_4
    const-string/jumbo v7, "location_home"

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    const-string/jumbo v7, "location_office"

    .line 893
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 895
    :cond_5
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v7

    if-eqz v7, :cond_10

    .line 896
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->showToastAndPromptTTSWhenIsDriving()V

    goto/16 :goto_0

    .line 791
    :cond_6
    const-string/jumbo v7, "location_office"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 792
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    move-result-object v7

    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->MY_PLACE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    if-ne v7, v8, :cond_7

    .line 793
    const-string/jumbo v7, "CM19"

    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 794
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a02f5

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 795
    goto :goto_1

    .line 807
    :cond_7
    const-string/jumbo v7, "google_map"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 808
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    move-result-object v7

    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->SCHEDULE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    if-eq v7, v8, :cond_d

    .line 810
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->IsOffline()Z

    move-result v7

    if-nez v7, :cond_0

    .line 816
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->isProviderEnabled()Z

    move-result v7

    if-nez v7, :cond_8

    .line 817
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getEnableGPSPDialog()V

    goto/16 :goto_0

    .line 822
    :cond_8
    sget v7, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_GOOGLE_MAPS:I

    if-ne v4, v7, :cond_b

    .line 823
    invoke-direct {p0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->hasGoogleAccount(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_9

    .line 824
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v6

    invoke-interface {v6}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 825
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    .line 827
    invoke-virtual {p0, v10}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->showToast(I)V

    .line 828
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v6

    .line 829
    const-wide/16 v7, 0x258

    invoke-virtual {v6, v7, v8}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    goto/16 :goto_0

    .line 834
    :cond_9
    const-string/jumbo v7, "CM13"

    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 836
    const-string/jumbo v7, ""

    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchedText:Ljava/lang/String;

    .line 837
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    if-eqz v7, :cond_a

    .line 838
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    const-string/jumbo v8, ""

    invoke-virtual {v7, v8}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->setText(Ljava/lang/CharSequence;)V

    .line 840
    :cond_a
    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 841
    invoke-virtual {v7, v5}, Landroid/view/View;->setVisibility(I)V

    .line 842
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->getInstance()Lcom/sec/android/automotive/drivelink/location/NavigationActivity;

    move-result-object v5

    .line 843
    const/4 v7, 0x0

    invoke-virtual {v5, p0, v7}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->startNavigation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    move v5, v6

    .line 846
    goto/16 :goto_0

    .line 849
    :cond_b
    const-string/jumbo v7, "CM19"

    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 851
    const-string/jumbo v7, ""

    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchedText:Ljava/lang/String;

    .line 852
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    if-eqz v7, :cond_c

    .line 853
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    const-string/jumbo v8, ""

    invoke-virtual {v7, v8}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->setText(Ljava/lang/CharSequence;)V

    .line 855
    :cond_c
    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 856
    invoke-virtual {v7, v5}, Landroid/view/View;->setVisibility(I)V

    .line 857
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->getInstance()Lcom/sec/android/automotive/drivelink/location/NavigationActivity;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {v5, p0, v7}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->startNavigation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    move v5, v6

    .line 860
    goto/16 :goto_0

    .line 862
    :cond_d
    const-string/jumbo v7, "location_myplace"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 863
    const-string/jumbo v7, "CM18"

    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 864
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v7

    if-nez v7, :cond_f

    .line 865
    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidCoords()Z

    move-result v7

    if-nez v7, :cond_f

    .line 868
    :cond_e
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 869
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v5, "android.settings.MY_PLACE_SETTINGS"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 870
    invoke-virtual {p0, v0, v11}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->startActivityForResult(Landroid/content/Intent;I)V

    move v5, v6

    .line 872
    goto/16 :goto_0

    .line 873
    .end local v0    # "i":Landroid/content/Intent;
    :cond_f
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 875
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->showToastAndPromptTTSWhenIsDriving()V

    goto/16 :goto_0

    .line 903
    :cond_10
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 904
    .restart local v0    # "i":Landroid/content/Intent;
    const-string/jumbo v5, "android.settings.MY_PLACE_SETTINGS"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 905
    invoke-virtual {p0, v0, v11}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->startActivityForResult(Landroid/content/Intent;I)V

    move v5, v6

    .line 906
    goto/16 :goto_0

    .line 933
    .end local v0    # "i":Landroid/content/Intent;
    :cond_11
    const-string/jumbo v5, "LocationActivity"

    const-string/jumbo v7, "location address is missing"

    invoke-static {v5, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v6

    .line 934
    goto/16 :goto_0

    .line 937
    :cond_12
    if-eqz v3, :cond_13

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_14

    .line 938
    :cond_13
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationName()Ljava/lang/String;

    move-result-object v3

    .line 949
    :cond_14
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->IsOffline()Z

    move-result v7

    if-nez v7, :cond_0

    .line 950
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->isProviderEnabled()Z

    move-result v7

    if-eqz v7, :cond_17

    .line 951
    sget v7, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_GOOGLE_MAPS:I

    if-ne v4, v7, :cond_16

    .line 952
    invoke-direct {p0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->hasGoogleAccount(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_15

    .line 953
    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 954
    invoke-virtual {v7, v5}, Landroid/view/View;->setVisibility(I)V

    .line 955
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->getInstance()Lcom/sec/android/automotive/drivelink/location/NavigationActivity;

    move-result-object v5

    invoke-virtual {v5, p0, v1}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->startNavigation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    :goto_2
    move v5, v6

    .line 1006
    goto/16 :goto_0

    .line 960
    :cond_15
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v6

    invoke-interface {v6}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 961
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    .line 963
    invoke-virtual {p0, v10}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->showToast(I)V

    .line 964
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v6

    .line 965
    const-wide/16 v7, 0x258

    invoke-virtual {v6, v7, v8}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    goto/16 :goto_0

    .line 970
    :cond_16
    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 971
    invoke-virtual {v7, v5}, Landroid/view/View;->setVisibility(I)V

    .line 972
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->getInstance()Lcom/sec/android/automotive/drivelink/location/NavigationActivity;

    move-result-object v5

    invoke-virtual {v5, p0, v1}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->startNavigation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    goto :goto_2

    .line 996
    :cond_17
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getEnableGPSPDialog()V

    goto/16 :goto_0
.end method

.method private setAutoShrink(Z)V
    .locals 4
    .param p1, "isOn"    # Z

    .prologue
    const/4 v3, 0x0

    .line 216
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 229
    :goto_0
    return-void

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 221
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchMode:Z

    if-nez v0, :cond_1

    .line 222
    const-string/jumbo v0, "LocationActivity"

    const-string/jumbo v1, "MSG_AUTO_SHRINK - start on playing"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mHandler:Landroid/os/Handler;

    .line 224
    const-wide/16 v1, 0x2710

    .line 223
    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 226
    :cond_1
    const-string/jumbo v0, "LocationActivity"

    const-string/jumbo v1, "MSG_AUTO_SHRINK - removing"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method private setListeners()V
    .locals 2

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mOnUpdateLocationSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;

    if-nez v0, :cond_0

    .line 269
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getOnUpdateLocationSuggestionListListener()Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mOnUpdateLocationSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 302
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getOnEditorActionListener()Landroid/widget/TextView$OnEditorActionListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 305
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setOnVoicePhoneActionBarListener(Lcom/sec/android/automotive/drivelink/phone/OnVoicePhoneActionBarListener;)V

    .line 318
    return-void
.end method

.method private setSuggetionListFlowParam()V
    .locals 6

    .prologue
    .line 1366
    const/4 v0, 0x0

    .line 1367
    .local v0, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v3, "DM_LOCATION"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v0

    .line 1368
    const/4 v2, 0x0

    .line 1370
    .local v2, "locationName":Ljava/lang/String;
    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 1371
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    .line 1372
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1417
    :cond_0
    return-void

    .line 1372
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;

    .line 1373
    .local v1, "locationItem":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 1374
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 1375
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v2

    .line 1376
    const-string/jumbo v4, "location_home"

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1377
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1378
    const v5, 0x7f0a02f0

    .line 1377
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1410
    :cond_2
    :goto_1
    if-nez v2, :cond_3

    .line 1411
    const-string/jumbo v2, ""

    .line 1414
    :cond_3
    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1379
    :cond_4
    const-string/jumbo v4, "location_office"

    .line 1380
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v5

    .line 1379
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 1380
    if-eqz v4, :cond_5

    .line 1381
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1382
    const v5, 0x7f0a02f5

    .line 1381
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1383
    goto :goto_1

    .line 1389
    :cond_5
    const-string/jumbo v4, "Group"

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1390
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1391
    const v5, 0x7f0a0320

    .line 1390
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1392
    goto :goto_1

    .line 1397
    :cond_6
    const-string/jumbo v4, "google_map"

    .line 1398
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v5

    .line 1397
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 1398
    if-eqz v4, :cond_7

    .line 1399
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1400
    const v5, 0x7f0a0307

    .line 1399
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1401
    goto :goto_1

    :cond_7
    const-string/jumbo v4, "location_myplaces"

    .line 1402
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v5

    .line 1401
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 1402
    if-eqz v4, :cond_2

    .line 1403
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1404
    const v5, 0x7f0a02f1

    .line 1403
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1406
    goto :goto_1

    .line 1407
    :cond_8
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method private showRecommendLocationsList()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1290
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationList:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 1363
    :goto_0
    return-void

    .line 1293
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->sortLocationList()V

    .line 1295
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationList:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->adjustToMaxSizeSuggestionList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationList:Ljava/util/ArrayList;

    .line 1296
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v5}, Landroid/support/v4/view/ViewPager;->setLongClickable(Z)V

    .line 1298
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationPageAdapter:Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;

    if-nez v1, :cond_1

    .line 1299
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;

    .line 1300
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationList:Ljava/util/ArrayList;

    .line 1301
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getListItemOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V

    .line 1299
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationPageAdapter:Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;

    .line 1302
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationPageAdapter:Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 1308
    :goto_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    if-nez v1, :cond_2

    .line 1309
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    .line 1310
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1311
    const/4 v1, -0x1

    const/4 v2, -0x2

    .line 1310
    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1312
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1313
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1314
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1315
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setGravity(I)V

    .line 1316
    const v1, 0x7f09008a

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 1317
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1322
    .end local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :goto_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationPageAdapter:Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->getPageCount()I

    move-result v2

    .line 1323
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v3

    .line 1322
    invoke-virtual {v1, v2, v5, v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setDotIndicator(IILandroid/view/View$OnClickListener;)V

    .line 1324
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    iget v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->dotIndex:I

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 1328
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationViewPager:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setClickable(Z)V

    .line 1331
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationPageAdapter:Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->getIdsToSave()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->idsToSave:Ljava/util/ArrayList;

    .line 1333
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationViewPager:Landroid/support/v4/view/ViewPager;

    new-instance v2, Lcom/sec/android/automotive/drivelink/location/LocationActivity$18;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$18;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    goto/16 :goto_0

    .line 1304
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationPageAdapter:Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->setLocationList(Ljava/util/ArrayList;)V

    .line 1305
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationPageAdapter:Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->notifyDataSetChanged()V

    goto :goto_1

    .line 1319
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->removeAllViews()V

    goto :goto_2
.end method

.method private showToastAndPromptTTSWhenIsDriving()V
    .locals 2

    .prologue
    const v1, 0x7f0a01fa

    .line 1013
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 1014
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 1017
    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->showToast(I)V

    .line 1020
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    .line 1022
    return-void
.end method

.method private sortLocationList()V
    .locals 2

    .prologue
    .line 1440
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1517
    :goto_0
    return-void

    .line 1444
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationList:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationActivity$19;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$19;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method private updateLocationList()V
    .locals 1

    .prologue
    .line 677
    .line 678
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationList:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->adjustToMaxSizeSuggestionList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 677
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationList:Ljava/util/ArrayList;

    .line 679
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationList:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->verifyKoreaMyPlaceSituation(Ljava/util/ArrayList;)V

    .line 680
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->showRecommendLocationsList()V

    .line 681
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->setSuggetionListFlowParam()V

    .line 682
    return-void
.end method

.method private verifyKoreaMyPlaceSituation(Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 687
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    const/4 v1, 0x0

    .line 688
    .local v1, "hasHome":Z
    const/4 v2, 0x0

    .line 690
    .local v2, "hasOffice":Z
    if-nez p1, :cond_0

    .line 691
    new-instance p1, Ljava/util/ArrayList;

    .end local p1    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 694
    .restart local p1    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_4

    .line 705
    if-nez v1, :cond_2

    .line 706
    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;

    invoke-direct {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;-><init>()V

    .line 707
    .local v3, "tempHome":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;
    const-string/jumbo v5, "location_home"

    invoke-virtual {v3, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->setPlaceName(Ljava/lang/String;)V

    .line 708
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 710
    .end local v3    # "tempHome":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;
    :cond_2
    if-nez v2, :cond_3

    .line 711
    new-instance v4, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;

    invoke-direct {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;-><init>()V

    .line 712
    .local v4, "tempOffice":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;
    const-string/jumbo v5, "location_office"

    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->setPlaceName(Ljava/lang/String;)V

    .line 713
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 715
    .end local v4    # "tempOffice":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;
    :cond_3
    return-void

    .line 694
    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;

    .line 695
    .local v0, "dlTempItem":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    const-string/jumbo v6, "location_home"

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 696
    const/4 v1, 0x1

    .line 698
    :cond_5
    const-string/jumbo v6, "location_office"

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 699
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public finish()V
    .locals 2

    .prologue
    .line 1550
    # invokes: Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->finish()V
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$17(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    .line 1551
    const v0, 0x7f040025

    const v1, 0x7f040001

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->overridePendingTransition(II)V

    .line 1556
    return-void
.end method

.method protected getLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    .locals 1

    .prologue
    .line 1534
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMyPlacesDialog()V
    .locals 0

    .prologue
    .line 1529
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->initGetMyPlacesDialog()V

    .line 1530
    return-void
.end method

.method public lockButtons(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1065
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 1067
    .local v0, "id":I
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    .line 1071
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 642
    const-string/jumbo v0, "LocationActivity"

    const-string/jumbo v1, "onActivityResult start"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    sparse-switch p1, :sswitch_data_0

    .line 655
    :cond_0
    :goto_0
    const-string/jumbo v0, "LocationActivity"

    const-string/jumbo v1, "onActivityResult end"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 657
    return-void

    .line 645
    :sswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 647
    const-string/jumbo v0, "LocationActivity"

    .line 648
    const-string/jumbo v1, "onActivityResult [REQUEST_LOAD_SIGN_IN_SCREEN] Sign in SUCCESS"

    .line 647
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 652
    :sswitch_1
    const-string/jumbo v0, "LocationActivity"

    const-string/jumbo v1, "resultado activity"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 643
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x4b1 -> :sswitch_1
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 609
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onBackPressed()V

    .line 611
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->hideSearchBar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->setAutoShrink(Z)V

    .line 617
    :goto_0
    return-void

    .line 615
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->setAutoShrink(Z)V

    .line 616
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->finish()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 151
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onCreate(Landroid/os/Bundle;)V

    .line 152
    const v1, 0x7f030011

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->setContentView(I)V

    .line 153
    const v1, 0x7f0a029a

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->setTitle(I)V

    .line 154
    iput-object p0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mContext:Landroid/content/Context;

    .line 156
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->bindViews()V

    .line 157
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->setListeners()V

    .line 159
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    .line 161
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mOnUpdateLocationSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;

    .line 160
    invoke-virtual {v1, v3}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->setOnUpdateLocationSugessionListListener(Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLocationSuggestionListListener;)V

    .line 162
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    .line 163
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getLocationSuggestionList()Ljava/util/ArrayList;

    move-result-object v1

    .line 162
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationList:Ljava/util/ArrayList;

    .line 164
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->updateLocationList()V

    .line 167
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setBtnSearchVisible()V

    .line 171
    :try_start_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1, v3}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    :cond_0
    :goto_0
    if-nez p1, :cond_2

    const/4 v1, 0x0

    :goto_1
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchMode:Z

    .line 180
    if-nez p1, :cond_3

    move-object v1, v2

    :goto_2
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchedText:Ljava/lang/String;

    .line 183
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->initSearchTextField()V

    .line 185
    const/high16 v1, 0x7f040000

    const v2, 0x7f040025

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->overridePendingTransition(II)V

    .line 204
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const v2, 0x7f0a0247

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setSayText(I)V

    .line 206
    if-eqz p1, :cond_1

    .line 207
    const-string/jumbo v1, "IS_TTS_STATE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 208
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setTTSState()V

    .line 211
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->moveoutOverlayService()V

    .line 213
    return-void

    .line 174
    :catch_0
    move-exception v0

    .line 175
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 179
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    const-string/jumbo v1, "SIP_SEARCH_MODE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    goto :goto_1

    .line 181
    :cond_3
    const-string/jumbo v1, "SIP_SEARCH_TEXT"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    .line 1100
    const v1, 0x7f090091

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1101
    const/16 v2, 0x8

    .line 1100
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1103
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onDestroy()V

    .line 1104
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    if-eqz v1, :cond_0

    .line 1105
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->stopTTSBarAnimation()V

    .line 1108
    :cond_0
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1, v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 1109
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->saveIds:Z

    if-eqz v1, :cond_1

    .line 1110
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->idsToSave:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1117
    :cond_1
    const v1, 0xddd5

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->finishActivity(I)V

    .line 1118
    return-void

    .line 1110
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1111
    .local v0, "idToSave":Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onFlowCommandCancel(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 1731
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onFlowCommandCancel(Ljava/lang/String;)V

    .line 1732
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->finish()V

    .line 1733
    return-void
.end method

.method protected onFlowListSelected(Ljava/lang/String;IZ)V
    .locals 3
    .param p1, "flowID"    # Ljava/lang/String;
    .param p2, "index"    # I
    .param p3, "isOrdinal"    # Z

    .prologue
    .line 1423
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1436
    :goto_0
    return-void

    .line 1426
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    if-eqz v0, :cond_1

    if-eqz p3, :cond_1

    .line 1427
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    add-int/2addr p2, v0

    .line 1430
    :cond_1
    if-ltz p2, :cond_2

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p2, v0, :cond_2

    .line 1431
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->selectItem(I)Z

    .line 1434
    :cond_2
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    const-wide/16 v1, 0x258

    invoke-virtual {v0, v1, v2}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 1140
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onPause()V

    .line 1141
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1267
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 1268
    if-nez p1, :cond_0

    .line 1286
    :goto_0
    return-void

    .line 1270
    :cond_0
    const-string/jumbo v0, "savedIds"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->idsToSave:Ljava/util/ArrayList;

    .line 1271
    const-string/jumbo v0, "wasShowingDialog"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1272
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->prepareAllowShareDialog()V

    .line 1274
    :cond_1
    const-string/jumbo v0, "search_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1275
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 1276
    const-string/jumbo v1, "search_text"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1275
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setSearchText(Ljava/lang/String;)V

    .line 1277
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchMode:Z

    .line 1278
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->showSearch()V

    .line 1281
    :cond_2
    const-string/jumbo v0, "IS_ERROR_STATE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1282
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 1283
    const-string/jumbo v1, "ERROR_STRING"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1282
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->displayError(Ljava/lang/CharSequence;)V

    .line 1285
    :cond_3
    const-string/jumbo v0, "dot_index"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->dotIndex:I

    goto :goto_0
.end method

.method protected onResume()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1155
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onResume()V

    .line 1156
    const v4, 0x7f090091

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1157
    const/16 v5, 0x8

    .line 1156
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1160
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v3

    .line 1161
    .local v3, "sharedPreference":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->idsToSave:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 1165
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isSearchMode()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1171
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->callOnClick()Z

    .line 1173
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1174
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchedText:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->setText(Ljava/lang/CharSequence;)V

    .line 1175
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->clearFocus()V

    .line 1176
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->requestFocus()Z

    .line 1178
    iput-boolean v7, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchMode:Z

    .line 1180
    const-string/jumbo v4, "input_method"

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    .line 1182
    .local v2, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1183
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    new-instance v5, Lcom/sec/android/automotive/drivelink/location/LocationActivity$17;

    invoke-direct {v5, p0, v2}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$17;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;Landroid/view/inputmethod/InputMethodManager;)V

    .line 1194
    const-wide/16 v6, 0x12c

    .line 1183
    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1207
    .end local v2    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    :goto_1
    const/4 v0, 0x0

    .line 1208
    .local v0, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v4, "DM_LOCATION"

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v0

    .line 1209
    if-eqz v0, :cond_1

    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    if-nez v4, :cond_1

    .line 1210
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->setSuggetionListFlowParam()V

    .line 1213
    :cond_1
    return-void

    .line 1161
    .end local v0    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1162
    .local v1, "idToSave":Ljava/lang/String;
    invoke-virtual {v3, v1, v7}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1197
    .end local v1    # "idToSave":Ljava/lang/String;
    .restart local v2    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    invoke-virtual {v4, v6}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->setCursorVisible(Z)V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 1217
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1219
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->saveIds:Z

    .line 1220
    const-string/jumbo v0, "savedIds"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->idsToSave:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1222
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1223
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchedText:Ljava/lang/String;

    .line 1224
    const-string/jumbo v0, "SIP_SEARCH_TEXT"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchedText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->allowDialog:Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;

    if-eqz v0, :cond_1

    .line 1228
    const-string/jumbo v0, "wasShowingDialog"

    .line 1229
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->allowDialog:Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;->isShowing()Z

    move-result v1

    .line 1228
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1231
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->allowDialog:Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1232
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->allowDialog:Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareAllowDialog;->dismiss()V

    .line 1236
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    if-eqz v0, :cond_3

    .line 1237
    const-string/jumbo v0, "SIP_SEARCH_MODE"

    .line 1238
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isSearchMode()Z

    move-result v1

    .line 1237
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1240
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isSearchMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1241
    const-string/jumbo v0, "search_mode"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1242
    const-string/jumbo v0, "search_text"

    .line 1243
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getSearchText()Ljava/lang/String;

    move-result-object v1

    .line 1242
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1246
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isErrorState()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1247
    const-string/jumbo v0, "IS_ERROR_STATE"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1248
    const-string/jumbo v0, "ERROR_STRING"

    .line 1249
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getErrorCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 1248
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 1253
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isTTSState()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1254
    const-string/jumbo v0, "IS_TTS_STATE"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1257
    :cond_4
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    if-eqz v0, :cond_5

    .line 1258
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->dotIndex:I

    .line 1259
    const-string/jumbo v0, "dot_index"

    .line 1260
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v1

    .line 1259
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1263
    :cond_5
    return-void
.end method

.method protected onStartResume()V
    .locals 1

    .prologue
    .line 1145
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateLocationList()V

    .line 1148
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchMode:Z

    if-nez v0, :cond_0

    .line 1149
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onStartResume()V

    .line 1151
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 322
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onStop()V

    .line 323
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 3

    .prologue
    .line 1122
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onUserLeaveHint()V

    .line 1125
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    .line 1126
    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 1125
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1127
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1128
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 1130
    :cond_0
    return-void
.end method

.method protected setDayMode()V
    .locals 3

    .prologue
    const v2, 0x7f08002a

    .line 1662
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->setDayMode()V

    .line 1664
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mMainLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 1665
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mMainLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 1668
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->locationListLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 1669
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->locationListLayout:Landroid/widget/RelativeLayout;

    .line 1670
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 1673
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_2

    .line 1674
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationViewPager:Landroid/support/v4/view/ViewPager;

    .line 1675
    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setBackgroundResource(I)V

    .line 1692
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mImgBtShare:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 1693
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mImgBtShare:Landroid/widget/ImageView;

    const v1, 0x7f0200b2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1696
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mImgBtRequest:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 1697
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mImgBtRequest:Landroid/widget/ImageView;

    .line 1698
    const v1, 0x7f0200ac

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1701
    :cond_4
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->pageNavigationLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_5

    .line 1702
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->pageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 1703
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 1706
    :cond_5
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->pageNavigationLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_6

    .line 1707
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->pageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 1708
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 1711
    :cond_6
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    if-eqz v0, :cond_7

    .line 1712
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setDayMode(Z)V

    .line 1715
    :cond_7
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->setDayMode()V

    .line 1717
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationPageAdapter:Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;

    if-eqz v0, :cond_8

    .line 1718
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->refreshList()V

    .line 1721
    :cond_8
    return-void
.end method

.method protected setNightMode()V
    .locals 3

    .prologue
    const v2, 0x7f08002e

    .line 1592
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->setNightMode()V

    .line 1594
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mMainLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 1596
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mMainLayout:Landroid/widget/RelativeLayout;

    const/high16 v1, 0x7f020000

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 1599
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->locationListLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 1600
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->locationListLayout:Landroid/widget/RelativeLayout;

    .line 1601
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 1604
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_2

    .line 1605
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationViewPager:Landroid/support/v4/view/ViewPager;

    .line 1606
    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setBackgroundResource(I)V

    .line 1619
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mImgBtShare:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 1620
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mImgBtShare:Landroid/widget/ImageView;

    .line 1621
    const v1, 0x7f0200b5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1624
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mImgBtRequest:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 1625
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mImgBtRequest:Landroid/widget/ImageView;

    .line 1626
    const v1, 0x7f0200af

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1629
    :cond_4
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->pageNavigationLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_5

    .line 1630
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->pageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 1631
    const v1, 0x7f08002b

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 1634
    :cond_5
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    if-eqz v0, :cond_6

    .line 1635
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setNightMode(Z)V

    .line 1638
    :cond_6
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->setNightMode()V

    .line 1640
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationPageAdapter:Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;

    if-eqz v0, :cond_7

    .line 1641
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->refreshList()V

    .line 1643
    :cond_7
    return-void
.end method
