.class public Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;
.super Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;
.source "SetupWizardMyCarActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$settings$SetupWizardMyCarActivity$CarBTState:[I = null

.field private static final KEY_ADDL_REG:Ljava/lang/String; = "KEY_ADDL_REG"

.field private static final KEY_CAR_DEVICE:Ljava/lang/String; = "KEY_DEVICE"

.field private static final KEY_CAR_DEVICE_ADDRESS:Ljava/lang/String; = "KEY_DEVICE_ADDRESS"

.field private static final KEY_CAR_DEVICE_NAME:Ljava/lang/String; = "KEY_DEVICE_NAME"

.field private static final KEY_FIRST_LOAD:Ljava/lang/String; = "KEY_FIRST_LOAD"

.field private static final KEY_PREV_BT_STATE:Ljava/lang/String; = "KEY_PREV_BT_STATE"

.field protected static final NAME_BT_STATE_UNREGISTERED:Ljava/lang/String; = "NAME_BT_STATE_UNREGISTERED"

.field private static final REQUEST_BT_SETTINGS:I = 0x1


# instance fields
.field private btnAdditionalRegistration:Landroid/widget/Button;

.field private btnRegisterFromMobile:Landroid/widget/Button;

.field private btnSave:Landroid/widget/Button;

.field protected canRefresh:Z

.field private cbRegisterAutoLaunch:Landroid/widget/CheckBox;

.field private isAdditionalRegistration:Z

.field private isFirstLoad:Z

.field private layoutBasicSettingsNext:Landroid/widget/LinearLayout;

.field private layoutBasicSettingsNextBtn:Landroid/widget/LinearLayout;

.field private layoutMyCarDefault:Landroid/widget/LinearLayout;

.field private layoutMyCarNotConnectedSkipMsg:Landroid/widget/LinearLayout;

.field private layoutMyCarUnRegisteredNotConnected:Landroid/widget/LinearLayout;

.field private layoutRegisterAutoLaunch:Landroid/widget/LinearLayout;

.field private mActivity:Landroid/app/Activity;

.field private mBTState:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

.field private mCarDevice:Landroid/bluetooth/BluetoothDevice;

.field private mCarDeviceAddress:Ljava/lang/String;

.field private mCarDeviceName:Ljava/lang/String;

.field private mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private final mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

.field private tvCantFind:Landroid/widget/TextView;

.field private tvMyCarMsg:Landroid/widget/TextView;

.field private tvMyCarMsg3:Landroid/widget/TextView;

.field private tvMyCarMsgBasic1:Landroid/widget/TextView;

.field private tvMyCarMsgBasic2:Landroid/widget/TextView;

.field private tvMyCarName:Landroid/widget/TextView;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$settings$SetupWizardMyCarActivity$CarBTState()[I
    .locals 3

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$settings$SetupWizardMyCarActivity$CarBTState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->values()[Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->FIRST_ACCESS_REGISTERED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->UNREGISTERED_CONNECTED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->UNREGISTERED_NOT_CONNECTED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$settings$SetupWizardMyCarActivity$CarBTState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;-><init>()V

    .line 55
    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    .line 56
    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->canRefresh:Z

    .line 41
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mBTState:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->cbRegisterAutoLaunch:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)V
    .locals 0

    .prologue
    .line 552
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->initLayout()V

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 508
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->makeDiscoverable(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;Z)V
    .locals 0

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->isAdditionalRegistration:Z

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutMyCarDefault:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutMyCarUnRegisteredNotConnected:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsg3:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvCantFind:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutMyCarNotConnectedSkipMsg:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)V
    .locals 0

    .prologue
    .line 380
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->initNewBTValues()V

    return-void
.end method

.method private initLayout()V
    .locals 7

    .prologue
    const v2, 0x7f0a0418

    const/4 v6, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 553
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$settings$SetupWizardMyCarActivity$CarBTState()[I

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mBTState:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 635
    :goto_0
    return-void

    .line 555
    :pswitch_0
    const-string/jumbo v0, "BaseActivity"

    .line 556
    const-string/jumbo v1, "initLayout FIRST_ACCESS_REGISTERED"

    .line 555
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutMyCarUnRegisteredNotConnected:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 558
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutMyCarDefault:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 559
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsg:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 560
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsg:Landroid/widget/TextView;

    .line 561
    const v1, 0x7f0a041f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 562
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsgBasic1:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 563
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsgBasic2:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 564
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 565
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutRegisterAutoLaunch:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 566
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->btnAdditionalRegistration:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 567
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->btnSave:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 570
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutMyCarUnRegisteredNotConnected:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 571
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutMyCarDefault:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 572
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isFirstAccess(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 573
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->isFirstLoad:Z

    if-eqz v0, :cond_0

    .line 574
    const-string/jumbo v0, "BaseActivity"

    .line 575
    const-string/jumbo v1, "initLayout UNREGISTERED_CONNECTED isFirstAccess isFirstLoad"

    .line 574
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 576
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsg:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 577
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsg:Landroid/widget/TextView;

    .line 578
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 579
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsgBasic1:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 580
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsgBasic2:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 581
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 582
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutRegisterAutoLaunch:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 583
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->btnAdditionalRegistration:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 584
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->btnSave:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    .line 586
    :cond_0
    const-string/jumbo v0, "BaseActivity"

    .line 587
    const-string/jumbo v1, "initLayout UNREGISTERED_CONNECTED isFirstAccess !isFirstLoad"

    .line 586
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsg:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 589
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsgBasic1:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 590
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsgBasic2:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 591
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsgBasic1:Landroid/widget/TextView;

    .line 592
    const v1, 0x7f0a0417

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 593
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsgBasic2:Landroid/widget/TextView;

    .line 594
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 595
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 596
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutRegisterAutoLaunch:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 597
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->btnAdditionalRegistration:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 598
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->btnSave:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    .line 601
    :cond_1
    const-string/jumbo v0, "BaseActivity"

    .line 602
    const-string/jumbo v1, "initLayout UNREGISTERED_CONNECTED !isFirstAccess"

    .line 601
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsg:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 604
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsg:Landroid/widget/TextView;

    .line 605
    const v1, 0x7f0a0416

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 606
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsgBasic1:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 607
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsgBasic2:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 608
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 609
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutRegisterAutoLaunch:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 610
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->btnAdditionalRegistration:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 611
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->btnSave:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    .line 615
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutMyCarDefault:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 616
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutMyCarUnRegisteredNotConnected:Landroid/widget/LinearLayout;

    .line 617
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 618
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsg3:Landroid/widget/TextView;

    .line 619
    const v1, 0x7f0a041b

    new-array v2, v6, [Ljava/lang/Object;

    .line 620
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->getPhoneName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    .line 618
    invoke-virtual {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 621
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvCantFind:Landroid/widget/TextView;

    .line 622
    const v1, 0x7f0a041c

    new-array v2, v6, [Ljava/lang/Object;

    .line 623
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->getPhoneName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    .line 621
    invoke-virtual {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 624
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isFirstAccess(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 625
    const-string/jumbo v0, "BaseActivity"

    .line 626
    const-string/jumbo v1, "initLayout UNREGISTERED_NOT_CONNECTED isFirstAccess"

    .line 625
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutMyCarNotConnectedSkipMsg:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 629
    :cond_2
    const-string/jumbo v0, "BaseActivity"

    .line 630
    const-string/jumbo v1, "initLayout UNREGISTERED_NOT_CONNECTED !isFirstAccess"

    .line 629
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutMyCarNotConnectedSkipMsg:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 553
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private initLayout(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;)V
    .locals 0
    .param p1, "btState"    # Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    .prologue
    .line 540
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mBTState:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    .line 541
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->initLayout()V

    .line 542
    return-void
.end method

.method private initNewBTValues()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 382
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "NAME_BT_STATE_UNREGISTERED"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 384
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDevice:Landroid/bluetooth/BluetoothDevice;

    .line 385
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->setDevice()V

    .line 386
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->UNREGISTERED_NOT_CONNECTED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mBTState:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    .line 419
    :goto_0
    return-void

    .line 387
    :cond_0
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isFirstAccess(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 388
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->hasRegisteredDevice(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 389
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->isAdditionalRegistration:Z

    if-nez v1, :cond_1

    .line 390
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->FIRST_ACCESS_REGISTERED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mBTState:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    .line 392
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->getRegisteredCar(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    move-result-object v0

    .line 393
    .local v0, "carDevice":Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getPlaceName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceName:Ljava/lang/String;

    .line 394
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceAddress:Ljava/lang/String;

    goto :goto_0

    .line 395
    .end local v0    # "carDevice":Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->getConnectedDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 397
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->getConnectedDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .line 396
    invoke-static {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isRegisteredDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v1

    .line 397
    if-nez v1, :cond_2

    .line 398
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->getConnectedDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDevice:Landroid/bluetooth/BluetoothDevice;

    .line 399
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->setDevice()V

    .line 400
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->UNREGISTERED_CONNECTED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mBTState:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    goto :goto_0

    .line 401
    :cond_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->getConnectedDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 403
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->getConnectedDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .line 402
    invoke-static {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isRegisteredDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v1

    .line 403
    if-nez v1, :cond_3

    .line 404
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->getConnectedDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDevice:Landroid/bluetooth/BluetoothDevice;

    .line 405
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->setDevice()V

    .line 406
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->UNREGISTERED_CONNECTED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mBTState:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    goto :goto_0

    .line 407
    :cond_3
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->getConnectedDevice2()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 409
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->getConnectedDevice2()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .line 408
    invoke-static {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isRegisteredDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v1

    .line 409
    if-nez v1, :cond_4

    .line 410
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->getConnectedDevice2()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDevice:Landroid/bluetooth/BluetoothDevice;

    .line 411
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->setDevice()V

    .line 412
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->UNREGISTERED_CONNECTED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mBTState:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    goto/16 :goto_0

    .line 415
    :cond_4
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDevice:Landroid/bluetooth/BluetoothDevice;

    .line 416
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->setDevice()V

    .line 417
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->UNREGISTERED_NOT_CONNECTED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mBTState:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    goto/16 :goto_0
.end method

.method private initPrevBTValues(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 357
    .line 358
    const-string/jumbo v0, "KEY_PREV_BT_STATE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 357
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mBTState:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    .line 360
    const-string/jumbo v0, "KEY_DEVICE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 359
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDevice:Landroid/bluetooth/BluetoothDevice;

    .line 362
    const-string/jumbo v0, "KEY_DEVICE_NAME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 361
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceName:Ljava/lang/String;

    .line 364
    const-string/jumbo v0, "KEY_DEVICE_ADDRESS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 363
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceAddress:Ljava/lang/String;

    .line 366
    const-string/jumbo v0, "KEY_ADDL_REG"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 365
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->isAdditionalRegistration:Z

    .line 368
    const-string/jumbo v0, "KEY_FIRST_LOAD"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 367
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->isFirstLoad:Z

    .line 369
    return-void
.end method

.method private makeDiscoverable(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 509
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getScanMode()I

    move-result v1

    const/16 v2, 0x17

    if-ne v1, v2, :cond_1

    .line 528
    :cond_0
    :goto_0
    return-void

    .line 513
    :cond_1
    if-eqz p1, :cond_2

    .line 515
    const-string/jumbo v1, "KEY_PREV_BT_STATE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 519
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mBTState:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->UNREGISTERED_NOT_CONNECTED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    if-ne v1, v2, :cond_0

    .line 523
    new-instance v0, Landroid/content/Intent;

    .line 524
    const-string/jumbo v1, "android.bluetooth.adapter.action.REQUEST_DISCOVERABLE"

    .line 523
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 525
    .local v0, "enableIntent":Landroid/content/Intent;
    const-string/jumbo v1, "android.bluetooth.adapter.extra.DISCOVERABLE_DURATION"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 527
    const/4 v1, 0x1

    .line 526
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private setDevice()V
    .locals 2

    .prologue
    .line 430
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDevice:Landroid/bluetooth/BluetoothDevice;

    if-nez v0, :cond_0

    .line 431
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceName:Ljava/lang/String;

    .line 432
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceAddress:Ljava/lang/String;

    .line 439
    :goto_0
    return-void

    .line 434
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAlias()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, ""

    .line 435
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAlias()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDevice:Landroid/bluetooth/BluetoothDevice;

    .line 436
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    .line 434
    :goto_1
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceName:Ljava/lang/String;

    .line 437
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceAddress:Ljava/lang/String;

    goto :goto_0

    .line 436
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAlias()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private showUnregisteredConnectedLayout(Landroid/bluetooth/BluetoothDevice;)V
    .locals 2
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 648
    invoke-static {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isRegisteredDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 649
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mBTState:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->FIRST_ACCESS_REGISTERED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    if-eq v0, v1, :cond_0

    .line 650
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDevice:Landroid/bluetooth/BluetoothDevice;

    .line 651
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->setDevice()V

    .line 652
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->UNREGISTERED_CONNECTED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->initLayout(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;)V

    .line 654
    :cond_0
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 468
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 469
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 474
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 723
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->isAdditionalRegistration:Z

    if-eqz v0, :cond_0

    .line 724
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->isAdditionalRegistration:Z

    .line 725
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->initNewBTValues()V

    .line 726
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->initLayout()V

    .line 731
    :goto_0
    return-void

    .line 728
    :cond_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onBondStateChanged(Landroid/bluetooth/BluetoothDevice;I)V
    .locals 1
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "state"    # I

    .prologue
    .line 696
    const/16 v0, 0xc

    if-ne v0, p2, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->canRefresh:Z

    if-eqz v0, :cond_0

    .line 697
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->isFirstLoad:Z

    .line 698
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->showUnregisteredConnectedLayout(Landroid/bluetooth/BluetoothDevice;)V

    .line 701
    :cond_0
    return-void
.end method

.method public onConnect(Landroid/bluetooth/BluetoothDevice;)V
    .locals 3
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/4 v2, 0x0

    .line 665
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    .line 666
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->canRefresh:Z

    if-eqz v0, :cond_0

    .line 667
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->isAdditionalRegistration:Z

    .line 668
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->isFirstLoad:Z

    .line 669
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->showUnregisteredConnectedLayout(Landroid/bluetooth/BluetoothDevice;)V

    .line 671
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v9, 0x7f090112

    const v8, 0x7f090005

    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 99
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->onCreate(Landroid/os/Bundle;)V

    .line 100
    const v2, 0x7f030034

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->setContentView(I)V

    .line 101
    iput-object p0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mActivity:Landroid/app/Activity;

    .line 102
    const-string/jumbo v2, "BaseActivity"

    const-string/jumbo v3, "onCreate"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v2

    .line 104
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 106
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 106
    invoke-interface {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestBluetoothStartAdapter(Landroid/content/Context;)V

    .line 109
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    invoke-virtual {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->register(Landroid/content/Context;)V

    .line 110
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->setInitialValues(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;)V

    .line 112
    if-eqz p1, :cond_1

    .line 114
    const-string/jumbo v2, "KEY_PREV_BT_STATE"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 115
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->initPrevBTValues(Landroid/os/Bundle;)V

    .line 123
    :goto_0
    const v2, 0x7f090011

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 122
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutBasicSettingsNext:Landroid/widget/LinearLayout;

    .line 125
    const v2, 0x7f090012

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 124
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutBasicSettingsNextBtn:Landroid/widget/LinearLayout;

    .line 126
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutBasicSettingsNextBtn:Landroid/widget/LinearLayout;

    .line 127
    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$1;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 156
    const v2, 0x7f09015c

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 155
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutMyCarUnRegisteredNotConnected:Landroid/widget/LinearLayout;

    .line 157
    const v2, 0x7f09015d

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsg3:Landroid/widget/TextView;

    .line 158
    const v2, 0x7f09015e

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvCantFind:Landroid/widget/TextView;

    .line 160
    const v2, 0x7f09015f

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 159
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->btnRegisterFromMobile:Landroid/widget/Button;

    .line 161
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->btnRegisterFromMobile:Landroid/widget/Button;

    new-instance v3, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setScroller(Landroid/widget/Scroller;)V

    .line 162
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->btnRegisterFromMobile:Landroid/widget/Button;

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setVerticalScrollBarEnabled(Z)V

    .line 163
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->btnRegisterFromMobile:Landroid/widget/Button;

    .line 164
    new-instance v3, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v3}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 166
    const v2, 0x7f090160

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 165
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutMyCarNotConnectedSkipMsg:Landroid/widget/LinearLayout;

    .line 169
    const v2, 0x7f090161

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 168
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutMyCarDefault:Landroid/widget/LinearLayout;

    .line 170
    const v2, 0x7f090162

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsg:Landroid/widget/TextView;

    .line 172
    const v2, 0x7f090163

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 171
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsgBasic1:Landroid/widget/TextView;

    .line 174
    const v2, 0x7f090164

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 173
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsgBasic2:Landroid/widget/TextView;

    .line 175
    const v2, 0x7f090167

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarName:Landroid/widget/TextView;

    .line 177
    const v2, 0x7f090168

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 176
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutRegisterAutoLaunch:Landroid/widget/LinearLayout;

    .line 179
    const v2, 0x7f090169

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 178
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->cbRegisterAutoLaunch:Landroid/widget/CheckBox;

    .line 181
    const v2, 0x7f09016a

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 180
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->btnAdditionalRegistration:Landroid/widget/Button;

    .line 182
    const v2, 0x7f0900e6

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->btnSave:Landroid/widget/Button;

    .line 184
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->cbRegisterAutoLaunch:Landroid/widget/CheckBox;

    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v3

    .line 186
    const-string/jumbo v4, "PREF_SETTINGS_MY_CAR_AUTO_LAUNCH"

    .line 185
    invoke-virtual {v3, v4, v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v3

    .line 184
    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 187
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->cbRegisterAutoLaunch:Landroid/widget/CheckBox;

    .line 188
    const v3, 0x7f0a0421

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 187
    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 189
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutRegisterAutoLaunch:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$2;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->cbRegisterAutoLaunch:Landroid/widget/CheckBox;

    .line 200
    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$3;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 212
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isFirstAccess(Landroid/app/Activity;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 214
    const v2, 0x7f090165

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 213
    check-cast v1, Landroid/widget/LinearLayout;

    .line 215
    .local v1, "ll":Landroid/widget/LinearLayout;
    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    .end local v1    # "ll":Landroid/widget/LinearLayout;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->btnSave:Landroid/widget/Button;

    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$5;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 252
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->btnRegisterFromMobile:Landroid/widget/Button;

    .line 253
    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$6;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 263
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->btnAdditionalRegistration:Landroid/widget/Button;

    .line 264
    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$7;

    invoke-direct {v3, p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$7;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;Landroid/os/Bundle;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 287
    const v2, 0x7f0a03e5

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->setTitle(I)V

    .line 288
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isFirstAccess(Landroid/app/Activity;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 289
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutBasicSettingsNext:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 290
    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 291
    invoke-virtual {p0, v8}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 292
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 293
    const v4, 0x7f0d0041

    .line 292
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 291
    invoke-virtual {v2, v3, v5, v5, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 294
    const v2, 0x7f090111

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 295
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020003

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 294
    invoke-virtual {v2, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 314
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->initLayout()V

    .line 315
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->makeDiscoverable(Landroid/os/Bundle;)V

    .line 317
    return-void

    .line 117
    :cond_1
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->isFirstLoad:Z

    .line 118
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->isAdditionalRegistration:Z

    .line 119
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->initNewBTValues()V

    goto/16 :goto_0

    .line 297
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutBasicSettingsNext:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 298
    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 301
    invoke-virtual {p0, v8}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 300
    check-cast v0, Landroid/widget/LinearLayout;

    .line 302
    .local v0, "btnNavBack":Landroid/widget/LinearLayout;
    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$8;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$8;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 308
    new-instance v2, Ljava/lang/StringBuilder;

    .line 309
    const v3, 0x7f0a03e5

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 310
    const-string/jumbo v3, ". "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 311
    const v3, 0x7f0a03f4

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 308
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 712
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->onDestroy()V

    .line 713
    const-string/jumbo v0, "BaseActivity"

    const-string/jumbo v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 715
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getScanMode()I

    move-result v0

    const/16 v1, 0x17

    if-ne v0, v1, :cond_0

    .line 717
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    .line 719
    :cond_0
    return-void
.end method

.method public onDisconnect(Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 684
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    .line 321
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->onResume()V

    .line 322
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->canRefresh:Z

    .line 323
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mBTState:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->UNREGISTERED_CONNECTED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    if-ne v3, v4, :cond_1

    .line 324
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v3, :cond_1

    .line 325
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 326
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 327
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    .line 328
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 327
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 343
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 344
    .local v2, "service":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 345
    return-void

    .line 328
    .end local v2    # "service":Landroid/content/Intent;
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 329
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v0, :cond_0

    .line 330
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    .line 331
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    .line 330
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 331
    if-eqz v4, :cond_0

    .line 332
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAlias()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    const-string/jumbo v4, ""

    .line 333
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAlias()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_3
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    .line 335
    .local v1, "newAlias":Ljava/lang/String;
    :goto_1
    if-eqz v1, :cond_0

    .line 336
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 337
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceName:Ljava/lang/String;

    .line 338
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarName:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 334
    .end local v1    # "newAlias":Ljava/lang/String;
    :cond_4
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAlias()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 485
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 486
    const-string/jumbo v0, "KEY_PREV_BT_STATE"

    .line 487
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mBTState:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->name()Ljava/lang/String;

    move-result-object v1

    .line 486
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    const-string/jumbo v0, "KEY_DEVICE"

    .line 489
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDevice:Landroid/bluetooth/BluetoothDevice;

    .line 488
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 490
    const-string/jumbo v0, "KEY_DEVICE_NAME"

    .line 491
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceName:Ljava/lang/String;

    .line 490
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    const-string/jumbo v0, "KEY_DEVICE_ADDRESS"

    .line 493
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceAddress:Ljava/lang/String;

    .line 492
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    const-string/jumbo v0, "KEY_ADDL_REG"

    .line 495
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->isAdditionalRegistration:Z

    .line 494
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 496
    const-string/jumbo v0, "KEY_FIRST_LOAD"

    .line 497
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->isFirstLoad:Z

    .line 496
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 498
    return-void
.end method

.method protected saveConnectedDeviceToSharedPreferences()V
    .locals 3

    .prologue
    .line 450
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceName:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string/jumbo v0, ""

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 451
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceAddress:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 452
    const-string/jumbo v0, ""

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 454
    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceName:Ljava/lang/String;

    .line 455
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mCarDeviceAddress:Ljava/lang/String;

    .line 454
    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    invoke-static {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->addRegisteredCarsPreference(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;)V

    .line 457
    :cond_0
    return-void
.end method
