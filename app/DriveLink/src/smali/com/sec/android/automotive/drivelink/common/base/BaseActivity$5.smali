.class Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;
.super Landroid/content/BroadcastReceiver;
.source "BaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->setBaseBroadCastReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    .line 461
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 465
    .line 466
    const-string/jumbo v3, "EXTRA_FLOW_COMMAND"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 468
    .local v0, "command":Ljava/lang/String;
    const-string/jumbo v3, "EXTRA_FLOW_COMMAND_FLOWID"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 470
    .local v1, "flowID":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 573
    :cond_0
    :goto_0
    return-void

    .line 473
    :cond_1
    const-string/jumbo v3, "BaseActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "[mVoiceCommandReceiver] - mFlowID:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    iget-object v5, v5, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mFlowID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    if-eqz v1, :cond_2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mFlowID:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 475
    const-string/jumbo v3, "DM_RETURN_MAIN"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 476
    const-string/jumbo v3, "BaseActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "[mVoiceCommandReceiver] - Canceled. "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 477
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 478
    const-string/jumbo v5, "command : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " / flowld :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 479
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 476
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 484
    :cond_2
    const-string/jumbo v3, "BaseActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "[mVoiceCommandReceiver] - "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 485
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "command : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 486
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 484
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v3, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowCommandCustom(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 489
    const-string/jumbo v3, "DM_RETURN_MAIN"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 490
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowCommandReturnMain()V

    goto/16 :goto_0

    .line 491
    :cond_3
    const-string/jumbo v3, "DM_CANCEL"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 492
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->mPrevFlowID:Ljava/lang/String;

    const-string/jumbo v4, "DM_MAIN"

    if-ne v3, v4, :cond_4

    .line 496
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIsCanceledtoMain(Z)V

    .line 498
    :cond_4
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v3, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowCommandCancel(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 499
    :cond_5
    const-string/jumbo v3, "DM_CONFIRM_YES"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 500
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v3, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowCommandYes(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 501
    :cond_6
    const-string/jumbo v3, "DM_CONFIRM_NO"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 502
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v3, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowCommandNo(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 503
    :cond_7
    const-string/jumbo v3, "DM_CONFIRM_SEND"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 504
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v3, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowCommandSend(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 505
    :cond_8
    const-string/jumbo v3, "DM_CONFIRM_EXCUTE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 506
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v3, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowCommandExcute(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 507
    :cond_9
    const-string/jumbo v3, "DM_CONFIRM_IGNORE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 508
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v3, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowCommandIgnore(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 509
    :cond_a
    const-string/jumbo v3, "DM_CONFIRM_LOOKUP"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 510
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v3, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowCommandLookup(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 511
    :cond_b
    const-string/jumbo v3, "DM_CONFIRM_RESET"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 512
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v3, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowCommandReset(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 513
    :cond_c
    const-string/jumbo v3, "DM_CONFIRM_ROUTE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 514
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v3, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowCommandRoute(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 515
    :cond_d
    const-string/jumbo v3, "DM_CONFIRM_ROUTE_NOP"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 516
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v3, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowCommandRouteNop(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 517
    :cond_e
    const-string/jumbo v3, "DM_REPLY"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 518
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v3, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowCommandReply(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 519
    :cond_f
    const-string/jumbo v3, "DM_CALL"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 520
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v3, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowCommandCall(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 521
    :cond_10
    const-string/jumbo v3, "DM_READ"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 522
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v3, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowCommandRead(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 523
    :cond_11
    const-string/jumbo v3, "DM_NEXT"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 524
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v3, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowCommandNext(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 525
    :cond_12
    const-string/jumbo v3, "DM_STOP_NAVIGATION"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 526
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v3, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowCommandStopNavigation(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 527
    :cond_13
    const-string/jumbo v3, "DM_LIST_SELECTED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 529
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v2

    .line 530
    .local v2, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v2, :cond_0

    .line 531
    const-string/jumbo v3, "DM_SMS_CONTACT"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    .line 533
    const-string/jumbo v3, "DM_SMS_CONTACT_SEARCH_LIST"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    .line 534
    const-string/jumbo v3, "DM_SMS_INBOX"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    .line 535
    const-string/jumbo v3, "DM_SMS_TYPE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    .line 537
    const-string/jumbo v3, "DM_SMS_INBOX_SEARCH_LIST"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 538
    :cond_14
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    .line 539
    iget v4, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionSelected:I

    .line 540
    iget-object v5, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageText:Ljava/lang/String;

    .line 538
    invoke-virtual {v3, v1, v4, v7, v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowListSelectedForMessage(Ljava/lang/String;IZLjava/lang/String;)V

    goto/16 :goto_0

    .line 542
    :cond_15
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    .line 543
    iget v4, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionSelected:I

    .line 542
    invoke-virtual {v3, v1, v4, v7}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowListSelected(Ljava/lang/String;IZ)V

    goto/16 :goto_0

    .line 546
    .end local v2    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_16
    const-string/jumbo v3, "DM_LIST_ORDINAL"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 548
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v2

    .line 549
    .restart local v2    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v2, :cond_0

    .line 550
    iget v3, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionSelected:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_17

    .line 551
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowListSelectedFailed()V

    goto/16 :goto_0

    .line 553
    :cond_17
    const-string/jumbo v3, "DM_SMS_CONTACT"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_18

    .line 555
    const-string/jumbo v3, "DM_SMS_CONTACT_SEARCH_LIST"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_18

    .line 556
    const-string/jumbo v3, "DM_SMS_INBOX"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_18

    .line 557
    const-string/jumbo v3, "DM_SMS_TYPE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_18

    .line 559
    const-string/jumbo v3, "DM_SMS_INBOX_SEARCH_LIST"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_19

    .line 560
    :cond_18
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    .line 561
    iget v4, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionSelected:I

    .line 562
    iget-object v5, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageText:Ljava/lang/String;

    .line 560
    invoke-virtual {v3, v1, v4, v6, v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowListSelectedForMessage(Ljava/lang/String;IZLjava/lang/String;)V

    goto/16 :goto_0

    .line 564
    :cond_19
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    .line 565
    iget v4, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionSelected:I

    .line 564
    invoke-virtual {v3, v1, v4, v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onFlowListSelected(Ljava/lang/String;IZ)V

    goto/16 :goto_0
.end method
