.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity;->initSearchTextField()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    .locals 1

    .prologue
    .line 583
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    return-object v0
.end method

.method private getSearchedInboxList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "keyword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 700
    const-string/jumbo v6, "i"

    const-string/jumbo v7, "getSearchedInboxList"

    .line 701
    const-string/jumbo v8, "[snowdeer] ================================================="

    .line 700
    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v6, v7, v8}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$39(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    const-string/jumbo v6, "i"

    const-string/jumbo v7, "getSearchedInboxList"

    .line 703
    const-string/jumbo v8, "[snowdeer] getSearchedContactList is called"

    .line 702
    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v6, v7, v8}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$39(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    const-string/jumbo v6, "i"

    const-string/jumbo v7, "getSearchedInboxList"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "[snowdeer] keyword : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 705
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 704
    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v6, v7, v8}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$39(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    const-string/jumbo v6, "i"

    const-string/jumbo v7, "getSearchedInboxList"

    .line 709
    const-string/jumbo v8, "[snowdeer] ================================================="

    .line 708
    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v6, v7, v8}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$39(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 712
    .local v4, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$54(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v0, v6, :cond_0

    .line 743
    return-object v4

    .line 713
    :cond_0
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$54(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 714
    .local v1, "inbox":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    const-string/jumbo v3, ""

    .line 716
    .local v3, "name":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getDLContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 717
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getDLContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 718
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getDLContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    .line 719
    const-string/jumbo v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 720
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getDLContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    .line 729
    :goto_1
    const-string/jumbo v6, "i"

    const-string/jumbo v7, "getSearchedInboxList"

    .line 730
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "[snowdeer] cachedContact : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 729
    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v6, v7, v8}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$39(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    .line 733
    .local v5, "src":Ljava/lang/String;
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 737
    .local v2, "key":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;->getInstance()Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;

    move-result-object v6

    invoke-virtual {v6, v5, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;->checkString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 738
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 712
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 722
    .end local v2    # "key":Ljava/lang/String;
    .end local v5    # "src":Ljava/lang/String;
    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    .line 725
    goto :goto_1

    .line 726
    :cond_3
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1, "arg0"    # Landroid/widget/TextView;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v8, 0x42

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 587
    if-eqz p3, :cond_2

    .line 588
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    if-eq v4, v8, :cond_0

    .line 589
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    const/16 v5, 0x54

    if-ne v4, v5, :cond_2

    .line 590
    :cond_0
    const-string/jumbo v4, "i"

    const-string/jumbo v5, "onEditorAction"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, " : mCurrentMode = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 591
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "text"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 590
    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v4, v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$39(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$47(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 695
    :goto_0
    return v2

    .line 597
    :cond_1
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$42(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 598
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$47(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 601
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListMode:I
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$43(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)I

    move-result v4

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    .line 603
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 606
    .local v0, "dlServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    new-instance v2, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11$1;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11$1;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;)V

    invoke-interface {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkContactListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;)V

    .line 667
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$47(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    .line 668
    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v4

    .line 666
    invoke-interface {v0, v2, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestSearchedContactList(Landroid/content/Context;Ljava/lang/String;)V

    .end local v0    # "dlServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    :cond_2
    :goto_1
    move v2, v3

    .line 695
    goto :goto_0

    .line 669
    :cond_3
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListMode:I
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$43(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)I

    move-result v4

    const/16 v5, 0x20

    if-ne v4, v5, :cond_2

    .line 673
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-static {v4, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$53(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 674
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 675
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$47(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    .line 676
    invoke-interface {v6}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    .line 674
    invoke-direct {p0, v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->getSearchedInboxList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$32(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Ljava/util/ArrayList;)V

    .line 677
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$33(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_4

    .line 678
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    const/16 v4, 0x43

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setCurrentMode(I)I
    invoke-static {v2, v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity;I)I

    .line 691
    :goto_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$53(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    goto :goto_1

    .line 681
    :cond_4
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$33(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 682
    .local v1, "size":I
    const/16 v4, 0x28

    if-le v1, v4, :cond_5

    .line 683
    const/16 v1, 0x28

    .line 685
    :cond_5
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$42(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/TextView;

    move-result-object v4

    .line 686
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 687
    const v6, 0x7f0a0244

    .line 686
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    .line 687
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    .line 688
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$47(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    aput-object v7, v6, v2

    .line 685
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 689
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setCurrentMode(I)I
    invoke-static {v2, v8}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity;I)I

    goto :goto_2
.end method
