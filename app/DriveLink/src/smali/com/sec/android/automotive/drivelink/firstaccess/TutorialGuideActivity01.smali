.class public Lcom/sec/android/automotive/drivelink/firstaccess/TutorialGuideActivity01;
.super Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;
.source "TutorialGuideActivity01.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mNextBtn:Landroid/widget/LinearLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/sec/android/automotive/drivelink/firstaccess/TutorialGuideActivity01;

    .line 29
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 28
    sput-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/TutorialGuideActivity01;->TAG:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    sget-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/TutorialGuideActivity01;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onCreate"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    const v0, 0x7f03003b

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/TutorialGuideActivity01;->setContentView(I)V

    .line 38
    const v0, 0x7f0a0279

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/TutorialGuideActivity01;->setTitle(I)V

    .line 41
    const v0, 0x7f090012

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/TutorialGuideActivity01;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 40
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/TutorialGuideActivity01;->mNextBtn:Landroid/widget/LinearLayout;

    .line 43
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->setContext(Landroid/content/Context;)V

    .line 45
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/TutorialGuideActivity01;->mNextBtn:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/firstaccess/TutorialGuideActivity01$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/TutorialGuideActivity01$1;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/TutorialGuideActivity01;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    return-void
.end method
