.class public Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$SineEaseOut;
.super Ljava/lang/Object;
.source "VoicePhoneActionBarLayout.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SineEaseOut"
.end annotation


# instance fields
.field public s:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 577
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 575
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$SineEaseOut;->s:F

    .line 578
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 575
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$SineEaseOut;->s:F

    .line 581
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 3
    .param p1, "input"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 584
    div-float v0, p1, v2

    const v1, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->sin(F)F

    move-result v0

    mul-float/2addr v0, v2

    const/4 v1, 0x0

    add-float/2addr v0, v1

    return v0
.end method
