.class public Lcom/sec/android/automotive/drivelink/common/flow/FlowConstants$FlowAction;
.super Ljava/lang/Object;
.source "FlowConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/flow/FlowConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FlowAction"
.end annotation


# static fields
.field public static final ACTION_DL_DM_FLOW_CHANGED:Ljava/lang/String; = "com.sec.android.automotive.drivelink.ACTION_DL_DM_FLOW_CHANGED"

.field public static final ACTION_DL_DM_FLOW_COMMAND:Ljava/lang/String; = "com.sec.android.automotive.drivelink.ACTION_DL_DM_FLOW_COMMAND"

.field public static final EXTRA_DRIVEN_TYPE:Ljava/lang/String; = "EXTRA_DRIVEN_TYPE"

.field public static final EXTRA_FLOW_COMMAND:Ljava/lang/String; = "EXTRA_FLOW_COMMAND"

.field public static final EXTRA_FLOW_COMMAND_FLOWID:Ljava/lang/String; = "EXTRA_FLOW_COMMAND_FLOWID"

.field public static final EXTRA_FLOW_ID:Ljava/lang/String; = "EXTRA_FLOW_ID"

.field public static final EXTRA_FROM_FLOW_MGR:Ljava/lang/String; = "EXTRA_FROM_FLOW_MGR"


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/flow/FlowConstants;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/common/flow/FlowConstants;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowConstants$FlowAction;->this$0:Lcom/sec/android/automotive/drivelink/common/flow/FlowConstants;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
