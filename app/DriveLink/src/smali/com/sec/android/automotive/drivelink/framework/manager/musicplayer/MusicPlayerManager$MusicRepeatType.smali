.class public final enum Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;
.super Ljava/lang/Enum;
.source "MusicPlayerManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MusicRepeatType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ALL:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

.field public static final enum NONE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

.field public static final enum ONE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    const-string/jumbo v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->NONE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    const-string/jumbo v1, "ONE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->ONE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    const-string/jumbo v1, "ALL"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->ALL:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    .line 38
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->NONE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->ONE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->ALL:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getEnum(I)Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;
    .locals 1
    .param p0, "val"    # I

    .prologue
    .line 42
    packed-switch p0, :pswitch_data_0

    .line 48
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->NONE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    :goto_0
    return-object v0

    .line 44
    :pswitch_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->ONE:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    goto :goto_0

    .line 46
    :pswitch_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->ALL:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    goto :goto_0

    .line 42
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
