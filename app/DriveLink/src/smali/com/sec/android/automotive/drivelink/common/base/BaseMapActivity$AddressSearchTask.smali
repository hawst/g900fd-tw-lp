.class public Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;
.super Landroid/os/AsyncTask;
.source "BaseMapActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AddressSearchTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private address:Ljava/lang/String;

.field private createMarks:Z

.field private latitude:D

.field private longitude:D

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;DDLjava/lang/String;)V
    .locals 4
    .param p2, "lat"    # D
    .param p4, "lng"    # D
    .param p6, "addr"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, 0x0

    .line 985
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 980
    iput-wide v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->latitude:D

    .line 981
    iput-wide v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->longitude:D

    .line 982
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->address:Ljava/lang/String;

    .line 983
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->createMarks:Z

    .line 986
    iput-wide p2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->latitude:D

    .line 987
    iput-wide p4, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->longitude:D

    .line 988
    iput-object p6, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->address:Ljava/lang/String;

    .line 990
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->latitude:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->longitude:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 991
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->createMarks:Z

    .line 993
    :cond_0
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 7
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const-wide/16 v4, 0x0

    .line 997
    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->latitude:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->longitude:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    .line 999
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->address:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->address:Ljava/lang/String;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1000
    const/4 v1, 0x0

    .line 1004
    .local v1, "latLng":Lcom/google/android/gms/maps/model/LatLng;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->address:Ljava/lang/String;

    .line 1005
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 1004
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/location/map/util/GeocodingAPI;->getLatLngFromAddress(Ljava/lang/String;Landroid/content/Context;)Lcom/google/android/gms/maps/model/LatLng;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1011
    :goto_0
    if-eqz v1, :cond_0

    .line 1012
    iget-wide v2, v1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iput-wide v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->latitude:D

    .line 1013
    iget-wide v2, v1, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    iput-wide v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->longitude:D

    .line 1018
    .end local v1    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->address:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->address:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1019
    :cond_1
    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->latitude:D

    .line 1020
    iget-wide v4, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->longitude:D

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v6

    .line 1019
    invoke-static {v2, v3, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/location/map/util/GeocodingAPI;->getAddressFromLatLng(DDLandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->address:Ljava/lang/String;

    .line 1022
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->address:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->address:Ljava/lang/String;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1023
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a021b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->address:Ljava/lang/String;

    .line 1026
    :cond_3
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->address:Ljava/lang/String;

    return-object v2

    .line 1006
    .restart local v1    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    :catch_0
    move-exception v0

    .line 1007
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->finishIsOffline()Z

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 7
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 1032
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBalloonMarker:Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$1(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1033
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mBalloonMarker:Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$1(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->removeMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V

    .line 1034
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$2(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V

    .line 1037
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$4(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;Ljava/lang/String;)V

    .line 1038
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    iget-wide v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->latitude:D

    iget-wide v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->longitude:D

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setLocationPoint(DD)V

    .line 1040
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mLocationMap:Lcom/sec/android/automotive/drivelink/location/map/LocationMap;

    const v1, 0x3f19999a    # 0.6f

    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->latitude:D

    iget-wide v4, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->longitude:D

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mUserIndex:I
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$5(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setZoomLevel(FDDI)V

    .line 1041
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$6(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->latitude:D

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->setLatitude(D)V

    .line 1042
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->mPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->access$6(Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->longitude:D

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->setLongitude(D)V

    .line 1044
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->createMarks:Z

    if-eqz v0, :cond_1

    .line 1045
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    iget-wide v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->latitude:D

    iget-wide v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->longitude:D

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->createMarks(DDLjava/lang/String;)V

    .line 1049
    :goto_0
    return-void

    .line 1047
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;

    iget-wide v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->latitude:D

    iget-wide v3, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity$AddressSearchTask;->longitude:D

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->setMarkBallon(DDLjava/lang/String;)V

    goto :goto_0
.end method
