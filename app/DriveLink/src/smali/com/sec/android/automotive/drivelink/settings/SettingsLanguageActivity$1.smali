.class Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$1;
.super Ljava/lang/Object;
.source "SettingsLanguageActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 136
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const-string/jumbo v1, "language"

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->settableLanguages:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsLanguageActivity;->onLanguageChanged()V

    .line 138
    return-void
.end method
