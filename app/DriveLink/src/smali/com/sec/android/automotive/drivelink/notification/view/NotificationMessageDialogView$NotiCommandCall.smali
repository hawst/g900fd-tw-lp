.class Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$NotiCommandCall;
.super Ljava/lang/Object;
.source "NotificationMessageDialogView.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/notification/INotiCommand;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NotiCommandCall"
.end annotation


# instance fields
.field private mDisplayName:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)V
    .locals 0

    .prologue
    .line 670
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$NotiCommandCall;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public action(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 676
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Call Action!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 678
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 679
    .local v6, "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    new-instance v9, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    .line 680
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$NotiCommandCall;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNumber:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->access$4(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)Ljava/lang/String;

    move-result-object v2

    .line 679
    invoke-direct {v9, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;-><init>(ILjava/lang/String;)V

    .line 681
    .local v9, "dlPhoneNumber":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 682
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    const-wide/16 v1, 0x0

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$NotiCommandCall;->mDisplayName:Ljava/lang/String;

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 685
    .local v0, "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-static {v0}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v8

    .line 687
    .local v8, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v7}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 688
    .local v7, "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput-object v8, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 689
    const/4 v1, 0x0

    iput v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    .line 690
    const-string/jumbo v1, "DM_DIAL"

    invoke-static {v1, v7}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 692
    return-void
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 695
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$NotiCommandCall;->mDisplayName:Ljava/lang/String;

    .line 696
    return-void
.end method
