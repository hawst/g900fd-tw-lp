.class Lcom/sec/android/automotive/drivelink/settings/SamsungAccount$1;
.super Ljava/lang/Object;
.source "SamsungAccount.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 10
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 137
    invoke-static {p2}, Lcom/msc/sa/aidl/ISAService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/msc/sa/aidl/ISAService;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$0(Lcom/msc/sa/aidl/ISAService;)V

    .line 138
    new-instance v5, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount$1$1;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount$1$1;-><init>(Lcom/sec/android/automotive/drivelink/settings/SamsungAccount$1;)V

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$12(Lcom/msc/sa/aidl/ISACallback;)V

    .line 213
    :try_start_0
    const-string/jumbo v5, "[Samsung Account]"

    const-string/jumbo v6, "clientId: u0zz5r9gn0"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    const-string/jumbo v5, "[Samsung Account]"

    const-string/jumbo v6, "clientSecret: 53507B3F26D537D106397316DEA2F29C"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    const-string/jumbo v5, "[Samsung Account]"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "packageName: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$8()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mSACallback:Lcom/msc/sa/aidl/ISACallback;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$13()Lcom/msc/sa/aidl/ISACallback;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 217
    const-string/jumbo v5, "[Samsung Account]"

    const-string/jumbo v6, "msaCallback: not Null."

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    :goto_0
    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mISaService:Lcom/msc/sa/aidl/ISAService;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$14()Lcom/msc/sa/aidl/ISAService;

    move-result-object v5

    const-string/jumbo v6, "u0zz5r9gn0"

    .line 221
    const-string/jumbo v7, "53507B3F26D537D106397316DEA2F29C"

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$8()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mSACallback:Lcom/msc/sa/aidl/ISACallback;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$13()Lcom/msc/sa/aidl/ISACallback;

    move-result-object v9

    .line 220
    invoke-interface {v5, v6, v7, v8, v9}, Lcom/msc/sa/aidl/ISAService;->registerCallback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/sa/aidl/ISACallback;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$15(Ljava/lang/String;)V

    .line 223
    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$8()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    .line 225
    .local v4, "manager":Landroid/accounts/AccountManager;
    const-string/jumbo v5, "com.osp.app.signin"

    invoke-virtual {v4, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 227
    .local v0, "accountArr":[Landroid/accounts/Account;
    array-length v5, v0

    if-lez v5, :cond_3

    .line 228
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 229
    .local v2, "data":Landroid/os/Bundle;
    const/16 v5, 0x9

    new-array v1, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "user_id"

    aput-object v6, v1, v5

    const/4 v5, 0x1

    const-string/jumbo v6, "birthday"

    aput-object v6, v1, v5

    const/4 v5, 0x2

    .line 230
    const-string/jumbo v6, "email_id"

    aput-object v6, v1, v5

    const/4 v5, 0x3

    const-string/jumbo v6, "mcc"

    aput-object v6, v1, v5

    const/4 v5, 0x4

    const-string/jumbo v6, "server_url"

    aput-object v6, v1, v5

    const/4 v5, 0x5

    const-string/jumbo v6, "cc"

    aput-object v6, v1, v5

    const/4 v5, 0x6

    .line 231
    const-string/jumbo v6, "api_server_url"

    aput-object v6, v1, v5

    const/4 v5, 0x7

    const-string/jumbo v6, "auth_server_url"

    aput-object v6, v1, v5

    const/16 v5, 0x8

    .line 232
    const-string/jumbo v6, "device_physical_address_text"

    aput-object v6, v1, v5

    .line 233
    .local v1, "additionalData":[Ljava/lang/String;
    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mExpiredAccessToken:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$16()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 234
    const-string/jumbo v5, "expired_access_token"

    .line 235
    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mExpiredAccessToken:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$16()Ljava/lang/String;

    move-result-object v6

    .line 234
    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :cond_0
    const-string/jumbo v5, "additional"

    invoke-virtual {v2, v5, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 238
    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mRegistrationCode:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$17()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 239
    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mISaService:Lcom/msc/sa/aidl/ISAService;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$14()Lcom/msc/sa/aidl/ISAService;

    move-result-object v5

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->requestId:I
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$18()I

    move-result v6

    .line 240
    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mRegistrationCode:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$17()Ljava/lang/String;

    move-result-object v7

    .line 239
    invoke-interface {v5, v6, v7, v2}, Lcom/msc/sa/aidl/ISAService;->requestAccessToken(ILjava/lang/String;Landroid/os/Bundle;)Z

    .line 241
    const-string/jumbo v5, "[Samsung Account]"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "mRegistrationCode: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mRegistrationCode:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$17()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    .end local v0    # "accountArr":[Landroid/accounts/Account;
    .end local v1    # "additionalData":[Ljava/lang/String;
    .end local v2    # "data":Landroid/os/Bundle;
    .end local v4    # "manager":Landroid/accounts/AccountManager;
    :goto_1
    return-void

    .line 219
    :cond_1
    const-string/jumbo v5, "[Samsung Account]"

    const-string/jumbo v6, "msaCallback: Null!!!!!!!!!!!!!!!!!!!!!!!"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 256
    :catch_0
    move-exception v3

    .line 257
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 243
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v0    # "accountArr":[Landroid/accounts/Account;
    .restart local v1    # "additionalData":[Ljava/lang/String;
    .restart local v2    # "data":Landroid/os/Bundle;
    .restart local v4    # "manager":Landroid/accounts/AccountManager;
    :cond_2
    :try_start_1
    const-string/jumbo v5, "[Samsung Account]"

    const-string/jumbo v6, "mRegistrationCode: null"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    # invokes: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->RequestUnregisterCallback()Z
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$7()Z

    goto :goto_1

    .line 248
    .end local v1    # "additionalData":[Ljava/lang/String;
    .end local v2    # "data":Landroid/os/Bundle;
    :cond_3
    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->eventNotifier:Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$6()Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 249
    const-string/jumbo v5, "[Samsung Account]"

    .line 250
    const-string/jumbo v6, "No Samsung Account Registered. Please sign in to Samsung Account"

    .line 249
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->eventNotifier:Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$6()Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;->doNotify(Landroid/os/Bundle;)V

    .line 253
    :cond_4
    # invokes: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->RequestUnregisterCallback()Z
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$7()Z

    .line 254
    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$8()Landroid/content/Context;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->RequestSignInScreen(Landroid/app/Activity;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/ComponentName;

    .prologue
    .line 263
    const-string/jumbo v0, "[Samsung Account]"

    const-string/jumbo v1, "onServiceDisconnected()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$19(Z)V

    .line 265
    return-void
.end method
