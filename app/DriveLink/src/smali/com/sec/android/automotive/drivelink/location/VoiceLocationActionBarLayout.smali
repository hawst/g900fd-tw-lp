.class public Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
.super Landroid/widget/RelativeLayout;
.source "VoiceLocationActionBarLayout.java"

# interfaces
.implements Lcom/nuance/drivelink/DLUiUpdater;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$ISearchListener;,
        Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;,
        Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$SineEaseOut;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I

.field private static final SINE_OUT:Landroid/view/animation/Interpolator;

.field private static TAG:Ljava/lang/String;


# instance fields
.field private attrs:Landroid/util/AttributeSet;

.field blackColor:Z

.field private btnImageBack:Landroid/widget/ImageView;

.field private btnSearchClear:Landroid/view/View;

.field private edtSearch:Landroid/widget/EditText;

.field private isRestore:Z

.field private isRunning:Z

.field private mAnimator:Landroid/view/ViewPropertyAnimator;

.field private mBackBtn:Landroid/widget/LinearLayout;

.field private mBackBtnImage:Landroid/widget/ImageView;

.field private mContext:Landroid/content/Context;

.field private mCorrectTextToSay:Ljava/lang/String;

.field private mCurrentMicState:Lcom/nuance/sample/MicState;

.field private mDirection:Z

.field private mErrorCharSequence:Ljava/lang/CharSequence;

.field private mHiText:Landroid/widget/TextView;

.field private mIsErrorState:Z

.field private mIsMicDisplayed:Z

.field private mIsPhraseSotting:Z

.field private mIsStartQEnded:Z

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mListeningText:Landroid/widget/TextView;

.field private mMicBtn:Landroid/widget/ImageView;

.field private mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

.field private mMicLayout:Landroid/widget/RelativeLayout;

.field private mMicProcessBg:Landroid/widget/ImageView;

.field private mMicStartQBg:Landroid/widget/ImageView;

.field private mNoNetworkLayout:Landroid/widget/LinearLayout;

.field private mNotifiyNewMicState:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;

.field private mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

.field private mOnVoicePhoneActionBarListener:Lcom/sec/android/automotive/drivelink/phone/OnVoicePhoneActionBarListener;

.field private mPhraseSpotterHandler:Landroid/os/Handler;

.field private mProcessText:Landroid/widget/TextView;

.field private mQuoteEnd:Landroid/widget/ImageView;

.field private mQuoteStart:Landroid/widget/ImageView;

.field private mRatio:F

.field private mSayText:Landroid/widget/TextView;

.field private mSearchBtn:Landroid/widget/ImageButton;

.field private mStandByLayout:Landroid/widget/LinearLayout;

.field private mStartQAni:Landroid/view/animation/Animation;

.field private mTTSBar:Landroid/widget/ImageView;

.field private mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

.field private mTTSBarLayout:Landroid/widget/RelativeLayout;

.field private mTTSText:Landroid/widget/TextView;

.field private mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

.field private mTitle:Landroid/widget/TextView;

.field private mTvSearchLocation:Landroid/widget/TextView;

.field private mVoiceLayout:Landroid/widget/RelativeLayout;

.field private rlMic:Landroid/view/View;

.field private rootView:Landroid/view/ViewGroup;

.field private searchLayout:Landroid/widget/FrameLayout;

.field private searchListener:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$ISearchListener;

.field public searchMode:Z

.field private tvSearchResultText:Landroid/widget/TextView;

.field private tvTitleBar:Landroid/widget/TextView;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 53
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 117
    const-string/jumbo v0, "VoiceLocationActionBarLayout"

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->TAG:Ljava/lang/String;

    .line 704
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$SineEaseOut;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$SineEaseOut;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 123
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 58
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsPhraseSotting:Z

    .line 97
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->searchMode:Z

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->attrs:Landroid/util/AttributeSet;

    .line 115
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mCorrectTextToSay:Ljava/lang/String;

    .line 706
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mRatio:F

    .line 708
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isRestore:Z

    .line 771
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mDirection:Z

    .line 772
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isRunning:Z

    .line 1154
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    .line 124
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mContext:Landroid/content/Context;

    .line 125
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->init()V

    .line 126
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 129
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsPhraseSotting:Z

    .line 97
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->searchMode:Z

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->attrs:Landroid/util/AttributeSet;

    .line 115
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mCorrectTextToSay:Ljava/lang/String;

    .line 706
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mRatio:F

    .line 708
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isRestore:Z

    .line 771
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mDirection:Z

    .line 772
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isRunning:Z

    .line 1154
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    .line 130
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mContext:Landroid/content/Context;

    .line 131
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->attrs:Landroid/util/AttributeSet;

    .line 132
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->init()V

    .line 133
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 137
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsPhraseSotting:Z

    .line 97
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->searchMode:Z

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->attrs:Landroid/util/AttributeSet;

    .line 115
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mCorrectTextToSay:Ljava/lang/String;

    .line 706
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mRatio:F

    .line 708
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isRestore:Z

    .line 771
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mDirection:Z

    .line 772
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isRunning:Z

    .line 1154
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    .line 138
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mContext:Landroid/content/Context;

    .line 139
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->attrs:Landroid/util/AttributeSet;

    .line 140
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->init()V

    .line 141
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Z
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsMicDisplayed:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Z
    .locals 1

    .prologue
    .line 708
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isRestore:Z

    return v0
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 708
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isRestore:Z

    return-void
.end method

.method static synthetic access$13()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 704
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;FJLandroid/view/animation/Interpolator;)V
    .locals 0

    .prologue
    .line 710
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->startScaleAnimation(FJLandroid/view/animation/Interpolator;)V

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Z
    .locals 1

    .prologue
    .line 772
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isRunning:Z

    return v0
.end method

.method static synthetic access$16(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Z
    .locals 1

    .prologue
    .line 771
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mDirection:Z

    return v0
.end method

.method static synthetic access$17(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 771
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mDirection:Z

    return-void
.end method

.method static synthetic access$18(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)V
    .locals 0

    .prologue
    .line 775
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->runTTSBarAnimation()V

    return-void
.end method

.method static synthetic access$19(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 772
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isRunning:Z

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSBar:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->edtSearch:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->btnSearchClear:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTvSearchLocation:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Lcom/sec/android/automotive/drivelink/common/view/ListeningView;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsStartQEnded:Z

    return-void
.end method

.method private handleLanguage()V
    .locals 7

    .prologue
    const v6, 0x7f0c0062

    const/4 v5, 0x1

    .line 326
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 328
    .local v0, "config":Landroid/content/res/Configuration;
    if-nez v0, :cond_1

    .line 329
    sget-object v2, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "condig is null"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    :cond_0
    :goto_0
    return-void

    .line 333
    :cond_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    .line 335
    .local v1, "locale":Ljava/util/Locale;
    if-nez v1, :cond_2

    .line 336
    sget-object v2, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "default locale is null"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 340
    :cond_2
    sget-object v2, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "locale: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    iget v2, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v5, :cond_7

    .line 343
    sget-object v2, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "portrait orientation"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    sget-object v2, Ljava/util/Locale;->FRANCE:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Ljava/util/Locale;->GERMANY:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 345
    sget-object v2, Ljava/util/Locale;->ITALY:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 346
    new-instance v2, Ljava/util/Locale;

    const-string/jumbo v3, "pt"

    const-string/jumbo v4, "BR"

    invoke-direct {v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 347
    new-instance v2, Ljava/util/Locale;

    const-string/jumbo v3, "es"

    const-string/jumbo v4, "ES"

    invoke-direct {v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 348
    new-instance v2, Ljava/util/Locale;

    const-string/jumbo v3, "es"

    const-string/jumbo v4, "US"

    invoke-direct {v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 349
    new-instance v2, Ljava/util/Locale;

    const-string/jumbo v3, "es"

    invoke-direct {v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 350
    :cond_3
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_4

    .line 351
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 355
    :cond_4
    new-instance v2, Ljava/util/Locale;

    const-string/jumbo v3, "ru"

    const-string/jumbo v4, "RU"

    invoke-direct {v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 356
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_5

    .line 357
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 360
    :cond_5
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mHiText:Landroid/widget/TextView;

    if-eqz v2, :cond_6

    .line 361
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mHiText:Landroid/widget/TextView;

    const/high16 v3, 0x42200000    # 40.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 365
    :cond_6
    sget-object v2, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 366
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSayText:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 367
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mHiText:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 368
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSayText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3, v6}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 370
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mHiText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mContext:Landroid/content/Context;

    .line 371
    const v4, 0x7f0c006d

    .line 370
    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 372
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 373
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSayText:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 374
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSayText:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 375
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mListeningText:Landroid/widget/TextView;

    const v3, 0x7f0a04dd

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 379
    :cond_7
    sget-object v2, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "landscape orientation"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    sget-object v2, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 382
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSayText:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 383
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mHiText:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 384
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSayText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3, v6}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 386
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mHiText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mContext:Landroid/content/Context;

    .line 387
    const v4, 0x7f0c0069

    .line 386
    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 388
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 389
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSayText:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 390
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSayText:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method private init()V
    .locals 9

    .prologue
    const v8, 0x7f090348

    const v7, 0x7f09033b

    const/4 v6, 0x0

    .line 150
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 151
    .local v0, "inflater":Landroid/view/LayoutInflater;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;->isListening()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsPhraseSotting:Z

    .line 154
    const v2, 0x7f0300d6

    .line 153
    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 156
    const v2, 0x7f090330

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->rootView:Landroid/view/ViewGroup;

    .line 158
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 159
    const v3, 0x7f090346

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 158
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    .line 160
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mContext:Landroid/content/Context;

    const v5, 0x7f0a0843

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 161
    const-string/jumbo v4, ". "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mContext:Landroid/content/Context;

    const v5, 0x7f0a03f4

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 160
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 162
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTitle:Landroid/widget/TextView;

    .line 163
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 164
    const v3, 0x7f090334

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 163
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    .line 165
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 166
    const v3, 0x7f090347

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 165
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mBackBtnImage:Landroid/widget/ImageView;

    .line 168
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 169
    const v3, 0x7f090335

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 168
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    .line 170
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 171
    const v3, 0x7f09031c

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 170
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    .line 174
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f09033a

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSText:Landroid/widget/TextView;

    .line 175
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 176
    invoke-virtual {v2, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 175
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    .line 177
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 178
    const v3, 0x7f090343

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 177
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mListeningText:Landroid/widget/TextView;

    .line 179
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 180
    const v3, 0x7f090344

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 179
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mProcessText:Landroid/widget/TextView;

    .line 181
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 182
    const v3, 0x7f090345

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 181
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    .line 184
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f09033f

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    .line 186
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 187
    const v3, 0x7f09033d

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 186
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    .line 188
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 189
    const v3, 0x7f09033e

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 188
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    .line 191
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f09034a

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->edtSearch:Landroid/widget/EditText;

    .line 192
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 193
    const v3, 0x7f090333

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 192
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->searchLayout:Landroid/widget/FrameLayout;

    .line 194
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 195
    invoke-virtual {v2, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 194
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->tvTitleBar:Landroid/widget/TextView;

    .line 196
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->rlMic:Landroid/view/View;

    .line 197
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 198
    const v3, 0x7f090332

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 197
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->tvSearchResultText:Landroid/widget/TextView;

    .line 199
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 200
    const v3, 0x7f090337

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 199
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mQuoteStart:Landroid/widget/ImageView;

    .line 201
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f090339

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mQuoteEnd:Landroid/widget/ImageView;

    .line 202
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f090336

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSayText:Landroid/widget/TextView;

    .line 203
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f090338

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mHiText:Landroid/widget/TextView;

    .line 204
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f090331

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->btnImageBack:Landroid/widget/ImageView;

    .line 205
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 206
    const v3, 0x7f090340

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    .line 205
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    .line 208
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 209
    const v3, 0x7f090349

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 208
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTvSearchLocation:Landroid/widget/TextView;

    .line 211
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f09034b

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->btnSearchClear:Landroid/view/View;

    .line 213
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mHiText:Landroid/widget/TextView;

    .line 214
    const-string/jumbo v3, "/system/fonts/Cooljazz.ttf"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->createTypefaceFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    .line 213
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 217
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f09033c

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    .line 218
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 219
    const v3, 0x7f090341

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 218
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    .line 220
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 221
    const v3, 0x7f090342

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 220
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSBar:Landroid/widget/ImageView;

    .line 223
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->handleLanguage()V

    .line 225
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->btnSearchClear:Landroid/view/View;

    new-instance v3, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$2;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$2;-><init>(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->edtSearch:Landroid/widget/EditText;

    new-instance v3, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$3;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$3;-><init>(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 259
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mContext:Landroid/content/Context;

    .line 260
    const v3, 0x7f04001e

    .line 259
    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    .line 261
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    new-instance v3, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$4;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$4;-><init>(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)V

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 280
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->isMicDisplayed()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsMicDisplayed:Z

    .line 282
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    new-instance v3, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$5;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$5;-><init>(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 291
    sget-object v2, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "Add OnClickMicListenerImpl in mVoiceLayout."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    new-instance v3, Lcom/nuance/sample/OnClickMicListenerImpl;

    .line 293
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-direct {v3, v4, v5}, Lcom/nuance/sample/OnClickMicListenerImpl;-><init>(Lcom/nuance/sample/MicStateMaster;Landroid/view/View;)V

    .line 292
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 295
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 296
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->attrs:Landroid/util/AttributeSet;

    sget-object v4, Lcom/sec/android/automotive/drivelink/R$styleable;->VoiceLocationActionBarLayout:[I

    .line 295
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 297
    .local v1, "typeArray":Landroid/content/res/TypedArray;
    invoke-virtual {v1, v6, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->blackColor:Z

    .line 301
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->blackColor:Z

    if-eqz v2, :cond_0

    .line 302
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->updateBgColor()V

    .line 304
    :cond_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 306
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setHasInternetLayout(Z)V

    .line 307
    return-void
.end method

.method private runTTSBarAnimation()V
    .locals 5

    .prologue
    const-wide/16 v3, 0x2bc

    .line 776
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    if-nez v0, :cond_0

    .line 777
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSBar:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    .line 778
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$7;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$7;-><init>(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 806
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$8;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$8;-><init>(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)Landroid/view/ViewPropertyAnimator;

    .line 816
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mDirection:Z

    if-eqz v0, :cond_1

    .line 817
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mContext:Landroid/content/Context;

    const/high16 v2, 0x428c0000    # 70.0f

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->dipToPixels(Landroid/content/Context;F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 818
    invoke-virtual {v0, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 822
    :goto_0
    return-void

    .line 820
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method private startScaleAnimation(FJLandroid/view/animation/Interpolator;)V
    .locals 2
    .param p1, "ratio"    # F
    .param p2, "duration"    # J
    .param p4, "interpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 713
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    if-nez v0, :cond_0

    .line 714
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setScaleX(F)V

    .line 715
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setScaleY(F)V

    .line 717
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    .line 718
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$6;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$6;-><init>(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 746
    :cond_0
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mRatio:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 754
    :goto_0
    return-void

    .line 749
    :cond_1
    iput p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mRatio:F

    .line 751
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 752
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 753
    sget-object v1, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method private startTTSBarAnimation()V
    .locals 1

    .prologue
    .line 825
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isRunning:Z

    .line 826
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->runTTSBarAnimation()V

    .line 827
    return-void
.end method

.method private updateBgColor()V
    .locals 3

    .prologue
    .line 961
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->blackColor:Z

    if-eqz v0, :cond_0

    .line 962
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "night mode is setted."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 963
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->rootView:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 964
    const v2, 0x7f02029d

    .line 963
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 965
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 972
    :cond_0
    return-void
.end method


# virtual methods
.method public displayError(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 498
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 500
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 501
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mErrorCharSequence:Ljava/lang/CharSequence;

    .line 503
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsErrorState:Z

    .line 504
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsStartQEnded:Z

    .line 505
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 506
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->stopTTSBarAnimation()V

    .line 507
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 508
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 509
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 510
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 511
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 512
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 513
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 514
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 515
    return-void
.end method

.method public displaySystemTurn(Ljava/lang/CharSequence;)V
    .locals 4
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 525
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "displaySystemTurn mTTSBarLayout visible"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 528
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsStartQEnded:Z

    .line 529
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 530
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 531
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 532
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 533
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 534
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 535
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 536
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 538
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 539
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 540
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 541
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 542
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 544
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 545
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02038c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 547
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->startTTSBarAnimation()V

    .line 552
    return-void
.end method

.method public displayUserTurn(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 521
    return-void
.end method

.method public displayWidgetContent(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 558
    return-void
.end method

.method protected getErrorCharSequence()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 683
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mErrorCharSequence:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 1

    .prologue
    .line 847
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMicState()Lcom/nuance/sample/MicState;
    .locals 1

    .prologue
    .line 670
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    return-object v0
.end method

.method public getSearchText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 900
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->edtSearch:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTextFromEditTextSearch()Ljava/lang/String;
    .locals 1

    .prologue
    .line 873
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->edtSearch:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handleUserCancel()V
    .locals 0

    .prologue
    .line 842
    return-void
.end method

.method public hideLocationLayout()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 144
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 145
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 146
    return-void
.end method

.method protected isErrorState()Z
    .locals 1

    .prologue
    .line 679
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsErrorState:Z

    return v0
.end method

.method public isSearchMode()Z
    .locals 1

    .prologue
    .line 877
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->searchMode:Z

    return v0
.end method

.method protected isTTSState()Z
    .locals 1

    .prologue
    .line 687
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isRunning:Z

    return v0
.end method

.method public onClickable(Z)V
    .locals 1
    .param p1, "isClickable"    # Z

    .prologue
    .line 1136
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 1137
    return-void
.end method

.method public onDisplayMic(Z)V
    .locals 4
    .param p1, "isMicDisplayed"    # Z

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 1172
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsMicDisplayed:Z

    .line 1178
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    if-ne v0, v1, :cond_0

    .line 1179
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsMicDisplayed:Z

    if-eqz v0, :cond_1

    .line 1180
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsErrorState:Z

    .line 1181
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsStartQEnded:Z

    .line 1182
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1183
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 1185
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1186
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1187
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1188
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1189
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1190
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 1192
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 1193
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1194
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1195
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1196
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1197
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02038c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1213
    :cond_0
    :goto_0
    return-void

    .line 1199
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsErrorState:Z

    .line 1200
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsStartQEnded:Z

    .line 1201
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1202
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 1204
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1205
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1206
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1207
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1208
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1209
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1210
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onPhraseSpotterStateChanged(Z)V
    .locals 3
    .param p1, "isSpotting"    # Z

    .prologue
    .line 1141
    const-string/jumbo v0, "UiUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " - PhraseSpotting : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1142
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1141
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1144
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsPhraseSotting:Z

    .line 1152
    return-void
.end method

.method public performCallForClick()V
    .locals 1

    .prologue
    .line 1131
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->callOnClick()Z

    .line 1132
    return-void
.end method

.method public setBlackStyle()V
    .locals 2

    .prologue
    .line 957
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 958
    return-void
.end method

.method public setBtnSearchInvisible()V
    .locals 2

    .prologue
    .line 863
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 864
    return-void
.end method

.method public setBtnSearchVisible()V
    .locals 2

    .prologue
    .line 859
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 860
    return-void
.end method

.method public setContactsHint()V
    .locals 2

    .prologue
    .line 1020
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->edtSearch:Landroid/widget/EditText;

    const v1, 0x7f0a0243

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 1021
    return-void
.end method

.method public setCurrentMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 494
    return-void
.end method

.method protected setDayMode(Z)V
    .locals 6
    .param p1, "darkMode"    # Z

    .prologue
    const v5, 0x7f020044

    const v4, 0x7f02000a

    const v3, 0x7f080031

    .line 1094
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->rootView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 1095
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->rootView:Landroid/view/ViewGroup;

    const v1, 0x7f020001

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 1096
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSayText:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 1097
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSayText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1099
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mHiText:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 1100
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mHiText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080033

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1101
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mQuoteStart:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 1102
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mQuoteStart:Landroid/widget/ImageView;

    const v1, 0x7f02035b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1103
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mQuoteEnd:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 1104
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mQuoteEnd:Landroid/widget/ImageView;

    const v1, 0x7f020359

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1105
    :cond_4
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSText:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    .line 1106
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1108
    :cond_5
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mListeningText:Landroid/widget/TextView;

    if-eqz v0, :cond_6

    .line 1109
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1111
    :cond_6
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mProcessText:Landroid/widget/TextView;

    if-eqz v0, :cond_7

    .line 1112
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1114
    :cond_7
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->btnImageBack:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    .line 1115
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->btnImageBack:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1116
    :cond_8
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_9

    if-nez p1, :cond_9

    .line 1117
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    const v1, 0x7f020013

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1118
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 1120
    :cond_9
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_a

    .line 1121
    if-nez p1, :cond_c

    .line 1122
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 1126
    :cond_a
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mBackBtnImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_b

    .line 1127
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mBackBtnImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1128
    :cond_b
    return-void

    .line 1124
    :cond_c
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    const v1, 0x7f020016

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setHasInternetLayout(Z)V
    .locals 3
    .param p1, "hasInternet"    # Z

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 311
    if-eqz p1, :cond_0

    .line 313
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 323
    :goto_0
    return-void

    .line 318
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 319
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 320
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 321
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public setISearchListener(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$ISearchListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$ISearchListener;

    .prologue
    .line 944
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->searchListener:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$ISearchListener;

    .line 945
    return-void
.end method

.method protected setMicDisplayed(Z)V
    .locals 0
    .param p1, "isMicDisplayed"    # Z

    .prologue
    .line 1216
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsMicDisplayed:Z

    .line 1217
    return-void
.end method

.method protected setNightMode(Z)V
    .locals 5
    .param p1, "darkMode"    # Z

    .prologue
    const v4, 0x7f020045

    const v3, 0x7f02000c

    const v2, 0x7f080030

    .line 1055
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->rootView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 1056
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->rootView:Landroid/view/ViewGroup;

    const v1, 0x7f08004e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 1057
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSayText:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 1058
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSayText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1060
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mHiText:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 1061
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mHiText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1063
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mQuoteStart:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 1064
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mQuoteStart:Landroid/widget/ImageView;

    const v1, 0x7f02035e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1065
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mQuoteEnd:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 1066
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mQuoteEnd:Landroid/widget/ImageView;

    const v1, 0x7f02035d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1067
    :cond_4
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSText:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    .line 1068
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1070
    :cond_5
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mListeningText:Landroid/widget/TextView;

    if-eqz v0, :cond_6

    .line 1071
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1073
    :cond_6
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mProcessText:Landroid/widget/TextView;

    if-eqz v0, :cond_7

    .line 1074
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1076
    :cond_7
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->btnImageBack:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    .line 1077
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->btnImageBack:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1078
    :cond_8
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_9

    if-nez p1, :cond_9

    .line 1079
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    const v1, 0x7f020014

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1080
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    .line 1081
    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 1083
    :cond_9
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_a

    .line 1084
    if-nez p1, :cond_c

    .line 1085
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 1089
    :cond_a
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mBackBtnImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_b

    .line 1090
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mBackBtnImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1091
    :cond_b
    return-void

    .line 1087
    :cond_c
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    const v1, 0x7f020016

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setNotifiyNewMicState(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;)V
    .locals 0
    .param p1, "mNotifiyNewMicState"    # Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;

    .prologue
    .line 1012
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mNotifiyNewMicState:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;

    .line 1013
    return-void
.end method

.method public setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 851
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 852
    return-void
.end method

.method public setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V
    .locals 2
    .param p1, "onEditorActionListener"    # Landroid/widget/TextView$OnEditorActionListener;

    .prologue
    .line 868
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->edtSearch:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 869
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->edtSearch:Landroid/widget/EditText;

    const-string/jumbo v1, "disableEmoticonInput=true;"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 870
    return-void
.end method

.method public setOnMicStateChangeListener(Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .prologue
    .line 675
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .line 676
    return-void
.end method

.method public setOnSearchBtnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 855
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 856
    return-void
.end method

.method public setOnVoicePhoneActionBarListener(Lcom/sec/android/automotive/drivelink/phone/OnVoicePhoneActionBarListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/automotive/drivelink/phone/OnVoicePhoneActionBarListener;

    .prologue
    .line 1231
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mOnVoicePhoneActionBarListener:Lcom/sec/android/automotive/drivelink/phone/OnVoicePhoneActionBarListener;

    .line 1232
    return-void
.end method

.method public setResutlText(I)V
    .locals 5
    .param p1, "size"    # I

    .prologue
    const/4 v4, 0x0

    .line 997
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->tvSearchResultText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 1009
    :goto_0
    return-void

    .line 1000
    :cond_0
    if-lez p1, :cond_1

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->edtSearch:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 1001
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->tvSearchResultText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1002
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->tvSearchResultText:Landroid/widget/TextView;

    .line 1003
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0244

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    .line 1004
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->edtSearch:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    aput-object v4, v2, v3

    .line 1002
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1006
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->tvSearchResultText:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1007
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->tvSearchResultText:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setSayListeningText(I)V
    .locals 1
    .param p1, "titleRes"    # I

    .prologue
    .line 940
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 941
    return-void
.end method

.method public setSayListeningText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 931
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 932
    return-void
.end method

.method public setSayText(I)V
    .locals 1
    .param p1, "titleRes"    # I

    .prologue
    .line 926
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mCorrectTextToSay:Ljava/lang/String;

    .line 927
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 928
    return-void
.end method

.method public setSayText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 916
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mCorrectTextToSay:Ljava/lang/String;

    .line 917
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 918
    return-void
.end method

.method public setSearchText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 904
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->edtSearch:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 905
    return-void
.end method

.method protected setTTSState()V
    .locals 1

    .prologue
    .line 691
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->displaySystemTurn(Ljava/lang/CharSequence;)V

    .line 692
    return-void
.end method

.method public setTitleBar(I)V
    .locals 1
    .param p1, "titleRes"    # I

    .prologue
    .line 908
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->tvTitleBar:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 909
    return-void
.end method

.method public setTitleBar(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 912
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->tvTitleBar:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 913
    return-void
.end method

.method public setVoiceLayoutOnClickListener(I)V
    .locals 5
    .param p1, "value"    # I

    .prologue
    .line 473
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->TAG:Ljava/lang/String;

    .line 474
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setVoiceLayoutOnClickListener a new OnClickMicListenerImpl: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 475
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 474
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 473
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/nuance/sample/OnClickMicListenerImpl;

    .line 477
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 478
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-direct {v1, v2, v3, v4}, Lcom/nuance/sample/OnClickMicListenerImpl;-><init>(Lcom/nuance/sample/MicStateMaster;Ljava/lang/String;Landroid/view/View;)V

    .line 476
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 479
    return-void
.end method

.method public setVoiceLayoutOnClickListener(Ljava/lang/String;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 482
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->TAG:Ljava/lang/String;

    .line 483
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setVoiceLayoutOnClickListener a new OnClickMicListenerImpl: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 484
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 483
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 482
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/nuance/sample/OnClickMicListenerImpl;

    .line 486
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-direct {v1, v2, p1, v3}, Lcom/nuance/sample/OnClickMicListenerImpl;-><init>(Lcom/nuance/sample/MicStateMaster;Ljava/lang/String;Landroid/view/View;)V

    .line 485
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 487
    return-void
.end method

.method public showMic()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 881
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->searchLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 882
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->searchMode:Z

    .line 883
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->btnImageBack:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 884
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 885
    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 884
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 886
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->edtSearch:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 887
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->edtSearch:Landroid/widget/EditText;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 888
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setResutlText(I)V

    .line 889
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 890
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 891
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 892
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 893
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 895
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setBtnSearchVisible()V

    .line 896
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 897
    return-void
.end method

.method public showSearch()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x4

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 397
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 401
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 402
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 404
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTitle:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 405
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 408
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_1

    .line 409
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 410
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 413
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->searchLayout:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_2

    .line 414
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->searchLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 417
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    if-eqz v1, :cond_3

    .line 418
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 422
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_4

    .line 423
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 426
    :cond_4
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->rlMic:Landroid/view/View;

    if-eqz v1, :cond_5

    .line 427
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->rlMic:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 430
    :cond_5
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->btnImageBack:Landroid/widget/ImageView;

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isSearchMode()Z

    move-result v1

    if-nez v1, :cond_6

    .line 431
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->btnImageBack:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 434
    :cond_6
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->edtSearch:Landroid/widget/EditText;

    if-eqz v1, :cond_7

    .line 453
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 454
    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 453
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 456
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-nez v1, :cond_7

    .line 457
    invoke-virtual {v0, v6, v6}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    .line 461
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_7
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->searchMode:Z

    .line 462
    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setResutlText(I)V

    .line 464
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->edtSearch:Landroid/widget/EditText;

    if-eqz v1, :cond_8

    .line 465
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->edtSearch:Landroid/widget/EditText;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 467
    :cond_8
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 469
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 470
    return-void
.end method

.method public showVoiceLayout()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x4

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1024
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1026
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1028
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1029
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1030
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1042
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1043
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1044
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1045
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1046
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mSearchBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1047
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1048
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 1049
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1050
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mBackBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1051
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->searchMode:Z

    .line 1052
    return-void

    .line 1034
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsMicDisplayed:Z

    if-nez v0, :cond_1

    .line 1035
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1036
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1037
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 1039
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected stopTTSBarAnimation()V
    .locals 1

    .prologue
    .line 830
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_0

    .line 831
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSBarAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 833
    :cond_0
    return-void
.end method

.method public updateMicLayouts()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 1220
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1221
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1222
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 1223
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1224
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1225
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1226
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1227
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02038c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1228
    return-void
.end method

.method public updateMicRMSChange(I)V
    .locals 1
    .param p1, "rmsValue"    # I

    .prologue
    .line 699
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsStartQEnded:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    if-eqz v0, :cond_0

    .line 700
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->update(I)V

    .line 702
    :cond_0
    return-void
.end method

.method public updateMicState(Lcom/nuance/sample/MicState;)V
    .locals 7
    .param p1, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const-wide/16 v5, 0x2bc

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 562
    const-string/jumbo v0, "UiUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    .line 565
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    if-nez v0, :cond_3

    .line 566
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 653
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mNotifiyNewMicState:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;

    if-eqz v0, :cond_0

    .line 654
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mNotifiyNewMicState:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$NotifiyNewMicState;->onNewMicState(Lcom/nuance/sample/MicState;)V

    .line 661
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mOnVoicePhoneActionBarListener:Lcom/sec/android/automotive/drivelink/phone/OnVoicePhoneActionBarListener;

    if-eqz v0, :cond_1

    .line 662
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mOnVoicePhoneActionBarListener:Lcom/sec/android/automotive/drivelink/phone/OnVoicePhoneActionBarListener;

    .line 664
    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    .line 662
    invoke-interface {v0, v4, v1}, Lcom/sec/android/automotive/drivelink/phone/OnVoicePhoneActionBarListener;->onVoicePhoneActionBarUpdate(II)V

    .line 666
    :cond_1
    return-void

    .line 568
    :pswitch_0
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsErrorState:Z

    .line 569
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsStartQEnded:Z

    .line 570
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 571
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->stopTTSBarAnimation()V

    .line 572
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 574
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsMicDisplayed:Z

    if-eqz v0, :cond_2

    .line 575
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->updateMicLayouts()V

    .line 598
    :goto_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 599
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 600
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 601
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 603
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 604
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 605
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    goto :goto_0

    .line 588
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 589
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mPhraseSpotterHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    .line 611
    :pswitch_1
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsErrorState:Z

    .line 612
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 613
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->stopTTSBarAnimation()V

    .line 614
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 615
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 616
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 617
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 618
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 619
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 621
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 622
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 623
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 624
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 625
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 626
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020385

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 627
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020388

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 629
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 632
    :pswitch_2
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsErrorState:Z

    .line 633
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsStartQEnded:Z

    .line 634
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->stopTTSBarAnimation()V

    .line 635
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 637
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 638
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 639
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mListeningText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 640
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mProcessText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 641
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTTSBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 643
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 644
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 645
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 646
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 649
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 650
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->play(I)V

    goto/16 :goto_0

    .line 658
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    invoke-interface {v0, p0, p1}, Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;->onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V

    goto/16 :goto_1

    .line 566
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
