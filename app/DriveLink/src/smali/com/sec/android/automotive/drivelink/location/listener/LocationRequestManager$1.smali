.class Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager$1;
.super Ljava/lang/Object;
.source "LocationRequestManager.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager$1;->this$0:Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNotifySmartAlert(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;Ljava/lang/String;)V
    .locals 3
    .param p1, "incidentInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;
    .param p2, "msgErro"    # Ljava/lang/String;

    .prologue
    .line 262
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager$1;->this$0:Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;

    # getter for: Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->access$0(Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v2

    .line 263
    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 267
    return-void

    .line 264
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    .line 265
    .local v1, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    invoke-interface {v1, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;->onNotifySmartAlert(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResponseCreateUserProfile(Z)V
    .locals 4
    .param p1, "result"    # Z

    .prologue
    .line 224
    if-nez p1, :cond_0

    .line 225
    const-string/jumbo v2, "LocationRequestManager"

    const-string/jumbo v3, "Fail to create user profile."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager$1;->this$0:Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mLocationShareManager:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->dismissProgressDialog()V

    .line 228
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager$1;->this$0:Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mLocationShareManager:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    invoke-virtual {v2, p1}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->onResponseCreateUserProfile(Z)V

    .line 230
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager$1;->this$0:Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;

    # getter for: Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->access$0(Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v2

    .line 231
    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 235
    return-void

    .line 232
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    .line 233
    .local v1, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    invoke-interface {v1, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;->onResponseCreateUserProfile(Z)V

    goto :goto_0
.end method

.method public onResponseRemoveGroupShared(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 220
    return-void
.end method

.method public onResponseRequestAcceptLocationShared(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V
    .locals 1
    .param p1, "friend"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager$1;->this$0:Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mLocationShareManager:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->onResponseRequestAcceptLocationShared(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V

    .line 216
    return-void
.end method

.method public onResponseRequestAcceptShareMyLocation(Z)V
    .locals 1
    .param p1, "result"    # Z

    .prologue
    .line 248
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager$1;->this$0:Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mLocationShareManager:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->dismissProgressDialog()V

    .line 249
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager$1;->this$0:Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mLocationShareManager:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    .line 250
    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->onResponseRequestAcceptShareMyLocation(Z)V

    .line 257
    return-void
.end method

.method public onResponseRequestAddFriendsToGroup(ZLjava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 0
    .param p1, "friendAdded"    # Z
    .param p3, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;",
            ")V"
        }
    .end annotation

    .prologue
    .line 204
    .local p2, "friends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    return-void
.end method

.method public onResponseRequestAllParticipantTracking(Ljava/util/HashMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 192
    .local p1, "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;>;"
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager$1;->this$0:Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;

    # getter for: Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->access$0(Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v2

    .line 193
    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 197
    return-void

    .line 194
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    .line 195
    .local v1, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    invoke-interface {v1, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;->onResponseRequestAllParticipantTracking(Ljava/util/HashMap;)V

    goto :goto_0
.end method

.method public onResponseRequestChangeGroupDestination(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 0
    .param p1, "response"    # Z
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 186
    return-void
.end method

.method public onResponseRequestCreateGroupShare(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 0
    .param p1, "groupCreated"    # Z
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 181
    return-void
.end method

.method public onResponseRequestFriendLocationShare(Ljava/lang/Exception;Z)V
    .locals 1
    .param p1, "error"    # Ljava/lang/Exception;
    .param p2, "result"    # Z

    .prologue
    .line 168
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->getInstance()Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    move-result-object v0

    .line 169
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->onResponseRequestFriendLocationShare(Ljava/lang/Exception;Z)V

    .line 176
    return-void
.end method

.method public onResponseRequestGroupSharedUpdateInfo(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 5
    .param p1, "updated"    # Z
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 143
    if-nez p1, :cond_0

    .line 144
    const-string/jumbo v2, "LocationRequestManager"

    .line 145
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Fail to get information from group "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 146
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 145
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 144
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager$1;->this$0:Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;

    # getter for: Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->access$0(Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v2

    .line 149
    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 153
    return-void

    .line 150
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    .line 151
    .local v1, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    invoke-interface {v1, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;->onResponseRequestGroupSharedUpdateInfo(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V

    goto :goto_0
.end method

.method public onResponseRequestQuitFromGroup(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 118
    return-void
.end method

.method public onResponseRequestRecommendedLocationList(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 109
    .local p1, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager$1;->this$0:Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;

    # getter for: Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->access$0(Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v2

    .line 110
    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 114
    return-void

    .line 111
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    .line 112
    .local v1, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    invoke-interface {v1, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;->onResponseRequestRecommendedLocationList(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public onResponseRequestReinviteFriendToGroup(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V
    .locals 0
    .param p1, "reinvited"    # Z
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .param p3, "friend"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    .line 240
    return-void
.end method

.method public onResponseRequestRestartGroupShared(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 0
    .param p1, "response"    # Z
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 103
    return-void
.end method

.method public onResponseRequestSetMyLocation(Z)V
    .locals 3
    .param p1, "locationSetted"    # Z

    .prologue
    .line 72
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager$1;->this$0:Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;

    # getter for: Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->access$0(Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v2

    .line 73
    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 77
    return-void

    .line 74
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    .line 75
    .local v1, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    invoke-interface {v1, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;->onResponseRequestSetMyLocation(Z)V

    goto :goto_0
.end method

.method public onResponseRequestShareMyLocation(Z)V
    .locals 1
    .param p1, "result"    # Z

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager$1;->this$0:Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/listener/LocationRequestManager;->mLocationShareManager:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->onResponseRequestShareMyLocation(Z)V

    .line 88
    return-void
.end method

.method public onResponseRequestUpdateParticipantStatus(Z)V
    .locals 0
    .param p1, "mResult"    # Z

    .prologue
    .line 244
    return-void
.end method
