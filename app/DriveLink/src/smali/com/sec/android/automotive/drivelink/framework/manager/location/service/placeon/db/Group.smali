.class public Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Group;
.super Ljava/lang/Object;
.source "Group.java"


# instance fields
.field private mDuration:I

.field private mGroupId:I

.field private mGroupName:Ljava/lang/String;

.field private mParticipants:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(IILjava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "groupId"    # I
    .param p2, "duration"    # I
    .param p3, "groupName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, "particpants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;>;"
    const/4 v0, -0x1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Group;->mGroupId:I

    .line 7
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Group;->mDuration:I

    .line 8
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Group;->mGroupName:Ljava/lang/String;

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Group;->mParticipants:Ljava/util/ArrayList;

    .line 13
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Group;->mGroupId:I

    .line 14
    iput p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Group;->mDuration:I

    .line 15
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Group;->mGroupName:Ljava/lang/String;

    .line 16
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Group;->mParticipants:Ljava/util/ArrayList;

    .line 17
    return-void
.end method


# virtual methods
.method public getmDuration()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Group;->mDuration:I

    return v0
.end method

.method public getmGroupId()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Group;->mGroupId:I

    return v0
.end method

.method public getmGroupName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Group;->mGroupName:Ljava/lang/String;

    return-object v0
.end method

.method public getmParticipants()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Group;->mParticipants:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setmDuration(I)V
    .locals 0
    .param p1, "mDuration"    # I

    .prologue
    .line 32
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Group;->mDuration:I

    .line 33
    return-void
.end method

.method public setmGroupId(I)V
    .locals 0
    .param p1, "mGroupId"    # I

    .prologue
    .line 24
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Group;->mGroupId:I

    .line 25
    return-void
.end method

.method public setmGroupName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mGroupName"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Group;->mGroupName:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public setmParticipants(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "mParticipants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Participants;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/db/Group;->mParticipants:Ljava/util/ArrayList;

    .line 49
    return-void
.end method
