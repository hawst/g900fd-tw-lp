.class public Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/AlarmDataHandler;
.super Ljava/lang/Object;
.source "AlarmDataHandler.java"


# static fields
.field static final ALARM_ALARMTIME_TIME:Ljava/lang/String; = "createtime"

.field static final ALARM_ALERT_TIME:Ljava/lang/String; = "alerttime"

.field static final ALARM_ID:Ljava/lang/String; = "_id"

.field static final ALARM_NAME:Ljava/lang/String; = "name"

.field static final ALARM_SNZACTIVE:Ljava/lang/String; = "snzactive"

.field private static final mAlarmUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string/jumbo v0, "content://com.samsung.sec.android.clockpackage/alarm"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 15
    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/AlarmDataHandler;->mAlarmUri:Landroid/net/Uri;

    .line 16
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAlarmInfo(Landroid/content/Context;I)Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # I

    .prologue
    .line 19
    new-instance v7, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;

    invoke-direct {v7}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;-><init>()V

    .line 20
    .local v7, "alarmInfo":Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;
    move/from16 v0, p2

    invoke-virtual {v7, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->setAlarmId(I)V

    .line 22
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 24
    .local v1, "contentResolver":Landroid/content/ContentResolver;
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/AlarmDataHandler;->mAlarmUri:Landroid/net/Uri;

    const/4 v3, 0x0

    const-string/jumbo v4, "_id = ?"

    .line 25
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v5, v6

    const/4 v6, 0x0

    .line 24
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 27
    .local v8, "cursor":Landroid/database/Cursor;
    const-string/jumbo v2, "name"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 30
    .local v10, "nameIndex":I
    const-string/jumbo v2, "snzactive"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 32
    .local v12, "snzactiveIndex":I
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 33
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 35
    .local v9, "name":Ljava/lang/String;
    invoke-interface {v8, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 37
    .local v11, "snzactive":I
    invoke-virtual {v7, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->setAlarmName(Ljava/lang/String;)V

    .line 39
    invoke-virtual {v7, v11}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLAlarmInfoImp;->setSNZActive(I)V

    .line 42
    .end local v9    # "name":Ljava/lang/String;
    .end local v11    # "snzactive":I
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 43
    return-object v7
.end method
