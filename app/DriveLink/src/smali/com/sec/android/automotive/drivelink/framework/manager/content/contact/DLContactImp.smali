.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
.source "DLContactImp.java"


# static fields
.field private static final serialVersionUID:J = 0x52aef2ef74434c68L


# instance fields
.field protected mContactId:J

.field protected mDisplayName:Ljava/lang/String;

.field protected mFirstName:Ljava/lang/String;

.field protected mLastName:Ljava/lang/String;

.field protected mPhoneNumberList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;"
        }
    .end annotation
.end field

.field protected mPhoneticName:Ljava/lang/String;

.field protected mPhotoId:J


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    const-wide/16 v1, -0x1

    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;-><init>()V

    .line 17
    iput-wide v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mContactId:J

    .line 18
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mDisplayName:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mFirstName:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mLastName:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    .line 22
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhotoId:J

    .line 23
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneticName:Ljava/lang/String;

    .line 26
    iput-wide v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mContactId:J

    .line 27
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mDisplayName:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mFirstName:Ljava/lang/String;

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mLastName:Ljava/lang/String;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    .line 31
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhotoId:J

    .line 32
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;)V
    .locals 5
    .param p1, "contactId"    # J
    .param p3, "displayName"    # Ljava/lang/String;

    .prologue
    const-wide/16 v3, -0x1

    const/4 v2, 0x0

    .line 59
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;-><init>()V

    .line 17
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mContactId:J

    .line 18
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mDisplayName:Ljava/lang/String;

    .line 19
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mFirstName:Ljava/lang/String;

    .line 20
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mLastName:Ljava/lang/String;

    .line 21
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    .line 22
    iput-wide v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhotoId:J

    .line 23
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneticName:Ljava/lang/String;

    .line 60
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mContactId:J

    .line 61
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mDisplayName:Ljava/lang/String;

    .line 62
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mFirstName:Ljava/lang/String;

    .line 63
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mLastName:Ljava/lang/String;

    .line 64
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    .line 65
    iput-wide v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhotoId:J

    .line 66
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "contactId"    # J
    .param p3, "displayName"    # Ljava/lang/String;
    .param p4, "phoneticName"    # Ljava/lang/String;

    .prologue
    const-wide/16 v3, -0x1

    const/4 v2, 0x0

    .line 68
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;-><init>()V

    .line 17
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mContactId:J

    .line 18
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mDisplayName:Ljava/lang/String;

    .line 19
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mFirstName:Ljava/lang/String;

    .line 20
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mLastName:Ljava/lang/String;

    .line 21
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    .line 22
    iput-wide v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhotoId:J

    .line 23
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneticName:Ljava/lang/String;

    .line 69
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mContactId:J

    .line 70
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mDisplayName:Ljava/lang/String;

    .line 71
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mFirstName:Ljava/lang/String;

    .line 72
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mLastName:Ljava/lang/String;

    .line 73
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    .line 74
    iput-wide v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhotoId:J

    .line 75
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneticName:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 5
    .param p1, "contactId"    # J
    .param p3, "displayName"    # Ljava/lang/String;
    .param p4, "phoneticName"    # Ljava/lang/String;
    .param p5, "firstName"    # Ljava/lang/String;
    .param p6, "lastName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p7, "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    const-wide/16 v3, -0x1

    const/4 v2, 0x0

    .line 88
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;-><init>()V

    .line 17
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mContactId:J

    .line 18
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mDisplayName:Ljava/lang/String;

    .line 19
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mFirstName:Ljava/lang/String;

    .line 20
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mLastName:Ljava/lang/String;

    .line 21
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    .line 22
    iput-wide v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhotoId:J

    .line 23
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneticName:Ljava/lang/String;

    .line 91
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mContactId:J

    .line 92
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mDisplayName:Ljava/lang/String;

    .line 93
    iput-object p5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mFirstName:Ljava/lang/String;

    .line 94
    iput-object p6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mLastName:Ljava/lang/String;

    .line 95
    iput-object p7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    .line 96
    iput-wide v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhotoId:J

    .line 97
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneticName:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 5
    .param p1, "contactId"    # J
    .param p3, "displayName"    # Ljava/lang/String;
    .param p4, "firstName"    # Ljava/lang/String;
    .param p5, "lastName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p6, "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    const-wide/16 v3, -0x1

    const/4 v2, 0x0

    .line 78
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;-><init>()V

    .line 17
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mContactId:J

    .line 18
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mDisplayName:Ljava/lang/String;

    .line 19
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mFirstName:Ljava/lang/String;

    .line 20
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mLastName:Ljava/lang/String;

    .line 21
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    .line 22
    iput-wide v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhotoId:J

    .line 23
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneticName:Ljava/lang/String;

    .line 80
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mContactId:J

    .line 81
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mDisplayName:Ljava/lang/String;

    .line 82
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mFirstName:Ljava/lang/String;

    .line 83
    iput-object p5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mLastName:Ljava/lang/String;

    .line 84
    iput-object p6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    .line 85
    iput-wide v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhotoId:J

    .line 86
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 6
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    const-wide/16 v4, -0x1

    const/4 v3, 0x0

    .line 34
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;-><init>()V

    .line 17
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mContactId:J

    .line 18
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mDisplayName:Ljava/lang/String;

    .line 19
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mFirstName:Ljava/lang/String;

    .line 20
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mLastName:Ljava/lang/String;

    .line 21
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    .line 22
    iput-wide v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhotoId:J

    .line 23
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneticName:Ljava/lang/String;

    .line 35
    iput-wide v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mContactId:J

    .line 36
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mDisplayName:Ljava/lang/String;

    .line 37
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mFirstName:Ljava/lang/String;

    .line 38
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mLastName:Ljava/lang/String;

    .line 39
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    .line 40
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    const/16 v1, 0xc

    invoke-direct {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;-><init>(ILjava/lang/String;)V

    .line 42
    .local v0, "number":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    iput-wide v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhotoId:J

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 6
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .param p2, "isMyPhoneNumber"    # Z

    .prologue
    const-wide/16 v4, -0x1

    const/4 v3, 0x0

    .line 46
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;-><init>()V

    .line 17
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mContactId:J

    .line 18
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mDisplayName:Ljava/lang/String;

    .line 19
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mFirstName:Ljava/lang/String;

    .line 20
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mLastName:Ljava/lang/String;

    .line 21
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    .line 22
    iput-wide v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhotoId:J

    .line 23
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneticName:Ljava/lang/String;

    .line 47
    iput-wide v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mContactId:J

    .line 48
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mDisplayName:Ljava/lang/String;

    .line 49
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mFirstName:Ljava/lang/String;

    .line 50
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mLastName:Ljava/lang/String;

    .line 51
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    .line 52
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    .line 53
    const/16 v1, 0xc

    .line 52
    invoke-direct {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;-><init>(ILjava/lang/String;)V

    .line 54
    .local v0, "number":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;
    invoke-virtual {v0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;->setIsMyPhoneNumber(Z)V

    .line 55
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    iput-wide v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhotoId:J

    .line 57
    return-void
.end method


# virtual methods
.method public addPhoneNumber(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;)V
    .locals 1
    .param p1, "phoneNumber"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 163
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 166
    return-void
.end method

.method public addPhoneNumber(Ljava/lang/String;)V
    .locals 3
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 170
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    const/16 v2, 0xc

    .line 173
    invoke-direct {v1, v2, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;-><init>(ILjava/lang/String;)V

    .line 172
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    return-void
.end method

.method public cloneWithoutPhoneNumber(Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;)Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    .locals 3
    .param p1, "instance"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .prologue
    .line 202
    if-nez p1, :cond_0

    .line 203
    const/4 v0, 0x0

    .line 213
    :goto_0
    return-object v0

    .line 206
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>()V

    .line 207
    .local v0, "newInstance":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->getContactId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->setContactId(J)V

    .line 208
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->setDisplayName(Ljava/lang/String;)V

    .line 209
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->getFirstName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->setFirstName(Ljava/lang/String;)V

    .line 210
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->getLastName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->setLastName(Ljava/lang/String;)V

    .line 211
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->getPhotoId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->setmPhotoId(J)V

    goto :goto_0
.end method

.method public getContactId()J
    .locals 2

    .prologue
    .line 102
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mContactId:J

    return-wide v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getFirstName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mFirstName:Ljava/lang/String;

    return-object v0
.end method

.method public getLastName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mLastName:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumberList()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 128
    .local v2, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 129
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v0, v4, :cond_1

    .line 149
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_0

    .line 150
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    .end local v0    # "i":I
    :cond_0
    return-object v2

    .line 130
    .restart local v0    # "i":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    .line 131
    .local v1, "phoneNumber":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    if-eqz v1, :cond_2

    .line 132
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneType()I

    move-result v3

    .line 134
    .local v3, "type":I
    packed-switch v3, :pswitch_data_0

    .line 129
    .end local v3    # "type":I
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 138
    .restart local v3    # "type":I
    :pswitch_0
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getPhoneticName(Landroid/content/Context;)Ljava/lang/String;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 218
    const/4 v8, 0x0

    .line 220
    .local v8, "phoneticName":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 222
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v4, "phonetic_name"

    aput-object v4, v2, v1

    .line 223
    .local v2, "projection":[Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "(has_phone_number > 0) AND _id = "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 225
    iget-wide v9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mContactId:J

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 223
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 227
    .local v3, "selection":Ljava/lang/String;
    const-string/jumbo v5, "display_name ASC "

    .line 230
    .local v5, "sortOrder":Ljava/lang/String;
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    .line 231
    const/4 v4, 0x0

    .line 229
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 233
    .local v7, "cursor":Landroid/database/Cursor;
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 235
    const-string/jumbo v1, "phonetic_name"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 238
    .local v6, "colPhoneticName":I
    :cond_0
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 239
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 242
    .end local v6    # "colPhoneticName":I
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 244
    return-object v8
.end method

.method public getPhotoId()J
    .locals 2

    .prologue
    .line 189
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhotoId:J

    return-wide v0
.end method

.method public setContactId(J)V
    .locals 0
    .param p1, "contactId"    # J

    .prologue
    .line 197
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mContactId:J

    .line 199
    return-void
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 185
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mDisplayName:Ljava/lang/String;

    .line 186
    return-void
.end method

.method public setFirstName(Ljava/lang/String;)V
    .locals 0
    .param p1, "firstName"    # Ljava/lang/String;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mFirstName:Ljava/lang/String;

    .line 178
    return-void
.end method

.method public setLastName(Ljava/lang/String;)V
    .locals 0
    .param p1, "lastName"    # Ljava/lang/String;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mLastName:Ljava/lang/String;

    .line 182
    return-void
.end method

.method public setPhoneNumberList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 158
    .local p1, "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhoneNumberList:Ljava/util/ArrayList;

    .line 159
    return-void
.end method

.method public setmPhotoId(J)V
    .locals 0
    .param p1, "photoId"    # J

    .prologue
    .line 193
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->mPhotoId:J

    .line 194
    return-void
.end method
