.class public Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;
.super Ljava/lang/Object;
.source "MultiWindowListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MultiWindowListener"

.field public static TOPAREA:I

.field private static volatile _instance:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;


# instance fields
.field mListener:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

.field topAreaHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const v0, 0x15f90

    sput v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->TOPAREA:I

    .line 13
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method public static getInstance()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;
    .locals 2

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->_instance:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    if-nez v0, :cond_1

    .line 24
    const-class v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    monitor-enter v1

    .line 25
    :try_start_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->_instance:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    if-nez v0, :cond_0

    .line 26
    new-instance v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->_instance:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    .line 24
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->_instance:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    return-object v0

    .line 24
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static getListener()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;
    .locals 2

    .prologue
    .line 35
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->_instance:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    if-nez v0, :cond_1

    .line 36
    const-class v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    monitor-enter v1

    .line 37
    :try_start_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->_instance:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->_instance:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    .line 36
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->_instance:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->mListener:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    return-object v0

    .line 36
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public getTopAreaHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->topAreaHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public registerListener(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    .prologue
    .line 55
    const-string/jumbo v0, "MultiWindowListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "registerListener:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->mListener:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    .line 57
    return-void
.end method

.method public setTopAreaHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->topAreaHandler:Landroid/os/Handler;

    .line 48
    return-void
.end method

.method public unregisterListener()V
    .locals 2

    .prologue
    .line 60
    const-string/jumbo v0, "MultiWindowListener"

    const-string/jumbo v1, "unregisterListener"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->mListener:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    .line 62
    return-void
.end method
