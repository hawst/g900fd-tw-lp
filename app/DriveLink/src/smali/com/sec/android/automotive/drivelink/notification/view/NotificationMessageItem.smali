.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;
.super Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
.source "NotificationMessageItem.java"


# instance fields
.field currentMode:I

.field mBitmap:Landroid/graphics/Bitmap;

.field mDlMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

.field mMessage:Ljava/lang/String;

.field mMsgType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

.field mName:Ljava/lang/String;

.field mNumber:Ljava/lang/String;

.field mTime:J


# direct methods
.method public constructor <init>(IILjava/lang/Object;Landroid/content/Context;)V
    .locals 3
    .param p1, "notiType"    # I
    .param p2, "priority"    # I
    .param p3, "notiInfo"    # Ljava/lang/Object;
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 24
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;-><init>(IILjava/lang/Object;Landroid/content/Context;)V

    .line 13
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->currentMode:I

    .line 15
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mName:Ljava/lang/String;

    .line 16
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mNumber:Ljava/lang/String;

    .line 17
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mMessage:Ljava/lang/String;

    .line 18
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mTime:J

    .line 19
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 20
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mDlMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    move-object v0, p3

    .line 26
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mName:Ljava/lang/String;

    move-object v0, p3

    .line 27
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mMessage:Ljava/lang/String;

    move-object v0, p3

    .line 28
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getMSGType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mMsgType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    move-object v0, p3

    .line 29
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mNumber:Ljava/lang/String;

    move-object v0, p3

    .line 30
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mTime:J

    move-object v0, p3

    .line 31
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getImage()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 32
    check-cast p3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    .end local p3    # "notiInfo":Ljava/lang/Object;
    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getDLMessage()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mDlMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .line 34
    return-void
.end method


# virtual methods
.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getCurrentMode()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->currentMode:I

    return v0
.end method

.method public getDlMessage()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mDlMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getMsgType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mMsgType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getNotiInfo()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 97
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mTime:J

    return-wide v0
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "mBitmep"    # Landroid/graphics/Bitmap;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 110
    return-void
.end method

.method public setCurrentMode(I)V
    .locals 0
    .param p1, "currentMode"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->currentMode:I

    .line 62
    return-void
.end method

.method public setDlMessage(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)V
    .locals 0
    .param p1, "mDlMessage"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mDlMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .line 118
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "mMessage"    # Ljava/lang/String;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mMessage:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public setMsgType(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;)V
    .locals 0
    .param p1, "mMsgType"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mMsgType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    .line 70
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mName"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mName:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setNotiInfo(Ljava/lang/Object;)V
    .locals 2
    .param p1, "notiInfo"    # Ljava/lang/Object;

    .prologue
    .line 45
    move-object v0, p1

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mName:Ljava/lang/String;

    move-object v0, p1

    .line 46
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mMessage:Ljava/lang/String;

    move-object v0, p1

    .line 47
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getMSGType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mMsgType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    move-object v0, p1

    .line 48
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mNumber:Ljava/lang/String;

    move-object v0, p1

    .line 49
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mTime:J

    move-object v0, p1

    .line 50
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getImage()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mBitmap:Landroid/graphics/Bitmap;

    move-object v0, p1

    .line 51
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;->getDLMessage()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mDlMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .line 52
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->setNotiInfo(Ljava/lang/Object;)V

    .line 54
    return-void
.end method

.method public setNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "mNumber"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mNumber:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public setTime(J)V
    .locals 0
    .param p1, "mTime"    # J

    .prologue
    .line 101
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->mTime:J

    .line 102
    return-void
.end method
