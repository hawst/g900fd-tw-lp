.class public Lcom/sec/android/automotive/drivelink/framework/db/location/RecentlyLocationSearchedTableHandler;
.super Ljava/lang/Object;
.source "RecentlyLocationSearchedTableHandler.java"


# static fields
.field private static final TABLE_LOCATION_RECENTLY_SEARCHED:Ljava/lang/String; = "tb_location_recently_searched"

.field private static final TAG:Ljava/lang/String; = "[NewLocationRecentlySearchedTableHandler]"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method private recentlySearchExists(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 139
    const-string/jumbo v3, "[NewLocationRecentlySearchedTableHandler]"

    const-string/jumbo v4, "recentlySearchExists"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    if-nez p2, :cond_1

    .line 142
    const/4 v1, 0x0

    .line 157
    :cond_0
    :goto_0
    return v1

    .line 143
    :cond_1
    const/4 v1, 0x0

    .line 144
    .local v1, "result":Z
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "SELECT * FROM tb_location_recently_searched INNER JOIN tb_locations ON tb_locations.location_id = tb_location_recently_searched.location_id WHERE tb_location_recently_searched.location_id = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 148
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 144
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 149
    .local v2, "sql":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 150
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 151
    const/4 v1, 0x1

    .line 154
    :cond_2
    if-eqz v0, :cond_0

    .line 155
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method


# virtual methods
.method public createRecentlySearchedTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 25
    const-string/jumbo v2, "[NewLocationRecentlySearchedTableHandler]"

    const-string/jumbo v3, "createRecentlySearchedTable"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    :try_start_0
    const-string/jumbo v1, "CREATE DATABASE IF NOT EXISTS tb_location_recently_searched (location_id VARCHAR, timestamp LONG, FOREIGN KEY (location_id) REFERENCES tb_locations (location_id)));"

    .line 34
    .local v1, "sql":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    .end local v1    # "sql":Ljava/lang/String;
    :goto_0
    return-void

    .line 35
    :catch_0
    move-exception v0

    .line 36
    .local v0, "e":Landroid/database/SQLException;
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    goto :goto_0
.end method

.method public deleteRecentlySearchedTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 41
    const-string/jumbo v2, "[NewLocationRecentlySearchedTableHandler]"

    const-string/jumbo v3, "deleteRecentlySearchedTable"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    :try_start_0
    const-string/jumbo v1, "DROP TABLE IF EXISTS tb_location_recently_searched;"

    .line 48
    .local v1, "sql":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    .end local v1    # "sql":Ljava/lang/String;
    :goto_0
    return-void

    .line 49
    :catch_0
    move-exception v0

    .line 50
    .local v0, "e":Landroid/database/SQLException;
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    goto :goto_0
.end method

.method public loadLocationSearchedList(Landroid/database/sqlite/SQLiteDatabase;IJ)Ljava/util/ArrayList;
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "maxCount"    # I
    .param p3, "fromTimeStamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "IJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    const-string/jumbo v5, "[NewLocationRecentlySearchedTableHandler]"

    const-string/jumbo v6, "loadLocationSearchedList"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 99
    .local v2, "searchedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "SELECT tb_locations.location_id, tb_locations.latitude, tb_locations.longitude, tb_locations.address, tb_locations.count FROM tb_location_recently_searched INNER JOIN tb_locations ON tb_locations.location_id = tb_location_recently_searched.location_id WHERE tb_location_recently_searched.timestamp > "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 106
    invoke-virtual {v5, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " LIMIT "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 99
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 108
    .local v4, "sql":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 110
    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_1

    .line 135
    :cond_0
    :goto_0
    return-object v2

    .line 113
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-nez v5, :cond_2

    .line 114
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 119
    :cond_2
    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationSearched;

    invoke-direct {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationSearched;-><init>()V

    .line 121
    .local v3, "searchedLocation":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationSearched;
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;-><init>()V

    .line 122
    .local v1, "location":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;
    const/4 v5, 0x0

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLocationID(Ljava/lang/String;)V

    .line 123
    const/4 v5, 0x1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v5

    invoke-virtual {v1, v5, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLatitude(D)V

    .line 124
    const/4 v5, 0x2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v5

    invoke-virtual {v1, v5, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLongitude(D)V

    .line 125
    const/4 v5, 0x3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLocationAddress(Ljava/lang/String;)V

    .line 126
    const/4 v5, 0x4

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setCount(I)V

    .line 128
    invoke-virtual {v3, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationSearched;->setLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 129
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 132
    if-eqz v0, :cond_0

    .line 133
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public saveRecentlySearched(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 10
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    const/4 v5, 0x0

    .line 55
    const-string/jumbo v6, "[NewLocationRecentlySearchedTableHandler]"

    const-string/jumbo v7, "saveRecentlySearched"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    if-nez p2, :cond_0

    .line 75
    :goto_0
    return v5

    .line 59
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/RecentlyLocationSearchedTableHandler;->recentlySearchExists(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v6

    if-nez v6, :cond_2

    move-object v0, p2

    .line 60
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    .line 61
    .local v0, "mLocation":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 62
    .local v1, "time":Ljava/util/Calendar;
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 63
    .local v2, "timestamp":J
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 64
    .local v4, "values":Landroid/content/ContentValues;
    const-string/jumbo v6, "location_id"

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->getLocationID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string/jumbo v6, "timestamp"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 66
    const-string/jumbo v6, "[NewLocationRecentlySearchedTableHandler]"

    const-string/jumbo v7, "Saving recently searched locations ... "

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    const-string/jumbo v6, "tb_location_recently_searched"

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-lez v6, :cond_1

    .line 68
    const-string/jumbo v5, "[NewLocationRecentlySearchedTableHandler]"

    const-string/jumbo v6, "Saved with success ... "

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    const/4 v5, 0x1

    goto :goto_0

    .line 71
    :cond_1
    const-string/jumbo v6, "[NewLocationRecentlySearchedTableHandler]"

    const-string/jumbo v7, "Fail when try to save ... "

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 75
    .end local v0    # "mLocation":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;
    .end local v1    # "time":Ljava/util/Calendar;
    .end local v2    # "timestamp":J
    .end local v4    # "values":Landroid/content/ContentValues;
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/RecentlyLocationSearchedTableHandler;->updateRecentlySearched(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v5

    goto :goto_0
.end method

.method public updateRecentlySearched(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    const/4 v2, 0x0

    .line 80
    const-string/jumbo v3, "[NewLocationRecentlySearchedTableHandler]"

    const-string/jumbo v4, "updateRecentlySearched"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    if-nez p2, :cond_1

    .line 91
    :cond_0
    :goto_0
    return v2

    :cond_1
    move-object v0, p2

    .line 84
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    .line 85
    .local v0, "nLocation":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 86
    .local v1, "values":Landroid/content/ContentValues;
    const-string/jumbo v3, "timestamp"

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 87
    const-string/jumbo v3, "tb_location_recently_searched"

    .line 88
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "location_id = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->getLocationID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 87
    invoke-virtual {p1, v3, v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_0

    .line 89
    const/4 v2, 0x1

    goto :goto_0
.end method
