.class Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$3;
.super Ljava/lang/Object;
.source "MusicPlayerManager.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->initialize(Landroid/content/Context;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 124
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 126
    .local v0, "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDriveLinkMusicPlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v1

    .line 127
    invoke-interface {v1, p1, p2, p3, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;->onMusicPlayError(Landroid/media/MediaPlayer;IILjava/lang/Object;)V

    .line 130
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$4(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->reset()V

    .line 131
    const/4 v1, 0x0

    return v1
.end method
