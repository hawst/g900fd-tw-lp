.class public interface abstract Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
.super Ljava/lang/Object;
.source "DriveLinkServiceInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;,
        Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;,
        Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;,
        Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;,
        Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDLCallMessageLogListener;,
        Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;,
        Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;,
        Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;,
        Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;,
        Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;,
        Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;,
        Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;,
        Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;,
        Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;,
        Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;,
        Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;,
        Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkScheduleListener;,
        Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;
    }
.end annotation


# virtual methods
.method public abstract addDLCallMessageLogData(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)V
.end method

.method public abstract addFavoriteContact(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Z
.end method

.method public abstract clearNewMissedCallFromCallLog(Landroid/content/Context;)V
.end method

.method public abstract dbGetGroupInfo()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
.end method

.method public abstract dbGetLocation(DD)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
.end method

.method public abstract getAllMusicList(Landroid/content/Context;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAllMusicSearchList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getBTPairingType(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;
.end method

.method public abstract getBluetoothCommunicatingState(Landroid/content/Context;)Z
.end method

.method public abstract getBluetoothConnectedState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
.end method

.method public abstract getBluetoothConnectedState(Landroid/content/Context;Ljava/lang/String;)Z
.end method

.method public abstract getBluetoothConnectionState(Landroid/content/Context;)Z
.end method

.method public abstract getBluetoothDevicePairedState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
.end method

.method public abstract getBluetoothDevicesDiscovered(Landroid/content/Context;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getBluetoothProfileConnectionState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
.end method

.method public abstract getCallLogImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)Landroid/graphics/Bitmap;
.end method

.method public abstract getCarSpeedStatus(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
.end method

.method public abstract getContactFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
.end method

.method public abstract getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;
.end method

.method public abstract getContactImageFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
.end method

.method public abstract getDevice(Landroid/content/Context;)Landroid/bluetooth/BluetoothDevice;
.end method

.method public abstract getDeviceName(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public abstract getLocation(Landroid/content/Context;)Landroid/location/Location;
.end method

.method public abstract getLocation(DD)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
.end method

.method public abstract getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)Landroid/graphics/Bitmap;
.end method

.method public abstract getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)Landroid/graphics/Bitmap;
.end method

.method public abstract getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;)Landroid/graphics/Bitmap;
.end method

.method public abstract getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Landroid/graphics/Bitmap;
.end method

.method public abstract getMusicCount(Landroid/content/Context;)I
.end method

.method public abstract getMusicPlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
.end method

.method public abstract getMusicPlaylistManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;
.end method

.method public abstract getMusicRemotePlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;
.end method

.method public abstract getNewMissedCallCount(Landroid/content/Context;)I
.end method

.method public abstract getPassKey(Landroid/content/Context;)I
.end method

.method public abstract getPhoneNumberList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUserProfile()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
.end method

.method public abstract initialize(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;)Z
.end method

.method public abstract insertNavigation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract isInitialized()Z
.end method

.method public abstract isMirrorLinkRunning()Z
.end method

.method public abstract registerMusicRemoteControl(Landroid/content/Context;)V
.end method

.method public abstract registerNotification(Landroid/content/Context;ZZZZZ)V
.end method

.method public abstract removeFavoriteContact(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
.end method

.method public abstract requestAcceptLocationShared(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract requestAcceptShareMyLocation(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)V
.end method

.method public abstract requestAddFriendsToGroup(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;",
            ")V"
        }
    .end annotation
.end method

.method public abstract requestAlbumList(Landroid/content/Context;)V
.end method

.method public abstract requestAllParticipantTracking(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
.end method

.method public abstract requestArtistList(Landroid/content/Context;)V
.end method

.method public abstract requestBluetoothConnectDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)V
.end method

.method public abstract requestBluetoothDisconnectDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)V
.end method

.method public abstract requestBluetoothMakeDeviceDiscoverable(Landroid/content/Context;)V
.end method

.method public abstract requestBluetoothPairDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)V
.end method

.method public abstract requestBluetoothPairingNSSPAnswer(Landroid/content/Context;Ljava/lang/String;[B)V
.end method

.method public abstract requestBluetoothPairingSSPAnswer(Landroid/content/Context;Ljava/lang/String;Z)V
.end method

.method public abstract requestBluetoothSearchDiscoverableDevices(Landroid/content/Context;)V
.end method

.method public abstract requestBluetoothSearchPairedDevices(Landroid/content/Context;)V
.end method

.method public abstract requestBluetoothStartAdapter(Landroid/content/Context;)V
.end method

.method public abstract requestBluetoothStopAdapter(Landroid/content/Context;)V
.end method

.method public abstract requestBluetoothStopDiscoveringDevices(Landroid/content/Context;)V
.end method

.method public abstract requestBluetoothUnpairDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)V
.end method

.method public abstract requestCallLogList(Landroid/content/Context;I)V
.end method

.method public abstract requestCancelUpdateApplication(Landroid/content/Context;)V
.end method

.method public abstract requestCancelUpdateVersionCheck(Landroid/content/Context;)V
.end method

.method public abstract requestChangeGroupDestination(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
.end method

.method public abstract requestChangeMessageStatusToRead(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)V
.end method

.method public abstract requestCheckUpdateVersion(Landroid/content/Context;)V
.end method

.method public abstract requestContactList(Landroid/content/Context;I)V
.end method

.method public abstract requestCreateGroupShare(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)V
.end method

.method public abstract requestCreateUserProfile(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)V
.end method

.method public abstract requestDLCallMessageLogList(Landroid/content/Context;Ljava/util/Date;I)V
.end method

.method public abstract requestFavoriteContactList(Landroid/content/Context;)V
.end method

.method public abstract requestFavoriteContactList(Landroid/content/Context;Z)V
.end method

.method public abstract requestFriendLocationShare(Landroid/content/Context;Ljava/lang/String;)V
.end method

.method public abstract requestGroupSharedUpdateInfo(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
.end method

.method public abstract requestInboxList(Landroid/content/Context;IZI)V
.end method

.method public abstract requestIncommingMessageList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;I)V
.end method

.method public abstract requestMapAvailableByCountryAndSaleCode(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract requestMusicFolderList(Landroid/content/Context;)V
.end method

.method public abstract requestMusicList(Landroid/content/Context;)V
.end method

.method public abstract requestMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)V
.end method

.method public abstract requestMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)V
.end method

.method public abstract requestMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;)V
.end method

.method public abstract requestMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
.end method

.method public abstract requestMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;)V
.end method

.method public abstract requestMusicListFromSearchResult(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;I)V
.end method

.method public abstract requestPlaylistList(Landroid/content/Context;)V
.end method

.method public abstract requestQuitFromGroup(I)V
.end method

.method public abstract requestRecommendedContactListForCall(Landroid/content/Context;I)V
.end method

.method public abstract requestRecommendedContactListForCall(Landroid/content/Context;IZ)V
.end method

.method public abstract requestRecommendedContactListForMessage(Landroid/content/Context;I)V
.end method

.method public abstract requestRecommendedContactListForMessage(Landroid/content/Context;IZ)V
.end method

.method public abstract requestRecommendedLocationList(Landroid/content/Context;I)V
.end method

.method public abstract requestReinviteFriendToGroup(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V
.end method

.method public abstract requestRemoveGroupShared(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
.end method

.method public abstract requestRestartGroupShared(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
.end method

.method public abstract requestSamsungAccountAccessToken(Landroid/content/Context;Landroid/os/Bundle;)V
.end method

.method public abstract requestSamsungAccountAuthCode(Landroid/content/Context;Landroid/os/Bundle;)V
.end method

.method public abstract requestSamsungAccountChecklistValidation(Landroid/content/Context;Landroid/os/Bundle;)V
.end method

.method public abstract requestSamsungAccountConnectService(Landroid/content/Context;)V
.end method

.method public abstract requestSamsungAccountDisclaimerAgreement(Landroid/content/Context;Landroid/os/Bundle;)V
.end method

.method public abstract requestSamsungAccountDisconnectService(Landroid/content/Context;)V
.end method

.method public abstract requestSamsungAccountRegisterCallback(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract requestSamsungAccountSCloudAccessToken(Landroid/content/Context;Landroid/os/Bundle;)V
.end method

.method public abstract requestSamsungAccountUnregisterCallback(Landroid/content/Context;)V
.end method

.method public abstract requestSamsungAccountUserName(Landroid/content/Context;Landroid/os/Bundle;)V
.end method

.method public abstract requestScheduleList(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;)V
.end method

.method public abstract requestSearchAllMusic(Landroid/content/Context;Ljava/lang/String;)V
.end method

.method public abstract requestSearchMusic(Landroid/content/Context;Ljava/lang/String;)V
.end method

.method public abstract requestSearchedContactList(Landroid/content/Context;Ljava/lang/String;)V
.end method

.method public abstract requestSearchedContactList(Landroid/content/Context;Ljava/lang/String;I)V
.end method

.method public abstract requestSendMessage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)V
.end method

.method public abstract requestSetMyLocation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
.end method

.method public abstract requestShareMyLocation(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
.end method

.method public abstract requestStartDrivingStatusMonitoring(Landroid/content/Context;)V
.end method

.method public abstract requestStartMirrorLink(Landroid/content/Context;)V
.end method

.method public abstract requestStopDrivingStatusMonitoring(Landroid/content/Context;)V
.end method

.method public abstract requestStopMirrorLink(Landroid/content/Context;)V
.end method

.method public abstract requestUnreadMessageCount(Landroid/content/Context;)V
.end method

.method public abstract requestUnreadMessageCountByInbox(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;)V
.end method

.method public abstract requestUnreadMessageCountBySync(Landroid/content/Context;)I
.end method

.method public abstract requestUpdateApplication(Landroid/content/Context;Z)V
.end method

.method public abstract requestUpdateParticipantStatus(Landroid/content/Context;ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)V
.end method

.method public abstract saveLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
.end method

.method public abstract saveMyPlace(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;)V
.end method

.method public abstract setCarSpeedThreshold(Landroid/content/Context;III)V
.end method

.method public abstract setOnDriveLinkCallLogListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;)V
.end method

.method public abstract setOnDriveLinkConnectivityListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;)V
.end method

.method public abstract setOnDriveLinkContactListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;)V
.end method

.method public abstract setOnDriveLinkDLCallMessageLogListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDLCallMessageLogListener;)V
.end method

.method public abstract setOnDriveLinkDrivingStatusListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkDrivingStatusListener;)V
.end method

.method public abstract setOnDriveLinkEventListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;)V
.end method

.method public abstract setOnDriveLinkFavoriteContactListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;)V
.end method

.method public abstract setOnDriveLinkLocationListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V
.end method

.method public abstract setOnDriveLinkMessageListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;)V
.end method

.method public abstract setOnDriveLinkMusicListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;)V
.end method

.method public abstract setOnDriveLinkMusicPlayerListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;)V
.end method

.method public abstract setOnDriveLinkMusicRemotePlayerListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;)V
.end method

.method public abstract setOnDriveLinkNotificationListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;)V
.end method

.method public abstract setOnDriveLinkRecommendedContactForCallListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;)V
.end method

.method public abstract setOnDriveLinkRecommendedContactForMessageListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;)V
.end method

.method public abstract setOnDriveLinkSamsungAccountListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;)V
.end method

.method public abstract setOnDriveLinkScheduleListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkScheduleListener;)V
.end method

.method public abstract setOnDriveLinkUpdateListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;)V
.end method

.method public abstract terminate(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;)V
.end method

.method public abstract unregisterMusicRemoteControl(Landroid/content/Context;)V
.end method

.method public abstract unregisterNotification(Landroid/content/Context;)V
.end method

.method public abstract updatePlayedMusicMetaData(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Z
.end method
