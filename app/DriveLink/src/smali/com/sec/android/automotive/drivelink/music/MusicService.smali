.class public Lcom/sec/android/automotive/drivelink/music/MusicService;
.super Landroid/app/Service;
.source "MusicService.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;
.implements Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;
.implements Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;,
        Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;,
        Lcom/sec/android/automotive/drivelink/music/MusicService$MyBinder;
    }
.end annotation


# static fields
.field public static final ERR_FILE_NOT_FOUND_MEDIA_ERROR:I = -0x26

.field public static final ERR_LOCAL_MUSIC_FILE_ZERO:I = 0x15f90

.field public static final ERR_PRIVATE_MODE_OFF:I = 0x64

.field private static final FADEDOWN:I = 0x5

.field private static final FADEUP:I = 0x6

.field private static final FFORWARD:I = 0xc350

.field private static final PREV_TOLERANCE:I = 0xbb8

.field private static final REWIND:I = 0xc351

.field private static final SUB:Ljava/lang/String; = "MUSICService"

.field private static final TAG:Ljava/lang/String; = "[CarMode]"

.field public static final VOICE_CMD_VOLUME_DOWN:I = 0xea61

.field public static final VOICE_CMD_VOLUME_MUTE:I = 0xea62

.field public static final VOICE_CMD_VOLUME_UNMUTE:I = 0xea63

.field public static final VOICE_CMD_VOLUME_UP:I = 0xea60

.field private static seekBackwardTime:I

.field private static seekForwardTime:I


# instance fields
.field b_isListenerOn:Z

.field private final binder:Landroid/os/IBinder;

.field private isStarted:Z

.field private mAFHighPriority:Z

.field private mAFMiddlePriority:Z

.field private mAFMiddlePriorityPrevPlaying:Z

.field private mAFPrevPlaying:Z

.field mAudioManager:Landroid/media/AudioManager;

.field mBTFW_Listener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

.field private mCallingState:Z

.field private mContext:Landroid/content/Context;

.field public mCurrentVolume:F

.field mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private mDefaultAlbumArt:Landroid/graphics/Bitmap;

.field private mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

.field private mFRAttach:[I

.field private mFRCount:I

.field mFW_Listener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

.field private mIsAudioFocusLoss:Z

.field private mIsAudioStateValue:I

.field private mIsMusicSearchResultExist:Z

.field private mIsPausedFromAudioFocusLoss:Z

.field private mListenrs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/music/MusicListener;",
            ">;"
        }
    .end annotation
.end field

.field private mMediaplayerHandler:Landroid/os/Handler;

.field private mMultiWindowCallPlayer:Z

.field private mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

.field public mMusicDelaycount:I

.field private mMusicList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation
.end field

.field private mMusicSetValue:Z

.field private mNotBTControlPriority:Z

.field public mNotifyMusicChange:Z

.field private mOnErrorPrevPlay:Z

.field private mRemoteController:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

.field private final mRemoteListener:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;

.field private mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

.field private mStreamVolumeValue:I

.field private mUserPausePriority:Z

.field private mbForegroundService:Z

.field private mbInitialized:Z

.field private mbIsFirstMusicPlay:Z

.field phoneListener:Landroid/telephony/PhoneStateListener;

.field public playByVoice:Z

.field telephoneMgr:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/16 v0, 0x7d0

    .line 54
    sput v0, Lcom/sec/android/automotive/drivelink/music/MusicService;->seekForwardTime:I

    .line 55
    sput v0, Lcom/sec/android/automotive/drivelink/music/MusicService;->seekBackwardTime:I

    .line 73
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 56
    iput v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mFRCount:I

    .line 57
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mFRAttach:[I

    .line 75
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    .line 76
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mListenrs:Ljava/util/ArrayList;

    .line 84
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCallingState:Z

    .line 85
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->telephoneMgr:Landroid/telephony/TelephonyManager;

    .line 87
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mOnErrorPrevPlay:Z

    .line 94
    iput v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioStateValue:I

    .line 98
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFPrevPlaying:Z

    .line 102
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFMiddlePriority:Z

    .line 107
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFHighPriority:Z

    .line 111
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFMiddlePriorityPrevPlaying:Z

    .line 115
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mUserPausePriority:Z

    .line 119
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mNotBTControlPriority:Z

    .line 128
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicSetValue:Z

    .line 130
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    .line 131
    iput v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicDelaycount:I

    .line 133
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMultiWindowCallPlayer:Z

    .line 134
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->playByVoice:Z

    .line 135
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mNotifyMusicChange:Z

    .line 137
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mContext:Landroid/content/Context;

    .line 138
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/MusicService$MyBinder;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/MusicService$MyBinder;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->binder:Landroid/os/IBinder;

    .line 140
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 141
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicList:Ljava/util/ArrayList;

    .line 142
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mbForegroundService:Z

    .line 143
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mbInitialized:Z

    .line 144
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mbIsFirstMusicPlay:Z

    .line 145
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioFocusLoss:Z

    .line 147
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsPausedFromAudioFocusLoss:Z

    .line 150
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsMusicSearchResultExist:Z

    .line 152
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mDefaultAlbumArt:Landroid/graphics/Bitmap;

    .line 156
    iput v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mStreamVolumeValue:I

    .line 1090
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/MusicService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/MusicService$1;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mFW_Listener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    .line 1207
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/MusicService$2;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mBTFW_Listener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    .line 1494
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/MusicService$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/MusicService$3;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->phoneListener:Landroid/telephony/PhoneStateListener;

    .line 1765
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/MusicService$4;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/MusicService$4;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteListener:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;

    .line 2039
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/MusicService$5;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMediaplayerHandler:Landroid/os/Handler;

    .line 48
    return-void

    .line 57
    :array_0
    .array-data 4
        0x7d0
        0x7d0
        0xfa0
        0xfa0
        0x1f40
        0x1f40
        0x3e80
    .end array-data
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/music/MusicService;)Z
    .locals 1

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mbIsFirstMusicPlay:Z

    return v0
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/music/MusicService;)V
    .locals 0

    .prologue
    .line 381
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTSeek()V

    return-void
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/music/MusicService;)V
    .locals 0

    .prologue
    .line 375
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTClicked()V

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/music/MusicService;)Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mNotBTControlPriority:Z

    return v0
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/music/MusicService;)V
    .locals 0

    .prologue
    .line 354
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTCallingState()V

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/music/MusicService;)V
    .locals 0

    .prologue
    .line 361
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTPlay()V

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/automotive/drivelink/music/MusicService;Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    return-void
.end method

.method static synthetic access$18(Lcom/sec/android/automotive/drivelink/music/MusicService;)V
    .locals 0

    .prologue
    .line 2104
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setFFREThreadWithStop()V

    return-void
.end method

.method static synthetic access$19(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V
    .locals 0

    .prologue
    .line 387
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyShuffle(Z)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V
    .locals 0

    .prologue
    .line 320
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyMusicChanged(Z)V

    return-void
.end method

.method static synthetic access$20(Lcom/sec/android/automotive/drivelink/music/MusicService;)Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCallingState:Z

    return v0
.end method

.method static synthetic access$21(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V
    .locals 0

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCallingState:Z

    return-void
.end method

.method static synthetic access$22(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteController:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    return-object v0
.end method

.method static synthetic access$23(Lcom/sec/android/automotive/drivelink/music/MusicService;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 2039
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMediaplayerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$24(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V
    .locals 0

    .prologue
    .line 128
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicSetValue:Z

    return-void
.end method

.method static synthetic access$25(Lcom/sec/android/automotive/drivelink/music/MusicService;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/music/MusicService;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mDefaultAlbumArt:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/music/MusicService;)Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mOnErrorPrevPlay:Z

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V
    .locals 0

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mOnErrorPrevPlay:Z

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/music/MusicService;)V
    .locals 0

    .prologue
    .line 347
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->notifySeekComplte()V

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/music/MusicService;)Z
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsPausedFromAudioFocusLoss:Z

    return v0
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/music/MusicService;)Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFPrevPlaying:Z

    return v0
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/music/MusicService;I)V
    .locals 0

    .prologue
    .line 339
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyError(I)V

    return-void
.end method

.method private getOnDriveLinkMusicListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;
    .locals 1

    .prologue
    .line 1677
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/MusicService$6;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/MusicService$6;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    return-object v0
.end method

.method private notifyBTCallingState()V
    .locals 5

    .prologue
    .line 355
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "notifyBTCallingState"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mListenrs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 358
    return-void

    .line 356
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .line 357
    .local v0, "l":Lcom/sec/android/automotive/drivelink/music/MusicListener;
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/music/MusicListener;->OnBTBtnCallingState()V

    goto :goto_0
.end method

.method private notifyBTClicked()V
    .locals 5

    .prologue
    .line 376
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "notifyBTPause"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mListenrs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 379
    return-void

    .line 377
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .line 378
    .local v0, "l":Lcom/sec/android/automotive/drivelink/music/MusicListener;
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/music/MusicListener;->OnBTBtnClicked()V

    goto :goto_0
.end method

.method private notifyBTPlay()V
    .locals 5

    .prologue
    .line 362
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "notifyBTPlay"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mListenrs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 365
    return-void

    .line 363
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .line 364
    .local v0, "l":Lcom/sec/android/automotive/drivelink/music/MusicListener;
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/music/MusicListener;->OnBTBtnPlay()V

    goto :goto_0
.end method

.method private notifyBTSeek()V
    .locals 5

    .prologue
    .line 382
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "notifyBTPause"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mListenrs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 385
    return-void

    .line 383
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .line 384
    .local v0, "l":Lcom/sec/android/automotive/drivelink/music/MusicListener;
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/music/MusicListener;->OnBTBtnSeek()V

    goto :goto_0
.end method

.method private notifyError(I)V
    .locals 5
    .param p1, "listSize"    # I

    .prologue
    .line 340
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "notifyError"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mListenrs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 344
    return-void

    .line 341
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .line 342
    .local v0, "l":Lcom/sec/android/automotive/drivelink/music/MusicListener;
    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicListener;->OnError(I)V

    goto :goto_0
.end method

.method private notifyFinished(Z)V
    .locals 5
    .param p1, "makePure"    # Z

    .prologue
    .line 330
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "notifyFinished"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mListenrs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 333
    if-eqz p1, :cond_0

    .line 334
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->initializationMusicPlayer(Z)V

    .line 335
    :cond_0
    return-void

    .line 331
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .line 332
    .local v0, "l":Lcom/sec/android/automotive/drivelink/music/MusicListener;
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/music/MusicListener;->OnFinished()V

    goto :goto_0
.end method

.method private notifyMusicChanged(Z)V
    .locals 5
    .param p1, "isPlayed"    # Z

    .prologue
    .line 322
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "notifyMusicChanged"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mListenrs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 325
    return-void

    .line 323
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .line 324
    .local v0, "l":Lcom/sec/android/automotive/drivelink/music/MusicListener;
    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicListener;->onMusicChanged(Z)V

    goto :goto_0
.end method

.method private notifySeekComplte()V
    .locals 5

    .prologue
    .line 348
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "notifySeekComplte"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mListenrs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 351
    return-void

    .line 349
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .line 350
    .local v0, "l":Lcom/sec/android/automotive/drivelink/music/MusicListener;
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/music/MusicListener;->OnSeekComplete()V

    goto :goto_0
.end method

.method private notifySetVolumeControl(Z)V
    .locals 5
    .param p1, "onoff"    # Z

    .prologue
    .line 369
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "notifySetVolumeControl"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mListenrs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 373
    return-void

    .line 370
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .line 371
    .local v0, "l":Lcom/sec/android/automotive/drivelink/music/MusicListener;
    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicListener;->OnSetVolumeControl(Z)V

    goto :goto_0
.end method

.method private notifyShuffle(Z)V
    .locals 6
    .param p1, "bShuffle"    # Z

    .prologue
    .line 388
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "notifyShuffle - "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mListenrs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 391
    return-void

    .line 389
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .line 390
    .local v0, "l":Lcom/sec/android/automotive/drivelink/music/MusicListener;
    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicListener;->OnShuffle(Z)V

    goto :goto_0
.end method

.method private notifyVolumeCmd(I)V
    .locals 6
    .param p1, "cmd"    # I

    .prologue
    .line 394
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "notifyVolumeCmd - "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mListenrs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 397
    return-void

    .line 395
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .line 396
    .local v0, "l":Lcom/sec/android/automotive/drivelink/music/MusicListener;
    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicListener;->OnVolumeControlCommand(I)V

    goto :goto_0
.end method

.method private playPlayerNew()V
    .locals 14

    .prologue
    .line 483
    const-string/jumbo v8, "i"

    const-string/jumbo v9, "[CarMode]"

    const-string/jumbo v10, "MUSICService"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, " music size "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v12, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v9, v10, v11}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-array v4, v8, [J

    .line 485
    .local v4, "list":[J
    const/4 v1, 0x0

    .line 486
    .local v1, "i":I
    const/4 v6, 0x0

    .line 487
    .local v6, "pos":I
    const/4 v0, -0x1

    .line 488
    .local v0, "curId":I
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v7

    .line 489
    .local v7, "showingMusic":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    if-eqz v7, :cond_0

    .line 490
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getId()I

    move-result v0

    .line 492
    :cond_0
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_2

    .line 504
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v8, "com.samsung.musicplus.intent.action.PLAY_CONTENTS"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 505
    .local v3, "intent":Landroid/content/Intent;
    const-string/jumbo v8, "base_uri"

    sget-object v9, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 506
    const-string/jumbo v8, "list"

    invoke-virtual {v3, v8, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 507
    const-string/jumbo v8, "listPosition"

    invoke-virtual {v3, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 508
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mContext:Landroid/content/Context;

    invoke-virtual {v8, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 510
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteController:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->isRegisteredRemoteController()Z

    move-result v8

    if-nez v8, :cond_1

    .line 512
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteController:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->requestReRegister()V

    .line 514
    :cond_1
    const-string/jumbo v8, "i"

    const-string/jumbo v9, "[CarMode]"

    const-string/jumbo v10, "MUSICService"

    const-string/jumbo v11, " start native music player"

    invoke-static {v8, v9, v10, v11}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    return-void

    .line 492
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 493
    .local v5, "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getId()I

    move-result v2

    .line 494
    .local v2, "id":I
    int-to-long v9, v2

    aput-wide v9, v4, v1

    .line 495
    const/4 v9, -0x1

    if-eq v0, v9, :cond_3

    if-ne v0, v2, :cond_3

    .line 496
    move v6, v1

    .line 497
    const/4 v0, -0x1

    .line 498
    const-string/jumbo v9, "i"

    const-string/jumbo v10, "[CarMode]"

    const-string/jumbo v11, "MUSICService"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, " pos = "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v10, v11, v12}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private runTTSMusicUnavailable()V
    .locals 3

    .prologue
    .line 832
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0117

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 833
    .local v0, "systemTurnText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    .line 834
    return-void
.end method

.method private setAudioFocusLogShow(I)V
    .locals 4
    .param p1, "focusChange"    # I

    .prologue
    .line 1451
    packed-switch p1, :pswitch_data_0

    .line 1472
    :goto_0
    :pswitch_0
    return-void

    .line 1453
    :pswitch_1
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onAudioFocusChange - AUDIOFOCUS_LOSS"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1456
    :pswitch_2
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onAudioFocusChange - AUDIOFOCUS_LOSS_TRANSIENT"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1459
    :pswitch_3
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onAudioFocusChange - AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1462
    :pswitch_4
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onAudioFocusChange - AUDIOFOCUS_GAIN"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1465
    :pswitch_5
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onAudioFocusChange - AUDIOFOCUS_GAIN_TRANSIENT"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1468
    :pswitch_6
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onAudioFocusChange - AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1451
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private setFFREThreadWithStop()V
    .locals 3

    .prologue
    .line 2105
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    if-eqz v1, :cond_0

    .line 2107
    :try_start_0
    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "FForwardRewindThread stop"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2108
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->isRunning:Z

    .line 2109
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->interrupt()V

    .line 2110
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2115
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    .line 2117
    :cond_0
    return-void

    .line 2111
    :catch_0
    move-exception v0

    .line 2112
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 2113
    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "FForwardRewindThread join exception"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setMusicList(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1013
    if-eqz p1, :cond_1

    .line 1014
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 1015
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicList:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicListRunning(Ljava/util/ArrayList;)V

    .line 1023
    :cond_0
    :goto_0
    return-void

    .line 1018
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1019
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicList:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicListRunning(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private setMusicListRunning(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "playlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    const/4 v5, 0x1

    .line 1031
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "setMusicShuffleList size : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1033
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setMusicPlaylist(Ljava/util/ArrayList;I)V

    .line 1035
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v5, :cond_0

    .line 1036
    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setShuffle(Z)V

    .line 1037
    :cond_0
    return-void
.end method

.method private setPlayBackRequestPause()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1909
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "setPlayBackRequestPause()"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1910
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFMiddlePriority:Z

    if-nez v0, :cond_0

    .line 1911
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFMiddlePriority:Z

    .line 1912
    iput v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioStateValue:I

    .line 1914
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1915
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mUserPausePriority:Z

    if-nez v0, :cond_3

    .line 1916
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFMiddlePriorityPrevPlaying:Z

    .line 1917
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "PLAYBACK_REQUEST_PAUSE - mAFMiddlePriorityPrevPlaying is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFMiddlePriorityPrevPlaying:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1918
    invoke-virtual {p0, v5, v6, v5}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 1919
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMediaplayerHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 1920
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, " MediaPlayer SetVolume 1"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1921
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setVolume(F)V

    .line 1922
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, " FADEUP Message removed"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1923
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMediaplayerHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1924
    iput v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicDelaycount:I

    .line 1925
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    .line 1927
    :cond_1
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "PLAYBACK_REQUEST_PAUSE - Music Pause"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1940
    :cond_2
    :goto_0
    return-void

    .line 1930
    :cond_3
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "PLAYBACK_REQUEST_PAUSE - User Pause = Not Music Pause"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1932
    :cond_4
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "PLAYBACK_REQUEST_PAUSE - Music Pause state"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1933
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mUserPausePriority:Z

    if-nez v0, :cond_2

    .line 1934
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "PLAYBACK_REQUEST_PAUSE - mIsAudioFocusLoss :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioFocusLoss:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1935
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "PLAYBACK_REQUEST_PAUSE - mAFPrevPlaying :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFPrevPlaying:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1936
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioFocusLoss:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFPrevPlaying:Z

    if-eqz v0, :cond_2

    .line 1937
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFMiddlePriorityPrevPlaying:Z

    goto :goto_0
.end method

.method private setPlayBackRequestResume()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1943
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "setPlayBackRequestResume()"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1944
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFMiddlePriority:Z

    if-eqz v1, :cond_0

    .line 1945
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFMiddlePriority:Z

    .line 1947
    :cond_0
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "mAFHighPriority : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFHighPriority:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1948
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "mAFMiddlePriorityPrevPlaying : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFMiddlePriorityPrevPlaying:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1949
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "mIsAudioStateValue : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioStateValue:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1950
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "mUserPausePriority : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mUserPausePriority:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1951
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, " isCalling : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isCalling()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1952
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isCalling()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1953
    const-string/jumbo v1, "JINSEIL"

    const-string/jumbo v2, "JINSEIL setPlayBackRequestResume is calling!!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2006
    :cond_1
    :goto_0
    return-void

    .line 1956
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFHighPriority:Z

    if-nez v1, :cond_8

    .line 1957
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_7

    .line 1958
    iget v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioStateValue:I

    if-eq v1, v8, :cond_3

    .line 1959
    iget v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioStateValue:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_3

    .line 1960
    iget v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioStateValue:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_3

    .line 1961
    iget v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioStateValue:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_3

    iget v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioStateValue:I

    if-eqz v1, :cond_3

    .line 1966
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFMiddlePriorityPrevPlaying:Z

    if-eqz v1, :cond_6

    .line 1967
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mUserPausePriority:Z

    if-nez v1, :cond_5

    .line 1968
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMediaplayerHandler:Landroid/os/Handler;

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    cmpl-float v1, v1, v7

    if-nez v1, :cond_4

    .line 1969
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, " MediaPlayer SetVolume 0"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1971
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v1, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setVolume(F)V

    .line 1974
    :try_start_0
    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICServiceSetVolume setting delay start"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1975
    const-wide/16 v1, 0x12c

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1979
    :goto_1
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, " FADEUP Message sending"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1981
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMediaplayerHandler:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1984
    :cond_4
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "mCurrentVolume : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1985
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->playPlayer()V

    .line 1986
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFMiddlePriorityPrevPlaying:Z

    .line 1987
    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService onPlaybackRequest in mAFMiddlePriorityPrevPlaying setting false"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1988
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "PLAYBACK_REQUEST_PAUSE - Music Play"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1976
    :catch_0
    move-exception v0

    .line 1977
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICServiceSetVolume setting delay exception"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1990
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_5
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->pausePlayer(I)V

    goto/16 :goto_0

    .line 1994
    :cond_6
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFMiddlePriorityPrevPlaying:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFPrevPlaying:Z

    if-nez v1, :cond_1

    .line 1995
    iput-boolean v8, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFPrevPlaying:Z

    .line 1996
    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService mAFPrevPlaying setting true"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2000
    :cond_7
    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICServiceisPlaying is true"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2003
    :cond_8
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFHighPriority:Z

    .line 2004
    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService mAFHighPriority is true and setting false"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private setRemoteControlAllMusicShuffleList()Z
    .locals 5

    .prologue
    const v4, 0x7f0a0117

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1057
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getDeviceMusicListSize()I

    move-result v3

    if-ne v3, v2, :cond_0

    .line 1058
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1059
    .local v0, "systemTurnText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    .line 1069
    .end local v0    # "systemTurnText":Ljava/lang/String;
    :goto_0
    return v1

    .line 1063
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setAllMusicShuffleList()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1064
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1065
    .restart local v0    # "systemTurnText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "systemTurnText":Ljava/lang/String;
    :cond_1
    move v1, v2

    .line 1069
    goto :goto_0
.end method


# virtual methods
.method public abandonAudioFocus()V
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 421
    :cond_0
    return-void
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 1044
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getCurrentPosition()I

    move-result v0

    return v0
.end method

.method public getDeviceMusicListSize()I
    .locals 1

    .prologue
    .line 1053
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getMaxMusicVolume()I
    .locals 2

    .prologue
    .line 1605
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    .line 1606
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    .line 1608
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    return v0
.end method

.method public getMediaPlayerListCount()I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 731
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    if-eqz v1, :cond_0

    .line 732
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMusicPlaylist()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 737
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMusicPlaylist()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 741
    :cond_0
    return v0
.end method

.method public getMusicPlayList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 771
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMusicPlaylist()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getMusicVolume()I
    .locals 2

    .prologue
    .line 1597
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    .line 1598
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    .line 1601
    :cond_0
    iget v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mStreamVolumeValue:I

    return v0
.end method

.method public getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .locals 1

    .prologue
    .line 686
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    if-nez v0, :cond_0

    .line 687
    const/4 v0, 0x0

    .line 689
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    goto :goto_0
.end method

.method public getPlayingMusicDuration()I
    .locals 3

    .prologue
    .line 697
    const/4 v1, 0x0

    .line 698
    .local v1, "ret":I
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 699
    .local v0, "item":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    if-eqz v0, :cond_0

    .line 700
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getDuration()I

    move-result v1

    .line 701
    :cond_0
    return v1
.end method

.method public getSeekFFRETime()I
    .locals 3

    .prologue
    .line 1548
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mFRAttach:[I

    array-length v0, v0

    iget v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mFRCount:I

    if-le v0, v1, :cond_0

    .line 1549
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mFRAttach:[I

    iget v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mFRCount:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mFRCount:I

    aget v0, v0, v1

    .line 1551
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mFRAttach:[I

    iget v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mFRCount:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_0
.end method

.method public getmAFHighPriority()Z
    .locals 1

    .prologue
    .line 2035
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFHighPriority:Z

    return v0
.end method

.method public declared-synchronized init(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 204
    monitor-enter p0

    :try_start_0
    const-string/jumbo v0, "e"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "init"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mbInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 258
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 210
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    .line 211
    const-string/jumbo v0, "e"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "@@@drivelink framework is not initialized"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 204
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 215
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getOnDriveLinkMusicListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkMusicListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;)V

    .line 217
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mContext:Landroid/content/Context;

    .line 218
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    .line 219
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getMusicPlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v0

    .line 218
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    .line 220
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;->ALL:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setRepeatType(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicRepeatType;)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mFW_Listener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setOnMusicPlayerListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;)V

    .line 226
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getMusicRemotePlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    .line 239
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->getInstance()Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->registerHeadsetPlugListener(Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;)V

    .line 241
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mBTFW_Listener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkMusicRemotePlayerListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;)V

    .line 243
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->isStarted:Z

    .line 244
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    .line 246
    const-string/jumbo v0, "phone"

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->telephoneMgr:Landroid/telephony/TelephonyManager;

    .line 247
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->telephoneMgr:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->phoneListener:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestMusicList(Landroid/content/Context;)V

    .line 253
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020224

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mDefaultAlbumArt:Landroid/graphics/Bitmap;

    .line 255
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mbInitialized:Z

    .line 257
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mbIsFirstMusicPlay:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public initMusicVolume()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 1577
    const-string/jumbo v0, "[CarMode]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Carmode_MusicService_setStreamVol_initMusicVolume before "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mStreamVolumeValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " broadcastCnt "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " IDLE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->isIdle()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1578
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    .line 1579
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    .line 1581
    :cond_0
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->isIdle()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1582
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mStreamVolumeValue:I

    .line 1583
    :cond_1
    const-string/jumbo v0, "[CarMode]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Carmode_MusicService_setStreamVol_initMusicVolume mAudioManager.getStreamVolume() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1584
    return-void
.end method

.method public initializationMusicPlayer(Z)V
    .locals 3
    .param p1, "initial"    # Z

    .prologue
    .line 1479
    if-eqz p1, :cond_0

    .line 1480
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setMusicPlaylist(Ljava/util/ArrayList;I)V

    .line 1482
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->isStarted:Z

    .line 1484
    return-void
.end method

.method public isAFLoss()Z
    .locals 1

    .prologue
    .line 424
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioFocusLoss:Z

    return v0
.end method

.method public isCallMultiwindow()Z
    .locals 1

    .prologue
    .line 1531
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMultiWindowCallPlayer:Z

    return v0
.end method

.method public isCalling()Z
    .locals 1

    .prologue
    .line 1491
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCallingState:Z

    return v0
.end method

.method public isMusicExist()Z
    .locals 3

    .prologue
    .line 723
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getMusicCount(Landroid/content/Context;)I

    move-result v0

    .line 724
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 725
    const/4 v1, 0x1

    .line 727
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isMusicList()Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;
    .locals 5

    .prologue
    .line 960
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mContext:Landroid/content/Context;

    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getAllMusicList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicList:Ljava/util/ArrayList;

    .line 961
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mContext:Landroid/content/Context;

    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getMusicCount(Landroid/content/Context;)I

    move-result v0

    .line 963
    .local v0, "mMusicCount":I
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "isMusicList"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    if-lez v0, :cond_0

    .line 967
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "isMusicList - true"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 968
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;->MUSIC_MUSICLIST_AVAILABLE:Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;

    .line 977
    :goto_0
    return-object v1

    .line 970
    :cond_0
    if-nez v0, :cond_1

    .line 971
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "isMusicList - false"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;->MUSIC_MUSICLIST_SIZE_ZERO:Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;

    goto :goto_0

    .line 976
    :cond_1
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "isMusicList - null"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 977
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;->MUSIC_MUSICLIST_NULL:Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;

    goto :goto_0
.end method

.method public isMusicListUnknown()Z
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 897
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 898
    .local v0, "item":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlayList()Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;

    move-result-object v2

    .line 899
    .local v2, "mPlayListState":Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isMusicList()Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;

    move-result-object v1

    .line 900
    .local v1, "mMusicListState":Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;
    if-nez v0, :cond_2

    .line 901
    const-string/jumbo v5, "i"

    const-string/jumbo v6, "[CarMode]"

    const-string/jumbo v7, "[MUSIC]"

    const-string/jumbo v8, "DLMusic is null"

    invoke-static {v5, v6, v7, v8}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 902
    sget-object v5, Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;->MUSIC_PLAYLIST_NULL:Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;

    if-ne v2, v5, :cond_1

    .line 903
    const-string/jumbo v5, "i"

    const-string/jumbo v6, "[CarMode]"

    const-string/jumbo v7, "[MUSIC]"

    const-string/jumbo v8, "Device PlayList is null"

    invoke-static {v5, v6, v7, v8}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 904
    sget-object v5, Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;->MUSIC_MUSICLIST_AVAILABLE:Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;

    if-ne v1, v5, :cond_0

    .line 905
    const-string/jumbo v4, "i"

    const-string/jumbo v5, "[CarMode]"

    const-string/jumbo v6, "[MUSIC]"

    const-string/jumbo v7, "Device Musiclist is Available"

    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 921
    :goto_0
    return v3

    .line 908
    :cond_0
    const-string/jumbo v3, "i"

    const-string/jumbo v5, "[CarMode]"

    const-string/jumbo v6, "[MUSIC]"

    const-string/jumbo v7, "Device Musiclist is zero"

    invoke-static {v3, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v3, v4

    .line 909
    goto :goto_0

    .line 912
    :cond_1
    const-string/jumbo v4, "i"

    const-string/jumbo v5, "[CarMode]"

    const-string/jumbo v6, "[MUSIC]"

    const-string/jumbo v7, "Device PlayList is Available"

    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 915
    :cond_2
    const-string/jumbo v5, "i"

    const-string/jumbo v6, "[CarMode]"

    const-string/jumbo v7, "[MUSIC]"

    const-string/jumbo v8, "DLMusic is not null"

    invoke-static {v5, v6, v7, v8}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 916
    sget-object v5, Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;->MUSIC_MUSICLIST_AVAILABLE:Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;

    if-ne v1, v5, :cond_3

    .line 917
    const-string/jumbo v4, "i"

    const-string/jumbo v5, "[CarMode]"

    const-string/jumbo v6, "[MUSIC]"

    const-string/jumbo v7, "Device Musiclist Available"

    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 920
    :cond_3
    const-string/jumbo v3, "i"

    const-string/jumbo v5, "[CarMode]"

    const-string/jumbo v6, "[MUSIC]"

    const-string/jumbo v7, "Device Musiclist is zero"

    invoke-static {v3, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v3, v4

    .line 921
    goto :goto_0
.end method

.method public isMusicSearchResultExist()Z
    .locals 1

    .prologue
    .line 2097
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsMusicSearchResultExist:Z

    return v0
.end method

.method public isMusicSetValue()Z
    .locals 1

    .prologue
    .line 1568
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicSetValue:Z

    return v0
.end method

.method public isPlayList()Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;
    .locals 5

    .prologue
    .line 934
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "isPlayList"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 935
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMusicPlaylist()Ljava/util/ArrayList;

    move-result-object v0

    .line 936
    .local v0, "playList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    if-eqz v0, :cond_1

    .line 937
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 938
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "isPlayList - true"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 939
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;->MUSIC_PLAYLIST_AVAILABLE:Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;

    .line 946
    :goto_0
    return-object v1

    .line 941
    :cond_0
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "isPlayList - false"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;->MUSIC_PLAYLIST_SIZE_ZERO:Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;

    goto :goto_0

    .line 945
    :cond_1
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "isPlayList - null"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 946
    sget-object v1, Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;->MUSIC_PLAYLIST_NULL:Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 710
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    if-eqz v2, :cond_0

    .line 711
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isPlayerPlaying()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 716
    :cond_0
    :goto_0
    return v1

    .line 714
    :catch_0
    move-exception v0

    .line 715
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICServiceMusicController is null"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isPlayingPrivateMusic()Z
    .locals 1

    .prologue
    .line 747
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isPlayingMusicInPrivateMode()Z

    move-result v0

    return v0
.end method

.method public isShuffled()Z
    .locals 1

    .prologue
    .line 752
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isShuffled()Z

    move-result v0

    return v0
.end method

.method public isStarted()Z
    .locals 1

    .prologue
    .line 1487
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->isStarted:Z

    return v0
.end method

.method public ismUserPausePriority()Z
    .locals 1

    .prologue
    .line 1784
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mUserPausePriority:Z

    return v0
.end method

.method public nextPlayer()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 586
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "nextPlayer"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->requestAudioFocus()V

    .line 592
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->playNextMusic(Z)Z

    .line 594
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->isStarted:Z

    .line 595
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioFocusLoss:Z

    .line 596
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mUserPausePriority:Z

    .line 597
    return-void
.end method

.method public nextPlayerNoPlay()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 600
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "nextPlayerNoPlay"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->playNextMusic(Z)Z

    .line 609
    invoke-direct {p0, v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyMusicChanged(Z)V

    .line 610
    return-void
.end method

.method public nextPlayerNoPlayOnError()V
    .locals 4

    .prologue
    .line 613
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "nextPlayerNoPlayOnError"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->playNextMusic(Z)Z

    .line 623
    return-void
.end method

.method public onAudioFocusChange(I)V
    .locals 9
    .param p1, "focusChange"    # I

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1789
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    if-nez v1, :cond_1

    .line 1906
    :cond_0
    :goto_0
    return-void

    .line 1791
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setAudioFocusLogShow(I)V

    .line 1792
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->isStarted:Z

    if-eqz v1, :cond_0

    .line 1794
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFMiddlePriority:Z

    if-eqz v1, :cond_2

    .line 1795
    iput p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioStateValue:I

    .line 1796
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "PLAYBACK_REQUEST_PAUSE Setting input AudioFocus"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1799
    :cond_2
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1807
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isCalling()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1808
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "Audio Focus Loss- isPlaying (Pause)"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1809
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFPrevPlaying:Z

    .line 1810
    invoke-virtual {p0, v6, v5, v6}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 1811
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsPausedFromAudioFocusLoss:Z

    .line 1812
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "mIsPausedFromAudioFocusLoss : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsPausedFromAudioFocusLoss:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1817
    :pswitch_2
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioFocusLoss:Z

    if-nez v1, :cond_0

    .line 1818
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioFocusLoss:Z

    .line 1823
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1824
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "Audio Focus Loss- isPlaying (Pause)"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1825
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFPrevPlaying:Z

    .line 1826
    invoke-virtual {p0, v6, v5, v6}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 1827
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsPausedFromAudioFocusLoss:Z

    .line 1828
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "mIsPausedFromAudioFocusLoss : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsPausedFromAudioFocusLoss:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1829
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMediaplayerHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 1830
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, " MediaPlayer SetVolume 1"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1831
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setVolume(F)V

    .line 1832
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, " FADEUP Message removed"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1833
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMediaplayerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 1834
    iput v7, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    .line 1835
    iput v6, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicDelaycount:I

    goto/16 :goto_0

    .line 1848
    :pswitch_3
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioFocusLoss:Z

    .line 1849
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, " mAFHighPriority : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFHighPriority:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1850
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, " mAFPrevPlaying : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFPrevPlaying:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1851
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, " mUserPausePriority : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mUserPausePriority:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1852
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, " isPlaying : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1853
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, " isCalling : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isCalling()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1854
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isCalling()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1855
    const-string/jumbo v1, "JINSEIL"

    const-string/jumbo v2, "JINSEIL is calling!!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1858
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFHighPriority:Z

    if-nez v1, :cond_5

    .line 1859
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFPrevPlaying:Z

    if-eqz v1, :cond_5

    .line 1860
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mUserPausePriority:Z

    if-nez v1, :cond_5

    .line 1861
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1862
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "Audio Focus Gain- Resume"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1863
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMediaplayerHandler:Landroid/os/Handler;

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    cmpl-float v1, v1, v7

    if-nez v1, :cond_4

    .line 1864
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, " MediaPlayer SetVolume 0"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1866
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v1, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setVolume(F)V

    .line 1869
    :try_start_0
    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICServiceSetVolume setting delay start"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1870
    const-wide/16 v1, 0x12c

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1874
    :goto_1
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, " FADEUP Message sending"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1876
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMediaplayerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1879
    :cond_4
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "mCurrentVolume : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1880
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->playPlayer()V

    .line 1881
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFPrevPlaying:Z

    .line 1882
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFMiddlePriorityPrevPlaying:Z

    if-eqz v1, :cond_5

    .line 1883
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFMiddlePriorityPrevPlaying:Z

    .line 1884
    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService AUDIOFOCUS_GAIN in mAFMiddlePriorityPrevPlaying setting false"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1891
    :cond_5
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "mIsPausedFromAudioFocusLoss : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsPausedFromAudioFocusLoss:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1892
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsPausedFromAudioFocusLoss:Z

    if-eqz v1, :cond_7

    .line 1893
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isPlayerPlaying()Z

    move-result v1

    if-nez v1, :cond_6

    .line 1894
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->pausePlayer(I)V

    .line 1896
    :cond_6
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsPausedFromAudioFocusLoss:Z

    goto/16 :goto_0

    .line 1871
    :catch_0
    move-exception v0

    .line 1872
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICServiceSetVolume setting delay exception"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1898
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_7
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isPlayerPlaying()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1899
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->pausePlayer(I)V

    .line 1900
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mDefaultAlbumArt:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->setMetaData(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 1799
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->binder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 199
    invoke-super {p0, p1}, Landroid/app/Service;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 200
    return-void
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 168
    const-string/jumbo v0, "e"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, " onCreate"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 170
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->abandonAudioFocus()V

    .line 176
    const-string/jumbo v0, "e"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, " onDestroy"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 178
    return-void
.end method

.method public onHeadsetPlugChange(Z)V
    .locals 2
    .param p1, "isPlugged"    # Z

    .prologue
    .line 2121
    if-eqz p1, :cond_0

    .line 2122
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "MUSICServiceHeadset plugged"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2123
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->notifySetVolumeControl(Z)V

    .line 2128
    :goto_0
    return-void

    .line 2125
    :cond_0
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "MUSICServiceHeadset unplugged"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2126
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->notifySetVolumeControl(Z)V

    goto :goto_0
.end method

.method public onPlaybackRequest(I)V
    .locals 3
    .param p1, "request"    # I

    .prologue
    .line 2010
    const-string/jumbo v0, "[CarMode]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "MUSICServicemNotBTControlPriority : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mNotBTControlPriority:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2011
    packed-switch p1, :pswitch_data_0

    .line 2032
    :goto_0
    return-void

    .line 2013
    :pswitch_0
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "MUSICServicePLAYBACK_REQUEST_PHONE_PAUSE"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2014
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mNotBTControlPriority:Z

    .line 2015
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setPlayBackRequestPause()V

    .line 2016
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->pausePlayer(I)V

    goto :goto_0

    .line 2019
    :pswitch_1
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "MUSICServicePLAYBACK_REQUEST_PAUSE"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2020
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setPlayBackRequestPause()V

    goto :goto_0

    .line 2023
    :pswitch_2
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "MUSICServicePLAYBACK_REQUEST_PHONE_RESUME"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2024
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mNotBTControlPriority:Z

    .line 2025
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setPlayBackRequestResume()V

    goto :goto_0

    .line 2028
    :pswitch_3
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "MUSICServicePLAYBACK_REQUEST_RESUME"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2029
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setPlayBackRequestResume()V

    goto :goto_0

    .line 2011
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 187
    const-string/jumbo v0, "e"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, " onUnbind"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public pausePlayer(ZZZ)V
    .locals 4
    .param p1, "isAutoFinishStart"    # Z
    .param p2, "isListenerOn"    # Z
    .param p3, "isByVoice"    # Z

    .prologue
    .line 556
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "pausePlayer"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    iput-boolean p2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->b_isListenerOn:Z

    .line 562
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v0, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->pausePlayer(Z)V

    .line 565
    return-void
.end method

.method public playPlayer()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 521
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "playPlayer"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->requestAudioFocus()V

    .line 538
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    if-nez v0, :cond_0

    .line 539
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "getPlayingMusic() == null"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    :goto_0
    return-void

    .line 543
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->resumePlayer()V

    .line 545
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->isStarted:Z

    .line 547
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioFocusLoss:Z

    .line 548
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mUserPausePriority:Z

    goto :goto_0
.end method

.method public prevPlayer(Z)V
    .locals 8
    .param p1, "bPlayByVoice"    # Z

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 629
    const-string/jumbo v2, "i"

    const-string/jumbo v3, "[CarMode]"

    const-string/jumbo v4, "MUSICService"

    const-string/jumbo v5, "prevPlayer"

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->requestAudioFocus()V

    .line 636
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayerStatus()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    move-result-object v1

    .line 637
    .local v1, "status":Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getCurrentPosition()I

    move-result v0

    .line 639
    .local v0, "passedTime":I
    if-eqz p1, :cond_0

    .line 640
    const-string/jumbo v2, "i"

    const-string/jumbo v3, "[CarMode]"

    const-string/jumbo v4, "MUSICService"

    const-string/jumbo v5, "prevPlayer() => bPlayByVoice == true"

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v2, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->playPrevMusic(Z)Z

    .line 652
    :goto_0
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->isStarted:Z

    .line 653
    iput-boolean v7, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioFocusLoss:Z

    .line 654
    iput-boolean v7, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mUserPausePriority:Z

    .line 655
    return-void

    .line 643
    :cond_0
    const-string/jumbo v2, "i"

    const-string/jumbo v3, "[CarMode]"

    const-string/jumbo v4, "MUSICService"

    const-string/jumbo v5, "prevPlayer() => bPlayByVoice == false"

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 644
    const/16 v2, 0xbb8

    if-le v0, v2, :cond_2

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->STARTED:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->RESUMED:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    if-ne v1, v2, :cond_2

    .line 645
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v2, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->startPlayer(Z)V

    goto :goto_0

    .line 648
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v2, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->playPrevMusic(Z)Z

    goto :goto_0
.end method

.method public prevPlayerNoPlay()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 658
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "prevPlayerNoPlay"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->playPrevMusic(Z)Z

    .line 664
    invoke-direct {p0, v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyMusicChanged(Z)V

    .line 666
    return-void
.end method

.method public registerListener(Lcom/sec/android/automotive/drivelink/music/MusicListener;)V
    .locals 1
    .param p1, "l"    # Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mListenrs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 300
    return-void
.end method

.method public requestAudioFocus()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x1

    .line 400
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v2, :cond_0

    .line 401
    const-string/jumbo v2, "i"

    const-string/jumbo v3, "[CarMode]"

    const-string/jumbo v4, "MUSICService"

    const-string/jumbo v5, "requestAudioFocus AUDIOFOCUS_GAIN"

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, p0, v7, v6}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v1

    .line 403
    .local v1, "result":I
    :goto_0
    if-eqz v1, :cond_1

    .line 413
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsPausedFromAudioFocusLoss:Z

    .line 415
    .end local v1    # "result":I
    :cond_0
    return-void

    .line 406
    .restart local v1    # "result":I
    :cond_1
    const-wide/16 v2, 0xc8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 411
    :goto_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, p0, v7, v6}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v1

    goto :goto_0

    .line 407
    :catch_0
    move-exception v0

    .line 409
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method

.method public seekToPlayer(I)V
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 670
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "seekToPlayer"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->seekToPlayer(I)V

    .line 675
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v1

    int-to-long v2, p1

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->setPlaybackState(ZJF)V

    .line 677
    return-void
.end method

.method public setAllMusicShuffleList()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1073
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getMediaPlayerListCount()I

    move-result v0

    .line 1075
    .local v0, "mMusicPlayListSize":I
    if-ne v0, v2, :cond_1

    .line 1077
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1078
    .local v1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1079
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v5

    invoke-virtual {v4, v1, v5, v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicPlayList(Ljava/util/ArrayList;ZZ)V

    .line 1086
    .end local v1    # "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    :cond_0
    :goto_0
    return v2

    .line 1082
    :cond_1
    if-gt v0, v2, :cond_0

    move v2, v3

    .line 1086
    goto :goto_0
.end method

.method public setCallMultiwindow(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 1535
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMultiWindowCallPlayer:Z

    .line 1536
    return-void
.end method

.method public setCheckListSettingMusicList()Z
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 838
    :try_start_0
    const-string/jumbo v5, "i"

    const-string/jumbo v6, "[CarMode]"

    const-string/jumbo v7, "[MUSIC]"

    const-string/jumbo v8, "updateMusicList request"

    invoke-static {v5, v6, v7, v8}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 840
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlayList()Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;

    move-result-object v2

    .line 841
    .local v2, "mPlayListState":Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isMusicList()Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;

    move-result-object v1

    .line 843
    .local v1, "mMusicListState":Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;
    sget-object v5, Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;->MUSIC_PLAYLIST_AVAILABLE:Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;

    if-ne v2, v5, :cond_2

    .line 845
    sget-object v5, Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;->MUSIC_MUSICLIST_AVAILABLE:Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;

    if-eq v1, v5, :cond_1

    .line 846
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->runTTSMusicUnavailable()V

    .line 887
    .end local v1    # "mMusicListState":Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;
    .end local v2    # "mPlayListState":Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;
    :cond_0
    :goto_0
    return v3

    .restart local v1    # "mMusicListState":Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;
    .restart local v2    # "mPlayListState":Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;
    :cond_1
    move v3, v4

    .line 849
    goto :goto_0

    .line 850
    :cond_2
    sget-object v5, Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;->MUSIC_PLAYLIST_SIZE_ZERO:Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;

    if-ne v2, v5, :cond_4

    .line 853
    sget-object v5, Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;->MUSIC_MUSICLIST_AVAILABLE:Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;

    if-ne v1, v5, :cond_3

    .line 854
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicShuffledListFW()V

    .line 856
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlayList()Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;

    move-result-object v2

    .line 857
    sget-object v5, Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;->MUSIC_PLAYLIST_AVAILABLE:Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;

    if-ne v2, v5, :cond_0

    move v3, v4

    .line 858
    goto :goto_0

    .line 862
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->runTTSMusicUnavailable()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 883
    .end local v1    # "mMusicListState":Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;
    .end local v2    # "mPlayListState":Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;
    :catch_0
    move-exception v0

    .line 884
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v4, "i"

    const-string/jumbo v5, "[CarMode]"

    const-string/jumbo v6, "[MUSIC]"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 865
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "mMusicListState":Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;
    .restart local v2    # "mPlayListState":Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;
    :cond_4
    :try_start_1
    sget-object v5, Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;->MUSIC_PLAYLIST_NULL:Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;

    if-ne v2, v5, :cond_0

    .line 868
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 869
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/music/MusicService;->stopPlayer()V

    .line 870
    :cond_5
    sget-object v5, Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;->MUSIC_MUSICLIST_AVAILABLE:Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_MUSICLIST_STATE;

    if-ne v1, v5, :cond_6

    .line 871
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicShuffleList(Z)V

    .line 872
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlayList()Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;

    move-result-object v2

    .line 873
    sget-object v5, Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;->MUSIC_PLAYLIST_AVAILABLE:Lcom/sec/android/automotive/drivelink/music/MusicService$MUSIC_PLAYLIST_STATE;

    if-ne v2, v5, :cond_0

    move v3, v4

    .line 874
    goto :goto_0

    .line 878
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->runTTSMusicUnavailable()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public setFFREInit()V
    .locals 1

    .prologue
    .line 1564
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mFRCount:I

    .line 1565
    return-void
.end method

.method public setFForwardRewind(Z)V
    .locals 5
    .param p1, "value"    # Z

    .prologue
    const/4 v4, 0x0

    .line 1645
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getCurrentPosition()I

    move-result v0

    .line 1646
    .local v0, "currentPosition":I
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getSeekFFRETime()I

    move-result v1

    .line 1647
    .local v1, "seekTime":I
    if-eqz p1, :cond_1

    .line 1648
    add-int v2, v0, v1

    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getPlayingMusicDuration()I

    move-result v3

    if-gt v2, v3, :cond_0

    .line 1649
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    add-int v3, v0, v1

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->seekToPlayer(I)V

    .line 1674
    :goto_0
    return-void

    .line 1651
    :cond_0
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicSetValue:Z

    .line 1652
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getPlayingMusicDuration()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->seekToPlayer(I)V

    .line 1653
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMediaplayerHandler:Landroid/os/Handler;

    const v3, 0xc350

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1661
    :cond_1
    sub-int v2, v0, v1

    if-ltz v2, :cond_2

    .line 1662
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    sub-int v3, v0, v1

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->seekToPlayer(I)V

    goto :goto_0

    .line 1664
    :cond_2
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicSetValue:Z

    .line 1665
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->seekToPlayer(I)V

    .line 1666
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMediaplayerHandler:Landroid/os/Handler;

    const v3, 0xc351

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public setFForwardRewind(ZZ)V
    .locals 1
    .param p1, "isFForward"    # Z
    .param p2, "isUp"    # Z

    .prologue
    .line 1556
    if-eqz p1, :cond_0

    .line 1557
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteController:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    invoke-virtual {v0, p2}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->ffoward(Z)V

    .line 1561
    :goto_0
    return-void

    .line 1559
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteController:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    invoke-virtual {v0, p2}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->rewind(Z)V

    goto :goto_0
.end method

.method public setMusicPlayList(Ljava/util/ArrayList;ZZ)V
    .locals 6
    .param p2, "isPlay"    # Z
    .param p3, "isRandomStart"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    const/4 v5, 0x0

    .line 777
    const-string/jumbo v2, "[CarMode]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "setMusicPlayList isPlay : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", isRandomStart : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 779
    const/4 v1, 0x0

    .line 780
    .local v1, "startMusicIndex":I
    if-eqz p3, :cond_0

    .line 781
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 782
    .local v0, "random":Ljava/util/Random;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    .line 785
    .end local v0    # "random":Ljava/util/Random;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->requestAudioFocus()V

    .line 787
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v2, p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setMusicPlaylist(Ljava/util/ArrayList;I)V

    .line 789
    if-eqz p2, :cond_1

    .line 791
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->isStarted:Z

    .line 792
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioFocusLoss:Z

    .line 793
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mUserPausePriority:Z

    .line 796
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v2, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->startPlayer(Z)V

    .line 798
    return-void
.end method

.method public setMusicPlayerVolume()V
    .locals 5

    .prologue
    .line 428
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, " setMusicPlayerVolume"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, " FADEUP Message removed"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMediaplayerHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 431
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, " MediaPlayer SetVolume 1"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setVolume(F)V

    .line 433
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "mCurrentVolume : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "mIsAudioStateValue : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioStateValue:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    return-void
.end method

.method public setMusicSearchResultFlag(Z)V
    .locals 0
    .param p1, "bMusicExist"    # Z

    .prologue
    .line 2101
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsMusicSearchResultExist:Z

    .line 2102
    return-void
.end method

.method public setMusicSetValue(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 1572
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicSetValue:Z

    .line 1573
    return-void
.end method

.method public setMusicShuffleList(Z)V
    .locals 5
    .param p1, "value"    # Z

    .prologue
    .line 993
    if-eqz p1, :cond_1

    .line 994
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    if-eqz v0, :cond_0

    .line 995
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicList(Z)V

    .line 996
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "setMusicShuffleList - setting : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    :cond_0
    :goto_0
    return-void

    .line 999
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicList(Z)V

    .line 1000
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "setMusicShuffleList - setting : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setMusicShuffledListFW()V
    .locals 1

    .prologue
    .line 982
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setAllMusicListShuffled()Z

    .line 983
    return-void
.end method

.method public setMusicVolume(I)V
    .locals 4
    .param p1, "volume"    # I

    .prologue
    const/4 v3, 0x3

    .line 1587
    const-string/jumbo v0, "[CarMode]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Carmode_MusicService_setStreamVol_setMusicVolume "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1588
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    .line 1589
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    .line 1591
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, p1, v1}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 1592
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mStreamVolumeValue:I

    .line 1593
    const-string/jumbo v0, "[CarMode]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Carmode_MusicService_setStreamVol_setMusicVolume volume "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " mStreamVolumeValue "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mStreamVolumeValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1594
    return-void
.end method

.method public setShuffle(Z)V
    .locals 4
    .param p1, "onOff"    # Z

    .prologue
    .line 757
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "setShuffle"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setShuffle(Z)V

    .line 760
    if-eqz p1, :cond_0

    .line 761
    const-string/jumbo v0, "[CarMode]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "shuffle is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 762
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->setShuffle(I)V

    .line 767
    :goto_0
    return-void

    .line 764
    :cond_0
    const-string/jumbo v0, "[CarMode]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "shuffle is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->setShuffle(I)V

    goto :goto_0
.end method

.method public setSoundMuteFalse()V
    .locals 2

    .prologue
    .line 823
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFMiddlePriority:Z

    if-eqz v0, :cond_0

    .line 824
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFHighPriority:Z

    if-eqz v0, :cond_0

    .line 825
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFHighPriority:Z

    .line 826
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "MUSICService mAFHighPriority false"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 829
    :cond_0
    return-void
.end method

.method public setSoundMuteTure()V
    .locals 2

    .prologue
    .line 814
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFMiddlePriority:Z

    if-eqz v0, :cond_0

    .line 815
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFHighPriority:Z

    if-nez v0, :cond_0

    .line 816
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAFHighPriority:Z

    .line 817
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "MUSICService mAFHighPriority true"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 820
    :cond_0
    return-void
.end method

.method public setUserSettingPauseOrStop()V
    .locals 4

    .prologue
    .line 805
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "User Setting Pause"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mUserPausePriority:Z

    .line 807
    return-void
.end method

.method public setVolumeCmd(I)V
    .locals 4
    .param p1, "cmd"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x3

    .line 1612
    packed-switch p1, :pswitch_data_0

    .line 1635
    :cond_0
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyVolumeCmd(I)V

    .line 1636
    return-void

    .line 1614
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2, v3}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    .line 1615
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2, v1, v3}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    .line 1616
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mStreamVolumeValue:I

    goto :goto_0

    .line 1619
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2, v3}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    .line 1620
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, -0x1

    invoke-virtual {v0, v2, v1, v3}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    .line 1621
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mStreamVolumeValue:I

    goto :goto_0

    .line 1624
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mStreamVolumeValue:I

    .line 1625
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2, v1}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    goto :goto_0

    .line 1628
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 1629
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    iget v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mStreamVolumeValue:I

    invoke-virtual {v0, v2, v1, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 1630
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2, v3}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    goto :goto_0

    .line 1612
    :pswitch_data_0
    .packed-switch 0xea60
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public startForegroundService(ILandroid/app/Notification;)V
    .locals 1
    .param p1, "appId"    # I
    .param p2, "notification"    # Landroid/app/Notification;

    .prologue
    .line 307
    if-nez p2, :cond_0

    .line 316
    :goto_0
    return-void

    .line 311
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mbForegroundService:Z

    if-nez v0, :cond_1

    .line 312
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->startForeground(ILandroid/app/Notification;)V

    .line 315
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mbForegroundService:Z

    goto :goto_0
.end method

.method public startPlayer()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 469
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "startPlayer"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->requestAudioFocus()V

    .line 475
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->startPlayer(Z)V

    .line 476
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->isStarted:Z

    .line 477
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioFocusLoss:Z

    .line 478
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mUserPausePriority:Z

    .line 479
    return-void
.end method

.method public startPlayerWithAllMusicShuffled()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 439
    const-string/jumbo v1, "i"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "startPlayerWithAllMusicShuffled"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    if-nez v1, :cond_1

    .line 465
    :cond_0
    :goto_0
    return-void

    .line 449
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getAllMusicList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    .line 451
    .local v0, "allMusicInDevice":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    if-eqz v0, :cond_0

    .line 454
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v5, :cond_2

    .line 455
    invoke-virtual {p0, v0, v6, v5}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicPlayList(Ljava/util/ArrayList;ZZ)V

    .line 456
    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setShuffle(Z)V

    .line 459
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->requestAudioFocus()V

    .line 460
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v1, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->startPlayer(Z)V

    .line 461
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->isStarted:Z

    .line 462
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mIsAudioFocusLoss:Z

    .line 463
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mUserPausePriority:Z

    goto :goto_0
.end method

.method public stopPlayer()V
    .locals 4

    .prologue
    .line 569
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "stopPlayer"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->stopPlayer()V

    .line 580
    return-void
.end method

.method public declared-synchronized terminate(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 262
    monitor-enter p0

    :try_start_0
    const-string/jumbo v1, "e"

    const-string/jumbo v2, "[CarMode]"

    const-string/jumbo v3, "MUSICService"

    const-string/jumbo v4, "terminate"

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->getInstance()Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->unregisterHeadsetPlugListener(Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;)V

    .line 266
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMediaplayerHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 267
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMediaplayerHandler:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 270
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mbInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 296
    :goto_0
    monitor-exit p0

    return-void

    .line 274
    :cond_1
    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mbInitialized:Z

    .line 280
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setOnMusicPlayerListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;)V

    .line 281
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getPlayerStatus()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    move-result-object v0

    .line 282
    .local v0, "status":Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->RESUMED:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    if-eq v0, v1, :cond_2

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->STARTED:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    if-ne v0, v1, :cond_3

    .line 283
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->stopPlayer()V

    .line 285
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->telephoneMgr:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->phoneListener:Landroid/telephony/PhoneStateListener;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 288
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->stopPlayer()V

    .line 289
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->resetMetaData()V

    .line 291
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mContext:Landroid/content/Context;

    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->unregisterMusicRemoteControl(Landroid/content/Context;)V

    .line 294
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->abandonAudioFocus()V

    .line 295
    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "unregister MusicRemoteControl"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 262
    .end local v0    # "status":Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public unregisterListener(Lcom/sec/android/automotive/drivelink/music/MusicListener;)V
    .locals 1
    .param p1, "l"    # Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .prologue
    .line 303
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mListenrs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 304
    return-void
.end method
