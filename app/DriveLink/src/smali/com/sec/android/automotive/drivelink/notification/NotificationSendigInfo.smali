.class public Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;
.super Ljava/lang/Object;
.source "NotificationSendigInfo.java"


# instance fields
.field private mDLMessage:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;

.field private mSendingType:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "ntype"    # I
    .param p2, "phoneNumber"    # Ljava/lang/String;
    .param p3, "msgBody"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;->mSendingType:I

    .line 8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;->mDLMessage:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;

    .line 11
    iput p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;->mSendingType:I

    .line 12
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;

    invoke-direct {v0, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;->mDLMessage:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;

    .line 14
    return-void
.end method


# virtual methods
.method public getDLMessage()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;->mDLMessage:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;

    return-object v0
.end method

.method public getSendingType()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;->mSendingType:I

    return v0
.end method
