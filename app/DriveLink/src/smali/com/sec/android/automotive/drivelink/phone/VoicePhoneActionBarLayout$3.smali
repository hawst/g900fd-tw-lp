.class Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$3;
.super Ljava/lang/Object;
.source "VoicePhoneActionBarLayout.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->startScaleAnimation(FJLandroid/view/animation/Interpolator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    .line 535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 539
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 543
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->isRestore:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->access$7(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->access$8(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;Z)V

    .line 545
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    const/high16 v1, 0x3f800000    # 1.0f

    const-wide/16 v2, 0x64

    # getter for: Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->SINE_OUT:Landroid/view/animation/Interpolator;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->access$9()Landroid/view/animation/Interpolator;

    move-result-object v4

    # invokes: Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->startScaleAnimation(FJLandroid/view/animation/Interpolator;)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->access$10(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;FJLandroid/view/animation/Interpolator;)V

    .line 549
    :goto_0
    return-void

    .line 547
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->access$8(Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;Z)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 554
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 559
    return-void
.end method
