.class public Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;
.super Lcom/google/android/gms/maps/SupportMapFragment;
.source "GoogleMapFragment.java"


# instance fields
.field private mAdapter:Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/android/gms/maps/SupportMapFragment;-><init>()V

    .line 13
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;)V
    .locals 0
    .param p1, "adapter"    # Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/gms/maps/SupportMapFragment;-><init>()V

    .line 16
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;->setAdapter(Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;)V

    .line 17
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 21
    invoke-super {p0, p1}, Lcom/google/android/gms/maps/SupportMapFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 22
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;->setRetainInstance(Z)V

    .line 24
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;->mAdapter:Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;

    if-nez v1, :cond_1

    .line 39
    :cond_0
    :goto_0
    return-void

    .line 28
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;->mAdapter:Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->setFragmentLoaded(Z)V

    .line 29
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;->getMap()Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v0

    .line 31
    .local v0, "map":Lcom/google/android/gms/maps/GoogleMap;
    if-eqz v0, :cond_0

    .line 35
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;->mAdapter:Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->setMap(Lcom/google/android/gms/maps/GoogleMap;)V

    .line 36
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;->mAdapter:Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->getMap()Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;->mAdapter:Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/GoogleMap;->setOnMarkerDragListener(Lcom/google/android/gms/maps/GoogleMap$OnMarkerDragListener;)V

    .line 37
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;->mAdapter:Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;->getMapCompletedListener()Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMapCreatedListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMapCreatedListener;->onMapCreated()V

    goto :goto_0
.end method

.method protected setAdapter(Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;)V
    .locals 0
    .param p1, "adapter"    # Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapFragment;->mAdapter:Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;

    .line 43
    return-void
.end method
