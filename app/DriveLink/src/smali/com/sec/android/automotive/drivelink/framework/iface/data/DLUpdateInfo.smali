.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLUpdateInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;
    }
.end annotation


# static fields
.field public static KEY_DOWNLOAD_VERSION:Ljava/lang/String;

.field public static KEY_INSTALL_RESULT:Ljava/lang/String;

.field public static KEY_IS_COMPLETED_DOWNLAOD:Ljava/lang/String;

.field public static KEY_IS_UPDATE_VERSION:Ljava/lang/String;

.field public static KEY_UPDATE_VERSION:Ljava/lang/String;

.field public static PREFERENCE_NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-string/jumbo v0, "drivelink"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->PREFERENCE_NAME:Ljava/lang/String;

    .line 60
    const-string/jumbo v0, "isUpdateVersion"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_IS_UPDATE_VERSION:Ljava/lang/String;

    .line 61
    const-string/jumbo v0, "UpdateVersionName"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_UPDATE_VERSION:Ljava/lang/String;

    .line 62
    const-string/jumbo v0, "DownloadVersion"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_DOWNLOAD_VERSION:Ljava/lang/String;

    .line 63
    const-string/jumbo v0, "isCompletedDownload"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_IS_COMPLETED_DOWNLAOD:Ljava/lang/String;

    .line 64
    const-string/jumbo v0, "installResult"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_INSTALL_RESULT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    return-void
.end method
