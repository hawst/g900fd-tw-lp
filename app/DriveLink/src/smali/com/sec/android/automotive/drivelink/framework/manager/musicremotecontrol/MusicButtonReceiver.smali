.class public Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MusicButtonReceiver.java"


# static fields
.field private static final ACTION_SAP_MEDIA_BUTTON:Ljava/lang/String; = "com.samsung.android.intent.action.MEDIA_BUTTON"

.field private static final REW_FF_TIME_INTERVAL:J = 0x12cL

.field private static mHandler:Landroid/os/Handler;

.field private static sLastHookClickTime:J

.field private static sLastsRewFFTimeClickTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 13
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->mHandler:Landroid/os/Handler;

    .line 14
    sput-wide v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sLastHookClickTime:J

    .line 15
    sput-wide v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sLastsRewFFTimeClickTime:J

    .line 18
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 22
    const-string/jumbo v0, "MY_TAG"

    .line 23
    const-string/jumbo v1, "[YANG] MusicButtonReceiver construct MusicButtonReceiver() "

    .line 22
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 24
    return-void
.end method

.method private sendMessage(IILjava/lang/Object;)V
    .locals 2
    .param p1, "notiType"    # I
    .param p2, "arg1"    # I
    .param p3, "data"    # Ljava/lang/Object;

    .prologue
    .line 111
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 112
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 113
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 114
    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 116
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 117
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 32
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->mHandler:Landroid/os/Handler;

    if-nez v5, :cond_1

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 36
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 40
    .local v1, "intentAction":Ljava/lang/String;
    const-string/jumbo v5, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 44
    const-string/jumbo v5, "MY_TAG"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "[YANG] MusicButtonReceiver() :: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 45
    const-string/jumbo v7, "handler : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 44
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    const-string/jumbo v5, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/view/KeyEvent;

    .line 48
    .local v0, "event":Landroid/view/KeyEvent;
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    .line 50
    .local v2, "keycode":I
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    if-nez v5, :cond_b

    .line 51
    const/16 v5, 0x57

    if-ne v2, v5, :cond_2

    .line 52
    const-string/jumbo v5, "TestApp"

    const-string/jumbo v6, "[YANG] Next Pressed"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    const/4 v5, 0x7

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sendMessage(IILjava/lang/Object;)V

    goto :goto_0

    .line 54
    :cond_2
    const/16 v5, 0x58

    if-ne v2, v5, :cond_3

    .line 55
    const-string/jumbo v5, "TestApp"

    const-string/jumbo v6, "[YANG] Previous pressed"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    const/4 v5, 0x6

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sendMessage(IILjava/lang/Object;)V

    goto :goto_0

    .line 57
    :cond_3
    const/16 v5, 0x4f

    if-ne v2, v5, :cond_5

    .line 58
    const-string/jumbo v5, "TestApp"

    .line 59
    const-string/jumbo v6, "[YANG] Head Set Hook pressed *********************************"

    .line 58
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v3

    .line 61
    .local v3, "time":J
    sget-wide v5, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sLastHookClickTime:J

    sub-long v5, v3, v5

    const-wide/16 v7, 0x12c

    cmp-long v5, v5, v7

    if-gez v5, :cond_4

    .line 64
    const/4 v5, 0x7

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sendMessage(IILjava/lang/Object;)V

    .line 65
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sendMessage(IILjava/lang/Object;)V

    .line 66
    const-wide/16 v5, 0x0

    sput-wide v5, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sLastHookClickTime:J

    goto/16 :goto_0

    .line 68
    :cond_4
    const/4 v5, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sendMessage(IILjava/lang/Object;)V

    .line 69
    sput-wide v3, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sLastHookClickTime:J

    goto/16 :goto_0

    .line 71
    .end local v3    # "time":J
    :cond_5
    const/16 v5, 0x56

    if-ne v2, v5, :cond_6

    .line 72
    const-string/jumbo v5, "TestApp"

    const-string/jumbo v6, "[YANG] KEYCODE_MEDIA_STOP"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    const/4 v5, 0x3

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_0

    .line 74
    :cond_6
    const/16 v5, 0x7e

    if-ne v2, v5, :cond_7

    .line 75
    const-string/jumbo v5, "TestApp"

    const-string/jumbo v6, "[YANG] KEYCODE_MEDIA_PLAY"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_0

    .line 77
    :cond_7
    const/16 v5, 0x7f

    if-ne v2, v5, :cond_8

    .line 78
    const-string/jumbo v5, "TestApp"

    const-string/jumbo v6, "[YANG] KEYCODE_MEDIA_PAUSE"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_0

    .line 80
    :cond_8
    const/16 v5, 0x55

    if-ne v2, v5, :cond_9

    .line 81
    const-string/jumbo v5, "TestApp"

    .line 82
    const-string/jumbo v6, "[YANG] KEYCODE_MEDIA_PLAY_PAUSE *********************************"

    .line 81
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    const/4 v5, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_0

    .line 84
    :cond_9
    const/16 v5, 0x59

    if-ne v2, v5, :cond_a

    .line 85
    const-string/jumbo v5, "TestApp"

    const-string/jumbo v6, "[YANG] KEYCODE_MEDIA_REWIND"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v3

    .line 87
    .restart local v3    # "time":J
    sget-wide v5, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sLastsRewFFTimeClickTime:J

    sub-long v5, v3, v5

    const-wide/16 v7, 0x12c

    cmp-long v5, v5, v7

    if-lez v5, :cond_0

    .line 88
    const/4 v5, 0x4

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sendMessage(IILjava/lang/Object;)V

    .line 89
    sput-wide v3, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sLastsRewFFTimeClickTime:J

    goto/16 :goto_0

    .line 91
    .end local v3    # "time":J
    :cond_a
    const/16 v5, 0x5a

    if-ne v2, v5, :cond_0

    .line 92
    const-string/jumbo v5, "TestApp"

    const-string/jumbo v6, "[YANG] KEYCODE_MEDIA_FAST_FORWARD"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v3

    .line 94
    .restart local v3    # "time":J
    sget-wide v5, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sLastsRewFFTimeClickTime:J

    sub-long v5, v3, v5

    const-wide/16 v7, 0x12c

    cmp-long v5, v5, v7

    if-lez v5, :cond_0

    .line 95
    const/4 v5, 0x5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sendMessage(IILjava/lang/Object;)V

    .line 96
    sput-wide v3, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sLastsRewFFTimeClickTime:J

    goto/16 :goto_0

    .line 99
    .end local v3    # "time":J
    :cond_b
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 100
    const/16 v5, 0x59

    if-ne v2, v5, :cond_c

    .line 101
    const-string/jumbo v5, "TestApp"

    const-string/jumbo v6, "[YANG] KEYCODE_MEDIA_REWIND"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const/4 v5, 0x4

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_0

    .line 103
    :cond_c
    const/16 v5, 0x5a

    if-ne v2, v5, :cond_0

    .line 104
    const-string/jumbo v5, "TestApp"

    const-string/jumbo v6, "[YANG] KEYCODE_MEDIA_FAST_FORWARD"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    const/4 v5, 0x5

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->sendMessage(IILjava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 27
    sput-object p1, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicButtonReceiver;->mHandler:Landroid/os/Handler;

    .line 28
    return-void
.end method
