.class Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$3;
.super Ljava/lang/Object;
.source "SettingsRejectMessageActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/16 v1, 0x50

    .line 96
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v2

    if-le v2, v1, :cond_0

    .line 98
    .local v1, "length":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    .line 99
    const/4 v4, 0x0

    invoke-interface {v3, v4, v1}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    .line 98
    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 100
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 102
    :try_start_0
    const-string/jumbo v2, "[SettingsRejectMessageActivity]"

    .line 103
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Length: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 104
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v4, v4, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v4

    .line 105
    const-string/jumbo v5, "KSC5601"

    invoke-virtual {v4, v5}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    array-length v4, v4

    .line 104
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 103
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 102
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    :goto_1
    return-void

    .line 97
    .end local v1    # "length":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    goto :goto_0

    .line 106
    .restart local v1    # "length":I
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1
.end method
