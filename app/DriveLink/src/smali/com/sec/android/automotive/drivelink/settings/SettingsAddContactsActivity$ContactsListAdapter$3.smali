.class Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;
.super Ljava/lang/Object;
.source "SettingsAddContactsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->buildItemRow(Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;Landroid/view/View;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

.field private final synthetic val$element:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;

.field private final synthetic val$holder:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->val$holder:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;

    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->val$element:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;

    .line 683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v9, 0x0

    const/4 v8, -0x1

    .line 686
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->val$holder:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;

    iget-object v4, v4, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->cbFavoriteContacts:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 689
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    move-result-object v4

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToSave:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$4(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    move-result-object v5

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mFavoriteList:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/2addr v4, v5

    .line 690
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    move-result-object v5

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToDelete:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 689
    sub-int/2addr v4, v5

    .line 690
    const/16 v5, 0xb

    if-le v4, v5, :cond_0

    .line 691
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->val$holder:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;

    iget-object v4, v4, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$ViewHolder;->cbFavoriteContacts:Landroid/widget/CheckBox;

    invoke-virtual {v4, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 732
    :goto_0
    return-void

    .line 694
    :cond_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->val$element:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;->getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v0

    .line 696
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    const/4 v2, 0x0

    .line 697
    .local v2, "inFavorites":Z
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    move-result-object v4

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mFavoriteList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v3, v4, :cond_4

    .line 704
    :goto_2
    if-nez v2, :cond_1

    .line 705
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    move-result-object v4

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToSave:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$4(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 707
    :cond_1
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    move-result-object v4

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToDelete:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 708
    .local v1, "i":I
    if-eq v1, v8, :cond_2

    .line 709
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    move-result-object v4

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToDelete:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 728
    :cond_2
    :goto_3
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    move-result-object v4

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToSave:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$4(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-gtz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    move-result-object v4

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToDelete:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_a

    .line 729
    :cond_3
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    move-result-object v4

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->btnDone:Landroid/widget/Button;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$5(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/Button;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 698
    .end local v1    # "i":I
    :cond_4
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    move-result-object v4

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mFavoriteList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v4

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->val$element:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;

    .line 699
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;->getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v6

    .line 698
    cmp-long v4, v4, v6

    if-nez v4, :cond_5

    .line 700
    const/4 v2, 0x1

    .line 701
    goto :goto_2

    .line 697
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 711
    .end local v0    # "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v2    # "inFavorites":Z
    .end local v3    # "j":I
    :cond_6
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->val$element:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;->getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v0

    .line 712
    .restart local v0    # "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    move-result-object v4

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToSave:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$4(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 713
    .restart local v1    # "i":I
    if-eq v1, v8, :cond_7

    .line 714
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    move-result-object v4

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToSave:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$4(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 716
    :cond_7
    const/4 v2, 0x0

    .line 717
    .restart local v2    # "inFavorites":Z
    const/4 v3, 0x0

    .restart local v3    # "j":I
    :goto_4
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    move-result-object v4

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mFavoriteList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v3, v4, :cond_8

    .line 724
    :goto_5
    if-eqz v2, :cond_2

    .line 725
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    move-result-object v4

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToDelete:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 718
    :cond_8
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    move-result-object v4

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mFavoriteList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v4

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->val$element:Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;

    .line 719
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/settings/SettingsContactsElement;->getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v6

    .line 718
    cmp-long v4, v4, v6

    if-nez v4, :cond_9

    .line 720
    const/4 v2, 0x1

    .line 721
    goto :goto_5

    .line 717
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 731
    :cond_a
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter$3;->this$1:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;->access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    move-result-object v4

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->btnDone:Landroid/widget/Button;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$5(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/Button;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0
.end method
