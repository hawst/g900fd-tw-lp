.class Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$1;
.super Ljava/lang/Object;
.source "DLLightSensorManager.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 161
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 146
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;

    iget-boolean v3, v3, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mLightSensorEnabled:Z

    if-eqz v3, :cond_1

    .line 147
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 148
    .local v1, "time":J
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x0

    aget v0, v3, v4

    .line 150
    .local v0, "lux":F
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->mRecentLightSamples:I
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->access$0(Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;)I

    move-result v3

    if-nez v3, :cond_0

    .line 151
    const-string/jumbo v3, "DLLightSensorManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "onSensorChanged : 1st lux : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    :cond_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;

    # invokes: Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->handleLightSensorEvent(JF)V
    invoke-static {v3, v1, v2, v0}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->access$1(Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;JF)V

    .line 156
    .end local v0    # "lux":F
    .end local v1    # "time":J
    :cond_1
    return-void
.end method
