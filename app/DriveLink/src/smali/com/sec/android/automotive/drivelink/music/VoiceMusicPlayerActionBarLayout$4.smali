.class Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$4;
.super Ljava/lang/Object;
.source "VoiceMusicPlayerActionBarLayout.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->runTTSBarAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$4;->this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    .line 511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 535
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$4;->this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->access$17(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;Z)V

    .line 536
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 527
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$4;->this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->isRunning:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->access$13(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$4;->this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$4;->this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->mDirection:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->access$14(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->access$15(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;Z)V

    .line 529
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout$4;->this$0:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    # invokes: Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->runTTSBarAnimation()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->access$16(Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;)V

    .line 531
    :cond_0
    return-void

    .line 528
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 523
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 517
    return-void
.end method
