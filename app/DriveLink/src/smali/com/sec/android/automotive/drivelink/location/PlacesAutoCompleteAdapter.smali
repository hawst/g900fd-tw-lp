.class public Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;
.super Landroid/widget/ArrayAdapter;
.source "PlacesAutoCompleteAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Landroid/widget/Filterable;"
    }
.end annotation


# instance fields
.field private final data:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field filter:Landroid/widget/Filter;

.field final placeService:Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;

.field private resultList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textViewResourceId"    # I

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->data:Ljava/util/Map;

    .line 47
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/places/PlaceServiceFactory;->getPlaceService()Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->placeService:Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->filter:Landroid/widget/Filter;

    .line 62
    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->resultList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;)Ljava/util/List;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->resultList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->data:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public applySearch(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->filter:Landroid/widget/Filter;

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 153
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->resultList:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->resultList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 94
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter$1;-><init>(Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->filter:Landroid/widget/Filter;

    .line 136
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->filter:Landroid/widget/Filter;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->resultList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->resultList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le p1, v0, :cond_1

    .line 82
    :cond_0
    const-string/jumbo v0, ""

    .line 84
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->resultList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getReference(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->data:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->data:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 147
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method
