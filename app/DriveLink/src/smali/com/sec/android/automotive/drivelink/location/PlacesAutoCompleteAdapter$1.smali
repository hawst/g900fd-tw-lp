.class Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter$1;
.super Landroid/widget/Filter;
.source "PlacesAutoCompleteAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->getFilter()Landroid/widget/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;

    .line 94
    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 7
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 98
    new-instance v0, Landroid/widget/Filter$FilterResults;

    invoke-direct {v0}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 100
    .local v0, "filterResults":Landroid/widget/Filter$FilterResults;
    if-eqz p1, :cond_0

    .line 102
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->placeService:Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;

    .line 103
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;->autocomplete(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 105
    .local v2, "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 106
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->access$1(Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;Ljava/util/List;)V

    .line 108
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 118
    :goto_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->resultList:Ljava/util/List;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->access$2(Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 119
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->resultList:Ljava/util/List;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->access$2(Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;)Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_3

    const/4 v3, 0x0

    :goto_2
    iput v3, v0, Landroid/widget/Filter$FilterResults;->count:I

    .line 123
    .end local v2    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :cond_0
    return-object v0

    .line 108
    .restart local v2    # "places":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;

    .line 109
    .local v1, "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->resultList:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->access$2(Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->data:Ljava/util/Map;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->access$3(Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;)Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->getReference()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 114
    .end local v1    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->access$1(Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;Ljava/util/List;)V

    .line 115
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->data:Ljava/util/Map;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->access$3(Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    goto :goto_1

    .line 119
    :cond_3
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;

    # getter for: Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->resultList:Ljava/util/List;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->access$2(Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;)Ljava/util/List;

    move-result-object v3

    .line 120
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    goto :goto_2
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 1
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .param p2, "results"    # Landroid/widget/Filter$FilterResults;

    .prologue
    .line 129
    if-eqz p2, :cond_0

    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->notifyDataSetChanged()V

    .line 134
    :goto_0
    return-void

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter$1;->this$0:Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/PlacesAutoCompleteAdapter;->notifyDataSetInvalidated()V

    goto :goto_0
.end method
