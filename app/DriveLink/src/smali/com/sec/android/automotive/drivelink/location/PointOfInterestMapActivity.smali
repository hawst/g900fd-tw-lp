.class public Lcom/sec/android/automotive/drivelink/location/PointOfInterestMapActivity;
.super Ljava/lang/Object;
.source "PointOfInterestMapActivity.java"


# static fields
.field public static final POI_STATE_NO_POI_FOUND:Ljava/lang/String; = "poi_state_no_poi_found"

.field public static final POI_TYPE:Ljava/lang/String; = "poi_type"

.field public static final POI_TYPE_GAS:Ljava/lang/String; = "poi_gas"

.field public static final POI_TYPE_PARKING:Ljava/lang/String; = "poi_parking"

.field public static final STATE_LATITUDE:Ljava/lang/String; = "latitude"

.field public static final STATE_LONGITUDE:Ljava/lang/String; = "longitude"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
