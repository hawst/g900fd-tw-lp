.class Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$3;
.super Ljava/lang/Object;
.source "FindMyCarMapViewActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->createView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    .line 261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mMyLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->access$12(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v1

    .line 265
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mMyLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->access$12(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v3

    .line 264
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->setZoomMark(DD)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->refreshView()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->access$13(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V

    .line 267
    return-void
.end method
