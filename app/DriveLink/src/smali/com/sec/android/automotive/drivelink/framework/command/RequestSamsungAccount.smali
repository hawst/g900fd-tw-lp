.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestSamsungAccount.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mData:Landroid/os/Bundle;

.field private mRequestID:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;ILandroid/os/Bundle;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "requestID"    # I
    .param p4, "data"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 17
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 10
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mContext:Landroid/content/Context;

    .line 11
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mRequestID:I

    .line 12
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mData:Landroid/os/Bundle;

    .line 19
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mContext:Landroid/content/Context;

    .line 20
    iput p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mRequestID:I

    .line 21
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mData:Landroid/os/Bundle;

    .line 22
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 85
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mContext:Landroid/content/Context;

    .line 86
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mData:Landroid/os/Bundle;

    .line 87
    return-void
.end method

.method protected process()V
    .locals 6

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v4

    .line 27
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getSamsungAccountManager()Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    move-result-object v3

    .line 29
    .local v3, "samsungAccountManager":Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;
    iget v4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mRequestID:I

    packed-switch v4, :pswitch_data_0

    .line 76
    :goto_0
    return-void

    .line 31
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountConnectService(Landroid/content/Context;)V

    goto :goto_0

    .line 35
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountDisconnectService(Landroid/content/Context;)V

    goto :goto_0

    .line 38
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mData:Landroid/os/Bundle;

    .line 39
    const-string/jumbo v5, "clientID"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "clientID":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mData:Landroid/os/Bundle;

    .line 41
    const-string/jumbo v5, "clienSecret"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 42
    .local v1, "clientSecret":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mData:Landroid/os/Bundle;

    .line 43
    const-string/jumbo v5, "packageName"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 45
    .local v2, "packageName":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mContext:Landroid/content/Context;

    .line 44
    invoke-virtual {v3, v4, v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountRegisterCallback(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 49
    .end local v0    # "clientID":Ljava/lang/String;
    .end local v1    # "clientSecret":Ljava/lang/String;
    .end local v2    # "packageName":Ljava/lang/String;
    :pswitch_3
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountUnregisterCallback(Landroid/content/Context;)V

    goto :goto_0

    .line 52
    :pswitch_4
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mContext:Landroid/content/Context;

    .line 53
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mData:Landroid/os/Bundle;

    .line 52
    invoke-virtual {v3, v4, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountAccessToken(Landroid/content/Context;Landroid/os/Bundle;)Z

    goto :goto_0

    .line 57
    :pswitch_5
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mData:Landroid/os/Bundle;

    .line 56
    invoke-virtual {v3, v4, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountDisclaimerAgreement(Landroid/content/Context;Landroid/os/Bundle;)Z

    goto :goto_0

    .line 61
    :pswitch_6
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mData:Landroid/os/Bundle;

    .line 60
    invoke-virtual {v3, v4, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountChecklistValidation(Landroid/content/Context;Landroid/os/Bundle;)Z

    goto :goto_0

    .line 65
    :pswitch_7
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mData:Landroid/os/Bundle;

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountAuthCode(Landroid/content/Context;Landroid/os/Bundle;)Z

    goto :goto_0

    .line 69
    :pswitch_8
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mData:Landroid/os/Bundle;

    .line 68
    invoke-virtual {v3, v4, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountSCloudAccessToken(Landroid/content/Context;Landroid/os/Bundle;)Z

    goto :goto_0

    .line 73
    :pswitch_9
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSamsungAccount;->mData:Landroid/os/Bundle;

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;->requestSamsungAccountUserName(Landroid/content/Context;Landroid/os/Bundle;)Z

    goto :goto_0

    .line 29
    nop

    :pswitch_data_0
    .packed-switch 0xace
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method protected response()V
    .locals 0

    .prologue
    .line 81
    return-void
.end method
