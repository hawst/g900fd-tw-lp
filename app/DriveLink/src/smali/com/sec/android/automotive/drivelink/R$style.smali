.class public final Lcom/sec/android/automotive/drivelink/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final AppBaseTheme:I = 0x7f0c0000

.field public static final AppComposerTheme:I = 0x7f0c0002

.field public static final AppTheme:I = 0x7f0c0001

.field public static final BasicDialogButton:I = 0x7f0c000b

.field public static final BasicDialogButtonBar:I = 0x7f0c0009

.field public static final BasicDialogButtonBarSeparator:I = 0x7f0c000a

.field public static final BasicDialogContent:I = 0x7f0c0006

.field public static final BasicDialogShadow:I = 0x7f0c0008

.field public static final BasicDialogTextContent:I = 0x7f0c0007

.field public static final BasicDialogTitleBar:I = 0x7f0c0004

.field public static final BasicDialogTitleText:I = 0x7f0c0005

.field public static final Button_Basic_Style:I = 0x7f0c004c

.field public static final CommomBackgroundNavigation:I = 0x7f0c000f

.field public static final CommonButton:I = 0x7f0c000c

.field public static final CommonButtonIconText:I = 0x7f0c000d

.field public static final CommonButtonNavigation:I = 0x7f0c000e

.field public static final CommonInputField:I = 0x7f0c00a4

.field public static final CommonSearchField:I = 0x7f0c00a5

.field public static final LocationButtonLand:I = 0x7f0c0010

.field public static final NoSearchResults:I = 0x7f0c00ac

.field public static final RegisterNextButton:I = 0x7f0c00ab

.field public static final RobotoBold_17:I = 0x7f0c0090

.field public static final RobotoBold_19:I = 0x7f0c0091

.field public static final RobotoBold_20:I = 0x7f0c0092

.field public static final RobotoBold_28:I = 0x7f0c0093

.field public static final RobotoBold_30:I = 0x7f0c0094

.field public static final RobotoBold_37:I = 0x7f0c0095

.field public static final RobotoBold_44:I = 0x7f0c0096

.field public static final RobotoLight:I = 0x7f0c0013

.field public static final RobotoLightBold:I = 0x7f0c002b

.field public static final RobotoLightBold_21:I = 0x7f0c002c

.field public static final RobotoLightBold_23:I = 0x7f0c002d

.field public static final RobotoLightBold_Shadow_30:I = 0x7f0c002e

.field public static final RobotoLight_12:I = 0x7f0c0019

.field public static final RobotoLight_14:I = 0x7f0c001a

.field public static final RobotoLight_15:I = 0x7f0c001b

.field public static final RobotoLight_16:I = 0x7f0c001c

.field public static final RobotoLight_17:I = 0x7f0c001d

.field public static final RobotoLight_18:I = 0x7f0c001e

.field public static final RobotoLight_19:I = 0x7f0c001f

.field public static final RobotoLight_19_ContentList_DefaultButton:I = 0x7f0c00ad

.field public static final RobotoLight_20:I = 0x7f0c0020

.field public static final RobotoLight_21:I = 0x7f0c0021

.field public static final RobotoLight_22:I = 0x7f0c0022

.field public static final RobotoLight_22_ShadowBlack2px70:I = 0x7f0c00ae

.field public static final RobotoLight_23:I = 0x7f0c0023

.field public static final RobotoLight_23_ShadowBlack2px70:I = 0x7f0c00af

.field public static final RobotoLight_24:I = 0x7f0c0024

.field public static final RobotoLight_24_ShadowBlack2px70:I = 0x7f0c00b0

.field public static final RobotoLight_25:I = 0x7f0c0025

.field public static final RobotoLight_25_ShadowBlack2px70:I = 0x7f0c00b1

.field public static final RobotoLight_25_messageComposerButton:I = 0x7f0c00b4

.field public static final RobotoLight_25_messageComposerButton_Quotes:I = 0x7f0c00b5

.field public static final RobotoLight_26:I = 0x7f0c0026

.field public static final RobotoLight_26_ShadowBlack2px70:I = 0x7f0c00b2

.field public static final RobotoLight_27:I = 0x7f0c0027

.field public static final RobotoLight_27_ShadowBlack2px70:I = 0x7f0c00b3

.field public static final RobotoLight_28:I = 0x7f0c0028

.field public static final RobotoLight_29:I = 0x7f0c0029

.field public static final RobotoLight_30:I = 0x7f0c002a

.field public static final RobotoLight_30_messageComposerButton:I = 0x7f0c00b6

.field public static final RobotoLight_30_messageComposerButton_Quotes:I = 0x7f0c00b7

.field public static final RobotoLight_31:I = 0x7f0c002f

.field public static final RobotoLight_32:I = 0x7f0c0030

.field public static final RobotoLight_33:I = 0x7f0c0031

.field public static final RobotoLight_34:I = 0x7f0c0032

.field public static final RobotoLight_35:I = 0x7f0c0033

.field public static final RobotoLight_36:I = 0x7f0c0034

.field public static final RobotoLight_37:I = 0x7f0c003f

.field public static final RobotoLight_40:I = 0x7f0c0035

.field public static final RobotoLight_41:I = 0x7f0c0036

.field public static final RobotoLight_42:I = 0x7f0c0037

.field public static final RobotoLight_43:I = 0x7f0c0038

.field public static final RobotoLight_44:I = 0x7f0c0039

.field public static final RobotoLight_45:I = 0x7f0c003a

.field public static final RobotoLight_46:I = 0x7f0c003b

.field public static final RobotoLight_47:I = 0x7f0c003c

.field public static final RobotoLight_48:I = 0x7f0c003d

.field public static final RobotoLight_65:I = 0x7f0c003e

.field public static final RobotoLight_ShadowBlack1px70:I = 0x7f0c0016

.field public static final RobotoLight_ShadowBlack2px70:I = 0x7f0c0015

.field public static final RobotoLight_ShadowBlack3px23:I = 0x7f0c0018

.field public static final RobotoLight_ShadowBlack3px70:I = 0x7f0c0017

.field public static final RobotoLight_ShadowWhite2px60:I = 0x7f0c0014

.field public static final RobotoLight_Shadow_1px_30:I = 0x7f0c004b

.field public static final RobotoLight_Shadow_1px_30_bt:I = 0x7f0c00bc

.field public static final RobotoLight_Shadow_1px_30_btQuotes:I = 0x7f0c00bb

.field public static final RobotoLight_Shadow_20:I = 0x7f0c0040

.field public static final RobotoLight_Shadow_21:I = 0x7f0c0041

.field public static final RobotoLight_Shadow_22:I = 0x7f0c0042

.field public static final RobotoLight_Shadow_23:I = 0x7f0c0043

.field public static final RobotoLight_Shadow_24:I = 0x7f0c0044

.field public static final RobotoLight_Shadow_25:I = 0x7f0c0045

.field public static final RobotoLight_Shadow_26:I = 0x7f0c0046

.field public static final RobotoLight_Shadow_27:I = 0x7f0c0047

.field public static final RobotoLight_Shadow_28:I = 0x7f0c0048

.field public static final RobotoLight_Shadow_29:I = 0x7f0c0049

.field public static final RobotoLight_Shadow_30:I = 0x7f0c004a

.field public static final RobotoLight_Shadow_31:I = 0x7f0c004d

.field public static final RobotoLight_Shadow_33:I = 0x7f0c004e

.field public static final RobotoLight_Shadow_36:I = 0x7f0c004f

.field public static final RobotoLight_Shadow_38:I = 0x7f0c0050

.field public static final RobotoLight_Shadow_40:I = 0x7f0c0051

.field public static final RobotoLight_Shadow_43:I = 0x7f0c0052

.field public static final RobotoRegular:I = 0x7f0c0053

.field public static final RobotoRegular_10:I = 0x7f0c0057

.field public static final RobotoRegular_13:I = 0x7f0c0058

.field public static final RobotoRegular_14:I = 0x7f0c0059

.field public static final RobotoRegular_15:I = 0x7f0c005a

.field public static final RobotoRegular_16:I = 0x7f0c005b

.field public static final RobotoRegular_17:I = 0x7f0c005c

.field public static final RobotoRegular_18:I = 0x7f0c005d

.field public static final RobotoRegular_19:I = 0x7f0c005e

.field public static final RobotoRegular_20:I = 0x7f0c005f

.field public static final RobotoRegular_21:I = 0x7f0c0060

.field public static final RobotoRegular_22:I = 0x7f0c0061

.field public static final RobotoRegular_23:I = 0x7f0c0062

.field public static final RobotoRegular_24:I = 0x7f0c0063

.field public static final RobotoRegular_25:I = 0x7f0c0064

.field public static final RobotoRegular_26:I = 0x7f0c0065

.field public static final RobotoRegular_27:I = 0x7f0c0066

.field public static final RobotoRegular_28:I = 0x7f0c0067

.field public static final RobotoRegular_29:I = 0x7f0c0068

.field public static final RobotoRegular_30:I = 0x7f0c0069

.field public static final RobotoRegular_31:I = 0x7f0c006a

.field public static final RobotoRegular_32:I = 0x7f0c006b

.field public static final RobotoRegular_33:I = 0x7f0c006c

.field public static final RobotoRegular_34:I = 0x7f0c006d

.field public static final RobotoRegular_35:I = 0x7f0c006e

.field public static final RobotoRegular_36:I = 0x7f0c006f

.field public static final RobotoRegular_37:I = 0x7f0c0070

.field public static final RobotoRegular_38:I = 0x7f0c0071

.field public static final RobotoRegular_39:I = 0x7f0c0072

.field public static final RobotoRegular_40:I = 0x7f0c0073

.field public static final RobotoRegular_41:I = 0x7f0c0074

.field public static final RobotoRegular_42:I = 0x7f0c0075

.field public static final RobotoRegular_43:I = 0x7f0c0076

.field public static final RobotoRegular_44:I = 0x7f0c0077

.field public static final RobotoRegular_45:I = 0x7f0c0078

.field public static final RobotoRegular_46:I = 0x7f0c0079

.field public static final RobotoRegular_47:I = 0x7f0c007a

.field public static final RobotoRegular_48:I = 0x7f0c007b

.field public static final RobotoRegular_53:I = 0x7f0c007c

.field public static final RobotoRegular_65:I = 0x7f0c007d

.field public static final RobotoRegular_Shadow1px_25:I = 0x7f0c007e

.field public static final RobotoRegular_ShadowBlack1px70:I = 0x7f0c0054

.field public static final RobotoRegular_ShadowBlack2px70:I = 0x7f0c0055

.field public static final RobotoRegular_ShadowBlack3px70:I = 0x7f0c0056

.field public static final RobotoRegular_Shadow_18:I = 0x7f0c0080

.field public static final RobotoRegular_Shadow_20:I = 0x7f0c0081

.field public static final RobotoRegular_Shadow_21:I = 0x7f0c0082

.field public static final RobotoRegular_Shadow_23:I = 0x7f0c0083

.field public static final RobotoRegular_Shadow_24:I = 0x7f0c0084

.field public static final RobotoRegular_Shadow_26:I = 0x7f0c007f

.field public static final RobotoRegular_Shadow_27:I = 0x7f0c0085

.field public static final RobotoRegular_Shadow_28:I = 0x7f0c0086

.field public static final RobotoRegular_Shadow_29:I = 0x7f0c0087

.field public static final RobotoRegular_Shadow_30:I = 0x7f0c0088

.field public static final RobotoRegular_Shadow_32:I = 0x7f0c0089

.field public static final RobotoRegular_Shadow_33:I = 0x7f0c008a

.field public static final RobotoRegular_Shadow_37:I = 0x7f0c008b

.field public static final RobotoRegular_Shadow_40:I = 0x7f0c008c

.field public static final RobotoRegular_Shadow_41:I = 0x7f0c008d

.field public static final RobotoRegular_Shadow_43:I = 0x7f0c008e

.field public static final RobotoRegular_Shadow_60:I = 0x7f0c008f

.field public static final SettingsCompactLinearLayout:I = 0x7f0c009d

.field public static final SettingsDefaultButton:I = 0x7f0c00a3

.field public static final SettingsListFeeder:I = 0x7f0c00a6

.field public static final SettingsNextButton:I = 0x7f0c0099

.field public static final SettingsNextText:I = 0x7f0c0097

.field public static final SettingsPreferenceCategory_Landscape:I = 0x7f0c009a

.field public static final SettingsPreferenceCategory_Portrait:I = 0x7f0c009b

.field public static final SettingsRegisterButton:I = 0x7f0c00a0

.field public static final SettingsRejectButton:I = 0x7f0c00a1

.field public static final SettingsRejectButtonOK:I = 0x7f0c00a2

.field public static final SettingsTitleBackButton:I = 0x7f0c009e

.field public static final SettingsTitleLinearLayout:I = 0x7f0c009c

.field public static final SettingsTitleTextWithBackButton:I = 0x7f0c009f

.field public static final StyleButtonReinivite:I = 0x7f0c00aa

.field public static final StyleTimeAmPmInfo_RobotoRegular13:I = 0x7f0c00ba

.field public static final StyleTimeInfo_RobotoLight14:I = 0x7f0c00b9

.field public static final TextMask:I = 0x7f0c0012

.field public static final Theme_Transparent:I = 0x7f0c0011

.field public static final TitleNavigation:I = 0x7f0c00a9

.field public static final TransparentExitPopup:I = 0x7f0c00b8

.field public static final WelcomeNextText:I = 0x7f0c0098

.field public static final full_screen_dialog:I = 0x7f0c0003

.field public static final list_view_text:I = 0x7f0c00a7

.field public static final list_view_text_header:I = 0x7f0c00a8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
