.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;
.source "NotificationAlarmDialogView.java"


# static fields
.field private static final ALARM_ALERT_DURATION:I = 0x32

.field private static final ALARM_ID:Ljava/lang/String; = "_id"

.field private static final ALARM_STOP:Ljava/lang/String; = "com.samsung.sec.android.clockpackage.DIRECT_ALARM_STOP"

.field private static final BDISMISS:Ljava/lang/String; = "bDismiss"

.field protected static final TAG:Ljava/lang/String; = "[NotificationAlarmView]"


# instance fields
.field bButtonClicked:Z

.field private bListen:Z

.field bUse24HourFormat:Z

.field private mClickListener:Landroid/view/View$OnClickListener;

.field mTvAlarmTitle:Landroid/widget/TextView;

.field mTvAmPm:Landroid/widget/TextView;

.field mTvTime:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 46
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 25
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvTime:Landroid/widget/TextView;

    .line 26
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAmPm:Landroid/widget/TextView;

    .line 28
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAlarmTitle:Landroid/widget/TextView;

    .line 29
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->bButtonClicked:Z

    .line 30
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->bUse24HourFormat:Z

    .line 215
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mClickListener:Landroid/view/View$OnClickListener;

    .line 349
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->bListen:Z

    .line 47
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->initSubClassNotification(Landroid/content/Context;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 40
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 25
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvTime:Landroid/widget/TextView;

    .line 26
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAmPm:Landroid/widget/TextView;

    .line 28
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAlarmTitle:Landroid/widget/TextView;

    .line 29
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->bButtonClicked:Z

    .line 30
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->bUse24HourFormat:Z

    .line 215
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mClickListener:Landroid/view/View$OnClickListener;

    .line 349
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->bListen:Z

    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->initSubClassNotification(Landroid/content/Context;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 25
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvTime:Landroid/widget/TextView;

    .line 26
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAmPm:Landroid/widget/TextView;

    .line 28
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAlarmTitle:Landroid/widget/TextView;

    .line 29
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->bButtonClicked:Z

    .line 30
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->bUse24HourFormat:Z

    .line 215
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mClickListener:Landroid/view/View$OnClickListener;

    .line 349
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->bListen:Z

    .line 35
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->initSubClassNotification(Landroid/content/Context;)V

    .line 36
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;IZ)V
    .locals 0

    .prologue
    .line 206
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->alarmStop(IZ)V

    return-void
.end method

.method private alarmStop(IZ)V
    .locals 4
    .param p1, "id"    # I
    .param p2, "bStop"    # Z

    .prologue
    .line 207
    const-string/jumbo v1, "[NotificationAlarmView]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Alarm Stop called. Id:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", isStop:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 209
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.samsung.sec.android.clockpackage.DIRECT_ALARM_STOP"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 210
    const-string/jumbo v1, "bDismiss"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 211
    const-string/jumbo v1, "_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 212
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 213
    return-void
.end method

.method private initSubClassNotification(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    const-string/jumbo v4, "[NotificationAlarmView]"

    const-string/jumbo v5, "initSubClassNotification"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 53
    const-string/jumbo v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 52
    check-cast v1, Landroid/view/LayoutInflater;

    .line 55
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const/4 v2, 0x0

    .line 56
    .local v2, "layout":I
    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->isMultiMode:Z

    if-eqz v4, :cond_1

    .line 57
    const v2, 0x7f03009c

    .line 61
    :goto_0
    const/4 v4, 0x0

    invoke-virtual {v1, v2, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 62
    .local v3, "view":Landroid/view/View;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mLayoutContent:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 65
    invoke-static {p1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v4

    .line 64
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->bUse24HourFormat:Z

    .line 67
    const v4, 0x7f0902a4

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvTime:Landroid/widget/TextView;

    .line 68
    const v4, 0x7f0902a5

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAmPm:Landroid/widget/TextView;

    .line 71
    const v4, 0x7f0902a7

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAlarmTitle:Landroid/widget/TextView;

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 74
    const-class v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;

    .line 76
    .local v0, "alarmInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;->getAlertTime()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;->getSNZActive()Z

    move-result v6

    .line 77
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;->getAlarmName()Ljava/lang/String;

    move-result-object v7

    .line 76
    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->setAlarmInfo(JZLjava/lang/String;)V

    .line 80
    .end local v0    # "alarmInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;
    :cond_0
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->setUseAutoTimeout(Z)V

    .line 82
    return-void

    .line 59
    .end local v3    # "view":Landroid/view/View;
    :cond_1
    const v2, 0x7f03009b

    goto :goto_0
.end method


# virtual methods
.method protected getTTSText()Ljava/lang/String;
    .locals 10

    .prologue
    const v9, 0x7f0a01fc

    const v8, 0x7f0a01a9

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 263
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 264
    .local v2, "tts":Ljava/lang/StringBuilder;
    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->bUse24HourFormat:Z

    if-eqz v3, :cond_2

    .line 272
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvTime:Landroid/widget/TextView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvTime:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 274
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvTime:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 275
    .local v0, "time":Ljava/lang/String;
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 278
    .local v1, "timeString":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAlarmTitle:Landroid/widget/TextView;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAlarmTitle:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 279
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 280
    new-array v4, v5, [Ljava/lang/Object;

    .line 281
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAlarmTitle:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v0, v4, v7

    .line 279
    invoke-virtual {v3, v8, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 289
    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 322
    .end local v0    # "time":Ljava/lang/String;
    .end local v1    # "timeString":Ljava/lang/String;
    :cond_0
    :goto_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 283
    .restart local v0    # "time":Ljava/lang/String;
    .restart local v1    # "timeString":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 284
    new-array v4, v7, [Ljava/lang/Object;

    aput-object v0, v4, v6

    .line 283
    invoke-virtual {v3, v9, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 293
    .end local v0    # "time":Ljava/lang/String;
    .end local v1    # "timeString":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvTime:Landroid/widget/TextView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvTime:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAmPm:Landroid/widget/TextView;

    if-eqz v3, :cond_0

    .line 294
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAmPm:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 296
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvTime:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 297
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAmPm:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 296
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 298
    .restart local v0    # "time":Ljava/lang/String;
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 301
    .restart local v1    # "timeString":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAlarmTitle:Landroid/widget/TextView;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAlarmTitle:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 302
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 303
    new-array v4, v5, [Ljava/lang/Object;

    .line 304
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAlarmTitle:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v0, v4, v7

    .line 302
    invoke-virtual {v3, v8, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 312
    :goto_2
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 306
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 307
    new-array v4, v7, [Ljava/lang/Object;

    aput-object v0, v4, v6

    .line 306
    invoke-virtual {v3, v9, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method public onNotiCommandFinished(I)V
    .locals 4
    .param p1, "cmd"    # I

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 354
    const/4 v0, 0x6

    if-ne p1, v0, :cond_1

    .line 355
    new-array v0, v1, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mButton1:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mButton2:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->setButtonQuoted(Z[Landroid/widget/Button;)V

    .line 356
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->bListen:Z

    .line 363
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->onNotiCommandFinished(I)V

    .line 364
    return-void

    .line 358
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->bListen:Z

    if-eqz v0, :cond_0

    .line 359
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->bListen:Z

    .line 360
    new-array v0, v1, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mButton1:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mButton2:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->setButtonQuoted(Z[Landroid/widget/Button;)V

    goto :goto_0
.end method

.method public onNotificationWillDisappear()V
    .locals 3

    .prologue
    .line 369
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->hasNotiItemInfo()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 370
    const-class v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 371
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->bButtonClicked:Z

    if-nez v1, :cond_0

    .line 372
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;

    .line 373
    .local v0, "alarmInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;->getSNZActive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 374
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;->getAlarmId()I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->alarmStop(IZ)V

    .line 380
    .end local v0    # "alarmInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->onNotificationWillDisappear()V

    .line 381
    return-void

    .line 376
    .restart local v0    # "alarmInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;->getAlarmId()I

    move-result v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->alarmStop(IZ)V

    goto :goto_0
.end method

.method public setAlarmInfo(JZLjava/lang/String;)V
    .locals 10
    .param p1, "time"    # J
    .param p3, "repeat"    # Z
    .param p4, "alarmType"    # Ljava/lang/String;

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getBaseTag()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getBaseTag()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "port"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 123
    const v7, 0x7f0202b8

    invoke-virtual {p0, v7}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->setIcon(I)Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;

    .line 124
    const-string/jumbo v7, "[NotificationAlarmView]"

    const-string/jumbo v8, "Portrait Icon !!"

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    :goto_0
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mLayoutContent:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-ne v7, v8, :cond_0

    .line 131
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mLayoutContent:Landroid/widget/LinearLayout;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 134
    :cond_0
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 135
    .local v4, "date":Ljava/util/Date;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 136
    .local v3, "cal":Ljava/util/Calendar;
    invoke-virtual {v3, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 138
    const-string/jumbo v2, " "

    .line 139
    .local v2, "ampm":Ljava/lang/String;
    const-string/jumbo v6, " "

    .line 141
    .local v6, "strTime":Ljava/lang/String;
    iget-boolean v7, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->bUse24HourFormat:Z

    if-eqz v7, :cond_3

    .line 142
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string/jumbo v7, "HH:mm"

    invoke-direct {v5, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 143
    .local v5, "sdfTime":Ljava/text/SimpleDateFormat;
    invoke-virtual {v5, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    .line 145
    const-string/jumbo v7, "[NotificationAlarmView]"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "long time:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", Time(24H):"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :goto_1
    if-eqz v6, :cond_6

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_6

    .line 165
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvTime:Landroid/widget/TextView;

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    :goto_2
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_7

    .line 170
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAmPm:Landroid/widget/TextView;

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    :goto_3
    if-eqz p4, :cond_8

    invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_8

    .line 182
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAlarmTitle:Landroid/widget/TextView;

    invoke-virtual {v7, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    :goto_4
    const v7, 0x7f0a037f

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->addButton(ILandroid/view/View$OnClickListener;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;

    .line 189
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->hasNotiItemInfo()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 190
    const-class v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;

    .line 192
    .local v0, "alarmInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;->getSNZActive()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 193
    const v7, 0x7f0a037e

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->addButton(ILandroid/view/View$OnClickListener;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;

    .line 200
    .end local v0    # "alarmInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;
    :cond_1
    :goto_5
    return-void

    .line 126
    .end local v2    # "ampm":Ljava/lang/String;
    .end local v3    # "cal":Ljava/util/Calendar;
    .end local v4    # "date":Ljava/util/Date;
    .end local v5    # "sdfTime":Ljava/text/SimpleDateFormat;
    .end local v6    # "strTime":Ljava/lang/String;
    :cond_2
    const v7, 0x7f0202b7

    invoke-virtual {p0, v7}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->setIcon(I)Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;

    .line 127
    const-string/jumbo v7, "[NotificationAlarmView]"

    const-string/jumbo v8, "Landscape Icon !!"

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 147
    .restart local v2    # "ampm":Ljava/lang/String;
    .restart local v3    # "cal":Ljava/util/Calendar;
    .restart local v4    # "date":Ljava/util/Date;
    .restart local v6    # "strTime":Ljava/lang/String;
    :cond_3
    const/16 v7, 0x9

    invoke-virtual {v3, v7}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 148
    .local v1, "am_pm":I
    if-nez v1, :cond_5

    .line 149
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 150
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 151
    const v9, 0x7f0a00e0

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 150
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 149
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 157
    :cond_4
    :goto_6
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string/jumbo v7, "hh:mm"

    invoke-direct {v5, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 158
    .restart local v5    # "sdfTime":Ljava/text/SimpleDateFormat;
    invoke-virtual {v5, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    .line 160
    const-string/jumbo v7, "[NotificationAlarmView]"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "long time:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", Time(12H):"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 161
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 160
    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 152
    .end local v5    # "sdfTime":Ljava/text/SimpleDateFormat;
    :cond_5
    const/4 v7, 0x1

    if-ne v1, v7, :cond_4

    .line 153
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 154
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 155
    const v9, 0x7f0a00e1

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 154
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 153
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_6

    .line 167
    .end local v1    # "am_pm":I
    .restart local v5    # "sdfTime":Ljava/text/SimpleDateFormat;
    :cond_6
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvTime:Landroid/widget/TextView;

    const-string/jumbo v8, ""

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 172
    :cond_7
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAmPm:Landroid/widget/TextView;

    const-string/jumbo v8, ""

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 184
    :cond_8
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mTvAlarmTitle:Landroid/widget/TextView;

    const-string/jumbo v8, ""

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 195
    .restart local v0    # "alarmInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;
    :cond_9
    const v7, 0x7f0a037e

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->mClickListener:Landroid/view/View$OnClickListener;

    .line 196
    const/4 v9, 0x0

    .line 195
    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->addButton(ILandroid/view/View$OnClickListener;Z)Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;

    goto/16 :goto_5
.end method

.method public setReadoutFlag(Landroid/content/Context;Z)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "flag"    # Z

    .prologue
    .line 117
    const/4 v0, 0x1

    invoke-super {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->setReadoutFlag(Landroid/content/Context;Z)Z

    move-result v0

    return v0
.end method

.method protected startDefaultNotiTimeoutFlow()V
    .locals 5

    .prologue
    const v4, 0x7f060001

    const/16 v3, 0x32

    const/4 v2, 0x5

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addBegin()V

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0, v4, v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addSound(II)V

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getTTSText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTS(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0, v4, v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addSound(II)V

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getTTSText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTS(Ljava/lang/String;)V

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addFinish()V

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationAlarmDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdFlowStart()V

    .line 101
    :cond_0
    return-void
.end method
