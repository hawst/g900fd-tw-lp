.class Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$9;
.super Ljava/lang/Object;
.source "SearchMusicListActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->getListItemOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    .line 468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 472
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$10(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    .line 473
    mul-int/lit8 v2, v2, 0x4

    .line 472
    add-int v0, v1, v2

    .line 474
    .local v0, "currentItem":I
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->driveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$13(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 475
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$1()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    move-result-object v3

    .line 476
    add-int/lit8 v4, v0, -0x1

    .line 474
    invoke-interface {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestMusicListFromSearchResult(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;I)V

    .line 478
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->imm:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$5(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$6(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 479
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$4(Z)V

    .line 480
    return-void
.end method
