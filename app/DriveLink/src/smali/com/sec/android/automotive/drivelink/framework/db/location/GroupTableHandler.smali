.class public Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;
.super Ljava/lang/Object;
.source "GroupTableHandler.java"


# static fields
.field private static final TABLE_PLACEON_GROUP:Ljava/lang/String; = "tb_location_group"

.field private static final TAG:Ljava/lang/String; = "PlaceOnGroupTableHandler"


# instance fields
.field private mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;)V
    .locals 0
    .param p1, "handler"    # Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 33
    return-void
.end method

.method private createPlaceOnGroup(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 10
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 154
    const-wide/16 v1, -0x1

    .line 156
    .local v1, "result":J
    :try_start_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 157
    .local v3, "time":Ljava/util/Calendar;
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 159
    .local v4, "timestamp":J
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 160
    .local v6, "values":Landroid/content/ContentValues;
    const-string/jumbo v7, "group_id"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 161
    const-string/jumbo v7, "group_name"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string/jumbo v7, "group_type"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;->getValue()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 163
    const-string/jumbo v7, "group_share_type"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getShareType()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 164
    const-string/jumbo v7, "timestamp"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 165
    const-string/jumbo v7, "expireTime"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getExpireTime()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 166
    const-string/jumbo v7, "url"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string/jumbo v7, "location_id"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const-string/jumbo v7, "tb_location_group"

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v1

    .line 176
    .end local v3    # "time":Ljava/util/Calendar;
    .end local v4    # "timestamp":J
    .end local v6    # "values":Landroid/content/ContentValues;
    :goto_0
    const-wide/16 v7, -0x1

    cmp-long v7, v1, v7

    if-nez v7, :cond_0

    const/4 v7, 0x0

    :goto_1
    return v7

    .line 170
    :catch_0
    move-exception v0

    .line 171
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 172
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 173
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0

    .line 176
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_0
    const/4 v7, 0x1

    goto :goto_1
.end method

.method private getGroupFromCursor(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 335
    const/4 v1, 0x0

    .line 336
    .local v1, "i":I
    const/4 v3, 0x0

    .line 337
    .local v3, "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    const/4 v4, 0x0

    .line 340
    .local v4, "locationId":Ljava/lang/String;
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;-><init>()V

    .line 341
    .local v0, "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setGroupId(I)V

    .line 342
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setGroupName(Ljava/lang/String;)V

    .line 343
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;->getTypeFromInt(I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setGroupType(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;)V

    .line 344
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setShareType(I)V

    .line 345
    add-int/lit8 v1, v1, 0x1

    .line 346
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setExpireTime(J)V

    .line 347
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setGroupUrl(Ljava/lang/String;)V

    .line 348
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 350
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->getLocationsTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;

    move-result-object v5

    .line 351
    invoke-virtual {v5, p1, v4}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->getLocation(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v3

    .line 352
    if-nez v3, :cond_0

    .line 353
    const-string/jumbo v5, "PlaceOnGroupTableHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Location "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 354
    const-string/jumbo v7, " not found!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 353
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    const/4 v0, 0x0

    .line 362
    .end local v0    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    :goto_0
    return-object v0

    .line 358
    .restart local v0    # "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    :cond_0
    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 360
    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->setGroupParticipants(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V

    goto :goto_0
.end method

.method protected static getNewPlaceOnGroupTableHandler()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    const-string/jumbo v0, "tb_location_group"

    return-object v0
.end method

.method private isGroupInTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 136
    const/4 v3, 0x0

    .line 138
    .local v3, "status":Z
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "select * from tb_location_group where group_id = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 138
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 140
    .local v2, "sql":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 141
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 142
    const/4 v3, 0x1

    .line 143
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 150
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v2    # "sql":Ljava/lang/String;
    :goto_0
    return v3

    .line 144
    :catch_0
    move-exception v1

    .line 145
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 146
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .line 147
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0
.end method

.method private savePlaceOnGroup(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 124
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 125
    :cond_0
    const-string/jumbo v0, "PlaceOnGroupTableHandler"

    const-string/jumbo v1, "savePlaceOnGroup: group or groupId invalid."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    const/4 v0, 0x0

    .line 132
    :goto_0
    return v0

    .line 129
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->isGroupInTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 130
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->updatePlaceOnGroup(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v0

    goto :goto_0

    .line 132
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->createPlaceOnGroup(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v0

    goto :goto_0
.end method

.method private setGroupParticipants(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 260
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 261
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->getPlaceOnParticipantsTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;

    move-result-object v2

    .line 263
    .local v2, "participantsTableHandler":Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v3

    invoke-virtual {v2, p1, v3}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;->getParticipants(Landroid/database/sqlite/SQLiteDatabase;I)Ljava/util/ArrayList;

    move-result-object v1

    .line 265
    .local v1, "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    if-eqz v1, :cond_0

    .line 266
    invoke-virtual {p2, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setGroupParticipants(Ljava/util/ArrayList;)V

    .line 273
    .end local v1    # "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    .end local v2    # "participantsTableHandler":Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;
    :goto_0
    return-void

    .line 268
    .restart local v1    # "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    .restart local v2    # "participantsTableHandler":Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;
    :cond_0
    const-string/jumbo v3, "PlaceOnGroupTableHandler"

    const-string/jumbo v4, "[getParticipants] failed"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 270
    .end local v1    # "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    .end local v2    # "participantsTableHandler":Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;
    :catch_0
    move-exception v0

    .line 271
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method private updatePlaceOnGroup(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 11
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 180
    const/4 v1, 0x0

    .line 182
    .local v1, "result":I
    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 183
    .local v2, "values":Landroid/content/ContentValues;
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupName()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 184
    const-string/jumbo v7, "group_name"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    :cond_0
    const-string/jumbo v7, "group_type"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;->getValue()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 186
    const-string/jumbo v7, "group_share_type"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getShareType()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 187
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getExpireTime()J

    move-result-wide v7

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-lez v7, :cond_1

    .line 188
    const-string/jumbo v7, "expireTime"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getExpireTime()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 189
    :cond_1
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupUrl()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    .line 190
    const-string/jumbo v7, "url"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :cond_2
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 192
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationID()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 193
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    .line 194
    const-string/jumbo v7, "location_id"

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_3
    const-string/jumbo v4, "group_id = ?"

    .line 197
    .local v4, "whereClause":Ljava/lang/String;
    const/4 v7, 0x1

    new-array v3, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 198
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v3, v7

    .line 200
    .local v3, "whereArgs":[Ljava/lang/String;
    const-string/jumbo v7, "tb_location_group"

    invoke-virtual {p1, v7, v2, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 208
    .end local v2    # "values":Landroid/content/ContentValues;
    .end local v3    # "whereArgs":[Ljava/lang/String;
    .end local v4    # "whereClause":Ljava/lang/String;
    :goto_0
    if-nez v1, :cond_4

    :goto_1
    return v5

    .line 202
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 204
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 205
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0

    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_4
    move v5, v6

    .line 208
    goto :goto_1
.end method


# virtual methods
.method public changeGroupDestination(Landroid/database/sqlite/SQLiteDatabase;ILcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "group_id"    # I
    .param p3, "newDestination"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    const/4 v3, 0x0

    .line 367
    if-ltz p2, :cond_0

    if-nez p3, :cond_1

    .line 386
    :cond_0
    :goto_0
    return v3

    .line 370
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->getGroupById(Landroid/database/sqlite/SQLiteDatabase;I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    move-result-object v1

    .line 371
    .local v1, "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    if-eqz v1, :cond_0

    .line 374
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 375
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->getLocationsTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;

    move-result-object v2

    .line 376
    .local v2, "locationTableHandler":Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;
    if-eqz v2, :cond_0

    .line 380
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, p1, v4}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->getLocation(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v0

    .line 379
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    .line 381
    .local v0, "currentDestination":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;
    if-eqz v0, :cond_0

    .line 384
    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLatitude(D)V

    .line 385
    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLongitude(D)V

    .line 386
    invoke-virtual {v2, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->saveLocation(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v3

    goto :goto_0
.end method

.method public createPlaceOnGroupTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 39
    :try_start_0
    const-string/jumbo v1, "CREATE TABLE IF NOT EXISTS tb_location_group (group_id INTEGER PRIMARY KEY, group_name VARCHAR, group_type INTEGER, group_share_type INTEGER, timestamp INTEGER, expireTime INTEGER, url VARCHAR, location_id VARCHAR(22), FOREIGN KEY(location_id) REFERENCES tb_locations(location_id));"

    .line 50
    .local v1, "sql":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    .end local v1    # "sql":Ljava/lang/String;
    :goto_0
    return-void

    .line 51
    :catch_0
    move-exception v0

    .line 52
    .local v0, "e":Landroid/database/SQLException;
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    goto :goto_0
.end method

.method public deleteAllGroups(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v1, 0x0

    .line 213
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 214
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->getPlaceOnParticipantsTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;->deleteAllParticipants(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 215
    const-string/jumbo v2, "PlaceOnGroupTableHandler"

    const-string/jumbo v3, "Fail to remove participants of group "

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :goto_0
    return v1

    .line 220
    :cond_0
    :try_start_0
    const-string/jumbo v2, "tb_location_group"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 222
    const/4 v1, 0x1

    goto :goto_0

    .line 223
    :catch_0
    move-exception v0

    .line 224
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 225
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 226
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0
.end method

.method public dropPlaceOnGroupTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 59
    :try_start_0
    const-string/jumbo v1, "DROP TABLE IF EXISTS tb_location_group;"

    .line 60
    .local v1, "sql":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    .end local v1    # "sql":Ljava/lang/String;
    :goto_0
    return-void

    .line 61
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Landroid/database/SQLException;
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    goto :goto_0
.end method

.method public getGroupById(Landroid/database/sqlite/SQLiteDatabase;I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "groupId"    # I

    .prologue
    const/4 v4, 0x0

    .line 234
    const/4 v2, 0x0

    .line 235
    .local v2, "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    const/4 v0, 0x0

    .line 236
    .local v0, "cursor":Landroid/database/Cursor;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "select *  from tb_location_group where group_id = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 237
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 236
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 239
    .local v3, "sql":Ljava/lang/String;
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p1, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 241
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_1

    .line 250
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 253
    :goto_0
    return-object v4

    .line 244
    :cond_1
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->getGroupFromCursor(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    move-result-object v2

    .line 246
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 250
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :goto_1
    move-object v4, v2

    .line 253
    goto :goto_0

    .line 247
    :catch_0
    move-exception v1

    .line 248
    .local v1, "e":Ljava/lang/NullPointerException;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 250
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 249
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v4

    .line 250
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 251
    throw v4
.end method

.method public getLocationGroup(Landroid/database/sqlite/SQLiteDatabase;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v1, 0x0

    .line 314
    const-string/jumbo v2, "select *  from tb_location_group"

    .line 316
    .local v2, "sql":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, " order by timestamp desc"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 317
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, " LIMIT 1;"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 319
    invoke-virtual {p1, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 321
    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 331
    :goto_0
    return-object v1

    .line 325
    :cond_0
    const/4 v1, 0x0

    .line 326
    .local v1, "placeOnGroup":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 327
    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->getGroupFromCursor(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    move-result-object v1

    .line 329
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public loadLocationGroupList(Landroid/database/sqlite/SQLiteDatabase;IJ)Ljava/util/ArrayList;
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "maxCount"    # I
    .param p3, "fromExpiretime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "IJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 277
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 279
    .local v2, "groupList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    const-string/jumbo v3, "select *  from tb_location_group"

    .line 281
    .local v3, "sql":Ljava/lang/String;
    const-wide/16 v4, 0x0

    cmp-long v4, p3, v4

    if-lez v4, :cond_0

    .line 282
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, " where expireTime >= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 285
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, " order by timestamp desc"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 287
    if-lez p2, :cond_1

    .line 288
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, " LIMIT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 291
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 293
    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 295
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_3

    .line 296
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 310
    :goto_0
    return-object v2

    .line 301
    :cond_3
    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->getGroupFromCursor(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    move-result-object v1

    .line 303
    .local v1, "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    if-eqz v1, :cond_4

    .line 304
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 306
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 308
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public saveGroup(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 9
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    const/4 v5, 0x0

    .line 68
    if-nez p2, :cond_0

    .line 69
    const-string/jumbo v6, "PlaceOnGroupTableHandler"

    const-string/jumbo v7, "Invalid group."

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :goto_0
    return v5

    .line 74
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v0

    .line 75
    .local v0, "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    if-nez v0, :cond_1

    .line 76
    const-string/jumbo v6, "PlaceOnGroupTableHandler"

    const-string/jumbo v7, "Invalid group location."

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 80
    :cond_1
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 81
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->getLocationsTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;

    move-result-object v1

    .line 82
    .local v1, "locationTableHandler":Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;
    invoke-virtual {v1, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->saveLocation(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 83
    const-string/jumbo v6, "PlaceOnGroupTableHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Fail to save location of group "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 88
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->savePlaceOnGroup(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 89
    const-string/jumbo v6, "PlaceOnGroupTableHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Fail to save group "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 94
    :cond_3
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipants()Ljava/util/ArrayList;

    move-result-object v3

    .line 95
    .local v3, "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    if-nez v3, :cond_4

    .line 96
    const-string/jumbo v6, "PlaceOnGroupTableHandler"

    const-string/jumbo v7, "participants are null"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 101
    :cond_4
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 102
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->getPlaceOnParticipantsTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;

    move-result-object v4

    .line 103
    .local v4, "participantsTableHandler":Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v6

    invoke-virtual {v4, p1, v6, v3}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;->saveParticipants(Landroid/database/sqlite/SQLiteDatabase;ILjava/util/ArrayList;)Z

    move-result v6

    .line 104
    if-nez v6, :cond_5

    .line 105
    const-string/jumbo v6, "PlaceOnGroupTableHandler"

    .line 106
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Fail to save participants of group "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 105
    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 112
    :cond_5
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->mDriveLinkLocationTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;

    .line 113
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->getLocationTrackingTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;

    move-result-object v2

    .line 114
    .local v2, "locationTrackingTableHandler":Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;
    invoke-virtual {v2, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;->saveParticipantTracking(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 115
    const-string/jumbo v6, "PlaceOnGroupTableHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Fail to save tracking of participants of group "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 116
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 115
    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 120
    :cond_6
    const/4 v5, 0x1

    goto/16 :goto_0
.end method
