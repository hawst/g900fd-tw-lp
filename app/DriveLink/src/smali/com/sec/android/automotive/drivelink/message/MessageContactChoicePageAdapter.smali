.class public Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "MessageContactChoicePageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static final ITEM_NUM_PER_PAGE:I = 0x4

.field private static final MAX_PAGE_TO_LOOP:I = 0x4e20


# instance fields
.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mContactChoiceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mInitialPosition:I

.field private mIsDayMode:Z

.field private mPageCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "clickListener"    # Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v1, 0x1

    .line 40
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 38
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mIsDayMode:Z

    .line 42
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mContext:Landroid/content/Context;

    .line 43
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mContactChoiceList:Ljava/util/List;

    .line 44
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    .line 45
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->getPageCount()I

    move-result v0

    .line 46
    .local v0, "pageCount":I
    if-le v0, v1, :cond_0

    .line 48
    const/16 v1, 0x2710

    rem-int/2addr v1, v0

    rsub-int v1, v1, 0x2710

    .line 47
    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mInitialPosition:I

    .line 49
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mPageCount:I

    .line 66
    :goto_0
    return-void

    .line 51
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mInitialPosition:I

    .line 52
    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mPageCount:I

    goto :goto_0
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 191
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 192
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mPageCount:I

    return v0
.end method

.method public getInitialPosition()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mInitialPosition:I

    return v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 232
    const/4 v0, -0x2

    return v0
.end method

.method public getPageCount()I
    .locals 3

    .prologue
    .line 208
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mContactChoiceList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    div-int/lit8 v0, v2, 0x4

    .line 209
    .local v0, "page":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mContactChoiceList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    rem-int/lit8 v1, v2, 0x4

    .line 210
    .local v1, "remainder":I
    if-lez v1, :cond_0

    .line 211
    add-int/lit8 v0, v0, 0x1

    .line 213
    :cond_0
    const/16 v2, 0xa

    if-le v0, v2, :cond_1

    .line 214
    const/16 v0, 0xa

    .line 215
    :cond_1
    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 19
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 84
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v15

    .line 85
    .local v15, "layoutInflater":Landroid/view/LayoutInflater;
    const v3, 0x7f03008b

    const/4 v4, 0x0

    invoke-virtual {v15, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v17

    .line 87
    .local v17, "v":Landroid/view/View;
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 88
    .local v16, "listItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/widget/RelativeLayout;>;"
    const v3, 0x7f0901d0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    const v3, 0x7f0901d1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    const v3, 0x7f0901d2

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    const v3, 0x7f0901d3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->getPageCount()I

    move-result v3

    rem-int v3, p2, v3

    mul-int/lit8 v13, v3, 0x4

    .line 98
    .local v13, "index":I
    const/4 v9, 0x0

    .line 101
    .local v9, "bitmap":Landroid/graphics/Bitmap;
    const/4 v12, 0x1

    .line 102
    .local v12, "i":I
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 183
    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 184
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    .line 186
    return-object v17

    .line 102
    .restart local p1    # "container":Landroid/view/ViewGroup;
    :cond_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/widget/RelativeLayout;

    .line 103
    .local v14, "layout":Landroid/widget/RelativeLayout;
    new-instance v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;

    const/4 v3, 0x0

    invoke-direct {v11, v3}, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;)V

    .line 106
    .local v11, "holder":Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;
    const v3, 0x7f0901cb

    invoke-virtual {v14, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 105
    iput-object v3, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    .line 108
    const v3, 0x7f0901cc

    invoke-virtual {v14, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 107
    iput-object v3, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->ivLogsMaskImage:Landroid/widget/ImageView;

    .line 109
    const v3, 0x7f0901cd

    invoke-virtual {v14, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    .line 111
    const v3, 0x7f0901ce

    invoke-virtual {v14, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 110
    iput-object v3, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->tvNumber:Landroid/widget/TextView;

    .line 113
    const v3, 0x7f0901cf

    invoke-virtual {v14, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 112
    iput-object v3, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->ivInboxIcon:Landroid/widget/TextView;

    .line 115
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mContactChoiceList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v13, :cond_5

    .line 117
    invoke-virtual {v14, v12}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 118
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v14, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mContactChoiceList:Ljava/util/List;

    invoke-interface {v3, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 122
    .local v10, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 123
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mContactChoiceList:Ljava/util/List;

    invoke-interface {v3, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-wide v3, v3, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    .line 124
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mContactChoiceList:Ljava/util/List;

    invoke-interface {v5, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v5, v5, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 125
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mContactChoiceList:Ljava/util/List;

    invoke-interface {v6, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v6

    .line 126
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mContactChoiceList:Ljava/util/List;

    invoke-interface {v7, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    .line 122
    invoke-direct/range {v2 .. v8}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 128
    .local v2, "d":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v3

    .line 127
    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 129
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mContext:Landroid/content/Context;

    .line 128
    invoke-virtual {v3, v4, v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 131
    if-nez v9, :cond_3

    .line 132
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 133
    iget-object v3, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    .line 134
    const v4, 0x7f02024e

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 143
    :goto_1
    const-string/jumbo v3, "MessageActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "MessageContactChoicePageAdapter ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 144
    iget-object v5, v10, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 143
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    iget-object v3, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    iget-object v4, v10, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    iget-object v4, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->tvNumber:Landroid/widget/TextView;

    .line 148
    invoke-virtual {v10}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v3

    const/4 v5, 0x0

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactData;

    iget-object v3, v3, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v3, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->ivInboxIcon:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 151
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mIsDayMode:Z

    if-eqz v3, :cond_4

    .line 152
    iget-object v3, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 153
    const v5, 0x7f08002f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 152
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 154
    iget-object v3, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->ivLogsMaskImage:Landroid/widget/ImageView;

    .line 155
    const v4, 0x7f020209

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 178
    .end local v2    # "d":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v10    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_1
    :goto_2
    add-int/lit8 v13, v13, 0x1

    .line 179
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 136
    .restart local v2    # "d":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .restart local v10    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_2
    iget-object v3, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    .line 137
    const v4, 0x7f02023d

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 140
    :cond_3
    iget-object v3, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    invoke-virtual {v3, v9}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 158
    :cond_4
    iget-object v3, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 159
    const v5, 0x7f080030

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 158
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 160
    iget-object v3, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->ivLogsMaskImage:Landroid/widget/ImageView;

    .line 161
    const v4, 0x7f02020c

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 162
    const v3, 0x7f020002

    invoke-virtual {v14, v3}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_2

    .line 166
    .end local v2    # "d":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v10    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_5
    iget-object v3, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 167
    iget-object v3, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->ivLogsMaskImage:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 168
    iget-object v3, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 169
    iget-object v3, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->tvNumber:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 170
    iget-object v3, v11, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter$ViewHolder;->ivInboxIcon:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 171
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mIsDayMode:Z

    if-nez v3, :cond_1

    .line 174
    const v3, 0x7f020002

    invoke-virtual {v14, v3}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_2
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 220
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setContactChoiceList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p1, "contactChoiceList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mContactChoiceList:Ljava/util/List;

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->getPageCount()I

    move-result v0

    .line 72
    .local v0, "pageCount":I
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 74
    const/16 v1, 0x2710

    rem-int/2addr v1, v0

    rsub-int v1, v1, 0x2710

    .line 73
    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mInitialPosition:I

    .line 75
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mPageCount:I

    .line 80
    :goto_0
    return-void

    .line 77
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mInitialPosition:I

    .line 78
    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mPageCount:I

    goto :goto_0
.end method

.method protected setDayMode()V
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mIsDayMode:Z

    .line 205
    return-void
.end method

.method protected setNightMode()V
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactChoicePageAdapter;->mIsDayMode:Z

    .line 201
    return-void
.end method
