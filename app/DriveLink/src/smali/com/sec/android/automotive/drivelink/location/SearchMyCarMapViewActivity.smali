.class public Lcom/sec/android/automotive/drivelink/location/SearchMyCarMapViewActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "SearchMyCarMapViewActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 12
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 14
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 15
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "find_my_car_param_function"

    .line 16
    const-string/jumbo v2, "search_car_position"

    .line 15
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 18
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/SearchMyCarMapViewActivity;->startActivity(Landroid/content/Intent;)V

    .line 19
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/SearchMyCarMapViewActivity;->finish()V

    .line 20
    return-void
.end method
