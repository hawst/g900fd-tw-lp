.class public Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "PrepareDialActivity.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# static fields
.field private static final MULTIWINDOW_STATE_PAUSE_DELAY:I = 0x320

.field public static final TAG:Ljava/lang/String; = "PrepareDial"


# instance fields
.field private final TIMER:I

.field private context:Landroid/content/Context;

.field private count:Landroid/widget/ImageView;

.field private index:I

.field private mAudioFocusReleased:Z

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

.field private mBmp:Landroid/graphics/Bitmap;

.field private mCancelCall:Landroid/widget/LinearLayout;

.field private mCancelCallImage:Landroid/widget/ImageView;

.field private mCancelCallText:Landroid/widget/TextView;

.field private mCountDownLayout:Landroid/widget/RelativeLayout;

.field private mDisplayName:Ljava/lang/String;

.field private mIsCanceled:Z

.field private mIsFromMsgReader:Z

.field private mMakeCall:Z

.field private mOutgoingLayout:Landroid/widget/RelativeLayout;

.field private mPhoneNumber:Ljava/lang/String;

.field private mPhoneNumberSize:I

.field private mPhoneType:I

.field private mProgress:Landroid/widget/ImageView;

.field private mQuotesEnd:Landroid/widget/ImageView;

.field private mQuotesStart:Landroid/widget/ImageView;

.field private mState:I

.field private mTTSPlayEnded:Z

.field private mTTSPlayed:Z

.field private mThumbImg:Landroid/widget/ImageView;

.field private mTtsName:Ljava/lang/String;

.field private mUse:Z

.field private remainedTime:I

.field timer:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    .line 76
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->TIMER:I

    .line 78
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->remainedTime:I

    .line 79
    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->index:I

    .line 88
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mTtsName:Ljava/lang/String;

    .line 107
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mIsFromMsgReader:Z

    .line 113
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mIsCanceled:Z

    .line 116
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMakeCall:Z

    .line 117
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mTTSPlayed:Z

    .line 118
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mTTSPlayEnded:Z

    .line 119
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mAudioManager:Landroid/media/AudioManager;

    .line 120
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mAudioFocusReleased:Z

    .line 472
    new-instance v0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->timer:Landroid/os/Handler;

    .line 71
    return-void
.end method

.method private abandonAudioFocus()V
    .locals 2

    .prologue
    .line 1024
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mAudioFocusReleased:Z

    if-nez v0, :cond_0

    .line 1025
    const-string/jumbo v0, "PrepareDial"

    const-string/jumbo v1, "[PrepareDialActivity] abandonAudioFocus()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1026
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 1027
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mAudioFocusReleased:Z

    .line 1030
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;I)V
    .locals 0

    .prologue
    .line 85
    iput p1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mState:I

    return-void
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Z
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->isMultiWindow()Z

    move-result v0

    return v0
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mThumbImg:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mCountDownLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mCancelCall:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mOutgoingLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$16(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$18(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneNumberSize:I

    return v0
.end method

.method static synthetic access$19(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Z
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mIsFromMsgReader:Z

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mState:I

    return v0
.end method

.method static synthetic access$20(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)V
    .locals 0

    .prologue
    .line 329
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->startNumberModePhoneActivity()V

    return-void
.end method

.method static synthetic access$21(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;Z)V
    .locals 0

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mIsCanceled:Z

    return-void
.end method

.method static synthetic access$22(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMakeCall:Z

    return v0
.end method

.method static synthetic access$23(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)V
    .locals 0

    .prologue
    .line 1049
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->startBargeInAndTimer()V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->remainedTime:I

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;I)V
    .locals 0

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->remainedTime:I

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->count:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mProgress:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mQuotesStart:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mQuotesEnd:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mCancelCallText:Landroid/widget/TextView;

    return-object v0
.end method

.method private getAudioFocus()V
    .locals 4

    .prologue
    .line 1033
    const-string/jumbo v1, "PrepareDial"

    const-string/jumbo v2, "[PrepareDialActivity] getAudioFocus()"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1034
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mAudioManager:Landroid/media/AudioManager;

    const/16 v2, 0x9

    .line 1035
    const/4 v3, 0x2

    .line 1034
    invoke-virtual {v1, p0, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 1047
    .local v0, "result":I
    return-void
.end method

.method public static getBargeinLanguage()I
    .locals 5

    .prologue
    .line 1073
    const-string/jumbo v2, "language"

    .line 1074
    const-string/jumbo v3, "en-US"

    .line 1072
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1075
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 1076
    .local v1, "language":Ljava/lang/String;
    const/4 v0, 0x1

    .line 1078
    .local v0, "lan":I
    const-string/jumbo v2, "ko-kr"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1079
    const/4 v0, 0x0

    .line 1106
    :cond_0
    :goto_0
    const-string/jumbo v2, "PrepareDial"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "language : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/ lan : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1108
    return v0

    .line 1080
    :cond_1
    const-string/jumbo v2, "zh-cn"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1081
    const/4 v0, 0x2

    .line 1082
    goto :goto_0

    :cond_2
    const-string/jumbo v2, "es-es"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1083
    const/4 v0, 0x3

    .line 1084
    goto :goto_0

    :cond_3
    const-string/jumbo v2, "fr-fr"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1085
    const/4 v0, 0x4

    .line 1086
    goto :goto_0

    :cond_4
    const-string/jumbo v2, "de-de"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1087
    const/4 v0, 0x5

    .line 1088
    goto :goto_0

    :cond_5
    const-string/jumbo v2, "it-it"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1089
    const/4 v0, 0x6

    .line 1090
    goto :goto_0

    :cond_6
    const-string/jumbo v2, "ja-jp"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1091
    const/4 v0, 0x7

    .line 1092
    goto :goto_0

    :cond_7
    const-string/jumbo v2, "ru-ru"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1093
    const/16 v0, 0x8

    .line 1094
    goto :goto_0

    :cond_8
    const-string/jumbo v2, "pt-br"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1095
    const/16 v0, 0x9

    .line 1096
    goto :goto_0

    :cond_9
    const-string/jumbo v2, "en-gb"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1097
    const/16 v0, 0xa

    .line 1098
    goto/16 :goto_0

    :cond_a
    const-string/jumbo v2, "v-es-la"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1099
    const/16 v0, 0xb

    goto/16 :goto_0
.end method

.method private isVisibleActivity()Z
    .locals 4

    .prologue
    .line 1180
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->isResumed()Z

    move-result v0

    .line 1181
    .local v0, "isVisible":Z
    const-string/jumbo v1, "PrepareDial"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "is visible activity:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1182
    return v0
.end method

.method private setPrepareDialLayout(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 11
    .param p1, "displayName"    # Ljava/lang/String;
    .param p2, "phoneNumber"    # Ljava/lang/String;
    .param p3, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    const v10, 0x7f0202d9

    const/4 v9, 0x4

    .line 371
    const v6, 0x7f090103

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 372
    .local v2, "contactName":Landroid/widget/TextView;
    const v6, 0x7f090105

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 374
    .local v3, "contactNumber":Landroid/widget/TextView;
    const v6, 0x7f090100

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mThumbImg:Landroid/widget/ImageView;

    .line 375
    move-object v0, p3

    .line 376
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_3

    .line 377
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mThumbImg:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {v7, v0, v10}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 384
    :goto_0
    const v6, 0x7f090104

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 386
    .local v1, "contactIcon":Landroid/widget/ImageView;
    iget v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneType:I

    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->getPhoneNumberDisplayType(I)Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->getCallDrawable()I

    move-result v6

    .line 385
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 388
    const-string/jumbo v6, "country_detector"

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/location/CountryDetector;

    .line 390
    .local v4, "detector":Landroid/location/CountryDetector;
    if-eqz p2, :cond_0

    if-eqz v4, :cond_0

    .line 391
    invoke-virtual {v4}, Landroid/location/CountryDetector;->detectCountry()Landroid/location/Country;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 392
    invoke-virtual {v4}, Landroid/location/CountryDetector;->detectCountry()Landroid/location/Country;

    move-result-object v6

    invoke-virtual {v6}, Landroid/location/Country;->getCountryIso()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 393
    const-string/jumbo v6, "PrepareDial"

    const-string/jumbo v7, "phoneNumber formatNumber"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    invoke-virtual {v4}, Landroid/location/CountryDetector;->detectCountry()Landroid/location/Country;

    move-result-object v6

    invoke-virtual {v6}, Landroid/location/Country;->getCountryIso()Ljava/lang/String;

    move-result-object v6

    .line 394
    invoke-static {p2, v6}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 396
    .local v5, "formatPhoneNumber":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 397
    move-object p2, v5

    .line 401
    .end local v5    # "formatPhoneNumber":Ljava/lang/String;
    :cond_0
    if-eqz p1, :cond_4

    const-string/jumbo v6, ""

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 402
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 406
    :goto_1
    invoke-virtual {v3, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 408
    if-eqz p1, :cond_1

    .line 409
    if-eqz p2, :cond_2

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 410
    :cond_1
    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 411
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 414
    :cond_2
    const v6, 0x7f09010d

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mQuotesStart:Landroid/widget/ImageView;

    .line 415
    const v6, 0x7f09010f

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mQuotesEnd:Landroid/widget/ImageView;

    .line 417
    const v6, 0x7f09010b

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mCancelCallImage:Landroid/widget/ImageView;

    .line 418
    const v6, 0x7f09010e

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mCancelCallText:Landroid/widget/TextView;

    .line 419
    return-void

    .line 380
    .end local v1    # "contactIcon":Landroid/widget/ImageView;
    .end local v4    # "detector":Landroid/location/CountryDetector;
    :cond_3
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mThumbImg:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 381
    const v8, 0x7f0202db

    .line 380
    invoke-static {v7, v8, v10}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 404
    .restart local v1    # "contactIcon":Landroid/widget/ImageView;
    .restart local v4    # "detector":Landroid/location/CountryDetector;
    :cond_4
    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private startBargeInAndTimer()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1050
    const-string/jumbo v0, "PrepareDial"

    const-string/jumbo v1, "[startBargeInAndTimer]"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1051
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    if-nez v0, :cond_0

    .line 1069
    :goto_0
    return-void

    .line 1055
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v0}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->getState()I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mState:I

    .line 1056
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mState:I

    if-ne v0, v4, :cond_1

    .line 1057
    const-string/jumbo v0, "PrepareDial"

    const-string/jumbo v1, "[startBargeInAndTimer] -"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1060
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    const/16 v1, 0x9

    invoke-static {}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getBargeinLanguage()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->startBargeIn(II)V

    .line 1061
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v0}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->isBargeInEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mUse:Z

    .line 1062
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v0}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->getState()I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mState:I

    .line 1064
    const-string/jumbo v0, "PrepareDial"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[startBargeInAndTimer] bargein state : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1066
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->timer:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1068
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mTTSPlayEnded:Z

    goto :goto_0
.end method

.method private startNumberModePhoneActivity()V
    .locals 8

    .prologue
    .line 330
    const-string/jumbo v4, "PrepareDial"

    const-string/jumbo v5, "startNumberModePhoneActivity"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    const-string/jumbo v4, "DM_DIAL"

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v1

    .line 335
    .local v1, "curFlowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v1, :cond_1

    .line 337
    const/4 v0, 0x0

    .line 338
    .local v0, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-eqz v4, :cond_0

    .line 339
    iget v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneNumberSize:I

    iget-object v5, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 340
    invoke-virtual {v5}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    .line 339
    if-eq v4, v5, :cond_0

    .line 342
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v5

    .line 344
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->context:Landroid/content/Context;

    .line 345
    iget-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v4

    .line 346
    const/4 v7, 0x0

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactData;

    iget-object v4, v4, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    .line 343
    invoke-interface {v5, v6, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getContactFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v3

    .line 347
    .local v3, "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-eqz v3, :cond_0

    .line 348
    invoke-static {v3}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v0

    .line 353
    .end local v3    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :cond_0
    new-instance v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 354
    .local v2, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v0, :cond_2

    .line 355
    iput-object v0, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 359
    :goto_0
    iget v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserIndex:I

    iput v4, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserIndex:I

    .line 360
    const-string/jumbo v4, "PrepareDial"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "[Phone FlowParams]:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    const-string/jumbo v4, "DM_DIAL_TYPE"

    invoke-static {v4, v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 367
    .end local v0    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v2    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_1
    return-void

    .line 357
    .restart local v0    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v2    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_2
    iget-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iput-object v4, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    goto :goto_0
.end method


# virtual methods
.method protected autoMultiModeSize()Z
    .locals 1

    .prologue
    .line 1114
    const/4 v0, 0x0

    return v0
.end method

.method public finish()V
    .locals 3

    .prologue
    .line 862
    const-string/jumbo v0, "PrepareDial"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "JINSEIL finish! "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMakeCall:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 863
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->finish()V

    .line 864
    return-void
.end method

.method protected makeCall(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "number"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 584
    const-string/jumbo v3, "PrepareDial"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "[makeCall] mMakeCall : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMakeCall:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 586
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 585
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v3

    .line 587
    const-string/jumbo v4, "PREF_SETTINGS_MY_NAVIGATION_PACKAGE"

    .line 588
    const-string/jumbo v5, "com.google.android.apps.maps"

    .line 586
    invoke-virtual {v3, v4, v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 589
    .local v2, "naviPackageName":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    .line 590
    const-string/jumbo v3, "com.sec.android.automotive.drivelink.NAVIGATION_PACKAGE_NAME"

    .line 589
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 591
    .local v1, "intent2":Landroid/content/Intent;
    const-string/jumbo v3, "com.android.phone"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 592
    const-string/jumbo v3, "INTENT_KEY_NAVIGATION_PACKAGE_NAME"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 597
    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 599
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v3}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->stopBargeIn()V

    .line 602
    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mIsCanceled:Z

    if-eqz v3, :cond_1

    .line 648
    :cond_0
    :goto_0
    return-void

    .line 609
    :cond_1
    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMakeCall:Z

    if-nez v3, :cond_0

    .line 623
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMakeCall:Z

    .line 625
    const-string/jumbo v3, "PrepareDial"

    const-string/jumbo v4, "Make a call intent"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 636
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->isMultiWindow()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 637
    const-string/jumbo v3, "PrepareDial"

    const-string/jumbo v4, "SET Multi-Navi Flag true"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 638
    invoke-static {p0, v6}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->setMultiWindowState(Landroid/content/Context;I)V

    .line 641
    :cond_2
    const-string/jumbo v3, "JINSEIL"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "JINSEIL "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.CALL_PRIVILEGED"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "tel:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 646
    .local v0, "intent":Landroid/content/Intent;
    const/4 v3, 0x0

    invoke-virtual {p0, v0, v3}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "resultCode"    # I
    .param p2, "requestCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 869
    const-string/jumbo v1, "PrepareDial"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "JINSEIL onActivityResult "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMakeCall:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 870
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMakeCall:Z

    if-eqz v1, :cond_0

    .line 871
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->timer:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 873
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 874
    .local v0, "h":Landroid/os/Handler;
    new-instance v1, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)V

    .line 881
    const-wide/16 v2, 0x3e8

    .line 874
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 884
    .end local v0    # "h":Landroid/os/Handler;
    :cond_0
    return-void
.end method

.method public onAudioFocusChange(I)V
    .locals 2
    .param p1, "focusChange"    # I

    .prologue
    .line 1123
    packed-switch p1, :pswitch_data_0

    .line 1150
    :goto_0
    :pswitch_0
    return-void

    .line 1134
    :pswitch_1
    const-string/jumbo v0, "PrepareDial"

    const-string/jumbo v1, "PrepareDialActivity AUDIOFOCUS_LOSS_TRANSIENT"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1147
    :pswitch_2
    const-string/jumbo v0, "PrepareDial"

    const-string/jumbo v1, "PrepareDialActivity AUDIOFOCUS_GAIN"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1123
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 653
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onBackPressed()V

    .line 655
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v0}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->stopBargeIn()V

    .line 657
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->remainedTime:I

    .line 658
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->timer:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 660
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneNumberSize:I

    if-le v0, v2, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mIsFromMsgReader:Z

    if-nez v0, :cond_0

    .line 661
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->startNumberModePhoneActivity()V

    .line 663
    :cond_0
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mIsCanceled:Z

    .line 665
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->finish()V

    .line 666
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v3, 0x0

    .line 686
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 688
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->isMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 689
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 690
    const v1, 0x7f0d0021

    .line 691
    const v2, 0x7f0d0022

    .line 689
    invoke-static {p0, v0, v1, v2}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->setMultiModeSize(Landroid/content/Context;Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;II)Z

    .line 693
    const v0, 0x7f030026

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->setContentView(I)V

    .line 699
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mDisplayName:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneNumber:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBmp:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->setPrepareDialLayout(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 701
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->remainedTime:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->remainedTime:I

    .line 702
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->setTimer()V

    .line 705
    const-string/jumbo v0, "PrepareDial"

    const-string/jumbo v1, "onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    const-string/jumbo v0, "PrepareDial"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "timer has : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->timer:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->timer:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 710
    const-string/jumbo v0, "PrepareDial"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "mMakeCall : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMakeCall:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    const-string/jumbo v0, "PrepareDial"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "bargein state : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v2}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->getState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 712
    const-string/jumbo v0, "PrepareDial"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "timer : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->remainedTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 713
    const-string/jumbo v0, "PrepareDial"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "mTTSPlayEnded : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mTTSPlayEnded:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMakeCall:Z

    if-nez v0, :cond_0

    .line 717
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mTTSPlayEnded:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->remainedTime:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 718
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->timer:Landroid/os/Handler;

    const-wide/16 v1, 0x29e

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 721
    :cond_0
    const v0, 0x7f09010a

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mCancelCall:Landroid/widget/LinearLayout;

    .line 723
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mCancelCall:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 739
    return-void

    .line 695
    :cond_1
    const v0, 0x7f030025

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->setContentView(I)V

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 131
    const-string/jumbo v6, "PrepareDial"

    const-string/jumbo v8, "onCreate"

    invoke-static {v6, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 135
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->addPendingReq()V

    .line 138
    const-string/jumbo v6, "DLPhraseSpotter"

    const-string/jumbo v8, "[stop] : PrepareDialActivity - onCreate()"

    invoke-static {v6, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->context:Landroid/content/Context;

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    .line 143
    const-string/jumbo v8, "audio"

    invoke-virtual {v6, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/media/AudioManager;

    .line 142
    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mAudioManager:Landroid/media/AudioManager;

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->isMultiWindow()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 146
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 147
    const v8, 0x7f0d0021

    .line 148
    const v9, 0x7f0d0022

    .line 146
    invoke-static {p0, v6, v8, v9}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->setMultiModeSize(Landroid/content/Context;Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;II)Z

    .line 150
    const v6, 0x7f030026

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->setContentView(I)V

    .line 155
    :goto_0
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->isMultiWindow()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 156
    const-string/jumbo v6, "PrepareDial"

    const-string/jumbo v8, "MultiMode SET RESUME :: when Create"

    invoke-static {v6, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    invoke-static {p0, v10}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->setMultiWindowState(Landroid/content/Context;I)V

    .line 162
    :cond_0
    const-string/jumbo v6, "DM_DIAL"

    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v3

    .line 164
    .local v3, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v3, :cond_5

    iget-object v6, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-eqz v6, :cond_5

    .line 165
    const/4 v6, -0x1

    iput v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->index:I

    .line 166
    iget v6, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    if-gez v6, :cond_4

    move v4, v7

    .line 168
    .local v4, "typeIndex":I
    :goto_1
    iget-object v6, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v6, v6, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mDisplayName:Ljava/lang/String;

    .line 170
    iget-object v6, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v6, v6, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    if-eqz v6, :cond_1

    .line 172
    iget-object v6, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v6, v6, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isJapaneseString(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 173
    iget-object v6, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v6, v6, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mTtsName:Ljava/lang/String;

    .line 176
    :cond_1
    iget-object v6, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 177
    invoke-virtual {v6}, Lcom/vlingo/core/internal/contacts/ContactData;->getType()I

    move-result v6

    .line 176
    iput v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneType:I

    .line 178
    iget-object v6, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/contacts/ContactData;

    iget-object v6, v6, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneNumber:Ljava/lang/String;

    .line 182
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v6

    .line 181
    check-cast v6, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 183
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v6

    .line 185
    iget-object v8, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-wide v8, v8, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    .line 184
    invoke-virtual {v6, p0, v8, v9}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getPhoneNumberList(Landroid/content/Context;J)Ljava/util/ArrayList;

    move-result-object v6

    .line 185
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 181
    iput v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneNumberSize:I

    .line 187
    iget-boolean v6, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mIsFromMsgReader:Z

    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mIsFromMsgReader:Z

    .line 190
    iget-object v6, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-static {v6}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getDlContact(Lcom/vlingo/core/internal/contacts/ContactMatch;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v1

    .line 191
    .local v1, "dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v6

    .line 192
    invoke-interface {v6, p0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 191
    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBmp:Landroid/graphics/Bitmap;

    .line 193
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBmp:Landroid/graphics/Bitmap;

    if-nez v6, :cond_2

    .line 194
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v6

    .line 195
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneNumber:Ljava/lang/String;

    invoke-interface {v6, p0, v8}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getContactImageFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 194
    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBmp:Landroid/graphics/Bitmap;

    .line 244
    .end local v1    # "dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v4    # "typeIndex":I
    :cond_2
    :goto_2
    const-string/jumbo v6, "DM_DIAL"

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->setActivityFlowID(Ljava/lang/String;)V

    .line 246
    const-string/jumbo v6, "PrepareDial"

    new-instance v8, Ljava/lang/StringBuilder;

    iget v9, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->index:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    const-string/jumbo v6, "PrepareDial"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Phonetype : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneType:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    new-instance v6, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-direct {v6}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;-><init>()V

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    .line 250
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    .line 251
    new-instance v8, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$2;

    invoke-direct {v8, p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)V

    invoke-virtual {v6, v8}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->InitBargeInRecognizer(Lcom/sec/android/app/IWSpeechRecognizer/IWSpeechRecognizerListener;)V

    .line 286
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mDisplayName:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneNumber:Ljava/lang/String;

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBmp:Landroid/graphics/Bitmap;

    invoke-direct {p0, v6, v8, v9}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->setPrepareDialLayout(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 290
    const/4 v6, 0x5

    iput v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->remainedTime:I

    .line 291
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->setTimer()V

    .line 293
    const v6, 0x7f09010a

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mCancelCall:Landroid/widget/LinearLayout;

    .line 295
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mCancelCall:Landroid/widget/LinearLayout;

    new-instance v8, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$3;

    invoke-direct {v8, p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)V

    invoke-virtual {v6, v8}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 314
    iput-boolean v10, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mTTSPlayed:Z

    .line 315
    iput-boolean v7, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMakeCall:Z

    .line 316
    iput-boolean v7, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mTTSPlayEnded:Z

    .line 317
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getAudioFocus()V

    .line 318
    return-void

    .line 152
    .end local v3    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_3
    const v6, 0x7f030025

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 167
    .restart local v3    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_4
    iget v4, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    goto/16 :goto_1

    .line 197
    :cond_5
    if-eqz v3, :cond_2

    iget-object v6, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserCallLog:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    if-eqz v6, :cond_2

    .line 198
    iget-object v6, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserCallLog:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mDisplayName:Ljava/lang/String;

    .line 201
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v6

    .line 200
    check-cast v6, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 202
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    .line 203
    iget-object v9, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserCallLog:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getPhoneNumber()Ljava/lang/String;

    move-result-object v9

    .line 201
    invoke-virtual {v6, v8, v9}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getContactFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v5

    .line 209
    .local v5, "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-eqz v5, :cond_9

    .line 210
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_9

    .line 211
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v8

    if-eq v6, v8, :cond_9

    .line 212
    const-string/jumbo v6, "PrepareDial"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "mDiaplayName : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", userName : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 213
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 212
    invoke-static {v6, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mDisplayName:Ljava/lang/String;

    .line 220
    :cond_6
    :goto_3
    if-eqz v5, :cond_b

    .line 221
    const-string/jumbo v6, "PrepareDial"

    const-string/jumbo v8, "found user by phone number"

    invoke-static {v6, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v2

    .line 223
    .local v2, "dp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_7
    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_a

    .line 233
    .end local v2    # "dp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    :goto_5
    iget-object v6, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserCallLog:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getPhoneNumber()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneNumber:Ljava/lang/String;

    .line 234
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v6

    .line 235
    iget-object v8, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserCallLog:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    invoke-interface {v6, p0, v8}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getCallLogImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 234
    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBmp:Landroid/graphics/Bitmap;

    .line 236
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBmp:Landroid/graphics/Bitmap;

    if-nez v6, :cond_8

    .line 237
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v6

    .line 238
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneNumber:Ljava/lang/String;

    invoke-interface {v6, p0, v8}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getContactImageFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 237
    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBmp:Landroid/graphics/Bitmap;

    .line 241
    :cond_8
    iput v10, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneNumberSize:I

    goto/16 :goto_2

    .line 215
    :cond_9
    if-nez v5, :cond_6

    .line 216
    const-string/jumbo v6, "PrepareDial"

    const-string/jumbo v8, "not found user by phonenymber"

    invoke-static {v6, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mDisplayName:Ljava/lang/String;

    goto :goto_3

    .line 223
    .restart local v2    # "dp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    :cond_a
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    .line 224
    .local v0, "d":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v8

    .line 225
    iget-object v9, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserCallLog:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getPhoneNumber()Ljava/lang/String;

    move-result-object v9

    .line 224
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    .line 225
    if-eqz v8, :cond_7

    .line 226
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneType()I

    move-result v8

    iput v8, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneType:I

    goto :goto_4

    .line 230
    .end local v0    # "d":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    .end local v2    # "dp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    :cond_b
    const-string/jumbo v6, "PrepareDial"

    const-string/jumbo v8, "can\'t find user by phone number"

    invoke-static {v6, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    iput v7, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneType:I

    goto :goto_5
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    .line 811
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onDestroy()V

    .line 813
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMakeCall:Z

    if-eqz v1, :cond_0

    .line 814
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->timer:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 818
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 819
    .local v0, "mHandler":Landroid/os/Handler;
    new-instance v1, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)V

    .line 829
    const-wide/16 v2, 0x12c

    .line 819
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 843
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 749
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onPause()V

    .line 768
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v0}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->stopBargeIn()V

    .line 772
    const-string/jumbo v0, "PrepareDial"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[onPause] mAudioFocusReleased : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mAudioFocusReleased:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 773
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->abandonAudioFocus()V

    .line 775
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMakeCall:Z

    if-eqz v0, :cond_0

    .line 776
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->timer:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 781
    :cond_0
    return-void
.end method

.method protected onRestart()V
    .locals 3

    .prologue
    .line 848
    const-string/jumbo v0, "PrepareDial"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[onRestart] mMakeCall : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMakeCall:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 856
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onRestart()V

    .line 857
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 679
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 680
    const-string/jumbo v0, "secs"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->remainedTime:I

    .line 681
    return-void
.end method

.method protected onResume()V
    .locals 11

    .prologue
    const v10, 0x7f0a013c

    const v9, 0x7f0a00aa

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 895
    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMakeCall:Z

    if-eqz v4, :cond_0

    .line 896
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->timer:Landroid/os/Handler;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 903
    :cond_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    if-eqz v4, :cond_1

    .line 904
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v4}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->getState()I

    move-result v4

    if-eqz v4, :cond_1

    .line 906
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    const/16 v5, 0x9

    invoke-static {}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getBargeinLanguage()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->startBargeIn(II)V

    .line 909
    :cond_1
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->timer:Landroid/os/Handler;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v4

    if-nez v4, :cond_2

    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mIsRotation:Z

    if-eqz v4, :cond_3

    .line 910
    :cond_2
    const-string/jumbo v4, "PrepareDial"

    const-string/jumbo v5, "[onResume] timer restarted!"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 911
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->timer:Landroid/os/Handler;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 913
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->timer:Landroid/os/Handler;

    const-wide/16 v5, 0x14d

    invoke-virtual {v4, v7, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 916
    :cond_3
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onResume()V

    .line 919
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->isMultiWindow()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 920
    invoke-static {p0, v8}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->setMultiWindowState(Landroid/content/Context;I)V

    .line 924
    :cond_4
    const-string/jumbo v4, "PrepareDial"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "[onResume] mTTSPlayed : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v6, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mTTSPlayed:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 926
    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mTTSPlayed:Z

    if-eqz v4, :cond_6

    .line 928
    const-string/jumbo v3, ""

    .line 930
    .local v3, "userName":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mDisplayName:Ljava/lang/String;

    if-eqz v4, :cond_8

    .line 932
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mTtsName:Ljava/lang/String;

    if-eqz v4, :cond_7

    .line 933
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mTtsName:Ljava/lang/String;

    .line 938
    :goto_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mDisplayName:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 939
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mDisplayName:Ljava/lang/String;

    const-string/jumbo v5, ""

    const-string/jumbo v6, " "

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 946
    :cond_5
    :goto_1
    if-eqz v3, :cond_9

    .line 947
    const-string/jumbo v4, "-"

    const-string/jumbo v5, " "

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 952
    :goto_2
    const-string/jumbo v4, "DM_DIAL"

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v1

    .line 954
    .local v1, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v0, ""

    .line 955
    .local v0, "Prompt":Ljava/lang/String;
    if-eqz v1, :cond_c

    .line 956
    iget-boolean v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->isVideoCall:Z

    if-eqz v4, :cond_a

    .line 957
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 958
    new-array v5, v8, [Ljava/lang/Object;

    aput-object v3, v5, v7

    .line 957
    invoke-virtual {v4, v10, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 981
    :goto_3
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelAudio()V

    .line 982
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v4

    new-instance v5, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$7;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$7;-><init>(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)V

    invoke-interface {v4, v0, v5}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 1019
    iput-boolean v7, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mTTSPlayed:Z

    .line 1021
    .end local v0    # "Prompt":Ljava/lang/String;
    .end local v1    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v3    # "userName":Ljava/lang/String;
    :cond_6
    return-void

    .line 935
    .restart local v3    # "userName":Ljava/lang/String;
    :cond_7
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mDisplayName:Ljava/lang/String;

    goto :goto_0

    .line 942
    :cond_8
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneNumber:Ljava/lang/String;

    if-eqz v4, :cond_5

    .line 943
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneNumber:Ljava/lang/String;

    const-string/jumbo v5, ""

    const-string/jumbo v6, " "

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 949
    :cond_9
    const-string/jumbo v3, ""

    goto :goto_2

    .line 959
    .restart local v0    # "Prompt":Ljava/lang/String;
    .restart local v1    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_a
    iget-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    if-eqz v4, :cond_b

    .line 960
    iget-object v0, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 961
    goto :goto_3

    .line 962
    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 963
    new-array v5, v8, [Ljava/lang/Object;

    aput-object v3, v5, v7

    .line 962
    invoke-virtual {v4, v9, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 965
    goto :goto_3

    .line 967
    :cond_c
    const-string/jumbo v4, "DM_DIAL_CONTACT_SEARCH_LIST"

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v2

    .line 969
    .local v2, "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v2, :cond_d

    iget-boolean v4, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->isVideoCall:Z

    if-eqz v4, :cond_d

    .line 970
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 971
    new-array v5, v8, [Ljava/lang/Object;

    aput-object v3, v5, v7

    .line 970
    invoke-virtual {v4, v10, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 972
    goto :goto_3

    .line 973
    :cond_d
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 974
    new-array v5, v8, [Ljava/lang/Object;

    aput-object v3, v5, v7

    .line 973
    invoke-virtual {v4, v9, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 671
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 673
    const-string/jumbo v0, "secs"

    iget v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->remainedTime:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 674
    return-void
.end method

.method protected onStop()V
    .locals 5

    .prologue
    .line 785
    const-string/jumbo v1, "PrepareDial"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[onStop] "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 788
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v1}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->stopBargeIn()V

    .line 790
    const-string/jumbo v1, "PrepareDial"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[onStop] mAudioFocusReleased : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mAudioFocusReleased:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->abandonAudioFocus()V

    .line 793
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMakeCall:Z

    if-eqz v1, :cond_0

    .line 794
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->timer:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 798
    :cond_0
    const-wide/16 v1, 0x12c

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 804
    :goto_0
    const-string/jumbo v1, "PrepareDial"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[onStop] super.onStop  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onStop()V

    .line 806
    return-void

    .line 799
    :catch_0
    move-exception v0

    .line 801
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method protected setTimer()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const v2, 0x7f02001c

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 422
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->remainedTime:I

    if-gtz v0, :cond_0

    .line 470
    :goto_0
    return-void

    .line 425
    :cond_0
    const v0, 0x7f090106

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mCountDownLayout:Landroid/widget/RelativeLayout;

    .line 426
    const v0, 0x7f090109

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mOutgoingLayout:Landroid/widget/RelativeLayout;

    .line 427
    const v0, 0x7f090108

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->count:Landroid/widget/ImageView;

    .line 428
    const v0, 0x7f090107

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mProgress:Landroid/widget/ImageView;

    .line 430
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mProgress:Landroid/widget/ImageView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, Landroid/widget/ImageView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 432
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->remainedTime:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->remainedTime:I

    .line 434
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->remainedTime:I

    packed-switch v0, :pswitch_data_0

    .line 452
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->count:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 462
    :goto_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->timer:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 464
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->count:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 465
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mProgress:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 466
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mQuotesStart:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 467
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mQuotesEnd:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 469
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mCancelCallText:Landroid/widget/TextView;

    invoke-virtual {v0, v5, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto :goto_0

    .line 436
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->count:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 440
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->count:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 441
    const v2, 0x7f02001b

    .line 440
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 444
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->count:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 445
    const v2, 0x7f02001a

    .line 444
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 448
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->count:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 449
    const v2, 0x7f020019

    .line 448
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 434
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
