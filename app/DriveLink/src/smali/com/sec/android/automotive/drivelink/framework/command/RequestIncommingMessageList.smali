.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestIncommingMessageList;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestIncommingMessageList.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mInbox:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

.field private mItemCount:I

.field private mMessageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;ILcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "inbox"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    .param p3, "itemCount"    # I
    .param p4, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, p4}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 14
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestIncommingMessageList;->mContext:Landroid/content/Context;

    .line 15
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestIncommingMessageList;->mInbox:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 16
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestIncommingMessageList;->mMessageList:Ljava/util/ArrayList;

    .line 23
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestIncommingMessageList;->mContext:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestIncommingMessageList;->mInbox:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 25
    iput p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestIncommingMessageList;->mItemCount:I

    .line 26
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 47
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestIncommingMessageList;->mContext:Landroid/content/Context;

    .line 48
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestIncommingMessageList;->mMessageList:Ljava/util/ArrayList;

    .line 49
    return-void
.end method

.method protected process()V
    .locals 4

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestIncommingMessageList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 31
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 32
    .local v0, "contentManager":Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestIncommingMessageList;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestIncommingMessageList;->mInbox:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 33
    iget v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestIncommingMessageList;->mItemCount:I

    .line 32
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getIncommingMessageList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestIncommingMessageList;->mMessageList:Ljava/util/ArrayList;

    .line 34
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestIncommingMessageList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 39
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMessageListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;

    move-result-object v0

    .line 40
    .local v0, "messageListener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;
    if-eqz v0, :cond_0

    .line 41
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestIncommingMessageList;->mMessageList:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;->onResponseRequestIncommingMessageList(Ljava/util/ArrayList;)V

    .line 43
    :cond_0
    return-void
.end method
