.class Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;
.super Ljava/lang/Thread;
.source "MusicPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ProgressThread"
.end annotation


# instance fields
.field public isRunning:Z

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V
    .locals 1

    .prologue
    .line 1869
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1870
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;->isRunning:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1876
    :cond_0
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;->isRunning:Z

    if-eqz v1, :cond_1

    .line 1877
    const-wide/16 v1, 0x384

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    .line 1878
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1879
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1880
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mProgHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$29(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1889
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;->isInterrupted()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1890
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 1891
    :goto_0
    return-void

    .line 1885
    :catch_0
    move-exception v0

    .line 1886
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
