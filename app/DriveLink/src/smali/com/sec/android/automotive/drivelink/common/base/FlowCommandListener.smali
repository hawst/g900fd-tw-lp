.class public interface abstract Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;
.super Ljava/lang/Object;
.source "FlowCommandListener.java"


# virtual methods
.method public abstract onFlowCommandCall(Ljava/lang/String;)V
.end method

.method public abstract onFlowCommandCancel(Ljava/lang/String;)V
.end method

.method public abstract onFlowCommandCustom(Ljava/lang/String;)Z
.end method

.method public abstract onFlowCommandExcute(Ljava/lang/String;)V
.end method

.method public abstract onFlowCommandIgnore(Ljava/lang/String;)V
.end method

.method public abstract onFlowCommandLookup(Ljava/lang/String;)V
.end method

.method public abstract onFlowCommandNext(Ljava/lang/String;)V
.end method

.method public abstract onFlowCommandNo(Ljava/lang/String;)V
.end method

.method public abstract onFlowCommandRead(Ljava/lang/String;)V
.end method

.method public abstract onFlowCommandReply(Ljava/lang/String;)V
.end method

.method public abstract onFlowCommandReset(Ljava/lang/String;)V
.end method

.method public abstract onFlowCommandReturnMain()V
.end method

.method public abstract onFlowCommandRoute(Ljava/lang/String;)V
.end method

.method public abstract onFlowCommandSend(Ljava/lang/String;)V
.end method

.method public abstract onFlowCommandYes(Ljava/lang/String;)V
.end method
