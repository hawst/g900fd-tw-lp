.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestBluetoothPairingNSSPAnswer.java"


# instance fields
.field private mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

.field private mContext:Landroid/content/Context;

.field private mDeviceName:Ljava/lang/String;

.field private mInputPIN:[B

.field private result:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/lang/String;[B)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "deviceName"    # Ljava/lang/String;
    .param p4, "inputPin"    # [B

    .prologue
    .line 21
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 23
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;->mContext:Landroid/content/Context;

    .line 24
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;->mDeviceName:Ljava/lang/String;

    .line 25
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;->mInputPIN:[B

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;->result:Z

    .line 27
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;->mContext:Landroid/content/Context;

    .line 57
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;->mDeviceName:Ljava/lang/String;

    .line 58
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;->mInputPIN:[B

    .line 59
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 60
    return-void
.end method

.method protected process()V
    .locals 4

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 33
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getConnectivityManager()Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v0

    .line 35
    .local v0, "connectivityManager":Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getBluetoothDeviceDetailsByName(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .line 34
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 37
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_0

    .line 38
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;->mContext:Landroid/content/Context;

    .line 39
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;->mInputPIN:[B

    .line 38
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->pairBluetoothDeviceByNSSP(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;[B)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;->result:Z

    .line 41
    :cond_0
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 47
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkConnectivityListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;

    move-result-object v0

    .line 48
    .local v0, "connectivityListener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;
    if-eqz v0, :cond_0

    .line 50
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothPairingNSSPAnswer;->result:Z

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;->onResponseRequestBluetoothPairingNSSPAnswer(Z)V

    .line 52
    :cond_0
    return-void
.end method
