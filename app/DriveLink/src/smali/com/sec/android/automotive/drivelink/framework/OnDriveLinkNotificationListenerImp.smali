.class public Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;
.super Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkBaseListenerImp;
.source "OnDriveLinkNotificationListenerImp.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;


# instance fields
.field private mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkBaseListenerImp;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    return-object v0
.end method


# virtual methods
.method public onNotifyAlarm(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;)V
    .locals 3
    .param p1, "alarmInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;

    .prologue
    .line 96
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v1, :cond_0

    .line 109
    :goto_0
    return-void

    .line 100
    :cond_0
    move-object v0, p1

    .line 101
    .local v0, "_alarmInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$5;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$5;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlarmInfo;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyBTPairingRequest(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;)V
    .locals 3
    .param p1, "btInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;

    .prologue
    .line 170
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v1, :cond_0

    .line 185
    :goto_0
    return-void

    .line 174
    :cond_0
    move-object v0, p1

    .line 175
    .local v0, "_btInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$9;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$9;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyCallState(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo;)V
    .locals 3
    .param p1, "callInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo;

    .prologue
    .line 25
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v1, :cond_0

    .line 39
    :goto_0
    return-void

    .line 29
    :cond_0
    move-object v0, p1

    .line 30
    .local v0, "_callInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$1;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyLocationFailedRequestSent(I)V
    .locals 2
    .param p1, "msg"    # I

    .prologue
    .line 552
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v0, :cond_0

    .line 567
    :goto_0
    return-void

    .line 556
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$27;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$27;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyLocationGroupDestinatiON_CHANGEdSent()V
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v0, :cond_0

    .line 347
    :goto_0
    return-void

    .line 336
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$16;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$16;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyLocationGroupDestinationChanged(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 4
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;
    .param p2, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 268
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v2, :cond_0

    .line 286
    :goto_0
    return-void

    .line 272
    :cond_0
    move-object v1, p1

    .line 273
    .local v1, "_msgInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;
    move-object v0, p2

    .line 274
    .local v0, "_location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$13;

    invoke-direct {v3, p0, v1, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$13;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyLocationGroupRequestIgnored()V
    .locals 2

    .prologue
    .line 513
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v0, :cond_0

    .line 527
    :goto_0
    return-void

    .line 517
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$25;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$25;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyLocationGroupShareRequest(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 4
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;
    .param p2, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 243
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v2, :cond_0

    .line 262
    :goto_0
    return-void

    .line 247
    :cond_0
    move-object v1, p1

    .line 248
    .local v1, "_msgInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;
    move-object v0, p2

    .line 249
    .local v0, "_location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$12;

    invoke-direct {v3, p0, v1, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$12;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyLocationGroupShareRequestIgnored()V
    .locals 2

    .prologue
    .line 312
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v0, :cond_0

    .line 327
    :goto_0
    return-void

    .line 316
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$15;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$15;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyLocationGroupSharingExpired()V
    .locals 2

    .prologue
    .line 352
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v0, :cond_0

    .line 367
    :goto_0
    return-void

    .line 356
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$17;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$17;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyLocationGroupSharingRequestSent()V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v0, :cond_0

    .line 307
    :goto_0
    return-void

    .line 295
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$14;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$14;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyLocationGroupSharingStopped(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 453
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v0, :cond_0

    .line 469
    :goto_0
    return-void

    .line 457
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$22;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$22;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyLocationHasNoSamsungAccount(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;)V
    .locals 3
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    .prologue
    .line 531
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v1, :cond_0

    .line 548
    :goto_0
    return-void

    .line 535
    :cond_0
    move-object v0, p1

    .line 536
    .local v0, "_msgInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$26;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$26;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyLocationRequest(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Ljava/lang/String;)V
    .locals 4
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 131
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v2, :cond_0

    .line 147
    :goto_0
    return-void

    .line 135
    :cond_0
    move-object v0, p1

    .line 136
    .local v0, "_msgInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;
    move-object v1, p2

    .line 138
    .local v1, "_url":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$7;

    invoke-direct {v3, p0, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$7;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyLocationRequestIgnored()V
    .locals 2

    .prologue
    .line 393
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v0, :cond_0

    .line 408
    :goto_0
    return-void

    .line 397
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$19;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$19;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyLocationRequestSent(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
    .locals 3
    .param p1, "dlContact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 372
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v1, :cond_0

    .line 388
    :goto_0
    return-void

    .line 376
    :cond_0
    move-object v0, p1

    .line 377
    .local v0, "_dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$18;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$18;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyLocationRequestWaiting(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
    .locals 3
    .param p1, "dlContact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 473
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v1, :cond_0

    .line 489
    :goto_0
    return-void

    .line 477
    :cond_0
    move-object v0, p1

    .line 478
    .local v0, "_dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$23;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$23;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyLocationShare(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Ljava/lang/String;)V
    .locals 4
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 151
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v2, :cond_0

    .line 166
    :goto_0
    return-void

    .line 155
    :cond_0
    move-object v0, p1

    .line 156
    .local v0, "_msgInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;
    move-object v1, p2

    .line 157
    .local v1, "_url":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$8;

    invoke-direct {v3, p0, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$8;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyLocationShareEnded()V
    .locals 2

    .prologue
    .line 493
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v0, :cond_0

    .line 508
    :goto_0
    return-void

    .line 497
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$24;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$24;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyLocationShareSent(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 433
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v0, :cond_0

    .line 448
    :goto_0
    return-void

    .line 437
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$21;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$21;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyLocationSharedIgnored()V
    .locals 2

    .prologue
    .line 413
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v0, :cond_0

    .line 428
    :goto_0
    return-void

    .line 417
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$20;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$20;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyLowBattery(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 79
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v1, :cond_0

    .line 92
    :goto_0
    return-void

    .line 83
    :cond_0
    move v0, p1

    .line 84
    .local v0, "_level":I
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$4;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$4;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyMirrorLinkSetup()V
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v0, :cond_0

    .line 217
    :goto_0
    return-void

    .line 207
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$10;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$10;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyMirrorLinkShutDown()V
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v0, :cond_0

    .line 237
    :goto_0
    return-void

    .line 226
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$11;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$11;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyOutgoingCall()V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v0, :cond_0

    .line 55
    :goto_0
    return-void

    .line 47
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$2;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifyReceivedMSG(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;)V
    .locals 3
    .param p1, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    .prologue
    .line 59
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v1, :cond_0

    .line 75
    :goto_0
    return-void

    .line 63
    :cond_0
    move-object v0, p1

    .line 64
    .local v0, "_msgInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$3;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$3;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onNotifySchedule(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;)V
    .locals 3
    .param p1, "scheduleInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;

    .prologue
    .line 113
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    if-nez v1, :cond_0

    .line 127
    :goto_0
    return-void

    .line 117
    :cond_0
    move-object v0, p1

    .line 118
    .local v0, "_scheduleInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$6;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$6;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public setDriveLinkListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;

    .prologue
    .line 20
    check-cast p1, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    .end local p1    # "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    .line 21
    return-void
.end method
