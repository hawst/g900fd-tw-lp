.class Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;
.super Ljava/lang/Thread;
.source "NotificationItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NotiSoundThread"
.end annotation


# instance fields
.field public hundredmillis:I

.field private mFinishListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

.field public sampleId:I

.field public soundPool:Landroid/media/SoundPool;

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

.field private timeoutCnt:I


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 829
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 830
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->mFinishListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

    .line 831
    iput v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->timeoutCnt:I

    .line 832
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->soundPool:Landroid/media/SoundPool;

    .line 833
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->sampleId:I

    .line 835
    iput v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->hundredmillis:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 848
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 850
    :try_start_0
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, ">>>SoundPlay Start."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->soundPool:Landroid/media/SoundPool;

    if-eqz v0, :cond_0

    .line 852
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->soundPool:Landroid/media/SoundPool;

    iget v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->sampleId:I

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 853
    const/high16 v6, 0x3f800000    # 1.0f

    .line 852
    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    move-result v8

    .line 854
    .local v8, "result":I
    const-string/jumbo v0, "[NotificationItem]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "SoundPlay: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 857
    .end local v8    # "result":I
    :cond_0
    :goto_0
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->timeoutCnt:I

    iget v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->hundredmillis:I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lt v0, v1, :cond_4

    .line 868
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->soundPool:Landroid/media/SoundPool;

    if-eqz v0, :cond_1

    .line 869
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->soundPool:Landroid/media/SoundPool;

    iget v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->sampleId:I

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->stop(I)V

    .line 872
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$4(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Landroid/media/SoundPool;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 873
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$4(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Landroid/media/SoundPool;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget v1, v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mSoundID:I

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->unload(I)Z

    .line 874
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$4(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Landroid/media/SoundPool;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/media/SoundPool;->setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V

    .line 875
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$4(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Landroid/media/SoundPool;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 876
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    invoke-static {v0, v9}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$5(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Landroid/media/SoundPool;)V

    .line 879
    :cond_2
    :goto_1
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "<<<SoundPlay End."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 881
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->mFinishListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

    if-eqz v0, :cond_3

    .line 882
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # invokes: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->abandonAudioFocus()Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$3(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Z

    .line 883
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->mFinishListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;->onNotiSoundFinished()V

    .line 886
    :cond_3
    return-void

    .line 858
    :cond_4
    const-wide/16 v0, 0x64

    :try_start_1
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->sleep(J)V

    .line 859
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->timeoutCnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->timeoutCnt:I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 862
    :catch_0
    move-exception v7

    .line 863
    .local v7, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-virtual {v7}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 864
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->mFinishListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 868
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->soundPool:Landroid/media/SoundPool;

    if-eqz v0, :cond_5

    .line 869
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->soundPool:Landroid/media/SoundPool;

    iget v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->sampleId:I

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->stop(I)V

    .line 872
    :cond_5
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$4(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Landroid/media/SoundPool;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 873
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$4(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Landroid/media/SoundPool;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget v1, v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mSoundID:I

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->unload(I)Z

    .line 874
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$4(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Landroid/media/SoundPool;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/media/SoundPool;->setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V

    .line 875
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$4(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Landroid/media/SoundPool;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 876
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    invoke-static {v0, v9}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$5(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Landroid/media/SoundPool;)V

    goto :goto_1

    .line 865
    .end local v7    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v7

    .line 866
    .local v7, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 868
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->soundPool:Landroid/media/SoundPool;

    if-eqz v0, :cond_6

    .line 869
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->soundPool:Landroid/media/SoundPool;

    iget v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->sampleId:I

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->stop(I)V

    .line 872
    :cond_6
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$4(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Landroid/media/SoundPool;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 873
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$4(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Landroid/media/SoundPool;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget v1, v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mSoundID:I

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->unload(I)Z

    .line 874
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$4(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Landroid/media/SoundPool;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/media/SoundPool;->setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V

    .line 875
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$4(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Landroid/media/SoundPool;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 876
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    invoke-static {v0, v9}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$5(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Landroid/media/SoundPool;)V

    goto/16 :goto_1

    .line 867
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    .line 868
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->soundPool:Landroid/media/SoundPool;

    if-eqz v1, :cond_7

    .line 869
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->soundPool:Landroid/media/SoundPool;

    iget v2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->sampleId:I

    invoke-virtual {v1, v2}, Landroid/media/SoundPool;->stop(I)V

    .line 872
    :cond_7
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$4(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Landroid/media/SoundPool;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 873
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$4(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Landroid/media/SoundPool;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget v2, v2, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mSoundID:I

    invoke-virtual {v1, v2}, Landroid/media/SoundPool;->unload(I)Z

    .line 874
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$4(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Landroid/media/SoundPool;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/media/SoundPool;->setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V

    .line 875
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mPool:Landroid/media/SoundPool;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$4(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Landroid/media/SoundPool;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/SoundPool;->release()V

    .line 876
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    invoke-static {v1, v9}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$5(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Landroid/media/SoundPool;)V

    .line 878
    :cond_8
    throw v0
.end method

.method public setOnSoundFinishedListener(Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

    .prologue
    .line 838
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->mFinishListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

    .line 839
    return-void
.end method
