.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
.source "DLMusicImp.java"


# instance fields
.field private mAlbum:Ljava/lang/String;

.field private mAlbumArt:Landroid/graphics/Bitmap;

.field private mAlbumId:I

.field private mArtist:Ljava/lang/String;

.field private mData:Ljava/lang/String;

.field private mDisplayName:Ljava/lang/String;

.field private mDuration:I

.field private mGenreName:Ljava/lang/String;

.field private mId:I

.field private mTitle:Ljava/lang/String;

.field private mUri:Landroid/net/Uri;

.field private mbAlbumArtCached:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;-><init>()V

    .line 9
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mId:I

    .line 10
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mTitle:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mDisplayName:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mData:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mArtist:Ljava/lang/String;

    .line 14
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbum:Ljava/lang/String;

    .line 15
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbumId:I

    .line 16
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mDuration:I

    .line 17
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mUri:Landroid/net/Uri;

    .line 18
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbumArt:Landroid/graphics/Bitmap;

    .line 19
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mbAlbumArtCached:Z

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mGenreName:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/net/Uri;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "displayName"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "data"    # Ljava/lang/String;
    .param p5, "artist"    # Ljava/lang/String;
    .param p6, "album"    # Ljava/lang/String;
    .param p7, "albumId"    # I
    .param p8, "duration"    # I
    .param p9, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;-><init>()V

    .line 9
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mId:I

    .line 10
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mTitle:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mDisplayName:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mData:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mArtist:Ljava/lang/String;

    .line 14
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbum:Ljava/lang/String;

    .line 15
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbumId:I

    .line 16
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mDuration:I

    .line 17
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mUri:Landroid/net/Uri;

    .line 18
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbumArt:Landroid/graphics/Bitmap;

    .line 19
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mbAlbumArtCached:Z

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mGenreName:Ljava/lang/String;

    .line 24
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mId:I

    .line 25
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mDisplayName:Ljava/lang/String;

    .line 26
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mTitle:Ljava/lang/String;

    .line 27
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mData:Ljava/lang/String;

    .line 28
    iput-object p5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mArtist:Ljava/lang/String;

    .line 29
    iput-object p6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbum:Ljava/lang/String;

    .line 30
    iput p7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbumId:I

    .line 31
    iput p8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mDuration:I

    .line 32
    iput-object p9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mUri:Landroid/net/Uri;

    .line 33
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbumArt:Landroid/graphics/Bitmap;

    .line 34
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mbAlbumArtCached:Z

    .line 35
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;-><init>()V

    .line 9
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mId:I

    .line 10
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mTitle:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mDisplayName:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mData:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mArtist:Ljava/lang/String;

    .line 14
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbum:Ljava/lang/String;

    .line 15
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbumId:I

    .line 16
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mDuration:I

    .line 17
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mUri:Landroid/net/Uri;

    .line 18
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbumArt:Landroid/graphics/Bitmap;

    .line 19
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mbAlbumArtCached:Z

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mGenreName:Ljava/lang/String;

    .line 41
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mTitle:Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method public getAlbum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbum:Ljava/lang/String;

    return-object v0
.end method

.method public getAlbumArt()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbumArt:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getAlbumId()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbumId:I

    return v0
.end method

.method public getArtist()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mArtist:Ljava/lang/String;

    return-object v0
.end method

.method public getData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mData:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mDuration:I

    return v0
.end method

.method public getGenreName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mGenreName:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mId:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public isAlbumArtCached()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mbAlbumArtCached:Z

    return v0
.end method

.method public resetInfo()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 153
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mTitle:Ljava/lang/String;

    .line 154
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mDisplayName:Ljava/lang/String;

    .line 155
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mData:Ljava/lang/String;

    .line 156
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mArtist:Ljava/lang/String;

    .line 157
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbum:Ljava/lang/String;

    .line 158
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbumId:I

    .line 159
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mDuration:I

    .line 160
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mUri:Landroid/net/Uri;

    .line 161
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbumArt:Landroid/graphics/Bitmap;

    .line 162
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mbAlbumArtCached:Z

    .line 163
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mGenreName:Ljava/lang/String;

    .line 164
    return-void
.end method

.method public setAlbum(Ljava/lang/String;)V
    .locals 0
    .param p1, "album"    # Ljava/lang/String;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbum:Ljava/lang/String;

    .line 126
    return-void
.end method

.method public setAlbumArt(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "albumArt"    # Landroid/graphics/Bitmap;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbumArt:Landroid/graphics/Bitmap;

    .line 142
    return-void
.end method

.method public setAlbumId(I)V
    .locals 0
    .param p1, "albumId"    # I

    .prologue
    .line 129
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mAlbumId:I

    .line 130
    return-void
.end method

.method public setArtist(Ljava/lang/String;)V
    .locals 0
    .param p1, "artist"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mArtist:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public setCachedFlag(Z)V
    .locals 0
    .param p1, "bCached"    # Z

    .prologue
    .line 145
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mbAlbumArtCached:Z

    .line 146
    return-void
.end method

.method public setData(Ljava/lang/String;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mData:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mDisplayName:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public setDuration(I)V
    .locals 0
    .param p1, "duration"    # I

    .prologue
    .line 133
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mDuration:I

    .line 134
    return-void
.end method

.method public setGenreName(Ljava/lang/String;)V
    .locals 0
    .param p1, "genreName"    # Ljava/lang/String;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mGenreName:Ljava/lang/String;

    .line 150
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mId:I

    .line 106
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mTitle:Ljava/lang/String;

    .line 110
    return-void
.end method

.method public setUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->mUri:Landroid/net/Uri;

    .line 138
    return-void
.end method
