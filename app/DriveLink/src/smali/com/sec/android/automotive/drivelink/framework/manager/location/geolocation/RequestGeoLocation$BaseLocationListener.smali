.class Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;
.super Ljava/lang/Object;
.source "RequestGeoLocation.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BaseLocationListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;


# direct methods
.method private constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;)V
    .locals 0

    .prologue
    .line 247
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;)V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 4
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    const/4 v3, 0x0

    .line 251
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "BaseLocationListener onLocationChanged"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    if-nez p1, :cond_1

    .line 281
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    const-string/jumbo v0, "RequestGeoLocation"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "location: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    .line 259
    const-string/jumbo v1, "gps"

    .line 258
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 259
    if-eqz v0, :cond_2

    .line 260
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "BaseLocationListener location from gps"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;Landroid/location/Location;)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->doResponseRequestGeoLocation(Landroid/location/Location;Z)V
    invoke-static {v0, p1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;Landroid/location/Location;Z)V

    goto :goto_0

    .line 266
    :cond_2
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    .line 267
    const-string/jumbo v1, "network"

    .line 266
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 267
    if-eqz v0, :cond_0

    .line 268
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "BaseLocationListener location from network"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    const/high16 v1, 0x43fa0000    # 500.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_3

    .line 270
    const-string/jumbo v0, "RequestGeoLocation"

    .line 271
    const-string/jumbo v1, "BaseLocationListener location with accuracy equal or less than 500m"

    .line 270
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;Landroid/location/Location;)V

    .line 273
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->doResponseRequestGeoLocation(Landroid/location/Location;Z)V
    invoke-static {v0, p1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;Landroid/location/Location;Z)V

    goto :goto_0

    .line 276
    :cond_3
    const-string/jumbo v0, "RequestGeoLocation"

    .line 277
    const-string/jumbo v1, "BaseLocationListener location with accuracy bigger than 500m"

    .line 276
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$BaseLocationListener;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;Landroid/location/Location;)V

    goto :goto_0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 2
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 295
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "BaseLocationListener onProviderDisabled"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 2
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 290
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "BaseLocationListener onProviderEnabled"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 2
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 285
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "BaseLocationListener onStatusChanged"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    return-void
.end method
