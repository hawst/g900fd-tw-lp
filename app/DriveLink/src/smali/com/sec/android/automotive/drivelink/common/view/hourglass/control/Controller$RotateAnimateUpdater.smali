.class Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$RotateAnimateUpdater;
.super Ljava/lang/Object;
.source "Controller.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RotateAnimateUpdater"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;


# direct methods
.method private constructor <init>(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$RotateAnimateUpdater;->this$0:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$RotateAnimateUpdater;)V
    .locals 0

    .prologue
    .line 180
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$RotateAnimateUpdater;-><init>(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;)V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 185
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$RotateAnimateUpdater;->this$0:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->mbStopped:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->access$3(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller$RotateAnimateUpdater;->this$0:Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;

    const/16 v1, 0x3e8

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->playDrop(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;->access$5(Lcom/sec/android/automotive/drivelink/common/view/hourglass/control/Controller;II)V

    .line 193
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 198
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 203
    return-void
.end method
