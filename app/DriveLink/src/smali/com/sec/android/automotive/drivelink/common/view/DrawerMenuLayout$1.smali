.class Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$1;
.super Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;
.source "DrawerMenuLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    .line 854
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseRequestRecommendedLocationList(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 858
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$0(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;Z)V

    .line 859
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$1(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;Z)V

    .line 860
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 872
    return-void

    .line 860
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;

    .line 861
    .local v0, "dlLocationItem":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 862
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v2

    .line 863
    const-string/jumbo v3, "location_home"

    .line 862
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 863
    if-eqz v2, :cond_2

    .line 864
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    invoke-static {v2, v4}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$0(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;Z)V

    .line 866
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 867
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v2

    .line 868
    const-string/jumbo v3, "location_office"

    .line 867
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 868
    if-eqz v2, :cond_0

    .line 869
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    invoke-static {v2, v4}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$1(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;Z)V

    goto :goto_0
.end method
