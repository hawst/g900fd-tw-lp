.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$19;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getSearchedInboxItemOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$19;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 1945
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1949
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$19;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->hideInputMethod()V
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$61(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    .line 1950
    const/4 v0, 0x0

    .line 1952
    .local v0, "currentItem":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$19;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v3

    .line 1953
    mul-int/lit8 v3, v3, 0x4

    .line 1952
    add-int/2addr v2, v3

    add-int/lit8 v0, v2, -0x1

    .line 1955
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$19;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchedInboxListVoice:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$37(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 1956
    .local v1, "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/MessageHolder;->getMessageHolder()Lcom/sec/android/automotive/drivelink/message/MessageHolder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/automotive/drivelink/message/MessageHolder;->setInbox(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;)V

    .line 1958
    const-string/jumbo v2, "DM_SMS_READBACK"

    .line 1959
    const/4 v3, 0x0

    .line 1958
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 1962
    return-void
.end method
