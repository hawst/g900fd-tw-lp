.class public Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "SettingsActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;


# static fields
.field private static final LANGUAGE_PROCESS_NAME:Ljava/lang/String; = "com.vlingo.midas"

.field private static final TAG:Ljava/lang/String; = "[SettingsActivity]"


# instance fields
.field private btnNavBack:Landroid/widget/LinearLayout;

.field private dividerNavigation:Landroid/view/View;

.field private layoutDebugSettings:Landroid/widget/LinearLayout;

.field private layoutItemServerDataReset:Landroid/widget/LinearLayout;

.field private layoutLanguage:Landroid/widget/LinearLayout;

.field private layoutMyPlace:Landroid/widget/LinearLayout;

.field private layoutNavigation:Landroid/widget/LinearLayout;

.field private layoutRegisterCar:Landroid/widget/LinearLayout;

.field private layoutRejectMessage:Landroid/widget/LinearLayout;

.field private layoutServerDataReset:Landroid/widget/LinearLayout;

.field private layoutSuggestedContacts:Landroid/widget/LinearLayout;

.field private layoutTermsOfService:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private final mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

.field private tvLanguageSummary:Landroid/widget/TextView;

.field private tvNavigationSummary:Landroid/widget/TextView;

.field private tvRegisterCarSummary:Landroid/widget/TextView;

.field private tvRejectMessageSummary:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    .line 62
    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    .line 63
    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    .line 40
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getLanguage()Ljava/lang/String;
    .locals 3

    .prologue
    .line 322
    const-string/jumbo v1, "language"

    .line 323
    const-string/jumbo v2, "en-US"

    .line 321
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 324
    .local v0, "language":Ljava/lang/String;
    const-string/jumbo v1, "de-DE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 325
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a050b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 349
    :goto_0
    return-object v1

    .line 326
    :cond_0
    const-string/jumbo v1, "en-GB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 327
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a050c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 328
    :cond_1
    const-string/jumbo v1, "en-US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 329
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a050d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 330
    :cond_2
    const-string/jumbo v1, "es-ES"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 331
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a050e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 332
    :cond_3
    const-string/jumbo v1, "v-es-LA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 333
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a050f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 334
    :cond_4
    const-string/jumbo v1, "fr-FR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 335
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0510

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 336
    :cond_5
    const-string/jumbo v1, "it-IT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 337
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0511

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 338
    :cond_6
    const-string/jumbo v1, "ko-KR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 339
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0513

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 340
    :cond_7
    const-string/jumbo v1, "ru-RU"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 341
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0515

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 342
    :cond_8
    const-string/jumbo v1, "pt-BR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 343
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0516

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 344
    :cond_9
    const-string/jumbo v1, "ja-JP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 345
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0512

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 346
    :cond_a
    const-string/jumbo v1, "zh-CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 347
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0514

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 349
    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method private getRejectMessage()Ljava/lang/String;
    .locals 4

    .prologue
    .line 409
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 410
    const-string/jumbo v2, "drivelink_rejectmessage_body"

    .line 408
    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 412
    .local v0, "rejectMessage":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 414
    const-string/jumbo v2, "PREF_SETTINGS_GENERAL_REJECT_MESSAGE_DEFAULT"

    .line 415
    const/4 v3, 0x1

    .line 413
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v1

    .line 415
    if-eqz v1, :cond_0

    .line 416
    const-string/jumbo v1, "[SettingsActivity]"

    const-string/jumbo v2, "getRejectMessage()... _default message"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 418
    const v2, 0x7f0a03dc

    .line 417
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 421
    .end local v0    # "rejectMessage":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 420
    .restart local v0    # "rejectMessage":Ljava/lang/String;
    :cond_0
    const-string/jumbo v1, "[SettingsActivity]"

    const-string/jumbo v2, "getRejectMessage()... "

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isEnableUserDataReset()Z
    .locals 5

    .prologue
    .line 426
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSpeakerID()Ljava/lang/String;

    move-result-object v1

    .line 427
    .local v1, "speakerID":Ljava/lang/String;
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 428
    .local v0, "isEnabled":Z
    :goto_0
    const-string/jumbo v3, "[SettingsActivity]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "onPrepareOptionsMenu: speakerID is "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 429
    const-string/jumbo v4, "; setting \'Delete All Personal Data\' to "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 430
    if-eqz v0, :cond_1

    const-string/jumbo v2, "enabled"

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 428
    invoke-static {v3, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    return v0

    .line 427
    .end local v0    # "isEnabled":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 430
    .restart local v0    # "isEnabled":Z
    :cond_1
    const-string/jumbo v2, "disabled"

    goto :goto_1
.end method

.method private setRegisterCarSubtitle()V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->isConnected()Z

    move-result v0

    .line 317
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->getConnectedDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .line 316
    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->setRegisterCarSubtitle(ZLandroid/bluetooth/BluetoothDevice;)V

    .line 318
    return-void
.end method

.method private setRegisterCarSubtitle(ZLandroid/bluetooth/BluetoothDevice;)V
    .locals 5
    .param p1, "connected"    # Z
    .param p2, "connectedDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const v3, 0x7f0a03ed

    .line 356
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->hasRegisteredDevice(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->tvRegisterCarSummary:Landroid/widget/TextView;

    const v1, 0x7f0a03ee

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 379
    :goto_0
    return-void

    .line 358
    :cond_0
    if-eqz p1, :cond_2

    .line 359
    invoke-static {p0, p2}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isRegisteredDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 360
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->tvRegisterCarSummary:Landroid/widget/TextView;

    .line 362
    const v1, 0x7f0a03e9

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 363
    invoke-static {p0, p2}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->getDeviceRegisteredName(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 361
    invoke-virtual {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 366
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->tvRegisterCarSummary:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->getRegisteredCar(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    move-result-object v2

    .line 367
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getPlaceName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 368
    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 369
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 370
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 369
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 366
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 373
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->tvRegisterCarSummary:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->getRegisteredCar(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    move-result-object v2

    .line 374
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getPlaceName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 375
    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 376
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 377
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 376
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 373
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public onBondStateChanged(Landroid/bluetooth/BluetoothDevice;I)V
    .locals 0
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "state"    # I

    .prologue
    .line 396
    return-void
.end method

.method public onConnect(Landroid/bluetooth/BluetoothDevice;)V
    .locals 2
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 383
    const-string/jumbo v0, "[SettingsActivity]"

    const-string/jumbo v1, "onConnect()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->setRegisterCarSubtitle(ZLandroid/bluetooth/BluetoothDevice;)V

    .line 385
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v3, 0x7f0a03d4

    const/16 v2, 0x8

    .line 69
    const-string/jumbo v0, "[SettingsActivity]"

    const-string/jumbo v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 72
    const v0, 0x7f03002e

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->setContentView(I)V

    .line 74
    iput-object p0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->mContext:Landroid/content/Context;

    .line 76
    const v0, 0x7f09012d

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->btnNavBack:Landroid/widget/LinearLayout;

    .line 77
    const v0, 0x7f090132

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutRegisterCar:Landroid/widget/LinearLayout;

    .line 78
    const v0, 0x7f090130

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutLanguage:Landroid/widget/LinearLayout;

    .line 79
    const v0, 0x7f090134

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutRejectMessage:Landroid/widget/LinearLayout;

    .line 80
    const v0, 0x7f090136

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutSuggestedContacts:Landroid/widget/LinearLayout;

    .line 81
    const v0, 0x7f090137

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutNavigation:Landroid/widget/LinearLayout;

    .line 82
    const v0, 0x7f09013a

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutMyPlace:Landroid/widget/LinearLayout;

    .line 83
    const v0, 0x7f09013c

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutItemServerDataReset:Landroid/widget/LinearLayout;

    .line 84
    const v0, 0x7f09013d

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutServerDataReset:Landroid/widget/LinearLayout;

    .line 85
    const v0, 0x7f09013f

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutDebugSettings:Landroid/widget/LinearLayout;

    .line 86
    const v0, 0x7f090140

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutTermsOfService:Landroid/widget/LinearLayout;

    .line 87
    const v0, 0x7f090133

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->tvRegisterCarSummary:Landroid/widget/TextView;

    .line 88
    const v0, 0x7f090131

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->tvLanguageSummary:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f090135

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->tvRejectMessageSummary:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f090138

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->tvNavigationSummary:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f090139

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->dividerNavigation:Landroid/view/View;

    .line 94
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 93
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 95
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    invoke-interface {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestBluetoothStartAdapter(Landroid/content/Context;)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    invoke-virtual {v0, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->register(Landroid/content/Context;)V

    .line 98
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->setInitialValues(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;)V

    .line 100
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->numberInstalledNavigationApps()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 101
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutNavigation:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->dividerNavigation:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 105
    :cond_0
    const-string/jumbo v0, "DELETE_USER_DATA"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->isEnableUserDataReset()Z

    move-result v0

    if-nez v0, :cond_2

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutItemServerDataReset:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 110
    :cond_2
    const-string/jumbo v0, "HIDDEN_FEATURE_DEBUG_SETTING"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 111
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutDebugSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 114
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutRegisterCar:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutLanguage:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutRejectMessage:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutSuggestedContacts:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 209
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutNavigation:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutMyPlace:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutServerDataReset:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$7;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutDebugSettings:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$8;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->layoutTermsOfService:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$9;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$9;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->btnNavBack:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    .line 262
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 263
    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 264
    const v2, 0x7f0a03f4

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 261
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->btnNavBack:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$10;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$10;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 273
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->setTitle(I)V

    .line 274
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 400
    const-string/jumbo v0, "[SettingsActivity]"

    const-string/jumbo v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    const v0, 0xddd5

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->finishActivity(I)V

    .line 402
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->mReceiver:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 403
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onDestroy()V

    .line 404
    return-void
.end method

.method public onDisconnect(Landroid/bluetooth/BluetoothDevice;)V
    .locals 2
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 389
    const-string/jumbo v0, "[SettingsActivity]"

    const-string/jumbo v1, "onDisconnect()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->setRegisterCarSubtitle(ZLandroid/bluetooth/BluetoothDevice;)V

    .line 391
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 278
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onPause()V

    .line 279
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 283
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onResume()V

    .line 285
    const-string/jumbo v1, "[SettingsActivity]"

    const-string/jumbo v2, "onResume"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 287
    const-string/jumbo v1, "[SettingsActivity]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "getDrivingStatus()"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 288
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 287
    invoke-static {v1, v2}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->SettingsDrivingDialog(Landroid/content/Context;)V

    .line 293
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 294
    const-string/jumbo v2, "drivelink_rejectmessage_on"

    .line 293
    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 295
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->tvRejectMessageSummary:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getRejectMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 306
    :goto_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->numberInstalledNavigationApps()I

    move-result v1

    if-le v1, v4, :cond_1

    .line 307
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->tvNavigationSummary:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->getResourcesID()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 310
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->tvLanguageSummary:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 312
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->setRegisterCarSubtitle()V

    .line 313
    return-void

    .line 297
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->tvRejectMessageSummary:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 298
    const v3, 0x7f0a0457

    .line 297
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 300
    :catch_0
    move-exception v0

    .line 301
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 302
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :catch_1
    move-exception v0

    .line 303
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v0}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_0
.end method
