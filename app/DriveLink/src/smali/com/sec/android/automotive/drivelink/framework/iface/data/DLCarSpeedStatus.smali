.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLCarSpeedStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getActualSpeed()F
.end method

.method public abstract getCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;
.end method

.method public abstract getLocation()Landroid/location/Location;
.end method
