.class public Lcom/sec/android/automotive/drivelink/common/view/easing/ElasticEaseOut;
.super Ljava/lang/Object;
.source "ElasticEaseOut.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field public s:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/ElasticEaseOut;->s:F

    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/ElasticEaseOut;->s:F

    .line 16
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 13
    .param p1, "input"    # F

    .prologue
    const v12, 0x40490fdb    # (float)Math.PI

    const/high16 v11, 0x40000000    # 2.0f

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 20
    const/4 v1, 0x0

    .line 21
    .local v1, "p":F
    const/4 v0, 0x0

    .line 22
    .local v0, "a":F
    cmpl-float v5, p1, v3

    if-nez v5, :cond_0

    .line 37
    :goto_0
    return v3

    .line 25
    :cond_0
    div-float/2addr p1, v4

    cmpl-float v5, p1, v4

    if-nez v5, :cond_1

    move v3, v4

    .line 26
    goto :goto_0

    .line 28
    :cond_1
    cmpl-float v5, v1, v3

    if-nez v5, :cond_2

    .line 29
    const v1, 0x3e99999a    # 0.3f

    .line 31
    :cond_2
    cmpl-float v3, v0, v3

    if-eqz v3, :cond_3

    cmpg-float v3, v0, v4

    if-gez v3, :cond_4

    .line 32
    :cond_3
    const/high16 v0, 0x3f800000    # 1.0f

    .line 33
    const/high16 v3, 0x40800000    # 4.0f

    div-float v2, v1, v3

    .line 37
    .local v2, "s":F
    :goto_1
    float-to-double v5, v0

    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    const/high16 v3, -0x3ee00000    # -10.0f

    mul-float/2addr v3, p1

    float-to-double v9, v3

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v7

    mul-double/2addr v5, v7

    .line 38
    mul-float v3, p1, v4

    sub-float/2addr v3, v2

    mul-float/2addr v3, v12

    mul-float/2addr v3, v11

    div-float/2addr v3, v1

    invoke-static {v3}, Landroid/util/FloatMath;->sin(F)F

    move-result v3

    float-to-double v3, v3

    .line 37
    mul-double/2addr v3, v5

    .line 38
    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    .line 37
    add-double/2addr v3, v5

    .line 38
    const-wide/16 v5, 0x0

    .line 37
    add-double/2addr v3, v5

    double-to-float v3, v3

    goto :goto_0

    .line 35
    .end local v2    # "s":F
    :cond_4
    div-float v3, v1, v12

    mul-float/2addr v3, v11

    float-to-double v5, v3

    div-float v3, v4, v0

    float-to-double v7, v3

    invoke-static {v7, v8}, Ljava/lang/Math;->asin(D)D

    move-result-wide v7

    mul-double/2addr v5, v7

    double-to-float v2, v5

    .restart local v2    # "s":F
    goto :goto_1
.end method
