.class public Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;
.super Landroid/content/BroadcastReceiver;
.source "HeadsetPlugReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;
    }
.end annotation


# static fields
.field private static final ACTION_HEADSET_PLUG_STATE:Ljava/lang/String; = "android.intent.action.HEADSET_PLUG"

.field private static final TAG:Ljava/lang/String;

.field private static mInstance:Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;


# instance fields
.field private mHeadsetPlugListener:Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;

.field private mListenerCollection:Ljava/util/AbstractCollection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/AbstractCollection",
            "<",
            "Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->TAG:Ljava/lang/String;

    .line 25
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->mInstance:Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 33
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$1;-><init>(Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->mHeadsetPlugListener:Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;

    .line 44
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->mListenerCollection:Ljava/util/AbstractCollection;

    .line 45
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;)Ljava/util/AbstractCollection;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->mListenerCollection:Ljava/util/AbstractCollection;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->mInstance:Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 53
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    const-string/jumbo v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 61
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->updateHeadSetDeviceInfo(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public registerHeadsetPlugListener(Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;

    .prologue
    .line 93
    if-nez p1, :cond_0

    .line 98
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->mListenerCollection:Ljava/util/AbstractCollection;

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public registerReceiver(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 109
    new-instance v0, Landroid/content/IntentFilter;

    .line 110
    const-string/jumbo v1, "android.intent.action.HEADSET_PLUG"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 109
    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 111
    return-void
.end method

.method public unregisterHeadsetPlugListener(Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;

    .prologue
    .line 101
    if-nez p1, :cond_0

    .line 106
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->mListenerCollection:Ljava/util/AbstractCollection;

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public unregisterReceiver(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 115
    :try_start_0
    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :goto_0
    return-void

    .line 116
    :catch_0
    move-exception v0

    .line 117
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Exception: unregisterReceiver!!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public updateHeadSetDeviceInfo(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 67
    const-string/jumbo v1, "state"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 69
    .local v0, "state":I
    packed-switch v0, :pswitch_data_0

    .line 79
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "headset state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :goto_0
    return-void

    .line 71
    :pswitch_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "headset unplugged"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->mHeadsetPlugListener:Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;->onHeadsetPlugChange(Z)V

    goto :goto_0

    .line 75
    :pswitch_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "headset plugged"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->mHeadsetPlugListener:Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;->onHeadsetPlugChange(Z)V

    goto :goto_0

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
