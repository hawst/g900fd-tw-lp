.class Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$7;
.super Ljava/lang/Object;
.source "SettingsSuggestedContactsActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->loadContent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseRequestFavoriteContactList(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 180
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->access$7(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;Ljava/util/ArrayList;)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->loadListContents()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->access$8(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->mList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->access$9(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0xb

    if-gt v0, v1, :cond_0

    .line 184
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutAddContact:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 188
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    # invokes: Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->setTextViewFavoriteContactsCount(I)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->access$10(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;I)V

    .line 189
    return-void

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->layoutAddContact:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;->access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsSuggestedContactsActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method
