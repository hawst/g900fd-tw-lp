.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicContainer;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLMusicContainer.java"


# instance fields
.field private mName:Ljava/lang/String;

.field private mSongCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicContainer;->mName:Ljava/lang/String;

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicContainer;->mSongCount:I

    .line 29
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicContainer;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getSongCount()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicContainer;->mSongCount:I

    return v0
.end method

.method protected setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicContainer;->mName:Ljava/lang/String;

    .line 55
    return-void
.end method

.method protected setSongCount(I)V
    .locals 0
    .param p1, "songCount"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicContainer;->mSongCount:I

    .line 63
    return-void
.end method
