.class Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$17;
.super Ljava/lang/Object;
.source "PhoneActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getListItemOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    .line 1832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1836
    const-string/jumbo v5, "PhoneActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "currentMode : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$4(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1838
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->isFinishing()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1839
    const-string/jumbo v5, "PhoneActivity"

    const-string/jumbo v6, "Activity is finishing now!!"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1917
    :cond_0
    :goto_0
    return-void

    .line 1843
    :cond_1
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$4(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I

    move-result v5

    const/16 v6, 0x20

    if-eq v5, v6, :cond_0

    .line 1871
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$4(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I

    move-result v5

    const/16 v6, 0x10

    if-ne v5, v6, :cond_2

    .line 1872
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$31(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v6

    .line 1873
    mul-int/lit8 v6, v6, 0x4

    .line 1872
    add-int/2addr v5, v6

    add-int/lit8 v0, v5, -0x1

    .line 1874
    .local v0, "currentItem":I
    const-string/jumbo v5, "PhoneActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "currentItem : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1875
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactList:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$1(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 1877
    .local v4, "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v5

    .line 1878
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 1877
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 1879
    const-string/jumbo v5, "CM07"

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 1881
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    .line 1882
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getMainPhoneNumber(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)I
    invoke-static {v6, v4}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$47(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)I

    move-result v6

    .line 1881
    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->startPrepareDialActivity(ILcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;I)V
    invoke-static {v5, v0, v4, v6}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$46(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;ILcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;I)V

    goto :goto_0

    .line 1884
    .end local v0    # "currentItem":I
    .end local v4    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$4(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I

    move-result v5

    const/16 v6, 0x42

    if-ne v5, v6, :cond_0

    .line 1885
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$34(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v6

    mul-int/lit8 v6, v6, 0x4

    add-int/2addr v5, v6

    add-int/lit8 v0, v5, -0x1

    .line 1888
    .restart local v0    # "currentItem":I
    const-string/jumbo v5, "PhoneActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "currentItem : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1889
    const-string/jumbo v5, "PhoneActivity"

    .line 1890
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "mContactChoices size : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$48(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1889
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1893
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactChoices:Ljava/util/List;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$48(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 1896
    .local v4, "user":Lcom/vlingo/core/internal/contacts/ContactMatch;
    const-string/jumbo v5, "DM_DIAL_CONTACT_SEARCH_LIST"

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v2

    .line 1898
    .local v2, "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const/4 v3, -0x1

    .line 1899
    .local v3, "index":I
    if-eqz v2, :cond_3

    iget v5, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchListType:I

    if-ltz v5, :cond_3

    .line 1900
    const-string/jumbo v5, "PhoneActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "FP is not null :  "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1901
    iget v7, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchListType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1900
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1902
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    iget v6, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchListType:I

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getTypePhoneNumber(Lcom/vlingo/core/internal/contacts/ContactMatch;I)I
    invoke-static {v5, v4, v6}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$49(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Lcom/vlingo/core/internal/contacts/ContactMatch;I)I

    move-result v3

    .line 1908
    :goto_1
    const-string/jumbo v5, "PhoneActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "[getListItemOnClickListener] index : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1909
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1908
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1910
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->startPrepareDialActivity(ILcom/vlingo/core/internal/contacts/ContactMatch;I)V
    invoke-static {v5, v0, v4, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$51(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;ILcom/vlingo/core/internal/contacts/ContactMatch;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1911
    .end local v2    # "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v3    # "index":I
    .end local v4    # "user":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :catch_0
    move-exception v1

    .line 1912
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1904
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v2    # "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .restart local v3    # "index":I
    .restart local v4    # "user":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_3
    :try_start_1
    const-string/jumbo v5, "PhoneActivity"

    const-string/jumbo v6, "FP is null "

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1905
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$17;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getMainPhoneNumber(Lcom/vlingo/core/internal/contacts/ContactMatch;)I
    invoke-static {v5, v4}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$50(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Lcom/vlingo/core/internal/contacts/ContactMatch;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v3

    goto :goto_1
.end method
