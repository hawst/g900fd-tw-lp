.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationCommonPopup.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "[NotificationCommonPopup]"


# instance fields
.field protected mButton1:Landroid/widget/Button;

.field protected mButton2:Landroid/widget/Button;

.field protected mButton3:Landroid/widget/Button;

.field protected mIvTitleIcon:Landroid/widget/ImageView;

.field protected mLayoutButton:Landroid/widget/LinearLayout;

.field protected mLayoutContent:Landroid/widget/LinearLayout;

.field protected mLayoutTitle:Landroid/widget/LinearLayout;

.field protected mTvText:Landroid/widget/TextView;

.field protected mTvTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 27
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutTitle:Landroid/widget/LinearLayout;

    .line 28
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mIvTitleIcon:Landroid/widget/ImageView;

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvTitle:Landroid/widget/TextView;

    .line 30
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutContent:Landroid/widget/LinearLayout;

    .line 31
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvText:Landroid/widget/TextView;

    .line 32
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutButton:Landroid/widget/LinearLayout;

    .line 33
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton1:Landroid/widget/Button;

    .line 34
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton2:Landroid/widget/Button;

    .line 35
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton3:Landroid/widget/Button;

    .line 54
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->initCommonPopup(Landroid/content/Context;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 27
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutTitle:Landroid/widget/LinearLayout;

    .line 28
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mIvTitleIcon:Landroid/widget/ImageView;

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvTitle:Landroid/widget/TextView;

    .line 30
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutContent:Landroid/widget/LinearLayout;

    .line 31
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvText:Landroid/widget/TextView;

    .line 32
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutButton:Landroid/widget/LinearLayout;

    .line 33
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton1:Landroid/widget/Button;

    .line 34
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton2:Landroid/widget/Button;

    .line 35
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton3:Landroid/widget/Button;

    .line 48
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->initCommonPopup(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 27
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutTitle:Landroid/widget/LinearLayout;

    .line 28
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mIvTitleIcon:Landroid/widget/ImageView;

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvTitle:Landroid/widget/TextView;

    .line 30
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutContent:Landroid/widget/LinearLayout;

    .line 31
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvText:Landroid/widget/TextView;

    .line 32
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutButton:Landroid/widget/LinearLayout;

    .line 33
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton1:Landroid/widget/Button;

    .line 34
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton2:Landroid/widget/Button;

    .line 35
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton3:Landroid/widget/Button;

    .line 42
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->initCommonPopup(Landroid/content/Context;)V

    .line 43
    return-void
.end method


# virtual methods
.method protected addButton(ILandroid/view/View$OnClickListener;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;
    .locals 4
    .param p1, "resouceId"    # I
    .param p2, "clickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 147
    const/4 v0, 0x0

    .line 148
    .local v0, "invisibleButton":Landroid/widget/Button;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton1:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_2

    .line 149
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton1:Landroid/widget/Button;

    .line 156
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 157
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 158
    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 161
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutButton:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 162
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutButton:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 166
    :cond_1
    return-object p0

    .line 150
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton2:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_3

    .line 151
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton2:Landroid/widget/Button;

    .line 152
    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton3:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton3:Landroid/widget/Button;

    goto :goto_0
.end method

.method protected addButton(Ljava/lang/String;Landroid/view/View$OnClickListener;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "clickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 172
    if-nez p1, :cond_1

    .line 195
    :cond_0
    :goto_0
    return-object p0

    .line 176
    :cond_1
    const/4 v0, 0x0

    .line 177
    .local v0, "invisibleButton":Landroid/widget/Button;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton1:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_3

    .line 178
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton1:Landroid/widget/Button;

    .line 185
    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    .line 186
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 187
    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 190
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutButton:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 191
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutButton:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 179
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton2:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_4

    .line 180
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton2:Landroid/widget/Button;

    .line 181
    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton3:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_2

    .line 182
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton3:Landroid/widget/Button;

    goto :goto_1
.end method

.method protected initCommonPopup(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    if-nez p1, :cond_0

    .line 59
    const-string/jumbo v2, "[NotificationCommonPopup]"

    const-string/jumbo v3, "Context is null!"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    :goto_0
    return-void

    .line 63
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 64
    const-string/jumbo v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 63
    check-cast v0, Landroid/view/LayoutInflater;

    .line 65
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f0300a2

    .line 66
    const/4 v3, 0x0

    .line 65
    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 67
    .local v1, "view":Landroid/view/View;
    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->addView(Landroid/view/View;)V

    .line 69
    const v2, 0x7f0902a9

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutTitle:Landroid/widget/LinearLayout;

    .line 70
    const v2, 0x7f0902aa

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mIvTitleIcon:Landroid/widget/ImageView;

    .line 71
    const v2, 0x7f090027

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvTitle:Landroid/widget/TextView;

    .line 72
    const v2, 0x7f0902ab

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutContent:Landroid/widget/LinearLayout;

    .line 73
    const v2, 0x7f0902bb

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvText:Landroid/widget/TextView;

    .line 74
    const v2, 0x7f0902ac

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutButton:Landroid/widget/LinearLayout;

    .line 75
    const v2, 0x7f0902ad

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton1:Landroid/widget/Button;

    .line 76
    const v2, 0x7f0902ae

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton2:Landroid/widget/Button;

    .line 77
    const v2, 0x7f0902af

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mButton3:Landroid/widget/Button;

    goto :goto_0
.end method

.method protected seTitle(I)Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;
    .locals 3
    .param p1, "resourceId"    # I

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 115
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 118
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvTitle:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 122
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 124
    :cond_1
    return-object p0
.end method

.method protected setIcon(I)Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;
    .locals 3
    .param p1, "resourceId"    # I

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 82
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mIvTitleIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mIvTitleIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 85
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mIvTitleIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 89
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 92
    :cond_1
    return-object p0
.end method

.method protected setIcon(Landroid/graphics/drawable/Drawable;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;
    .locals 3
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 96
    if-nez p1, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-object p0

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mIvTitleIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mIvTitleIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 103
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mIvTitleIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 106
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 107
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected setText(I)Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;
    .locals 3
    .param p1, "resourceId"    # I

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 201
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 204
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutContent:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 208
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutContent:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 211
    :cond_1
    return-object p0
.end method

.method protected setText(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 215
    if-nez p1, :cond_1

    .line 229
    :cond_0
    :goto_0
    return-object p0

    .line 219
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 222
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutContent:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 226
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutContent:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected setTitle(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;
    .locals 3
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 128
    if-nez p1, :cond_1

    .line 141
    :cond_0
    :goto_0
    return-object p0

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 135
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mLayoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 138
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvTitle:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 139
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonPopup;->mTvTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
