.class public Lcom/sec/android/automotive/drivelink/location/service/GetGroupThread;
.super Ljava/lang/Object;
.source "GetGroupThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private groupId:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "groupId"    # I

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput p1, p0, Lcom/sec/android/automotive/drivelink/location/service/GetGroupThread;->groupId:I

    .line 11
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 15
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;-><init>()V

    .line 16
    .local v0, "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    iget v1, p0, Lcom/sec/android/automotive/drivelink/location/service/GetGroupThread;->groupId:I

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->setGroupId(I)V

    .line 17
    return-void
.end method
