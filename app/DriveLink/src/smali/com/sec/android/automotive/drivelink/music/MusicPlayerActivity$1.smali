.class Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$1;
.super Landroid/os/Handler;
.source "MusicPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    .line 1066
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x6

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1070
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 1072
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1121
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "Unkown MSG !!!!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1124
    :cond_0
    :goto_0
    return-void

    .line 1074
    :pswitch_0
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "MSG_AUTO_SHRINK - handling"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1076
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->isNotificationTopActivity()Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$0(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1077
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$1(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/os/Handler;

    move-result-object v0

    .line 1078
    const-wide/16 v1, 0x1f4

    .line 1077
    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 1082
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mTouchEvent:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$2(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1083
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->finish()V

    goto :goto_0

    .line 1087
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->isNotificationTopActivity()Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$0(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1088
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$1(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1089
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    goto :goto_0

    .line 1094
    :pswitch_2
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "MSG_AUTO_DISAPPEAR_SOUND - handling"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1095
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundView(Z)V
    invoke-static {v0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$4(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 1096
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    goto :goto_0

    .line 1100
    :pswitch_3
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "MSG_PLAYER_DIM_ON"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1101
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setBtnEnabled(Z)V

    goto :goto_0

    .line 1105
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setBtnEnabled(Z)V

    .line 1106
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "MSG_PLAYER_DIM_OFF"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1111
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevBtn:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$5(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1112
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mNextBtn:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$6(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0

    .line 1116
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevBtn:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$5(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1117
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mNextBtn:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$6(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0

    .line 1072
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_1
    .end packed-switch
.end method
