.class Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$8;
.super Ljava/lang/Object;
.source "NotificationItem.java"

# interfaces
.implements Landroid/media/SoundPool$OnLoadCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->playSound(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$8;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .line 807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadComplete(Landroid/media/SoundPool;II)V
    .locals 2
    .param p1, "arg0"    # Landroid/media/SoundPool;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I

    .prologue
    .line 813
    if-nez p3, :cond_0

    .line 814
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$8;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mRingtoneListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$6(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$8;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->soundThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;

    if-eqz v0, :cond_0

    .line 815
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$8;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->soundThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;

    .line 816
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$8;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mRingtoneListener:Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$6(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->setOnSoundFinishedListener(Lcom/sec/android/automotive/drivelink/notification/OnSoundFinishedListener;)V

    .line 817
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$8;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->soundThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;

    iput-object p1, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->soundPool:Landroid/media/SoundPool;

    .line 818
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$8;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->soundThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;

    iput p2, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->sampleId:I

    .line 820
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$8;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->soundThread:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiSoundThread;->start()V

    .line 823
    :cond_0
    return-void
.end method
