.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationLocationRequestDialogView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I = null

.field private static final MAX_ATTEMPT:I = 0x2

.field private static final TAG:Ljava/lang/String; = "NotificationLocationRequestDialogView"

.field private static count:Ljava/lang/Integer;

.field private static isActive:Z

.field private static isCancel:Z


# instance fields
.field private handler:Landroid/os/Handler;

.field private mCancelButton:Landroid/widget/Button;

.field private mContactBitMap:Landroid/widget/ImageView;

.field protected mContext:Landroid/content/Context;

.field private mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private mName:Ljava/lang/String;

.field private mNameView:Landroid/widget/TextView;

.field private mNumber:Ljava/lang/String;

.field private mShareButton:Landroid/widget/Button;

.field private mUrl:Ljava/lang/String;

.field private mViewLocal:Landroid/view/View;

.field private scheduler:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 43
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->isActive:Z

    .line 49
    sput-boolean v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->isCancel:Z

    .line 64
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->count:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v2, 0x0

    .line 112
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 53
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContext:Landroid/content/Context;

    .line 54
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNameView:Landroid/widget/TextView;

    .line 56
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mName:Ljava/lang/String;

    .line 57
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNumber:Ljava/lang/String;

    .line 58
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mUrl:Ljava/lang/String;

    .line 59
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContactBitMap:Landroid/widget/ImageView;

    .line 60
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mViewLocal:Landroid/view/View;

    .line 61
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mShareButton:Landroid/widget/Button;

    .line 62
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mCancelButton:Landroid/widget/Button;

    .line 67
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    .line 69
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 196
    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->handler:Landroid/os/Handler;

    .line 114
    invoke-virtual {p4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;

    .line 116
    .local v0, "locInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;
    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mName:Ljava/lang/String;

    .line 118
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getNumber()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNumber:Ljava/lang/String;

    .line 119
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getUrl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mUrl:Ljava/lang/String;

    .line 126
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 127
    return-void

    .line 121
    :cond_0
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mName:Ljava/lang/String;

    .line 122
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNumber:Ljava/lang/String;

    .line 123
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const/4 v2, 0x0

    .line 92
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 53
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContext:Landroid/content/Context;

    .line 54
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNameView:Landroid/widget/TextView;

    .line 56
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mName:Ljava/lang/String;

    .line 57
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNumber:Ljava/lang/String;

    .line 58
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mUrl:Ljava/lang/String;

    .line 59
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContactBitMap:Landroid/widget/ImageView;

    .line 60
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mViewLocal:Landroid/view/View;

    .line 61
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mShareButton:Landroid/widget/Button;

    .line 62
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mCancelButton:Landroid/widget/Button;

    .line 67
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    .line 69
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 196
    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->handler:Landroid/os/Handler;

    .line 94
    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;

    .line 96
    .local v0, "locInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;
    if-eqz v0, :cond_0

    .line 97
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mName:Ljava/lang/String;

    .line 98
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getNumber()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNumber:Ljava/lang/String;

    .line 99
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getUrl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mUrl:Ljava/lang/String;

    .line 106
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 107
    return-void

    .line 101
    :cond_0
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mName:Ljava/lang/String;

    .line 102
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNumber:Ljava/lang/String;

    .line 103
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v2, 0x0

    .line 73
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 53
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContext:Landroid/content/Context;

    .line 54
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNameView:Landroid/widget/TextView;

    .line 56
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mName:Ljava/lang/String;

    .line 57
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNumber:Ljava/lang/String;

    .line 58
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mUrl:Ljava/lang/String;

    .line 59
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContactBitMap:Landroid/widget/ImageView;

    .line 60
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mViewLocal:Landroid/view/View;

    .line 61
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mShareButton:Landroid/widget/Button;

    .line 62
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mCancelButton:Landroid/widget/Button;

    .line 67
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    .line 69
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 196
    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->handler:Landroid/os/Handler;

    .line 75
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;

    .line 77
    .local v0, "locInfo":Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;
    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mName:Ljava/lang/String;

    .line 79
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getNumber()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNumber:Ljava/lang/String;

    .line 80
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotiLocInfo;->getUrl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mUrl:Ljava/lang/String;

    .line 87
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 88
    return-void

    .line 82
    :cond_0
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mName:Ljava/lang/String;

    .line 83
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNumber:Ljava/lang/String;

    .line 84
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method private acceptDisclaimer()V
    .locals 0

    .prologue
    .line 325
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->acceptShareMyLocation()V

    .line 326
    return-void
.end method

.method private acceptShareMyLocation()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 329
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNumber:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNumber:Ljava/lang/String;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 330
    :cond_0
    const-string/jumbo v1, "NotificationLocationRequestDialogView"

    const-string/jumbo v2, "Friend\'s phone number not found."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    :cond_1
    :goto_0
    return-void

    .line 335
    :cond_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 336
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNumber:Ljava/lang/String;

    .line 335
    invoke-interface {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getContactFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v0

    .line 337
    .local v0, "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-nez v0, :cond_3

    .line 338
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 340
    .local v6, "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    const/16 v2, 0xc

    .line 341
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNumber:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;-><init>(ILjava/lang/String;)V

    .line 340
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 342
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .end local v0    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    const-wide/16 v1, -0x1

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNumber:Ljava/lang/String;

    const-string/jumbo v4, ""

    const-string/jumbo v5, ""

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 345
    .end local v6    # "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    .restart local v0    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :cond_3
    const-string/jumbo v1, "DM_LOCATION_SHARE_CONFIRM"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v7

    .line 349
    .local v7, "params":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v7, :cond_1

    .line 351
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mUrl:Ljava/lang/String;

    iput-object v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUrl:Ljava/lang/String;

    .line 352
    invoke-static {v0}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v1

    iput-object v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 353
    iget-object v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->firstName:Ljava/lang/String;

    .line 354
    iput-boolean v9, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mIsShareMyLocation:Z

    .line 355
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 356
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNumber:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getContactImageFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 355
    iput-object v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mBitmap:Landroid/graphics/Bitmap;

    .line 357
    iget-object v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_4

    .line 359
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02024e

    .line 358
    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mBitmap:Landroid/graphics/Bitmap;

    .line 361
    :cond_4
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 362
    const v2, 0x7f0a084f

    .line 361
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 364
    .local v8, "prompt":Ljava/lang/String;
    new-array v2, v9, [Ljava/lang/Object;

    .line 365
    if-nez v0, :cond_5

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_1
    aput-object v1, v2, v10

    .line 364
    invoke-static {v8, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 366
    iget-object v1, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->extras:Landroid/os/Bundle;

    const-string/jumbo v2, "CONTINUE_REQUEST_SHARE_LOCATION"

    invoke-virtual {v1, v2, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 369
    const-string/jumbo v1, "DM_LOCATION_SHARE_CONFIRM"

    .line 368
    invoke-static {v1, v7}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto/16 :goto_0

    .line 365
    :cond_5
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)V
    .locals 0

    .prologue
    .line 324
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->acceptDisclaimer()V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)V
    .locals 0

    .prologue
    .line 223
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->disableTimer()V

    return-void
.end method

.method static synthetic access$4(Z)V
    .locals 0

    .prologue
    .line 47
    sput-boolean p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->isActive:Z

    return-void
.end method

.method static synthetic access$5(Z)V
    .locals 0

    .prologue
    .line 49
    sput-boolean p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->isCancel:Z

    return-void
.end method

.method static synthetic access$6()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->count:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$7()Z
    .locals 1

    .prologue
    .line 49
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->isCancel:Z

    return v0
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$9(Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 64
    sput-object p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->count:Ljava/lang/Integer;

    return-void
.end method

.method private disableTimer()V
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->isActive:Z

    .line 225
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->isShutdown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->isTerminated()Z

    move-result v0

    if-nez v0, :cond_1

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdownNow()Ljava/util/List;

    .line 228
    :cond_1
    return-void
.end method

.method private disclaimer()V
    .locals 0

    .prologue
    .line 321
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->initPlaceOnDisclaimnerDialog()V

    .line 322
    return-void
.end method

.method private enableTimer()V
    .locals 8

    .prologue
    const-wide/16 v2, 0x3

    const/4 v7, 0x0

    .line 231
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->isActive:Z

    if-eqz v0, :cond_0

    .line 232
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->count:Ljava/lang/Integer;

    .line 233
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    .line 234
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$2;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)V

    .line 246
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    move-wide v4, v2

    .line 234
    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 247
    sput-boolean v7, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->isActive:Z

    .line 249
    :cond_0
    return-void
.end method

.method private getLocationBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    const v4, 0x7f02023b

    .line 374
    const/4 v1, 0x0

    .line 380
    .local v1, "maskedBitmap":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 381
    const v3, 0x7f020237

    .line 379
    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 382
    .local v0, "contactBitmap":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v0, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 385
    if-nez v1, :cond_0

    .line 386
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 387
    const v3, 0x7f02023d

    .line 386
    invoke-static {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 391
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 392
    const v3, 0x7f02023c

    .line 391
    invoke-static {v2, v1, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageOverlay(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    return-object v2
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v4, 0x7f0902b8

    const v3, 0x7f0902b7

    .line 130
    if-nez p1, :cond_0

    .line 168
    :goto_0
    return-void

    .line 134
    :cond_0
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContext:Landroid/content/Context;

    .line 136
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 135
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 139
    const-string/jumbo v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 138
    check-cast v0, Landroid/view/LayoutInflater;

    .line 141
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mViewLocal:Landroid/view/View;

    if-nez v1, :cond_1

    .line 143
    const v1, 0x7f0300b0

    .line 142
    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mViewLocal:Landroid/view/View;

    .line 145
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mViewLocal:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 146
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mViewLocal:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 148
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mViewLocal:Landroid/view/View;

    const v2, 0x7f0902de

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNameView:Landroid/widget/TextView;

    .line 150
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mViewLocal:Landroid/view/View;

    .line 151
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 150
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mShareButton:Landroid/widget/Button;

    .line 152
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mViewLocal:Landroid/view/View;

    .line 153
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 152
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mCancelButton:Landroid/widget/Button;

    .line 155
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNameView:Landroid/widget/TextView;

    .line 156
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mName:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 157
    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 159
    const v3, 0x7f0a0316

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 158
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 156
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    const v1, 0x7f0902e0

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContactBitMap:Landroid/widget/ImageView;

    .line 162
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContactBitMap:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mNumber:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->getLocationBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 164
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->readNotificationTTS()V

    .line 167
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->setUseAutoTimeout(Z)V

    goto/16 :goto_0
.end method

.method private initPlaceOnDisclaimnerDialog()V
    .locals 3

    .prologue
    .line 270
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;-><init>()V

    .line 271
    .local v0, "dialog":Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;
    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$3;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView$3;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->setOnClickListener(Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;)V

    .line 316
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    .line 317
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "dialog 3"

    .line 316
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 318
    return-void
.end method

.method private positiveButton()V
    .locals 3

    .prologue
    .line 258
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 259
    const-string/jumbo v1, "PREF_ACCEPT_DISCLAIMER_PLACE_ON"

    const/4 v2, 0x0

    .line 258
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v0

    .line 259
    if-eqz v0, :cond_0

    .line 260
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->acceptDisclaimer()V

    .line 261
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->disableTimer()V

    .line 262
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->dismiss()V

    .line 266
    :goto_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->isCancel:Z

    .line 267
    return-void

    .line 264
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->disclaimer()V

    goto :goto_0
.end method

.method private readNotificationTTS()V
    .locals 3

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cancelNotiFlow()V

    .line 173
    const-string/jumbo v0, "DM_LOCATION_SHARE_CONFIRM"

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->setVoiceFlowId(Ljava/lang/String;)V

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addBegin()V

    .line 175
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->getTTSText()Ljava/lang/String;

    move-result-object v1

    .line 176
    const-string/jumbo v2, "DM_LOCATION_SHARE_CONFIRM"

    .line 175
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTSWithFlowId(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addFinish()V

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdFlowStart()V

    .line 179
    return-void
.end method


# virtual methods
.method public actionCancel()V
    .locals 2

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->dismiss()V

    .line 217
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->disableTimer()V

    .line 218
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 219
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v1

    .line 218
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->removeNotification(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    .line 220
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->isCancel:Z

    .line 221
    return-void
.end method

.method protected getTTSText()Ljava/lang/String;
    .locals 5

    .prologue
    .line 397
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 398
    const v3, 0x7f0a061d

    .line 397
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 399
    .local v0, "prompt":Ljava/lang/String;
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mName:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mName:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 400
    .local v1, "promptDefault":Ljava/lang/String;
    return-object v1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 183
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 184
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 194
    :goto_0
    return-void

    .line 186
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->actionCancel()V

    goto :goto_0

    .line 189
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->positiveButton()V

    goto :goto_0

    .line 184
    :pswitch_data_0
    .packed-switch 0x7f0902b7
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onFlowCommandCancel(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 436
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->actionCancel()V

    .line 437
    return-void
.end method

.method public onFlowCommandNo(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 441
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->actionCancel()V

    .line 442
    return-void
.end method

.method public onFlowCommandYes(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 429
    const-string/jumbo v0, "DM_LOCATION_SHARE_CONFIRM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->positiveButton()V

    .line 432
    :cond_0
    return-void
.end method

.method public onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 405
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p2}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 416
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V

    .line 417
    return-void

    .line 407
    :pswitch_0
    new-array v0, v4, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mShareButton:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mCancelButton:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->setButtonQuoted(Z[Landroid/widget/Button;)V

    goto :goto_0

    .line 410
    :pswitch_1
    new-array v0, v4, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mShareButton:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mCancelButton:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->setButtonQuoted(Z[Landroid/widget/Button;)V

    goto :goto_0

    .line 413
    :pswitch_2
    new-array v0, v4, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mShareButton:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->mCancelButton:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->setButtonQuoted(Z[Landroid/widget/Button;)V

    goto :goto_0

    .line 405
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onNotiCommandFinished(I)V
    .locals 1
    .param p1, "cmd"    # I

    .prologue
    .line 421
    const/16 v0, 0x9

    if-ne p1, v0, :cond_0

    .line 422
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->dismiss()V

    .line 424
    :cond_0
    return-void
.end method

.method public onNotificationWillDisappear()V
    .locals 0

    .prologue
    .line 253
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onNotificationWillDisappear()V

    .line 254
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationRequestDialogView;->enableTimer()V

    .line 255
    return-void
.end method
