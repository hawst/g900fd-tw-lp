.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestAcceptShareMyLocation.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDuration:J

.field private mGroupUrl:Ljava/lang/String;

.field private mMyLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

.field private mResult:Z

.field mStatus:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "groupUrl"    # Ljava/lang/String;
    .param p4, "status"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;
    .param p5, "myLocation"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p6, "duration"    # J

    .prologue
    const/4 v2, 0x0

    .line 22
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 11
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mContext:Landroid/content/Context;

    .line 12
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mDuration:J

    .line 13
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mMyLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mResult:Z

    .line 15
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->ACCEPTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mStatus:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    .line 16
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mGroupUrl:Ljava/lang/String;

    .line 23
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mContext:Landroid/content/Context;

    .line 24
    iput-object p5, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mMyLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 25
    iput-wide p6, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mDuration:J

    .line 26
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mStatus:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    .line 27
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mGroupUrl:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mContext:Landroid/content/Context;

    .line 50
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mMyLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 51
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mStatus:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    .line 52
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mGroupUrl:Ljava/lang/String;

    .line 53
    return-void
.end method

.method protected process()V
    .locals 7

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mGroupUrl:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mStatus:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    .line 34
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mMyLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    iget-wide v5, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mDuration:J

    .line 33
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->acceptShareMyLocation(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)Z

    move-result v0

    .line 32
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mResult:Z

    .line 35
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 40
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    move-result-object v0

    .line 41
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    if-eqz v0, :cond_0

    .line 43
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestAcceptShareMyLocation;->mResult:Z

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;->onResponseRequestAcceptShareMyLocation(Z)V

    .line 45
    :cond_0
    return-void
.end method
