.class public Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;
.super Landroid/app/Service;
.source "DLTaskObserverService.java"


# static fields
.field public static final ACTION_TASK_OBSERVER:Ljava/lang/String; = "com.sec.android.automotive.drivelink.ACTION_TASK_OBSERVER"

.field private static DELAY_TIME:J = 0x0L

.field public static final EXTRA_TASK_PKG_NAME:Ljava/lang/String; = "EXTRA_TASK_PKG_NAME"

.field public static final EXTRA_TASK_STATE:Ljava/lang/String; = "EXTRA_TASK_STATE"

.field private static final STATE_NOT_RUNNING:Ljava/lang/String; = "STATE_NOT_RUNNING"

.field private static final STATE_RUNNING:Ljava/lang/String; = "STATE_RUNNING"

.field private static final TAG:Ljava/lang/String; = "DLTaskObserverService"


# instance fields
.field private isFullPolling:Z

.field private mRunningAppMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTaskHandler:Landroid/os/Handler;

.field private mTaskReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    const-wide/16 v0, 0xbb8

    sput-wide v0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->DELAY_TIME:J

    .line 29
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 31
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->mRunningAppMap:Ljava/util/HashMap;

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->isFullPolling:Z

    .line 72
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$1;-><init>(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->mTaskHandler:Landroid/os/Handler;

    .line 109
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$2;-><init>(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->mTaskReceiver:Landroid/content/BroadcastReceiver;

    .line 20
    return-void
.end method

.method public static IsAppRunning(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 152
    .line 153
    const-string/jumbo v3, "activity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 152
    check-cast v0, Landroid/app/ActivityManager;

    .line 154
    .local v0, "activityManager":Landroid/app/ActivityManager;
    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    .line 156
    .local v2, "taskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v2, :cond_0

    .line 157
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 168
    .end local v1    # "i":I
    :cond_0
    const/4 v3, 0x0

    :goto_1
    return v3

    .line 158
    .restart local v1    # "i":I
    :cond_1
    const-string/jumbo v5, "DLTaskObserverService"

    .line 159
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "APP Pkg Name:"

    invoke-direct {v6, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 160
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 159
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 158
    invoke-static {v5, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 162
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v4

    .line 163
    goto :goto_1

    .line 157
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->mRunningAppMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->getAppRunningState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 193
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->sendTaskObserverDone(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->mTaskHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4()J
    .locals 2

    .prologue
    .line 22
    sget-wide v0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->DELAY_TIME:J

    return-wide v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->isFullPolling:Z

    return v0
.end method

.method private getAppRunningState(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 172
    const/4 v0, 0x0

    .line 174
    .local v0, "state":Ljava/lang/String;
    invoke-static {p0, p1}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->IsAppRunning(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 175
    const-string/jumbo v0, "STATE_RUNNING"

    .line 180
    :goto_0
    return-object v0

    .line 177
    :cond_0
    const-string/jumbo v0, "STATE_NOT_RUNNING"

    goto :goto_0
.end method

.method public static getTaskObserverIntent(ILjava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "state"    # I
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 184
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 185
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.automotive.drivelink.ACTION_TASK_OBSERVER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 186
    const-string/jumbo v1, "EXTRA_TASK_STATE"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 187
    const-string/jumbo v1, "EXTRA_TASK_PKG_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 188
    const-string/jumbo v1, "com.sec.android.automotive.drivelink"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 190
    return-object v0
.end method

.method private registerReceiver()V
    .locals 2

    .prologue
    .line 67
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "com.sec.android.automotive.drivelink.ACTION_TASK_OBSERVER"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 68
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->mTaskReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 70
    return-void
.end method

.method private sendTaskObserverDone(ILjava/lang/String;)V
    .locals 3
    .param p1, "state"    # I
    .param p2, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 194
    const-string/jumbo v0, "DLTaskObserverService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "sendTaskObserverDone :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    invoke-static {p1, p2}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->getTaskObserverIntent(ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->sendBroadcast(Landroid/content/Intent;)V

    .line 196
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 37
    const-string/jumbo v0, "DLTaskObserverService"

    const-string/jumbo v1, "[onBind] - Unsupported"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 43
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 44
    const-string/jumbo v0, "DLTaskObserverService"

    const-string/jumbo v1, "[onCreate]"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->registerReceiver()V

    .line 47
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 51
    const-string/jumbo v0, "DLTaskObserverService"

    const-string/jumbo v1, "[onDestroy] - Unsupported"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 54
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->mTaskHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 55
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->mTaskReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 57
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 61
    const-string/jumbo v0, "DLTaskObserverService"

    const-string/jumbo v1, "[onStartCommand]"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    const/4 v0, 0x1

    return v0
.end method
