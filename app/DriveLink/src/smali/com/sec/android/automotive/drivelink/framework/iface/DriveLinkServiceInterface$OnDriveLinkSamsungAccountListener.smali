.class public interface abstract Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;
.super Ljava/lang/Object;
.source "DriveLinkServiceInterface.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnDriveLinkSamsungAccountListener"
.end annotation


# virtual methods
.method public abstract onReceiveSamsungAccountAccessToken(ZLandroid/os/Bundle;)V
.end method

.method public abstract onReceiveSamsungAccountAuthCode(ZLandroid/os/Bundle;)V
.end method

.method public abstract onReceiveSamsungAccountChecklistValidation(ZLandroid/os/Bundle;)V
.end method

.method public abstract onReceiveSamsungAccountDisclaimerAgreement(ZLandroid/os/Bundle;)V
.end method

.method public abstract onReceiveSamsungAccountSCloudAccessToken(ZLandroid/os/Bundle;)V
.end method

.method public abstract onReceiveSamsungAccountUserName(ZLandroid/os/Bundle;)V
.end method

.method public abstract onResponseSamsungAccountConnectService(Z)V
.end method

.method public abstract onResponseSamsungAccountDisconnectService()V
.end method

.method public abstract onResponseSamsungAccountRegisterCallback(Z)V
.end method

.method public abstract onResponseSamsungAccountUnregisterCallback()V
.end method
